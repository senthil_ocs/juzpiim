<?php
// Heading
$_['heading_title']                           = 'Purchase Summary';

// Text

$_['text_tran_no']                   		 = 'Transaction No';
$_['text_tran_dt']                   		 = 'Transaction Date';
$_['text_supplier']                   		 = 'Supplier';
$_['text_total']                   		 	 = 'Total';

?>