<?php
// Heading
$_['heading_title']           = 'Transaction';

// Text

$_['text_general']             = 'Stock In/Out';
$_['text_stock']               = 'Stock Take';
$_['text_purchase']            = 'Purchase';
$_['text_auto_po']             = 'Auto PO';
$_['text_purchase_return']     = 'Purchase Return';
$_['text_sales']			   = 'Sales';
$_['text_sales_return']		   = 'Sales Return';
$_['text_stock_adjust']		   = 'Stock Adjustment';
$_['text_stock_take']		   = 'Stock Take';
$_['text_stock_updation']	   = 'Stock Updation';
$_['purchase_heading_title']   = 'Purchase';
$_['sales_heading_title']	   = 'Sales';
$_['stock_heading_title']	   = 'Stock';

$_['alt_purchase']             = 'ALT+P';
$_['alt_purchasereturn']       = 'ALT+R';
$_['alt_sales']                = 'ALT+S';
$_['alt_salesreturn']          = 'ALT+U';
$_['alt_stock']                = 'ALT+A';
//$_['alt_stockadjustment']      = 'ALT+J';

$_['alt_stocktake']            = 'ALT+T';
$_['alt_stockupdate']          = 'ALT+D';

$_['purchase_id']             = 'purchase';
$_['purchasereturn_id']       = 'purchase_r';
$_['sales_id']                = 'sales';
$_['salesreturn_id']          = 'sales_r';
$_['stock_id']                = 'stock';
$_['stockadjust_id']		  = 'stock_adjust';

$_['stocktake_id']            = 'stocktake';
$_['stockupdate_id']          = 'stockupdate';

?>