<?php
// Heading
$_['heading_title']			= 'Price Change by Item Group';

$_['text_success']           = 'Success: You have modified products!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_name']            = 'Product Name';
$_['column_code']            = 'Inventory Id';
$_['column_barcodes']        = 'Barcode(s)';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_name']             = 'Product Name:';
$_['entry_description']      = 'Description:';
$_['entry_date_available']   = 'Date Available:';
$_['entry_quantity']         = 'Quantity:';
$_['entry_minimum']          = 'Minimum Quantity:';
$_['entry_stock_status']     = 'Out Of Stock Status:';
$_['entry_price']            = 'Price:';
$_['entry_tax_class']        = 'Tax Class:';
$_['entry_subtract']         = 'Subtract Stock:';
$_['entry_weight_class']     = 'Weight Class:';
$_['entry_weight']           = 'Weight:';
$_['entry_length']           = 'Length Class:';
$_['entry_dimension']        = 'Dimensions (L x W x H):';
$_['entry_image']            = 'Image:';
$_['entry_date_start']       = 'Date Start:';
$_['entry_date_end']         = 'Date End:';
$_['entry_priority']         = 'Priority:';
$_['entry_text']             = 'Text:';
$_['entry_option']           = 'Option:';
$_['entry_option_value']     = 'Option Value:';
$_['entry_required']         = 'Required:';
$_['entry_status']           = 'Status:';
$_['entry_sort_order']       = 'Sort Order:';
$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';


$_['entry_name']				= 'Name:';
$_['entry_code']				= 'Inventory code:';
$_['entry_image']				= 'Product Image:';
$_['entry_department']			= 'Department:';
$_['entry_category']			= 'Category:';
$_['entry_brand']            	= 'Brand:';
$_['entry_origin']            	= 'Origin:';
$_['entry_uom']					= 'UOM:';	
$_['entry_bin']					= 'Bin:';
$_['entry_vendor']            	= 'Vendor Code:';
$_['entry_description']			= 'Description:';
$_['entry_shortdescription']	= 'Short Description:';
$_['entry_remarks']            	= 'Remarks:';
$_['entry_qoh']					= 'QOH:';
$_['entry_special']            	= 'Special Price:';
$_['entry_allowzeroprice']		= 'Allow Zero Price:';
$_['entry_nontaxitem']			= 'Non Tax Item:';
$_['entry_discontinued']		= 'Discontinued:';
$_['entry_price']            	= 'Retail Price:';
$_['entry_specialprice']		= 'Special Price:';
$_['entry_avgcost']            	= 'Avg Cost:';
$_['entry_discountcode']		= 'Coupon Code:';
$_['entry_unitcost']			= 'Unit Cost:';
$_['entry_profit']            	= 'Retail Profit %:';
$_['entry_barcode']            	= 'Barcode:';
$_['entry_status']       		= 'Active';
$_['tab_description']       	= 'Description';
$_['tab_data']       			= 'Price';
$_['tab_barcode']       		= 'Barcode';
$_['button_add_barcode']       	= 'Add Barcode';

// Error
$_['error_code']              	= 'Please enter valid code';
$_['error_department']        	= 'Please enter valid department';
$_['error_category']          	= 'Please enter valid category';
$_['error_brand']             	= 'Please enter valid brand';
$_['error_origin']            	= 'Please enter valid origin';
$_['error_uom']          	  	= 'Please enter valid uom';
$_['error_vendor']            	= 'Please enter valid vendor';
$_['error_price']          	  	= 'Please enter valid retail price';
$_['error_profit']            	= 'Please enter valid retail profit';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';



$_['button_submit']          = 'Submit';
$_['entry_change_status']    = 'Change Status';
$_['entry_change_price']     = 'Change Retail Price';
$_['entry_change_quantity']  = 'Change Quantity';



?>