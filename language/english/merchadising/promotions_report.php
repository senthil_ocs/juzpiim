<?php
// Heading
$_['promotion_heading_title'] = 'Inventory Promotion List';

//Report
$_['button_print']            = 'Print';
$_['column_invt_code']        = 'Inventory Code';
$_['column_selling_price']    = 'Selling Price';
$_['column_promo_price']      = 'Promo Price';
$_['column_quantity_on_hand'] = 'QtyOnHand';
$_['column_promotion_period'] = 'Promotion Period';
$_['button_search']           = 'Search';
$_['text_promotion_period']   = '<strong>Promotion Period Between:</strong>';
$_['text_from_date']          = 'From Date:';
$_['text_to_date']            = 'To Date:';

?>