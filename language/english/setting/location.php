<?php
// Heading
$_['heading_title']           = 'Location List';

// Text
$_['text_success']            = 'Success: You have modified Locations!';
$_['location_code']           = 'Location Code';

// Column
$_['column_name']             = 'Location Name';
$_['column_location_code']    = 'Location Code';
$_['column_country']          = 'Country';
$_['column_phone']			  = 'Phone No';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Location Name ';
$_['entry_code']        	  = 'Location Code ';
$_['entry_address']           = 'Address ';
$_['entry_country']    		  = 'Country ';
$_['entry_postcode'] 		  = 'Postcode ';
$_['entry_phone']             = 'Phone No ';

// Error
$_['error_permission']        = 'Warning: You do not have permission to modify countries!';
$_['error_name']              = 'Country Name must be between 3 and 128 characters!';
$_['error_default']           = 'Warning: This country cannot be deleted as it is currently assigned as the default store country!';
$_['error_store']             = 'Warning: This country cannot be deleted as it is currently assigned to %s stores!';
$_['error_address']           = 'Warning: This country cannot be deleted as it is currently assigned to %s address book entries!';
$_['error_affiliate']         = 'Warning: This country cannot be deleted as it is currently assigned to %s affiliates!';
$_['error_zone']              = 'Warning: This country cannot be deleted as it is currently assigned to %s zones!';
$_['error_zone_to_geo_zone']  = 'Warning: This country cannot be deleted as it is currently assigned to %s zones to geo zones!';

$_['entry_type']        	  = 'Type';
$_['column_type']    		  = 'Type';
?>