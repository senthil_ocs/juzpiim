<?php
// Heading
$_['heading_title']     = 'Payment Type Master List';
$_['edit_title']     	= 'Edit Payment Type Master';
$_['new_title']     	= 'New Payment Type Master';

// Text
$_['text_success']      = 'Success: You have modified Payment Type Master!';

// Column
$_['column_name']   = 'Name';
$_['column_status']  = 'Status';
$_['column_code']  = 'Code';
$_['column_xero_account_code']  = 'Xero Account Code';
$_['column_added_date'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']   	   = 'Name ';
$_['entry_code']   	   = 'Code';
$_['entry_xero_account_code']	   = 'Xero Account Code';
$_['entry_firstname']  = 'First Name ';
$_['entry_lastname']   = 'Last Name ';
$_['entry_email']      = 'E-Mail ';
$_['entry_user_group'] = 'User Group ';
$_['entry_status']     = 'Status ';
$_['entry_captcha']    = 'Enter the code in the box below ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify users!';
$_['error_account']    = 'Warning: You can not delete your own account!';
$_['error_exists']     = 'Warning: Username is already in use!';
$_['error_name']       = 'Payment Type Master Name must be between 3 and 20 characters!';
$_['error_code']       = 'Payment Type Master Code must be between 2 and 15 characters!';
$_['error_captcha']    = 'Verification code does not match the image!';
?>
