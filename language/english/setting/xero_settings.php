<?php
// Heading
$_['heading_title']     = 'Xero Settings List';
$_['edit_title']     	= 'Edit Xero Settings';
$_['new_title']     	= 'New Xero Settings';

// Text
$_['text_success']      = 'Success: You have modified Xero Settings!';

// Column
$_['column_name']   = 'Name';
$_['column_status']  = 'Status';
$_['column_available_to']  = 'Available To';
$_['column_xero_account_code']  = 'Xero Account Code';
$_['column_isNetwork']  = ' Is Network';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']   	   = 'Name ';
$_['entry_available_to']   = 'Available To';
$_['entry_isNetwork']	   = 'Is Network';
$_['entry_xero_account_code']  = 'Xero Account Code';
$_['entry_firstname']  = 'First Name ';
$_['entry_lastname']   = 'Last Name ';
$_['entry_email']      = 'E-Mail ';
$_['entry_user_group'] = 'User Group ';
$_['entry_status']     = 'Status ';
$_['entry_captcha']    = 'Enter the code in the box below ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify users!';
$_['error_account']    = 'Warning: You can not delete your own account!';
$_['error_exists']     = 'Warning: Username is already in use!';
$_['error_name']       = 'Xero Settings Name must be between 3 and 20 characters!';
$_['error_xero_acc_code'] = 'Xero Account Code is required';
$_['error_captcha']    = 'Verification code does not match the image!';
?>
