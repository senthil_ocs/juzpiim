<?php
// Heading
$_['heading_title']     = 'Terms List';
$_['edit_title']     	= 'Edit Terms';
$_['new_title']     	= 'New Terms';

// Text
$_['text_success']      = 'Success: You have modified Terms!';

// Column
$_['column_termsname']   = 'Name';
$_['column_termstatus']  = 'Status';
$_['column_termscode']  = 'Code';
$_['column_noof_days']  = 'Days';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']   	   = 'Name ';
$_['entry_code']   	   = 'Code';
$_['entry_days']	   = 'Days';
$_['entry_firstname']  = 'First Name ';
$_['entry_lastname']   = 'Last Name ';
$_['entry_email']      = 'E-Mail ';
$_['entry_user_group'] = 'User Group ';
$_['entry_status']     = 'Status ';
$_['entry_captcha']    = 'Enter the code in the box below ';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify users!';
$_['error_account']    = 'Warning: You can not delete your own account!';
$_['error_exists']     = 'Warning: Username is already in use!';
$_['error_termsname']  = 'Terms Name must be between 3 and 20 characters!';
$_['error_termscode']  = 'Terms Code must be between 2 and 15 characters!';
$_['error_captcha']    = 'Verification code does not match the image!';
?>
