<?php
// Heading
$_['heading_title']                = 'Common Settings';

// Text
$_['text_general']                 = 'General';
$_['text_company_setup']           = 'Company Setup';
$_['text_location_setup']          = 'Location Setup';
$_['text_user_group']              = 'User Group';
$_['text_user_setup']              = 'User Setup';
$_['text_user_rights']             = 'User Groups/Rights';
$_['text_general_setup']           = 'General Setup';
$_['text_tax_setup']               = 'Tax Setup';
$_['text_terms_setup']             = 'Terms Setup';
$_['text_currency_setup']          = 'Currency Setup';
$_['text_country_master']          = 'Country Master';
$_['text_stock_status']            = 'Stock Status';
$_['text_sales']                   = 'Sales';
$_['text_payment_type']            = 'Payment Type';
$_['text_customer_master']         = 'Customer Master';
$_['text_supplier']                = 'Supplier';
$_['text_supplier_master']         = 'Supplier Master';
$_['text_utilities']               = 'Utilities';
$_['text_adjust_payment']          = 'Adjust Payment';


$_['alt_company']              = 'ALT+C';
$_['alt_location']             = 'ALT+L';
$_['alt_user']                 = 'ALT+U';
$_['alt_usergroup']            = 'ALT+G';
$_['alt_generalsetup']         = 'ALT+S';
$_['alt_taxsetup']             = 'ALT+T';
$_['alt_termsetup']            = 'ALT+E';
$_['alt_currencysetup']        = 'ALT+R';
$_['alt_countrymaster']        = 'ALT+M';
$_['alt_stockstatus']          = 'ALT+K';

$_['alt_paymenttype']          = 'ALT+P';
$_['alt_customergroup']        = 'ALT+O';
$_['alt_customermaster']       = 'ALT+A';

$_['alt_suppliermaster']       = 'ALT+L';
$_['alt_adjustpayment']        = 'ALT+I';


$_['company_id']               = 'company';
$_['location_id']             = 'location';
$_['user_id']                 = 'user';
$_['usergroup_id']            = 'usergroup';
$_['generalsetup_id']         = 'generalsetup';
$_['taxsetup_id']             = 'taxsetup';
$_['termsetup_id']            = 'termsetup';
$_['currencysetup_id']        = 'currencysetup';
$_['countrymaster_id']        = 'countrymaster';
$_['stockstatus_id']          = 'stockstatus';

$_['paymenttype_id']          = 'paymenttype';
$_['customergroup_id']        = 'customergroup';
$_['customermaster_id']       = 'customermaster';

$_['suppliermaster_id']       = 'suppliermaster';
$_['adjustpayment_id']        = 'adjustpayment';



?>
