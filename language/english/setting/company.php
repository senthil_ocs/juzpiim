<?php
// Heading
$_['heading_title']                 = 'Company Setup';

// Text
$_['text_success']            		= 'Success: You have modified Companty Details!';

// Entry
$_['entry_location']			= 'Location Code:';
$_['entry_companyname']			= 'Company Name';
$_['entry_address']				= 'Address';
$_['entry_country']				= 'Country';
$_['entry_city']				= 'City';
$_['entry_state']				= 'State/Province';
$_['entry_postal']				= 'Postal Code';
$_['entry_email']				= 'Email';
$_['entry_weburl']				= 'WebPage';
$_['entry_phone']				= 'Phone';
$_['entry_fax']					= 'Fax';
$_['entry_remarks']				= 'Remarks';
$_['entry_businessregno']		= 'Business Reg No';
$_['entry_gstreg']				= 'GST Reg';
$_['entry_gstperc']				= 'GST Perc';
$_['entry_postgsttype']			= 'Post GST Type';
$_['entry_purchasegsttype']		= 'Purchase GST Type';
$_['entry_salesgsttype']		= 'Sales GST Type';
$_['entry_displaymessages1']	= 'Display Message 1';
$_['entry_displaymessages2']	= 'Display Message 2';
$_['entry_discount']			= 'Max.Discount %:';
$_['entry_pricechange']			= 'Price Change %:';
$_['entry_billmessage']			= 'Bill Message:';
$_['entry_promotions']			= 'Print Promotions and discounts detail in receipt.';
$_['entry_lastdate']			= 'Last EOD Date:';
$_['entry_configemail']			= 'Configure Email:';
$_['entry_server']				= 'Server:';
$_['entry_displayname']			= 'Display Name:';
$_['entry_fromemailid']			= 'From Email Id:';
$_['entry_password']			= 'Password:';
$_['entry_cpassword']			= 'Confirm Password:';
$_['entry_company_logo']		= 'Company Logo';

$_['button_update']			= 'Update';
$_['button_cancel']			= 'Cancel';


// Error
$_['error_name']              = 'Password must be between 6 characters!';
$_['error_permission'] 		  = 'Warning: You do not have permission to modify users!';



?>