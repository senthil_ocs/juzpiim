<?php
// Heading
$_['heading_title']           = 'Customers';

// Text

$_['text_general']             = 'Customer In/Out';
$_['text_customer']            = 'Customers';
$_['text_customer_pricing']    = 'Customer Pricing';
$_['text_stock']               = 'Stock Take';
$_['text_sales']			   = 'Sales';
$_['text_sales_return']		   = 'Sales Return';
$_['text_stock_adjust']		   = 'Stock Adjustment';
$_['text_stock_take']		   = 'Stock Take';
$_['text_stock_updation']	   = 'Stock Updation';

$_['alt_customers']            = 'ALT+P';
$_['alt_customerspricing']     = 'ALT+R';
$_['alt_sales']                = 'ALT+S';
$_['alt_salesreturn']          = 'ALT+U';
$_['alt_stock']                = 'ALT+A';
//$_['alt_stockadjustment']      = 'ALT+J';

$_['alt_stocktake']            = 'ALT+T';
$_['alt_stockupdate']          = 'ALT+D';

$_['purchase_id']             = 'purchase';
$_['purchasereturn_id']       = 'purchase_r';
$_['sales_id']                = 'sales';
$_['salesreturn_id']          = 'sales_r';
$_['stock_id']                = 'stock';
$_['stockadjust_id']		  = 'stock_adjust';

$_['stocktake_id']            = 'stocktake';
$_['stockupdate_id']          = 'stockupdate';

?>