<?php

// Heading

$_['heading_title']			 = 'B2B Customers';

$_['customer_pricing_title'] = 'Customer Pricing';

$_['sales_heading_title']    = 'Transaction Sales';

$_['stock_heading_title']    = 'Stock Adjustment';



/* code for transaction purchase */

$_['text_tran_no']          = 'Tran No';

$_['text_tran_dt']          = 'Tran Dt';

$_['text_tran_type']        = 'Tran Type';

$_['text_vendor_code']      = 'Vendor Code';

$_['text_vendor_name']      = 'Vendor Name';

$_['text_net_total']        = 'Net Total';

$_['text_created_by']       = 'Created By';

$_['text_created_on']       = 'Created On';



$_['entry_tran_no']         = 'Tran No ';

$_['entry_tran_dt']         = 'Tran Date ';

$_['entry_tran_type']       = 'Tran Type ';

$_['entry_vendor_code']     = 'Vendor Code ';

$_['entry_vendor_name']     = 'Vendor Name ';

$_['entry_net_total']       = 'Net Total ';

$_['entry_refno']           = 'Ref No ';

$_['entry_refdate']         = 'Ref Date ';

$_['entry_location']         = 'Location ';



$_['text_inventory_code']	= 'Inventory code';

$_['text_qty']	            = 'Qty';

$_['text_product_name']     = 'Product Name';

$_['text_price']	        = 'Unit Cost';

$_['text_raw_cost']	        = 'Cost';

$_['text_disc_perc']	    = 'Disc(%)';

$_['text_disc_price']	    = 'Disc($)';

$_['text_net_price']	    = 'Net Price';

$_['text_total']	        = 'Total';

$_['text_tax']	            = 'Tax';

$_['text_location']	         = 'Location';

$_['text_weight_class']     = 'Weight Class';

$_['text_tax_class']        = 'Tax Class';

$_['text_no_data']	        = 'No data!';

$_['button_add']	        = 'Add';

$_['button_clear']	        = 'Clear';

$_['column_action']         = 'Action';

$_['text_remove']	        = 'Remove';

$_['text_select_vendor']	= 'Please select vendor!';

$_['text_print']	        = 'Print';

$_['button_hold']	        = 'Hold';

$_['button_show_hold']	    = 'Show Hold List';

$_['button_back']	        = 'Back';

$_['text_modify']	        = 'Modify';

$_['text_hold']	            = 'Hold';

$_['text_return_qty']	    = 'Return Qty';

$_['text_stock_adjust_type'] = 'Stock Adjust Type';

$_['text_enter_transaction'] = 'Please enter transaction number';



$_['text_success']           = 'Success: You have added purchase!';

$_['stock_text_success']     = 'Success: You have modified inventory stock!';

$_['purchase_text_success']  = 'Success: You have added purchase!';



/* End */



$_['text_plus']              = '+';

$_['text_minus']             = '-';

$_['text_default']           = 'Default';

$_['text_image_manager']     = 'Image Manager';

$_['text_browse']            = 'Browse';

$_['text_clear']             = 'Clear';

$_['text_option']            = 'Option';

$_['text_option_value']      = 'Option Value';

$_['text_percent']           = 'Percentage';

$_['text_amount']            = 'Fixed Amount';



// Column

$_['column_name']            = 'Product Name';

$_['column_code']            = 'Inventory Id';

$_['column_image']           = 'Image';

$_['column_price']           = 'Price';

$_['column_quantity']        = 'Quantity';

$_['column_status']          = 'Status';



// Entry

$_['entry_name']             = 'Product Name:';

$_['entry_description']      = 'Description:';

$_['entry_date_available']   = 'Date Available:';

$_['entry_quantity']         = 'Quantity:';

$_['entry_minimum']          = 'Minimum Quantity:';

$_['entry_stock_status']     = 'Out Of Stock Status:';

$_['entry_price']            = 'Price:';

$_['entry_tax_class']        = 'Tax Class:';

$_['entry_subtract']         = 'Subtract Stock:';

$_['entry_weight_class']     = 'Weight Class:';

$_['entry_weight']           = 'Weight:';

$_['entry_length']           = 'Length Class:';

$_['entry_dimension']        = 'Dimensions (L x W x H):';

$_['entry_image']            = 'Image:';

$_['entry_date_start']       = 'Date Start:';

$_['entry_date_end']         = 'Date End:';

$_['entry_priority']         = 'Priority:';

$_['entry_text']             = 'Text:';

$_['entry_option']           = 'Option:';

$_['entry_option_value']     = 'Option Value:';

$_['entry_required']         = 'Required:';

$_['entry_status']           = 'Status:';

$_['entry_sort_order']       = 'Sort Order:';

$_['entry_filter']           = 'Filters:<br /><span class="help">(Autocomplete)</span>';





$_['entry_name']				= 'Name:';

$_['entry_code']				= 'Inventory code:';

$_['entry_image']				= 'Product Image:';

$_['entry_department']			= 'Department:';

$_['entry_category']			= 'Category:';

$_['entry_brand']            	= 'Brand:';

$_['entry_origin']            	= 'Origin:';

$_['entry_uom']					= 'UOM:';

$_['entry_bin']					= 'Bin:';

$_['entry_vendor']            	= 'Vendor Code ';

$_['entry_description']			= 'Description:';

$_['entry_shortdescription']	= 'Short Description:';

$_['entry_remarks']            	= 'Remarks:';

$_['entry_qoh']					= 'QOH:';

$_['entry_special']            	= 'Special Price:';

$_['entry_allowzeroprice']		= 'Allow Zero Price:';

$_['entry_nontaxitem']			= 'Non Tax Item:';

$_['entry_discontinued']		= 'Discontinued:';

$_['entry_price']            	= 'Retail Price:';

$_['entry_specialprice']		= 'Special Price:';

$_['entry_avgcost']            	= 'Avg Cost:';

$_['entry_discountcode']		= 'Coupon Code:';

$_['entry_unitcost']			= 'Unit Cost:';

$_['entry_profit']            	= 'Retail Profit %:';

$_['entry_barcode']            	= 'Barcode:';

$_['entry_status']       		= 'Active';

$_['tab_description']       	= 'Description';

$_['tab_data']       			= 'Price';

$_['tab_barcode']       		= 'Barcode';

$_['button_add_barcode']       	= 'Add Barcode';



// Error

$_['error_code']              	= 'Please enter valid code';

$_['error_department']        	= 'Please enter valid department';

$_['error_category']          	= 'Please enter valid category';

$_['error_brand']             	= 'Please enter valid brand';

$_['error_origin']            	= 'Please enter valid origin';

$_['error_uom']          	  	= 'Please enter valid uom';

$_['error_vendor']            	= 'Please enter valid vendor';

$_['error_price']          	  	= 'Please enter valid retail price';

$_['error_profit']            	= 'Please enter valid retail profit';



$_['text_length_day']        = 'Day';

$_['text_length_week']       = 'Week';

$_['text_length_month']      = 'Month';

$_['text_length_month_semi'] = 'Semi Month';

$_['text_length_year']       = 'Year';



// Error

$_['error_warning']          = 'Warning: Please check the form carefully for errors!';

$_['error_permission']       = 'Warning: You do not have permission to modify products!';

$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';

$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';



$_['text_department']		= 'Department';

$_['text_category']			= 'Category';

$_['text_brand']			= 'Brand';

$_['text_barcode']          = 'Barcode';

$_['text_vendor']           = 'Vendor';

$_['text_search']           = 'Search';

$_['entry_customer_code']     = 'Customer Code ';
$_['entry_customer_name']     = 'Customer Name ';

?>