<?php
// Heading
$_['heading_title']           = 'Bin';

// Text
$_['text_success']            = 'Success: You have modified Bin!';

// Column
$_['column_name']             = 'Bin Name';
$_['column_description']      = 'Bin Description';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Bin Name:';
$_['entry_description']       = 'Bin Description:';

// Error
$_['error_name']              = 'Bin Name must be between 3 and 128 characters!';
$_['error_description']       = 'Bin description must be between 3 and 128 characters!';
?>