<?php
// Heading
$_['heading_title']           = 'Cashier';

// Text
$_['text_success']            = 'Success: You have modified Cashier!';

// Column
$_['column_name']             = 'Cashier Name';
$_['column_email']             = 'Cashier Email';
$_['column_status']    		  = 'Status';
$_['column_group']            = 'Cashier Group';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Cashier Name:';
$_['entry_group']             = 'Cashier Group:';
$_['entry_password']       	  = 'Password:';
$_['entry_cpassword']         = 'Confirm Password:';
$_['entry_email']      		  = 'E-Mail:';
$_['entry_status']    		  = 'Status:';

// Error
$_['error_name']              = 'Cashier Name must be between 3 and 30 characters!';
$_['error_group']             = 'Please select any group!';
$_['error_email']             = 'Email address must be enter valid';
$_['error_password']       	  = 'Password is mismatch.!';
$_['error_permission'] 		  = 'Warning: You do not have permission to modify users!';
?>