<?php
// Heading
$_['heading_title']           = 'UOM Type Maintance';

// Text
$_['text_success']            = 'Success: You have modified UOM Type!';

// Column
$_['column_name']             = 'UOM Type Name';
$_['column_value']            = 'UOM Type Value';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'UOM Type Name';
$_['entry_value']              = 'UOM Type Value';

// Error
$_['error_name']              = 'UOM Type Name must be between 3 and 128 characters!';
$_['error_uom_id']            = 'Please select UOM!';

?>