<?php
// Heading
$_['heading_title']           = 'Cashier Group';

// Text
$_['text_success']            = 'Success: You have modified Cashier Group!';

// Column
$_['column_name']             = 'Cashier Group Name';
$_['column_permission']       = 'Permission';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Cashier Group Name:';
$_['entry_access']        	  = 'Access Permission:';
$_['entry_remarks']       	  = 'Remarks:';

// Error
$_['error_name']              = 'Cashier Group Name must be between 3 and 30 characters!';
$_['error_permission'] 		  = 'Warning: You do not have permission to modify users!';
?>