<?php
// Heading
$_['heading_title']           = 'UOM Maintance';

// Text
$_['text_success']            = 'Success: You have modified UOM!';

// Column
$_['column_name']             = 'UOM Name';
$_['column_code']             = 'UOM Code';
$_['column_description']      = 'UOM Remarks';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'UOM Name ';
$_['entry_code']              = 'UOM Code ';
$_['entry_description']       = 'UOM Remarks ';

// Error
$_['error_name']              = 'UOM Name must be between 3 and 128 characters!';
$_['error_name']              = 'UOM Code must be between above 2 characters!';
$_['error_description']       = 'UOM Remarks must be between 3 and 128 characters!';
$_['error_product']           = 'Warning: This UOM cannot be deleted as it is currently assigned to %s products!';


?>