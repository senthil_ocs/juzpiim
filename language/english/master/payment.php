<?php
// Heading
$_['heading_title']           = 'Payment Maintance';

// Text
$_['text_success']            = 'Success: You have modified payment!';

// Column
$_['column_name']             = 'Payment Name';
$_['column_status']           = 'Status';
$_['column_position']         = 'Position';
$_['column_id']            	  = 'ID';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Payment Name ';
$_['entry_description']       = 'Description ';
$_['entry_status']            = 'Status ';
$_['entry_position']          = 'Position ';


// Error
$_['error_name']              = 'Payment Name must be between 3 and 128 characters!';
$_['error_description']       = 'Payment description must be between 3 and 128 characters!';
?>