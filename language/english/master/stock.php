<?php
// Heading
$_['heading_title']           = 'Stock Adjustment Type Maintance';

// Text
$_['text_success']            = 'Success: You have modified Stock Adjustment Type!';

// Column
$_['column_name']             = 'Stock Adjustment Type Name';
$_['column_code']             = 'Stock Adjustment Type Code';
$_['column_method']     	  = 'Stock Method';
$_['column_position']     	  = 'Stock Position';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Stock Adjustment Type Name:';
$_['entry_code']              = 'Stock Adjustment Type Code:';
$_['entry_method']       	  = 'Stock Method:';
$_['entry_position']       	  = 'Stock Position:';

// Error
$_['error_name']              = 'Stock Adjustment Type Name must be between 3 and 128 characters!';
$_['error_name']              = 'Stock Adjustment Type Code must be between above 2 characters!';
$_['error_method']      	  = 'Stock Adjustment Type method must be select anyone!';
?>