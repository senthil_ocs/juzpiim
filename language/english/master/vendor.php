<?php
// Heading
$_['heading_title']           = 'Ventor Maintance';

// Text
$_['text_success']            = 'Success: You have modified Ventor!';

// Column
$_['column_name']             = 'Ventor Name';
$_['column_code']             = 'Ventor Code';
$_['column_address']      	  = 'Address';
$_['column_country']      	  = 'Country';
$_['column_zipcode']      	  = 'Zip Code';
$_['column_phone']      	  = 'Phone';
$_['column_email']      	  = 'Email';

$_['column_action']           = 'Action';

// Entry
$_['entry_name']            	= 'Ventor Name ';
$_['entry_code']            	= 'Ventor Code ';
$_['entry_address']       		= 'Address ';
$_['entry_country']       		= 'Country ';
$_['entry_zipcode']       		= 'Zip Code ';
$_['entry_phone']       		= 'Phone ';
$_['entry_email']       		= 'Email ';
$_['entry_fax']       			= 'Fax ';
$_['entry_attn']       			= 'Attn ';
$_['entry_remarks']       		= 'Remarks ';
$_['entry_gst']       			= 'GST';
$_['entry_status']       		= 'Active';

// Error
$_['error_name']              = 'Ventor Name must be between 3 and 128 characters!';
$_['error_code']              = 'Ventor Code must be between above 3 characters!';
$_['error_address']       	  = 'Ventor Address must be between 3 and 128 characters!';
$_['error_country']       	  = 'Ventor Country must be between 3 and 30 characters!';
?>