<?php
// Heading
$_['heading_title']           = 'Sales Man Maintance';

// Text
$_['text_success']            = 'Success: You have modified salesman!';

// Column
$_['column_name']             = 'Sales Man Name';
$_['column_id']            	  = 'ID';
$_['column_action']           = 'Action';

// Entry
$_['entry_name']              = 'Sales Man Name:';
$_['entry_password']		  = 'Password:';

// Error
$_['error_name']              = 'Sales Man Name must be between 3 and 128 characters!';
?>