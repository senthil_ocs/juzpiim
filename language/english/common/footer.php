<?php
// Text
$_['text_footer']  = '<a href="http://www.opencart.com">OpenCart</a> &copy; 2009-' . date('Y') . ' All Rights Reserved.<br />Version %s';
$_['text_confirm'] = 'Delete/Uninstall cannot be undone! Are you sure you want to do this?';
?>