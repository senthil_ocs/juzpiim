<?php
// header
$_['heading_title']  = 'Change Password';

// Text
$_['text_heading']   = 'Change Password';
$_['text_login']     = 'Please enter your login details.';

// Entry
$_['entry_old_password']	 	= 'Old Password:';
$_['entry_new_password'] 		= 'New Password:';
$_['entry_conform_password']	= 'Conform Password:';

// Button
$_['button_change']   = 'Submit';

// Error
$_['error_old_password']    = 'Please enter correct old password';
$_['error_token']    		= 'Invalid token session. Please login again.';
$_['error_password']    	= 'Please enter correct password';
?>
