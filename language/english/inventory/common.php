<?php
// Heading
$_['heading_title']                = 'Common Inventory ';

// Text
$_['text_master']                  = 'Master';
$_['text_main_category']           = 'Department';
$_['text_sub_category']            = 'Category';
$_['text_brand']              	   = 'Brand';
$_['text_uom_master']              = 'Uom Master';
$_['text_inventory']               = 'Inventory';
$_['text_new_nventory']            = 'New Inventory';
$_['text_modify_inventory']        = 'Modify Inventory';
$_['text_barcode_print']           = 'Barcode Add/ Print';
$_['text_inventory_search']        = 'Inventory Search';
$_['text_quick_edit']              = 'Quick Edit';
$_['text_inventory_movement']      = 'Inventory Movement';
$_['text_pricing']      		   = 'Pricing';
$_['text_discount']     		   = 'Discount';
$_['text_uom_type_master']         = 'Uom Type Master';


$_['alt_department']               = 'ALT+D';
$_['alt_category']                 = 'ALT+C';
$_['alt_brand']                    = 'ALT+R';
$_['alt_ucom']                     = 'ALT+U';

$_['alt_newinventory']             = 'ALT+N';
$_['alt_modifyinventory']          = 'ALT+M';
$_['alt_barcode']                  = 'ALT+P';
$_['alt_search']                   = 'ALT+A';
$_['alt_edit']                     = 'ALT+Q';
$_['alt_inv_moment']               = 'ALT+I';
$_['alt_ucomtype']                 = '';
$_['alt_discount']                 = 'ALT+O';

$_['department_id']                = 'department';
$_['category_id']                  = 'category';
$_['brand_id']                     = 'brand';
$_['ucom_id']                      = 'ucom';

$_['ninventory_id']                = 'ninventory';
$_['minventory_id']                = 'minventory';
$_['barcode_id']                   = 'barcode';
$_['search_id']                    = 'search';
$_['qedit_id']                     = 'qedit';
$_['invmoment_id']                 = 'invmoment';

$_['discount_id']                 = 'discount';
?>