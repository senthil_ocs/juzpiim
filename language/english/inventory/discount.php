<?php

$_['heading_title']				= 'Pair Discount';

$_['text_success']           = 'Success: You have modified Discount!';

$_['column_discount_type']		= 'Type';
$_['column_name']				= 'Name';
$_['column_amount']				= 'Amount';
$_['column_action']				= 'Action';

$_['entry_name']				= 'Discount Name ';
$_['entry_amount1']				= 'Percent % off ';
$_['entry_amount2']				= '$ Amount off ';
$_['entry_type']				= 'Type ';

$_['error_warning']				= "Check the error's ans submit the form";