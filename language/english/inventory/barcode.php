<?php
// Heading
$_['heading_title']             = 'Barcode';

// Error
$_['error_permission'] 		  	= 'Warning: You do not have permission to modify users!';
$_['error_account']    			= 'Warning: You can not delete as it is currently assigned as the store account terms!';

//barcode print
$_['entry_bar_code']			= 'Bar Code ';
$_['entry_inventory_code']		= 'Inventory Code ';
$_['entry_name']			    = 'Name ';
$_['entry_price']			    = 'Price ';
$_['entry_remarks']			    = 'Remarks ';
$_['entry_labels']			    = 'No Of Labels ';

$_['button_print']			    = 'Print';

$_['error_invalid']             = 'Please enter all valid details';
$_['text_no_results']           = 'No data Found!';


?>