<?php
// Heading
$_['heading_title']             = 'Category';
$_['subCate_title']             = 'Sub Category';

// Text
$_['text_success']				= 'Success: You have modified Category!';

// Column
$_['column_name']				= 'Category name';
$_['column_department']			= 'Department';
$_['column_remarks']			= 'Remarks';
$_['column_action']				= 'Action';
$_['column_code']				= 'Category Code';

// Entry
$_['entry_name']				= 'Category name';
$_['entry_department']			= 'Department';
$_['entry_remarks']				= 'Remarks';
$_['entry_status']				= 'Status';
$_['entry_code']				= 'Category Code';


// Error
$_['error_name']              	= 'Please enter valid name';
$_['error_department']          = 'Please select any one department';
$_['error_permission'] 		  	= 'Warning: You do not have permission to modify users!';
$_['error_product']             = 'Warning: This category cannot be deleted as it is currently assigned to %s products!';
$_['error_brand']            	= 'Warning: This category cannot be deleted as it is currently assigned to %s brands!';

?>