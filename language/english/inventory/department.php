<?php
// Heading
$_['heading_title']             = 'Department';

// Text
$_['text_success']				= 'Success: You have modified Department!';

// Column
$_['column_name']				= 'Department name';
$_['column_remarks']			= 'Remarks';
$_['column_salesman']			= 'Status';
$_['column_action']				= 'Action';
$_['column_code']				= 'Department Code';

// Entry
$_['entry_name']				= 'Department name';
$_['entry_remarks']				= 'Remarks';
$_['entry_salesman']			= 'Status';
$_['entry_code']				= 'Department Code';

// Error
$_['error_name']              	= 'Please enter valid name';
$_['error_code']              	= 'Please enter Department Code';
$_['error_permission'] 		  	= 'Warning: You do not have permission to modify users!';
$_['error_account']    			= 'Warning: You can not delete as it is currently assigned as the store account terms!';
$_['error_product']             = 'Warning: This department cannot be deleted as it is currently assigned to %s products!';
$_['error_category']            = 'Warning: This department cannot be deleted as it is currently assigned to %s category!';
?>