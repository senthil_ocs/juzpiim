<?php
// Heading
$_['heading_title']             = 'Brand';

// Text
$_['text_success']				= 'Success: You have modified Brand!';

// Column
$_['column_name']				= 'Brand name';
$_['column_code']				= 'Brand Code';

$_['column_department']			= 'Department';
$_['column_category']			= 'Category';
$_['column_remarks']			= 'Remarks';
$_['column_status']			= 'Status';
$_['column_action']				= 'Action';
// Entry
$_['entry_name']				= 'Brand name ';
$_['entry_code']				= 'Brand Code ';
$_['entry_department']			= 'Department ';
$_['entry_category']			= 'Category ';
$_['entry_remarks']				= 'Remarks ';
$_['entry_status']				= 'Status ';

// Error
$_['error_name']              	= 'Please enter valid name';
$_['error_code']              	= 'Please enter brand code';
$_['error_department']          = 'Please select any one department';
$_['error_category']            = 'Please select any one category';
$_['error_permission'] 		  	= 'Warning: You do not have permission to modify users!';
$_['error_product']             = 'Warning: This brand cannot be deleted as it is currently assigned to %s products!';


?>