<?php 
require('api.php');

$product_details = array(
				"primary_category" => "10000552", 
				"name" => "api create product144 test sample", 
				"short_description" => "This is a nice products", 
				"brand" => "ABBA", 
				"model" => "asdf", 
				"kid_years" => "Kids (6-10yrs)", 
				"delivery_option_sof" => "Yes", 
				"seller_sku" => "api-create-test-1", 
				"color_family" => "Green", 
				"size" => "40", 
				"quantity" => "1", 
				"price" => "388.50", 
				"quantity" => "1", 
				"package_length" => "14",
				"package_height" => "15",
				"package_weight" => "16",
				"package_width" => "17",
				"package_content" => "this is what's in the box",
				"image1" => "http://sg.s.alibaba.lzd.co/original/59046bec4d53e74f8ad38d19399205e6.jpg",
				"image2" => "http://sg.s.alibaba.lzd.co/original/179715d3de39a1918b19eec3279dd482.jpg",

				);
$c = new LazopClient(API_URL,APP_KEY,APP_SECRET); 
$request = new LazopRequest('/product/update');

$product_xml = "<?xml version='1.0' encoding='UTF-8'?>
			<Request>     
			 <Product>         
			  <PrimaryCategory>".$product_details["primary_category"]."</PrimaryCategory>         
			  <SPUId/>         
			  <AssociatedSku/>         
			  <Attributes>             
			   <name>".$product_details["name"]."</name>             
			   <short_description>".$product_details["short_description"]."</short_description>             
			   <brand>".$product_details["brand"]."</brand>             
			   <model>".$product_details["model"]."</model>             
			   <kid_years>".$product_details["kid_years"]."</kid_years>     
			   <delivery_option_sof>".$product_details["delivery_option_sof"]."</delivery_option_sof>		 
			   <!--should be set as ‘Yes’ only for products to be delivered by seller-->    
			  </Attributes>         
			  <Skus>             
			   <Sku>      
			    <SellerSku>".$product_details["seller_sku"]."</SellerSku>                 
			    <color_family>".$product_details["color_family"]."</color_family>                 
			    <size>".$product_details["size"]."</size>                 
			    <quantity>".$product_details["quantity"]."</quantity>                 
			    <price>".$product_details["price"]."</price>                 
			    <package_length>".$product_details["package_length"]."</package_length>                 
			    <package_height>".$product_details["package_height"]."</package_height>                 
			    <package_weight>".$product_details["package_weight"]."</package_weight>                 
			    <package_width>".$product_details["package_width"]."</package_width>                 
			    <package_content>".$product_details["package_content"]."</package_content>                 
			    <Images>                     
			     <Image>".$product_details["image1"]."</Image>                     
			     <Image>".$product_details["image2"]."</Image>                 
			    </Images>             
			   </Sku>         
			  </Skus>     
			 </Product> 
			</Request>";
    

$request->addApiParam('payload',$product_xml);
$result_product = $c->execute($request, ACCESS_TOKEN);

$result_products = json_decode($result_product);

printArray($result_products);

?>