<?php 
//ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
require 'api.php';
/* GET ORDERS */
$ordersList = array();
$orders = GetOrders();

if(count($orders->data->orders)>0){
	foreach ($orders->data->orders as $key => $order) {
		$OrderItems = GetOrderItems($order->order_id);
		$ordersList[$key]['order_number'] = $order->order_number;
		$ordersList[$key]['price'] = $order->price;
		$ordersList[$key]['customer_first_name'] = $order->customer_first_name;
		$ordersList[$key]['customer_last_name'] = $order->customer_last_name;	
		$ordersList[$key]['statuses'] = $order->statuses;
		$ordersList[$key]['updated_at'] = $order->updated_at;
		$ordersList[$key]['created_at'] = $order->created_at;
		$ordersList[$key]['payment_method'] = $order->payment_method;
		$ordersList[$key]['address_billing'] = $order->address_billing;
		$ordersList[$key]['order_id'] = $order->order_id;
		$ordersList[$key]['remarks'] = $order->remarks;
		$ordersList[$key]['address_shipping'] = $order->address_shipping;
		$ordersList[$key]['items_count'] = $order->items_count;
		$ordersList[$key]['delivery_info'] = $order->delivery_info;
		$ordersList[$key]['tax_code'] = $order->tax_code;
		$ordersList[$key]['shipping_fee'] = $order->shipping_fee;
		$ordersList[$key]['order_items'] = (array)$OrderItems->data;
		
	}

}
//echo json_decode($ordersList);
printArray($ordersList); exit;

//include('templete/orders.php');
?>

<div>
		<h2>Get Orders Api</h2>
	</div>