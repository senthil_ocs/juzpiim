<?php
	include(DIR_SERVER.'/MPDF/mpdf.php');
	
	ini_set('display_errors', 1);
	ini_set('log_errors', 1);
	error_reporting(E_ALL);
	ob_end_clean();

	$mpdf = new mPDF('c','A4');
	$mpdf->mirrorMargins = 1;
	$mpdf->SetMargins(0, 0, 5);

	$mpdf->SetDisplayMode('fullpage');
	$mpdf->setFooter('{PAGENO}');
	$mpdf->SetFont('DejaVuSans');

	$mpdf->WriteHTML('Hello World 中文');
	$mpdf->Output('asdasd.pdf', 'D');
?>