<?php
	include(DIR_SERVER.'/MPDF/mpdf.php');
	ob_end_clean();
	$config = [
	    'mode' => '+aCJK', 
	    "allowCJKoverflow" => true, 
	    "autoScriptToLang" => true,
	    "allow_charset_conversion" => true,
	    "autoLangToFont" => true,
	    "charset_in" => 'utf-8'
	]; // not used anywhere

	if($landscape=='1'){
		$mpdf=new mPDF('c','A4-L');
	}else{
		$mpdf = new mPDF('utf-8');
		// $mpdf=new mPDF($config);
	}
	$mpdf->autoLangToFont  = true;
	$mpdf->mirrorMargins = 1;
	$mpdf->SetMargins(0, 0, 5);

	$query = $this->db->query("SELECT DISTINCT logo FROM " . DB_PREFIX . "company");
	$logo = $query->row['logo'];
	if(PDF_WM_LOGO=='1'){
		// $mpdf->SetWatermarkImage(DIR_SERVER.'image/'.$logo);
		$mpdf->showWatermarkImage = true;
		$mpdf->watermarkImageAlpha = 0.1;
	}

	$mpdf->SetDisplayMode('fullpage');
	$mpdf->setFooter($footer);

	// $mpdf->SetFont('DejaVuSans');
	$stylesheet = file_get_contents(HTTP_SERVER.'view/stylesheet/pdf.css');
	$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
	$mpdf->WriteHTML($str);
 
	if(isset($type) && $type=='F') {
		return $mpdf->Output($filename, 'F');
	}else{
		$mpdf->Output($filename, 'D');
		exit;
	}
	?>