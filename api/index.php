<?php 
	require "../config.php";
	include_once(DIR_SERVER."api/includes/class.apiCommon.php");
	include_once(DIR_SERVER."api/includes/class.SqlFunctions.php");
	include_once(DIR_SERVER."api/includes/class.api.php");
	$api = new api();

	if(isset($_REQUEST['op'])){
		$op = $_REQUEST['op'];
		switch ($op) {
		    case "get_access":
		          if(isset($_REQUEST['group_id'])){
		          	$access = $api->getGroupAccess($_REQUEST['group_id']);
		          	$res = array("status"=>"success","details"=>$access); 
		          }else{
		        	$res = array("status"=>"error","message"=>"group_id should not empty");  	
		          } 	  	
		          break;
		    case "get_license":
				  $licenseFile = DIR_SERVER .'/license.txt';
				  if(file_exists($licenseFile)){
				  	$str = file_get_contents($licenseFile);
				  	$resultAry = array();
				  	$strAry = explode("|", $api->my_simple_crypt($str,'d'));
				  	if($strAry[0]){ $resultAry['mac_address'] = $strAry[0];}
				  	if($strAry[1]){ $resultAry['from_date'] = $strAry[1];}
				  	if($strAry[2]){ $resultAry['to_date'] = $strAry[2];}
				  	if($strAry[3]){ $resultAry['location'] = $strAry[3];}
				  	if($strAry[4]){ $resultAry['location'] = $strAry[4];}
				  	if($strAry[5]){ $resultAry['terminal_count'] = $strAry[5];}
				  	$res = array("status"=>"success","details"=>$resultAry); 

				  }else{
					$res = array("status"=>"error","message"=>"license file not available");  	
				  }		  	
		          break;		    
		    default:
		        $res = array("status"=>"error","message"=>"given method not available");
		}

	}else{
		$res = array("status"=>"error","message"=>"method should not empty");
	}

	$api->showJsonResults($res);
?>