<?php
class api extends MysqlFns
{
  function api()
  {
     $this->MysqlFns();
  }
  function printArray($str){
    print "<pre>";
      print_r($str);
    print "</pre>";
  }
  function Remove_whitespace($str)
  {
      if(isset($str)) {
          $RequestData = urldecode(trim($str));
      } else {
          $RequestData ="";
      }
      return $RequestData;
  }
  function dbInput($string)
  {
      $string = $this->Remove_whitespace($string);
      if (function_exists('mysql_real_escape_string')){
          return mysql_real_escape_string($string);
      }else if(function_exists('mysql_escape_string')) {
          return mysql_escape_string($string);
      }
      return addslashes($string);
  }
  function showJsonResults($arr)
  {
    echo $this->array2json($arr);
  }     
  function array2json($arr) {
    $parts = array();
    $is_list = false;   
    //Find out if the given array is a numerical array
    $keys = array_keys($arr);
    $max_length = count($arr)-1;
    if($arr) {
      if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {
        //See if the first key is 0 and last key is length - 1
        $is_list = true;
        for($i=0; $i<count($keys); $i++) {
          //See if each key correspondes to its position                  
          if($i != $keys[$i]) { //A key fails at position check.
            $is_list = false; //It is an associative array.
            break;
           }
         }
      }
    }   
    foreach($arr as $key=>$value) {
      if(is_array($value)) {
        //Custom handling for arrays
        if($is_list) $parts[] = $this->array2json($value); /* :RECURSION: */
        else $parts[] = '"' . $key . '":' . $this->array2json($value); /* :RECURSION: */
      } else {
        $str = '';
        if(!$is_list) $str = '"' . $key . '":';
               //Custom handling for multiple data types
        if(is_numeric($value)) $str .= '"'.$value.'"'; //Numbers
        elseif($value === false) $str .= 'false'; //The booleans
        elseif($value === true) $str .= 'true';
        else $str .= '"' . $value . '"'; ///All other things
        // :TODO: Is there any more datatype we should be in the lookout for? (Object?)
        $parts[] = $str;
       }
    }
    $json = implode(',',$parts);    
    if($is_list) return  '[' . $json . ']';//Return associative JSON
    return  '{' . $json . '}';//Return associative JSON
  } 
  function getLocationsFromHQ(){
    $sql = "SELECT * FROM " .DB_PREFIX . "location ORDER BY location_id DESC";
    $res = $this->SelectQryMSSQL($sql);
    return $res;
  }
  function getGroupAccess($group_id){
    
     $sql="select id,name,link,description from tbl_access where access_type='pos_access'";
     $accessres = $this->SelectQryMSSQL($sql);
    
     $sql="SELECT access_reports_pos FROM " .DB_PREFIX . "user_group WHERE user_group_id='".$group_id."'";
     $result = $this->SelectQryMSSQL($sql);
     $posarr = explode (',', $result[0]['access_reports_pos']);
      
     $count =count($accessres);
     for($i=0; $i<$count; $i++){
       if(in_array($accessres[$i]['id'],$posarr)){
          $allres[$i]['id'] = $accessres[$i]['id'];
          $allres[$i]['name'] = $accessres[$i]['name'];
          $allres[$i]['link'] = $accessres[$i]['link'];
          $allres[$i]['description'] = $accessres[$i]['description'];
          $allres[$i]['status'] ='Active';
       }else{
          $allres[$i]['id'] = $accessres[$i]['id'];
          $allres[$i]['name'] = $accessres[$i]['name'];
          $allres[$i]['link'] = $accessres[$i]['link'];
          $allres[$i]['description'] = $accessres[$i]['description'];
          $allres[$i]['status'] ='InActive';
       }
       
       $newarr[]=$allres[$i];
     }
    return $newarr; 
  }
  function my_simple_crypt($string, $action = 'e' ) {
    // you may change these values to your own

    $secret_key = 'juzpos';
    $secret_iv  = 'ABCD1234567GHJK!@#%^';
 
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
 
    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
    
    return $output;
  }  
}
?>