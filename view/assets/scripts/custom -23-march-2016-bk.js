/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.            
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

}();

$(document).ready(function(){
       
    $(".readall").click(function(){
        var ids = $(this).attr("id");
        ids = ids.replace("-read",'');
        if($(this).is(":checked")){
          $("#"+ids).find(".read").attr("checked",true);
          $("#"+ids).find(".read").parent('span').addClass("checked");

        } else{
          $("#"+ids).find(".read").attr("checked",false);
          $("#"+ids).find(".read").parent('span').removeClass("checked");
        }
    });

    $(".modifyall").click(function(){
        var ids = $(this).attr("id");
        ids = ids.replace("-modify",'');
        if($(this).is(":checked")){
          $("#"+ids).find(".modify").attr("checked",true);
          $("#"+ids).find(".modify").parent('span').addClass("checked");

        } else{
          $("#"+ids).find(".modify").attr("checked",false);
          $("#"+ids).find(".modify").parent('span').removeClass("checked");
        }
    });

    $(".read").click(function(){
         var classs = $(this).attr("class");
         classs = $.trim(classs.replace("read",""));
         var total = $("#"+classs).find("input.read").length;
         var checkedlen = $("#"+classs).find("input.read:checked").length;
         if(total==checkedlen){
            $("#"+classs+"-read").attr("checked",true);
            $("#"+classs+"-read").parent('span').addClass("checked");
         } else{
            $("#"+classs+"-read").attr("checked",false);
            $("#"+classs+"-read").parent('span').removeClass("checked");
         }
    });
     $(".modify").click(function(){
         var classs = $(this).attr("class");
         classs = $.trim(classs.replace("modify",""));
         var total = $("#"+classs).find("input.modify").length;
         var checkedlen = $("#"+classs).find("input.modify:checked").length;
         if(total==checkedlen){
            $("#"+classs+"-modify").attr("checked",true);
            $("#"+classs+"-modify").parent('span').addClass("checked");
         } else{
            $("#"+classs+"-modify").attr("checked",false);
            $("#"+classs+"-modify").parent('span').removeClass("checked");
         }
    });

    $("thead input[type=checkbox]").click(function(){
         if($(this).is(":checked")){
             $('input[name*=\'selected[]\']').attr('checked', true);
             $('input[name*=\'selected[]\']').parent().addClass("checked");
         } else{
            $('input[name*=\'selected[]\']').attr('checked', false);
            $('input[name*=\'selected[]\']').parent().removeClass("checked");
         }
    });

       /*if($(".posaccess").is(":checked")){
            $("#posaccessrow").slideDown();
        } else{
            $("#posaccessrow").slideUp();
        }

        if($(".adminaccess").is(":checked")){
            $("#adminaccessrow").slideDown();
        } else{
            $("#adminaccessrow").slideUp();
        }*/

    $(".posaccess").click(function(){
        if($(this).is(":checked")){
            $("#posaccessrow").find('input[type=checkbox]').attr("checked",true);
            $("#posaccessrow").find('input[type=checkbox]').parent().addClass("checked");
        } else{
            $("#posaccessrow").find('input[type=checkbox]').attr("checked",false);
              $("#posaccessrow").find('input[type=checkbox]').parent().removeClass("checked");
        }

    });

    $(".adminaccess").click(function(){
        if($(this).is(":checked")){
            $("#adminaccessrow").find('input[type=checkbox]').attr("checked",true);
            $("#adminaccessrow").find('input[type=checkbox]').parent().addClass("checked");
        } else{
            $("#adminaccessrow").find('input[type=checkbox]').attr("checked",false);
            $("#adminaccessrow").find('input[type=checkbox]').parent().removeClass("checked");        
        }

    });

    $('.date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        yearRange: '1990:2016',
    });

    /// Model Loading
  $('#discount_type').change(function() {
    var id = this.value;
    //alert(id);
    if(id == 1){
       $('#fixed').addClass('dispalynone');
    } else {
      $('#fixed').removeClass('dispalynone');
    }
    if(id == 2){
       $('#percent').addClass('dispalynone');
       $('#fixed').addClass('fixed');      
    } else {
      $('#percent').removeClass('dispalynone');
      $('#fixed').removeClass('fixed');
    }
  });

  $('#ajax-modal1').delegate("#continue_order","click", function(){
     $("#clearallpos").trigger('click');
     $("#ajax-modal1").find('.modal-body').html(''); 
     $('#ajax-modal1').modal('hide'); 
  });

   $('#ajax-modal-disc').delegate("#continue_order_disc","click", function(){
     $("#ajax-modal-disc").find('.modal-body').html(''); 
     addproducts('');
     $('#ajax-modal-disc').modal('hide'); 
  });

  $('#ajax-modal1').delegate(".paymethod", "click", function() {

      var payment_mode = []; 
      var payment_mode_name = [];
       

       var name = $(this).find('input').attr("id");

       if($(this).hasClass('active')){
           $(this).find('input').attr("checked",false);
           if(name.toLowerCase() == 'cash'){
              $("#show_cash_mode").addClass('hidden');
           } else if(name.toLowerCase() == 'visa') {
             $("#show_visa_mode").addClass('hidden');
              $("#show_slip_mode").addClass('hidden');
           } else if(name.toLowerCase() =='cash card'){
              $("#show_card_mode").addClass('hidden');
              $("#show_slip_mode").addClass('hidden');
           }

       } else{
          $(this).find('input').attr("checked",true);
          if(name.toLowerCase() == 'cash'){
              $("#show_cash_mode").removeClass('hidden');

          } else if(name.toLowerCase() == 'visa') {
             $("#show_visa_mode").removeClass('hidden');
              $("#show_slip_mode").removeClass('hidden');
          } else if(name.toLowerCase() =='cash card'){
              $("#show_card_mode").removeClass('hidden');
              $("#show_slip_mode").removeClass('hidden');
           }
       }
       $('.paymethod > input:checked').each(function(i, selected){ 
          payment_mode[i] = $(selected).val();
       });
       if(payment_mode.length>1){
          $("#visa_amount").attr('disabled',false);
          $("#card_amount").attr('disabled',false);
       } else{
          $("#visa_amount").attr('disabled',true);
          $("#card_amount").attr('disabled',true);

       }


  });

$("#holdform").ajaxForm({ //login form
        success: function(output){
            console.log(output);
            output = JSON.parse(output);
            if(output['error']){
               $("#holdmessage").html('<p>'+output['error']+'</p>');
               $("#holdmessage").removeClass('alert-success;');
               $("#holdmessage").addClass('alert-danger').show();
            } else{
              var stringmss =  'The current product entry is hold.The hold bill number is '+output['hold_bill_no'];
              $("#holdmessage").html('<p>'+stringmss+'</p>');
              $("#holdmessage").removeClass('alert-danger');
              $("#holdmessage").addClass('alert-success').show();
              $("#clearallpos").trigger('click');
              $("#holdorder").hide();
              $("#form-body").hide();

            }
            
        }
     });

$("#releaseform").ajaxForm({ //login form
        success: function(output){
            console.log(output);
            output = JSON.parse(output);
            if(output['error']){               
               $("#releasemessage").removeClass('alert-success;');
               $("#releasemessage").html('<p>'+output['error']+'</p>');
               $("#releasemessage").addClass('alert-danger').show();
            } else{
              addproducts('');
              $("#ajax-modal-release").modal('hide');
            }
            
        }
     }); 

   $("#reprintform").ajaxForm({
       success:function(output){
         $("#ajax-modal-reprintinvoice").find(".modal-body").html(output);
         $("#ajax-modal-print").modal('hide');
         $("#ajax-modal-reprintinvoice").modal('show'); 
       }
   });  
   
   /*void Form and Model Process*/
     $('.modal').on('hide.bs.modal', function (e) {
         $(this).find('.alert').html('');
         $(this).find('.alert').html('');
         $(this).find('.alert').removeClass('alert-danger');
         $(this).find('.alert').removeClass('alert-succes');
         $(this).find('.alert').hide();
         $(this).find('button').show();
    }); 
    $("#voidbill").click(function(){
         $('body').modalmanager('loading');
         $("#ajax-modal-void").modal();
    }); 
    $("#voidform").ajaxForm({
       success:function(output){
             output = JSON.parse(output);
             if(output['is_verify']){
               if(output['success']){
                   $("#ajax-modal-void").modal('hide');
                   $("#ajax-modal-voidconfirm").modal('show');
                   $("#void_billno").val(output['order_id']);
                   var st ='<h4>Are You Surely want to cancel the order #'+output['order_id']+'</h4>';
                   $("#ajax-modal-voidconfirm").find('.alert').removeClass('alert-danger').removeClass('alert-success').html(st).show();
                   $("#ajax-modal-voidconfirm").find("#voidinvoiceconfirm").show();
                   $("#ajax-modal-voidconfirm").find("#voidinvoiceconfirm").next().text('No');
                   
                } else{
                  var st =output['message'];
                  $("#ajax-modal-void").find('.alert').html(st);
                  $("#ajax-modal-void").find('.alert').addClass('alert-danger').show();
                }

             } else{

             }
       }
   });
    $("#voidformc").ajaxForm({
       success:function(output){
             output = JSON.parse(output);
             if(output['is_verify']=='0'){
               if(output['success']){
                   $("#void_billno").val('');
                   var st ='<p>'+output['message'] +'</p>';
                   $("#ajax-modal-voidconfirm").find('.alert').html(st).removeClass('alert-danger').addClass('alert-success').show();
                   $("#ajax-modal-voidconfirm").find("#voidinvoiceconfirm").hide();
                   $("#ajax-modal-voidconfirm").find("#voidinvoiceconfirm").next().text('OK');

                } else{
                  var st =output['message'];
                  $("#ajax-modal-void").find('.alert').html(st);
                  $("#ajax-modal-void").find('.alert').addClass('alert-danger').show();
                }

             } else{

             }
       }
   });
    
   $("#printin").click(function(){
        var ht = $("#print-body").html();
        var mywindow = window.open('', 'Invoice', 'height=1000,width=1000');
        mywindow.document.write('<html><head><title>Invoice</title>');
        mywindow.document.write('</head><body>');
        mywindow.document.write(ht);
        mywindow.document.write('</body></html>');
        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
        mywindow.close();
        return true;
   }); 

  $( "#addProducts" ).delegate( "#backssale", "click", function() {
      $("#addProducts").hide();
      $("#posterminal").show();
   });

  $("#pospayment").click(function(){
      if($("#Makepayment").is(":visible")){
        $("#Makepayment").effect('slide', { direction: 'right', mode: 'hide' }, 500);
        $(".addingposbuttons").show();
        $(".finishbuttons").hide();
      } else{
        $("#Makepayment").effect('slide', { direction: 'right', mode: 'show' }, 500);
        $(".addingposbuttons").hide();
        $(".finishbuttons").show();
      }
  });
  $("#backToSaleButton").click(function(){
    $("#Makepayment").effect('slide', { direction: 'right', mode: 'hide' }, 500);
    $(".addingposbuttons").show();
    $(".finishbuttons").hide();
  });

   $("#bill-payment-form").ajaxForm({ //login form
            success: function(output){
                console.log(output);
                output = JSON.parse(output);
                if(output['error']){
                    alert(output['error']);
                } else{
                    $("#errormessage").addClass('hidden');

                }
                if(output['success']){
                    var html ='<div class="alert alert-success">The order has been received</div>';
                    html =html+'<h4>The order invoice is : # <span>'+output['htmldata']['last_order_id']+'</span></h4>';
                    html =html+'<h4>The return changes to customer is : # <span>'+output['htmldata']['return_amount']+'</h4>';
                    html =html+'<button type="button" name="submit" id="continue_order" class="btn btn-primary">Continue Order</button>';

                   $("#ajax-modal1").find('.modal-body').html(html);
                   $("#ajax-modal1").find('.modal-footer').hide(); 
                }
            }
         });

        $("#finishSaleButton").click(function(){
           $("#bill-payment-form").submit();
        });

       //// Need to Modify 

      $(".payment-max-btn").click(function(){
         var total = $("#totalamthidden").val();
         $(this).parent().find('input').val(total);
      });

      $(".td-btn").click(function() {
        var id = $(this).attr('value'); 
        var cashValue= $('#paid_amount').val();
        if(cashValue=="") cashValue=0;;
        var Result =  parseFloat(cashValue)+parseFloat(id);
        $('#paid_amount').val(Result);
        $('#payments_total').val(Result);
      });


});

/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();