$(document).ready(function(){
	 Mousetrap.bind('f1', function(e) {
        e.preventDefault();
        $("#reprint").trigger('click');
        return false;
    });
   // Search Products
	Mousetrap.bind('ins', function(e) {
        e.preventDefault();
        $("#searchproducts_pos").trigger('click');
        return false;
    });
    //Discount 
    Mousetrap.bind('f2', function(e) {
        e.preventDefault();
        $("#disc").trigger('click');
        return false;
    });
    //Void Bill 
    Mousetrap.bind('f3', function(e) {
        e.preventDefault();
        $("#voidbill").trigger('click');
        return false;
    });

    //Tender
    Mousetrap.bind('f4', function(e) {
        e.preventDefault();
        $("#tender").trigger('click');
        return false;
    });

     //Clear POS
    Mousetrap.bind('f5', function(e) {
        e.preventDefault();
        $("#clearpos").trigger('click');
        return false;
    });
    // Release
    Mousetrap.bind('f6', function(e) {
        e.preventDefault();
        $("#release").trigger('click');
        return false;
    });

    // hold
    Mousetrap.bind('f7', function(e) {
        e.preventDefault();
        $("#hold").trigger('click');
        return false;
    });

    // exchange
    Mousetrap.bind('f8', function(e) {
        e.preventDefault();
        $("#exitem").trigger('click');
        return false;
    });

    // Price Changes utility
    Mousetrap.bind('f9', function(e) {
        e.preventDefault();
        $("#change").trigger('click');
        return false;
    });

       // Price Changes utility
    Mousetrap.bind('alt+r', function(e) {
        e.preventDefault();
        $("#exitem").trigger('click');
        if($("#exitem").hasClass("btn-primary")){
             $("#refundprocess").slideDown();
          } else{
            $("#refundprocess").slideUp();
          }     
        return false;
    });

    Mousetrap.bind('alt+s', function(e) {
        e.preventDefault();
        $("#returnsale").trigger('click');
        if($("#returnsale").hasClass("btn-primary")){
             $("#refundprocess").slideDown();
          } else{
            $("#refundprocess").slideUp();
          }     
        return false;
    });

    Mousetrap.bind('alt+a', function(e) {
        e.preventDefault();
        $("#searchcustomer").trigger('click');
        return false;
    });

    Mousetrap.bind('alt+b', function(e) {
        e.preventDefault();
        $("#pospayment").trigger('click');
        return false;
    });

    Mousetrap.bind('alt+i', function(e) {
        e.preventDefault();
        var href = $('#new_product').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+c', function(e) {
        e.preventDefault();
        var href = $('#new_customer').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+d', function(e) {
        e.preventDefault();
        $("#disc").trigger('click');
        return false;
    });

    Mousetrap.bind('enter', function(e) {        
        if($('.positembox').is(':focus')){
            e.preventDefault();
            $("#searchproducts_pos").trigger('click');
         } else if($('.barcode_search').is(':focus')){
            e.preventDefault();
            $("#barcode").trigger('click');
         } else if($('#cust_search').is(':focus')){
            e.preventDefault();
            $('#searchcustomer').trigger('click');
         }
        //return false;
    });


    $('.positembox').live("keypress", function(e) {
        if (e.keyCode == 13) {
            $("#searchproducts_pos").trigger('click');
            return false; // prevent the button click from happening
        }
    });
    $('.barcode_search').live("keypress", function(e) {
        if (e.keyCode == 13) {
            $("#barcode").trigger('click');
            return false; // prevent the button click from happening
        }
    });
     $('#cust_search').live("keypress", function(e) {
        if (e.keyCode == 13) {
            $('#searchcustomer').trigger('click');
            return false; // prevent the button click from happening
        }
    });

    $(".barcode_search").change(function(){
        $("#barcode").trigger('click');
        $(".barcode_search").val();
    });
    /*Mousetrap.bind('alt+s', function(e) {
        e.preventDefault();
        $("#exitem").trigger('click');
        return false;
    });*/

});