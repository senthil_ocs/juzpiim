
function purchaseChart(data){
    am4core.useTheme(am4themes_animated);
    var chart = am4core.createFromConfig({
      // Reduce saturation of colors to make them appear as toned down
      "colors": {
        "saturation": 0.9
      },

      // Setting data
      "data": data,

      // Add Y axis
      "yAxes": [{
        "type": "ValueAxis",
        "renderer": {
          "maxLabelPosition": 0.98
        }
      }],

      // Add X axis
      "xAxes": [{
        "type": "CategoryAxis",
        "renderer": {
          "minGridDistance": 20,
          "grid": {
            "location": 0
          }
        },
        "dataFields": {
          "category": "country"
        }
      }],

      // Add series
      "series": [{
        // Set type
        "type": "ColumnSeries",

        // Define data fields
        "dataFields": {
          "categoryX": "country",
          "valueY": "visits"
        },

        // Modify default state
        "defaultState": {
          "transitionDuration": 1000
        },

        // Set animation options
        "sequencedInterpolation": true,
        "sequencedInterpolationDelay": 100,

        // Modify color appearance
        "columns": {
          // Disable outline
          "strokeOpacity": 0.5,

          // Add adapter to apply different colors for each column
          "adapter": {
            "fill": function (fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            }
          }
        }
      }],

      // Enable chart cursor
      "cursor": {
        "type": "XYCursor",
        "behavior": "zoomX"
      }
    }, "purchase", "XYChart");
}

function purchaseReturnChart(data){

    am4core.useTheme(am4themes_animated);
    var chart = am4core.createFromConfig({
      // Reduce saturation of colors to make them appear as toned down
      "colors": {
        "saturation": 0.9
      },

      // Setting data
      "data": data,

      // Add Y axis
      "yAxes": [{
        "type": "ValueAxis",
        "renderer": {
          "maxLabelPosition": 0.98
        }
      }],

      // Add X axis
      "xAxes": [{
        "type": "CategoryAxis",
        "renderer": {
          "minGridDistance": 20,
          "grid": {
            "location": 0
          }
        },
        "dataFields": {
          "category": "country"
        }
      }],

      // Add series
      "series": [{
        // Set type
        "type": "ColumnSeries",

        // Define data fields
        "dataFields": {
          "categoryX": "country",
          "valueY": "visits"
        },

        // Modify default state
        "defaultState": {
          "transitionDuration": 1000
        },

        // Set animation options
        "sequencedInterpolation": true,
        "sequencedInterpolationDelay": 100,

        // Modify color appearance
        "columns": {
          // Disable outline
          "strokeOpacity": 0.5,

          // Add adapter to apply different colors for each column
          "adapter": {
            "fill": function (fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            }
          }
        }
      }],

      // Enable chart cursor
      "cursor": {
        "type": "XYCursor",
        "behavior": "zoomX"
      }
    }, "purchaseReturn", "XYChart");
}

function salesChart(data){

    am4core.useTheme(am4themes_animated);
    var chart = am4core.createFromConfig({
      // Reduce saturation of colors to make them appear as toned down
      "colors": {
        "saturation": 0.9
      },

      // Setting data
      "data": data,

      // Add Y axis
      "yAxes": [{
        "type": "ValueAxis",
        "renderer": {
          "maxLabelPosition": 0.98
        }
      }],

      // Add X axis
      "xAxes": [{
        "type": "CategoryAxis",
        "renderer": {
          "minGridDistance": 20,
          "grid": {
            "location": 0
          }
        },
        "dataFields": {
          "category": "country"
        }
      }],

      // Add series
      "series": [{
        // Set type
        "type": "ColumnSeries",

        // Define data fields
        "dataFields": {
          "categoryX": "country",
          "valueY": "visits"
        },

        // Modify default state
        "defaultState": {
          "transitionDuration": 1000
        },

        // Set animation options
        "sequencedInterpolation": true,
        "sequencedInterpolationDelay": 100,

        // Modify color appearance
        "columns": {
          // Disable outline
          "strokeOpacity": 0.5,

          // Add adapter to apply different colors for each column
          "adapter": {
            "fill": function (fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            }
          }
        }
      }],

      // Enable chart cursor
      "cursor": {
        "type": "XYCursor",
        "behavior": "zoomX"
      }
    }, "sales", "XYChart");
}

function salesReturnChart(data){

    am4core.useTheme(am4themes_animated);
    var chart = am4core.createFromConfig({
      // Reduce saturation of colors to make them appear as toned down
      "colors": {
        "saturation": 0.9
      },

      // Setting data
      "data": data,

      // Add Y axis
      "yAxes": [{
        "type": "ValueAxis",
        "renderer": {
          "maxLabelPosition": 0.98
        }
      }],

      // Add X axis
      "xAxes": [{
        "type": "CategoryAxis",
        "renderer": {
          "minGridDistance": 20,
          "grid": {
            "location": 0
          }
        },
        "dataFields": {
          "category": "country"
        }
      }],

      // Add series
      "series": [{
        // Set type
        "type": "ColumnSeries",

        // Define data fields
        "dataFields": {
          "categoryX": "country",
          "valueY": "visits"
        },

        // Modify default state
        "defaultState": {
          "transitionDuration": 1000
        },

        // Set animation options
        "sequencedInterpolation": true,
        "sequencedInterpolationDelay": 100,

        // Modify color appearance
        "columns": {
          // Disable outline
          "strokeOpacity": 0.5,

          // Add adapter to apply different colors for each column
          "adapter": {
            "fill": function (fill, target) {
              return chart.colors.getIndex(target.dataItem.index);
            }
          }
        }
      }],

      // Enable chart cursor
      "cursor": {
        "type": "XYCursor",
        "behavior": "zoomX"
      }
    }, "salesReturn", "XYChart");
}