<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Customer Pricing List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  } ?>
				</ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
            <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a> &nbsp; &nbsp;
            <?php } ?>
            </div>    
          </div>
        </div>			
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $filter_date_from; ?>" class="textbox date" autocomplete="off">
		                        </td>                       
		                        <td>
		                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="textbox date" autocomplete="off">
		                        </td> 
		                        <td>
		                           <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
		                            <?php if(count($Tolocations)>=2){?>
		                            <?php }?>
		                            <?php
		                               if(!empty($Tolocations)){
		                                    foreach($Tolocations as $value){ ?>
		                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

		                            <?php  } } ?> 
		                        </select>
		                        </td>
		                         <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
		                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
		                            <div id="suggesstion-box" class="auto-compltee"></div>
		                        </td>      
		                        <td align="center" colspan="4">
		                        	<input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
                  </div>
                </div>
                <div style="clear:both; margin:0 0 15px 0;"></div>
              </form>
            </div>
          </div>
        <div style="clear:both; margin:0 0 15px 0;"></div>
			<div style="clear:both"></div>
			
			<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-warning fade in setting-warning"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
      </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
        </div>
			<?php } ?>	

		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="POST" enctype="multipart/form-data" id="form">
          <div class="innerpage-listcontent-blocks"> 
            <table class="table orderlist statusstock">
              <thead>
                <tr class="heading">
                  <td class="left"><?php echo 'Customer Name'; ?></td>
                  <td class="left"><?php echo 'Location'; ?></td>
                  <td class="left"><?php echo 'From Date'; ?></td>
                  <td class="left"><?php echo 'To Date'; ?></td>
                  <td class="right"><?php echo 'Action'; ?></td>
                </tr>
              </thead>
              <tbody>
            <?php if ($terminals) { $class = 'odd'; ?>
  					<?php foreach ($terminals as $value) { ?>
  					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
  							 <tr class="<?php echo $class; ?>">
  									<td class="left"><?php echo $value['name']; ?></td>
                    <td class="left"><?php echo $value['location']; ?></td>
                    <td class="left"><?php echo date('d/m/Y',strtotime($value['FromDate'])); ?></td>
                    <td class="left"><?php echo date('d/m/Y',strtotime($value['ToDate'])); ?></td>
  									<td class="right">
  										<?php foreach ($value['action'] as $action) { ?>
  											<!-- [ <a href="<?php echo $action['href']; ?>"><?php echo 'Views'; ?></a> ] -->
  											[ <a href="<?php echo $action['edit']; ?>"><?php echo 'Edit'; ?></a> ]
  											[ <a href="<?php echo $action['delete']; ?>" onclick="return confirm('Are you sure, want to delete this?');" ><?php echo 'Delete'; ?></a> ]
  											<!-- [ <a class="promodelete" data-href="<?php echo $action['delete']; ?>"><?php echo 'Delete'; ?></a> ] -->
  										<?php } ?>
  									</td>
  								</tr>
  							<?php } ?>
    						<?php } else { ?>
    						<tr>
    							<td align="center" colspan="10"><?php echo $text_no_results; ?></td>
    						</tr>
    						<?php } ?>
                </tbody>
              </table>
            </div>
          </form>
          <div class="pagination"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport() {
	document.report_filter.submit();
}

  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
          $("#suggesstion-box").show();
          $("#suggesstion-box").html(str);
        }else{
          $("#suggesstion-box").hide();
          $("#suggesstion-box").html('');
        }
      }
    });
  }
});
  
function getProductautoFill(sku) {
  var filter_name = $("#filter_name").val();
   if(filter_name ==''){
      $('#filter_sku').val('');
   }
  if(sku.length<=3){
    return false;
  }

    var location = $("#filter_location").val();
    var ajaxData = 'sku='+sku+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //alert("Product Not Available or Disabled.");
                //$("#suggesstion-box").hide();
               // clearPurchaseData();
               // return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
        var newVal = val.split("||");
        $('#filter_name').val(newVal[2]);
        $('#filter_sku').val(newVal[1]);
        $('#suggesstion-box').hide();
        if(cnt=='1'){
            $( "#movement_report" ).submit();
        }

    }

function getValue(name,sku){  
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}

$('.discountdelete').click(function (event) {
    if (confirm('Are you sure you want to Delete this?')) {
        var url = $(this).attr('data-href');
        $.ajax({
            url: url,
            type: "GET",
            success: function () {
              location.reload();
            }
        });
    }
});
</script>
