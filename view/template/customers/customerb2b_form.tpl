<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">B2B Customer Master</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Customer Code</label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                  <input type="text" name="customer_code" class="textbox" value="<?php echo $customer_code;?>">
                                </td>
                            </tr>                             
                            <tr>
                             <td width="35%"><label for="name">Discount</label>
                                  </td>
                                  <td width="65%" class="order-nopadding1">
                                    <select name="discount" id="discount" class="selectpicker selectdropdown">
                                      <option value="">- None -</option>
                                      <?php foreach($discounts as $discountslist) { ?>
                                      <?php if($discountslist['discount_id'] == $discount) { ?>
                                      <option value="<?php echo $discountslist['discount_id']; ?>" selected="selected">
                                       <?php echo $discountslist['discount_name']; ?>
                                     </option>
                                      <?php } } ?>
                                    </select>
                                  </td>
                            </tr>
                             <tr>
                             <td width="35%"><label for="name">Sales Tax</label>
                                  </td>
                                  <td width="65%" class="order-nopadding1">
                                    <select name="salestax" id="salestax" class="selectpicker selectdropdown">
                                     <option value="">- None -</option>
                                      <?php foreach($taxrates as $taxrate) { ?>
                                      <?php if($taxrate['tax_rate_id'] == $salestax) { ?>
                                      <option value="<?php echo $taxrate['tax_rate_id']; ?>" selected="selected">
                                       <?php echo $taxrate['name']; ?>
                                     </option>
                                      <?php } } ?>
                                    </select>                                    
                                  </td>
                              </tr>
                              <tr>
                                <td width="35%"><label for="name">Tax Allowed</label></td>
                                <td width="65%" class="order-nopadding1">
                                  <input style="margin-left:0px" type="radio" name="tax_allow" value="0" <?php if($tax_allow == 0) { ?> checked="checked"<?php } ?>>Yes
                                  <input style="margin-left:0px" type="radio" name="tax_allow" value="1" <?php if($tax_allow == 1) { ?> checked="checked"<?php } ?>>No
                                </td>
                              </tr>
                              <tr>
                                <td width="35%"><?php echo 'Status'; ?></td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="status" class="selectpicker selectdropdown">                                   
                                        <option value="0" <?php if ($status == 0) { ?> selected="selected" <?php } ?>><?php echo 'Enabled'; ?></option>
                                        <option value="1" <?php if ($status == 1) { ?> selected="selected" <?php } ?>><?php echo 'Disabled'; ?></option>
                                    </select>
                                </td>
                              </tr>
                        </tbody>
                    </table>
                     <table class="table orderlist">
                    	<thead>
                        <tr><th colspan="2">Biographical</th></tr>
                      </thead>
                    	<tbody>
                        <tr>
                          <td width="35%"><label for="name">Name <span class="required">*</span></label></td>
                          <td width="65%" class="order-nopadding">
                            <?php if($error_name) { ?>
                            <div class="input-icon right">
                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                            <input type="text" name="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Name">
                          </div>
                          <?php } ?>
                          <?php if(!$error_name) { ?>
                          <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Name">
                          <?php } ?>
                          </td>
                        </tr>
	                       <tr>
                          <td width="35%"><label for="name">Title</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="customer_title" value="<?php echo $customer_title; ?>" class="textbox" placeholder="Title">
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Company</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="company" value="<?php echo $company; ?>" class="textbox" placeholder="Company">
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Date of Birth</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="birth_date" value="<?php echo $birth_date; ?>" class="textbox date" placeholder="Date of Birth">
                          </td>
                        </tr>   
                      </tbody>
                    </table>
                      <table class="table orderlist">
                      	<thead>
                        	<tr>
                            	<th colspan="2">Phones</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
              							<td width="35%">
              								<label for="home_num">Home</label>
              							</td>
              							<td width="65%" class="order-nopadding">
              								<input type="text" name="home" value="<?php echo $home; ?>" class="textbox" placeholder="Home" />
              							</td>
            						  </tr>
                          <tr>
                            <td width="35%">
                              <label for="work">Work</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="work" value="<?php echo $work; ?>" class="textbox" placeholder="Work" />
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="mobile">Mobile<span class="required">*</span></label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <?php if($error_mobile) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                  <input type="text" name="mobile" value="" class="textbox requiredborder" placeholder="Mobile" />
                                </div>
                              <?php } else { ?>
                              <input type="text" name="mobile" value="<?php echo $mobile; ?>" class="textbox" placeholder="Mobile" />
                              <?php } ?>
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="pager">Pager</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="pager" value="<?php echo $pager; ?>" class="textbox" placeholder="Pager" />
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="fax">Fax</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="fax" value="<?php echo $fax; ?>" class="textbox" placeholder="Fax" />
                            </td>
                          </tr>
                        </tbody>
                      </table>
                   	</div>
                   	<div class="col-md-6">
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Address</th></tr>
                        	</thead>
                        	<tbody>
	                        	<tr>
            									<td width="35%"><label for="address">Address</label></td>
            									<td width="65%" class="order-nopadding">
            										<input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Address1">
                                <input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox" placeholder="Address 2">
            									</td>
            								</tr>								            
                            <tr>
                              <td width="35%"><label for="city">City</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="textbox" placeholder="City">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="address2">State</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="textbox" placeholder="State">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="zipcode">Zip Code<span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_zipcode) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                   <input type="text" name="zipcode" id="zipcode" value="" class="textbox requiredborder" placeholder="Zip Code">
                                </div>
                                <?php } else { ?>
                                  <input type="text" name="zipcode" id="zipcode" value="<?php echo $zipcode; ?>" class="textbox" placeholder="Zip Code">
                                <?php }?>

                              </td>
                            </tr>
                        </tbody>
                   	  </table>
                      <table class="table orderlist">
                        <thead>
                          <tr><th colspan="2">Others</th></tr>
                        </thead>
                          <tbody>
                            <tr>
                              <td width="35%"><label for="website">Website</label></td>
                              <td width="65%" class="order-nopadding">
                            <?php if($error_website) { ?>
                            <div class="input-icon right">
                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Website"></i>
                             <input type="text" name="website" id="website" value="<?php echo $website; ?>" class="textbox requiredborder" placeholder="Website">
                          </div>
                          <?php } ?>
                          <?php if(!$error_website) { ?>
                                <input type="text" name="website" id="website" value="<?php echo $website; ?>" class="textbox" placeholder="Website">
                                <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="primary_email">Primary Email <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                            <?php if($error_primary_email) { ?>
                            <div class="input-icon right">
                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Email"></i>
                            <input type="text" name="primary_email" id="primary_email" value="<?php echo $primary_email; ?>" class="textbox requiredborder" placeholder="Primary Email">
                          </div>
                          <?php } ?>
                          <?php if(!$error_primary_email) { ?>
                                <input type="text" name="primary_email" id="primary_email" value="<?php echo $primary_email; ?>" class="textbox" placeholder="Primary Email">
                                <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="secondary_email">Secondary Email</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="secondary_email" id="secondary_email" value="<?php echo $secondary_email; ?>" class="textbox" placeholder="Secondary Email">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="address2">Custom</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="custom" id="custom" value="<?php echo $custom; ?>" class="textbox" placeholder="Custom">
                              </td>
                            </tr>
                        </tbody>
                      </table>
                      <table class="table orderlist">
                          <thead>
                            <tr><th colspan="2">Tags</th></tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="65%" class="order-nopadding">
                                <input style="border-right:1px solid #DDDDDD" type="text" name="tag" value="<?php echo $tag; ?>" class="textbox" placeholder="Tag(s)">
                              </td>
                            </tr>
                        </tbody>
                      </table>
                       <table class="table orderlist">
                          <thead>
                            <tr><th colspan="2">Do Not</th></tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="35%"><label for="email"></label></td>
                              <td width="65%" class="order-nopadding5">
                                <?php if(count($do_not) > 0) { ?>                                                            
                                <input type="checkbox" name="do_not[]" id="email" value="Email" <?php if(in_array('Email', $do_not)) { ?> checked="checked" <?php } ?>>Email<br>
                                <input type="checkbox" name="do_not[]" id="mail" value="Mail"<?php if(in_array('Mail', $do_not)) { ?> checked="checked" <?php } ?>>Mail<br>
                                 <input type="checkbox" name="do_not[]" id="call" value="Call" <?php if(in_array('Call', $do_not)) { ?> checked="checked" <?php } ?>>Call
                                <?php } ?>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                   	 </div>
                   </div>
                  <!--  <div class="col-md-12">
                      <table class="table orderlist">
                        <thead>
                            <tr><th colspan="2">Custom Fields <a href="#" style="float:right;color: white; margin-right:10px;"><i class="icon-gear"></i>Settings</a></th></tr>
                        </thead>
                        <tbody>
                        <tr>
                          </td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="custom_field" id="custom_field" value="You must save customer before setting custom fields. Hide custom fields" readonly class="textbox">
                          </td>
                        </tr>
                        </tbody>
                    </table>
                    </div> -->
                    <div class="col-md-12">
                      <table class="table orderlist">
                        <thead>
                            <tr><th colspan="2">Notes</th></tr>
                        </thead>
                        <tbody>
                        <tr>
                          </td>
                          <td width="65%" class="order-nopadding">
                            <textarea style="border-right: 1px solid #ddd;" name="notes" id="notes" class="textbox" rows="5"><?php echo $notes; ?></textarea>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
            	<!-- 24-Feb-2016 Ends /!-->
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
function getTypeList(v)
{
  $.ajax({
      url:"index.php?route=customers/customersb2b/getTypeDetails&token=<?php echo $token; ?>",
      type:"POST",
      data:{customer_type:v},     
      success:function(out){
          out = JSON.parse(out);          
          html = '<option value="">- Select Discount -</option>';
            if(out != '') {                    
                html += '<option value="' + out['discount'] + '"';
                  if (out['discount'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['discount_name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'discount\']').html(html);

          html = '<option value="">- Select Tax -</option>';
            if(out != '') {                    
                html += '<option value="' + out['taxcategory'] + '"';
                  if (out['taxcategory'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'salestax\']').html(html);      
      }
  });
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
  $('.date').datepicker({ dateFormat: 'dd/mm/yy' }).val()({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:'+(new Date).getFullYear() 
  });
</script>