<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="movement_report">
        <div class="page-content" >
            <h3 class="page-title">Customer Pricing</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                 <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
              <!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                  <div class="col-md-12">
                  <table class="table orderlist">
                    <thead>
                            <tr><th colspan="8">Customer details</th></tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td width="10%"><label for="name">Name<span class="required">*</span></label></td>  
                      <td width="15%"> 
                          <input type="hidden" name="filter_customer" id="filter_customer2">
                          <input type="text" id="filter_customer" class="textbox" value="" placeholder="Search Customer" autocomplete="off">
                      </td>
                      <td width="10%"><label for="name">Locations<span class="required">*</span></label></td>
                      <td width="15%">
                             <select name="filter_location" class="textbox" style="min-height: 35px; padding: 7px;">
                              <?php if(count($locations)>=2){?>
                              <option value="">-- Select Location --</option>
                              <?php }?>
                              <?php
                                 if(!empty($locations)){
                                      foreach($locations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                              <?php  } } ?> 
                          </select>
                      </td>
                      </tr>
                     <br>
                      <tr>
                        <td width="10%"><label for="name">From date<span class="required">*</span></label></td>                   
                        <td width="15%"> 
                            <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $filter_date_from; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>
                        <td width="10%"><label for="name">To date<span class="required">*</span></label></td>
                        <td width="15%">
                            <input type="text" placeholder='To Date' name="filter_date_to" id="filter_date_to" value="<?php echo $filter_date_to; ?>" class="textbox date" autocomplete="off" onchange="loaddaterangeData(this.value);" style="width: 20%;">
                        </td>
                      </tr>
                    </tbody>
                  </table>          
                </div>
                    <div class="col-md-6"></div>
         </div>
          <div class="col-md-12">
            <h2> Products List</h2>
            <h4> Existing Items</h4>
              <table class="table" id="Existingdata">
                        <thead>
                          <tr class="heading">
                            <td>SKU</td>
                            <td>Name</td>
                            <td>LPP price</td>
                            <td>Selling Price</td>
                            <td>Customer Price</td>
                            <td>Difference</td>
                            <td>Action</td>
                          </tr>
                        </thead>
                      <tbody id="resultexistTable">
                      
                   <?php 
                   if($PromotionSkuInfo){ $i=0;
                    foreach($PromotionSkuInfo as $products){ $i++;?>
                        <tr id=<?php echo "exist_sku_".$products['sku'];?>>
                          <td><input type="text" name="exist_sku_[]" readonly value='<?php echo $products['sku']; ?>'></td>
                          <td><input type="text" name="exist_name[]" readonly value='<?php echo $products['name']; ?>'></td>
                          <td><input type="text" name="exist_sku_price[]" class="sku_price" readonly value='<?php echo $products['sku_price']; ?>'></td>
                          <td><input type="text" name="exist_pro_price[]" class="pro_price" readonly value='<?php echo $products['promotion_price']; ?>'></td>
                          <td><a onblur="removeexistSKU('<?php echo $products['sku'];?>')">Remove</a></td>
                        
                        </tr>
                <?php } } ?>
                
              </tbody>
            </table>
            </div>
          <div class="col-md-12">
             <h4>New Items </h4>
            <table class="table orderlist statusstock">
              <tbody>
                <tr class="filter">  
                    <td> 
                          <select name="filter_department" id="filter_department" onchange="getCategory();" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $data['filter_department']) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                      <td>
                        <select name="filter_category" id="filter_category" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="">Select Department</option>
                        </select> 
                    </td>
                    <!-- <td>
                        <input type="text" placeholder="SKU / Name" name="filter_name" value="<?php echo $data['filter_name'];?>" class="textbox" 
                        autocomplete="off">
                    </td> -->
                    <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                            <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                    <td>
                         <input type="number" step="any" max="100" name="discount" id="discount"  class="textbox" placeholder="Enter discount percentage" autocomplete="off">
                    </td>
                  <td align="center" colspan="3">
                   <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>">
                      <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="loadData();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> Load Data</button>
                  </td>
                </tr>
              </tbody>
            </table>            
          </div>  
          <div class="col-md-12">
                <div class="innerpage-listcontent-blocks"> 
                 <table class="table orderlist statusstock" id="resultTable">
                        <thead>
                           <tr class="heading">
                            <td>SKU</td>
                            <td>Name</td>
                            <td>LPP price</td>
                            <td>Selling Price</td>
                            <td>Customer Price</td>
                            <td>Difference</td>
                           <!--  <td>Options</td> -->
                            <td>Action</td>
                           </tr>
                        </thead>
                 </table>
              </div>
         </div>
       </form>
    </div>
</div>

<?php echo $footer; ?>

<script type="text/javascript">
 /*$('.date').datepicker({ dateFormat: 'dd-mm-yy' }).val()({
    changeMonth: true,
    changeYear: true  
  }); */

  $(document).ready(function(){
      getCategory();
      var customer = "<?php echo $filter_customer; ?>";
      if(customer !=''){
          getCustomerDetails(customer);
      }
  });

    function getCustomerDetails(cust){
      $.ajax({
          type: "POST",
          url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
          data:{cust:cust},

          success:function(data){
            data = JSON.parse(data);
            $('#filter_customer').val(data.name);
            $('#filter_customer2').val(data.customercode);
          }
      });
  }

  $( "#filter_customer" ).autocomplete({
      source: function( request, response ) {
          $('#filter_customer2').val('');
          $.ajax({
              url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
              type: 'post',
              dataType: "json",
              data: {
                  search: request.term
              },
              success: function( data ) {
                  response(data);
              }
          });
      },
      select: function (event, ui) {
          $('#filter_customer').val(ui.item.label);
          $('#filter_customer2').val(ui.item.data_ref);
          return false;
      }
  });

  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
          $("#suggesstion-box").show();
          $("#suggesstion-box").html(str);
        }else{
          $("#suggesstion-box").hide();
          $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});
  
function getProductautoFill(sku) {
  var filter_name = $("#filter_name").val();
   
    if(filter_name ==''){
      $('#filter_sku').val('');
    }
    if(sku.length<=3){
      return false;
    }

    var location = $("#filter_location").val();
    var ajaxData = 'sku='+sku+'&location_code='+location;
    
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //alert("Product Not Available or Disabled.");
                //$("#suggesstion-box").hide();
               // clearPurchaseData();
               // return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
    var newVal = val.split("||");
    $('#filter_name').val(newVal[2]);
    $('#filter_sku').val(newVal[1]);
    $('#suggesstion-box').hide();
    if(cnt=='1'){
        $( "#movement_report" ).submit();
    }
}

function getValue(name,sku){  
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}

function loadData() {
  var filter_customer        = $('select[name=\'filter_customer\']').attr('value');  
  var filter_location        = $('select[name=\'filter_location\']').attr('value');  
  var filter_date_from       = $('input[name=\'filter_date_from\']').attr('value');  
  var filter_date_to         = $('input[name=\'filter_date_to\']').attr('value');  
  var filter_department      = $('select[name=\'filter_department\']').attr('value');  
  var filter_category        = $('select[name=\'filter_category\']').attr('value');
  var discountPercentage     = $('input[name=\'discount\']').attr('value');   
  var error = 0;
  if(filter_customer==''){
    alert('Please Select Customers');
    return false;
  }else if(filter_location==''){
    alert('Please select Location');
    return false;
  }else if(filter_date_from==''){
    alert('Please Enter Form date');
    return false;
  }else if(filter_date_to==''){
    alert('Please Enter To date');
    return false;
  }
  var ccP ='';
  var diffP = '';
  if (filter_department!='' || filter_category!='') {
        $("#resultTable").find("tr:gt(0)").remove();
        var ajaxData = $("form").serialize();
        $.ajax({
        type: "POST",
        url: 'index.php?route=customers/customerspricing/ajaxGetCustomerpricingList&token=<?php echo $token; ?>',
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result!='') {
               $('#selected_product_cnt').val('1');
               $.each(result, function(i, item) {
                var sku       = item.sku;
                var name      = item.name;
                var sku_cost  = item.sku_cost;
                var sku_price = item.sku_price;
                var skuS      = "'"+item.sku+"'";
                if(discountPercentage){
                  ccP = (sku_price - (sku_price*(discountPercentage/100))).toFixed(2);
                  diffP = (sku_price - ccP).toFixed(2); 
                }
                var TR = '<tr id="sku_'+i+'"><td><input type="text" name="sku[]" readonly value="'+sku+'"></td><td><input type="text" name="name[]" readonly value="'+name+'"></td><td><input type="text" name="lpp_price[]" id="lppprice_'+i+'" value="'+sku_cost+'"></td><td><input type="text" name="sku_price[]" id="skuprice_'+i+'" value="'+sku_price+'" readonly></td><td><input type="text" required name="cust_price[]" id="cust_price_'+i+'" onChange="custprice(this.value,'+i+');" value="'+ccP+'"></td><td><input type="text" readonly name="diff_price[]" id="diffprice_'+i+'" value="'+diffP+'"></td><td><a onclick="removeSKU('+i+')">Remove</td></tr>';
                $('#resultTable tr:last').after(TR);
               });
            } else {
                
            }
        }
    });
  }else{  
      alert("Select Department or category for loadData");
      return false;
  }
}

function getCategory(){
    var department_code = $('#filter_department').val();
    var category = '<?php echo $filter_category; ?>';

    if(department_code!=''){
        $.ajax({
            type : 'POST',
            url: 'index.php?route=customers/customerspricing/ajaxGetCategory&token=<?php echo $token; ?>',
            data: { department_code:department_code,category:category},
            success:function(data){
                $('#filter_category').html(data);
            } 
        });
    } else {
        $('#filter_category').html('<option value="">Select Category</option>');
      // getSubcategory();
    }
}
 function loaddaterangeData(todate) {
  var filter_customer        = $('select[name=\'filter_customer\']').attr('value');
  var filter_date_from       = $('input[name=\'filter_date_from\']').attr('value');     
  var filter_date_to         = todate;  
  var error = 0;
  if(filter_customer==''){
    alert('Please Select Customers');
    return false;
  }else if(filter_date_from==''){
    alert('Please Enter Form date');
    return false;
  }else if(filter_date_to==''){
    alert('Please Enter To date');
     return false;
  }
        $("#Existingdata").find("tr:gt(0)").remove();
        var ajaxData = $("form").serialize();
        $.ajax({
        type: "POST",
        url: 'index.php?route=customers/customerspricing/ajaxGetExistingdataList&token=<?php echo $token; ?>',
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result!='') {
               $('#selected_product_cnt').val('1');
               $.each(result, function(i, item) {
                var sku       = item.sku;
                var productname = item.product_name;
                var lpp_price = item.lpp_price;
                var sellingprice = item.selling_price;
                var cust_price = item.customer_price;
                var difference = item.price_difference;
                 var TR = '<tr id="cust_'+sku+'"><td><input type="text" name="skuvalue[]" readonly value='+sku+'></td><td><input type="text" name="productname[]" readonly value='+productname+'></td><td><input type="text" name="skucostprice" value='+lpp_price+'></td><td><input type="text" name="sellingprice[]" value='+sellingprice+' readonly></td><td><input type="text" name="cust_price_exist[]" readonly value='+cust_price+'></td><td><input type="text" name="difference[]" readonly value='+difference+'></td><td><a onclick="removeexistSKU('+sku+')">Remove</td></tr>';
                $('#Existingdata tr:last').after(TR);
               });
            } else {
                
            }
         }

       });
}

function removeSKU(sku){
    $('#resultTable tr#sku_'+sku).remove();
  }

function custprice(thisvalue,sku) {
  var selpre  = parseFloat($('#skuprice_'+sku).val());
  var lpp_pre = parseFloat($('#lppprice_'+sku).val());

   if(thisvalue !=''){
      if(thisvalue >= lpp_pre){
        var diff = selpre - thisvalue;
        $('#diffprice_'+sku).val(diff.toFixed(2));
      }else{
        alert('Your price is less then last purchase price');
         //$('#cust_price_'+sku).val('');
      }
    }
}

function removeexistSKU(sku){
  $('#Existingdata tr#cust_'+sku).remove();
}
</script>