<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="utf-8"/>
<title>JuzApps POS | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="view/assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="view/assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->

</head>
<!-- BEGIN BODY -->
<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <img src="view/assets/img/logo.png" alt=""/>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="" method="post">
        <h3 class="form-title">Forget Password ?</h3>
         <p>Enter your e-mail address below to get your password.</p>
            <?php if ($error_warning):?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span><?php echo $error_warning; ?></span>
            </div>
            <?php endif; ?>
        <div style="clear:both"></div>
        <div class="form-group">
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
               <input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="email" value="<?php echo $email; ?>">
                <?php if ($error_emailname) { ?>
              <div class="text-danger"><?php echo $error_emailname; ?></div>
              <?php } ?>
            </div>
        </div>
        <div class="form-actions" style="height:60px;" >
            <a href="<?php echo $back; ?>" ><button type="button" id="back-btn" class="btn btn-default">
            <i class="m-icon-swapleft"></i> Back </button></a>
            <button type="submit" class="btn btn-info pull-right">
            Submit </button>
        </div>
        <?php if ($redirect) : ?>
            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
        <?php endif; ?>
    </form>
    <!-- END LOGIN FORM -->
    
    <!-- END REGISTRATION FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
     <?php echo date('Y'); ?> &copy; JuzApps POS.
</div>

</body>
<!-- END BODY -->
</html>