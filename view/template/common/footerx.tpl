<!-- BEGIN FOOTER -->
<div class="footer">
  <?php if(JUZAPPS_LOGO_HIDE_LEFTMENU!= '1'){ ?>
  <div class="footer-inner-img">
    <img src="<?php echo SITE_URL.'/view/assets/img/logo_smlX.png'; ?>">
  </div>
  <?php } ?>
  <div class="footer-inner">
		 <?php echo date('Y'); ?> &copy; JuzApps POS.
	</div>
	<div class="footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!-- <script src="view/assets/plugins/jquery-1.11.0.min.js" type="text/javascript"></script> -->
<script src="view/assets/plugins/jquery-1.11.2.min.js"></script>



<script src="view/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="view/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<!-- <script src="view/assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
<script src="view/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="view/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="view/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="view/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="view/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script> -->
<script src="view/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery.peity.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery-knob/js/jquery.knob.js" type="text/javascript"></script>
<script src="view/assets/plugins/flot/jquery.flot.js" type="text/javascript"></script>
<script src="view/assets/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="view/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
 <script src="view/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
 <!-- <script src="view/assets/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script> -->
<!-- <script src="view/assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script> -->
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="view/assets/scripts/app.js" type="text/javascript"></script>
<script src="view/assets/scripts/custom.js" type="text/javascript"></script>
<script src="view/assets/scripts/mousetrap.min.js" type="text/javascript"></script>
<?php if($route=='pos/pos'){ ?>
<script src="view/assets/scripts/shortcut.js" type="text/javascript"></script>
   <?php } ?>
<script src="view/assets/scripts/index.js" type="text/javascript"></script>
<script src="view/assets/scripts/tasks.js" type="text/javascript"></script>
<script src="view/assets/scripts/table-advanced.js"></script>
<script type="text/javascript" src="view/assets/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="view/assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="view/assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
<script type="text/javascript" src="view/assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script type="text/javascript" src="view/assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="view/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="view/assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="view/assets/scripts/ui-extended-modals.js" type="text/javascript"></script>
<script src="view/assets/scripts/jquery.form.js" type="text/javascript"></script>
<script src="view/assets/scripts/jspdf.debug.js" type="text/javascript"></script>
<script src="view/assets/scripts/jquery.numpad.js" type="text/javascript"></script>

<!-- END PAGE LEVEL SCRIPTS -->

<script>
jQuery(document).ready(function() {
  addproducts('');
   <?php if($route=='pos/pos'){ ?>
     $( "#addProducts" ).delegate( ".addProductToCart", "click", function() {

        var product = $(this).attr('data-id');
        product     = product.replace('addProduct-','');
        var s     = '#aprow-'+product;
        var sku  = $(s).find('.sku').text();
        var barcodes  = $(s).find('.barcodes').text();
        var name  = $(s).find('.name').text();
        var price  = $(s).find('.price').text();
        price = Math.round(price).toFixed(2);
        var quantity = $(s).find('.quantity').text();
        addproducts(sku);
        $("#backssale").trigger('click');

        /*var sn = $('#producttobilling tr').length;
        var string ='<tr><td>'+sn+'</td><td>'+sku+'</td><td><a href="">'+name+'</a></td><td><input style="width:39px;" type="text" name=quantity-'+product+'/></td><td>$'+price+'</td><td> $'+price+'</td><td>$'+price+'</td><td> $'+price+'</td></tr>';
        $("#producttobilling").append(string);
        $("#ajax-modal").modal('hide');*/
    });
   $( "#addProducts" ).delegate( "#addProductssts .pagination > .links a", "click", function(e) {
        e.preventDefault();
        var page = $(this).attr('class');
        $("#page_prod").val(page);
        $("#products_search_forms").submit();
     });

     <?php if(isset($this->session->data["attachedcustomer"])) { ?>
      attachcustomer('<?php echo $this->session->data["attachedcustomer"]; ?>');
      <?php } ?>
   $("#deletecus").click(function(){
      attachcustomer(0);
   });

   <?php } ?>
   App.init(); // initlayout and core plugins
   Index.init();
   Index.initCalendar();
  /* Index.initJQVMAP(); // init index page's custom scripts
    // init index page's custom scripts
   Index.initCharts(); // init index page's custom scripts
   Index.initChat();
   Index.initMiniCharts();
   Index.initPeityElements();
   Index.initKnowElements();
   //Index.initDashboardDaterange();
   Tasks.initDashboardWidget();
   TableAdvanced.init();*/

   $('#form').submit(function(){
    if($('input[name*=\'selected\']:checked').length != 0) {
        if ($(this).attr('action').indexOf('delete',1) != -1) {
            if (!confirm('<?php echo $text_confirm; ?>')) {
                return false;
            }
        }
      } else {
        alert('Please Select atleast one item!');
        return false;
      }
    });

   $(".Auser_sel").click(function(){
       var d = $(this).attr("id");
       $('.accper').attr('checked', "checked");
       $('.accper').parent('span').addClass("checked");
   });
    $(".Ruser_sel").click(function(){
       var d = $(this).attr("id");
       $('.accper').attr('checked', "checked");
       $('.accper').parent('span').removeClass("checked");
   });
   $(".Buser_sel").click(function(){
       var d = $(this).attr("id");
       $('.accpet').attr('checked', "checked");
       $('.accpet').parent('span').addClass("checked");
   });
    $(".Cuser_sel").click(function(){
       var d = $(this).attr("id");
       $('.accpet').attr('checked', "checked");
       $('.accpet').parent('span').removeClass("checked");
   });

   	$(".viewadvanced").click(function() {
      $(".nventory-orderblocks-show-hide").slideToggle("fold, 1000");
    });
   // Search Products
    $("#searchproducts_pos").click(function(){
        if(!$('#Makepayment').is(':visible')){
          if($(".positembox").val()!=""){
         $.ajax({
             url:'index.php?route=pos/pos/addProducts&token=<?php echo $this->request->get["token"]; ?>',
             data:{searchkey:$(".positembox").val()},
             type:'POST',
             beforeSend:function(){
              $("#addProducts").html('<img src="view/image/newloading.gif" class="loadinggif"/>');
              $("#addProducts").show();
              $("#posterminal").hide();
             },
             success:function(out){
                 $("#addProducts").html(out);
                 $("#addProducts").show();
                 $("#posterminal").hide();
                 $(".positembox").val('');
                 $(".positembox").focus();
                 if($(".addProductToCart").length == 1){
                    $(".addProductToCart").trigger('click');
                }
               /* $("#ajax-modal").find('.modal-body').html(out);
                $("#ajax-modal").modal();*/
             }
         });
       }
       }

    });
    ///Add and Search Customer
    $("#searchcustomer").click(function(){
         //$('body').modalmanager('loading');
         if(!$('#Makepayment').is(':visible')){
         $.ajax({
             url:'index.php?route=pos/pos/getCustomers&token=<?php echo $this->request->get["token"]; ?>&r=1',
             data:{searchkey:$("#cust_search").val()},
             type:'POST',
             beforeSend:function(){
              $("#addProducts").html('<img src="view/image/newloading.gif" class="loadinggif"/>');
              $("#addProducts").show();
              $("#posterminal").hide();
             },
             success:function(out){
                 $("#addProducts").html(out);
                 $("#addProducts").show();
                 $("#posterminal").hide();
                 if($(".addcustosale").length == 1){
                    $(".addcustosale").trigger('click');
                }
               /* $("#ajax-modal").find('.modal-body').html(out);
                $("#ajax-modal").modal();*/
             }
         });
       }

    });


     $("#barcode").click(function(){
       if(!$('#Makepayment').is(':visible')){
         var s = $(".barcode_search").val();
         if($.trim(s)!=""){
         $.ajax({
             url:'index.php?route=pos/pos/addProducts&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             data:{filter_barcodes:s},
             beforeSend:function(){
             /* $("#addProducts").html('<img src="view/image/newloading.gif" class="loadinggif"/>');
              $("#addProducts").show();
              $("#posterminal").hide();*/
             },
             success:function(out){
                 $("#addProducts").html(out);
                 $("#addProducts").show();
                 $("#posterminal").hide();
                 $(".barcode_search").val('');
                 $(".barcode_search").focus();
                if($(".addProductToCart").length == 1){
                    $(".addProductToCart").trigger('click');
                }
             }
         });
        }
       }

    });

     $("#searchproduct-key").click(function(){
         $('body').modalmanager('loading');
         $.ajax({
             url:'index.php?route=functionkey/home_search&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             success:function(out){
                $("#ajax-modal").find('.modal-body').html(out);
                $("#ajax-modal").modal();
             }
         });
    });


  $("#tender").click(function(){
         $.ajax({
             url:'index.php?route=functionkey/f4&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             success:function(out){
                $("#Makepayment").html(out);
                if($("#Makepayment").is(":visible")){
                   $("#Makepayment").effect('slide', { direction: 'right', mode: 'hide' }, 500);
                } else{
                  $("#Makepayment").effect('slide', { direction: 'right', mode: 'show' }, 500);
                }
             }
         });

    });

   $("#disc").click(function(){
         $.ajax({
             url:'index.php?route=functionkey/f2&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             beforeSend:function(){
              $("#addProducts").html('<img src="view/image/newloading.gif" class="loadinggif"/>');
              $("#addProducts").show();
              $("#posterminal").hide();
             },
             success:function(out){
                $("#addProducts").html(out);
                $("#addProducts").show();
                $("#posterminal").hide();
             }
         });

    });
   ///
   /*CHANGE*/
      $("#change").click(function(){
         $('body').modalmanager('loading');
          var key = $('.current .posquantity').attr("key");
          var qty = "0";
         $.ajax({
             url:'index.php?route=functionkey/f11&token=<?php echo $this->request->get["token"]; ?>&&product_key='+key+'',
             type:'POST',
             success:function(out){
                $("#ajax-modal-change").find('.modal-body-change').html(out);
                $("#ajax-modal-change").find('.modal-footer').show();
                $("#ajax-modal-change").modal();
             }
         });
       });

      <?php if($this->user->hasPermission('modify','functionkey/f11') && $config_product_discount=='1') { ?>
         $( "table#producttobilling" ).delegate( ".posproductname", "click", function(e) {
              e.preventDefault();
              var keys = $(this).parent().parent().attr("id");
              key    = keys.replace("row-","");
              var qty = "0";
              $.ajax({
                 url:'index.php?route=functionkey/f11&token=<?php echo $this->request->get["token"]; ?>&&product_key='+key+'',
                 type:'POST',
                 success:function(out){
                    $("#"+keys).html(out);
                 }
             });
           });
   <?php } ?>

     ///attache customer


   /*REPRINT*/
    $("#reprint").click(function(){
         $('body').modalmanager('loading');
         $("#ajax-modal-print").modal();

    });

    $("#qedit").click(function(){
      $("#ajax-modal-qedit").modal();
    });

    $('#qedit_submit').click(function(){
      var s = $('#qedit_barcode').val();
      $.ajax({
           url:'index.php?route=inventory/common/getProductIdByBarcode&token=<?php echo $this->request->get["token"]; ?>',
           type:'POST',
           data:{qedit_barcode:s},
           success:function(output){
            output = JSON.parse(output);
            if(output['error']){
              $("#qedit_error").html('<p>'+output['error']+'</p>');
              $("#qedit_error").removeClass('alert alert-success;');
              $("#qedit_error").addClass('alert alert-danger').show();
            } else {
              var str1 = output['url'];
              var str2 = str1.replace("&amp;","&");
              var url = str2.replace("&amp;","&");
              window.location.href=url;
            }
           }
      });
    });

    /* HOLD */
    $("#hold").click(function(){
       $.ajax({
             url:'index.php?route=functionkey/f6&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             success:function(output){
               try {
                  var output = $.parseJSON(output);
                } catch (e) {
                }
                if(output['error']){
                    alert(output['error']);
                }  else{
                  $("#addProducts").html(output);
                  $("#addProducts").show();
                  $("#posterminal").hide();
                  $("#Makepayment").hide();
                }
             }
         });

    });
    /*$('#ajax-modal-hold').on('hide.bs.modal', function (e) {
        $("#holdmessage").html('');
        $("#holdmessage").removeClass('alert-danger');
         $("#holdmessage").removeClass('alert-succes');
         $("#holdmessage").hide();
         $("#holdorder").show();
         $("#form-body").show();
    });*/


    /* HOLD */
    /*RELEASE*/
    $("#release").click(function(){
    $('body').modalmanager('loading');
    $("#ajax-modal-release").modal();

    });
    $("#addProducts" ).delegate(".reopenquotes", "click", function(e) {
          e.preventDefault();
          var rowId = $(this).attr('id');
          $.ajax({
              url:"index.php?route=functionkey/f7&token=<?php echo $this->request->get["token"]; ?>",
              data:{relaease_bill_no:rowId},
              type:"POST",
              success:function(out){
                out = JSON.parse(out);
                if(out['error']){
                   alert(out['error']);
                } else{
                   addproducts('');
                  $("#addProducts").hide();
                  $("#posterminal").show();
                  $("#Makepayment").hide();
                }
              }
          });
    });
   /*  $('#ajax-modal-release').on('hide.bs.modal', function (e) {
        $("#releasemessage").html('');
        $("#releasemessage").removeClass('alert-danger');
         $("#releasemessage").removeClass('alert-succes');
         $("#releasemessage").hide();
         $("#releaseorder").show();
         $("#form-body").show();
    });  */
    /*RELEASE*/
   $("#clearpos").click(function(){
         $("#ajax-modal-clear").modal('show');
     });
   $("#clearallpos").click(function(){
         $.ajax({
             url:'index.php?route=pos/cart/clear&token=<?php echo $this->request->get["token"]; ?>',
             type:'POST',
             success:function(out){
                if(out==1){
                  addproducts('');
                  attachcustomer(0);
                  $("#ajax-modal-clear").modal('hide');
                }
             }
         });
   });


  /* $('tr .product-details').click(function(){
    var id = $(this).closest('tr').prop('id');
    alert(id);
  });*/

  $( "table#producttobilling" ).delegate( "input.posquantity", "change", function() {
       var id = $(this).attr('name');
       var values = id.split("-");
       var key    = values[1];
       var qty    =  $(this).val();
       $.ajax({
         url:'index.php?route=pos/addcart/update&token=<?php echo $this->request->get["token"]; ?>',
         data:{key:key,qty:qty},
         type:'POST',
         success:function(out){
            if(out==1){
              addproducts('');
            }
         }

       });
  });

  $( "table#producttobilling" ).delegate( ".removeproduct", "click", function() {
         var id = $(this).attr('id');
             id = id.replace("delete-",'');
          $.ajax({
            url:'index.php?route=pos/addcart/update&token=<?php echo $this->request->get["token"]; ?>',
            data:{key:id,qty:0},
            type:'POST',
            success:function(out){
              if(out==1){
                addproducts('');
              }
            }
          });

     });

});

/// fundtion for pos
 function addproducts(sku)
 {
    $.ajax({
      url:"index.php?route=pos/addcart&token=<?php echo $this->request->get['token']; ?>",
      data:{code:sku},
      type:"POST",
      success:function(results){
          //alert(results);
          results = JSON.parse(results);
          var out = results['products'];
          var string ="";
          for(var i=0; i<out.length;i++){
                  if(out[i]['quantity']>0){
                     var clas = 'sales';
                  } else{
                     var clas = 'refunds';
                  }
                  var product_id = out[i]['product_id'];
            string = string+'<tr class="product-details '+clas+'" id="row-'+out[i]['product_id']+'"><td><div class="btn-group btn-group-solid"><button type="button" id="delete-'+out[i]['product_id']+'" class="btn btn-default removeproduct"><i class="fa  fa-trash-o" title="deleteproducts"></i></button></div></td><td align="center">'+out[i]['code']+'</td><td align="center"><a href="#" class="posproductname1">'+out[i]['name']+'</a></td><td><input style="width:39px;" type="text" class="posquantity" id="posquantity" value="'+out[i]['quantity']+'" key='+out[i]['product_id']+' name="quantity-'+out[i]['product_id']+'"/></td><td align="right">'+out[i]['price']+'</td><td align="right">'+out[i]['sub_total']+'</td><td align="right" id="disc-amount">'+out[i]['discount_amount']+'<a href="#" class="posproductname"><img src="dis2.png"></a></td><td align="right">'+out[i]['total']+'</td></tr>';

            //$("#posquantity-"+out[i]['product_id']).numpad();
          }
          $("#producttobilling tbody").html(string);
          var tot = results['total'];
          var pr ="";
          for(var i=0; i <tot.length; i++){
	          if(tot[i]['code']=='total'){ var id = 'id="totalName"'; $("#totalamthidden").val(tot[i]["value"]);} else { var id =""}
	             pr = pr+'<tr><th '+id+' >'+tot[i]["title"]+'</th><td align="right">'+tot[i]["text"]+'</td></tr>';
	          }
          $("#totalpricetbl tbody").html(pr);

          if($(".refunds").length>0 || $("#exitem").hasClass("btn-primary")){
             $("#refundprocess").slideDown();
          } else{
            $("#refundprocess").slideUp();
          }
          if( $("#exitem").hasClass("btn-primary")){
            $("tr.sales").hide();
            $("tr.refunds").show();
          } else{
           $("tr.sales").show();
           $("tr.refunds").hide();
          }
           //$(".posquantity").numpad( {openOnEvent: 'focus'});
      }
    });

  $("table#producttobilling" ).delegate(".product-details", "click", function() {
          var rowId = $(this).attr('id');
          if(rowId) {
            $('table#producttobilling tr').removeClass('current');
            //$(this).addClass('current');
        }
    });
  }

  function Refundproducts(){
     $.ajax({
          url:"index.php?route=functionkey/f9&token=<?php echo $this->request->get['token'] ?>",
          type:"POST",
          success:function(output){
               console.log(output);
          }
       });
  }

  function attachcustomer(c)
     {
      $.ajax({
            url:"index.php?route=pos/pos/attachCustomer&token=<?php echo $this->request->get['token'] ?>",
            data:{customer_id:c},
            type:"POST",
            success:function(out){
              out = JSON.parse(out);
              if(out["success"]==true){
                 $("#pos_customer_id").val(out['c_id']);
                 $("#cust_disp").val(out['Name']);
                 $("#attchcbtns").show();
                 var editurl = '<a href="'+out["Editurl"]+'"><button class="btn btn-primary cust_new"><i class="fa fa-edit"></i> Edit</button></a>';
                 $("#attchcbtns").find('.edit').html(editurl);
                 $("#attchcbox").hide();
                 $("#addcuscbtns").hide();
              } else{
                $("#pos_customer_id").val('');
                 $("#cust_disp").val('');
                 $("#attchcbtns").hide();
                 $("#attchcbox").show();
                 $("#addcuscbtns").show();
                 $$("#attchcbtns").find('.edit').html("");
              }
            }

         })
     }

  function selectProduct(v){
    //alert(v);
    $.ajax({
        url:'index.php?route=transaction/purchase/getProductSkuDetails&token=<?php echo $this->request->get["token"]; ?>',
        data:{id:v},
        type:'POST',
         success:function(out){
            //alert(out);
            out = JSON.parse(out);
            $('#sku').val(out['sku']);
            $('#product_id').val(out['product_id']);
            $('#price').val(out['average_cost']);
            $('#raw_cost').val(out['price']);
            $('#name').val(out['name']);
            $('#ajax-modal').modal('hide');
         }
    });
}
$(document).ready(function(){
  if ( $('.readonly-text').is('[readonly]') ) {
    $('.readonly-text').css('background-color', '#EEEEEE');
  } 
});

$(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});
$("#cancel").submit(function() {
  var answer = confirm("Confirm submit?")
    if (answer){
      return true;
    }
    else {
      return false;
    }
});
$("#cancel").click(function() {
  var answer = confirm("Do you want to cancel the data")
    if (answer){
      return true;
    }
    else {
      return false;
    }
});
$("#clear").click(function() {
  var answer = confirm("Do you want to clear the data")
    if (answer){
      return true;
    }
    else {
      return false;
    }
});
</script>


<style>
#producttobilling tbody tr.current{
  background-color: #83D6FD !important;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>