<?php echo $header; ?>
<div class="content">
	<div id="login-box">
		<form action="<?php echo $action; ?>" method="post" id="change-password-form" class="login-form">
        	<?php if ($success) : ?>
                <div class="success" style="margin:15px;"><?php echo $success; ?></div>
            <?php endif; ?>
            <?php if ($error_warning):?>
            <div class="warning"><?php echo $error_warning; ?></div>
            <?php endif; ?>
			<h1><?php echo $heading_title; ?></h1>
			<div class="user-pic">
                <img src="view/image/user.png" alt="user">
                <span></span>
            </div>
            <input type="password" name="password" value="" class="password required-entry validate-password" placeholder="New Password">
			<input type="password" name="conform" value="" class="password required-entry validate-cpassword" placeholder="Conform Password">
            <button type="submit" class="signin" value=""><?php echo $button_change; ?></button>
		</form>
    </div>
	<script type="text/javascript">
		//<![CDATA[
			var changePasswordForm = new VarienForm('change-password-form', true);
		//]]>
	</script>
</div>
<?php echo $footer; ?>