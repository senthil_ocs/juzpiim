<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>JuzERP | Login</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<meta name="MobileOptimized" content="320">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->

<link rel="stylesheet" type="text/css" href="view/assets/plugins/select2/select2.css"/>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME STYLES -->
<link href="view/assets/css/style-conquer.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/style.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
<link href="view/assets/css/pages/login.css" rel="stylesheet" type="text/css"/>
<link href="view/assets/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
</head>
<!-- BEGIN BODY -->
<body class="login">
<?php if(LICENSE_REMAINING_DAYS>=1){?>
    <div class="footercookie-cont" id="footercookie-container">
        <div class="footercookie-wrap">
            <p>Your License going to Expire in <span> <?php echo LICENSE_REMAINING_DAYS;?> Days </span></p>
        </div>
    </div>
 <?php }?>

<!-- BEGIN LOGO -->

<div class="logo">
<?php if(PRODUCT_LOGO_HIDE_LOGIN!= '1'){
	$config = new Config();
	$this->load->model('setting/company');
	$this->load->model('tool/image');
	$company_info = $this->model_setting_company->getCompanyDetails(1);	
	if(isset($company_info['logo'])) {
		$preview_logo = $this->model_tool_image->resize($company_info['logo'], 180, 108);
	} 
 ?>
<!--  	<?php if($preview_logo){ ?>
 		<img src="<?php echo $preview_logo;?>" alt=""/>
 	<?php }else{?>	
    	<img src="view/assets/img/logo2.jpeg" alt=""/>
    <?php }?> -->
<?php }?>
</div>

<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="" method="post">
        <div><img src="<?php echo $preview_logo;?>" style="padding: 0px 55px;" alt=""/></div>
        <h3 class="form-title">Login to your account</h3>

            <?php if ($error_warning):?>
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>
                <?php echo $error_warning; ?></span>
            </div>
            <?php endif; ?>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
               
                <input class="form-control placeholder-no-fix" type="text" placeholder="Username" name="email"  />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password"  placeholder="Password" name="password1" />
            </div>
        </div>
        <div class="form-actions" style="height:60px;" >
            <label class="checkbox" style="width:100%;float:left;height:60px;">
            <!--<input type="checkbox" name="remember" value="1"/> Remember me </label>!-->
             <input type="checkbox" name="rememberme" id="rememberme"  class ="" value = "1" />  <label id = "remembermelbl" class= "" for="rememberme">Remember me 
             </label>
            <button type="submit" class="btn btn-info pull-right" style="margin-top:-4px;">
            <?php echo $button_login; ?> </button>
        </div>
        <!--10-MArch-2016!-->
        
       
       
    </form>
    <!-- END LOGIN FORM -->
    
    <!-- END REGISTRATION FORM -->
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
    <?php if(JUZAPPS_LOGO_HIDE_LOGIN!= '1'){ ?>
    <div class="footer-logo">
        <img src="view/assets/img/logo_footer.png" alt=""/>
    </div>
    <?php } ?>
    <?php echo date('Y'); ?> &copy; JuzApps POS.
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->

<script src="view/assets/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="view/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="view/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="view/assets/scripts/app.js" type="text/javascript"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script src="view/assets/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
jQuery(document).ready(function() {     
  App.init();
  Login.init();
 });
</script>
<script>
$(document).ready(function(){
    $('#login-trigger').click(function(e){
        e.preventDefault();
    $(this).next('#login-content').slideToggle();
    $(this).toggleClass('active');
    if ($(this).hasClass('active'))
        $(this).find('span').html('&#x25B2;')
    else
        $(this).find('span').html('&#x25BC;')
    })
    $('body').click(function(e) {
        if(e.target.id != 'login-trigger' && e.target.id !='login-content' && e.target.id !='username' && e.target.id !='password' && e.target.id !='rememberme' && e.target.id !='submit' && e.target.id !='remembermelbl' ) {
        $('#login-content').slideUp('slow');
        }
    });
    $('#login-trigger1').click(function(e){
        e.preventDefault();
        $(this).next('#login-content1').slideToggle();
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) $(this).find('span').html('&#x25B2;')
        else $(this).find('span').html('&#x25BC;')
    })
    $('body').click(function(e) {
        if(e.target.id != 'login-trigger1' && e.target.id !='login-content1' && e.target.id !='username' && e.target.id !='password' && e.target.id !='rememberme' && e.target.id !='submit' && e.target.id !='remembermelbl') {
        $('#login-content1').slideUp('slow');
        }
    });
});

</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>