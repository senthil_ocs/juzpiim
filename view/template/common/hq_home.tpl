<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<h3 class="page-title">
			Dashboard <small>statistics and more</small>
			</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<!-- <a href="<?php echo $dashboard; ?>">Dashboard</a> -->
						<a href="#">Dashboard</a>
					</li>
				</ul>
			</div>
			
			<div class="row">
      			<div class="col-md-10" style="border: 5px solid #eeeeee; margin: 20px;">
      				<h4>Payment History</h4>
      				<div class="innerpage-listcontent-blocks">
				        <div class="portlet bordernone" style="margin-bottom:0 !important;padding: 0px 0px 0px 21px;">
				          	<div class="page-bar portlet-title" style="height: 61px; min-height: 60px; padding:17px 15px 0 0;">
					            <div class="caption"> 
					              <form method="POST" name="list_filter" id="list_form_validation"> 
					                  <table class="table orderlist statusstock" style="margin: -6px 0 0 16px; width: 97%;">
					                    <tbody>
						                    <tr class="filter">                     
						                        <td>
						                           <select name="filter_delivery_man" id="filter_delivery_man" style="min-height: 35px; padding: 7px; width: 100%;">
							                            <option value="">Select Delivery Man</option>
							                            <?php foreach ($salesman as $value) { ?>
							                              <option value="<?php echo $value['id']; ?>" <?php if($filters['filter_delivery_man'] == $value['id']){ echo "selected"; } ?> ><?php echo $value['name']; ?></option>
							                            <?php } ?>
							                        </select>
						                        </td>
						                        <td> <input type="text" id='filter_date_from' placeholder='Date From' name="filter_date_from" value="<?php echo $filters['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
						                        </td>
						                        <td> <input type="text" id='filter_date_to' placeholder='Date To' name="filter_date_to" value="<?php echo $filters['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
						                        </td>
						                        <td>
						                          <select class="textbox" multiple name="filter_payment_status[]" id="filter_payment_status">
						                            <option value="Pending" <?php if(in_array('Pending',$filters['filter_payment_status'])){ echo "selected"; } ?>>Pending</option>
						                            <option value="Paid" <?php if(in_array('Paid',$filters['filter_payment_status'])){ echo "selected"; } ?>>Paid</option>
						                            <option value="Partial" <?php if(in_array('Partial',$filters['filter_payment_status'])){ echo "selected"; } ?>>Partial</option>
						                            <option value="Hold" <?php if(in_array('Hold',$filters['filter_payment_status'])){ echo "selected"; } ?>>Hold</option>
						                          </select>
						                        </td>
												<td class="center" style="background-color: #eeeeee;border: 1px solid #eeeeee;">
						                            <button class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i>Search</button>
						                        </td>
						                    </tr>
					                    </tbody>
					                  </table>
					            </div>
				            </div>                        
					    </form>
				        </div>     
				    </div>
					<table class="table orderlist statusstock">
	                     <thead>
		                    <tr class="heading">
		                        <td class="center">S.No</td>
		                        <td class="center">Sales Man</td>
		                        <td class="center">Invoice No</td>
		                        <td class="center">Do No</td>
		                        <td class="center">Do Date</td>
		                        <td class="center">Amount</td>
		                        <td class="center">Status</td>
		                    </tr>
                        </thead>
                        <tbody>
                        	<?php if (count($purchase_history) > 0) { $i = 1; ?>
	                        	<?php foreach ($purchase_history as $value) { ?>
		                        	<tr>
			                        	<td class="center"><?php echo $i; ?></td>
			                        	<td class="center"><?php echo $value['delivery_man']; ?></td>
			                        	<td class="center"><?php echo $value['invoice_no']; ?></td>
			                        	<td class="center"><?php echo $value['do_no']; ?></td>
			                        	<td class="center"><?php echo $value['payment_date']; ?></td>
			                        	<td class="center"><?php echo $value['amount']; ?></td>
			                        	<td class="center"><?php echo $value['payment_status']; ?></td>
		                        	</tr>
	                        	<?php $i++; } ?>
	                        <?php } else { ?>
	                        	<tr>
								  <td align="center" colspan="7"><?php echo 'No results!'; ?></td>
								</tr>
	                        <?php } ?>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
	    $("#filter_payment_status").CreateMultiCheckBox({ 
	      width: '230px', 
	      defaultText : 'Select Payment Status', 
	      height:'250px' 
	    });
	
	    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
	    if(payment.length > 0){
	      for (var i = 0, len = payment.length; i < len; i++) 
	      {
	        if(payment[i] != ''){
	            $("input[type=checkbox][class='mulinput'][value="+payment[i]+"]").prop("checked",true);
	        }
	      }
	    }
	});
</script>
<?php echo $footer; ?>