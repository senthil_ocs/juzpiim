          <div class="page-title">Search by Product</div>
          <div class="message"></div>
          <div class="product-search-form">
            <input type="hidden" name="page" id="page" value="<?php if(!empty($page)) { echo $page; } ?>"  />
              <div class="search-box" onLoad="focusCall();">
                <input type="text" name="searchKey" id="searchKeynew" 
                    placeholder="Search by Product Name, Description and Invoice code" 
                    class="input-text" 
                    value="<?php if(!empty($filter_name)) { echo $filter_name; } ?>" 
                    onkeyup="getProducts(this.value);"/></div>
              <!--<div class="search-box-button"><button id="doSearchProduct" class="btn btn-reset" name="cancel" type="button">GO</button></div>-->
          </div>

    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="payment-pagenew">
        <div id="maintableadmin">
            <table class="">
            <thead>
                <tr><td>Image</td>
                    <td>Inv Code</td>
                    <td>Barcode</td>
                    <td>Product Name</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                <div id="ajaxloader" style="display:none;"></div>
            <?php if ($product_collection) { //echo 'Count'.count($product_collection); ?>
                <?php foreach ($product_collection as $key=>$product) { ?>
                    <tr class="product-result-row" key="<?php echo $product['product_id']; ?>">
                        
                        <td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="border: 1px solid #DDDDDD;" /></td>
                        
                        <td><?php echo $product['sku']; ?></td>
                        <td><?php echo implode(',<br>',$product['barcodes']); ?></td>
                        <td><?php echo $product['name']; ?></td>
                        <td>
                        <?php if (!empty($product['special'])) { ?>
            <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
            <span style="color: #93fe45;"><?php echo $product['special']['price']; ?></span>
            <?php } else { ?>
            <?php echo $product['price']; ?>
            <?php } ?>
                        </td>
                        <td>
                        <?php if ($product['quantity'] <= 0) { ?>
            <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
            <?php } elseif ($product['quantity'] <= 5) { ?>
            <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
            <?php } else { ?>
            <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
            <?php } ?>
                        </td>
                        <td>
                        <?php if ($product['quantity'] <= 0) { ?>
                        <span style="color: #FF0000;"><?php echo $out_of_stock ?></span>
                        <?php } else { ?>
            <input type="hidden" name="addProduct" id="addProduct-<?php echo $product['product_id'] ?>" value="<?php echo $product['sku'] ?>" />
                        <button id="addProductToCart" data-id="addProduct-<?php echo $product['product_id'] ?>" class="addbtn btn-update" name="addProductToCart" type="button" onclick="addProductToForm('<?php echo $product['product_id'] ?>','<?php echo $product['sku'] ?>','<?php echo $product['name'] ?>','<?php echo $product['price']; ?>');">Add</button>
            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr> 
                    <td class="center empty" colspan="8">There is no records found</td>
                </tr>
            <?php } ?>
            <!-- <tr>
                <td class="center" colspan="8"><div class="pagination"><?php echo $pagination; ?></div></td>
            </tr> -->
            </tbody>
        </table>
  </div>
</form>


