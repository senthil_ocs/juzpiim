<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-success">Save changes</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
			<h3 class="page-title">
			Dashboard <small>statistics and more</small>
			</h3>
			
			<div class="row ">
				<div class="col-md-6 col-sm-6">
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cubes"></i>Recent Sales (10)
							</div>
							<div style="float:right" class="caption">
								<label class="btn btn-default btn-sm all_order">
									<a href="<?php echo $sales_summary; ?>">View All Sales</a></label>
							</div>							
							<div class="actions">
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-scrollable">
								<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th style="text-align: center;">Date</th>
										<th style="text-align: center;">Invoice No.</th>
										<th style="text-align: center;">Amount</th>
										<!-- <th style="text-align: center;">Action</th> -->
									</tr>
								</thead>
								<tbody>
									<?php if ($sales) { ?>
										<?php foreach ($sales as $sale) { ?>
										<?php if($sale['total'] == 0.00) { ?>

										<?php } else { ?>
								<tr>
									<td><?php echo $sale['sale_date']; ?></td>				
									<td><?php echo $sale['invoice_no'];?></td>
									<td>$<?php echo $sale['total'];?> &nbsp;&nbsp;&nbsp;&nbsp;	<!-- <span class="label label-success label-sm"><?php echo $order['status_name']; ?></span> --></td>
									<!-- <td><a style="width:100%" href="#" class="btn btn-default btn-xs">View</a></td> -->
								</tr>
								<?php } ?>
								<?php } ?>
									<?php } else { ?>
										<tr>

											<td align="center" colspan="5">No Results!!</td>
										</tr>
									<?php } ?>
								</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-6 col-sm-6">
					<div class="portlet tasks-widget">
						<div class="portlet-title">
							<div class="caption">
								<i class="icon-bar-chart"></i> Sale Statistics of <a> <?php echo date("F,Y"); ?> </a>
							</div>
							<div class="actions">
							</div>
						</div>
						
						<div class="portlet-body" id="chartdiv" style="width: 100%; height: 340px; padding:0px !important;">
							<div id="norecords"></div>
						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>

<script src="view/javascript/amcharts/amcharts.js" type="text/javascript"></script>
<script src="view/javascript/amcharts/serial.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<?php echo $footer; ?>
<script type="text/javascript">
var chart;

var chartData = [];

 function CreateAmCharts(chartData,Chartdiv) {
 	
	chart = new AmCharts.AmSerialChart();
	chart.pathToImages = "<?php echo SITE_URL; ?>image/";
	chart.dataProvider = chartData;
	chart.dataDateFormat = "YYYY-MM-DD";
	chart.categoryField = "date";
   
	var categoryAxis = chart.categoryAxis;
	categoryAxis.parseDates = true; 
	categoryAxis.minPeriod = "DD"; 
	categoryAxis.gridAlpha = 0.1;
	categoryAxis.minorGridAlpha = 0.1;
	categoryAxis.axisAlpha = 0;
	categoryAxis.minorGridEnabled = true;
	categoryAxis.inside = true;
	categoryAxis.title = 'Date';

	// value
	var valueAxis = new AmCharts.ValueAxis();
	valueAxis.integersOnly = true;
	valueAxis.tickLength = 0;
	valueAxis.axisAlpha = 0;
	valueAxis.showFirstLabel = false;
	valueAxis.showLastLabel = false;
	valueAxis.title = 'Sales';
	chart.addValueAxis(valueAxis);

	// GRAPH
	var graph = new AmCharts.AmGraph();
	graph.dashLength = 3;
	graph.lineColor = "#00CC00";
	graph.valueField = "sales";
	graph.dashLength = 3;
	graph.bullet = "round";
	graph.balloonText = "[[category]]<br><b><span style='font-size:14px;'>Sale:[[sales]]</span></b>";
	chart.addGraph(graph);
	    
	// CURSOR
	var chartCursor = new AmCharts.ChartCursor();
	chartCursor.valueLineEnabled = true;
	chartCursor.valueLineBalloonEnabled = true;
	chart.addChartCursor(chartCursor);

	// SCROLLBAR
	var chartScrollbar = new AmCharts.ChartScrollbar();
	chart.addChartScrollbar(chartScrollbar);

	// HORIZONTAL GREEN RANGE
	var guide = new AmCharts.Guide();
	guide.value = 10;
	guide.toValue = 20;
	guide.fillColor = "#00CC00";
	guide.inside = true;
	guide.fillAlpha = 0.2;
	guide.lineAlpha = 0;
	valueAxis.addGuide(guide);

	// TREND LINES
	// first trend line
	var trendLine = new AmCharts.TrendLine();
	// note,when creating date objects 0 month is January, as months are zero based in JavaScript.
	trendLine.initialDate = new Date(2012, 0, 2, 12); // 12 is hour - to start trend line in the middle of the day
	trendLine.finalDate = new Date(2012, 0, 11, 12);
	trendLine.initialValue = 10;
	trendLine.finalValue = 19;
	trendLine.lineColor = "#CC0000";
	chart.addTrendLine(trendLine);

	// second trend line
	trendLine = new AmCharts.TrendLine();
	trendLine.initialDate = new Date(2012, 0, 17, 12);
	trendLine.finalDate = new Date(2012, 0, 22, 12);
	trendLine.initialValue = 16;
	trendLine.finalValue = 10;
	trendLine.lineColor = "#CC0000";
	chart.addTrendLine(trendLine);

	// WRITE
	chart.write("chartdiv");
	
}
$(document).ready(function(){	
	$.ajax({
		url:'index.php?route=common/home/saleschart&token=<?php echo $this->session->data['token']; ?>',
		data: { charData:'',filter_year:'17' },
		success: function(json) {	
    		var obj = JSON.parse(json);
			CreateAmCharts(obj,'chartdiv');
			if(obj.length==0) {
				$("#chartdiv").html('<p style="text-align:center;font-size:14px;color:#ff0000;padding-top:140px;">No Record(s) Found!</p>');
			}
			//CreateAmCharts('obj.sales','obj.date','chartdiv');
		}
	
	  });	
});
</script>

