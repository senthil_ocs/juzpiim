	<?php
    
    	$dashboard = array('common/home','sale/order');
           
        $inventorys = array('inventory/common','inventory/category','inventory/category/insert','inventory/department','inventory/department/insert','inventory/brand','inventory/brand/insert','inventory/inventory/insert','inventory/inventory','inventory/barcode','inventory/search','master/uom','master/uom/insert','inventory/discount','inventory/discount/insert','inventory/inventorymovement','inventory/changelog','inventory/product_import');
        /*$transaction = array('transaction/common','transaction/purchase','transaction/purchase/insert','transaction/purchase_return','transaction/purchase_return/insert','transaction/sales','transaction/sales/insert','transaction/sales_return','transaction/sales_return/insert','transaction/purchase_invoice','transaction/purchase_invoice/update_invoice','transaction/sales_invoice/view','transaction/sales_invoice');*/
        $purchase = array('transaction/common_purchase','transaction/purchase','transaction/purchase/insert','transaction/purchase_return','transaction/purchase_return/insert','transaction/purchase_invoice/update_invoice','transaction/purchase_invoice','master/vendor','transaction/service_purchase');

        $sales = array('transaction/common_sales','transaction/sales','transaction/sales/insert','transaction/sales_return','transaction/sales_return/insert','transaction/sales_invoice/view','transaction/sales_invoice','setting/customers','customers/customerspricing','transaction/quotation','transaction/payment','transaction/payment','transaction/service_sales','transaction/delivery','transaction/lazada_order_import','transaction/qsm_order_import');

        $stock = array('transaction/common_stock','transaction/stock_request','transaction/stock_adjustment');

        $report = array('report/report','master/reports/printreports','inventory/reports/printreports','inventory/reports/printstock','transaction/transaction_reports','master/user','master/customers','master/user_right','master/terms','master/uomreports','master/reports','inventory/reports','master/discountreports');

        $pos = array('pos/pos','pos/common');
        
		$setting = array('setting/common','setting/company','user/user_permission','user/user','setting/setting','setting/location','user/user/insert','localisation/tax_rate','localisation/tax_rate/insert','setting/terms','setting/terms/insert','localisation/currency','localisation/currency/insert','setting/country','setting/country/insert','setting/zone','setting/zone/insert','setting/geo_zone','setting/geo_zone/insert','localisation/stock_status','localisation/stock_status/insert','localisation/tax_class','localisation/tax_class/insert','user/user_right','master/payment','master/payment/insert','setting/route','setting/route/insert','setting/salesman','setting/salesman/insert','setting/payment_type_master','setting/xero_settings','setting/salesperson','setting/salesmans/update','setting/email','setting/email_template');

        $customer =array('customers/customers','setting/customers/customertype','setting/customers/inserttype','setting/customers','setting/customers/insert','setting/member','setting/member/insert','master/vendor','master/vendor/insert');
      
    ?>
    
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse" style="margin-top:32px; ">
			<ul class="page-sidebar-menu">
				<li class="sidebar-toggler-wrapper">
					<div class="sidebar-toggler">
					</div>
					<div class="clearfix">
					</div>
				</li>                
                <li class="start">
					
				</li>				
				<li <?php if(in_array($route, $dashboard)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $home; ?>">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					<span class="selected"></span>
					</a>
				</li>
				<?php //if($this->user->hasPermission('access', 'pos/pos') || $this->user->hasPermission('modify', 'pos/pos')) { ?>
				<!-- <li <?php if(in_array($route, $pos)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $commonpos; ?>">
					<i class="icon-puzzle"></i>
					<span class="title">POS</span>					
					</a>
				</li> -->
				<?php //} ?>
				<?php if($this->user->hasPermission('access', 'inventory/common') || $this->user->hasPermission('modify', 'inventory/common')) { ?>
                <li <?php if(in_array($route, $inventorys)) {  ?> class="active" <?php } ?>>
                	<a href="<?php echo $commominventory; ?>">
					<i class="icon-present"></i>
					<span class="title">Product</span>					
					</a>
                </li>
                <?php } ?>
                <?php if($this->user->hasPermission('access', 'transaction/common_purchase') || $this->user->hasPermission('modify', 'transaction/common')) { ?>
				<li <?php if(in_array($route, $purchase)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $purchasetransaction; ?>">
					<i class="fa fa-shopping-cart"></i>
					<span class="title">Purchase</span>					
					</a>
				</li>
				<?php } ?>
                <?php if($this->user->hasPermission('access', 'transaction/common_sales') || $this->user->hasPermission('modify', 'transaction/common')) { ?>
				<li <?php if(in_array($route, $sales)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $salestransaction; ?>">
					<i class="fa fa-address-book"></i>
					<span class="title">Sales</span>					
					</a>
				</li>
				<?php } ?>
                <?php if($this->user->hasPermission('access', 'transaction/common_stock') || $this->user->hasPermission('modify', 'transaction/common')) { ?>
				<li <?php if(in_array($route, $stock)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $stocktransaction; ?>">
					<i class="icon-calendar"></i>
					<span class="title">Stock</span>					
					</a>
				</li>
				<?php } ?>
				<?php /*if($this->user->hasPermission('access', 'customer/common') || $this->user->hasPermission('modify', 'customer/common')) {*/ ?>
				<!-- <li <?php if(in_array($route, $customer)) {  ?> class="active" <?php } ?>>
					<a href="<?php echo $commomcustomer; ?>">
					<i class="icon-user"></i>
					<span class="title">APAR Settings</span>					
					</a>
				</li> -->
				<?php /*}*/ ?>
				<?php //if($this->user->hasPermission('access', 'report/report') || $this->user->hasPermission('modify', 'report/report')){ ?>
				<li <?php if(in_array($route, $report)) { ?> class="active" <?php } ?>>
					<a href="<?php echo $commonreport; ?>">
					<i class="icon-bar-chart"></i>
					<span class="title">Reports</span>
					</a>
				</li> 
				<?php //} ?>
				<?php if($this->user->hasPermission('access', 'setting/common')|| $this->user->hasPermission('modify', 'setting/common')) { ?>
                <li<?php if(in_array($route, $setting)) {  ?> class="active" <?php } ?>>
                	<a href="<?php echo $commomsetting; ?>">
					<i class="fa fa-wrench"></i>
					<span class="title">Settings</span>					
					</a>
				</li>
				<?php } ?>
				<li>
                	<a href="<?php echo $logout; ?>">
					<i class="fa fa-power-off"></i>
					<span class="title">Logout</span>					
					</a>
				</li>
			</ul>
		</div>
	</div>