 <div class="innerpage-listcontent-blocks">
              <table class="table orderlist statusstock">
                  <tr>
                    <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2">
                      <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                            <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                  </tr>
                  
                    <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                  <tr <?php echo $contact; ?>>
                     <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                    <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                  </tr>
                </table>
              <div>