<tr>
    <td class="label" colspan="2">
        <label for="invoice_no">Invoice No:</label>
    </td>
    <td class="value" colspan="2">
        <input type="text" name="invoice_no" id="invoice_no" value="<?php echo $invoice_no; ?>" readonly="readonly" class="disabled" />
        <input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>" readonly="readonly" />
    </td>
    <td class="label" colspan="2">
        <label for="invoice_no">Net Amount:</label>
    </td>
    <td class="value"colspan="2">
        <input type="text" name="net_amount" id="net_amount" value="<?php echo $net_amount; ?>" readonly="readonly" class="disabled" />
    </td>
</tr>
<tr>
    <td class="label" colspan="2">
        <label for="invoice_no">Paid Amount:</label>
    </td>
    <td class="value" colspan="2">
        <input type="text" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" />
    </td>
    <td class="label" colspan="2">
        <label for="invoice_no">Balance Amount:</label>
    </td>
    <td class="value"colspan="2">
        <input type="text" name="balance_amount" id="balance_amount" value="<?php echo $balance_amount; ?>" />
    </td>
</tr>
<tr>
    <td class="label" colspan="2">
        <label for="invoice_no">Payment Method:</label>
    </td>
    <td class="value" colspan="2">
        <select name="payment_mode[]" class="input-text" id="payment_mode" multiple="multiple" onchange="silpmethod(this)">
            <option value="">Select Payment</option>
            <?php if ($paymentCollection) { ?>
                <?php foreach ($paymentCollection as $key=>$payment) { ?>
                    <option value="<?php echo $payment['payment_id']; ?>" <?php if(in_array($payment['payment_id'], explode(",", $payment_method))): ?> selected="selected"<?php endif; ?>>
						<?php echo $payment['payment_name']; ?>
					</option>
                <?php } ?>
            <?php } ?>
        </select>
    </td>
    <td class="label" colspan="2">
        <label for="invoice_no">Slip No:</label>
    </td>
    <td class="value"colspan="2">
        <?php if($silp_number): ?>
            <input type="text" name="silp_number" id="silp_number" value="<?php echo $silp_number; ?>" />
        <?php else: ?>
            <input type="text" name="silp_number" id="silp_number" value="" readonly="readonly" class="disabled" />
        <?php endif; ?>
    </td>
</tr>

<tr>
    <td class="label" colspan="2">
        <label for="invoice_no">Card Amount:</label>
    </td>
    <td class="value" colspan="2">
        <input type="text" name="card_amount" id="card_amount" value="<?php if(!empty($card_amount)) { echo $card_amount; } ?>" <?php if(empty($card_amount)) { ?>readonly="readonly" class="disabled" <?php } else { ?>class="numeric_validator"<?php } ?> />
    </td>
    <td class="label" colspan="2">
        <label for="invoice_no">Cash Amount:</label>
    </td>
    <td class="value"colspan="2">
        <input type="text" name="cash_amount" id="cash_amount" value="<?php if(!empty($cash_amount)) { echo $cash_amount; } ?>" <?php if(empty($cash_amount)) { ?>readonly="readonly" class="disabled" <?php } else { ?>class="numeric_validator"<?php } ?> />
    </td>
</tr>
<script type="text/javascript">jQuery(".numeric_validator").ForceNumericOnly();</script>
