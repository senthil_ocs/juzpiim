<div class="portlet bordernone">
    <div class="portlet-title portlet-height">
        <div class="caption caption-size">
            DISCOUNT 
        </div>   
    </div> 
    <p class="adjustment-para"></p>
    <h4 style="margin-left:15px;border-bottom:1px solid #DDDDDD;line-height:32px;">APPLY DISCOUNT TO ALL SALE LINES</h4>
    
    <p class="sub-para">The discount you select will be applied to all the current lines on the sale. This will not affect lines that can't be discounted. Lines added after will not automatically have the discount applied. Selecting 'None / Clear All' will remove all discounts from the sale.</p>    
</div>

<div class="portlet bordernone">
    <div class="portlet-body form">
        <div class="message" id="disc-error"></div>
        <form  class="form-horizontal" id="bill-discount-form" action="<?php echo $action; ?>" method="POST">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Discount Methods: </label>
                    <div class="col-md-4">
                        <select name="discount_mode" class="input-text form-control" id="discount_mode">
                        <option value="">Select Mode</option>
                        <?php if ($discountCollection) { ?>
                        <?php foreach ($discountCollection as $key=>$discount) { ?>
                        <option value="<?php echo $discount['discount_status_id']; ?>" <?php if($discount_mode==$discount['discount_status_id']): ?> selected="selected"<?php endif; ?>>
                        <?php echo $discount['name']; ?>
                        </option>
                        <?php } ?>
                        <?php } ?>
                        </select>
                        <span class="help-block">
                        A block of help text. </span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Voucher Discount: </label>
                    <div class="col-md-4">
                        <div class="input-group">
                          <input type="text" name="voucher_discount" id="voucher_discount" value="<?php echo $voucher_discount; ?>" class="input-text form-control">
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-success">Submit</button>
                        <button type="button" class="btn btn-default backssale">Cancel</button>
                        <button type="button" class="btn btn-default backssale removediscount">Remove Discount</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>

<script src="view/assets/scripts/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#bill-discount-form").ajaxForm({ //login form
        success: function(output){
            output = JSON.parse(output);
            if(output['error']){
                var html ='<div class="alert alert-danger"><p>'+output['error']+'</p></div>';
                $('#disc-error').html(html);
            } else{
                $('#disc-error').html('');
                addproducts('');
                $(".backssale").trigger('click');

            }
        }
    });
    $(".removediscount").click(function(){
        $("#discount_mode").val("1");
        $("#voucher_discount").val("0");
            $("#bill-discount-form").submit();
    });
});
</script>
