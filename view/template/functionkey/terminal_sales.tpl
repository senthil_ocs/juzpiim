<?php echo $popup_header; ?>
<div id="terminal-page">
<div class="page-title">Terminal Sales</div>
<div class="message"></div>

<?php if (empty($is_terminal_sale)) { ?>
<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="cashier-login-form">
    <div id="main-table">
    
        <div class="payment-method-details sales_login_wrapper">
        	<div class="fields">                
                <div class="field">
                	<div class="label">Cashier/Admin Password: </div>
                    <div class="value"><input type="password" name="cashier_password" id="cashier_password" value="" class="input-text" autocomplete="off" ></div>
                </div>
			</div>
        </div>
        
       <div class="form-action cashier-login-buttons">
           <button type="button" name="submit" id="cashierLoginConfirm" class="btn btn-update">Enter</button>
           <button type="button" name="cancel" id="cashierLoginClose" class="btn btn-reset">Close</button>
        </div>
        
	</div>
</form>
<?php } else { ?>

<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="settlement-form">
<div class="payment-method-details terminal_sale_wrapper">
    <div class="fields">
        <div class="field">
            <div class="label">Cashier Id: </div>
            <div class="value">
                <input type="text" name="cashier_id" id="cashier_id" value="<?php echo $cashier_id; ?>" class="input-text disabled" readonly="readonly" />
            </div>
        </div>
        <div class="field">
            <div class="label">Cashier Name: </div>
            <div class="value"><input type="text" name="cashier_name" id="cashier_name" value="<?php echo $cashier_name; ?>" class="input-text disabled" readonly="readonly" /></div>
        </div>
    </div>
</div>

<div id="main-table">

    <table class="list">
        <thead>
            <tr>
                <td>$</td>
                <td>No Of $</td>
                <td>Amount</td>
            </tr>
        </thead>
        <tbody>
        	<?php foreach($price_types as $price_type): ?>
            	<?php if(is_array($settlementCash) && array_key_exists($price_type, $settlementCash)): ?>
					<?php 
                        $totalamount	= $price_type * $settlementCash[$price_type];
						$amountQty	= $settlementCash[$price_type];
                    ?>
                <?php else: ?>
                	<?php 
                        $totalamount	= 0;
						$amountQty	= 0;
                    ?>
                <?php endif; ?>
                <tr>
                    <td><?php echo $price_type; ?></td>
                    <td><input type="text" name="<?php echo $price_type; ?>_amount" onchange="calculateSettle(this.value,<?php echo $price_type; ?>);" value="<?php echo $amountQty; ?>" class="input-text numeric_validator"/></td>
                    <td><?php echo $this->currency->format($totalamount); ?></td>
                </tr>
            <?php endforeach; ?>
            <tr class="terminal-sale-title">
                <td>Pay Mode</td>
                <td>Computer Total</td>
                <td>Manual Total</td>
            </tr>
            
            <tr>
                <td>CASH</td>
                <td><?php echo $computer_cash_total; ?></td>
                <td class="total_price" id="manualTotal"><?php if(!empty($manual_total)) { echo $manual_total; } ?></td>
            </tr>
            <tr>
                <td>CASH CARD</td>
                <td><?php echo $computer_card_total; ?></td>
                <td class="total_price"><?php echo $computer_card_total; ?></td>
            </tr>
            <tr>
                <td>EXPENSES</td>
                <td><?php echo $expense_total_amount; ?></td>
                <td class="total_price"><?php echo $expense_total_amount; ?></td>
            </tr>
            <tr>
                <td>TOTAL</td>
                <td><?php echo $computer_grant_total; ?></td>
                <td id="grantTotal"><?php if(!empty($manual_grant_total)) { echo $manual_grant_total; } ?></td>
            </tr>
            
            <tr>
            	<td colspan="3">
                <div class="form-action cashier-login-buttons">
                   <button type="button" name="submit" id="settlementConfirmbutton" onclick="settlementConfirm(<?php echo $computer_grant_total_value; ?>, <?php echo $manual_grant_total_value; ?>)" class="btn btn-update">Save</button>
                   <button type="button" name="cancel" id="cashierLoginClose" class="btn btn-reset">Close</button>
                </div>
        		</td>
            </tr>
            
        </tbody>        
    </table>
    
</div>
</form>
<script type="text/javascript">jQuery(".numeric_validator").ForceNumericOnly();</script>

<?php } ?>

</div>
<?php echo $popup_footer; ?>
