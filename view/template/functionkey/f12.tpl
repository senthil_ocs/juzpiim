<div class="row">
<div id="bill-discount-page" class="popup-page" style="min-height: 100px;">
<div class="page-title"></div>
<div class="message" id="disc-error">

</div>
<form method="post" enctype="multipart/form-data" id="change-discount-form" action="<?php echo $action; ?>">
    <div id="main-table">
		<div class="payment-method-details" style="width: 55%; text-align: center;">
        	<div class="fields">
                <div class="field">
                	<div class="label tender_label">Price Change Methods: </div>
                    <div class="value" style="margin: 0 0 20px 0;">
                        <select name="discount_mode" class="input-text" id="discount_mode">
                            <option value="">Select Mode</option>
                            <?php if ($discountCollection) { ?>
                                <?php foreach ($discountCollection as $key=>$discount) { ?>
                                    <option value="<?php echo $discount['discount_status_id']; ?>" <?php if($discount_mode==$discount['discount_status_id']): ?> selected="selected"<?php endif; ?>>
                                        <?php echo $discount['name']; ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
					</div>
                </div>
                <div class="field">
                	<div class="label tender_label">Apply Value: </div>
                    <div class="value"><input type="text" name="voucher_discount" id="voucher_discount" value="<?php echo $voucher_discount; ?>" class="input-text"></div>
                </div>
			</div>
        </div>
        <?php //if(!empty($totals)): ?>
			<?php $disabled_button	= false; ?>
           <!--  <div class="cash-details">
                <ul>
                    <?php foreach ($totals as $total) { ?>                        
                         <?php $cart_total	= $total['value']; ?>
                        <?php if(strtolower($total['title'])=='total' && $cart_total > 0):?>
                            <?php $disabled_button	= true; ?>
                        <?php endif; ?>
                        <li><?php echo $total['title']; ?> <span><?php echo $total['text']; ?></span></li>
                    <?php } ?>
                </ul>                
            </div> -->
		<?php //endif; ?>
	</div>
</form>
</div>
<script src="view/assets/scripts/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function(){
    $("#change-discount-form").ajaxForm({
        success: function(output){
            output = JSON.parse(output);
            if(output['error']){
                var html ='<div class="alert alert-danger"><p>'+output['error']+'</p></div>';
                $('#disc-error').html(html);
            } else{
                $('#disc-error').html('');
                var html ='<div class="alert alert-success"><p>Discount Applied Success fully</p></div>';
                html =html+'<button type="button" name="submit" id="continue_order_change" class="btn btn-primary">Continue Order</button>';
                 $("#ajax-modal-change").find('.modal-body-change').html(html);
                 $("#ajax-modal-change").find('.modal-footer').hide(); 


            }
        }
    });
    $("#doSaveOrder3").click(function(){
        $("#change-discount-form").submit();
    });
 });
	
</script>
