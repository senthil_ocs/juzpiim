<body class="invoice-page">
<div class="print-invoice">
	<?php if(!empty($invoiceCollection)): ?>
        <p style="text-align:center;"><?php echo $companyInfo; ?></p>
        <p class="separation">&nbsp;</p>
        <p class="separation">&nbsp;</p>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td colspan="3" class="pdf-date">Date: <?php echo date('d/m/Y',strtotime($invoiceCollection['date_added'])); ?></td>
                    <td style="text-align:right;" colspan="3" class="">Time: <?php echo date('h:i A',strtotime($invoiceCollection['date_added'])); ?></td>
                </tr>
                <tr>
                    <td colspan="3">Bill No: <?php echo $invoiceCollection['invoice_no']; ?></td>
                </tr>
                <tr>
                    <td colspan="4">Cashier: <?php echo $invoiceCollection['cashier']; ?></td>
                </tr>
                <tr>
                   <?php if($CustomerInfo['customer_id']){ ?>
                    <td colspan="3">Customer: <?php echo $CustomerInfo['customer_title'].'.'.$CustomerInfo['name']; ?></td>
                    <?php } ?>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr >
                    <td class="pdf-td">S.No</td>
                    <td class="pdf-td">Item Code</td>
                    <td class="pdf-td">Qty</td>
                    <td class="pdf-td">Discount</td>
                    <td class="pdf-td">U/Price</td>
                    <td class="pdf-td" style="text-align:right;">Amount</td>
                </tr>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <?php if(!empty($productCollection)): ?>
                    <?php foreach($productCollection as $key=>$product): ?>
                        <?php $totalQty	= $totalQty+$product['quantity'];
                            if(!empty($this->session->data['update_price'][$product['product_id']])){
                                $product['discountamt'] = $product['price']-$this->session->data['update_price'][$product['product_id']];
                        }
                        ?>
                        <tr>
                            <td width="12%" style="text-align:left;"><?php echo $key+1; ?></td>
                            <td width="36%"><?php echo $product['sku']; ?></td>
                            <td width="12%" style="text-align:left;"><?php echo $product['quantity']; ?></td>
                            <td width="20%" style="text-align:left;"><?php echo number_format($product['discountamt'],2); ?></td>
                            <td width="20%" style="text-align:center;"><?php echo number_format($product['price'], 2); ?></td>
                            <td width="20%" style="text-align:right;"><?php echo number_format($product['total'], 2); ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="5"><?php echo $product['name']; ?></td>
                        </tr>
                        <?php if($product['label'] != '') { ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="5">(<?php echo $product['label']; ?>)</td>
                        </tr>
                        <?php } ?>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" class="pdf-td">Total Qty : <?php echo $totalQty; ?></td>
                    <td colspan="3" class="pdf-td" style="text-align:right;">Total Items : <?php echo $invoiceCollection['total_items']; ?></td>
                </tr>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <?php if(!empty($totalDetails)): ?>
                    <?php foreach($totalDetails as $key=>$total): ?>
                        <tr>
                            <td colspan="3" class="pdf-td"><?php echo $total['title']; ?></td>
                            <td>&nbsp;</td>
                            <td colspan="3" class="pdf-td" style="text-align:right;"><?php echo $total['text']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="pdf-td">Paid By :</td>
                </tr>
                <?php foreach ($paymentDetails as $key => $paymode) {
                     if($paymode!=0 && $paymode!=""){
                    ?>
                <tr>
                    <td colspan="3" class="pdf-td">
                     <?php echo $key; ?>
                    </td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="pdf-td" style="text-align:right;"><?php echo number_format($paymode, 2); ?></td>
                </tr>
                <?php }
            } ?>
                <?php if($invoiceCollection['payment_code'] !='cash'): ?>
                <tr>
                    <td colspan="3" class="pdf-td">Slip Number : </td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="pdf-td" style="text-align:right;"><?php echo $invoiceCollection['silp_number']; ?></td>
                </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" class="pdf-td">Change Due  :  </td>
                    <td colspan="4" class="pdf-td" style="text-align:right;">$<?php echo number_format($change, 2); ?></td>
                </tr>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" class="pdf-td" style="text-align:center;">THANK YOU VISIT AGAIN!!!</td>
                </tr>
                <tr>
                    <td colspan="6" class="separation">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6" align="center">Please Retain Your receipt for Exchange Within 7 Days.</td>
                </tr>

                <tr>
                    <td colspan="6" style="text-align:center;">Powered By www.oblaksolutions.com</td>
                </tr>
            </tbody>
        </table>
	<?php else: ?>
    	<p style="text-align:center; color:#FF0000">Please enter correct bill number and try again.</p>
    <?php endif; ?>
</div>
</body>
