<?php echo $popup_header; ?>
<div id="payment-page">
<div class="page-title">Payment Mode Change for today sale</div>
<div class="message"></div>
<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="payment-form">
    <div id="main-table">
        <table class="list">
            <thead>
                <tr>
                    <td>S.No</td>
                    <td>Invoice No</td>
                    <td>Time</td>
                    <td>Net Amount</td>
                    <td>Total Item(s)</td>
                    <td>Pay Mode</td>
                    <td>Tender</td>
                    <td>Change</td>
                </tr>
            </thead>
            <tbody>
            <?php if ($invoice_collection) { ?>
                <?php foreach ($invoice_collection as $key=>$invoice) { ?>
                    <tr class="payment-change-row <?php if($order_id == $invoice['order_id']):?> current <?php endif; ?>" key="<?php echo $invoice['order_id']; ?>">
                        <td><?php echo $key+1; ?></td>
                        <td><?php echo $invoice['invoice_no']; ?></td>
                        <td><?php echo date("h:i:s A", strtotime($invoice['order_time'])); ?></td>
                        <td class="highlight"><?php echo $invoice['net_amount']; ?></td>
                        <td><?php echo $invoice['total_items']; ?></td>
                        <td class="highlight">
                        <?php 
						$payment_method = '';
						foreach ($invoice['paymode'] as $paymode) {
						    $payment_method .= $paymode['payment_name'].',';
						}
						echo rtrim($payment_method, ",");
						?>
                        </td>
                        <td><?php echo $invoice['tendar']; ?></td>
                        <td><?php echo $invoice['changes']; ?></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr> 
                    <td class="center empty" colspan="8">There is no records found</td>
                </tr>
            <?php } ?>
            <tr>
                <td class="center" colspan="8"><div class="pagination"><?php echo $pagination; ?></div></td>
            </tr>
            <?php if ($invoice_collection) { ?>
				<?php echo $payment_bottom; ?>
                <tr class="action">
                    <td colspan="8" align="right"><button type="button" class="btn btn-update" name="submit" id="updatePaymentSection">Update</button></td>
                </tr>
			 <?php } ?>
            </tbody>
        </table>
	</div>
</form>
</div>
<?php echo $popup_footer; ?>
