<?php echo $popup_header; ?>

<div id="payment-page">
  <div class="page-title">Own Expenses</div>
  <div class="message"></div>
  <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="cashier-expenses-form">
    <div id="main-table">
      <div class="payment-method-details sales_login_wrapper">
        <div class="fields">
          <div class="field">
            <div class="label">Expense Amount: </div>
            <div class="value">
              <input type="text" name="expense_amount" id="expense_amount" value="" class="input-text" >
            </div>
          </div>
          <div class="field">
            <div class="label">Description: </div>
            <div class="value">
              <input type="text" name="description" id="description" value="" class="input-text" >
            </div>
          </div>
        </div>
      </div>
      <div class="form-action cashier-login-buttons">
        <button type="button" name="submit" id="cashierExpensesConfirm" class="btn btn-update">Enter</button>
        <button type="button" name="cancel" id="cashierExpensesClose" class="btn btn-reset">Close</button>
      </div>
    </div>
  </form>
</div>
<script type="text/javascript">jQuery("#expense_amount").ForceNumericOnly();</script>
<?php echo $popup_footer; ?> 