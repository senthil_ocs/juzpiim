<td colspan="2">
    <a href="#"><?php echo $product_details["name"];  ?></a>
</td>
<td colspan="6">
<div class="row prchg ">
   <div class="portlet bordernone bgnone">
        <div class="portlet-body form bgnone">
            <div class="message" id="change_error"></div>
            <form method="post" class="form-horizontal" action="<?php echo $changeprice; ?>" enctype="multipart/form-data" id="change-price-form">
            <input type="hidden" name="product_id" value="<?php echo $product_details['product_id']; ?>" />
             <div class="form-body">
            <div class="form-group">
                    <label class="col-md-3 control-label">Discount Methods: * </label>
                    <div class="col-md-4">
                         <select name="price_mode" class="input-text form-control" id="discount_mode">
                            <option value="">Select Mode</option>
                            <option value="1">Percentage</option>
                            <option value="2">Fixed</option>
                        </select>
                    </div>
            </div>
             <div class="form-group">
                    <label class="col-md-3 control-label">Apply Value: *</label>
                    <div class="col-md-4">
                        <input type="text" name="product_price" id="product_price" value="" class="input-text form-control">
                    </div>
            </div>
        </div>
        <div class="form-actions bgnone ">
                    <div class="col-md-offset-3 col-md-9 pull-right">
                        <button type="submit" class="btn btn-primary cust_new">Save</button>
                        <button type="button" class="btn btn-primary cust_new" id="cancelchg">Cancel</button>
                        <button type="button" class="btn btn-primary cust_new removediscount">Remove Discount</button>
                    </div>
            </div>
            </form>
        </div>
    </div>
</div>
</td>
<div id="change-price-utility" class="popup-page">
<div class="message"></div>
<form method="post" action="<?php echo $changeprice; ?>" enctype="multipart/form-data" id="change-price-form">
	<input type="hidden" name="product_id" value="<?php echo $product_details['product_id']; ?>" />
    <div id="main-table">
		<div class="payment-method-details price-change-utility">
            <div id="change_error" style="display: block;"></div>
        	<div class="fields">
                <div class="field" style="margin: 0 0 15px 0;">
                	<div class="label tender_label">Price change Methods: </div>
                    <div class="value">
                        <select name="price_mode" class="input-text" id="discount_mode">
                            <option value="">Select Mode</option>
                            <option value="1">Percentage</option>
                            <option value="2">Fixed</option>
                        </select>
					</div>
                </div>
                <div class="field" style="margin: 0 0 15px 0;">
                	<div class="label tender_label">Apply Value: </div>
                    <div class="value"><input type="text" name="product_price" id="product_price" value="<?php echo $product_details['price']; ?>" class="input-text"></div>
                </div>
			</div>
        </div>
	</div>
</form>
</div>
<style type="text/css">
.change_error{margin:0 0 10px 0; color: #C9302C; text-align: center; font-weight: bold;}
.price-change-utility{width: 100%}
</style>
<script type="text/javascript">
 $(document).ready(function(){
   $("#change-price-form").ajaxForm({
        success: function(output){
            output = JSON.parse(output);
            if(output['success']){
                addproducts('');
                $("#ajax-modal-change").modal('hide');
            } else if(output['error']){
                var msg = output['error'];
                $('#change_error').html(msg);
                $("#change_error").addClass('alert alert-danger').show();
            }
        }
    });
    $("#cancelchg").click(function(){
        addproducts('');
    });
    $(".removediscount").click(function(){
        $("#discount_mode").val("1");
            $("#change-price-form").submit();
    });
 });

</script>