<?php if($show) { ?>
<div class="row">
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="purchaseformpp">
    <input type="hidden"  id="page_purchase" name="page" >
    <input type="hidden" name="search_form" value='1'>
</form>
<?php } ?>
<div class="page-content-wrapper" id="pag_purch">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <table class="table table-striped table-bordered table-hover" id="sample_1">
            <thead>
                <tr>
                    <th>Image</th>
                    <th>Inv Code</th>
                    <th>Barcode</th>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php if ($product_collection) { ?>
                <?php foreach ($product_collection as $key=>$product) { ?>
                    <tr class="product-result-row" key="<?php echo $product['product_id']; ?>" id="row-<?php echo $product['product_id'] ?>">
                        
                        <td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="border: 1px solid #DDDDDD;" /></td>
                        
                        <td class="sku"><?php echo $product['sku']; ?></td>
                        <td class="barcodes"><?php echo implode(',<br>',$product['barcodes']); ?></td>
                        <td class="name"><?php echo $product['name']; ?></td>
                        <td class="price">
                        <?php if (!empty($product['special'])) { ?>
						<span style="text-decoration: line-through;" class="special"><?php echo $product['price']; ?></span><br/>
						<span style="color: #93fe45;" class="specials"><?php echo $product['special']['price']; ?></span>
						<?php } else { ?>
						<?php echo $product['price']; ?>
						<?php } ?>
                        </td>
                        <td class="quantity">
                        <?php if ($product['quantity'] <= 0) { ?>
						<span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
						<?php } elseif ($product['quantity'] <= 5) { ?>
						<span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
						<?php } else { ?>
						<span style="color: #008000;"><?php echo $product['quantity']; ?></span>
						<?php } ?>
                        </td>
                        <td>
                        <?php if ($product['quantity'] <= 0) { ?>
                        <span style="color: #FF0000;"><?php echo $out_of_stock ?></span>
                        <?php } else { ?>
						<input type="hidden" name="addProduct" id="addProduct-<?php echo $product['product_id'] ?>" value="<?php echo $product['sku'] ?>">
                        <button data-id="addProduct-<?php echo $product['product_id'] ?>" class="btn btn-update addProductToCart" name="addProductToCart" value="<?php echo $product['product_id'] ?>" onclick="selectProduct(this.value);" type="button">Add</button>
						<?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr> 
                    <td class="center empty" colspan="7">There is no records found</td>
                </tr>
            <?php } ?>
         
            </tbody>
        </table>        
        </div>    
    <div class="pagination" style="margin: 0 0 0 18px;width: 97%;"><?php echo $pagination; ?></div>
</div>
    <?php if($show) {?>
</div>
<script type="text/javascript">
  $(document).ready(function(){
      //$('#sample_1').dataTable({"pageLength": 5});
   /* $( "#sample_1" ).delegate( ".addProductToCart", "click", function() {
        var product = $(this).attr('data-id');
        product     = product.replace('addProduct-','');
        var s     = $('#row-'+product);
        var clone = $('#row-'+product).clone();
        var sku  = $(s).find('.sku').text();
        var barcodes  = $(s).find('.barcodes').text();
        var name  = $(s).find('.name').text();
        var price  = $(s).find('.price').text();
        price = Math.round(price).toFixed(2);
        var quantity = $(s).find('.quantity').text();
        addproducts(sku);
        
        /*var sn = $('#producttobilling tr').length;
        var string ='<tr><td>'+sn+'</td><td>'+sku+'</td><td><a href="">'+name+'</a></td><td><input style="width:39px;" type="text" name=quantity-'+product+'/></td><td>$'+price+'</td><td> $'+price+'</td><td>$'+price+'</td><td> $'+price+'</td></tr>';
        $("#producttobilling").append(string);
        $("#ajax-modal").modal('hide');//
    });*/
$('#pag_purch').delegate(".pagination > .links a", "click",function(e) {
      e.preventDefault();
      var page = $(this).attr('class');
      $('#page_purchase').val(page);
      $('#purchaseformpp').submit();
   });

 $("#purchaseformpp").ajaxForm({
        success:function(out)
        {
          $("#pag_purch").html(out);
        }
     });
  });
</script>
<?php } ?>