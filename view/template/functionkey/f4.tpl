<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="bill-payment-form" class="form-horizontal">
    <div class="row">
        <div class="alert hidden" id="errormessage">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <h4>Payment Methods: </h4>
            <?php if ($paymentCollection) { ?>
                <div class="btn-group" data-toggle="buttons">
                <?php foreach ($paymentCollection as $key=>$payment) { ?>
                <label class="btn btn-primary paymethod">
                <input type="checkbox" name="payment_mode[]" class="toggle" id='<?php echo $payment['payment_name']; ?>' value ="<?php echo $payment['payment_id']; ?>"><?php echo $payment['payment_name']; ?></label>
                <?php } ?>
                </div>
            <?php } ?>
            <div class="form-body">
                <div class="form-group">
                    <label class="label tender_label">Paid Amount:</label>
                    <div class="col-md-6">
                         <input type="text" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" class="form-control input-small"  <?php if(!empty($show_slip_method)): ?> readonly="readonly" disabled="disabled" <?php endif; ?> />
                    </div>
                </div>
                <div class="form-group hidden" id="show_slip_mode">
                    <label class="label tender_label">Slip Number:</label>
                    <div class="col-md-6">
                         <input type="text"  name="silp_number" id="silp_number" value="" class='form-control input-small'  />
                    </div>
                </div>
                <div class="form-group hidden" id="show_card_mode">
                    <label class="label tender_label">Card Amount:</label>
                    <div class="col-md-6">
                         <input type="text"  name="card_amount" id="card_amount" value="<?php echo $paid_amount; ?>" class='form-control input-small'  />
                    </div>
                </div>
                <div class="form-group hidden" id="show_cash_mode">
                    <label class="label tender_label">Cash Amount:</label>
                    <div class="col-md-6">
                         <input type="text"  name="cash_amount" id="cash_amount" value="<?php echo $paid_amount; ?>" class='form-control input-small'  />
                    </div>
                </div>

               

                <div class="form-group hidden" id="show_visa_mode">
                    <label class="label tender_label">Visa Amount:</label>
                    <div class="col-md-6">
                         <input type="text"  name="visa_amount" id="visa_amount" value="<?php echo $paid_amount; ?>" class='form-control input-small' />
                    </div>
                </div>
            </div>

        </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 cash-details">
      <?php if(!empty($totals)): 
      $disabled_button  = false; 
      ?>

                <ul>
                    <?php foreach ($totals as $total) { ?>                        
                        <?php $cart_total = $total['value']; ?>
                        <?php if(strtolower($total['title'])=='total' && $cart_total > 0):?>
                            <?php $disabled_button  = true; ?>
                        <?php endif; ?>
                        <li>
                            <span class="title1"><?php echo $total['title']; ?>: </span>
                            <span><?php echo $total['text']; ?></span>
                        </li>
                    <?php } ?>
                </ul>  
    <?php endif; ?>
</div>

</div>
</form>
<div class="helpText"><p>* Hold Control Key and Click more than one Payment Methods </p></div>
<script src="view/assets/scripts/jquery.form.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
<?php if($disabled_button) { ?>
  $("#doSaveOrder").attr('disabled',true);
 <?php } else { ?>
 $("#doSaveOrder").attr('disabled',false);
 <?php } ?> 
 $("#doSaveOrder").click(function(){
    $("#errormessage").addClass('hidden');
    $("#bill-payment-form").submit();
 });

 $("#bill-payment-form").ajaxForm({ //login form
            success: function(output){
                console.log(output);
                output = JSON.parse(output);
                if(output['error']){
                    $("#errormessage").html('<p>'+output['error']+'</p>');
                    $("#errormessage").addClass('alert-danger');
                    $("#errormessage").removeClass('hidden');
                } else{
                    $("#errormessage").addClass('hidden');

                }
                if(output['success']){
                    var html ='<div class="alert alert-success">The order has been received</div>';
                    html =html+'<h4>The order invoice is : # <span>'+output['htmldata']['last_order_id']+'</span></h4>';
                    html =html+'<h4>The return changes to customer is : # <span>'+output['htmldata']['return_amount']+'</h4>';
                    html =html+'<button type="button" name="submit" id="continue_order" class="btn btn-primary">Continue Order</button>';

                   $("#ajax-modal1").find('.modal-body').html(html);
                   $("#ajax-modal1").find('.modal-footer').hide(); 
                }
            }
         });
 });

</script>