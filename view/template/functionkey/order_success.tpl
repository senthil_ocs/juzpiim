<?php echo $popup_header; ?>
<div id="order-success-page" class="popup-page">
<?php if(!empty($last_order_id)): ?>
<div class="page-title">Order Success section</div>
<div class="message"><div class="success">The order has been received</div></div>
    <div id="main-table">
    	<div class="order-success-details">
        	<p>The order invoice is : # <span><?php echo $last_order_id; ?></span></p>
           	<?php if(!empty($return_amount)): ?>
            	<div class="return-amount">The return changes to customer is : # <span><?php echo $return_amount; ?></span></div>
			<?php endif; ?>
            <div class="form-action">
                <button type="button" name="submit" id="continue_order" class="btn btn-update">Continue Order</button>
            </div>
        </div>
	</div>
</div>
<?php endif; ?>
<?php echo $popup_footer; ?>
