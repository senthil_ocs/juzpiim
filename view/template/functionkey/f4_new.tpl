<?php echo $popup_header; ?>
<style type="text/css">
.gridtable{
  display: table; 
  padding: 5px;
  margin: 5px;
  background: #000;
}
.gridrow{
  display: table-row; 
  vertical-align: top;
  border-left: 0px;
  color: #fff;
  font-size: 14px;
}
.gridcell{
  display: table-cell;
  vertical-align: top;
  padding: 7px;
  width: 300px;
  text-align:left;
  margin: 1px;

}
.gridtd {
  width: 100px;
  font-style:italic;
}
.h{border-right: 2px solid #7E7E7E;background:#4F4F4F; text-align:center;}
.fh{background:#4F4F4F;text-align:center;}
.gridcell .input-text{border-radius: 5px; padding: 5px 0; width: 98%;}


</style>
<div id="bill-payment-page" class="popup-page">
<div class="page-title">Bill Settlement Screen</div>
<div class="message"></div>
<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="bill-payment-form">
    <div id="main-table">
    	
        <div class="payment-method-details gridtable">
            <div class="gridrow">
             <span class="gridcell h">
                Payments Methods
             </span>
             <span class="gridcell h">
                Paid Amount
             </span>
             <span class="gridcell fh">
                Slip Number
             </span>
        </div>
         <?php if ($paymentCollection) { ?>
            <?php foreach ($paymentCollection as $key=>$payment) { ?>
                <div class="gridrow">
                     <span class="gridcell gridtd">
                            <input type="checkbox" id="payment_mode" value="<?php echo $payment['payment_id']; ?>" name="payment_mode[]" 
                                    <?php if(in_array($payment['payment_id'], explode(",", $payment_method))){?> checked="checked" <?php }?>>
                                 <?php echo $payment['payment_name']; ?>
                     </span>
                     <span class="gridcell">
                        <input type="text" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" class="input-text">
                     </span>
                     <span class="gridcell">
                        <input type="text" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" class="input-text"> 
                     </span>
                </div>
           <?php } ?> 
        <?php } ?>

        

            <?php /*?>
        	<div class="fields">
                <div class="field">
                	<div class="label">Payment Methods: </div>
                    <div class="value">
                        <select name="payment_mode[]" class="input-text" id="payment_mode" multiple="multiple" onchange="cashSilpModeChange(this)">
                            <option value="">Select Payment</option>
                            <?php if ($paymentCollection) { ?>
                                <?php foreach ($paymentCollection as $key=>$payment) { ?>
                                    <option value="<?php echo $payment['payment_id']; ?>" <?php if(in_array($payment['payment_id'], explode(",", $payment_method))): ?> selected="selected"<?php endif; ?>>
                                        <?php echo $payment['payment_name']; ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
					</div>
                </div>
                <div class="field">
                	<div class="label">Paid Amount: </div>
                    <div class="value"><input type="text" name="paid_amount" id="paid_amount" value="<?php echo $paid_amount; ?>" class="input-text"<?php if(!empty($show_slip_method)): ?> readonly="readonly" disabled="disabled" <?php endif; ?> /></div>
                    <script type="text/javascript">jQuery("#paid_amount").ForceNumericOnly();</script>
                </div>
                <?php if(!empty($show_slip_method)): ?>
                    <div class="field">
                        <div class="label">Slip Number: </div>
                        <div class="value"><input type="text" name="silp_number" id="silp_number" value="" class="input-text" /></div>
                    </div>
				<?php endif; ?>
                
                <?php if(!empty($show_card_method)): ?>
                    <div class="field">
                        <div class="label">Card Amount: </div>
                        <div class="value"><input type="text" name="card_amount" id="card_amount" value="" class="input-text numeric_validator" onchange="dogetCardCashAmount(this.value, 'card')" /></div>
                    </div>
				<?php endif; ?>
                
				<?php if(!empty($show_cash_method)): ?>
                    <div class="field">
                        <div class="label">Cash Amount: </div>
                        <div class="value"><input type="text" name="cash_amount" id="cash_amount" value="" class="input-text numeric_validator" /></div>
                    </div>
				<?php endif; ?>
                <script type="text/javascript">jQuery(".numeric_validator").ForceNumericOnly();</script>
			</div>
            <?php */ ?>
        </div>


    	<?php if(!empty($totals)): ?>
			<?php $disabled_button	= false; ?>
            <div class="cash-details">
                <ul>
                    <?php foreach ($totals as $total) { ?>
                        <?php //$cart_total	= str_replace('$','',$total['text']); ?>
                        <?php $cart_total	= $total['value']; ?>
                        <?php if(strtolower($total['title'])=='total' && $cart_total > 0):?>
                            <?php $disabled_button	= true; ?>
                        <?php endif; ?>
                        <li><?php echo $total['title']; ?> <span><?php echo $total['text']; ?></span></li>
                    <?php } ?>
                </ul>
                <div class="form-action">
                    <?php if(empty($disabled_button)): ?>
                        <button type="button" name="submit" class="btn btn-update disabled">Enter</button>
                        <button type="button" name="cancel" id="doCancelOrder" class="btn btn-reset">Close</button>
                    <?php else: ?>
                        <button type="button" name="submit" id="doSaveOrder" class="btn btn-update">Enter</button>
                        <button type="button" name="cancel" id="doCancelOrder" class="btn btn-reset">Close</button>
                    <?php endif; ?>
                </div>
            </div>
		<?php endif; ?>
	</div>
</form>
</div>

<?php echo $popup_footer; ?>
