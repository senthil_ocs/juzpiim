<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Stock Adjustment Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                	<?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>StockAdjustment No</td>
                          <td><?php echo $purchaseInfo['stkAdjustment_id'] ?></td>
                          <td>StockAdjustment Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['stkAdjustment_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Location Code</td>
                          <td><?php echo $purchaseInfo['location_code']; ?></td>
                          <td>Terminal Code</td>
                          <td><?php echo $purchaseInfo['terminal_code']; ?></td>
                        </tr>
                        <tr>
                          <td>Add or Deduct</td>
                          <td><?php if($purchaseInfo['add_or_deduct'] == 1){ echo "Deduct"; } else { echo "Add"; } ?></td>
                          <td>Remarks</td>
                          <td><?php echo $purchaseInfo['remarks'];  ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.no</td>
                        <td class="center">Sku</td>
                        <td class="center">Sku Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">Price</td>
                       <!--  <td class="center">NetTotal</td>
                        <td class="center">GST</td>  -->
                        <td class="center">Total</td>
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i=1; ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <?php //printArray($products); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['sku_description']; ?></td>
                              <td><?php echo $products['qty']; ?></td>
                              <td class="right"><?php echo $products['unit_cost']; ?></td>
                              <!-- <td class="right"><?php echo $products['net_value']; ?></td>
                              <td class="right"><?php echo $products['gst']; ?></td> -->
                              <td class="right"><?php echo $products['total_value']; ?></td>
                            </tr>
                            <?php $total+= $products['total_value']; ?>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="5" align="right">Total</td>
                              <td class="right"><?php echo $this->currency->format($total); ?></td>
                            </tr>
                          <?php } else { ?>
                            <tr>
                                <td colspan="5">No records found!</td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>