<?php echo $header; ?>
<div class="clearfix"></div>
<div class="page-container">
    <?php echo $sidebar; ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"></h3>      
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li>
                        <?php echo $breadcrumb['separator']; ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                    </li>
                    <?php  }?>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="window.print();"><i class="fa fa-print"></i> Print</a> -->
                            <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a> -->
                            <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
                        </div>    
                    </div>
                </div>                          
            </div>

            <div style="clear:both"></div>            
            <div class="row">     
                <div class="col-md-12">      
                    <div class="innerpage-listcontent-blocks hideprint">
                        <table class="table orderlist statusstock">
                            <tr>
                                <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                                    <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="col-md-6"><strong>GST No: </strong>9268354532</div>
                                    <div class="col-md-6">Date: </strong><?php echo date('d/m/Y'); ?></div>
                                </td>
                            </tr>

                            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                            <tr <?php echo $contact; ?>>
                                <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                                <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                            </tr>
                        </table>
                    <div><h1>Price Tag Report</h1></div>
                </div>
                <div style="clear:both"></div>     

                <!-- NEW SEARCH  -->
                <div style="margin: 0px auto;max-width: 800px; width: 100%;">
                    <div class="innerpage-listcontent-blocks hideprint">
                        <div class="portlet bordernone" style="margin-bottom:0 !important">
                            <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px 0; padding:16px 0 0 0;background:none !important;">
                                <div class="caption"> 
                                 <form method="post" name="report_filter">
                                    <input type="hidden" name="type" id="type">                 
                                        <table class="table orderlist statusstock" style="margin: -6px 0 0 0; width: 100%;">
                                            <tbody>
                                               <tr class="filter"> 
                                                <td>
                                                    <select name="filter_department" id="filter_department" class="selectdropdown">
                                                        <option value="">-- Select Department --</option>
                                                        <?php foreach ($department_collection as $department) { ?>
                                                            <?php if (trim($department['department_code']) == trim($filter_department)) { ?>
                                                                <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                                            <?php } ?>   
                                                        <?php } ?>
                                                    </select> 
                                                </td>
                                                <td>
                                                    <select name="filter_category" id="filter_category" class="selectdropdown">
                                                        <option value="">-- Select category --</option>
                                                        <?php foreach ($category_collection as $category) { ?>
                                                            <?php if (trim($category['category_code']) == trim($filter_category)) { ?>
                                                                <option value="<?php echo trim($category['category_code']); ?>" selected="selected"><?php echo $category['category_name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                                            <?php } ?>   
                                                        <?php } ?>
                                                    </select>
                                                </td>
                                                <td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" placeholder="Product Name / Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                                                    <input type="hidden" name="sku" id="sku">
                                                <div id="suggesstion-box" class="auto-compltee"></div>
                                                </td>

                                                <td align=""><button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                                                </td>

                                                <td align="">
                                                    <button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="clearsearch();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> Clear</button>
                                                </td>

                                                <?php if(count($results)>0){ ?>
                                                    <td></td>
                                                    <td><a class="btn btn-zone btn-primary printpage" id="printBtn" style="width: 100%; min-height: 36px; border-radius:0 !important;" onClick="printpage('settlement-print');"><i class="fa fa-print"></i> Print</a></td>
                                                <?php } ?>

                                            </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>                        
                            </div>     
                            <div style="clear:both; margin: 0 0 15px 0;"></div>
                        </div>
                    </div>
                
                </div>
                <div style="margin: 0px auto;max-width: 700px; width: 100%;" id="settlement-print">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="innerpage-listcontent-blocks settlement-wrapper">
                              <?php //printArray($results);
                                    $tot = count($results);
                                    for($i=0;$i<$tot;$i++){ 
                                       
                                     ?>
                                    <div style="float: left;width: 320px;margin-bottom: 30px; padding: 15px;font-size: 18px;text-align:center;height: 100px;">
                                        <p><?php echo $results[$i]['name'];?> <br><br>
                                            <span style="font-size: 19px;font-weight:bold;margin-top:-2px;">S$ <?php echo number_format($results[$i]['sku_price'],2);?> </span>
                                        </p>    
                                    </div>

                               <?php } ?>
                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caption"></div>          
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript"><!--
    function clearsearch(){
       url = 'index.php?route=transaction/transaction_reports/pricetag&token=<?php echo $token; ?>';
       location = url;
         
    }
    function filter() {
    url = 'index.php?route=transaction/transaction_reports/pricetag&token=<?php echo $token; ?>';
    var filter_name = $('#filter_name').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }

    //var filter_department = $('input[name=\'filter_department\']').attr('value');
    var filter_department = $('#filter_department').attr('value');
    
    if (filter_department) {
        url += '&filter_department=' + encodeURIComponent(filter_department);
    }

    //var filter_category = $('input[name=\'filter_category\']').attr('value');
    var filter_category = $('#filter_category').attr('value');
    
    if (filter_category) {
        url += '&filter_category=' + encodeURIComponent(filter_category);
    }
    

    location = url;
}
function printpage(printdivname){
    //var newstr = document.getElementById(printdivname).innerHTML;
    /*var oldstr = document.body.innerHTML;*/
    //document.body.innerHTML = newstr;
    $(".header").hide();
    $(".page-sidebar-wrapper").hide();
    $(".page-bar").hide();
    $(".hideprint").hide();
    $(".printpage").hide();
    window.print();
    //document.body.innerHTML = oldstr;
    $(".header").show();
    $(".page-sidebar-wrapper").show();
    $(".page-bar").show();
    $(".hideprint").show();
    $(".printpage").show();
    //return true;
}

//--></script> 
<?php echo $footer; ?>  <script type="text/javascript"><!--
$('#form input').keydown(function(e) {
    if (e.keyCode == 13) {
        filter();
    }
});

function getkeyCode(){
    $(document).keydown(function(e){
        if (e.keyCode == 37) { 
           alert( "left pressed" );
           return false;
        }
    });
}
/*
var chosen = "";
$('#filter_name').keydown(function(e) {
    if (e.keyCode == 40) { 
        if(chosen === "") {
            chosen = 0;
        } else if((chosen+1) < $('ul#ui-autocomplete li').length) {
            chosen++; 
        }
        $('ul#ui-autocomplete li').removeClass('selected');
        $('ul#ui-autocomplete li:eq('+chosen+')').addClass('selected');
        return false;
    }
    if (e.keyCode == 38) { 
        if(chosen === "") {
            chosen = 0;
        } else if(chosen > 0) {
            chosen--;            
        }
        $('ul#ui-autocomplete li').removeClass('selected');
        $('ul#ui-autocomplete li:eq('+chosen+')').addClass('selected');
        return false;
    }
});
*/


// $('input[name=\'filter_name\']').autocomplete({
//     'source': function(request, response) {
//         $.ajax({
//             url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
//             dataType: 'html',
//             success: function(str) {
//                 if(str){
//                     $("#suggesstion-box").show();
//                     $("#suggesstion-box").html(str);

//                 }else{
//                     $("#suggesstion-box").hide();
//                     $("#suggesstion-box").html('');
//                 }
//             }
//         });
//     }
    
// });

function getProductautoFill(sku) {
    if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }
    var ajaxData = 'sku='+sku;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_request/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
               if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    selectedProduct(result);
                }
            }
            $("#pre-loader-icon").hide();
        }
    });
}

function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
}

function selectedProduct(val) {
    var newVal = val.split("||");
    $("#product_id").val(newVal[0]);
    $("#sku").val(newVal[1]);
    $("#filter_name").val(newVal[2]);
    $("#suggesstion-box").hide();
}

function getValue(strhtml){
    // $('#filter_name').val(strhtml)
    $('#suggesstion-box').hide();
}
</script> 