<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID" class="form" id="purchase_form">
        <?php if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php endif; ?>
        <div class="page-content" >
            <h3 class="page-title">Purchase Order</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <?php if (!isset($purchase_id)) { ?>
                   <button class="btn btn-update btn-warning" type="submit" name="purchase_button" value="hold" title="Hold" style="margin: 5px 5px 0 0; border-radius: 4px" id="btn_hold"><i class="fa fa-ban"></i> <?php echo $text_hold; ?></button>
                    <?php } ?>                    
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save" id="btn_save"></i> <?php echo $button_save; ?></button>

                <?php if($convert_invoice){ ?>
                <a href="<?php echo $convert_invoice; ?>" onclick="return confirm('Do you want to convert this purchase as Purchase Invoice');"><button type="button" class="btn btn-info" style="margin: 5px 5px 0 0; border-radius: 4px">Convert Invoice </button></a>
                <?php } ?>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" id="cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
        <div class="col-md-12">         
            <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Purchase Order Number<span class="required">*</span></label>
                            <?php if ($error_transaction_no) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Number"></i>
                                <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter Your Transaction Number" readonly="readonly">                                        
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_no) { ?>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Number" readonly="readonly">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Purchase Order Date<span class="required">*</span></label>
                            <?php if ($error_transaction_date) { ?>
                                <div class="input-icon right div_error">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Date"></i>
                                <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class=" textbox requiredborder purchase_inputfields_error" readonly=''>
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_date) { ?>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="textbox purchase_inputfields" readonly=''>
                            <?php } ?>
                        </td>                        
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor; ?></label>
                           <select name="vendor" id="vendor_code" onchange="getVendorProductDetails(this)">
                            <option value="">-- Select Vendor --</option>
                            <?php if(!empty($vendor_collection)): ?>
                                <?php foreach($vendor_collection as $vendordetails): ?>
                                    <option class="<?php echo $vendordetails['gst'].'||'.$vendordetails['tax_method']; ?>" value="<?php echo $vendordetails['vendor_id']; ?>" 
                                            label="<?php echo $vendordetails['vendor_name'];?>"
                                     <?php if($vendordetails['vendor_id']==$vendor):?> selected="selected" <?php endif; ?>>
                                        <?php echo $vendordetails['vendor_name']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                         <input type="hidden" name="vendor_allow_gst" id="vendor_allow_gst" value="">
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor_name; ?></label>
                            <input type="text" name="vendor_name" id="vendor_name" value="<?php echo $vendor_name; ?>" class="textbox purchase_inputfields" readonly="readonly" style="margin: 0 0 0 -4px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error">Customer</label>
                            <div>
                                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id; ?>">
                                <input type="text" name="customer_filter" id="customer_filter" class="textbox purchase_inputfields" autocomplete="off" placeholder="Search Customer">
                                <div id="customerSearch"></div>
                            </div>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label">Shipping Address</label>
                            <select name="shipping_id" id="shipping_id">
                                <option value="">Select Shipping</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <input type="hidden" name="pm" id="pm" value="<?php echo PURCHASE_MARGIN;?>">
                           <label class="purchase_label_error">Reference No</label>
                           <input type="text" name="reference_no" placeholder="Reference No" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields">
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error">Arrival Date From</label>
                            <input type="text" name="arrival_date_from" id="arrival_date_from" class="date" value="<?php echo $arrival_date_from; ?>" readonly>To:
                            <input type="text" name="arrival_date_to" id="arrival_date_to" class="date" value="<?php echo $arrival_date_to; ?>" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                           <label class="purchase_label" for="currency_code">Currency Code</label>
                           <select name="currency_code" class="" id="currency_code" onchange="checkCompanyCurrency();"<?php if ($purchase_id > 0) { echo "disabled"; } ?>>
                            <option value="">-- Select Currency --</option>                            
                            <?php if(!empty($currencys)){
                                foreach ($currencys as $value) { ?>
                                <option value="<?php echo $value['code']; ?>" <?php if($value['code']==$currency_code){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error" for="name">Tax Class<span class="required">*</span></label>
                            <select name="tax_class_id" class="" id="tax_class_id">
                            <?php if(!empty($tax_classes)){
                                foreach ($tax_classes as $value) { ?>
                                <option value="<?php echo $value['tax_class_id']; ?>" <?php if($value['tax_class_id'] == $tax_class_id){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                            <select name="tax_type" id="tax_type">
                                <option value="1" <?php if($tax_type=='1'){ echo "Selected"; } ?>> Exclusive </option>
                                <option value="2" <?php if($tax_type=='2'){ echo "Selected"; } ?>> Inclusive </option>
                           </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name" id="ConversionRate">Conversion Rate</label>
                            <input type="text" name="conversion_rate" id="conversion_rate" value="<?php echo $conversion_rate; ?>" class="textbox purchase_inputfields" placeholder="Enter Conversion Rate" <?php if ($purchase_id > 0) { echo "disabled"; } ?>>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="term">Term</label>
                            <select name="term_id" id="term_id" onchange="changeRefDate();">
                                <option value="">Select Term</option>
                                <?php foreach ($allTerms as $term) { ?>
                                    <option data-days="<?php echo $term['noof_days']; ?>" value="<?php echo $term['terms_id']; ?>" <?php if($term['terms_id']==$term_id) { echo "selected"; } ?> ><?php echo $term['terms_name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error" for="name">Attachment</label>
                                <input type="file" name="attachment" id="attachment" class="textbox purchase_inputfields fileUpload">
                                <a href="<?php echo HTTP_SERVER; ?>/uploads/PurchaseAttachment/<?php echo $attachment; ?>"  class="btn btn-zone btn-primary fileDisplay" target="_blank"> Preview </a>
                                <a id="removeAttachment" class="btn btn-zone btn-danger fileDisplay"><i class="fa fa-times"></i> Remove</a>
                        </td>
                        <td class="purchase_extra terms_idorder-nopadding"></td>
                        <td class="order-nopadding">
                        <input type="hidden" id="location_code" name="location_code" value="HQ">
                            <label class="purchase_label" for="remarks">Remarks</label>
                            <input type="text" name="remarks" id="remarks" value="<?php echo $remarks; ?>" class="textbox purchase_inputfields" style="margin: 0 0 0 -4px;">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
                <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">S.No</td>
                            <td class="center">Product SKU</td>
                            <td class="center">Product Name</td>
                            <td class="center">Description</td>
                            <td class="center">Qty</td>
                            <td class="center">FOC Qty</td>
                            <td class="center">Unit Cost</td>
                            <td class="center">Net Price</td>
                            <td class="center">Action</td>
                        </tr>
                    </thead>                      
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) { $i=1;
                                $totalTax = 0; ?>
                                <?php foreach ($cartPurchaseDetails as $purchase) {  // ?>
                                <?php if($apply_tax_type=='2') {
                                    $totalValue = round($purchase['total'],2);
                                } else {
                                     $totalValue = round($purchase['tax'] + $purchase['total'],2);
                                }
                                $totalTax+=$purchase['tax'];
                            ?>
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td><?php echo $i++; ?></td>    
                            <td class="center"><?php echo $purchase['code']; ?></td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td class="center">
                                <textarea class="textbox update_qty description" name="description" rows = "2" cols ="60" id="desc_<?php echo $purchase['product_id']; ?>" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>"><?php echo str_replace('<br />', '', nl2br($purchase['description']));  ?></textarea>
                            </td>
                            <td class="order-nopadding">
                                <input type="text" id="qty_<?php echo $purchase['product_id']; ?>" value="<?php echo $purchase['quantity']; ?>" class="textbox small_box update_qty" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>" style="direction: rtl;">
                            </td>
                            <td class="right"><?php echo $purchase['foc_quantity']; ?></td>
                            <td class="order-nopadding">
                                <?php $purchase['price'] = str_replace("$","",$purchase['price']); ?>
                                <input type="text" id="price_<?php echo $purchase['product_id']; ?>"  value="<?php echo str_replace(",","",$purchase['price']); ?>" class="textbox small_box update_price" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>" style="direction: rtl;">
                            </td>
                            <!-- <td class="center"><?php echo $purchase['sku_qty']; ?></td> -->
                            <td class="text-right" id="subTot_<?php echo $purchase['product_id']; ?>" data-discnt="<?php echo $purchase['discount']; ?>">
                                <?php echo $purchase['sub_total']; ?>
                            </td>
                            <!-- <td class="text-right"><?php if (($purchase['discount_mode'] == '2') && !empty($purchase['discount'])): echo $purchase['discount']; endif; ?></td> -->
                            
                            <td class="center"><a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="13" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0;display: none;">
                 <?php if (empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div>
                <div class="innerpage-listcontent-blocks">      
                    <table id="addproductTbl" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">Product SKU</td>
                            <td class="center">Product Name</td>
                            <td class="center">Description</td>
                            <td class="center">UOM</td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center">FOC Qty</td>
                            <td class="center">Unit Cost</td>
                            <td class="center"><?php echo $text_total; ?></td>
                        </tr>
                    </thead>                      

                        <tbody>
                            <tr>
                                <td class="insertproduct">
                                    <input type="hidden" id="hasChild" value="<?php echo $rowdata['hasChild'];?>" name="hasChild">
                                    <input type="text" name="sku" id="sku" class="textbox small_box" tabindex="1"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>onkeyup="getProductautoFill(this.value);" autocomplete="off">
                                    <input type="hidden" name="product_id" id="product_id"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
                                   <div id="suggesstion-box"></div>
                                </td>
                                <td class="insertproduct" style="width: 20%;">
                                    <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" tabindex="2" />
                                </td>
                                <td class="insertproduct" style="width: 20%;">
                                    <textarea class="textbox description" name="description" id="description" rows = "2" cols ="60"><?php if (!empty($rowdata)){ echo nl2br($purchase['description']); } ?></textarea>
                                    <!-- <input type="text" name="description" id="description" value="<?php if (!empty($rowdata)){ echo $rowdata['description']; } ?>" class="textbox small_box" /> -->
                                </td>
                                <td class="insertproduct" style="padding: 8px !important;">
                                    <input type="text" name="uom" id="uom" value="<?php if (!empty($rowdata)){ echo $rowdata['uom']; } ?>" class="textbox small_box" tabindex="3" readonly >
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);" tabindex="4">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="foc_quantity" id="foc_quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['foc_quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);" tabindex="5">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="textbox small_box" tabindex="6">
                                </td>
                                <!-- <td class="insertproduct">
                                    <input type="text" name="sku_qty" id="sku_qty" value="<?php if (!empty($rowdata)){ echo $rowdata['sku_qty']; } ?>" class="textbox small_box" readonly tabindex="8">
                                </td> -->
                                <td class="insertproduct">
                                    <input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="textbox row_total_box" onblur="changeunitCost(this.value);" tabindex="11">
                                </td>   
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="insertproduct purchase_add_button" colspan="8" align="right" style="padding: 10px 0 10px 0 !important;">
                                    <a onclick="addToPurchase();" class="btn btn-zone btn-primary" tabindex="12" id="addItem"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                                    <a onclick="clearPurchaseData();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                                    
                                </td>
                            </tr>
                            <?php $i = 0; ?>
                            <?php if(!empty($purchase_totals)) { ?>
                            <?php foreach ($purchase_totals as $total) { ?>
                                <?php if ($i == 0) { ?>
                                    <tr id ="TR<?php echo $total['code'];?>">
                                        
                                        <td class="insertproduct heading-purchase">Bill Disc(%)</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="bill_discount_percentage" id="bill_discount_percentage"
                                                value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>"
                                                onkeyup="updateBillDiscount('0');" class="textbox small_box"
                                                <?php if(!empty($hide_bill_discount_percentage)) { ?> readonly="readonly" <?php } ?>>
                                        </td>
                                        <td class="insertproduct heading-purchase">Bill Disc($)</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box"
                                            <?php if(!empty($hide_bill_discount_price)) { ?> readonly="readonly" <?php } ?>>
                                        </td>
                                        <td class="insertproduct heading-purchase">Delivery Fees($)</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="handling_fee" id="handling_fee" value="<?php echo $handling_fee; ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box">
                                        </td>
                                        <td align="right" class="heading-purchase" style="text-align: right;">
                                           <?php echo $total['title']; ?>
                                        </td>
                                        <td colspan="1" align="right" class="purchase_total_left insertproduct">
                                            <input type="text" name="<?php echo $total['code']; ?>"
                                                id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" 
                                                readonly="readonly" style="text-align:right;"/>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ($total['code'] == 'sub_total') { ?>
                                    <input type="hidden" name="sub_total_value"  id="sub_total_value" value="<?php echo $total['value'] ?>" />
                                <?php } ?>
                                 <?php if ($total['code'] == 'discount') { ?>
                                <tr id="TRdiscount">
                                    <td class="purchase_total_left" colspan="8" align="right"><?php echo $total['title']; ?></td>
                                    <td class="purchase_total_right insertproduct" colspan="1">
                                        <input readonly="readonly" class="textbox row_total_box" value="<?php echo $total['text'] ?>" id="discount" 
                                        name="discount" type="text" style="text-align: right;">
                                    </td>
                                 </tr>
                             <?php }?>
                             <?php if($total['code'] == 'tax') { ?>   
                            <tr id="TRtax">
                                <td class="purchase_total_left" id="tax_title" colspan="7" align="right"><?php echo $total['title']; ?></td>
                                <td class="purchase_total_right insertproduct" colspan="1">
                                    <input readonly="readonly" class="textbox row_total_box" value="<?php echo $total['text'] ?>" id="tax" 
                                    name="tax" type="text" style="text-align: right;">
                                </td>
                             </tr>

                            <?php } if($total['code'] == 'handling'){
                                        $handling_fee = $total['text'];
                                    }
                                    if($total['code'] == 'total'){
                                        $total_value = $total['text'];
                                    }
                                    $i++; } ?>

                            <tr id="TRhandling_fee">
                                <td colspan="7" align="right" class="purchase_total_left">Delivery Fees($)</td>
                                <td class="purchase_total_right insertproduct" colspan="2">
                                    <input type="text" id="TOThandling_fee" value="<?php echo $handling_fee; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;">
                                </td>
                            </tr>
                            <tr id="TRtotal" style="background-color: rgb(244, 244, 248);">
                                <td colspan="7" align="right" class="purchase_total_left">Order Totals</td>
                                <td class="purchase_total_right insertproduct" colspan="2">
                                    <input type="text" name="total" id="total" value="<?php echo $total_value; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;">
                                </td>
                            </tr>
                            <?php } ?>
                        </tfoot> 
                    </table>
                    <table id="product" class="table orderlist invetorty-tab" style="margin-bottom:160px;">
                            <thead>
                                <tr><th colspan="2">Tag Sales Order</th></tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td align="center" style="width: 80%;">Sales Order</td>
                                    <td class="center">Action</td>
                                </tr>
                            </thead>
                            <?php $product_row = 0; ?>
                            <?php   if(!empty($salesOrders)){
                                    foreach ($salesOrders as $order) { ?>
                            <tbody id="product-row<?php echo $product_row; ?>">
                                <tr>
                                    <td class="center">
                                        <input type="text" value="<?php echo $order['order_no']; ?>" class="textbox" oninput="getSalesOrder(this.value,<?php echo $product_row; ?>);" id="invoice_no_text_<?php echo $product_row; ?>">
                                        <input type="hidden" value="<?php echo $order['order_no']; ?>" id="invoice_no_<?php echo $product_row; ?>" name="invoice_no[]" style="position: absolute;">
                                        <div id="suggesstion_<?php echo $product_row; ?>"></div>
                                    </td>
                                    <td class="center"><a onclick="$('#product-row<?php echo $product_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> Remove</a></td>
                                </tr>
                            </tbody>
                            <?php $product_row++; ?>
                            <?php } } ?>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td class="center"><a onclick="addProduct();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> Add Order</a></td>
                                </tr>
                            </tfoot>
                      </table>

                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</div>

<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<?php echo $footer; ?>
<script type="text/javascript">
$('.date').datepicker({
    dateFormat: 'dd/mm/yy',
    minDate: 0
});

$(document).ready(function() {
    GetTaxClassAndType();
    checkCompanyCurrency(); 
    updateTaxValues(); 
    $("#close").click(function(){
        $("#popup").fadeOut("fast");
    });
});

$(document).ready(function() {
    var fileName = '<?php echo $attachment; ?>';
    if (fileName != '') {
        $('.fileDisplay').show();
        $('.fileUpload').hide();
    } else {
        $('.fileDisplay').hide();
        $('.fileUpload').show();
    }
});

$("#searchproduct-key").click(function(){
    $('#popup').bPopup({
        contentContainer:'.payment-page',
        easing: 'easeOutBack',
        speed: 990,
        transition: 'slideDown',
        width:'660',
        height:'660',
        scrollBar:true,
        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->request->get["token"]; ?>'
    });
});
function addProductGrid(){
    $("#addProductLink").on("click",function(){
         var $this = $(this);
        $this.text('See More');
        tableBody = $('#addproductTbl').find("tbody"),
        trLast = tableBody.find("tr:last"),
        trNew = trLast.clone();
        trLast.after(trNew);
});
}
function addProductToForm(pid,sku,name,price,sku_qty){
    $('#sku').val(sku);
    $('#product_id').val(pid);
    $('#name').val(name);
    $('#price').val(price);
    $('#raw_cost').val(price);
    $('#sku_qty').val(sku_qty);
    removeAllOverlay();
}
function getProducts(val){
        var page =1;
        var ajaxData = "search_product_text=" + val + "&page=" + page;
        $("#ajaxloader").show();
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',
            data: ajaxData,
            success: function(result) {
                $("#ajaxloader").hide();
                if (result=='') {
                    //
                } else {
                    $('.payment-page').html(result);
                    var el = $("#searchKeynew").get(0);
                    var elemLen = el.value.length;
                    el.selectionStart = elemLen;
                    el.selectionEnd = elemLen;
                    el.focus();
                    return false;
                }
            }
        });
}

function removeAllOverlay() {
    $('.b-close').click();
}
function getVendorProductDetails(ele) {
        var option  = $("option:selected", ele).attr("class");
        var vnamefull = $('#vendor_code').find("option:selected").attr('label');
        var option = option.split("||");
        getVendorCurrency($('#vendor_code').val());
        getVendorTerm();

        if(option[0] >1){
            $("#vendor_allow_gst").val(1);
        }else{
            $("#vendor_allow_gst").val(0);
        }
        $("#vendor_name").val(vnamefull);
        if(option[0]!='0'){
            $('#tax_class_id').val('2').trigger('click',true);
        }else{
            $('#tax_class_id').val('1');
        }
        GetTaxClassAndType();
        if(option[1]=='2'){
            $('#tax_type').val('2').trigger('click',true);
        }else{
            $('#tax_type').val('1').trigger('click',true);
        }
}
function getVendorCurrency(vendor){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getVendorCurrency&token=<?php echo $token; ?>',
        data:{vendor:vendor},

        success:function(data){
            $('#currency_code').val(data).trigger('change',true);
        }
    });
}
function getVendorTerm(){
    var vendor = $('#vendor_code').val();
    if(vendor!=''){
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/getVendorTerm&token=<?php echo $token; ?>',
            data:{vendor:vendor},

            success:function(data){
                data = JSON.parse(data);
                if(data.status){
                    $('#term_id').val(data.term);
                }else{
                    $('#term_id').val('');
                }
            }
        });
    }
}


$('#tax_class_id').change(function(){
    GetTaxClassAndType();
    updateTaxValues();
});
$('#tax_type').change(function(){
    updateTaxValues();
});

function updateTaxValues(){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getOneCartDetail&token=<?php echo $token; ?>',
        success:function(data){
            $.each(JSON.parse(data), function(propName, propVal) {
                update_qty_price(propVal.id,propVal.code);
            });
        }
    });
}
function GetTaxClassAndType(){
    if($('#tax_class_id').val() !='1'){
        $('#tax_type').show();
    }else{
        $('#tax_type').hide();
    }
}
function getVendorProductDetailsxx() { // RAGU Hided for loaded issue fix and added new above this fn
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
                $("#pre-loader-icon").hide();
            }
        }
    });
}
function selectedProduct(val) {
        //alert(val);
        var newVal = val.split("||");
        var price = parseFloat(newVal[3]);
        var sku_qty = parseFloat(newVal[6]);
        $("#product_id").val(newVal[0]);
        $("#sku").val(newVal[1].replace("^","'").replace("!!",'"'));
        $("#name").val(newVal[2].replace("^","'").replace("!!",'"'));
        $("#price").val(price.toFixed(4));
        $("#uom").val(newVal[5]);
        $("#sku_qty").val(sku_qty.toFixed(2));
        $("#hasChild").val(newVal[7]);
        $("#suggesstion-box").hide();
        $("#quantity").focus();
    }
function getProductautoFill(sku) {
    if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }
    var location = $("#location_code").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;
    
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                    //$("#sku_qty").focus();
                }else{
                    selectedProduct(result);
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function getProductDetails() {
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
    //$('#weight_class_id').val('');
    //$('#tax_class_id').val('');
    $('#quantity').val('');
    $('#price').val('');
    $('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');
    $('#uom').val('');
    $('#hasChild').val('');
    $('#sku_qty').val('');
    $('#foc_quantity').val('');
    $('#description').val('');
}
function Selectlocation() {
    var sku = $('#sku').val();
   if(sku){
        alert("Please clear selected item");
         //clearPurchaseData();
        return false;
    }
}

function addToPurchase() {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    var vendor_id  = $('#vendor_code').val();
    var pm         = $('#pm').val();
    var quantity     = $('#quantity').val();
    var foc_quantity = $('#foc_quantity').val();
 
    if(vendor_id==''){
        alert("Please select Vendor before add Item");
        return false;
    }
    if(price==0){
        var con = confirm("Do you want to proceed with 0 price?");
        if(con==false){
            return false;
        }
    }
    if(quantity < '0' || quantity ==''){
        alert('Quantity must be greater then 0');
        return false;
    }
    if(quantity <= '0' && foc_quantity <= '0' ){
        alert('Please Enter More Then 1 Quantity');
        return false;
    }else{
        /*var pmcal = parseFloat(price)+(price * pm);
        if(pmcal>sprice){
            var cn = confirm("Your selling Price is less than margin price("+pmcal.toFixed(2)+") please update selling price");
            if(!cn){
                return false;
            }
        }*/
       
    }
    var quantity   = $('#quantity').val();
    if (product_id && price && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
            dataType: 'json',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    //jQuery('#page').html(result);
                    tableBody = $('#special').find("tbody"),
                    tableBody.empty().append(result['tbl_data']);
                    if(result['tax_str']!=''){
                        $('#TRtax').remove();
                        var newRow = result['tax_str'];
                        $("#TRtotal").before(newRow);
                    }
                    //alert(result['sub_total_value']);
                    //location.reload();
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    if(result['sub_total_value']=='0'){
                        //jQuery("#TRtax").remove();
                        $("#TRdiscount").remove();
                    }
                    if(result['tax_value']=='0'){
                        //jQuery("#TRtax").remove();
                    }
                    if(result['discount']=='0'){
                        $("#TRdiscount").remove();
                    }
                    $('#sku').focus();
                        // end
                }
                clearPurchaseData();
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
     // code for check vendor has gst value 1
    
}
function isInt(n) {
   return n % 1 === 0;
}
function isFloat(n){
    result = (n - Math.floor(n)) !== 0; 
    return result;
}
function changeunitCost(total){
     var quantity   = $('#quantity').val();
     var price   = $('#price').val();

     if(price>0){
        var uc = total / quantity;
        $('#price').val(uc.toFixed(4));
     }else if(quantity){
        var uc = total / quantity;
        $('#price').val(uc.toFixed(4));
     }
}
function updateRowData(fieldValue) {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    var quantity   = $('#quantity').val();
    var uom        = $('#uom').val();
    var sku_qty        = $('#sku_qty').val();
    if(uom!='KG' && isFloat(quantity)){
        alert("Not allowed Decimal quantity");
        $('#quantity').val('');
        $('#quantity').focus();
        //return false;        
    }
    if (fieldValue && product_id && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/AjaxupdateRowData&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    var row_total=result['rowdata']['sub_total'];
                    $('#row_total').val(row_total.toFixed(2));
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            //jQuery('#quantity').focus();
        }else if(raw_cost==''){
            //jQuery('#price').focus();
        }
    }
}
function updateRowDataxx(fieldValue) { // RAGU Hided for loaded issue fix and added new above this fn
    var product_id = $('#product_id').val();
    var price   = $('#price').val();
    var quantity   = $('#quantity').val();
    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
}
function removeProductData(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }
            }
        }
    });
}
function removeProductDataxx(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                //window.location.reload();
                //jQuery('#page').html(result);
            }
        }
    });
}
function updateBillDiscount(fieldValue) {
    if (fieldValue) {
        
        if($('#bill_discount_percentage').val() > 99){
            $('#bill_discount_percentage').val('').focus();
        }

        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $("#TRdiscount").remove();
                    if(result['discountRow']){
                        $("#TRsub_total").after(result['discountRow']);
                    }
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    $('#TOThandling_fee').val(result['handling_fee']);
                    $('#bill_discount_percentage').val(result['discount_percentage']);
                    $('#bill_discount_price').val(result['discount_price']);
                }
                $("#pre-loader-icon").hide();
            }
        });
        // updateTaxValues();
    }
}
function updateBillDiscountxx(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}
</script>
<script type="text/javascript">

$(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});

$("#row_total").keyup(function(event) {
    if (event.keyCode === 13) {
        addToPurchase()
    }
});

$('html').click(function(e){
    if (e.target.id != 'country-list') {
        $("#suggesstion-box").hide(); 
    }
});

window.onload = function(e){ 
     var option  = $("#vendor_code option:selected").attr("class");
     if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            // $("#tax_class_id").val(listVal);
            // $("#vendor_allow_gst").val(1);
     }   
}
// var isSubmitting = false
// $(document).ready(function () {
//     $('.form').submit(function(){
//         isSubmitting = true
//     })
//     var initial_data = $(".form").serialize();
//     $(window).on('beforeunload', function() {
//         var convert_invoice = '<?php echo $convert_invoice ?>';
//         if(convert_invoice){
//             if (!isSubmitting && $('form').serialize() != initial_data ){
//                 return 'You have unsaved changes which will not be saved.'
//             }
//         }
//     });
// });

$('.form').submit(function(){

    if($('#vendor_code').val() ==''){
        alert('Please select Vendor');
        return false;
    }
    if($('#reference_no').val() ==''){
        alert('Please enter Ref No');
        return false;
    }
    if($('#currency_code').val() ==''){
        alert('Please Select Currency');
        return false;
    }
    if($('#ConversionRate').html() =='Conversion Rate<span class="required">*</span>' && $('#conversion_rate').val()==''){
        alert('Please Enter Conversion Rate');
        return false;
    }
     
});

$('.update_price, .update_qty').blur(function(){
    var product_id = $(this).attr("data-product_id");
    var code       = $(this).attr("data-code");
    update_qty_price(product_id,code);
});

function update_qty_price(id,sku){
    var qty                      = $('#qty_'+id).val();
    var price                    = $('#price_'+id).val();
    var description              = $('#desc_'+id).val();
    var tax                      = $('#tax_class_id').val();
    var tax_type                 = $('#tax_type').val();
    var bill_discount_percentage = $('#bill_discount_percentage').val();
    var bill_discount_price      = $('#bill_discount_price').val();

    $.ajax({
        type:'POST',
        url:'index.php?route=transaction/purchase/ajaxUpdateqtyAndPrice&token=<?php echo $token; ?>',
        data:{product_id:id,sku:sku,price:price,quantity:qty,tax_class_id:tax,tax_type:tax_type,bill_discount_percentage:bill_discount_percentage,bill_discount_price:bill_discount_price, description: description},

        success:function(data){
            var objData = JSON.parse(data);
            var sku     = objData.sku;
            $("td#subTot_"+sku).html(objData.netPrice);
            $("#sub_total").val(objData.orderSubTotal);
            if(objData.ordertax_value > 0){
                $('#TRtax').remove();
                $("#TRtotal").before(objData.tax_str);
            }else{
                $('#TRtax').remove();
            }
            console.log(objData.tax_type);
            if(objData.tax_type == '1'){
                $('#tax_title').html('GST (Excl)');
            }else{
                $('#tax_title').html('GST (Incl)');
            }
            $("#total").val(objData.orderTotal);
        }
    });
}
function checkCompanyCurrency(){
    var currency = $('#currency_code').val();

    if(currency!=''){
        $.ajax({
            type:'POST',
            url:'index.php?route=transaction/purchase/checkCompanyCurrency&token=<?php echo $token; ?>',
            data:{currency:currency},

            success:function(data){
                if(data){
                    $("#ConversionRate").html('Conversion Rate');
                    $('#conversion_rate').val('1');
                    $("#conversion_rate").prop("readonly", true);
                }else{
                    $("#ConversionRate").html('Conversion Rate<span class="required">*</span>');
                    $("#conversion_rate").prop("readonly", false);
                }
            }
        });   
    }
}

function changeRefDate(){
    days = $('select[name=term_id]').find(':selected').attr('data-days');
    if(days != undefined && days !='0'){
        var myDate = new Date(new Date().getTime()+(days*24*60*60*1000));
        var month  = myDate.getMonth() + 1;
        $('#arrival_date_from').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
        date = myDate.getDate() + 1;
        $('#arrival_date_to').val(date+'/'+month+'/'+myDate.getFullYear());
    }else{
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        $('#arrival_date_to').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
        $('#arrival_date_from').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
    }
}

$("#removeAttachment").click(function(){
    var fileName = '<?php echo $attachment; ?>';
    $.ajax({
        url:"index.php?route=transaction/purchase/removeAttachment&token=<?php echo $token; ?>",
        type:"POST",
        data:{
            fileName    : fileName,
            purchase_id : $("#purchase_id").val()
        },
        success:function(out){
            $('.fileDisplay').hide();
            $('.fileUpload').show();
        }
    });
});


    var product_row = <?php echo $product_row; ?>;
    function addProduct() {
        html  = '<tbody id="product-row' + product_row + '">';
        html += '  <tr>'; 
        html += '    <td class="center"><input class="textbox" type="text" id="invoice_no_text_'+product_row+'" placeholder="Search Order" oninput="getSalesOrder(this.value,'+product_row+');" autocomplete="off" autofocus><input type="hidden" id="invoice_no_'+product_row+'" name="invoice_no[]"><div id="suggesstion_'+product_row+'" style="position: absolute;"></div></td>';
        html += '    <td class="center"><a onclick="$(\'#product-row' + product_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> Remove</a></td>';
        html += '  </tr>';
        html += '</tbody>';
        
        $('#product tfoot').before(html);
        $('#invoice_no_text_'+product_row).focus();
        product_row++;
    }
    function getSalesOrder(invoice_no, row){
        if(invoice_no =='' || SEARCH_CHAR_CNT > invoice_no.length ){
            return false;
        }
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/getSalesOrder&token=<?php echo $this->session->data["token"]; ?>',
            data: {invoice_no : invoice_no, row:row},

            success: function(m) {
                var datas = JSON.parse(m);
                if (datas.status) {
                    $("#suggesstion_"+row).show();
                    $('#suggesstion_'+row).html(datas.data);
                } else {
                    alert(datas.data);
                    $("#invoice_no_text_"+row).val('');
                    $("#invoice_no_"+row).val(invoice_no);
                    $("#suggesstion_"+row).hide();
                }
            }
        });
    }
    function selectedOrder(invoice_no,name,row){
        $("#invoice_no_text_"+row).val(invoice_no);
        $("#invoice_no_"+row).val(invoice_no);
        $("#suggesstion_"+row).hide();
    }
    
    $("#customer_filter").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function( data ) {
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            $('#customer_id').val(ui.item.data_ref);
            getShippingAddress(ui.item.data_ref);
            return false;
        }
    });

    function getShippingAddress(vendor){
        var shipping_id = "<?php echo $shipping_id; ?>";
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/quotation/getShippingAddress&token=<?php echo $token; ?>',
            data:{vendor:vendor},

            success:function(data){
                $('#shipping_id').html(data);
                if(shipping_id !=''){
                    $("#shipping_id").val(shipping_id).trigger('change',true);
                }
            }
        });
    }

    $(document).ready(function(){
        var purchase_id = "<?php echo $purchase_id; ?>";
        if(purchase_id > 0){
            var customer_id = "<?php echo $customer_id; ?>";
            getCustomerDetails(customer_id);
            getShippingAddress(customer_id);
        }
    });

    function getCustomerDetails(cust){
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
            data:{cust:cust},

            success:function(data){
                data = JSON.parse(data);
                $("#customer_filter").val(data.name);
            }
        });
    }



</script>
<div id="ajax-modal" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Available Products</h4>
    </div>
    <div class="modal-body">
        Loading........
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>