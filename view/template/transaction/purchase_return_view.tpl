<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Purchase Return Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>  
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['transaction_no'] ?></td>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['transaction_date'])); ?></td>
                        </tr>
                       <tr>
                          <td>Vendor Code</td>
                          <td><?php echo $vendorDetail['vendor_code']; ?></td>
                          <td>Vendor Name</td>
                          <td><?php echo $vendorDetail['vendor_name']; ?></td>
                        </tr>
                        <tr>
                          <td>Currency Code</td>
                          <td><?php echo $purchaseInfo['currency_code']; ?></td>
                          <td>Currency Rate</td>
                          <td><?php echo $purchaseInfo['conversion_rate']; ?></td>
                        </tr>
                         <tr>
                          <td>Remark</td>
                          <td colspan="3"><?php echo $purchaseInfo['remarks']; ?></td>
                        </tr>
                        <tr>
                          <td>Created by</td>
                          <td><?php echo $purchaseInfo['created_by'].' at '.showDate($purchaseInfo['created_date']); ?></td>
                          <td>Modified by</td>
                          <td><?php echo $purchaseInfo['modified_by'].' at '.showDate($purchaseInfo['date_modified']); ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
                        <td class="center">Product Name</td>
                        <td class="center">Quantity</td>
                        <td class="center">Quantity(FOC)</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Total</td>
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1;  ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <?php 
                              //$lotno_details = unserialize($products['lotno_details']);
                            ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['name']; ?></td>
                              <td><?php echo $products['quantity']; ?></td>
                              <td><?php echo $products['foc']; ?></td>
                              <td class="text-right"><?php echo number_format($products['price'],4); ?></td>
                              <!-- <td><?php if($lotno_details[0]['LotNo']!=''){ echo $lotno_details[0]['LotNo']; } else { echo '-'; } ?></td>
                              <td><?php if($lotno_details[0]['ExpiryDate']!=''){ echo date('d/m/Y',strtotime($lotno_details[0]['ExpiryDate'])); } else { echo '-'; } ?></td> -->
                              <td class="text-right"><?php echo number_format($products['quantity'] * $products['price'],2); ?></td>
                            </tr>
                            <?php $quantity+= $products['quantity']; ?>
                            <?php $i++; } ?>
                            <!-- <tr>
                              <td colspan="6" align="right">Total</td>
                              <td class="text-right"><?php echo $quantity; ?></td>
                            </tr> -->
                          <?php } ?>

                          <tr>
                              <td colspan="6" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['sub_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="6" class="text-right">Discount</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['discount'],2); ?></td>
                            </tr>
                            <tr>
                              <?php $tax_type=$purchaseInfo['tax_type']=='1' ? 'Exclusive':'Inclusive';?>
                              <td colspan="6" class="text-right"><?php echo $purchaseInfo['tax_title'].' '.$tax_type; ?></td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['gst'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="6" class="text-right">Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="6" class="text-right">FCTotal</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['fc_nettotal'],2); ?></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
}
  
</script>
<?php echo $footer; ?>