<?php echo $header; ?>
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Payment Form</h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
         <div class="page-toolbar">
           <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0 !important">
                <button class="btn btn-primary" id="saveBtn" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</button></a>
            </div>
            </div>
          </div>
      </div>
      
        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption">
              <form method="POST">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                        <td>
                            <input type="hidden" name="filter_customer" id="filter_customer" value="<?php echo $filter_customer; ?>">
                           <input type="text" id="customer_name" class="textbox" placeholder="Search Customer">
                        </td>
                         <td>
                           <select name="filter_network" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Network</option>
                            <?php if(count($networks) > 0){
                              foreach ($networks as $value) {  ?>
                                <option value="<?php echo $value['id']; ?>" <?php if($value['id']==$filter_network){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                            <?php } } ?>
                            <?php if(!empty($locations)){
                                  foreach ($locations as $location) { ?>
                                    <option value="<?php echo $location['location_code']; ?>" <?php if($location['location_code'] ==   $filter_network ){ echo "selected"; } ?>><?php echo $location['location_name']; ?></option>
                            <?php } } ?>
                        </select>
                        </td>
                        <td><input type="hidden" name="page"> 
                          <input type="text" placeholder='Invoice Date' name="filter_invoice_date" value="<?php echo $filter_invoice_date; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td class="center" colspan="4">
                          <button style="width: 100%; min-height: 36px; border-radius:0 !important;"  class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>              
            </div>     
          </form>
        <div style="clear:both; margin: 0 0 15px 0;"></div>
      </div>
    </div>
    
    <div style="clear:both"></div>
      <?php if ($error_warning) { ?>
    <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>

    <div class="row">
      <div class="col-md-12">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
                  <table class="table orderlist statusstock" style="margin-top: 15px;">
                      <thead>
                        <tr>
                          <td colspan="3" class="right">Total</td>
                          <td style="width: 20%;"><span id="total_amount"></span></td>
                        </tr>
                        <tr>
                          <td style="width: 323px;">Payment Type : </td>
                          <td style="width: 323px;">
                            <select name="payment_type" id="payment_type" style="min-height: 25px; padding: 2px; width: 80%;">
                              <option value="">Select Payment Type</option>
                              <?php if(count($payments) > 0){
                                foreach ($payments as $value) {  ?>
                                  <option value="<?php echo $value['code']; ?>" <?php if($value['code']==$payment_type){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                              <?php } } ?>
                            </select>
                          </td>
                          <td>Is Paid</td>
                          <td><input type="checkbox" name="is_paid" id="is_paid" value="1"></td>
                        </tr>
                      </thead>
                  </table>
                    <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td width="1" style="text-align: center;"><input type="checkbox" id="select_all" name="select_all" /></td>
                        <td class="center">S.No</td>
                        <td class="center">Invoice No</td>
                        <td class="center">Invoice Date</td>
                        <td class="center">Reference No</td>
                        <td class="center">Customer</td>
                        <td class="center">Net Total</td>
                        <td class="center">Payment</td>
                        <td class="center">Balance Amount</td>
                      </tr>
                        </thead>
                        <tbody>
                    <?php if ($orders) { ?>
                    <?php $class = 'odd'; $i=1; ?>
                    <?php foreach ($orders as $order) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    
                    <tr class="<?php echo $class; ?>">  
                      <td style="text-align: center;">
                          <input type="checkbox" class="selecteds" name="selecteds['<?php echo $order['invoice_no']; ?>']" value="<?php echo $order['invoice_no']; ?>" data-ref="<?php echo $i-1; ?>" net_total="<?php echo $order['net_total']; ?>" id="selecteds<?php echo $i-1; ?>">
                      </td>
                      <td class="center"><?php echo $i; ?></td>
                      <td class="center"><?php echo $order['invoice_no']; ?></td>
                      <td class="center"><?php echo $order['invoice_date']; ?></td>
                      <td class="center"><?php echo $order['network_order_id']; ?></td>
                      <td class="center"><?php echo $order['cust_name']; ?></td>
                      <td class="center"><?php echo $order['net_total']; ?></td>
                      <td class="center"><!-- <?php /*echo $order['net_total'];*/ ?> -->
                        <input type="number" name="payment_amount['<?php echo $order['invoice_no']; ?>']" class="payment_amount" id="payment_amount_<?php echo $i-1; ?>" value="" net_total="<?php echo $order['net_total']; ?>" data-ref="<?php echo $i-1; ?>" tabindex="<?php echo $i; ?>">
                      </td>
                      <td class="center">
                        <input type="text" name="balance_amount" id="balance_amount_<?php echo $i-1; ?>" value="" readonly>
                      </td>
                    </tr>
          <?php $i++; } ?>
          <?php } else { ?>
          <tr>
            <td align="center" colspan="12"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
          </tbody>
            </table>
           </div>
           </form>
       <div class="pagination"><?php echo $pagination; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    
    $(document).ready(function(){
      $('#total_amount').html('0.00');
    
      var customer = "<?php echo $filter_customer; ?>";
      if(customer !=''){
          getCustomerDetails(customer);
      }
    });

  $("#customer_name").autocomplete({
      source: function( request, response ) {
          $.ajax({
              url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
              type: 'post',
              dataType: "json",
              data: {
                  search: request.term
              },
              success: function( data ) {
                  response(data);
              }
          });
      },
      select: function (event, ui) {
          $('#customer_name').val(ui.item.label);
          $('#filter_customer').val(ui.item.data_ref);
          return false;
      }
  });
  
  function getCustomerDetails(cust){
      $.ajax({
          type: "POST",
          url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
          data:{cust:cust},

          success:function(data){
            data = JSON.parse(data);
            $('#customer_name').val(data.name);
            $('#filter_customer').val(data.customercode);
          }
      });
  }

  $('#saveBtn').click(function(){
    if($('#payment_type').val() ==''){
        alert('Please select payment type');
        return false;
    }
    $('#form').submit();
  });

  $(".payment_amount").on('keyup',function(){
      var index= $(this).attr('data-ref');
      var payment_amount= parseFloat($(this).val());
      var net_total= parseFloat($(this).attr('net_total'));
      var payment_id = 'payment_amount_'+index;
      var balance_id = 'balance_amount_'+index;
      var selecteds = 'selecteds'+index;
      var balance_amount = net_total - payment_amount;
      if($('#'+selecteds).is(":checked")){
        if(payment_amount <= net_total){
          $('#'+balance_id).val(balance_amount);
          totalAmount();
        } else{
          $('#'+payment_id).val(net_total);
          $('#'+balance_id).val('0.00');
          alert('Payment value should be less than Net Total !');
          totalAmount();
        }
      }
  });

    function totalAmount() {
      var count = '<?php echo count($orders); ?>';
      var total = 0;
      for(var i=0; i<count; i++){
        var payment_id = 'payment_amount_'+i;
        if($('#'+payment_id).val() != ''){
          total += parseFloat($('#'+payment_id).val());
        }
      }
      total = total.toFixed(2);
      $('#total_amount').html(total);
    }

    $(".selecteds").click(function(){
        var index= $(this).attr('data-ref');
        var net_total= $(this).attr('net_total');
        var payment_id = 'payment_amount_'+index;
        var balance_id = 'balance_amount_'+index;
        if(this.checked){
          $('#'+payment_id).val(net_total);
          $('#'+balance_id).val('0.00');
        } else {
          $('#'+payment_id).val('');
          $('#'+balance_id).val('');
        }
        totalAmount();
    });

    $("#select_all").click(function(){
        if(this.checked){
            $('.selecteds').each(function(){
                if(!(this.checked)){
                    $(this).trigger('click');
                }
            });
        }else{
            $('.selecteds').each(function(){
                if(this.checked){
                    $(this).trigger('click');
                }
            });
          $('#total_amount').html('');
        }
    });
</script>