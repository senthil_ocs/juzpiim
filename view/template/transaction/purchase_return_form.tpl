<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID" class="form">
        <?php //if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php //endif; ?>
        <div class="page-content" >
            <h3 class="page-title">Purchase Return</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>                    
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>                                   
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" id="purchase_save_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" id="cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
            <div class="col-md-12">         
               <table class="table orderlist purchase_table">
                <tbody>
                    
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_no; ?><!-- <span class="required">*</span> --></label>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields"  placeholder="Enter Your Transaction Number">
                          
                        </td>
                        <td class="purchase_extra order-nopadding"></td>      
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_dt; ?></label>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="textbox purchase_inputfields" readonly>
                        </td>                                 
                    </tr>
                    <tr>
                        <?php if(isset($this->session->data['vendor_id'])){ $vandor_id = $this->session->data['vendor_id']; } ?>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor; ?></label>
                           <select name="vendor" id="vendor_code" onchange="getVendorProductDetails(this)">
                            <option value="">-- Select Vendor --</option>
                            <?php if(!empty($vendor_collection)): ?>
                                <?php foreach($vendor_collection as $vendordetails): ?>
                                    <option class="<?php echo $vendordetails['gst']; ?>" value="<?php echo $vendordetails['vendor_id']; ?>"
                                            label="<?php echo $vendordetails['vendor_name'];?>"
                                     <?php if($vendordetails['vendor_id']==$vendor):?> selected="selected" <?php endif; ?>>
                                        <?php echo $vendordetails['vendor_name'].'||'.$vendordetails['tax_method']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                         <input type="hidden" name="vendor_allow_gst" id="vendor_allow_gst" value="">
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor_name; ?></label>
                            <input type="text" name="vendor_name" id="vendor_name" value="<?php echo $vendor_name; ?>" class="textbox purchase_inputfields" readonly="readonly" style="margin: 0 0 0 -4px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refno; ?><!-- <span class="required">*</span> --></label>
                            <?php if(isset($this->session->data['reference_num'])){ $reference_no=$this->session->data['reference_num']; } ?>
                            <input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields" placeholder="Enter ref no" required onblur="getVendorProductDetails()">
                          
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refdate; ?><!-- <span class="required">*</span> --></label>
                            <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox purchase_inputfields" readonly>
                           
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                           <label class="purchase_label" for="name">Location</label>
                           <select name="location_code" id="location_code">
                             <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ 
                                        if($location_code!=$value['location_code']){
                                    ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if(trim($clocation_code) == trim($value['location_code']))echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php } } } ?> 
                        </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error" for="name">Tax Class<span class="required">*</span></label>
                            <select name="tax_class_id" class="" id="tax_class_id">
                            <?php if(!empty($tax_classes)){
                                foreach ($tax_classes as $value) { ?>
                                <option value="<?php echo $value['tax_class_id']; ?>" <?php if($value['tax_class_id']==$tax_class_id){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                            <select name="tax_type" id="tax_type">
                                <option value="2" <?php if($tax_type=='2'){ echo "Selected"; } ?>> Inclusive </option>               
                                <option value="1" <?php if($tax_type=='1'){ echo "Selected"; } ?>> Exclusive </option>
                           </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                           <label class="purchase_label" for="currency_code">Currency Code</label>
                           <select name="currency_code" class="" id="currency_code" onchange="checkCompanyCurrency();"<?php if ($purchase_id > 0) { echo "disabled"; } ?>>
                            <option value="">-- Select Currency --</option>                            
                            <?php if(!empty($currencys)){
                                foreach ($currencys as $value) { ?>
                                <option value="<?php echo $value['code']; ?>" <?php if($value['code']==$currency_code){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name" id="ConversionRate">Conversion Rate</label>
                            <input type="text" name="conversion_rate" id="conversion_rate" value="<?php echo $conversion_rate; ?>" class="textbox purchase_inputfields" placeholder="Enter Conversion Rate" <?php if ($purchase_id > 0) { echo "disabled"; } ?>>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="term_id">Term</label>
                            <select name="term_id" id="term_id" onchange="changeRefDate();">
                                <option value="">Select Term</option>
                                <?php foreach ($allTerms as $term) { ?>
                                    <option data-days="<?php echo $term['noof_days']; ?>" value="<?php echo $term['terms_id']; ?>" <?php if($term['terms_id']==$term_id) { echo "selected"; } ?> ><?php echo $term['terms_name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                    </tr>
                </tbody>
            </table>
                </div>
               <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">SKU</td>
                            <td class="center" style="width: 25%;"><?php echo $text_product_name; ?></td>
                            <td class="center" style="width: 10%;"><?php echo $text_qty; ?></td>
                            <td class="center" style="width: 10%;"><?php echo $text_price; ?></td>
                            <td class="center"><?php echo $text_net_price; ?></td>
                            <td class="center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>                      
                        <tbody>                        
                            <?php if (!empty($product)) {  ?>
                            <?php foreach ($product as $product) { ?>                                
                            <tr id="purchase_<?php echo $product['product_id']; ?>">
                                <td class="center"><?php echo $product['inventory_code']; ?></td>
                                <td class="center"><?php echo $product['productname']; ?></td>
                                <td class="center order-nopadding">
                                    <input type="text" id="qty_<?php echo $product['product_id']; ?>" value="<?php echo $product['quantity']; ?>" class="textbox small_box update_qty_price" data-product_id="<?php echo $product['product_id']; ?>" data-code="<?php echo $product['code']; ?>">
                                </td>
                                <td class="center order-nopadding">
                                <?php $product['price'] = str_replace("$","",$product['price']); ?>
                                <input type="text" id="price_<?php echo $product['product_id']; ?>"  value="<?php echo str_replace(",","",$product['price']); ?>" class="textbox small_box update_qty_price" data-product_id="<?php echo $product['product_id']; ?>" data-code="<?php echo $product['code']; ?>">
                                </td>
                                <td class="center" id="td_nettot_<?php echo $product['product_id']; ?>"><?php echo $product['sub_total']; ?></td>
                                <td class="center"><a onclick="removeProductData(<?php echo $product['product_id']; ?>);"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td colspan="10" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">                 
                <div class="innerpage-listcontent-blocks"> 
                   <!-- <?php if(!empty($ptransaction_no)) { ?> 
                    <div id="purchase_return_add_form"></div>
                    <?php } else { ?> -->
                     <table id="addproductTbl" class="table orderlist statusstock">
    <thead>
        <tr class="heading-purchase">
            <td class="center">SKU</td>
            <td class="center" colspan="2"><?php echo $text_product_name; ?></td>
            <td class="center"><?php echo $text_return_qty; ?></td>
            <td class="center"><?php echo $text_price; ?></td>
            <td class="center" colspan="2"><?php echo $text_total; ?></td>
        </tr>
    </thead>                    
    <?php //if (!empty($products)) { ?>
    <tbody>
        <tr>
            <td class="insertproduct">
                <input type="text" name="sku" id="sku" class="textbox"
                <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>
                            onkeyup="getProductautoFill(this.value);" autocomplete="off">
                <input type="hidden" name="product_id" id="product_id"
                <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
               <div id="suggesstion-box"></div>
            </td>
            <td class="insertproduct" colspan="2">
                <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox" readonly="readonly" />
            </td>
            <td class="insertproduct">
            <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
            </td>
            <td class="insertproduct">
               <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="textbox small_price_box">
            </td>

            <td class="insertproduct" colspan="2">
                <input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="textbox row_total_box" readonly="readonly">
            </td>   
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td class="insertproduct purchase_add_button" colspan="7" align="right" style="padding: 10px 0 10px 0 !important;">
                <a onclick="clearPurchaseData();" id="clear" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
            </td>
        </tr>
        <tr>
            <td class="insertproduct heading-purchase">Remarks</td>
            <td class="insertproduct heading-purchase" colspan="6">
                <input type="text" name="remarks" id="remarks" value="<?php echo $remarks; ?>" class="textbox remarks_box" placeholder="Enter Remarks">
            </td>
        </tr>
        <?php $i = 0; ?>
        <?php if(!empty($purchase_totals)) { 
        $discount = 0;
        if(isset($this->session->data['bill_discount']) && isset($this->session->data['bill_discount_mode']) ){
            if($this->session->data['bill_discount_mode'] =='1'){
                $bill_discount_percentage = $this->session->data['bill_discount'];
            }else{
                $bill_discount_price = $this->session->data['bill_discount'];
            }
        }
        ?>
        <?php foreach ($purchase_totals as $total) { ?>
        <?php if ($i == 0) { ?>
        <tr id ="TR<?php echo $total['code'];?>">
            <td class="insertproduct heading-purchase" colspan="2">Bill Disc(%)</td>
            <td class="order-nopadding">
                <input type="text" name="bill_discount_percentage" id="bill_discount_percentage"
                    value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>"
                    onkeyup="updateBillDiscount('0');" class="textbox small_box"
                    <?php if(!empty($hide_bill_discount_percentage)) { ?> readonly="readonly" <?php } ?>>
            </td>
            <td class="insertproduct heading-purchase">Bill Disc($)</td>
            <td class="order-nopadding">
                <input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box"
                <?php if(!empty($hide_bill_discount_price)) { ?> readonly="readonly" <?php } ?>>
            </td>
            <td colspan="1" align="center" class="purchase_total_left insertproduct heading-purchase">
               <?php echo $total['title']; ?>
            </td>
            <td class="purchase_total_right insertproduct">
                <input type="text" name="<?php echo $total['code']; ?>"
                id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box text-right" readonly="readonly" />
            </td>
        </tr>
       
        <?php } ?>
        <?php if ($total['code'] == 'sub_total') { ?>
            <input type="hidden" name="sub_total_value"  id="sub_total_value" value="<?php echo $total['value'] ?>" /><?php } ?>
        <?php if($total['code']== 'tax'){ ?> 
           <tr id="TRtax">
                <td align="right" class="purchase_total_left" colspan="6">GST (7%) (Excl)</td>
                <td class="purchase_total_right insertproduct">
                    <input type="text" readonly="readonly" class="textbox row_total_box text-right" value="<?php echo $total['text']; ?>" id="tax" name="tax">
                </td>
            </tr>
        <?php } ?>

       <?php if($total['code']== 'discount'){  $discount = $total['value'] ?> 
           <tr id="TRdiscount">
                <td align="right" class="purchase_total_left" colspan="6">Discount</td>
                <td class="purchase_total_right insertproduct">
                    <input type="text" readonly="readonly" class="textbox row_total_box text-right" value="<?php echo $total['text']; ?>" id="discount" name="discount">
                </td>
            </tr>
        <?php } ?>

        <?php $i++; } ?>
        <tr id="TRtotal" style="background-color: rgb(244, 244, 248);">
            <td colspan="6" align="right" class="purchase_total_right">Order Totals</td>
            <td class="purchase_total_right insertproduct">
                <input type="text" name="total" id="total" value="<?php echo $total['text']; ?>" class="textbox row_total_box text-right" readonly="readonly">
            </td>
        </tr>
        <?php } ?>
    </tfoot> 
    <?php /*} else { ?>
    <tfoot>
        <tr>
            <td colspan="10" align="center" class="purchase_no_data"><?php echo $text_select_vendor; ?></td>
        </tr>
    </tfoot>
    <?php }*/ ?>
</table>
    <!-- <?php } ?> --> 

                </div>
                </div>
            </div>
            </div>
            </form>
  </div>
  </div>
  </div> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<?php echo $footer; ?>

<script type="text/javascript">
function getReturnProductDetails() {
    //$("#suggesstion-box").hide();
    var transNumber = $('#ptransaction_no').val();
    if (transNumber) {
        //$("#suggesstion-box").hide();
        //$("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase_return/getProductDetailsByTransNo&token=<?php echo $token; ?>',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    var output = JSON.parse(result);
                    $('#purchase_id').val(output.purchase_id);
                    $('#vendor_code').val(output.vendor_code);
                    $('#vendor').val(output.vendor);
                    $('#transaction_date').val(output.transaction_date);
                    $('#vendor_name').val(output.vendor_name);
                    $('#reference_no').val(output.reference_no);
                    $('#reference_date').val(output.reference_date);
                    callAddForm(ajaxData);
                    //$("#suggesstion-box").hide();
                    //$('#purchaseform').html(result);  
                }
                //$("#pre-loader-icon").hide();
            }
        });
    }
}
function callAddForm(ajaxData){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase_return/getAddForm&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                //$('#purchase_return_add_form').html(result);
                $('#addproductTbl').html(result)
                /*$(".hidetable").hide();*/
            }
            //$("#pre-loader-icon").hide();
        }
    });
    
}
function getProductautoFill(sku) {
    if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }
    var ajaxData = 'sku='+encodeURIComponent(sku);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    selectedProduct(result);
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}

function selectedProduct(val) {
    var newVal = val.split("||");
    $("#product_id").val(newVal[0]);
    $("#sku").val(newVal[1].replace("^","'").replace("!!",'"'));
    $("#name").val(newVal[2].replace("^","'").replace("!!",'"'));
    $("#price").val(newVal[3]);
    $('#quantity').val(0);
    $("#raw_cost").val(newVal[4]);
    $("#uom").val(newVal[5]);
    $("#suggesstion-box").hide();
    $('#quantity').focus();
}

function getProductDetails() {

    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase_return/getProductDetailsByProductId&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {

            } else {
                var output = JSON.parse(result);
                $('#name').val(output.rowdata.name);
                $('#weight_class_id').val(output.rowdata.weight_class_id);
                $('#quantity').val(0);
                $('#price').val(output.rowdata.price);
                $('#discount_percentage').val(output.rowdata.discount_percentage);
                $('#discount_price').val(output.rowdata.discount_price);
                $('#row_total').val(output.rowdata.total);
            }
        }
    });
}

function clearPurchaseData() {
    $('#sku').val('');
    $('#product_id').val('');
    $('#name').val('');
    $('#weight_class_id').val('');
    $('#quantity').val('');
    $('#total_quantity').val('');
    $('#price').val('');
    $('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');    
}

function addToPurchase() {
    var product_id  = $('#product_id').val();
    var price       = $('#price').val();
    var quantity    = $('#quantity').val();
    var tot_quantity    = $('#total_quantity').val();
    var vendor_id  = $('#vendor_code').val();
    if(vendor_id==''){
        alert("Please select Vendor before add Item");
        return false;
    }
    //$("#pre-loader-icon").show();
    if(parseInt(quantity)=='0'){
        alert("Return quantity should not 0");
        return false; 
    }
    if(parseInt(quantity)>parseInt(tot_quantity)){
        //alert("Please enter return quantity below or equal to quantity");
        return false;
    }
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase_return/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
        dataType: 'json',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                if(quantity>parseInt(result['available_stock'])){
                    alert("Required quantity not availablle");
                    return false;
                }
                tableBody = $('#special').find("tbody"),
                tableBody.empty().append(result['tbl_data']);
                if(result['tax_str']!=''){
                    $('#TRtax').remove();
                    var newRow = result['tax_str'];
                    $("#TRtotal").before(newRow);
                }
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    //jQuery("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax_value']=='0'){
                    //jQuery("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }
                $("#sku").focus();
            }
            clearPurchaseData();
            $("#pre-loader-icon").hide();
        }
    });
}

function activeTaxClassByVendor(){
    var option  = $('select[name="vendor"] :selected').attr('class')
    // console.log(option);
    if(parseInt(option)=='1'){
        var listVal = $('#tax_class_id option:eq(1)').val();
        $("#tax_class_id").val(listVal);
        $("#vendor_allow_gst").val(1);
    }else{
        $("#tax_class_id").val('');
         $("#vendor_allow_gst").val(0);
    } 
}
function updateRowData(fieldValue) {
    var product_id = $('#product_id').val();
    if (fieldValue && product_id) {
        //$("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase_return/updateRowCountData&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    var output = JSON.parse(result);
                    if(output.warning){
                        alert(output.warning);
                        $("#quantity").val(output.quantity);
                    } else {
                        $('#row_total').val(output.rowdata.sub_total);
                    }
                    //$('#purchaseform').html(result);  
                }
                //$("#pre-loader-icon").hide();
            }
        });
    }
}
function removeProductData(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase_return/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }
            }
            // clearPurchaseData();
            // $("#pre-loader-icon").hide();
        }
    });
}
function updateBillDiscount(fieldValue) {
    if (fieldValue) {
        //$("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase_return/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                var data = JSON.parse(result);
                $("#sub_total").val(data.sub_total);
                $("#discount").val(data.discount);
                $("#tax").val(data.tax);
                $("#total").val(data.total);
                $('#bill_discount_percentage').val(data.discount_percentage);
                $('#bill_discount_price').val(data.discount_price);
                $("#TRdiscount").hide();
                    if(data.discountRow){
                        $("#TRsub_total").after(data.discountRow);
                    }
                }
            }
        });
    }
}
</script>
<script type="text/javascript">
    /*jQuery("#price").ForceNumericOnly();
    jQuery("#quantity").ForceNumericOnly();
    jQuery("#discount_percentage").ForceNumericOnly();
    jQuery("#discount_price").ForceNumericOnly();
    jQuery("#bill_discount_percentage").ForceNumericOnly();
    jQuery("#bill_discount_price").ForceNumericOnly();*/
</script>
<script type="text/javascript">
<?php if ($error_warning) { ?>
    loadReturnProductDetails();
<?php } ?>
function loadReturnProductDetails() {
    var transNumber = $('#transaction_no').val();
    if (transNumber) {
        //$("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase_return/getProductDetailsByTransNo&token=<?php echo $token; ?>',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    var output = JSON.parse(result);
                    $('#purchase_id').val(output.purchase_id);
                    $('#vendor_code').val(output.vendor_code);
                    $('#vendor').val(output.vendor);
                    $('#transaction_date').val(output.transaction_date);
                    $('#vendor_name').val(output.vendor_name);
                    $('#reference_no').val(output.reference_no);
                    $('#reference_date').val(output.reference_date);
                    callAddForm(ajaxData);
                    //$('#purchaseform').html(result);  
                }
                //$("#pre-loader-icon").hide();
            }
        });
    }
}

$(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});
$('#expiry_date').datepicker({
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    changeYear: true,
    minDate: 0
    //yearRange: '1990:2016',
});

function getVendorProductDetails(ele) {
        var option  = $("option:selected", ele).attr("class");
        var vnamefull = $('#vendor_code').find("option:selected").attr('label');
        var vname = vnamefull.split("||");
        getVendorCurrency($('#vendor_code').val());
        getVendorTerm();

        if(option >1){
            $("#vendor_allow_gst").val(1);
        }else{
            $("#vendor_allow_gst").val(0);
        }
        $("#vendor_name").val(vnamefull);
        if(option[0]!='0'){
            $('#tax_class_id').val('2').trigger('click',true);
        }else{
            $('#tax_class_id').val('1');
        }
        GetTaxClassAndType();
        if(option[1]=='2'){
            $('#tax_type').val('2').trigger('click',true);
        }else{
            $('#tax_type').val('1').trigger('click',true);
        }
}
function getVendorTerm(){
    var vendor = $('#vendor_code').val();   
    if(vendor!=''){
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/getVendorTerm&token=<?php echo $token; ?>',
            data:{vendor:vendor},

            success:function(data){
                data = JSON.parse(data)
                if(data.status){
                    $('#term_id').val(data.term);
                }else{
                    $('#term_id').val('');
                }
            }
        });
    }
}
$(document).ready(function(){
    GetTaxClassAndType();
    changeRefDate();
})
$('#tax_class_id').change(function(){
    GetTaxClassAndType();
    updateTaxValues();
});
$('#tax_type').change(function(){
    updateTaxValues();
});

function GetTaxClassAndType(){
    if($('#tax_class_id').val() !='1'){
        $('#tax_type').show();
    }else{
        $('#tax_type').hide();
    }
}
function updateTaxValues(){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getOneCartDetail&token=<?php echo $token; ?>',
        success:function(data){
            console.log(data);
            $.each(JSON.parse(data), function(propName, propVal) {
                update_qty_price(propVal.id,propVal.code);
            });
        }
    });
}

$('.update_qty_price').blur(function(){
    var sku = $(this).attr("data-sku");
    var product_id = $(this).attr("data-product_id");
    var code       = $(this).attr("data-code");
    update_qty_price(product_id,code);
});

function update_qty_price(id,sku){

    var qty                      = $('#qty_'+id).val();
    var price                    = $('#price_'+id).val();
    var tax                      = $('#tax_class_id').val();
    var tax_type                 = $('#tax_type').val();
    var bill_discount_percentage = $('#bill_discount_percentage').val();
    var bill_discount_price      = $('#bill_discount_price').val();

    $.ajax({
        type:'POST',
        url:'index.php?route=transaction/purchase_return/ajaxUpdateqtyAndPrice&token=<?php echo $token; ?>',
        data:{product_id:id,sku:sku,price:price,quantity:qty,tax_class_id:tax,tax_type:tax_type,bill_discount_percentage:bill_discount_percentage,bill_discount_price:bill_discount_price},

        success:function(data){
            var objData = JSON.parse(data);
            console.log(objData);objData
            var sku     = objData.sku;
            $("td#td_nettot_"+sku).html(objData.netPrice);
            $("#sub_total").val(objData.orderSubTotal);
            if(objData.ordertax_value > 0){
                $('#TRtax').remove();
                $("#TRtotal").before(objData.tax_str);
            }else{
                $('#TRtax').remove();
            }
            console.log(objData.tax_type);
            if(objData.tax_type == '1'){
                $('#tax_title').html('GST (Excl)');
            }else{
                $('#tax_title').html('GST (Incl)');
            }
            $("#total").val(objData.orderTotal);
        }
    });
    // updateBillDiscount('0');
}

$('#formID').submit(function(){

    if($('#currency_code').val() ==''){
        alert('Please Select Currency');
        return false;
    }
    if($('#ConversionRate').html() =='Conversion Rate<span class="required">*</span>' && $('#conversion_rate').val()==''){
        alert('Please Enter Conversion Rate');
        return false;
    }
     
});
function checkCompanyCurrency(){
    var currency = $('#currency_code').val();

    if(currency!=''){
        $.ajax({
            type:'POST',
            url:'index.php?route=transaction/purchase/checkCompanyCurrency&token=<?php echo $token; ?>',
            data:{currency:currency},

            success:function(data){
                if(data){
                    $("#ConversionRate").html('Conversion Rate');
                    $('#conversion_rate').val('1');
                    $("#conversion_rate").prop("readonly", true);
                }else{
                    $("#ConversionRate").html('Conversion Rate<span class="required">*</span>');
                    $("#conversion_rate").prop("readonly", false);
                }
            }
        });   
    }
}
function getVendorCurrency(vendor){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getVendorCurrency&token=<?php echo $token; ?>',
        data:{vendor:vendor},

        success:function(data){
            $('#currency_code').val(data).trigger('change',true);
        }
    });
}
function changeRefDate(){
    days = $('select[name=term_id]').find(':selected').attr('data-days');
    if(days != undefined && days !='0'){
        var myDate = new Date(new Date().getTime()+(days*24*60*60*1000));
        var month = myDate.getMonth() + 1;
        $('#reference_date').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
    }else{
        var myDate = new Date();
        var month = myDate.getMonth() + 1;
        $('#reference_date').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
    }
}
</script>