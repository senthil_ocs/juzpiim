<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Auto PO</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                
                        </div>    
                    </div>
                </div>               						
      </div>
 <?php 

$filter_department='';
$filter_supplier  ='';
 
$filter_to_from   = date('d/m/yy'); 
$filter_to_date   = date('d/m/yy');

/*$filter_from_date =  $_POST['filter_from_date'];
$datenew=date('Y-m-d',strtotime($filter_from_date));

$date = str_replace('/', '-', $filter_from_date);*/

   

if(isset($_REQUEST['filter_from_date'])){
    $filter_from_date = $_REQUEST['filter_from_date'];
}
if(isset($_REQUEST['filter_to_date'])){
    $filter_to_date = $_REQUEST['filter_to_date'];
}
if(isset($_REQUEST['filter_department'])){
    $filter_department = $_REQUEST['filter_department'];
}
if(isset($_REQUEST['filter_supplier'])){
    $filter_supplier = $_REQUEST['filter_supplier'];
}
//echo $datenew;

?>
  <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="list_form_validation">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='from_date' name="filter_from_date" value="<?php echo $filter_from_date; ?>" class="textbox date" autocomplete="off">
                          <input type="hidden" name="token" value="<?php echo $_REQUEST['token']; ?>">
                        </td>                       
                        <td>
                          <input type="text" id="to_date" name="filter_to_date" value="<?php echo $filter_to_date; ?>" class="textbox date" autocomplete="off">
                        </td> 
                         <!-- <td>
                           <select name="filter_department" id="department" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Department --</option>
                        </select>
                        </td> -->
                        <td>
                          <select name="filter_supplier" id="supplier" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Supplier --</option>
                            <?php foreach ($autopo_vendor as $key) { ?>
                              <option value="<?php echo $key['vendor_code'];?>" <?php if($key['vendor_code']==$filter_supplier){ echo "Selected"; } ?>><?php
                              if($key['vendor_name'] !='' && isset($key['vendor_name'])){
                                    $key['vendor_code'] = $key['vendor_name'];
                              }
                              echo $key['vendor_code']; ?></option>
                            <?php } ?>
                        </select>
                        </td>                   
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </form>
         </div>                        
        </div> 
    <div style="clear:both; margin: 0 0 15px 0;"></div>
  </div>
  </div>

			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				              <table class="table orderlist statusstock">
                            <thead>
                              <tr class="heading">
                                <td class="center">S.No</td>
                                <td class="center">PO Request No</td>
                                <td class="center">Date</td>
                                <td class="center">Vendor</td>
                                <td class="center">Total lines</td>
                                <td class="center">Sub total</td>
                                <td class="center">GST</td>
                                <td class="center">Net Total</td>
                                <td class="center">Location</td>
                                <td class="center">Action</td>
                              </tr>
                            </thead>
                            <tbody>
                        <?php if(!empty($po_request_header)){    
                          $i=1;
                          foreach ($po_request_header as $po_request) { 
                            
                            if($po_request['vendor_name'] !='' && isset($po_request['vendor_name'])){
                                    $po_request['Vendor_Code'] = $po_request['vendor_name'];
                             }
                             ?>
                              <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $po_request['PO_Req_No']; ?></td>
                                <td><?php echo $po_request['PO_Req_Date']; ?></td>
                                <td><?php echo $po_request['Vendor_Code']; ?></td>
                                <td><?php echo $po_request['Total_Lines']; ?></td>
                                <td><?php echo $po_request['Subtotal']; ?></td>
                                <td><?php echo $po_request['Gst']; ?></td>
                                <td><?php echo $po_request['Nettotal']; ?></td>
                                <td><?php echo $po_request['location_code']; ?></td>
                                <td><a href="<?php echo $edit_url; ?>&po_reqno=<?php echo $po_request['PO_Req_No']; ?>">View</a></td>
                              </tr>
                        <?php $i++; } }else{ ?>
                              <tr>
                                <td colspan="10" align="center">No data found</td>
                              </tr>
                        <?php } ?>
                            </tbody>
                    </table>
                    </div>
                </form>
            <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$("#from_date,#to_date").datepicker({dateFormat: "d/m/yy"}).datepicker();

function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    $(".heading a").removeAttr("href");
  $(".printHide").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
    $(".printHide").show();
}

</script> 