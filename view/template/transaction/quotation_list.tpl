<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Quotation List</h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                  <?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>
        </ul>
      <div class="page-toolbar">
        <div class="btn-group pull-right">
          <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
            <?php if($this->user->hasPermission('modify', $route)) { ?>
               <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
              <?php } ?>
              <?php if ($hold) { ?>
              <a href="<?php echo $back_button; ?>"><button type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_back; ?></button></a>
              <?php } else { ?>
              <?php } ?>
              </div>
            </div>
          </div>
      </div>

        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td><input type="hidden" name="page"> 
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                
                         <td>
                           <select name="filter_location" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Sales Channel --</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filters['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox" placeholder="Transaction / Reference No">
                        </td>

                          <td class="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
      <div style="clear:both"></div>
        <?php if ($error_warning) { ?>
      <div class="alert alert-block alert-danger fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $error_warning; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
             </div>
      <?php } ?>
      <?php if ($success) { ?>
        <div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
          <button type="button" class="close" data-dismiss="alert"></button>
                </div>
      <?php } ?>
    <div class="row">
      <div class="col-md-12">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">

                    <table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr class="heading">
                      <!-- <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td> -->
                      <td class="center">S.No</td>
                      <td class="center"><?php echo $text_tran_no; ?></td>
                      <td class="center"><?php echo $text_tran_dt; ?></td>
                      <td class="center">Location</td>
                      <td class="center">Customer</td>
                      <td class="center"><?php echo $text_net_total; ?></td>
                      <td class="center printHide"><?php echo $column_action; ?></td>
                      </tr>
                        </thead>
                        <tbody>
                            <?php if ($purchases) { ?>
                            <?php $class = 'odd'; $i=1; ?>
          <?php foreach ($purchases as $purchase) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                      <tr class="<?php echo $class; ?>">
<!--                      <td style="text-align: center;">
                        <?php $checked = ''; if ($product['selected']) { 
                              $checked='checked="checked"'; 
                        }  ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $purchase['purchase_id']; ?>" <?php echo $checked; ?> >
                      </td> -->
                      <td class="center"><?php echo $purchase['pagecnt'] + $i; ?></td>
                      <td class="center"><?php echo $purchase['transaction_no']; ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['location_code']; ?></td>
                      <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="center"><?php echo $purchase['total']; ?></td>
                      
                      <td class="center printHide">
                      <?php if(!$purchase['issaled']){ ?>
                        <a href="<?php echo $purchase['modify_button'] ?>"><i class='fa fa-edit' data-toggle="tooltip" title="Modify"></i></a>
                      <?php } ?>
                      <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" data-toggle="tooltip" title="View"></i></a>

                      <?php if(!$purchase['issaled']){ ?>
                       <a href="<?php echo $purchase['delete_button']; ?>" onclick="return confirm('Are you confirm,Delete this!')";><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a>
                      <?php } ?>

                       <a href="<?php echo $purchase['download_button']; ?>"><i class="fa fa-print" aria-hidden="true" data-toggle="tooltip" title="Print" style="font-size: 16px;"></i></a>
                    </td>

          </tr>

          <?php $i++; } ?>
          <?php } else { ?>
          <tr>
            <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
          </tr>
          <?php } ?>
          </tbody>
            </table>
           </div>
           </form>
       <div class="pagination"><?php echo $pagination; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$(document).ready(function(){
    $('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [2,3,5,6],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
  });
});
</script>
<?php echo $footer; ?>
</script>