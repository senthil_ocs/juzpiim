<?php echo $header; ?>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<style type="text/css">
 .modal-scrollable{
    z-index: 10050 !important;
  }
</style>

<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Multiple Invoice DO</h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
                    
        </ul>
         <div class="page-toolbar">
           <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <?php if($purchaseInfo['payment_status'] != 'Delivered'){ ?>
                  <?php if(count($productDetails) > 0){ ?>
                <button type="submit" id="saveBtn" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-edit"></i> Save</button>
              <?php  } } ?>
              <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
              </div>    
              </div>
          </div>                          
      </div>
      <div style="clear:both"></div>
      <?php if ($warning) { ?>
        <div class="alert alert-block alert-success fade in setting-success" style="margin: 0px 0px 10px;">     <?php echo $warning; ?>
             <button type="button" class="close" data-dismiss="alert"></button>
        </div>
      <?php } ?>
      <div style="clear:both"></div>

    <div class="row">     
      <div class="col-md-12">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="multiple_do_form">
                <div class="innerpage-listcontent-blocks"> 
                  <input type="hidden" name="delivery_id" id="delivery_id" value="<?php echo $purchaseInfo['delivery_man_id']; ?>" >
                  <input type="text" name="shipping_id" id="shipping_id" value="<?php echo $shippingAddress['id']; ?>" hidden>
                  <input type="text" name="customer_id" id="customer_id" value="<?php echo $vendorDetail['customercode']; ?>" hidden>
                  <input type="text" name="sales_invoice_id" id="sales_invoice_id" value="<?php echo $purchaseInfo['id']; ?>" hidden>
                  <input type="text" name="sales_transaction_no" id="sales_transaction_no" value="<?php echo $purchaseInfo['invoice_no']; ?>" hidden>
                  <input type="hidden" name="total_invoice_items" value="<?php echo $total_invoice_items; ?>">
                  <table class="table orderlist statusstock">
                      <thead><!-- <?php /*printArray($purchaseInfo);die;*/ ?> -->
                        <tr>
                          <td style="width: 20%;">Transaction No</td>
                          <td style="width: 30%;"><?php echo $purchaseInfo['invoice_no'] ?></td>
                          <td style="width: 20%;">Transaction Date</td>
                          <td style="width: 30%;"><?php echo date('d/m/Y',strtotime($purchaseInfo['invoice_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Customer Code</td>
                          <td><?php echo $vendorDetail['cust_code']; ?></td>
                          <td>Customer Name</td>
                          <td><?php echo $vendorDetail['name']; ?></td>
                        </tr>
                        <tr>
                          <td>Shipping Code</td>
                          <td><?php echo $shippingAddress['shipping_code']; ?></td>
                          <td>Shipping Address</td>
                          <td><?php echo $shippingAddress['address1'].' '.$shippingAddress['address2'].' '.$shippingAddress['city'].' '.$shippingAddress['zip']; ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">DO No</td>
                        <td class="center">Customer Name</td>               
                        <td class="center">Delivery Man</td>
                        <td class="center">Delivery Date</td>
                        <td class="center">Status</td>
                        <td class="center">Action</td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(count($doDetails) > 0){ $i = 1;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($doHeaders as $products) { ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even');
                          if($products['assignedon'] !=''){
                            $products['assignedon'] = date('d/m/Y',strtotime($products['assignedon']));
                          }else{
                            $products['assignedon'] = '-';
                          }
                        ?>
                        <tr class="<?php echo $class; ?>">
                          <td class="center"><?php echo $i; ?></td>
                          <td><?php echo $products['do_no']; ?></td>
                          <td><?php echo $vendorDetail['name']; ?></td>
                          <td><?php echo $products['delivery_man_name']; ?></td>
                          <td><?php echo $products['assignedon']; ?></td>
                          <td><?php echo showText($products['status']); ?></td>
                          <td>
                            <?php if($products['status'] !='Delivered' && $products['status'] !='Canceled'){ ?>
                            <!-- <a id="edit_<?php echo $products['do_no']; ?>"><i class="fa fa-edit" aria-hidden="true" data-toggle="tooltip" title="Edit" onclick="updateDeliveryMan_Date('<?php echo $products['do_no']; ?>');"></i></a> -->
                            <a data-toggle="modal" data-target="#ajax-modal-qedit" data-backdrop="static" data-keyboard="false" id="openModel"></a>
                          <?php } ?>

                            <a href="<?php echo $download_button; ?>&do_no=<?php echo $products['do_no']; ?>"><i class="fa fa-print" data-toggle="tooltip" title="Download DO"></i></a>

                            <?php if($products['status'] == 'Delivered'){ ?>
                              <a href="#" data-image="<?php echo $products['signature']; ?>" class=" imagePopupBtn"  data-toggle="tooltip" title="View Signature">  <i class='fa fa-image' style="font-size:14px;"></i> </a>
                            <?php } ?>

                          </td>
                        </tr>
                        <?php $i++; } ?>
                      <?php }else{ ?>
                        <tr>
                          <td align="center" colspan="7">No Data!</td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                  <?php if(count($productDetails) > 0 && $purchaseInfo['delivery_status'] !='Canceled' ){ ?>
                  <table class="table orderlist statusstock">
                    <tbody>
                      <tr>
                        <td><label> Select Delivery Man : </label></td>
                        <td>
                          <select name="deliveryman_id" id="deliveryman_id">
                            <?php foreach ($salesman as $value) { ?>
                              <option value="<?php echo $value['id']; ?>" <?php if($purchaseInfo['delivery_man_id'] == $value['id']){ echo "selected"; } ?> ><?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <td><label> Delivery Date : </label></td>
                        <td>
                            <input type="text" placeholder='Delivery Date' name="delivery_date" value="<?php echo date('d/m/Y'); ?>" class="textbox date" autocomplete="off">
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td><input type="checkbox" id="select_all" name="select_all"/></td>
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
                        <td class="center">Product Name</td>
                        <td class="center">Quantity</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Action</td>
                      </tr>
                        </thead>
                        <tbody>
                          <?php $i = 1; 
                           ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <tr class="<?php echo $class; ?>">
                                  <td>
                                    <input type="checkbox" name="selected[]" value="<?php echo $products['product_id']; ?>" />
                                  </td>
                                  <td class="center"><?php echo $i; ?></td>
                                  <td><?php echo $products['sku']; ?></td>
                                  <td><?php echo $products['description']; ?></td>
                                  <td>
                                    <input type="number" name="quantity[<?php echo $products['product_id']; ?>]"<?php echo $i; ?> value="<?php echo $products['qty']-$products['do_qty']; ?>" onkeyup="validateQty('<?php echo $products['qty']-$products['do_qty']; ?>',this.value,'<?php echo $products['product_id']; ?>');" min="1" tabindex="<?php echo $i; ?>">
                                    <input type="hidden" name="real_quantity[<?php echo $products['product_id']; ?>]"<?php echo $i; ?> value="<?php echo $products['qty']; ?>">
                                    <input type="hidden" name="do_quantity[<?php echo $products['product_id']; ?>]"<?php echo $i; ?> value="<?php echo $products['do_qty']; ?>">
                                  </td>
                                  <td><?php echo $products['sku_price']; ?></td>
                                  <td>
                                    <?php if($purchaseInfo['delivery_status'] == 'Pending' || $purchaseInfo['delivery_status'] == 'Cancelled'){ echo "-"; } else if($products['do_qty'] ==0){ ?>
                                        <a><i class="fa fa-trash danger" aria-hidden="true" data-toggle="tooltip" title="Cancel Product" onclick="cancelPartially('<?php echo $products['product_id']; ?>');"></i></a>
                                        <?php if($products['tagged']){ ?>
                                        <a><i class="fa fa-unlock success" aria-hidden="true" data-toggle="tooltip" title="Untag" onclick="untagProduct('<?php echo $products['product_id']; ?>');"></i></a>
                                    <?php } } else{ echo "-"; } ?>
                                  </td>
                                </tr>
                            <?php $i++; } ?>
                        </tbody>
                      </table>
                          <?php } ?>
                    </div>
                 </form>
             </div>
          </div>
      </div>
  </div>
</div>

<div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Update Delivery for : <span id="popup_header"></span></h4>
    </div>
    <div class="modal-body">       
        <div class="form-body">
            <div id="qedit_error" class=""></div>
            <div class="form-group">
                <div class="row">     
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputsm">Select Delivery Man</label>
                      <select name="delivery_man" id="delivery_man" class="salesdropdown form-control">
                      <option value="">Select Delivery Man</option>
                      <?php
                          foreach($salesman as $salesman){ ?>
                            <option value="<?php echo $salesman['id'];?>"> <?php echo $salesman['name']; ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="inputsm">Delivery Date</label>
                      <input type="hidden" id="do_no">
                      <input type="text" id="delivery_date" name="delivery_date" class="form-control date" autocomplete="off">
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#" class="btn btn-primary" id="update_delivery">Save</a>   
      <a href="#" class="btn btn-danger" id="close_model" data-dismiss="modal">Close</a>   
    </div>
</div>

<div id="ajax-modal" class="modal fade modal-scroll in" tabindex="-1">
  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="form-body" id="imagePopup">
    </div>
    <div class="modal-footer">
      <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
  </div>
</div>

<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">

$('.date').datepicker({
  minDate : 0,
  dateFormat: 'dd/mm/yy'
});

$('.imagePopupBtn').click(function(){
  var image = $(this).attr("data-image");
  var path  = "<?php echo HTTPS_SERVER; ?>doc/signature/"+image;
  console.log("hi : "+path);
  $('#imagePopup').html('<img src="'+path+'" width="500" height="300">');
    $('#ajax-modal').modal('show');
});
function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}
$('.do_status').on('change', function(){
    var status = $(this).val();
    var product_id = $(this).attr('data-ref');
    var invoice_no = $(this).attr('data-invoice_no');
    $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/doStatusUpdate&token=<?php echo $this->session->data["token"]; ?>',
          data: {product_id: product_id,status:status,invoice_no:invoice_no},
          success: function(result) {
            alert('Status updated successfully!');
          }
      });
});

$("#saveBtn").click(function() {
    var deliveryman_id = $('#deliveryman_id').val();
    if(deliveryman_id > 0){
      $('.multiple_do_form').submit();
    } else {
      alert('Please Select Delivery Man!');
    }
});

function updateDeliveryMan_Date(do_no){
  $('#popup_header').html(do_no);
  
    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/getDeliveryManAndDate&token=<?php echo $this->session->data["token"]; ?>',
        data: {do_no: do_no},
        success: function(data) {
          data = JSON.parse(data);
          if(data.status){
              $('#delivery_man').val(data.delivery_man);
              $('#do_no').val(do_no);
              $('#delivery_date').val(data.delivery_date);
              $('#openModel').trigger('click',true);
          }
        }
    });
}

$('#update_delivery').click(function(){
    var do_no         = $('#do_no').val();
    var delivery_man  = $('#delivery_man').val();
    var delivery_date = $('#delivery_date').val();

    if(do_no){
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/updateDelivery&token=<?php echo $this->session->data["token"]; ?>',
          data: {do_no: do_no,delivery_man:delivery_man,delivery_date:delivery_date},
          success: function(data) {
            data = JSON.parse(data);
            if(data.status){
                location.reload();
            }
          }
      }); 
    }
});

function validateQty(qty, inputQty, id){
  if(parseInt(qty) < parseInt(inputQty)){
    $("input[name='quantity["+id+"]']").val(qty);
  }
  if(parseInt(inputQty) < 1){
    $("input[name='quantity["+id+"]']").val(qty);
  }
}

function cancelPartially(product_id){
    var invoice_no = '<?php echo $purchaseInfo['invoice_no']; ?>';
    var order_no   = '<?php echo $purchaseInfo['sales_trans_no']; ?>';
    if(!confirm('Are you sure, delete this?')){
      return false;
    }
    if(invoice_no !='' && product_id !='' ){
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/cancelPartiallInvoice&token=<?php echo $this->session->data["token"]; ?>',
            data: { 
              invoice_no : invoice_no, 
              product_id : product_id, 
              order_no : order_no
            },
            success: function(data) {
              data = JSON.parse(data);
              if(data.status){
                  location.reload();
              }else{
                alert(data.err);
              }
            }
        });
    }else{
      alert('Some details are wrong, try again!');
    }
}

function untagProduct(product_id){
    var invoice_no = '<?php echo $purchaseInfo['invoice_no']; ?>';
    var order_no   = '<?php echo $purchaseInfo['sales_trans_no']; ?>';
    if(!confirm('Are you sure, untag this?')){
      return false;
    }
    if(invoice_no !='' && product_id !='' ){
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/untagProduct&token=<?php echo $this->session->data["token"]; ?>',
            data: { 
              invoice_no : invoice_no, 
              product_id : product_id, 
              order_no : order_no
            },
            success: function(data) {
              data = JSON.parse(data);
              if(data.status){
                  location.reload();
              }else{
                alert(data.err);
              }
            }
        });
    }else{
      alert('Some details are wrong, try again!');
    }
}

</script>
<?php echo $footer; ?>