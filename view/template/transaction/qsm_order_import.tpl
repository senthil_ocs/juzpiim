<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
        <div class="page-content" >
            <h3 class="page-title">QSM Order Import</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($action !=''){  ?>
                                <a href="<?php echo $action; ?>"><button type="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> Submit</button></a>
                            <?php } if(!empty($orders)){ ?>
                                <a href="<?php echo $deleteAllBtn; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i> Delete All</button></a>

                                <a href="<?php echo $clearImportedOrders; ?>"><button type="button" class="btn btn-warning" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i> Skip All</button></a>
                            <?php } ?>
                            <a href="<?php echo $back; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</button></a>
                        </div>    
                    </div>
                </div>                            
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                     <?php if ($error) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error; ?>
                        </div>
                    <?php } ?>
                </div>
                
                <div style="clear:both"></div>
                <form method="post" action="<?php echo $fileUploadAction; ?>" id="qsmForm" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <table class="table orderlist">                       
                            <tbody>
                                <tr data-toggle="tooltip" title=".csv file only support">
                                    <td>
                                        <input type="file" name="file">
                                        <span style="font-size: 10px;"> .csv file only support</span> 
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary" <?php if(!empty($orders)) { echo "disabled"; }?> onclick="disableSubmit();" id="importBtn">Import</button>  
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>

            <div class="row">
            <div class="col-md-12">
                <div class="innerpage-listcontent-blocks">
                    <table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
                                <td>Sno</td>
                                <td>Customer</td>
                                <td>Order Number</td>
                                <td>Sku</td>
                                <td>Product ID</td>
                                <td>Name</td>
                                <td>Qty</td>
                                <td>UnitPrice</td>
                                <td>Total</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($orders)) { $m = 1;
                            foreach ($orders as $order) { ?>
                            <tr>
                                <td><?php echo $m; ?></td>
                                <td><?php echo $order['customerName']; ?></td>
                                <td><?php echo $order['orderNumber']; ?></td>
                                <td><?php echo $order['sellerSKU']; ?></td>
                                <td><?php echo $order['product_id']; ?></td>
                                <td><?php echo $order['itemName']; ?></td>
                                <td><?php echo $order['qty']; ?></td>
                                <td><?php echo $order['unitPrice']; ?></td>
                                <td><?php echo $order['total']; ?></td>
                                <td>
                                    <?php if(($order['product_id'] =='0' || $order['product_id'] =='') && $order['orderExist']=='0'){ ?>
                                        <a onclick="openMatchPopup('<?php echo $order['sellerSKU']; ?>','<?php echo $order['unitPrice']; ?>','<?php echo $order['orderID']; ?>');"><i class="fa fa-exchange" aria-hidden="true" data-toggle="tooltip" title="Match another product" style="font-size: 16px;"></i></a>
                                        <a data-toggle="modal" data-target="#ajax-modal-qedit" data-backdrop="static" data-keyboard="false" id="openModel"></a>

                                        <a href="<?php echo $order['createNewBtn']; ?>" target="_blank"><i class="fa fa-plus" aria-hidden="true" data-toggle="tooltip" title="Create new product" style="font-size: 16px;"></i></a>

                                    <?php } else if($order['orderExist'] =='1'){ ?>
                                         <a href="<?php echo $order['deleteBtn']; ?>"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Skip This Order" style="font-size: 16px;color: red;"></i></a>
                                    <?php }else { echo "-"; } ?>
                                </td>
                            </tr>
                            <?php $m++; } }else{ ?>
                            <tr>
                                <td colspan="10" align="center">Please import file!</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1" style="font-family: unset;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Match Product for : <span id="match_product_sku"></span></h4>
        </div>
        <div class="modal-body">       
            <div class="form-body">
                <div id="qedit_error" class=""></div>
                <div class="form-group">
                    <div class="row">     
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="inputsm">Search Product</label>
                          <input type="text" class="form-control" id="match_sku" name="match_sku" onkeyup="getProductautoFill(this.value);" autocomplete="off">
                          <input type="hidden" id="match_product_id">
                          <div id="suggesstion-box"></div>
                        </div>
                        <div class="form-group">
                          <label for="inputsm">Price</label>
                          <input type="text" class="form-control" name="match_price" id="match_price" readonly>
                          <input type="hidden" id="match_id">
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update_match_product">Update</button>
          <button type="button" class="btn btn-danger"  id="close_model" data-dismiss="modal">Cancel</button>
        </div>
    </div>

<?php echo $footer; ?>
<script type="text/javascript">
    function getProductautoFill(sku) {
        if(sku.length < SEARCH_CHAR_CNT){
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/lazada_order_import/getProductDetails&token=<?php echo $token; ?>',
            data: {filter_name : sku},
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                    return false;
                }
            }
        });
    }

    function selectedProduct(sku,id) {
        $("#match_sku").val(sku);
        $("#match_product_id").val(id)
        $("#suggesstion-box").hide();
    }

    function openMatchPopup(sku,price,id){
        $('#match_product_sku').html(sku);
        $("#match_sku").val('');
        $("#match_product_id").val('');
        $("#match_price").val(price);
        $("#match_id").val(id);
        $('#openModel').trigger('click',true);
        $("#match_sku").focus();
    }

    $(document).on('click','#update_match_product',function(){
        if($("#match_product_id").val() ==''){
            alert('Please select product');
            return false;
        }
        var id = $("#match_id").val();

        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/qsm_order_import/updateQSMProduct&token=<?php echo $token; ?>',
            data: {sku : $('#match_product_sku').html(), product_id : $("#match_product_id").val()},
            success: function(res) {
                if(res){
                    location.reload();
                }
            }
        });
    });
    function disableSubmit(){
        $('#importBtn').attr('disabled', true);
        setTimeout(function(){ 
            $('#importBtn').removeAttr('disabled');
            $('#qsmForm').submit();
        }, 1500);
    }
</script>