<?php echo $header; ?>



  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">



  <?php if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php endif; ?>



  <div class="box">



  	<div class="content">



		<div class="form-information">



			<div class="heading product-form purchase_form_page">



				<h1><?php echo $heading_title; ?></h1>



				<div class="action">



					<?php if (empty($purchase_id)) { ?>



                    <button class="btn btn-update" type="submit" name="purchase_button" value="hold" title="Save"><?php echo $text_hold; ?></button>



                    <?php } ?>



                    <button class="btn btn-update" type="submit" name="purchase_button" value="submit" title="Save"><?php echo $button_save; ?></button>



					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>



				</div>



			</div>



			<div class="form-new product-form">



            	<?php if ($error_warning) { ?>



                    <div class="warning" style="width:94%"><?php echo $error_warning; ?></div>



                <?php } ?>



				<table cellspacing="0" class="tab-list">



					<tbody>



						<tr>



							<td class="value">



								 <div class="transaction_wrapper">



									 <table cellspacing="0" class="form-list top_main_wrapper">



										<tbody>



											<tr>



												<td class="label">



													<label for="name"><?php echo $entry_tran_no; ?><span class="required">*</span></label>



												</td>



												<td class="value">



													<input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="input-text validate[required]" readonly="readonly" />



													<?php if ($error_transaction_no) { ?>



													<span class="error"><?php echo $error_transaction_no; ?></span>



													<?php } ?>



												</td>







                                                <td class="label">



													<label for="name"><?php echo $entry_tran_dt; ?><span class="required">*</span></label>



												</td>



												<td class="value">



													<input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="date input-text validate[required]" >



													<?php if ($error_transaction_date) { ?>



													<span class="error"><?php echo $error_transaction_date; ?></span>



													<?php } ?>



												</td>



											</tr>







                                            <tr>



												<td class="label">



													<label for="vendor"><?php echo $entry_vendor; ?><span class="required">*</span></label>



												</td>



												<td class="value">



													<select name="vendor" id="vendor_code" onchange="getVendorProductDetails()" class="input-text validate[required]" >



														<option value="">-- Select Vendor --</option>



														<?php if(!empty($vendor_collection)): ?>



															<?php foreach($vendor_collection as $vendordetails): ?>



																<option value="<?php echo $vendordetails['vendor_id']; ?>"

																        label="<?php echo $vendordetails['vendor_name']; ?>"

																 <?php if($vendordetails['vendor_id']==$vendor):?> selected="selected" <?php endif; ?>>



																	<?php echo $vendordetails['code']; ?>



																</option>



															<?php endforeach; ?>



														<?php endif; ?>



													</select>



                                                    <?php if ($error_vendor) { ?>



                                                    	<span class="error"><?php echo $error_vendor; ?></span>



                                                    <?php } ?>



												</td>



                                                <td class="label">



													<label for="name"><?php echo $entry_vendor_name; ?></label>



												</td>



												<td class="value" id="trans_vendor_name">



													<input type="text" name="vendor_name" id="vendor_name" value="<?php echo $vendor_name; ?>" class="input-text" readonly="readonly">



												</td>



											</tr>







                                            <tr>



												<td class="label">



													<label for="name"><?php echo $entry_refno; ?><span class="required">*</span></label>



												</td>



												<td class="value">



													<input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="input-text validate[required]" placeholder="Enter ref no">



													<?php if ($error_reference_no) { ?>



													<span class="error"><?php echo $error_reference_no; ?></span>



													<?php } ?>



												</td>



                                                <td class="label">



													<label for="name"><?php echo $entry_refdate; ?><span class="required">*</span></label>



												</td>



												<td class="value">



													<input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date input-text validate[required]" >



													<?php if ($error_reference_date) { ?>



													<span class="error"><?php echo $error_reference_date; ?></span>



													<?php } ?>



												</td>



											</tr>







										</tbody>



									</table>



								</div>







                                <div class="purchase_grid">



									<table id="special" class="list">



										<thead>



											<tr>



												<td class="center"><?php echo $text_inventory_code; ?></td>



												<td class="center"><?php echo $text_product_name; ?></td>



												<td class="center"><?php echo $text_qty; ?></td>



                                                <td class="center"><?php echo $text_raw_cost; ?></td>



												<td class="center"><?php echo $text_price; ?></td>



                                                <td class="center"><?php echo $text_disc_perc; ?></td>



                                                <td class="center"><?php echo $text_disc_price; ?></td>



                                                <td class="center"><?php echo $text_net_price; ?></td>



                                                <td class="center"><?php echo $text_tax; ?></td>



                                                <td class="center"><?php echo $text_total; ?></td>



                                                <td class="center"><?php echo $column_action; ?></td>



											</tr>



										</thead>



                                        <tbody>



										<?php if (!empty($cartPurchaseDetails)) { ?>



											<?php foreach ($cartPurchaseDetails as $purchase) { ?>



                                                <?php



												    if($apply_tax_type=='2') {



													    $totalValue = $purchase['total'];



													} else {



													    $totalValue = $purchase['tax'] + $purchase['total'];



													}



												?>



                                                <tr id="purchase_<?php echo $purchase['product_id']; ?>">



                                                    <td class="center"><?php echo $purchase['code']; ?></td>



                                                    <td class="center"><?php echo $purchase['name']; ?></td>



                                                    <td class="center"><?php echo $purchase['quantity']; ?></td>



                                                    <td class="center"><?php echo $purchase['price']; ?></td>



                                                    <td class="center"><?php echo $purchase['raw_cost']; ?></td>



                                                    <td class="center"><?php if (($purchase['discount_mode'] == '1') && !empty($purchase['discount'])): echo $purchase['discount']; endif; ?></td>



                                                    <td class="center"><?php if (($purchase['discount_mode'] == '2') && !empty($purchase['discount'])): echo $purchase['discount']; endif; ?></td>



                                                    <td class="center"><?php echo $purchase['sub_total']; ?></td>



                                                    <td class="center"><?php echo $this->currency->format($purchase['tax']); ?></td>



                                                    <td class="center"><?php echo $this->currency->format($totalValue); ?></td>



                                                    <td class="center">[ <a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"><?php echo $text_remove ?></a> ]</td>



                                                </tr>



                                            <?php } ?>



										<?php } else { ?>



                                            <tr>



                                                <td colspan="11" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>



                                            </tr>



                                        <?php } ?>



                                        </tbody>



									</table>



								</div>







                                <div class="purchase_add">

                                	<div id="get-products-container" class="get-products-title">

                                		<?php if (!empty($products)) { ?>

                                		<a id="searchproduct-key" class="get-products-text">Get Products</a>

                                		<?php }?>

                                	</div>

									<table class="list" id="addproductTbl">

										<thead>

											<tr>

												<td class="center"><?php echo $text_inventory_code; ?></td>

												<td class="center"><?php echo $text_product_name; ?></td>

												<td class="center"><?php echo $text_weight_class; ?></td>

                                                <td class="center"><?php echo $text_qty; ?></td>

                                                <td class="center"><?php echo $text_raw_cost; ?></td>

												<td class="center"><?php echo $text_price; ?></td>

                                                <td class="center"><?php echo $text_disc_perc; ?></td>

                                                <td class="center"><?php echo $text_disc_price; ?></td>

                                                <td class="center"><?php echo $text_tax_class; ?></td>

                                                <td class="center">Action<?php //echo $text_total; ?></td>

											</tr>

										</thead>

										<?php if (!empty($products)) { ?>

										<tbody>

											<tr>

												<td class="center">

       													 <input type="text" name="sku" id="sku" class="input-text"

       													 	<?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>

       													 	onkeyup="getProductautoFill(this.value);">

       													 <input type="hidden" name="product_id" id="product_id"

       													   <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>

       										   <div id="suggesstion-box"></div>

       										   </td>



												<td class="center"><input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="input-text" readonly="readonly" /></td>



												<td class="center">



                                                	<select name="weight_class_id" id="weight_class_id" class="input-text">



                                                        <?php if (!empty($weight_classes)): ?>



															<?php foreach($weight_classes as $weight): ?>



                                                                <option value="<?php echo $weight['weight_class_id']; ?>" <?php if (!empty($rowdata) && ($weight['weight_class_id'] == $rowdata['weight_class_id'])): ?> selected="selected" <?php endif; ?>>



                                                                    <?php echo $weight['title']; ?>



                                                                </option>



                                                            <?php endforeach; ?>



                                                        <?php endif; ?>



                                                    </select>



                                                </td>



												<td class="center"><input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="input-text small_box" onblur="updateRowData(this.value);" /></td>





                                                <td class="center"><input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="input-text small_price_box" /></td>





                                                <td class="center"><input type="text" name="raw_cost" id="raw_cost" value="<?php if (!empty($rowdata)){ echo $rowdata['raw_cost']; } ?>" class="input-text small_price_box" /></td>



												<td class="center"><input type="text" name="discount_percentage" id="discount_percentage" onblur="updateRowData('0');" value="<?php if (!empty($rowdata)){ echo $rowdata['discount_percentage']; } ?>" class="input-text small_box"<?php if(!empty($hide_discount_percentage)): ?> readonly="readonly" <?php endif; ?> /></td>



                                                <td class="center"><input type="text" name="discount_price" id="discount_price" onblur="updateRowData('0');" value="<?php if (!empty($rowdata)){ echo $rowdata['discount_price']; } ?>" class="input-text small_box"<?php if(!empty($hide_discount_price)): ?> readonly="readonly" <?php endif; ?> /></td>



                                                <td class="center">



                                                    <select name="tax_class_id" id="tax_class_id" onchange="updateRowData('0');" class="input-text">



                                                    	<option value="">Select Tax</option>



                                                        <?php if (!empty($tax_classes)): ?>



															<?php foreach($tax_classes as $tax): ?>



                                                                <option value="<?php echo $tax['tax_class_id']; ?>" <?php if (!empty($rowdata) && ($tax['tax_class_id'] == $rowdata['tax_class_id'])): ?> selected="selected" <?php endif; ?>>



                                                                    <?php echo $tax['title']; ?>



                                                                </option>



                                                            <?php endforeach; ?>



                                                        <?php endif; ?>



                                                    </select>



                                                </td>

                                                <?php /*?>

                                                <td class="center"><input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="input-text row_total_box" readonly="readonly" /></td>

												<?php */ ?>

												<td class="center">[ <a onclick="addProductGrid();" id="addProductLink">Add More</a> ]</td>



											</tr>



										</tbody>



                                        <tfoot>



											<tr>



                                                <td class="left purchase_add_button" colspan="10" align="right">



                                                	<a onclick="clearPurchaseData();" class="btn btn-zone"><?php echo $button_clear; ?></a>



                                                    <a onclick="addToPurchase();" class="btn btn-zone"><?php echo $button_add; ?></a>



                                                </td>



											</tr>



                                            <?php $i = 0; ?>

											<?php if(!empty($purchase_totals)): ?>

                                            	<?php foreach ($purchase_totals as $total) { ?>

												<?php if ($i == 0): ?>

                                                <tr id ="TR<?php echo $total['code'];?>">

                                                    <td>Remarks</td>

                                                    <td><input type="text" name="remarks" id="remarks" value="<?php if (!empty($remarks)){ echo $remarks; } ?>" class="input-text" /></td>



                                                    <td>Bill Disc(%)</td>



                                                    <td>

                                                    	<input type="text" name="bill_discount_percentage" id="bill_discount_percentage"

                                                    	value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>"

                                                    	onblur="updateBillDiscount('0');" class="input-text"

                                                    	<?php if(!empty($hide_bill_discount_percentage)): ?> readonly="readonly" <?php endif; ?> />

                                                    </td>



                                                    <td>Bill Disc($)</td>



                                                    <td><input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onblur="updateBillDiscount('0');" class="input-text"

                                                    	<?php if(!empty($hide_bill_discount_price)): ?> readonly="readonly" <?php endif; ?> /></td>



                                                    <td colspan="3" align="right" class="purchase_total_left">

                                                    	<?php echo $total['title']; ?>

                                                    </td>



                                                    <td class="purchase_total_right">

                                                    	<input type="text" name="<?php echo $total['code']; ?>"

                                                    	id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="input-text row_total_box" readonly="readonly" />

                                                    </td>



                                                </tr>

                                                <?php else: ?>

                                                <tr id ="TR<?php echo $total['code'];?>">

                                                    <td colspan="9" align="right" class="purchase_total_left"><?php echo $total['title']; ?></td>

	                                                <td class="purchase_total_right">

	                                                	<input type="text" name="<?php echo $total['code']; ?>" id="<?php echo $total['code']; ?>"

	                                                	 value="<?php echo $total['text']; ?>" class="input-text row_total_box" readonly="readonly" />

	                                                </td>

                                                </tr>

                                                <?php endif; ?>



                                                <?php if ($total['code'] == 'sub_total'): ?>

                                                		<input type="hidden" name="sub_total_value" value="<?php echo $total['value'] ?>" /><?php endif; ?>



                                            <?php $i++; } ?>



											<?php endif; ?>



										</tfoot>



										<?php } else { ?>



										<tfoot>



											<tr>



												<td colspan="10" class="purchase_no_data"><?php echo $text_select_vendor; ?></td>



											</tr>



										</tfoot>



                                        <?php } ?>



									</table>



								</div>



							</td>



						</tr>



					</tbody>



				</table>



			</div>



		</div>



	</div>



  </div>



</form>





<style type="text/css">

	#suggesstion-box{position:absolute;}

	#country-list{float:left;list-style:none;margin-left:38px;padding:0;width:211px;cursor:pointer;}

	#country-list li{padding: 10px; background:#FAFAFA;border-bottom:#F0F0F0 1px solid;}

	#country-list li:hover{background:#F0F0F0;}

</style>



<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>



<script type="text/javascript"><!--



$('.date').datepicker({dateFormat: 'yy-mm-dd'});



$('.datetime').datetimepicker({



	dateFormat: 'yy-mm-dd',



	timeFormat: 'h:m'



});



$('.time').timepicker({timeFormat: 'h:m'});



//--></script>







<script type="text/javascript">



$("#searchproduct-key").click(function(){

    jQuery('#popup').bPopup({

        contentContainer:'.payment-page',

        easing: 'easeOutBack',

        speed: 990,

        transition: 'slideDown',

        width:'660',

        height:'660',

        scrollBar:true,

        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>'

    });

});



function addProductGrid(){

	jQuery("#addProductLink").on("click",function(){

		 var $this = jQuery(this);

		$this.text('See More');

		tableBody = jQuery('#addproductTbl').find("tbody"),

        trLast = tableBody.find("tr:last"),

        trNew = trLast.clone();

		trLast.after(trNew);

});

}

function addProductToForm(pid,sku,name,price){

	jQuery('#sku').val(sku);

	jQuery('#product_id').val(pid);

	jQuery('#name').val(name);

	jQuery('#price').val(price);

	jQuery('#raw_cost').val(price);

	removeAllOverlay();

}



function getProducts(val){

		var page =1;

		var ajaxData = "search_product_text=" + val + "&page=" + page;

	    jQuery("#ajaxloader").show();

	    jQuery.ajax({

	        type: "POST",

	        url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',

	        data: ajaxData,

	        success: function(result) {

	            jQuery("#ajaxloader").hide();

	            if (result=='') {

	                //

	            } else {

	            	jQuery('.payment-page').html(result);

	                var el = jQuery("#searchKeynew").get(0);

	                var elemLen = el.value.length;

	                el.selectionStart = elemLen;

	                el.selectionEnd = elemLen;

	                el.focus();

	                return false;

	            }

	        }

	    });

}



$(document).ready(function() {

    $("#close").click(function(){

        $("#popup").fadeOut("fast");

    });

});

function removeAllOverlay() {

	jQuery('.b-close').click();

}





function getVendorProductDetails() {

		var vname = jQuery('#vendor_code').find("option:selected").attr('label');

		jQuery("#vendor_name").val(vname);

}





function getVendorProductDetailsxx() { // RAGU Hided for loaded issue fix and added new above this fn

	jQuery("#pre-loader-icon").show();



	var ajaxData = jQuery("form").serialize();



	jQuery.ajax({



		type: "POST",



		url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>',



		data: ajaxData,



		success: function(result) {



			if (result=='') {



				//



			} else {

				jQuery('#page').html(result);

				jQuery("#pre-loader-icon").hide();



			}



		}



	});



}







function selectedProduct(val) {

		var newVal = val.split("||");

		$("#product_id").val(newVal[0]);

		$("#sku").val(newVal[1]);

		$("#name").val(newVal[2]);

		$("#price").val(newVal[3]);

		$("#raw_cost").val(newVal[3]);

		$("#suggesstion-box").hide();

	}



function getProductautoFill(sku) {

	var ajaxData = 'sku='+sku;

	jQuery.ajax({

		type: "POST",

		url: 'index.php?route=transaction/purchase/getProductDetails&token=<?php echo $token; ?>',

		data: ajaxData,

		success: function(result) {

			if (result=='') {

				//

			} else {

				$("#suggesstion-box").show();

				$("#suggesstion-box").html(result);

				return false;

			}

			jQuery("#pre-loader-icon").hide();

		}

	});

}







function getProductDetails() {



	jQuery("#pre-loader-icon").show();



	var ajaxData = jQuery("form").serialize();



	jQuery.ajax({



		type: "POST",



		url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=inventory',



		data: ajaxData,



		success: function(result) {



			if (result=='') {



				//



			} else {



				jQuery('#page').html(result);



			}



			jQuery("#pre-loader-icon").hide();



		}



	});



}







function clearPurchaseData() {



	jQuery('#product_id').val('');



	jQuery('#name').val('');



	jQuery('#weight_class_id').val('');



	jQuery('#quantity').val('');



	jQuery('#price').val('');



	jQuery('#discount_percentage').val('');



	jQuery('#discount_price').val('');



	jQuery('#tax_class_id').val('');



	jQuery('#row_total').val('');



}









function addToPurchase() {

	var product_id = jQuery('#product_id').val();

	var price   = jQuery('#price').val();

	var quantity   = jQuery('#quantity').val();

	if (product_id && price && quantity) {

		jQuery("#pre-loader-icon").show();

		var ajaxData = jQuery("form").serialize();

		jQuery.ajax({

			type: "POST",

			url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=product',

			data: ajaxData,

			success: function(result) {

				if (result=='') {

					//

				} else {

					jQuery('#page').html(result);

				}

				jQuery("#pre-loader-icon").hide();

			}

		});

	}else{

		if(quantity==''){

			jQuery('#quantity').focus();

		}else if(raw_cost==''){

			jQuery('#price').focus();

		}

	}

}



function updateRowData(fieldValue) {

	var product_id = jQuery('#product_id').val();

	var price      = jQuery('#price').val();

	var quantity   = jQuery('#quantity').val();



	if (fieldValue && product_id && raw_cost && quantity) {

		jQuery("#pre-loader-icon").show();

		var ajaxData = jQuery("form").serialize();

		jQuery.ajax({

			type: "POST",

			url: 'index.php?route=transaction/purchase/AjaxupdateRowData&token=<?php echo $token; ?>&updateType=rowcount',

			data: ajaxData,

			dataType: 'json',

			success: function(result) {

				if (result=='') {

					//

				} else {

					jQuery('#row_total').val(result['rowdata']['total']);

				}

				jQuery("#pre-loader-icon").hide();

			}

		});

	}else{

		if(quantity==''){

			jQuery('#quantity').focus();

		}else if(raw_cost==''){

			jQuery('#price').focus();

		}

	}

}





function updateRowDataxx(fieldValue) { // RAGU Hided for loaded issue fix and added new above this fn

	var product_id = jQuery('#product_id').val();

	var price   = jQuery('#price').val();

	var quantity   = jQuery('#quantity').val();

	if (fieldValue && product_id && raw_cost && quantity) {

		jQuery("#pre-loader-icon").show();

		var ajaxData = jQuery("form").serialize();

		jQuery.ajax({

			type: "POST",

			url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=rowcount',

			data: ajaxData,

			success: function(result) {

				if (result=='') {

					//

				} else {

					jQuery('#page').html(result);

				}

				jQuery("#pre-loader-icon").hide();

			}

		});

	}else{

		if(quantity==''){

			jQuery('#quantity').focus();

		}else if(raw_cost==''){

			jQuery('#price').focus();

		}

	}

}





function removeProductData(removeId) {

	var ajaxData = jQuery("form").serialize();

	jQuery.ajax({

		type: "POST",

		url: 'index.php?route=transaction/purchase/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,

		data: ajaxData,

		dataType: 'json',

		success: function(result) {

			if (result=='') {

				//

			} else {

				jQuery("#purchase_"+removeId).remove();

				jQuery('#sub_total').val(result['sub_total']);

				jQuery('#sub_total_value').val(result['sub_total_value']);

				jQuery('#total').val(result['total']);

				jQuery('#discount').val(result['discount']);

				jQuery('#tax').val(result['tax']);

				if(result['sub_total_value']=='0'){

					jQuery("#TRtax").remove();

					jQuery("#TRdiscount").remove();

				}

				if(result['tax']=='0'){

					jQuery("#TRtax").remove();

				}

				if(result['discount']=='0'){

					jQuery("#TRdiscount").remove();

				}



			}

		}

	});

}



function removeProductDataxx(removeId) {

	var ajaxData = jQuery("form").serialize();

	jQuery.ajax({

		type: "POST",

		url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,

		data: ajaxData,

		success: function(result) {

			if (result=='') {

				//

			} else {

				//window.location.reload();

				//jQuery('#page').html(result);

			}

		}

	});

}



function updateBillDiscount(fieldValue) {

	if (fieldValue) {

		jQuery("#pre-loader-icon").show();

		var ajaxData = jQuery("form").serialize();

		jQuery.ajax({

			type: "POST",

			url: 'index.php?route=transaction/purchase/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',

			data: ajaxData,

			dataType: 'json',

			success: function(result) {

				if (result=='') {

					//

				} else {

					if(result['discountRow']){

						jQuery("#TRsub_total").after(result['discountRow']);

					}

					jQuery('#sub_total').val(result['sub_total']);

					jQuery('#sub_total_value').val(result['sub_total_value']);

					jQuery('#total').val(result['total']);

					jQuery('#discount').val(result['discount']);

					jQuery('#tax').val(result['tax']);

					jQuery('#bill_discount_percentage').val('');

					jQuery('#bill_discount_price').val('');

				}

				jQuery("#pre-loader-icon").hide();

			}

		});

	}

}





function updateBillDiscountxx(fieldValue) {

	if (fieldValue) {

		jQuery("#pre-loader-icon").show();

		var ajaxData = jQuery("form").serialize();

		jQuery.ajax({

			type: "POST",

			url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=billDiscount',

			data: ajaxData,

			success: function(result) {

				if (result=='') {

					//

				} else {

					jQuery('#page').html(result);

				}

				jQuery("#pre-loader-icon").hide();

			}

		});

	}

}



</script>

<script type="text/javascript">

	jQuery("#price").ForceNumericOnly();

	jQuery("#quantity").ForceNumericOnly();

	jQuery("#discount_percentage").ForceNumericOnly();

	jQuery("#discount_price").ForceNumericOnly();

	jQuery("#bill_discount_percentage").ForceNumericOnly();

	jQuery("#bill_discount_price").ForceNumericOnly();

</script>



<script type="text/javascript">

	//var z = dhtmlXComboFromSelect("product_id");



	//z.enableFilteringMode(true);



</script>



<?php echo $footer; ?>