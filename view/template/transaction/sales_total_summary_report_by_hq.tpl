<!-- NEW CODE -->
<?php echo $header; ?>
<style>
  #priceAlign{text-align: right;}
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Total Sales Summary Report By Outlets</h3>     
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date">
                                    </td>                       
                          <td>
                            <select name="filter_location" id="filter_location" class="textbox">
                              <option value="">Select Location </option>
                              <?php if(count($location)>0){ ?>
                                <?php foreach($location as $locations){ ?>
                                  <option <?php if($_REQUEST['filter_location'] == $locations['location_code'] ){ ?> selected <?php } ?> value="<?php echo $locations['location_code']; ?>"><?php echo $locations['location_name']; ?></option>
                                <?php } ?>
                              <?php } ?>
                            </select>
                          </td>
                                    <td>
                     
                      </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row"> 
            <div style="clear:both"></div>
              <?php if ($error_warning) { ?>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <div class="alert alert-block alert-danger fade in "><?php echo $error_warning; ?>
                         <button type="button" class="close" data-dismiss="alert"></button>
                  </div>
                </div>
            <?php } ?>

              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center" style="width: 7%;">Date</td>
                          <td class="center" style="width: 5%;">Outlet Name</td>
                          <td class="center" style="width: 10%;">Total Sales Before GST</td>
                          <td class="center" style="width: 5%;">GST</td>
                          <td class="center" style="width: 5%;">Discount</td>
                          <td class="center" style="width: 5%;">Round Off</td>
                          <td class="center" style="width: 5%;">Total Sales</td>
                          <td class="center" style="width: 5%;">CASH</td>
                          <td class="center" style="width: 5%;">NETS</td>
                          <td class="center" style="width: 5%;">VISA</td>
                          <td class="center" style="width: 7%;">CHEQUE</td>
                          <td class="center" style="width: 7%;">CREDIT</td>
                          <td class="center" style="width: 7%;">BANKTR</td>
                          <td class="center" style="width: 7%;">REDMART</td>
                          <td class="center" style="width: 7%;">VOUCHER</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($sales); ?>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <?php $sale_discrepencies = ($sale['before_gst']+$sale['gst'])-$sale['total_sales']; ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['date']; ?></td>
                          <td class="center"><?php echo $sale['outlet_name']; ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['before_gst'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['gst'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['discount'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['round_off'],2); ?></td>

                          <td class="center" id="priceAlign"><?php echo number_format($sale['total_sales'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['cash'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['nets'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['visa'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['cheque'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['credit'],2); ?></td>

                          <td class="center" id="priceAlign"><?php echo number_format($sale['banktr'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['redmart'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['voucher'],2); ?></td>

                        </tr>
                        <?php 
                        $strBeforegst +=$sale['before_gst'];
                        $strGst +=$sale['gst'];
                        $strTotalsales +=$sale['total_sales'];

                        $strdiscount +=$sale['discount'];
                        $strroundoff +=$sale['round_off'];

                        $strCash +=$sale['cash'];
                        $strNets +=$sale['nets'];
                        $strVisa +=$sale['visa'];
                        $strCheque +=$sale['cheque'];
                        $strCredit +=$sale['credit'];

                         $strBanktr +=$sale['banktr'];
                         $strRedmart +=$sale['redmart'];
                         $strVoucher +=$sale['voucher'];


                        ?>
                        <?php } ?>
                        <tr>
                          <td colspan="2" align="right"><strong>Total:</td>
                          <td id="priceAlign"><strong><?php echo number_format($strBeforegst,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strGst,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strdiscount,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strroundoff,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strTotalsales,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCash,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strNets,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strVisa,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCheque,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCredit,2); ?></strong></td>

                           <td id="priceAlign"><strong><?php echo number_format($strBanktr,2); ?></strong></td>
                           <td id="priceAlign"><strong><?php echo number_format($strRedmart,2); ?></strong></td>
                           <td id="priceAlign"><strong><?php echo number_format($strVoucher,2); ?></strong></td>
                           
                        </tr>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
//--></script> 
<?php echo $footer; ?>