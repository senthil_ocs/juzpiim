<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Itemwise Sales Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a> -->

              <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
             <a href="<?php echo $exportPDFAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
             
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Sales Summary Report By Sku</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
       <div>
            <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Date From - <span id="filter_date_from_id"><?php echo $data['filter_date_from']?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Date To - <span id="filter_date_to_id"><?php echo $data['filter_date_to']?></span> </span>
                <?php } ?>
               <!--  <?php if($data['filter_sku'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   sku - <span id="filter_sku_id"><?php echo $data['filter_sku']?></span> </span>
                <?php } ?> -->
                 <?php if($data['filter_department'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   Department- <span id="filter_department_id"></span> </span>
                <?php } ?>
               
                  <?php if($data['filter_category'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   Category - <span id="filter_category_id"><?php echo $data['filter_category']?></span> </span>
                <?php } ?>

                 <?php if($data['filter_location'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   Location - <span id="filter_location_id"><?php echo $data['filter_location']?></span> </span>
                <?php } ?>

                 <?php if($data['filter_brand'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   Brand - <span id="filter_brand_id"><?php echo $data['filter_brand']?></span> </span>
                <?php } ?>

                 <?php if($data['filter_vendor'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">   Vendor - <span id="filter_vendor_id"><?php echo $data['filter_vendor']?></span> </span>
                <?php } ?>
                <?php if($data['filter_sku']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  SKU : <span id="filter_product_id"><?php echo $filter_sku;?></span> </span>
                <?php } ?>
                <?php if($data['filter_channel']!= ''){
                 $filter_network_name = $this->model_transaction_sales->getNetworkName($data['filter_channel']); ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Sales Channel : <?php echo $filter_network_name;?></span>
                <?php } ?>
            </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter" id="movement_report">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                                    </td> 
                                    <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px;">
                              <?php if(count($Tolocations)>=2){?>
                              <option value="">-- Select Location --</option>
                              <?php }?>
                              <?php
                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>
                              <?php  } } ?> 
                          </select>
                          </td>
                          <td>
                            <select name="filter_channel" id="filter_channel" style="min-height: 35px; padding: 7px; width: 100%;">
                              <option value="">Select Sales channel</option>
                              <?php foreach ($channels as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $data['filter_channel']){ echo 'selected'; } ?>> <?php echo $value['name']; ?></option>
                              <?php } ?>
                            </select> 
                          </td>
                          <td> 
                           <select name="filter_department" class="textbox" style="min-height: 35px; padding: 7px;width: 20%;">
                                    <option value="">Select Department</option>
                                    <?php if (!empty($department_collection)) { ?>
                                        <?php foreach ($department_collection as $department) { ?>
                                            <?php if (trim($department['department_code']) ==  $data['filter_department']) { ?>
                                                <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                            <?php } ?>   
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </td>
                          <td>
                            <select name="filter_category" class="textbox" style="min-height: 35px; padding: 7px;width: 20%;">
                                <option value="">Select Category</option>
                                <?php if (!empty($category_collection)) { ?>
                                    <?php foreach ($category_collection as $category) { ?>
                                        <?php if (trim($category['category_code']) == $data['filter_category']) { ?>
                                            <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                              <?php echo $category['category_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select> 
                        </td>
                        <td>
                            <select name="filter_brand" class="textbox" style="min-height: 35px; padding: 7px;width: 20%;">
                                <option value="">Select Brand</option>
                              <?php if (!empty($brand_collection)) { ?>
                                  <?php foreach ($brand_collection as $brand) { ?>
                                      <?php if (trim($brand['brand_code']) == $data['filter_brand']) { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                      <?php } else { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
                                      <?php } ?>   
                                  <?php } ?>
                              <?php } ?>
                            </select> 
                        </td>
                        <td>
                            <select name="filter_vendor" class="textbox" style="min-height: 35px; padding: 7px;width: 20%;">
                                 <option value="">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if (trim($vendor['vendor_code']) == $data['filter_vendor']) { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select> 
                        </td>
                        <!-- <td>
                            <input type="text" placeholder='Sku' name="filter_sku" value="<?php echo $data['filter_sku'] ; ?>" class="textbox" style="min-height: 35px; width: 20%;" />
                          </td> -->
                           <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                            <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                         <td style="display: none;">
                          <input type="text" name="filter_barcodes" id="filter_barcodes" value="" class="textbox" placeholder="Scan Barcode">
                        </td>
                           <td>
                           </td>

                          <td align="center" colspan="4">
                            <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>">
                            <input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $_REQUEST['filter_product_id'] ; ?>">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            <br>
             <table class="table orderlist statusstock">
              <tr>
                <td class="right" colspan="9" style="width:90%;">Total</td>
                <td class="right" style="width:10%;"><?php echo $nettotal;?></td>
              </tr>
            </table>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Location Code</td>
                          <td class="center">Department</td>
                          <td class="center">Category</td>
                          <td class="center">Brand</td>
                          <td class="center">Vendor</td>
                          <td class="center">Sku</td>
                          <td class="center">Name</td>
                          <td class="center">Quantity</td>
                          <td class="center">Sku Price</td>
                          <td class="center">Discount</td>
                          <td class="center">Net Total</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['location_code']; ?></td>
                          <td class="center"><?php echo $sale['department']; ?></td>
                          <td class="center"><?php echo $sale['category']; ?></td>
                          <td class="center"><?php echo $sale['brand']; ?></td>
                          <td class="center"><?php echo $sale['vendor']; ?></td>
                          <td class="center"><?php echo $sale['sku']; ?></td>
                          <td class="center"><?php echo $sale['name']; ?></td>
                          <td class="center"><?php echo $sale['qty']; ?></td>
                          <td class="center"><?php echo $sale['sku_price']; ?></td>
                          <td class="center"><?php echo $sale['discount']; ?></td>
                          <td class="right"><?php echo $sale['net_total']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="11"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_department_id" ).html(filter_department);

  var filter_location = $('[name="filter_location"] option:selected').text();
  $( "#filter_location_id" ).html(filter_location);

  var filter_category = $('[name="filter_category"] option:selected').text();
  $( "#filter_category_id" ).html(filter_category);

  var filter_brand = $('[name="filter_brand"] option:selected').text();
  $( "#filter_brand_id" ).html(filter_brand);

  var filter_vendor = $('[name="filter_vendor"] option:selected').text();
  $( "#filter_vendor_id" ).html(filter_vendor);

   var filter_name = $("#filter_name").val();
  $( "#filter_product_id" ).html(filter_name);
   
 
});

/*
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});
*/
  
function getProductautoFill(sku) {

    /*var frm_date = $("#filter_date_from").val();
    var to_date = $("#filter_date_to").val();
    if(frm_date==''){
      alert("From date should not empty");
      $("#filter_date_from").focus();
    }else if(to_date==''){
      alert("To date should not empty");
      $("#filter_date_to").focus();
    }*/
  var filter_name = $("#filter_name").val();
   
   if(filter_name ==''){
    $('#filter_sku').val('');
   }

  if(sku.length < SEARCH_CHAR_CNT){
      return false;
  }
    var location = $("#filter_location_code").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                  // alert("Product Not Available or Disabled.");
                  $("#suggesstion-box").hide();
                 clearPurchaseData();
                 return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
        var newVal = val.split("||");
       console.log(newVal);
        $('#filter_name').val(newVal[2].replace("^","'").replace('!!','"'));
        $('#filter_sku').val(newVal[1].replace("^","'").replace('!!','"'));
        $('#filter_product_id').val(newVal[0]);
        $('#suggesstion-box').hide();
        // if(cnt=='1'){
        //     $( "#movement_report" ).submit();
        // }
    }

function getValue(name,sku){
  console.log(sku);
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}
</script>
