<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Stock Take List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               
                <a href="<?php echo $insert; ?>"><button type="button" name="stock_take_button" value="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
               
                <?php if ($hold) { ?>
                    <a href="<?php echo $back_button; ?>"><button name="stock_take_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> <?php echo $button_back; ?></button></a>
                    <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">  
                        <td>
                          <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date">
                        </td>
                        <td>
                          <input type="text" id='' placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date">
                        </td> 
                        <td>
                           <select name="filter_location_code" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Location --</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td> 
                        <td align="center" colspan="4">
                          <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>                        
              </div>     
           </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;">
            </div> 			 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead><!-- locationcode,terminal code,added date,total items -->
                           <tr class="heading">
                      <td class="center">S.No</td>
                      <td class="center">Stock take Id</td>     
        					    <td class="center"><?php echo $text_location_code; ?></td>
                      <td class="center"><?php echo $text_terminal_code; ?></td>
                      <td class="center">updated Status</td>
                      <td class="center"><?php echo $text_created_on; ?></td>
                      <td class="center">Creaded by</td>
                      <td class="center"><?php echo $column_action; ?></td>
                      
					         </tr>
                        </thead>
                        <tbody>
                            <?php 

                            if ($stock_take) { $i=1; ?>
                            <?php $class = 'odd'; 
                            $total=$purchaseDetail['totalItem'];
                            ?>
					<?php foreach ($stock_take as $stocktake) { 
            if($stocktake['is_updated']=='1'){
              $status = 'Updated';
            }else{
              $status = 'Not Updated';
            }
           ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td class="center"><?php echo $stocktake['pagecnt']+$i;?></td>
                      <td class="center"><?php echo $stocktake['stocktake_id']; ?></td>
                      <td class="center"><?php echo $stocktake['location_code']; ?></td>
					            <td class="center"><?php echo $stocktake['terminal_code']; ?></td>
                      <td class="center"><?php echo $status; ?></td>
                      <td class="center"><?php echo $stocktake['createdon']; ?></td>
                      <td class="center"><?php echo $stocktake['createdby']; ?></td>
                      <td class="center">
                      <?php if ($hold): ?>
                      <a href="<?php echo $stocktake['modify_button'] ?>"><i class='fa fa-edit' style='font-size:19px' data-toggle="tooltip" title="Modify"></i></a>
                      &nbsp;&nbsp;
                      <?php endif; ?>
                    <a href="<?php echo $stocktake['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="View"></i></a></td>


					</tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

function filterReport() {
    url = 'index.php?route=transaction/stock_take&token=<?php echo $token; ?>';
    
    var filter_date_from = jQuery('input[name=\'filter_date_from\']').attr('value');
    if (filter_date_from) {
        url += '&filter_date_from=' + encodeURIComponent(filter_date_from);
    }

    var filter_date_to = jQuery('input[name=\'filter_date_to\']').attr('value');
    if (filter_date_to) {
        url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
    }
    var filter_location_code = jQuery('select[name=\'filter_location_code\']').attr('value');
    if (filter_location_code) {
        url += '&filter_location_code=' + encodeURIComponent(filter_location_code);
    }

    location = url;
}
</script> 
<?php echo $footer; ?>