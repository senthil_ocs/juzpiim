<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Sales Detail Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

        <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Purchase Invoice Details Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
         <div>
              <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <span id="filter_date_from_id"><?php echo $data['filter_date_from'];?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_date_to_id"></span><?php echo $data['filter_date_to'];?> </span>
                <?php } ?>
                 <?php if($data['filter_transactionno']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Invoice No - <span id="filter_transactionno_id"></span><?php echo $data['filter_transactionno'];?> </span>
                <?php } ?>

                <?php if($data['filter_location_code']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Location - <span id="filter_transactionno_id"></span><?php echo $data['filter_location_code'];?> </span>
                <?php } ?>
                 <?php if($data['filter_supplier']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Supplier - <span id="filter_supplierid"></span></span>
                <?php } ?>


              </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                   <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">   
                       <td width="15%"><b>Reference date</b>
                            <input type="checkbox" id='filter_reference_date' placeholder='' name="filter_reference_date" value="1" <?php if($data['filter_reference_date'] == 1){ echo 'checked';} ?> class="">
                        </td>                  
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                         </td>
                          <td>
                           <select name="filter_location_code" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location_code'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>                       
                          <td>
                            <input type="text" placeholder='Invoice No' name="filter_transactionno" value="<?php echo $data['filter_transactionno'] ; ?>" class="textbox" autocomplete="off"/ style="width: 20%;">
                          </td>
                        <td>
                            <select name="filter_supplier" class="textbox" style="min-height: 35px; padding: 7px; width: 20%;">
                                <option value="">Select Supplier</option>
                                <?php for($i=0;$i<count($vendors);$i++){ ?>
                                      <?php if ($vendors[$i]['vendor_id'] == $_REQUEST['filter_supplier']) { ?>
                                        <option value="<?php echo $vendors[$i]['vendor_id'];?>" selected="selected">
                                            <?php echo $vendors[$i]['vendor_name'];?>
                                        </option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendors[$i]['vendor_id'];?>">
                                                <?php echo $vendors[$i]['vendor_name'];?>
                                            </option>
                                      <?php } ?>
                                  <?php } ?>
                              </select>
                          </td>
                          <td>
                          </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_invoice');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                   <?php if ($purchaseHeader) { ?>
                      <?php foreach ($purchaseHeader as $key => $sales) { ?>
                    <table class="table orderlist statusstock">
                    <tr <?php echo $contact; ?>>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Transaction No:</strong> <?php echo $sales['transaction_no']; ?></span> </td>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Reference No:</strong> <?php echo $sales['reference_no']; ?></span> </td>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Reference Date:</strong> <?php echo $sales['reference_date']; ?></span> </td>
                      <td align="right"><span class="print_date"><strong>Date:</strong> <?php echo $sales['transaction_date']; ?></span></td>
                    </tr>
                    </table>
                        <table class="table orderlist statusstock">
                          <thead>
                            <tr class="heading">

                                <td class="center">SKU</td>
                                <td class="center">Name</td>
                                <td class="center">Qty</td>
                                <td class="center">Price</td>
                                <td class="center">Sub Total</td>
                                <td class="center">Discount</td>
                                <td class="center">GST</td>
                                <td class="center">Net Total</td>
                            </tr>
                          </thead>
                          <tbody>

                          <?php $class = 'odd'; ?>
                          <?php
                            $actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0; 
                            foreach ($sales['salesDetails'] as $value) {
                            	if($value['discount_percentage']>=1){
									$value['discount_price'] = $value['net_price']*($value['discount_percentage']/100);
								}


                            	$i++; ?>
                          <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                          <?php 
                          $actualtotal+=$value['total'];
                          $tot_subtotal+=$value['net_price'];
                          $tot_gst+=$value['tax_price'];
                          $tot_discount+=$value['discount_price'];
                          ?>
                        <tr class="<?php echo $class; ?>">
                          <td class="center"><?php echo $value['sku_title']; ?></td>
                          <td class="center"><?php echo $value['name']; ?></td>
                          <td class="center"><?php echo $value['quantity']; ?></td>
                          <td class="right"><?php echo $value['price']; ?></td>
                          <td class="right"><?php echo $value['net_price']; ?></td>
                          <td class="right"><?php echo $value['discount_price']; ?></td>
                          <td class="right"><?php echo $value['tax_price']; ?></td>
                          <td class="right"><?php echo $value['total']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr><td colspan="4" class="text-right"><b>Total</b></td>
                          <td class="text-right"><?php echo number_format($tot_subtotal,2); ?></td>
                          <td class="text-right"><?php echo number_format($tot_discount,2); ?></td>
                          <td class="text-right"><?php echo number_format($tot_gst,2); ?></td>
                          <td class="text-right"><?php echo number_format($actualtotal,2); ?></td>
                        </tr>
                      <?php }?>

                      <?php } else { ?>
                      <table class="table orderlist statusstock">
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                      </table>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
  $(document).ready(function() {
            var filter_supplier = $('[name="filter_supplier"] option:selected').text();
            $("#filter_supplierid").html(filter_supplier);
        });
//--></script> 
<?php echo $footer; ?>