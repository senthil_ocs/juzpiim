<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Itemwise Sales Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>

              <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
             <a href="<?php echo $exportPDFAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
             
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Fast Selling and Low Moving Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
       <div>
            <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From - <span id="filter_date_from_id"><?php echo $data['filter_date_from']?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_date_to_id"><?php echo $data['filter_date_to']?></span> </span>
                <?php } ?>
                 <?php if($data['filter_location'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Location - <span id="filter_location_id"><?php echo $data['filter_location']?></span> </span>
                <?php } ?>
                <?php if($data['filter_quantity_start'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Quantity - <span id="filter_quantity_start"><?php echo $data['filter_quantity_start']?></span> </span>
                <?php } ?>
                <?php if($data['filter_quantity_end'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Quantity - <span id="filter_quantity_end"><?php echo $data['filter_quantity_end']?></span> </span>
                <?php } ?>
                 <?php if($data['filter_fastlow'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Movings - <span id="filter_fastlow_id"><?php echo $data['filter_fastlow']?></span> </span>
                <?php } ?>

                <?php if($data['filter_channel']!= ''){
                 $filter_network_name = $this->model_transaction_sales->getNetworkName($data['filter_channel']); ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Sales Channel : <?php echo $filter_network_name;?></span>
                <?php } ?>              

            </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td> 
                        <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px;">
                              <?php if(count($Tolocations)>=2){?>
                              <option value="">-- Select Location --</option>
                              <?php }?>
                              <?php
                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>
                              <?php  } } ?> 
                          </select>
                          </td>
                          <td>
                            <select name="filter_channel" id="filter_channel" style="min-height: 35px; padding: 7px; width: 100%;">
                              <option value="">Select Sales channel</option>
                              <?php foreach ($channels as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $data['filter_channel']){ echo 'selected'; } ?>> <?php echo $value['name']; ?></option>
                              <?php } ?>
                            </select> 
                          </td>
                           <td>
                            <input type="text" placeholder='Quantity Start' name="filter_quantity_start" id="filter_quantity_start" value="<?php echo $data['filter_quantity_start']; ?>" class="textbox" />

                          </td>
                           <td>
                            <input type="text" placeholder='Quantity End' name="filter_quantity_end" id="filter_quantity_end" value="<?php echo $data['filter_quantity_end']; ?>" class="textbox" />
                            
                          </td>
                          <td> 
                           <select name="filter_fastlow" class="textbox" style="min-height: 35px; padding: 7px;width: 20%;">
                                    <option value="fast" selected="selected">Fast Selling</option>
                                    <option value="low" <?php if($data['filter_fastlow'] == 'low')echo 'selected'; ?> >Low Moving</option>
                            </select>
                            </td>
                          <td>
                           </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            <br>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Location Code</td>
                          <td class="center">Department</td>
                          <td class="center">Sku</td>
                          <td class="center">Name</td>
                          <td class="center">Quantity</td>
                         
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['location_code']; ?></td>
                          <td class="center"><?php echo $sale['department']; ?></td>
                          <td class="center"><?php echo $sale['sku']; ?></td>
                          <td class="center"><?php echo $sale['description']; ?></td>
                          <td class="center"><?php echo $sale['qty']; ?></td>                         
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}

$(document).ready(function(){
  var filter_quantity_start = $([name="filter_quantity_start"]).text();
  $( "#filter_quantity_start" ).html(filter_quantity_start);

   var filter_quantity_end = $([name="filter_quantity_end"]).text();
  $( "#filter_quantity_end" ).html(filter_quantity_end);

  var filter_location = $('[name="filter_location"] option:selected').text();
  $( "#filter_location_id" ).html(filter_location);
 
  var filter_fastlow = $('[name="filter_fastlow"] option:selected').text();
  $( "#filter_fastlow_id" ).html(filter_fastlow);
});


//--></script> 
<?php echo $footer; ?>