<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              
              <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF</a>

              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                 <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Pick List Report</h3>       
           </div>
        </div>
       <div>
              <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <span id="filter_date_from_id"><?php echo $data['filter_date_from'];?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_date_to_id"></span><?php echo $data['filter_date_to'];?> </span>
                <?php } ?>
                 <?php if($data['filter_transactionno']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Invoice No - <span id="filter_transactionno_id"></span><?php echo $data['filter_transactionno'];?> </span>
                <?php } ?>

                <?php if($data['location_code']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Location - <span id="filter_transactionno_id"></span><?php echo $data['location_code'];?> </span>
                <?php } ?>

              </p>
        </div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 65px;margin: 0px 0px -15px -12px; padding:18px 0px 0px 0px;">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                        <td>
                           <select name="filter_delivery_man" id="filter_delivery_man" class="salesdropdown">
                            <option value="">Delivery Man</option>
                            <?php
                                foreach($salesmanlist as $salesman){?>
                                  <option value="<?php echo $salesman['id'];?>" <?php echo $filters['filter_delivery_man'] == $salesman['id'] ? 'selected' : '' ; ?>> <?php echo $salesman['name']; ?></option>

                            <?php  }  ?> 
                            </select>
                        </td>
                        <td>
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                       <?php /* ?><td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                            <option value="Scheduling">Scheduling</option>
                            <option value="Pending_Delivery">Pending Delivery</option>
                            <option value="On_Delivery">On Delivery</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Canceled">Canceled</option>
                          </select>
                        </td>
                        <?php */ ?>
                          <td align="right">
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </form>
          <div style="clear:both; margin: 0 0 15px 0;"></div>
          </div>
      </div>            
    <div style="clear:both; margin: 0 0 15px 0;">
    </div>
            <!-- <table class="table orderlist statusstock">
                <tr>
                    <td colspan="9" class="right">Total :
                        <?php echo number_format($sales_net_total,2); ?>
                    </td>
                </tr>
            </table> -->
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr  class="heading">
                          <td class="center">SNo</td>
                          <td class="center">InvoiceNo</td>
                          <td class="center">DoNo</td>
                          <td class="center">From</td>
                          <td class="center">Customer</td>
                          <td class="center">SKU</td>
                          <td class="center">Name</td>
                          <td class="center">Qty</td>
                          <td class="center">Route</td>
                          <td class="center">Date</td>
                          <td class="center">DeliveryMan</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($delivery) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($delivery as $delivery) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>"> 
                          <td class="center"><?php echo $i; ?></td>
                          <td class="center"><?php echo $delivery['invoice_no']; ?></td>
                          <td class="center"><?php echo $delivery['do_no']; ?></td>
                          <td class="center"><?php echo showText($delivery['from']); ?></td>
                          <td class="center"><?php echo $delivery['vendor_name']; ?></td>
                          <td class="center"><?php echo $delivery['sku']; ?></td>
                          <td class="center"><?php echo $delivery['sku_name']; ?></td>
                          <td class="center"><?php echo $delivery['qty']; ?></td>
                          <td class="center"><?php echo $delivery['route_code']; ?></td>
                          <td class="center"><?php echo $delivery['assignedon']; ?></td>
                          <td class="center"><?php echo $delivery['deliveryman']; ?></td>
                         
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                       
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/rowreorder/1.2.7/js/dataTables.rowReorder.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
    $("#filter_delivery_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Delivery Status', 
      height:'250px'
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    if(delivery.length > 0){
      for (var i = 0, len = delivery.length; i < len; i++) 
      {
        if(delivery[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+delivery[i]+"]").prop("checked",true);
        }
      }
    }
    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    if(payment.length > 0){
      for (var i = 0, len = payment.length; i < len; i++) 
      {
        if(payment[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+payment[i]+"]").prop("checked",true);
        }
      }
    }
});
$('#table_sort').dataTable({
    /*"aaSorting" : [[ 0, "desc" ]],*/
         paging : false,
      searching : false,
        "bInfo" : false,
      "columnDefs": [ {
          "targets": [],
          "orderable": false
        },
        {
        "targets": "_all",
        "defaultContent": "-"
      } ]
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
    document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
 
//--></script> 
<?php echo $footer; ?>