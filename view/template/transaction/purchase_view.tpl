<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Purchase Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
				</ul>
				 <div class="page-toolbar">
             <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  <?php if(!$purchaseInfo['isinvoice'] && !$purchaseInfo['hold']){  ?>
                    <a href="<?php echo $convert_invoice; ?>" class="btn btn-info" style="margin: 5px 5px 0 0; border-radius: 4px">Convert Invoice</a>
                  <?php } if($purchaseInfo['xero_purchase_id']==null && $purchaseInfo['status']==null){  ?>
                  <?php }  ?>
                  <a href="<?php echo $download_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-print"></i> Print</a>

                  <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                  <?php if(!$purchaseInfo['isinvoice']){  ?>
                    <a href="<?php echo $delete_button; ?>" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i>Delete</a>
                  <?php } ?>
                  
                  </div>    
                </div>
            </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['transaction_no'] ?></td>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['transaction_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Vendor Code</td>
                          <td><?php echo $vendorDetail['vendor_code']; ?></td>
                          <td>Vendor Name</td>
                          <td><?php echo $vendorDetail['vendor_name']; ?></td>
                        </tr>
                        <tr>
                          <td>Ref No</td>
                          <td><?php echo $purchaseInfo['reference_no']; ?></td>
                          <td>Arrival Date</td>
                          <td><?php if($purchaseInfo['reference_date']!=''){ echo date('d/m/Y',strtotime($purchaseInfo['reference_date'])); } if($purchaseInfo['arrival_date_to']!=''){ echo ' To: '.date('d/m/Y',strtotime($purchaseInfo['arrival_date_to'])); }; ?></td>
                        </tr>
                        <tr>
                          <td>Currency Code</td>
                          <td><?php echo $purchaseInfo['currency_code']; ?></td>
                          <td>Currency Rate</td>
                          <td><?php echo $purchaseInfo['conversion_rate']; ?></td>
                        </tr>
                        <tr>
                          <td>Remark</td>
                          <td colspan="3"><?php echo $purchaseInfo['remarks']; ?></td>
                        </tr>
                        <tr>
                          <td>Created by</td>
                          <td><?php echo $purchaseInfo['created_by'].' at '.showDate($purchaseInfo['created_date']); ?></td>
                          <td>Modified by</td>
                          <td><?php echo $purchaseInfo['modified_by'].' at '.showDate($purchaseInfo['date_modified']); ?></td>
                        </tr>
                      </thead>
                  </table>
                  <?php if(!empty($taggedOrders)){ ?>
                    <table class="table orderlist statusstock">
                      <thead>
                        <tr class="heading">
                            <td colspan="2">Order No</td>
                            <td colspan="2">Customer Name</td>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($taggedOrders as $order) { ?>
                            <tr>
                                <td colspan="2"><?php echo $order['order_no']; ?></td>
                                <td colspan="2"><?php echo $order['cust_name']; ?></td>
                            </tr>
                        <?php  } ?>
                      </tbody>
                    </table>
                  <?php } ?>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">UnTag</td>
                        <td class="center">Sku</td>
                        <td class="center">Product Name</td>
	                      <td class="center">Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">Quantity(FOC)</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Total</td>                        
                      </tr>
                        </thead>
                        <tbody>
                            <?php if(count($productDetails) > 0){ $i = 1;  
                            $class = 'odd';  
                            foreach ($productDetails as $products) {
                            $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td class="center">
                                <?php if($purchaseInfo['istagged']){ ?>
                                <a><i class="fa fa-unlock success" aria-hidden="true" data-toggle="tooltip" title="Untag" onclick="getTaggedOrders('<?php echo $products['product_id']; ?>','<?php echo $purchaseInfo['purchase_id']; ?>');"></i></a>
                                <a data-toggle="modal" data-target="#ajax-modal-edit" data-backdrop="static" data-keyboard="false" id="openModel"></a>
                              <?php } else echo "-"; ?>
                              </td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['name']; ?>
                                <?php if(!empty($products['childItems'])){
                                    echo "<br>";
                                    foreach ($products['childItems'] as $childItems) {
                                      echo '<span style="color:#2f44a5;">'.$childItems['childsku'].' - '.$childItems['name'].'</span><br>';
                                    }
                                  }
                                ?>
                              </td>
                              <td><?php echo nl2br($products['description']); ?></td>
                              <td><?php echo $products['qty']; ?></td>
                              <td><?php echo $products['foc']; ?></td>
                              <td class="text-right"><?php echo number_format($products['prices'] * $products['conversion_rate'],2); ?></td>
                              <td class="text-right"><?php echo number_format(($products['qty'] *$products['prices']) * $products['conversion_rate'],2); ?></td>
                            </tr>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="8" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['sub_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">Discount</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['discount'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">Delivery Fees</td>
                              <td class="text-right"><?php echo $purchaseInfo['handling_fee']; ?></td>
                            </tr>
                            <tr>
                              <?php $tax_type=$purchaseInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
                                    $tax_type= $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)'; ?>
                              <td colspan="8" class="text-right"><?php echo 'GST '.$tax_type; ?></td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['gst'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">FCTotal</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['fc_nettotal'],2); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>

<div id="ajax-modal-edit" class="modal fade modal-scroll in" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Untag Sales Order Items</h4>
    </div>
    <div class="modal-body">       
        <div class="form-body">
            <div class="form-group">
                <div class="row">
                  <div class="table-responsive">
                    <table class="table table-borderless">
                        <tbody id="tbody"> </tbody>
                    </table>
                  </div>
              </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#" class="btn btn-primary" id="untag_so_items">Un Tag</a>   
      <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>   
    </div>
</div>

<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}
function getTaggedOrders(pid, purchase_id){
    if(pid !='' && purchase_id !=''){
      $.ajax({
          url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/getTaggedOrders&token=<?php echo $this->session->data["token"]; ?>',
          type: 'POST',
          data : {product_id : pid, purchase_id: purchase_id },
          success:function(data){
            var res = JSON.parse(data);
            if (res.status){
                $('#tbody').html(res.msg);
                $("#untag_so_items").html('Un Tag');
                $('#openModel').trigger('click',true);
            } else {
                alert(res.err);
            }
          }
      });
    }
}
$(document).on('click','#untag_so_items', function(){
    var itmes = $("input[name='untaggingItems[]']:checked").map(function(){return $(this).val();}).get();
    if(itmes.length === 0){
        return false;
    }
    $("#untag_so_items").html('<div class="loader"></div>');
    $.ajax({
        url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/untagProduct&token=<?php echo $this->session->data["token"]; ?>',
        type: 'POST',
        data : {itmes : itmes },
        success:function(data){
          var res = JSON.parse(data);
          if (res.status){
              location.reload();
          } else {
              alert(res.err);
          }
        }
    });
});
$(document).on('click', '#select_all', function(){
    if(this.checked){
        $('.untaggingItems').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.untaggingItems').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
    }
});
</script>
<?php echo $footer; ?>