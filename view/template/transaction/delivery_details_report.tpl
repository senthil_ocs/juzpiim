<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Delivery Details Report</h3>       
           </div>
        </div>

         <div>
              <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <span id="filter_date_from_id"><?php echo $data['filter_date_from'];?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_date_to_id"></span><?php echo $data['filter_date_to'];?> </span>
                <?php } ?>
                 <?php if($data['filter_transactionno']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Invoice No - <span id="filter_transactionno_id"></span><?php echo $data['filter_transactionno'];?> </span>
                <?php } ?>

                <?php if($data['filter_location']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Location - <span id="filter_transactionno_id"></span><?php echo $data['filter_location'];?> </span>
                <?php } ?>

              </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 100px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                   <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                                                <td>
                          <input type="hidden" name="page"> 
                          <input type="text" placeholder='Customer Name' name="filter_customer_name" value="<?php echo $filters['filter_customer_name'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" name="filter_contact_number" value="<?php echo $filters['filter_contact_number']; ?>" class="textbox" placeholder="Contact Number">
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox" placeholder="Invoice No" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" placeholder='Do No' name="filter_do_number" value="<?php echo $filters['filter_do_number'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                           <select name="filter_delivery_man" id="filter_delivery_man" class="salesdropdown">
                            <option value="">Delivery Man</option>
                            <?php
                                foreach($salesmanlist as $salesman){?>
                                  <option value="<?php echo $salesman['id'];?>" <?php echo $filters['filter_delivery_man'] == $salesman['id'] ? 'selected' : '' ; ?>> <?php echo $salesman['name']; ?></option>

                            <?php  }  ?> 
                        </select>
                        </td>
                      </tr>
                      <tr class="filter">
                        <td>Delivery Date : <span style="float: right;"><input type="checkbox" name="filter_is_delivery" value="1" <?php if($filters['filter_is_delivery']=='1'){ echo "checked"; } ?> data-toggle="tooltip" title="Filter by delivery date"></span></td>
                        <td>
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                            <option value="Scheduling">Scheduling</option>
                            <option value="Pending_Delivery">Pending Delivery</option>
                            <option value="On_Delivery">On Delivery</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Canceled">Canceled</option>
                          </select>
                        </td>
                          <td align="right">
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>           
            </div>
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                   <?php if ($deliveryHeader) { ?>
                      <?php foreach ($deliveryHeader as $key => $delivery) { ?>
                    <table class="table orderlist statusstock">
                    <tr <?php echo $contact; ?>>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Do No:</strong> <?php echo $delivery['do_no']; ?></span> </td>
                      <td align="right"><span class="print_date"><strong>Invoice No:</strong> <?php echo $delivery['invoice_no']; ?></span></td>
                    </tr>
                    </table>
                        <table class="table orderlist statusstock">
                          <thead>
                            <tr class="heading">

                                <td class="center">SKU</td>
                                <td class="center">Name</td>
                                <td class="center">Customer</td>
                                <td class="center">Delivery Man</td>
                                <td class="center">Qty</td>
                                <td class="center">Delivery Date</td>
                            </tr>
                          </thead>
                          <tbody>

                          <?php $class = 'odd'; ?>
                          <?php
                            foreach ($delivery['deliveryDetails'] as $value) { $i++; ?>
                          <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">
                          <td class="center"><?php echo $value['sku']; ?></td>
                          <td class="center"><?php echo $value['name']; ?></td>
                          <td class="center"><?php echo $delivery['name']; ?></td>
                          <td class="center"><?php echo $delivery['deliveryman']; ?></td>
                          <td class="center"><?php echo $value['qty']; ?></td>
                          <td class="center"><?php echo $delivery['delivery_date']; ?></td> 
                        </tr>
                        <?php } } } else { ?>
                      <table class="table orderlist statusstock">
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                      </table>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">

$(document).ready(function(){
    $("#filter_delivery_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Delivery Status', 
      height:'250px'
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    if(delivery.length > 0){
      for (var i = 0, len = delivery.length; i < len; i++) 
      {
        if(delivery[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+delivery[i]+"]").prop("checked",true);
        }
      }
    }
    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    if(payment.length > 0){
      for (var i = 0, len = payment.length; i < len; i++) 
      {
        if(payment[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+payment[i]+"]").prop("checked",true);
        }
      }
    }
});

function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
</script> 
<?php echo $footer; ?>