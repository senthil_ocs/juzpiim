<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View PO Request Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php 
          foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a href="<?php echo $link_convert; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-angle-double-right"></i>Convert to Purchase</a>

                    <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF / Print</a>
                                        
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Req No</td>
                          <td><?php echo $purchaseInfo['PO_Req_No'] ?></td>
                        </tr>
                        <tr>
                          <td>Transaction Date</td>
                          <td><?php echo $purchaseInfo['PO_Req_Date']; ?></td>
                        </tr>
                        <tr>
                          <td>Vendor Code</td>
                          <td><?php echo $purchaseInfo['Vendor_Code']; ?></td>
                        </tr>
                        <tr>
                          <td>Vendor Name</td>
                          <td><?php echo $purchaseInfo['vendor_name']; ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">Sku Cost</td>
                        <td class="center">Sub Total</td>
                        <td class="center">GST</td>

                        <td class="center">Total Price</td>
                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1; 
                           ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['Sku']; ?></td>
                              <td><?php echo $products['Description']; ?></td>
                              <td><?php echo $products['Qty']; ?></td>
                              <td class="right"><?php echo $products['Skucost']; ?></td>
                              <td class="right"><?php echo $products['Subtotal']; ?></td>
                              <td class="right"><?php echo $products['Gst']; ?></td>
                              <td class="right"><?php echo $products['Nettotal']; ?></td>
                            </tr>
                            <?php $quantity+= $products['Qty']; 
                                  $priceTotal+= $products['Skucost'];
                                  $st+= $products['Subtotal']; 
                                  $gstTotal+= $products['Gst'];
                                  $netPriceTotal+= $products['Nettotal'];

                            ?>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="5" align="right">Total</td>
                              <td class="right"><?php echo number_format($st,2); ?></td>
                              <td class="right"><?php echo number_format($gstTotal,2); ?></td>
                              <td class="right"><?php echo number_format($netPriceTotal,2); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

</script>
<?php echo $footer; ?>