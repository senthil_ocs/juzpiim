<!-- NEW CODE -->
<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a> -->
              <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                  <i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                  <i class="fa fa-file-pdf-o"></i> Export PDF</a>    
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

    <div style="clear:both"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="innerpage-listcontent-blocks"> 
           <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
          <h3 class="page-title">Purchase Return Summary Report</h3>     
          <p>
            <?php if($data['filter_date_from'] || $data['filter_transactionno'] || $data['filter_supplier'] || $data['filter_location_code'])  { ?>
            <span style="font-size: 15px;"> Filter By</span>
            <?php } ?>

          <?php if($data['filter_date_from']) { ?>
                <span style="font-size: 15px;margin-right: 15px;" > From Date - To Date : <?php echo $data['filter_date_from']; ?> - <?php echo $data['filter_date_to']; ?></span>
          <?php } ?>

          <?php if($data['filter_location_code']) { ?>
                <span style="font-size: 15px;margin-right: 15px;" > Location  : <span id="filter_location_codeid"></span></span>
          <?php } ?>

          <?php if($data['filter_transactionno']) { ?>
                <span style="font-size: 15px;margin-right: 15px;" > Transaction No : <?php echo $data['filter_transactionno']; ?></span>
          <?php } ?>

          <?php if($data['filter_supplier']) { ?>
                <span style="font-size: 15px;margin-right: 15px;" > Supplier : <span id="filter_supplierid"></span></span>
          <?php } ?>

        </p>
        </div>
      </div>

      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0;">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                                    </td>                       
                          <td>
                            <input type="text" placeholder='Transaction No' name="filter_transactionno" value="<?php echo $data['filter_transactionno'] ; ?>" class="textbox" autocomplete="off"/>
                                      </td>
                           <td>
                           <select name="filter_location_code" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location_code'] == $value['location_code'])
                                      echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>//location_name

                            <?php  } } ?> 
                        </select>
                        </td>

                           <td>

                      <select name="filter_supplier" class="textbox" style="min-height: 35px; padding: 7px">
                        <option value="">Select Supplier</option>
                          <?php for($i=0;$i<count($vendors);$i++){ ?>
                          <?php if ($vendors[$i]['vendor_id'] == $data['filter_supplier']) { ?>
                        <option value="<?php echo $vendors[$i]['vendor_id'];?>" selected="selected"><?php echo $vendors[$i]['vendor_name'];?></option>
                           <?php } else { ?>
                        <option value="<?php echo $vendors[$i]['vendor_id'];?>"><?php echo $vendors[$i]['vendor_name'];?></option>
                           <?php } ?>
                           <?php } ?>
                      </select>
                    </td>
                    <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>

                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <table class="table orderlist statusstock">
                <tr>
                  <td class="right">Total : <?php echo number_format($purchase_return_grand_total,2); ?></td>
                </tr>
            </table>  
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Tran No</td>
                          <td class="center">Tran Date</td>
                          <td class="center">Location</td>
                          <td class="center">Vendor Name</td>
                          <td class="center">Referece No</td>
                          <td class="center">Reference Date</td>
                          <td class="center">Sub Total</td>
                          <td class="center">GST</td>
                          <td class="center">Net Total</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($purchases) { $i=0; $total=0; ?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($purchases as $purchase) { 
                        $i++; $total+=$purchase['total'];
                        ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><a href="<?php echo $purchase['purchasedetails']; ?>"><?php echo $purchase['transaction_no']; ?></a></td>
                          <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                          <td class="center"><?php echo $purchase['location_code']; ?></td>
                          <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                          <td class="center"><?php echo $purchase['reference_no']; ?></td>
                          <td class="center"><?php echo $purchase['reference_date']; ?></td>
                          <td class="text-right"><?php echo number_format($purchase['sub_total'],2); ?></td>
                          <td class="text-right"><?php echo number_format($purchase['gst'],2); ?></td>
                          <td class="text-right"><?php echo number_format($purchase['total'],2); ?></td>
                         
                        </tr>
                        <?php } ?>
                        <?php  if(count($products_sum)>=1){ ?>
                          <!-- <tr>
                           <td class="right" style="text-align: right;" colspan="5">Grand Total</td>
                           <td class="right"><?php echo $products_sum['purchase_grand_total']; ?></td>
                           <td colspan="1"></td>
                           <td colspan="1"></td>
                          </tr>   -->
                          <?php } ?> 

                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
$(document).ready(function(){
  var filter_supplier = $('[name="filter_supplier"] option:selected').text();
  $( "#filter_supplierid" ).html(filter_supplier);

  var filter_location_code = $('[name="filter_location_code"] option:selected').text();
  $("#filter_location_codeid").html(filter_location_code);

});
//--></script> 
<?php echo $footer; ?>