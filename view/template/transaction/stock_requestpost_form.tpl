<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <?php if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php endif; ?>
        <div class="page-content" >
            <h3 class="page-title">Stock Request Post form </h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>                    
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                  
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" id="cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
        <div class="col-md-12">         
            <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_no; ?></label>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Number" readonly="readonly"><!-- <?php echo $transaction_no; ?> -->
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_dt; ?><span class="required">*</span></label>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="textbox purchase_inputfields" readonly>
                            
                        </td>                        
                    </tr>
                     <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_from_location; ?></label>
                              <input type="hidden" name="from_location_code" id="from_location_code" value="<?php echo $location_details['location_code'];?>">  
                              <input type="text" name="from_location" id="from_location" value="<?php echo $location_details['location_name'].' ('.$location_details['location_code'].')'; ?>" class="textbox purchase_inputfields" readonly="readonly">
                        </td>

                        
                        <td class="purchase_extra order-nopadding"></td>
                         <td class="order-nopadding"> 
                          <label class="purchase_label" for="name"><?php echo $entry_to_location; ?></label>
                        	 <input type="hidden" name="to_location_code" id="to_location_code" value="<?php echo $to_location_id;?>"> 
                           <select name="to_location_id" id="to_location_id" data-ref="to_address" class="selectdropdown" disabled style="margin:5px 0px 5px 0px;" >
                                <option value="">Select Location</option>
                                <?php foreach ($Tolocations as $locationsDetails) { ?>
                                    <?php if($locationsDetails['location_code']!=$location_details['location_code']) {?>
                                        <option value="<?php echo $locationsDetails['location_id']; ?>|<?php echo $locationsDetails['location_code']; ?>" <?php if($to_location_id == $locationsDetails['location_code']){ echo 'selected'; } ?> > 
                                        <?php echo $locationsDetails['location_name'].' ('.$locationsDetails['location_code'].')'; ?></option>
                                    <?php }?>
                                <?php } ?>
                            </select>
                        </td>

                       
                    </tr>
                   
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refno; ?></label>
                             <?php if(count($filter_ref_details)>=1){ ?>
                             <span id="ref_yes">
                                    <select name="reference_no" id="reference_no" class="selectdropdown" style="margin:5px 0px 5px 0px;" onChange="loadRefPurchaseItems(this.value);">
                                        <option value="">ALL</option>
                                        <?php foreach ($filter_ref_details as $ref) { ?>
                                                <?php if($_REQUEST['ref']==$ref['reference_no']){?>
                                                <option value="<?php echo $ref['reference_no'];?>|<?php echo $ref['reference_date'];?>" selected="selected"><?php echo $ref['reference_no'];?></option>
                                                <?php }else{?>
                                                <option value="<?php echo $ref['reference_no'];?>|<?php echo $ref['reference_date'];?>"><?php echo $ref['reference_no'];?></option>
                                                <?php }?>
                                        <?php }?>
                                       
                                    </select>
                              </span>
                               <?php }else{?>
                            <span id="ref_no" <?php if(count($filter_ref_details)>=1){?> style="display: none; <?php }?>">
                                    <input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields" placeholder="Enter ref no">
                            </span>
                            <?php }?>

                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refdate; ?></label>
                             <?php if($error_reference_date) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter ref date"></i>
                                <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox requiredborder purchase_inputfields_error">                        
                            </div>
                            <?php } ?>
                            <?php if(!$error_reference_date) { 
                                    if($_REQUEST['ref_date']!='' && $_REQUEST['ref_date']!='undefined'){
                                       $reference_date = $_REQUEST['ref_date'];
                                    }
                            ?>
                            <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox purchase_inputfields">
                            <?php } ?>
                            <input type="hidden" id="location_fill" name="location_fill" value="<?php echo $location_fill;?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
                <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">No</td>
                            <td class="center">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center"><?php echo $text_product_name; ?></td>
                            <td class="center"><?php echo $text_qty; ?></td>
                             <td class="center">Received Qty</td>
                            <td class="center"><?php echo $text_raw_cost; ?></td>
                            <td class="center">Selling Price</td>
                            <!--<td class="center"><?php echo $text_disc_perc; ?></td>
                            <td class="center"><?php echo $text_disc_price; ?></td>
                            <td class="center"><?php echo $text_net_price; ?></td>
                            <td class="center"><?php echo $text_tax; ?></td>-->
                            <td class="center"><?php echo $text_total; ?></td> 
                        </tr>
                    </thead>                      
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) {$i=1; ?>
                                <?php foreach ($cartPurchaseDetails as $purchase) {  ?>
                                <?php if($apply_tax_type=='2') {
                                $totalValue = round($purchase['total'],2);
                                } else {
                                $totalValue = round($purchase['tax'] + $purchase['total'],2);
                                }
                            ?>
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td class="center"><?php echo $i++; ?></td>
                            <td class="center"><?php echo $purchase['code']; ?>
                                <input type="hidden" name="sku[]" value="<?php echo $purchase['code']; ?>">
                            </td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td align="center"><?php echo $purchase['quantity']; ?></td>
                            <td align="center"><input type="text" name="received_qty[]" value="<?php echo $purchase['quantity']; ?>" class="textbox small_box" style="width: 50px;"></td>
                            <td align="right"><?php echo $purchase['price']; ?></td>
                            <td align="right"><?php echo $purchase['raw_cost']; ?></td>
                            <!--<td class="center">
                            <?php if (($purchase['discount_mode'] == '1') && !empty($purchase['discount'])): echo $purchase['discount']; endif; ?></td>
                            <td class="center"><?php if (($purchase['discount_mode'] == '2') && !empty($purchase['discount'])): echo $purchase['discount']; endif; ?></td>
                            <td class="center"><?php echo $purchase['sub_total']; ?></td>
                            <td class="center"><?php echo $this->currency->format($purchase['tax']); ?></td> -->
                            <td align="right"><?php echo $this->currency->format($totalValue); ?></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="10" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0;display: none;">
                 <?php if (!empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div>
                <div class="innerpage-listcontent-blocks">      
                   
                    <table id="addproductTbl" class="table orderlist statusstock">
                        <?php if (!empty($products)) { ?>
                            
                        <tfoot>
                            <?php $i = 0; ?>
                            <?php if(!empty($purchase_totals)) { ?>
                            <?php foreach ($purchase_totals as $total) { ?>
                                <?php if ($i == 0) { ?>
                                    <tr id ="TR<?php echo $total['code'];?>">
                                        <td class="insertproduct heading-purchase"  align="right" style="text-align: right; margin-top: 2px; margin-right: 5px;">Remarks  &nbsp;&nbsp;</td>
                                        <td class="order-nopadding" style="padding: 5px;" >
                                            <textarea name="remarks" id="remarks" cols="60" rows="4"><?php if (!empty($remarks)){ echo $remarks; } ?></textarea>
                                              <input type="hidden" name="<?php echo $total['code']; ?>"
                                                id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" />
                                        </td>
                                        <td  align="right" class="purchase_total_left" colspan="3">Order Totals</td>
		                                <td class="purchase_total_right insertproduct">
		                                	<input type="text" name="total" id="total" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;">
		                                </td>
                                      
                                    </tr>
                                <?php } ?>                                
                            <?php $i++; } ?>                            
                            <?php } ?>
                        </tfoot> 
                        <?php } else { ?>
                        <tfoot>
                            <tr>
                                <td colspan="10" class="purchase_no_data center"><?php echo $text_select_vendor; ?></td>
                            </tr>
                        </tfoot>
                        <?php } ?>
                    </table>
                </div>
                </div>
            </div>
            </div>
            </form>
         </div>
    </div>

