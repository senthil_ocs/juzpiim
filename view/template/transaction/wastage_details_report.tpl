<!-- NEW CODE -->
<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $exportPDFAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <div style="clear:both"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="innerpage-listcontent-blocks"> 
            <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
          <h3 class="page-title">Wastage Details Report </h3>     
          <p>
            <?php if($data['filter_date_from'] || $data['filter_date_to'] || $data['filter_location']) { ?>
            <span style="font-size: 15px;"> Filter By</span>
            <?php } ?>
          <?php if($data['filter_date_from']) { ?>
                <span style="font-size: 15px;margin-right: 15px;" > From Date - To Date : <?php echo $data['filter_date_from']; ?> - <?php echo $data['filter_date_to']; ?></span>
          <?php } ?>
           <?php if($data['filter_location']){ ?>
                    <span style="font-size: 15px;margin-right: 15px;"> Location - <?php echo $data['filter_location']; ?> </span>
            <?php } ?>

        </p>
        </div>
      </div>

      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
                <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo 
                          $data['filter_date_from']; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                            </td>                       
                          <td>
                      </td>
                      <td>
                           <select name="filter_location_code" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>//location_name

                            <?php  } } ?> 
                        </select>
                        </td>

                      <td align="center" colspan="4">
                        <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                  </div>                        
                  </div>     
                </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                   <?php if ($wastages) { ?>
                      <?php foreach ($wastages as $key => $wastage) { ?>
                    <table class="table orderlist statusstock">
                    <tr <?php echo $contact; ?>>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Wastage No:</strong> <?php echo $wastage['wastage_no']; ?></span> </td>
                      <td align="right"><span class="print_date"><strong>Wastage Date:</strong> <?php echo $wastage['wastage_date']; ?></span></td>
                    </tr>
                    </table>
                        <table class="table orderlist statusstock">
                          <thead>
                            <tr class="heading">
                                <td class="center">Item</td>
                                <td class="center">current stock</td>
                                <td class="center">New stock</td>
                                <td class="center">Stock Difference</td>
                                <td class="center">Sku Cost</td>
                                <td class="center">Sub Total</td>
                                <td class="center">GST</td>
                                <td class="center">Net Total</td>
                                <td class="center">Actual Total</td>
                            </tr>
                          </thead>
                          <tbody>

                          <?php 
                          $i=0;  
                          $subtotal=0;
                          $gst=0;
                          $nettotal=0;
                          $actualtotal=0; 
                          ?>
                          <?php $class = 'odd'; ?>
                          <?php foreach ($wastage['wastages_details'] as $value) { $i++; ?>
                          <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                          <?php 
                          $actualtotal +=$value['actualtotal'];
                          ?>
                        <tr class="<?php echo $class; ?>">
                          <td class="center"><?php echo $value['sku_description']; ?></td>
                          <td class="center"><?php echo $value['current_stock']; ?></td>
                          <td class="center"><?php echo $value['new_stock']; ?></td>
                          <td class="center"><?php echo $value['stock_difference']; ?></td>
                          <td class="center"><?php echo $value['sku_cost']; ?></td>
                          <td class="right"><?php echo $value['subtotal']; ?></td>
                          <td class="right"><?php echo $value['gst']; ?></td>
                          <td class="right"><?php echo $value['nettotal']; ?></td>
                          <td class="right"><?php echo $value['actualtotal']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr><td colspan="8" class="text-right"><b>Total</b></td>
                          <!-- <td class="text-right"><?php echo number_format($subtotal,4); ?></td> -->
<!--                           <td class="text-right"><?php echo number_format($gst,4); ?></td>
                          <td class="text-right"><?php echo number_format($nettotal,4); ?></td> -->
                          <td class="text-right"><?php echo number_format($actualtotal,2); ?></td>
                        </tr>
                      <?php  if(count($wastages_sum)>=1){ ?>
                        <!-- <tr>
                         <td class="right" style="text-align: right;" colspan="8">Grand Total</td>
                         <td class="right"><?php echo $wastages_sum['actual_total']; ?></td>
                        </tr>   -->
                        <?php } ?> 
                      <?php }?>

                      <?php } else { ?>
                      <table class="table orderlist statusstock">
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                      </table>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

function filterReport() {
  url = 'index.php?route=transaction/transaction_reports/wastageDetailsReport&token=<?php echo $token; ?>';
    
  var filter_date_from = $('[name="filter_date_from"]').val();  
  if (filter_date_from != '*') {
    url += '&filter_date_from=' + encodeURIComponent(filter_date_from);
  }
  var filter_location_code = $('[name="filter_location_code"]').val();  
  if (filter_location_code != '*') {
    url += '&filter_location_code=' + encodeURIComponent(filter_location_code);
  }
  
  var filter_date_to = $('[name="filter_date_to"]').val();  
  if (filter_date_to != '*') {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }
  location = url;
}

$(document).ready(function(){
  var filter_supplier = $('[name="filter_supplier"] option:selected').text();
  $( "#filter_supplierid" ).html(filter_supplier);
});

//--></script> 
<?php echo $footer; ?>
