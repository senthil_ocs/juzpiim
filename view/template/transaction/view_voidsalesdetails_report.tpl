<!-- NEW CODE -->
<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">View Void Sales Detail Report</h3>     
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date">
                                    </td>                       
                                    <td>
                     
                    </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Sku</td>
                          <td class="center">Location Code</td>
                          <td class="center">Invoice No</td>
                          <td class="center">Invoice Date</td>
                          <td class="center">Qty</td>
                          <td class="center">Sub Total</td>
                          <td class="center">Discount</td>
                          <td class="center">GST</td>
                          <td class="center">Actual Total</td>
                          <td class="center">Round Off</td>
                          <td class="center">Net Total</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['sku']; ?></td>
                          <td class="center"><?php echo $sale['location_code']; ?></td>
                          <td class="center"><?php echo $sale['invoice_no']; ?></td>
                          <td class="center"><?php echo $sale['invoice_date']; ?></td>
                          <td class="center"><?php echo $sale['qty']; ?></td>
                          <td class="center"><?php echo $sale['sub_total']; ?></td>
                          <td class="center"><?php echo $sale['discount']; ?></td>
                          <td class="center"><?php echo $sale['gst']; ?></td>
                          <td class="center"><?php echo $sale['actual_total']; ?></td>
                          <td class="center"><?php echo $sale['round_off']; ?></td>
                          <td class="center"><?php echo $sale['net_total']; ?></td>
                        </tr>
                        <?php 
                          $strQty+=$sale['qty'];
                          $strSubtotal+=$sale['sub_total'];
                          $strDiscount+=$sale['discount'];
                          $strGst+=$sale['gst'];
                          $strActualtotal+=$sale['actual_total'];
                          $strRoundoff+=$sale['round_off'];
                          $strNettotal+=$sale['net_total'];
                        ?>
                        <?php } ?>
                        <tr>
                          <td colspan="4" align="right">Total:</td>
                          <td><?php echo number_format($strQty,3); ?></td>
                          <td><?php echo number_format($strSubtotal,3); ?></td>
                          <td><?php echo number_format($strDiscount,3); ?></td>
                          <td><?php echo number_format($strGst,3); ?></td>
                          <td><?php echo number_format($strActualtotal,3); ?></td>
                          <td><?php echo number_format($strRoundoff,3); ?></td>
                          <td><?php echo number_format($strNettotal,3); ?></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="11"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
//--></script> 
<?php echo $footer; ?>