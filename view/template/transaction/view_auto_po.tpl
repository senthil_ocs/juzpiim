<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Auto PO Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                	<?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">
			<div class="col-md-12">
			   	<form  method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Po Request No</td>
                          <td><?php echo $po_req_header['PO_Req_No']; ?></td>
                        </tr>
                        <tr>
                          <td>Date</td>
                          <td><?php echo $po_req_header['PO_Req_Date']; ?></td>
                        </tr>
                        <!-- <tr>
                          <td>Transaction Type</td>
                          <td>kjhgjhgdf</td>
                        </tr> -->
                        <tr>
                          <td>Vendor Code</td>
                          <td><?php echo $po_req_header['Vendor_Code']; ?></td>
                        </tr>
                        <tr>
                          <td>Vendor Name</td>
                          <td><?php
                           if($po_req_header['vendor_name'] !='' && isset($po_req_header['vendor_name'])){
                                    $po_req_header['Vendor_Code'] = $po_req_header['vendor_name'];
                             }
                             echo $po_req_header['Vendor_Code']; ?></td>
                        </tr>
                        <tr>
                          <td>Ref No</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Ref Date</td>
                          <td>-</td>
                        </tr>
                        <tr>
                          <td>Remark</td>
                          <td> </td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">PO Request No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">SKU Cost</td>
                        <td class="center">GST</td>
                        <td class="center">SKU Quantity</td>
                        <td class="center">Min Quantity</td>                        
                        <td class="text-right">Net Total</td>
                      </tr>
                        </thead>
                        <tbody>
                <?php if(!empty($po_req_detail)){  $i=1;
                foreach ($po_req_detail as $key ) { 
                          $total += $key['Nettotal'];
                          $gst   += $key['Gst'];
                  ?>
                          <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $key['Po_Req_No']; ?></td>
                            <td><?php echo $key['Sku']; ?></td>
                            <td><?php echo $key['Description']; ?></td>
                            <td>
                                <input type="text" name="" value="<?php echo $key['Qty']; ?>" class="textbox small_box qtyval_<?php echo $key['Sku'];?>" style="min-width: 80%;line-height: 30px;"> 
                                <a class="purchase_label auotpo_qty_change" data-sku="<?php echo $key['Sku'];?>" data-toggle="tooltip" title="Update Quantity" ><i class="fa fa-refresh"></i> </a>
                            </td>
                            <td><?php echo $key['Skucost']; ?></td>
                            <td><?php echo $key['Gst']; ?></td>
                            <td><?php echo $key['sku_qty']; ?></td>
                            <td><?php echo $key['min_qty']; ?></td>
                            <td class="text-right"><?php echo $key['Nettotal']; ?></td>
                            <input type="hidden" value="<?php echo $key['Gst']; ?>" class="gst_<?php echo $key['Sku'];?>">
                            <input type="hidden" value="<?php echo $key['Skucost']; ?>" class="skucost_<?php echo $key['Sku'];?>">
                          </tr>
                <?php $i++; } } ?>
                            <tr>
                              <td colspan="9" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo $total; ?></td>
                            </tr>
                            <tr>
                              <td colspan="9" class="text-right">Discount</td>
                              <td class="text-right">0</td>
                            </tr>
                            <tr>
                              <td colspan="9" class="text-right">GST</td>
                              <td class="text-right"><?php echo $gst; ?></td>
                            </tr>
                            <tr>
                              <td colspan="9" class="text-right">Total</td>
                              <td class="text-right"><?php echo $total; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

$(document).ready(function(){
    $(".auotpo_qty_change").on('click', function(){
        var sku = $(this).attr('data-sku');
        var qty = $('.qtyval_'+sku).val();
        var gst = $('.gst_'+sku).val();
        var skucost = $('.skucost_'+sku).val();

        if(sku !='' && qty !='' ){
            $.ajax({
                url: "index.php?route=transaction/auto_po/update_auto_po&token=<?php echo $token; ?>",
                method:"post",
                data: {sku:sku,qty:qty,gst:gst,skucost:skucost},
                success:function(data){
                    if(data){
                       alert('Update successfully');
                    }else{
                       alert('Cannot update Quantity');
                    }
                }
            });
        }else{
          alert('No details to update!');
        }
    });
});

</script>
<?php echo $footer; ?>