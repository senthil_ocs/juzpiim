<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Stock Adjustment</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>                    
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
        <div class="col-md-12">         
            <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_no; ?><span class="required">*</span></label>
                            <?php if ($error_transaction_no) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Number"></i>
                                <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter Your Transaction Number" readonly="readonly">                                        
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_no) { ?>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Number" readonly="readonly">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_dt; ?><span class="required">*</span></label>
                            <?php if ($error_transaction_date) { ?>
                                <div class="input-icon right div_error">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Date"></i>
                                <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="date textbox requiredborder purchase_inputfields_error">
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_date) { ?>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="date textbox purchase_inputfields">
                            <?php } ?>
                        </td>                        
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor; ?></label>
                           <select name="vendor" id="vendor_code" onchange="getVendorProductDetails(this)">
                            <option value="">-- Select Vendor --</option>
                            <?php if(!empty($vendor_collection)): ?>
                                <?php foreach($vendor_collection as $vendordetails): ?>
                                    <option class="<?php echo $vendordetails['gst']; ?>" value="<?php echo $vendordetails['vendor_id']; ?>"
                                            label="<?php echo $vendordetails['vendor_name']; ?>"
                                     <?php if($vendordetails['vendor_id']==$vendor):?> selected="selected" <?php endif; ?>>
                                        <?php echo $vendordetails['vendor_code']; ?>
                                    </option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                         <input type="hidden" name="vendor_allow_gst" id="vendor_allow_gst" value="">
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor_name; ?></label>
                            <input type="text" name="vendor_name" id="vendor_name" value="<?php echo $vendor_name; ?>" class="textbox purchase_inputfields" readonly="readonly" style="margin: 0 0 0 -4px;">
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refno; ?><span class="required">*</span></label>
                             <?php if($error_reference_no) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter ref no"></i>
                                <input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter ref no">       
                            </div>
                            <?php } ?>
                            <?php if (!$error_reference_no) { ?>
                            <input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields" placeholder="Enter ref no">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refdate; ?><span class="required">*</span></label>
                             <?php if($error_reference_date) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter ref date"></i>
                                <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox requiredborder purchase_inputfields_error">                        
                            </div>
                            <?php } ?>
                            <?php if(!$error_reference_date) { ?>
                            <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox purchase_inputfields">
                            <?php } ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
                <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center"><?php echo $text_product_name; ?></td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center"><?php echo $text_raw_cost; ?></td>
                            <td class="center"><?php echo $text_price; ?></td>
                            <td class="center"><?php echo $text_net_price; ?></td>
                            <td class="center"><?php echo $text_tax; ?></td>
                            <td class="center"><?php echo $text_total; ?></td>
                            <td class="center">Reason</td>
                            <td class="center">Remark</td>
                            <td class="center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>                      
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) { ?>
                                <?php foreach ($cartPurchaseDetails as $purchase) { ?>
                                <?php if($apply_tax_type=='2') {
                                $totalValue = $purchase['total'];
                                } else {
                                $totalValue = $purchase['tax'] + $purchase['total'];
                                }

                                if($purchase['neg_quantity']){
                                    $quantity = $purchase['neg_quantity'];
                                } else {
                                    $quantity = $purchase['quantity'];
                                }
                    
                            ?>
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td class="center"><?php echo $purchase['code']; ?></td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td class="center"><?php echo $quantity; ?></td>
                            <td class="center"><?php echo $purchase['price']; ?></td>
                            <td class="center"><?php echo $purchase['raw_cost']; ?></td>
                            <td class="center"><?php echo $purchase['sub_total']; ?></td>
                            <td class="center"><?php echo $this->currency->format($purchase['tax']); ?></td>
                            <td class="center"><?php echo $this->currency->format($totalValue); ?></td>
                            <td class="center"><?php echo $purchase['reason']; ?></td>
                            <td class="center"><?php echo $purchase['remark']; ?></td>
                            <td class="center">[ <a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"><?php echo $text_remove ?></a> ]</td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="13" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0;display: none;">
                 <?php if (!empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div>
                <div class="innerpage-listcontent-blocks">      
                    <table id="addproductTbl" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center"><?php echo $text_product_name; ?></td>
                            <td class="center"><?php echo $text_weight_class; ?></td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center"><?php echo $text_raw_cost; ?></td>
                            <td class="center"><?php echo $text_price; ?></td>
                            <td class="center"><?php echo $text_disc_perc; ?></td>
                            <td class="center"><?php echo $text_disc_price; ?></td>
                            <td class="center"><?php echo $text_tax_class; ?></td>
                            <td class="center">Reason</td>
                            <td class="center">Remark</td>
                            <td class="center"><?php echo $text_total; ?></td>
                        </tr>
                    </thead>                      
                        <?php if (!empty($products)) { ?>
                        <tbody>
                            <tr>
                                <td class="insertproduct">
                                    <input type="text" name="sku" id="sku" class="textbox small_box"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>
                                                onkeyup="getProductautoFill(this.value);" autocomplete="off">
                                    <input type="hidden" name="product_id" id="product_id"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
                                   <div id="suggesstion-box"></div>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" />
                                </td>
                                <td class="insertproduct" style="padding: 8px !important;">
                                    <select name="weight_class_id" id="weight_class_id">
                                        <?php if (!empty($weight_classes)): ?>
                                            <?php foreach($weight_classes as $weight): ?>
                                                <option value="<?php echo $weight['weight_class_id']; ?>" <?php if (!empty($rowdata) && ($weight['weight_class_id'] == $rowdata['weight_class_id'])): ?> selected="selected" <?php endif; ?>>
                                                    <?php echo $weight['title']; ?>
                                                </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="raw_cost" id="raw_cost" value="<?php if (!empty($rowdata)){ echo $rowdata['raw_cost']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="discount_percentage" id="discount_percentage" onblur="updateRowData('0');" value="<?php if (!empty($rowdata)){ echo $rowdata['discount_percentage']; } ?>" class="textbox small_price_box"<?php if(!empty($hide_discount_percentage)) { ?> readonly="readonly" <?php } ?>>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="discount_price" id="discount_price" onblur="updateRowData('0');" value="<?php if (!empty($rowdata)){ echo $rowdata['discount_price']; } ?>" class="textbox small_price_box"<?php if(!empty($hide_discount_price)) { ?> readonly="readonly" <?php } ?>>
                                </td>
                                <td class="insertproduct" style="padding: 8px !important;">
                                    <select name="tax_class_id" id="tax_class_id" onchange="updateRowData('0');">
                                        <option value="">Select Tax</option>
                                        <?php if (!empty($tax_classes)): ?>
                                            <?php foreach($tax_classes as $tax): ?>
                                                <option value="<?php echo $tax['tax_class_id']; ?>" <?php if (!empty($rowdata) && ($tax['tax_class_id'] == $rowdata['tax_class_id'])): ?> selected="selected" <?php endif; ?>>
                                                    <?php echo $tax['title']; ?>
                                                </option>
                                            <?php endforeach; ?>
                                          <?php endif; ?>
                                    </select>
                                </td>

                                 <td class="insertproduct">
                                    <input type="text" name="reasons" id="reasons" value="<?php if (!empty($rowdata)){ echo $rowdata['reasons']; } ?>" class="textbox small_price_box">
                                </td>

                                <td class="insertproduct">
                                    <input type="text" name="remark" id="remark" value="<?php if (!empty($rowdata)){ echo $rowdata['remark']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="textbox row_total_box" readonly="readonly">
                                </td>   
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="insertproduct purchase_add_button" colspan="12" align="right" style="padding: 10px 0 10px 0 !important;">
                                    <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                                    <a onclick="clearPurchaseData();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                                    
                                </td>
                            </tr>
                            <?php $i = 0; ?>
                            <?php if(!empty($purchase_totals)) { ?>
                            <?php foreach ($purchase_totals as $total) { ?>
                                <?php if ($i == 0) { ?>
                                    <tr id ="TR<?php echo $total['code'];?>">
                                        <td class="insertproduct heading-purchase">Remarks</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="remarks" id="remarks" value="<?php if (!empty($remarks)){ echo $remarks; } ?>" class="textbox remarks_box">
                                        </td>
                                        <td class="insertproduct heading-purchase">Bill Disc(%)</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="bill_discount_percentage" id="bill_discount_percentage"
                                                value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>"
                                                onblur="updateBillDiscount('0');" class="textbox small_box"
                                                <?php if(!empty($hide_bill_discount_percentage)) { ?> readonly="readonly" <?php } ?>>
                                        </td>
                                        <td class="insertproduct heading-purchase">Bill Disc($)</td>
                                        <td class="order-nopadding">
                                            <input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onblur="updateBillDiscount('0');" class="textbox small_box"
                                            <?php if(!empty($hide_bill_discount_price)) { ?> readonly="readonly" <?php } ?>>
                                        </td>
                                        <td colspan="5" align="center" class="purchase_total_left insertproduct heading-purchase">
                                           <?php echo $total['title']; ?>
                                        </td>
                                        <td class="purchase_total_right insertproduct">
                                            <input type="text" name="<?php echo $total['code']; ?>"
                                                id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" />
                                        </td>
                                    </tr>
                                <?php } ?>
                                <?php if ($total['code'] == 'sub_total') { ?>
                                    <input type="hidden" name="sub_total_value"  id="sub_total_value" value="<?php echo $total['value'] ?>" />
                                <?php } ?>
                            <?php $i++; } ?>
                            <tr id="TRtotal" style="background-color: rgb(244, 244, 248);">
                                <td colspan="11" align="right" class="purchase_total_left">Order Totals</td>
                                <td class="purchase_total_right insertproduct">
                                    <input type="text" name="total" id="total" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly">
                                </td>
                            </tr>
                            <?php } ?>
                        </tfoot> 
                        <?php } else { ?>
                        <tfoot>
                            <tr>
                                <td colspan="12" class="purchase_no_data center"><?php echo $text_select_vendor; ?></td>
                            </tr>
                        </tfoot>
                        <?php } ?>
                    </table>
                </div>
                </div>
            </div>
            </div>
            </form>
         </div>
    </div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
    dateFormat: 'yy-mm-dd',
    timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<script type="text/javascript">
$("#searchproduct-key").click(function(){
    $('#popup').bPopup({
        contentContainer:'.payment-page',
        easing: 'easeOutBack',
        speed: 990,
        transition: 'slideDown',
        width:'660',
        height:'660',
        scrollBar:true,
        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->request->get["token"]; ?>'
    });
});
function addProductGrid(){
    $("#addProductLink").on("click",function(){
         var $this = $(this);
        $this.text('See More');
        tableBody = $('#addproductTbl').find("tbody"),
        trLast = tableBody.find("tr:last"),
        trNew = trLast.clone();
        trLast.after(trNew);
});
}
function addProductToForm(pid,sku,name,price){
    $('#sku').val(sku);
    $('#product_id').val(pid);
    $('#name').val(name);
    $('#price').val(price);
    $('#raw_cost').val(price);
    removeAllOverlay();
}
function getProducts(val){
        var page =1;
        var ajaxData = "search_product_text=" + val + "&page=" + page;
        $("#ajaxloader").show();
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',
            data: ajaxData,
            success: function(result) {
                $("#ajaxloader").hide();
                if (result=='') {
                    //
                } else {
                    $('.payment-page').html(result);
                    var el = $("#searchKeynew").get(0);
                    var elemLen = el.value.length;
                    el.selectionStart = elemLen;
                    el.selectionEnd = elemLen;
                    el.focus();
                    return false;
                }
            }
        });
}

$(document).ready(function() {
    $("#close").click(function(){
        $("#popup").fadeOut("fast");
    });
});
function removeAllOverlay() {
    $('.b-close').click();
}
function getVendorProductDetails(ele) {
        var option  = $("option:selected", ele).attr("class");
        var vnamefull = $('#vendor_code').find("option:selected").attr('label');
        var vname = vnamefull.split("||");
        if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
        }else{
            $("#tax_class_id").val('');
             $("#vendor_allow_gst").val(0);
        }
        $("#vendor_name").val(vname);
}
function getVendorProductDetailsxx() { // RAGU Hided for loaded issue fix and added new above this fn
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
                $("#pre-loader-icon").hide();
            }
        }
    });
}
function selectedProduct(val) {
        //alert(val);
        var newVal = val.split("||");
        $("#product_id").val(newVal[0]);
        $("#sku").val(newVal[1]);
        $("#name").val(newVal[2]);
        $("#price").val(newVal[3]);
        $("#raw_cost").val(newVal[4]);
        $("#suggesstion-box").hide();
    }
function getProductautoFill(sku) {
    if(sku.length<=3){
        return false;
    }
    var ajaxData = 'sku='+sku;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetails&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
                $("#suggesstion-box").show();
                $("#suggesstion-box").html(result);
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function getProductDetails() {
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
    //$('#weight_class_id').val('');
    //$('#tax_class_id').val('');
    $('#quantity').val('');
    $('#price').val('');
    $('#raw_cost').val('');
    $('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');
}
function addToPurchase() {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    if(price==0){
        var con= confirm("Do you want to proceed with 0 price?");
        if(con==false){
            return false;
        }
    }

    var quantity   = $('#quantity').val();
    if (product_id && price && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/stock_adjustment/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
            dataType: 'json',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    //jQuery('#page').html(result);
                    tableBody = $('#special').find("tbody"),
                    tableBody.empty().append(result['tbl_data']);
                    if(result['tax_str']!=''){
                        $('#TRtax').remove();
                        var newRow = result['tax_str'];
                        $("#TRsub_total").after(newRow);
                    }
                    //alert(result['sub_total_value']);
                    //location.reload();
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    if(result['sub_total_value']=='0'){
                        //jQuery("#TRtax").remove();
                        $("#TRdiscount").remove();
                    }
                    if(result['tax_value']=='0'){
                        //jQuery("#TRtax").remove();
                    }
                    if(result['discount']=='0'){
                        $("#TRdiscount").remove();
                    }
                    $('#sku').focus();
                        // end
                }
                clearPurchaseData();
                activeTaxClassByVendor();
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
     // code for check vendor has gst value 1
     activeTaxClassByVendor();
    
}
function activeTaxClassByVendor(){
   var option =  $("#vendor_allow_gst").val();
    if(option=='1'){
        var listVal = $('#tax_class_id option:eq(1)').val();
        $("#tax_class_id").val(listVal);
    }else{
        $("#tax_class_id").val('');
    } 
}
function updateRowData(fieldValue) {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    var quantity   = $('#quantity').val();
    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/AjaxupdateRowData&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#row_total').val(result['rowdata']['total']);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            //jQuery('#quantity').focus();
        }else if(raw_cost==''){
            //jQuery('#price').focus();
        }
    }
}
function updateRowDataxx(fieldValue) { // RAGU Hided for loaded issue fix and added new above this fn
    var product_id = $('#product_id').val();
    var price   = $('#price').val();
    var quantity   = $('#quantity').val();
    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
}
function removeProductData(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }
            }
        }
    });
}
function removeProductDataxx(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                //window.location.reload();
                //jQuery('#page').html(result);
            }
        }
    });
}
function updateBillDiscount(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    if(result['discountRow']){
                        $("#TRsub_total").after(result['discountRow']);
                    }
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    $('#bill_discount_percentage').val('');
                    $('#bill_discount_price').val('');
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}
function updateBillDiscountxx(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
    $(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});
    
    $('html').click(function(e){
    if (e.target.id != 'country-list') {
        $("#suggesstion-box").hide(); 
    }
});

window.onload = function(e){ 
     var option  = $("#vendor_code option:selected").attr("class");
     if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
     }   
}


</script>
<div id="ajax-modal" class="modal container fade" tabindex="-1">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Available Products</h4>
        </div>
        <div class="modal-body">
            Loading........
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>