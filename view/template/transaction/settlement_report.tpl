<?php echo $header; ?>
<style>
  #priceAlign{text-align: right;width: 5%;}
</style>
<div class="clearfix"></div>
<div class="page-container">
    <?php echo $sidebar; ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"></h3>      
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li>
                        <?php echo $breadcrumb['separator']; ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                    </li>
                    <?php  }?>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="window.print();"><i class="fa fa-print"></i> Print</a> -->
                            <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a> -->
                            <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
                        </div>    
                    </div>
                </div>                          
            </div>

            <div style="clear:both"></div>            
            <div class="row">     
                <div class="col-md-12">      
                    <div class="innerpage-listcontent-blocks hideprint">
                        <table class="table orderlist statusstock">
                            <tr>
                                <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                                    <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <div class="col-md-6"><strong>GST No: </strong>9268354532</div>
                                    <div class="col-md-6">Date: </strong><?php echo date('d/m/Y'); ?></div>
                                </td>
                            </tr>

                            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                            <tr <?php echo $contact; ?>>
                                <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                                <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                            </tr>
                        </table>
                    <div><h1>Settlement Report</h1></div>
                </div>
                <div style="clear:both"></div>     

                <!-- NEW SEARCH  -->
                <div style="margin: 0px auto;max-width: 800px; width: 100%;">
                    <div class="innerpage-listcontent-blocks hideprint">
                        <div class="portlet bordernone" style="margin-bottom:0 !important">
                            <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0;background:none !important;">
                                <div class="caption"> 
                                    <form method="post" name="report_filter">
                                    <input type="hidden" name="type" id="type">                 
                                        <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                                            <tbody>
                                                <tr class="filter">                     
                                                    <td> <input type="text" id='filter_date_from' placeholder='Choose Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date" onblur="hideFilters();" autocomplete="off"></td>
                                                    <td></td>
                                                     <?php if(count($location)>0){ ?>
                                                        <td>
                                                            <select name="filter_location" id="filter_location" class="textbox" onchange="getterminals(this.value);">
                                                              <option value="">Select Location </option>
                                                                <?php foreach($location as $locations){ ?>
                                                                  <option <?php if($_REQUEST['filter_location'] == $locations['location_code'] ){ ?> selected <?php } ?> value="<?php echo $locations['location_code']; ?>"><?php echo $locations['location_name']; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </td>
                                                      <?php } ?>

                                                       <td>
                                                          <select name="filter_teminal" class="textbox" style="min-height: 35px; padding: 7px;" id="filter_teminal" onchange="getCashier(this.value);">
                                                            <option value="0">Select Terminal</option>
                                                              <?php for($i=0;$i<count($terminals);$i++){ ?>
                                                                  <?php if ($terminals[$i]['shift_teminal'] == $_REQUEST['filter_teminal']) { ?>
                                                                              <option value="<?php echo $terminals[$i]['shift_teminal'];?>" selected="selected"><?php echo $terminals[$i]['shift_teminal'];?></option>
                                                                   <?php } else { ?>
                                                                              <option value="<?php echo $terminals[$i]['shift_teminal'];?>"><?php echo $terminals[$i]['shift_teminal'];?></option>
                                                                   <?php } ?>
                                                               <?php } ?>
                                                          </select>
                                                        </td>

                                                        <td>
                                                          <select name="filter_cashier" class="textbox" style="min-height: 35px; padding: 7px;" id="filter_cashier" onchange="getshift(this.value);">
                                                            <option value="0">Select Cashier</option>
                                                              <?php for($i=0;$i<count($cashier);$i++){ ?>
                                                                  <?php if ($cashier[$i]['shift_cashier'] == $_REQUEST['filter_cashier']) { ?>
                                                                            <option value="<?php echo $cashier[$i]['shift_cashier'];?>" selected="selected"><?php echo 
                                                                              $cashier[$i]['shift_cashier'];?></option>
                                                                   <?php } else { ?>
                                                                              <option value="<?php echo $cashier[$i]['shift_cashier'];?>"><?php echo $cashier[$i]['shift_cashier'];?></option>
                                                                   <?php } ?>
                                                               <?php } ?>
                                                          </select>
                                                        </td>

                                                        <td>
                                                          <select name="filter_shift" class="textbox" style="min-height: 35px; padding: 7px;" id="filter_shift">
                                                            <option value="0">Select Shift</option>
                                                              <?php for($i=0;$i<count($shift);$i++){ ?>
                                                                  <?php if ($shift[$i]['shift_no'] == $_REQUEST['filter_shift']) { ?>
                                                                              <option value="<?php echo $shift[$i]['shift_no'];?>" selected="selected"><?php echo 
                                                                              $shift[$i]['shift_no'];?></option>
                                                                   <?php } else { ?>
                                                                              <option value="<?php echo $shift[$i]['shift_no'];?>"><?php echo $shift[$i]['shift_no'];?></option>
                                                                   <?php } ?>
                                                               <?php } ?>
                                                          </select>
                                                        </td>

                                                    <td align="center" colspan="5">
                                                        <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                                                    </td>
                                                    
                                                    <?php if(count($settlementdetails)>0){ ?>
                                                    <td></td>
                                                    <td><a class="btn btn-zone btn-primary printpage" id="printBtn" style="width: 100%; min-height: 36px; border-radius:0 !important;" onClick="printpage('settlement-print');"><i class="fa fa-print"></i> Print</a></td>
                                                    <?php } ?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </form>
                                </div>                        
                            </div>     
                            <div style="clear:both; margin: 0 0 15px 0;"></div>
                        </div>
                    </div>
                
                </div>
                <div style="margin: 0px auto;max-width: 800px; width: 100%;" id="settlement-print">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="innerpage-listcontent-blocks settlement-wrapper">
                                <table class="table orderlist statusstock">
                                    <?php if(count($settlementdetails)>0){ ?>
                                    <tr>
                                        <td colspan="4">
                                            <h4><strong><?php echo $companyInfo['name']; ?></strong></h4>
                                            <p><?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?> <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?><?php if ($companyInfo['city']) { echo ','.$companyInfo['city']; } ?><?php if ($companyInfo['postal_code']) { echo '-('.$companyInfo['postal_code'].')'; } ?></p>
                                            <p>Ph: <?php echo $companyInfo['phone']; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax: <?php echo $companyInfo['fax']; ?></p>
                                            <p>Email: <?php echo $companyInfo['email']; ?></p><p></p>
                                            <p>Web: <?php echo $companyInfo['web_url']; ?></p><p></p>
                                            <p>GST REG NO: <?php echo $companyInfo['business_reg_no']; ?></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="dottoed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td>Shift_refno</td>
                                        <td><?php echo $settlementdetails['shift_regno']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Shift_date</td>
                                        <td><?php echo $settlementdetails['shift_date']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Shift_terminal</td>
                                        <td><?php echo $settlementdetails['shift_teminal']; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Shift_cashier</td>
                                        <td><?php echo $settlementdetails['shift_cashier']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="dottoed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Paymode</strong></td>
                                        <td align="right"><strong>Sys.amt</strong></td>
                                        <td><strong>Mnl.Amt</strong></td>
                                        <td><strong>(+/-)</strong></td>
                                    </tr>
                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                     <?php if($settlementdetails['cashpayment'] !='' || $settlementdetails['cashpayment_mnl']!='' ){ ?>
                                    <tr>
                                        <td>CASH</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['cashpayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['cashpayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['cashpayment_diff']; ?></td>
                                    </tr>
                                     <?php }?>
                                    <?php if($settlementdetails['visapayment'] !='' || $settlementdetails['visapayment_mnl']!=''){ ?>
                                    <tr>
                                        <td>VISA</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['visapayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['visapayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['visapayment_diff']; ?></td>
                                    </tr>
                                    <?php }?>
                                    <?php if($settlementdetails['masterpayment'] !='' || $settlementdetails['masterpayment_mnl']!=''){ ?>
                                    <tr>
                                        <td>MASTER</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['masterpayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['masterpayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['masterpayment_diff']; ?></td>
                                    </tr>
                                    <?php }?>

                                    <?php if($settlementdetails['netspayment'] !='' || $settlementdetails['netspayment_mnl']!='' ){ ?>
                                    <tr>
                                        <td>NETS</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['netspayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['netspayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['netspayment_diff']; ?></td>
                                    </tr>
                                     <?php }?>

                                    <?php if($settlementdetails['redMartpayment'] !='' || $settlementdetails['redMartpayment_mnl']!='' ){ ?>
                                    <tr>
                                        <td>REDMART</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['redMartpayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['redMartpayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['redMartpayment_diff']; ?></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['creditpayment'] !='' || $settlementdetails['creditpayment_mnl']!=''){ ?>
                                    <tr>
                                        <td>CREDIT</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['creditpayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['creditpayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['creditpayment_diff']; ?></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['bankpayment'] !='' || $settlementdetails['bankpayment_mnl']!=''){ ?>
                                    <tr>
                                        <td>BANKTR</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['bankpayment']; ?></td>
                                         <td id="priceAlign"><?php echo $settlementdetails['bankpayment_mnl']; ?></td>
                                         <td id="priceAlign"><?php echo $settlementdetails['bankpayment_diff']; ?></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['chequepayment'] !='' || $settlementdetails['chequepayment_mnl']!=''){ ?>
                                    <tr>
                                        <td>CHEQUE</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['chequepayment']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['chequepayment_mnl']; ?></td>
                                        <td id="priceAlign"><?php echo $settlementdetails['chequepayment_diff']; ?></td>
                                    </tr>
                                    <?php } ?>


                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td height="30"><u>Credit sales Collection Summary</u></td>
                                    </tr>

                                    <?php if($settlementdetails['cashtr_amount'] !=''){ ?>
                                    <tr>
                                        <td>CASH</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['cashtr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['visatr_amount'] !=''){ ?>
                                    <tr>
                                        <td>VISA</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['visatr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($settlementdetails['banktr_amount'] !=''){ ?>
                                    <tr>
                                        <td>BANKTR</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['banktr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['netstr_amount'] !=''){ ?>
                                    <tr>
                                        <td>NETS</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['netstr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                    <?php if($settlementdetails['ccardtr_amount'] !=''){ ?>
                                    <tr>
                                        <td>CCARD</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['ccardtr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>

                                    <?php if($settlementdetails['chequetr_amount'] !=''){ ?>
                                    <tr>
                                        <td>CHEQUE</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['chequetr_amount']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                     <?php if($settlementdetails['creditsales_total'] !=''){ ?>
                                    <tr>
                                        <td>Grand Total</td>
                                        <td id="priceAlign"><?php echo $settlementdetails['creditsales_total']; ?></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php } ?>

                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Sub Total</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['subtotalsales']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Discount</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['discount']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Gst Inclusive 7%</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['gst']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Round Off</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['round_off'] ; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Total Sales</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['nettotalsales']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                    <tr><td height="10"></td></tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Opening Cash Balance</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['opening_cash']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Today Cash Collection</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['cash_collection']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Today Cash Collection from credit sales</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['credit_cash_collection']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Today Deposit to Machine</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['cash_in_machine']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Today Withdraw from Machine</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['cash_out_machine']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Today Closing Balance</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['closing_cash']; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment">Credit Pending</td>
                                        <td colspan="2" align="right"><?php echo $settlementdetails['credit_pending']; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="dashed-border" colspan="4"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="text-alignment"><?php echo $_REQUEST['filter_date_from']; ?></td>
                                        <td colspan="2" align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="right" colspan="4">
                                            <a class="btn btn-zone btn-primary printpage" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage('settlement-print');"><i class="fa fa-print"></i> Print</a>
                                        </td>
                                    </tr>
                                    <?php } else { ?>
                                    <tr><td height="10"></td></tr>
                                    <tr>
                                        <td align="center">Settlement not found!</td>
                                    </tr>
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="caption"></div>          
            <div class="pagination"><?php echo $pagination; ?></div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript"><!--
/*function printpage(){
$(".set-bg-color").hide();
$(".pagination").hide();
window.print();
$(".set-bg-color").show();
$(".pagination").show();
}*/
function printpage(printdivname){
    //var newstr = document.getElementById(printdivname).innerHTML;
    /*var oldstr = document.body.innerHTML;*/
    //document.body.innerHTML = newstr;
    $(".header").hide();
    $(".page-sidebar-wrapper").hide();
    $(".page-bar").hide();
    $(".hideprint").hide();
    $(".printpage").hide();
    window.print();
    //document.body.innerHTML = oldstr;
    $(".header").show();
    $(".page-sidebar-wrapper").show();
    $(".page-bar").show();
    $(".hideprint").show();
    $(".printpage").show();
    //return true;
}
function getterminals(va){
	if(va!=''){
		$.ajax({
	        type: "POST",
	        url: 'index.php?route=transaction/transaction_reports/AjaxloadTerminals&token=<?php echo $token; ?>&locationCode='+va,
	        data: va,
	        dataType: 'json',
	        success: function(result) {
	            if (result=='') {
	                //
	            } else {
	              $('select#filter_teminal').children('option:not(:first)').remove();
	               for (var i = 0; i < result.length; i++) //The json object has lenght
    	            {
        	            var object = result[i]; //You are in the current object
            	        $('#filter_teminal').append('<option value='+object.shift_teminal+'>' + object.shift_teminal  + '</option>'); //now you access the property.
	                }
	            }
	        }
	    });
	}
}
function getCashier(va){
    if(va!=''){
        locationCode     =  $("#filter_location").val();
        filter_date      =  $("#filter_date_from").val();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/transaction_reports/AjaxloadTerminalsCashier&token=<?php echo $token; ?>&locationCode='+locationCode+'&terminal='+va+'&filter_date='+filter_date,
            data: va,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                  $('select#filter_cashier').children('option:not(:first)').remove();
                   for (var i = 0; i < result.length; i++) //The json object has lenght
                    {
                        var object = result[i]; //You are in the current object
                        $('#filter_cashier').append('<option value='+object.shift_cashier+'>' + object.shift_cashier  + '</option>'); //now you access the property.
                    }
                }
            }
        });
    }
}
function getshift(va){
    if(va!=''){
        locationCode =  $("#filter_location").val();
        filter_date  =  $("#filter_date_from").val();
        filter_teminal =  $("#filter_teminal").val();
        filter_cashier =  $("#filter_cashier").val();

        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/transaction_reports/AjaxloadTerminalsShift&token=<?php echo $token; ?>&locationCode='+locationCode+'&terminal='+filter_teminal+'&filter_date='+filter_date+'&filter_cashier'+va,
            data: va,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                  $('select#filter_shift').children('option:not(:first)').remove();
                   for (var i = 0; i < result.length; i++) //The json object has lenght
                    {
                        var object = result[i]; //You are in the current object
                        $('#filter_shift').append('<option value='+object.shift_no+'>' + object.shift_no  + '</option>'); //now you access the property.
                    }
                }
            }
        });
    }
}

function filterReport(report_type) {
document.report_filter.submit();
}
function hideFilters(){
     //$("#filter_teminal").hide();
     //$("#printBtn").hide();
}
function hideFilterslocation(){
     //$("#filter_teminal").hide();
     //$("#printBtn").hide();
}
//--></script> 
<?php echo $footer; ?>    