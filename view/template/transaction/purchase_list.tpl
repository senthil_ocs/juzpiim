<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Purchase Orders</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                      <li>
                      	<?php echo $breadcrumb['separator']; ?>
                          <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                      </li>
                  <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    
                    <button id="export_pdf" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> PDF</span></button>
                    
                    <button id="export_csv" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> CSV</span></button>

                    <button id="copy_btn" type="button" class="btn btn-primary btn-warning" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-copy"></i> Duplicate Order</button>

                    <button id="convert_invoice" type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-repeat"></i> Convert Invoice</button>

                    <?php if($this->user->hasPermission('modify', $route)) { ?>
                      <a href="<?php echo $insert; ?>"><button type="button" name="purchase_button" value="submit" class="btn btn-success" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> New Purchase Order</span></button></a>
                    <?php } ?>

                    <button id="bulk_cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px; float: right;"><i class="fa fa-trash"></i><span> Cancel</span></button>

              </div>
            </div>
          </div>               						
      </div>

      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="list_form_validation">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">   
                        <input type="hidden" name="page">                  
                        <td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from']; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $data['filter_date_to']; ?>" class="textbox date" autocomplete="off">
                        </td> 
                         <td>
                           <select name="filter_location_code" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Location --</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>
                        <td>
                          <select name="filter_supplier" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Supplier --</option>
                            <?php foreach ($suppliers as $key) { ?>
                              <option value="<?php echo $key['vendor_id'];?>" <?php if($key['vendor_id']==$filter_supplier){ echo "Selected"; } ?>><?php echo $key['vendor_name']; ?></option>
                            <?php } ?>
                        </select> 
                        </td>
                        <td>
                          <input type="text" name="filter_transactionno" value="<?php echo $filter_transactionno; ?>" class="textbox" placeholder="Transaction / Reference No">
                        </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>                        
            </div> 
          <div style="clear:both; margin: 0 0 15px 0;"></div>
          </div>
      </div>

			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
        		<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
        			<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			    <?php } ?>
			    <?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			    <?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <input type="hidden" name="filterStr" value="<?php echo $filterStr; ?>">
                <div class="innerpage-listcontent-blocks"> 
				        <table class="table orderlist statusstock" id="table_sort" data-order='[[ 1, "asc" ]]'>
                  <thead>
                     <tr class="heading">
                      <td width="1" style="text-align: center;"><input type="checkbox" id="select_all" name="select_all" /></td>
                      <td class="center">S.No</td>
                      <td class="center">Order No</td>
                      <td class="center">Order Date</td>
                      <td class="center"><?php echo $text_location; ?></td>
        					    <td class="center">Vendor</td>
                      <td class="center"><?php echo $text_net_total; ?></td>
                      <td class="center">FC NetTotal</td>
                      <td class="center">Status</td>
                      <td class="center printHide"><?php echo $column_action; ?></td>
                      
					         </tr>
                        </thead>
                        <tbody>
                            <?php if ($purchases) { $i = 1;  ?>
                            <?php $class = 'odd'; ?>
              					<?php foreach ($purchases as $purchase) { ?>
              					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
          					<tr class="<?php echo $class; ?>">
                      <td style="text-align: center;">
                          <input type="checkbox" class="selected_orders" name="selected_orders[]" value="<?php echo $purchase['purchase_id']; ?>">
                      </td>
                      <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
          					  <td class="center"><?php echo $purchase['transaction_no']; ?></td>
                      <td class="center"><?php echo date('d/m/Y',strtotime($purchase['transaction_date'])); ?></td>
                      <td class="center"><?php echo $purchase['location_code']; ?></td>
					            <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="text-right"><?php echo number_format($purchase['total'],2); ?></td>
                      <td class="text-right"><?php echo number_format($purchase['fc_nettotal'],2); ?></td>
                      <td class="center"><?php echo $purchase['status'] == 'Partial' ? 'Required': $purchase['status']; ?></td>
                      
                      <td class="center printHide">
                        <?php if(!$purchase['invoice_no']){ ?>
                       <a href="<?php echo $purchase['modify_button'] ?>"><i class='fa fa-edit'  data-toggle="tooltip" title="Modify"></i></a>
                        <?php } ?>
                       
                       <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" data-toggle="tooltip" title="View"></i></a>
                       
                      <?php if(!$purchase['invoice_no']){ ?>
                       <a href="<?php echo $purchase['delete_button']; ?>" onclick="return confirm('Are you confirm,Delete this!')";><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a>
                        <?php } ?>

                       <a href="<?php echo $purchase['download_button']; ?>"><i class="fa fa-print" aria-hidden="true" data-toggle="tooltip" title="Print" style="font-size: 16px;"></i></a>

                     </td>                       
					</tr>
          <?php $i++; ?>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="12"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
            </table>
            </div>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">

  $( document ).ready(function() {
  $('#table_sort').dataTable({
      "aaSorting" : [[ 1, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [0,4,6,7,9],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
  });

$("#select_all").click(function(){
    if(this.checked){
        $('.selected_orders').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.selected_orders').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
    }
});

   $('.xero').css("cursor",'pointer');
   $( ".xero" ).click(function() {
    var xero = $(this);
    var order_id = $(this).attr('data-purchase_id');
    $('.tdclass_'+order_id).html('<div class="loader"></div>');
    var xeroUrl = '<?php echo XERO_URL; ?>purchaseOrder.php?order_id='+order_id;

    $.get(xeroUrl, 
        function (response, status) {  
          var data = JSON.parse(response);
          if(data.status == 'success'){
            xero.removeClass('xero');
            $('.tdclass_'+order_id).html('<span class="text">Success</span>');
          }else{
            $(this).text('Failed');
            $(this).removeClass('text-primary');
            $(this).addClass('text-danger');
          }
      });
  });
});

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});


$("#list_form_validation").submit(function(){
  var fromdt = $('#from_date').val();
  var todt = $('#to_date').val();
  if(fromdt !='' || todt != ''){
      if(fromdt == ''){
         $('#from_date').focus();
        alert('Please Select the From date');
        return false;
      }
      if(todt == ''){
          $('#to_date').focus();
        alert('Please Select the To date');
         return false;
      }
  }
});

$("#copy_btn").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
      alert('Please select atleast one order to copy.!'); return false;
    }
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/copy_orders&token=<?php echo $this->session->data["token"]; ?>").submit();
});

$('#single_deleteBtn').click(function(){
    var purchase_id = $(this).attr('data-purchase_id');
    purchase_ids = [purchase_id];
    $('input [name="selected"]').val(purchase_ids);
    $('#slect_'+purchase_id).trigger('click');
    $('#form').submit();
});

$("#bulk_cancel").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
        alert('Please select atleast one order to cancel'); 
        return false;
    }
    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/bulk_cancel&token=<?php echo $this->session->data["token"]; ?>',
        data: {selected_orders: selected_orders},
        success: function(result) {
            location.reload(); 
        }
    });
});

$("#export_pdf").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
        alert('Please select atleast one order to export'); 
        return false;
    }
    $.ajax({
        url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/export_pdf&token=<?php echo $this->session->data["token"]; ?>',
        type: 'POST',
        data : {selected_orders : selected_orders},
        success:function(data){
            downloadAll();
        }
    });
});

$("#export_csv").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
        alert('Please select atleast one order to export'); 
        return false;
    }
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/export_csv&token=<?php echo $this->session->data["token"]; ?>").submit();
});

function downloadAll(){
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    $.each( selected_orders, function( key, value ) {
      $.get('download/purchase_'+value+'.pdf').done(function() { 
        var link  = document.createElement('a');
        link.href = 'download/purchase_'+value+'.pdf';
        link.download = "purchase_"+value+".pdf";
        link.click();
        link.remove();
      }).fail(function() { 
          alert(value+'- Not Found!');
      });
    });
    // location.reload();
};

function clearTempFolder(){
  $.ajax({
      url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales/clearTempFolder&token=<?php echo $this->session->data["token"]; ?>',
      type: 'POST',
      success:function(e){
      }
  });
};

$("#convert_invoice").on("click", function(e){
    e.preventDefault();
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/purchase/bulk_convert_invoice&token=<?php echo $this->session->data["token"]; ?>").submit();
});
</script>