<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title"><?php echo $heading_title; ?></h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" name="purchase_button" value="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <?php } ?>
                <?php if ($hold) { ?>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> <?php echo $button_back; ?></button></a>
                    <?php } else { ?>
                    <a href="<?php echo $show_hold_button; ?>"><button type="button" class="btn btn-action btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-ban"></i> <?php echo $button_show_hold; ?></button></a>
                    <?php } ?>
                </div>    
              </div>
          </div>  
			 </div>

<!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="transferlist_form_validation">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                                    </td>                       
                          <?php  if($location_details['location_code']=='HQ'){ ?>
                            <td>                            
                              <select name="filter_from_location" class="textbox" style="min-height: 35px; padding: 7px; width: 100%;">
                                <option value="">From location</option>
                                <?php

                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($_REQUEST['filter_from_location']==$value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>
                                 <?php    }
                                  } 
                                 ?> 
                              </select>
                            </td>
                          <?php }?>
                          <td>
                            <select name="filter_to_location" class="textbox" style="min-height: 35px; padding: 7px; width: 100%;">
                              <option value="">To location</option>
                              <?php
                             
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($_REQUEST['filter_to_location']==$value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                               <?php    }
                                } 
                               ?> 
                            </select>
                          </td>
                          <td>
			                      <select name="filter_accepted_status" class="textbox" style="min-height: 35px; min-width:100px; padding: 7px">
			                        <option value="" <?php if($_REQUEST['filter_accepted_status'] == '')echo 'selected'; ?>> Select Posted </option>
                              <option value="0" <?php if($_REQUEST['filter_accepted_status'] == '0')echo 'selected'; ?> >No </option>
                              <option value="1" <?php if($_REQUEST['filter_accepted_status'] == '1')echo 'selected'; ?> > Yes </option>                             
			                      </select>
                   			 </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
			<div style="clear:both;margin: 0 0 15px 0;"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
            

				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
                            <td class="center">S.No</td>
                            <td class="center">Trans No</td>
                            <td class="center">Date</td>
                            <td class="center">From Location</td>
                            <td class="center">To Location</td>
                            <td class="center">Transfer Value</td>
                            <td class="center">Post Status</td>
                            <td class="center">Ref No</td>
                            <td class="center">Ref Date</td>
                            
                            <td class="center">Action</td>
                      
					         </tr>
                        </thead>
                        <tbody>
                            <?php if ($purchases) { $i = 1; ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($purchases as $purchase) { 

          ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
            <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
					  <td class="center"><?php echo $purchase['transaction_no']; ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['from_location']; ?></td>
                      <td class="center"><?php echo $purchase['to_location']; ?></td>
                      <td class="center"><?php echo $purchase['total']; ?></td>
                      <td class="center <?php if($purchase['posted']=='Yes'){?> greentxt <?php } else { ?> redtxt <?php } ?>"><?php echo $purchase['posted']; ?></td>
                      <td class="center"><?php echo $purchase['reference_no']; ?></td>
                      <td class="center"><?php echo $purchase['reference_date']; ?></td>
                      
                      <td class="center">
                      <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="View"></i></a>
                      <?php if($hold=='1' && $purchase['posted']=='No'){?>
                      <a href="<?php echo $purchase['modify_button'] ?>"><i class='fa fa-edit' style='font-size:19px' data-toggle="tooltip" title="Modify"></i></a>                     
                      <?php }?>
                      <?php if($hold=='0' && $purchase['posted']=='No'){?>
                      <a href="<?php echo $purchase['posted_button'] ?>"><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></a>                  
                      <?php }?>
                      </td>


					</tr>
					<?php $i++;  } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$("#transferlist_form_validation").submit(function(){
  var fromdt = $('#from_date').val();
  var todt = $('#to_date').val();
  if(fromdt !='' || todt != ''){
      if(fromdt == ''){
         $('#from_date').focus();
        alert('Please Select the From date');
        return false;
      }
      if(todt == ''){
          $('#to_date').focus();
        alert('Please Select the To date');
         return false;
      }
  }
});

</script> 
