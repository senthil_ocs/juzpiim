<?php echo $header; ?>
<head>
<meta charset="ISO-8859-1">

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css"rel="stylesheet">
<style type="text/css">
 .modal-scrollable{
    z-index: 10050 !important;
  }
  #sorttable{cursor: pointer;}
</style>
</head>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Delivery List</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                  
                  <button  class="btn btn-primary" id="export_pdf" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> PDF</span></button>

                  <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> New Delivery</span></button></a>
                  <button id="bulk_cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px; float: right;"><i class="fa fa-trash"></i><span> Cancel</span></button>

                <?php } ?>
                  </div>
               </div>
            </div>
      </div>

        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 105px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption" style="width: 98%;">
              <form method="post" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                        <td>
                          <input type="hidden" name="page"> 
                          <input type="text" placeholder='Customer Name' name="filter_customer_name" value="<?php echo $filters['filter_customer_name'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" name="filter_contact_number" value="<?php echo $filters['filter_contact_number']; ?>" class="textbox" placeholder="Contact Number">
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox" placeholder="Invoice No" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" placeholder='Do No' name="filter_do_number" value="<?php echo $filters['filter_do_number'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                           <select name="filter_delivery_man" id="filter_delivery_man" class="salesdropdown">
                            <option value="">Delivery Man</option>
                            <?php
                                foreach($salesmanlist as $salesman){?>
                                  <option value="<?php echo $salesman['id'];?>" <?php echo $filters['filter_delivery_man'] == $salesman['id'] ? 'selected' : '' ; ?>> <?php echo $salesman['name']; ?></option>

                            <?php  }  ?> 
                        </select>
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                            <option value="Scheduling" <?php if(in_array('Scheduling',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Scheduling</option>
                            <option value="Pending_Delivery" <?php if(in_array('Pending_Delivery',$filters['filter_delivery_status'])){ echo "selected"; }?>>Pending Delivery</option>
                            <option value="On_Delivery" <?php if(in_array('On_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>On Delivery</option>
                            <option value="Delivered" <?php if(in_array('Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Delivered</option>
                            <option value="Canceled" <?php if(in_array('Canceled',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Canceled</option>
                            <option value="Faild_Delivery" <?php if(in_array('Faild_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Faild Delivery</option>
                          </select>
                        </td>
                      </tr>
                      <tr class="filter">
                        <td>
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_payment_status[]" id="filter_payment_status">
                            <option value="Pending" <?php if(in_array('Pending',$filters['filter_payment_status'])){ echo "selected"; } ?>>Pending</option>
                            <option value="Paid" <?php if(in_array('Paid',$filters['filter_payment_status'])){ echo "selected"; } ?>>Paid</option>
                            <option value="Partial" <?php if(in_array('Partial',$filters['filter_payment_status'])){ echo "selected"; } ?>>Partial</option>
                            <option value="Hold" <?php if(in_array('Hold',$filters['filter_payment_status'])){ echo "selected"; } ?>>Hold</option>
                          </select>
                        </td>
                        <td colspan="3" align="right">
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                </tbody>
              </table>
            </div>                        
          </div>     
        </form>
      <div style="clear:both; margin: 0 0 15px 0;"></div>
    </div>
  </div>

			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success" style="margin: 10px 10px 0px 0px;"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 12px;margin-top: 12px;"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<form method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
            				<table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr class="heading">
                        <td width="1" style="text-align: center;">
                           <input type="checkbox" id="select_all" name="select_all" />
                         </td> 
                        <td class="center">S.No</td>
                        <td class="center">Invoice No</td>
                        <td class="center">DO No</td>
                        <td class="center">From</td>
                        <td class="center">Customer</td>
                        <td class="center">Shipping Address</td>
                        <td class="center">Route</td>
                        <td class="center">Date</td>
                        <td class="center">Delivery Man</td>
            					  <td class="center">Status</td>
                        <td class="center">Payment</td>
                        <td class="center">Remarks (C)</td>
                        <td class="center">Paid / Balance</td>
                        <td class="center">Action</td>
             					</tr>
                        </thead>
                        <tbody id="sorttable">

                      <?php if ($delivery) { ?>
                      <?php $class = 'odd'; $i=1; ?>
            					<?php foreach ($delivery as $delivery) { ?>
            					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
            					<tr class="<?php echo $class; ?>" data-do="<?php echo $delivery['do_no']; ?>">
                       <td style="text-align: center;">
                         <input type="checkbox" class="selected_orders" name="selected_orders[]" value="<?php echo $delivery['do_no']; ?>"> 
                      </td>
                      <td class="center"><?php echo $delivery['pagecnt']+$i; ?></td>
				          	  <td class="center"><?php echo $delivery['invoice_no']; ?></td>
                      <td class="center"><?php echo $delivery['do_no']; ?></td>
                      <td class="center"><?php echo showText($delivery['from']); ?></td>
                      <td class="center"><?php echo $delivery['vendor_name']; ?></td>
                      <td class="center"><?php echo $delivery['shipping_address']; ?></td>
                      <td class="center"><?php echo $delivery['route_code']; ?></td>
                      <td class="center"><?php echo $delivery['assignedon']; ?></td>
                      <td class="center"><?php echo $delivery['deliveryman']; ?></td>
                      <td class="center" id="delivery_<?php echo $delivery['do_no']; ?>"><?php echo showText($delivery['status']); ?></td>
                      <td class="center"><?php echo $delivery['payment_status']; ?></td>
                      <td class="center"><?php echo $delivery['reason']; ?></td>
                      <td class="center"><?php echo $delivery['paidBalance']; ?></td>
                      <td align="center">
                          <?php if($delivery['status'] !='Delivered' && $delivery['status'] !='Canceled'){ ?>
                            <a id="edit_<?php echo $delivery['do_no']; ?>"><i class="fa fa-edit" aria-hidden="true" data-toggle="tooltip" title="Edit" onclick="updateDeliveryMan_Date('<?php echo $delivery['do_no']; ?>');"></i></a>
                            <a data-toggle="modal" data-target="#ajax-modal-qedit" data-backdrop="static" data-keyboard="false" id="openModel"></a>
                          <?php } ?>

                          <a href="<?php echo $delivery['download_button']; ?>"><i class="fa fa-print" data-toggle="tooltip" title="Download DO"></i></a>
                          
                          <?php if($delivery['status'] !='Delivered' && $delivery['status'] !='Canceled'){ ?>
                            <a id="cancel_<?php echo $delivery['do_no']; ?>"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Cancel" onclick="CancelDelivery('<?php echo $delivery['do_no']; ?>');"></i>
                          <?php } ?>
                          <?php if($delivery['status'] == 'Delivered'){ ?>
                              <a href="#" data-image="<?php echo $delivery['signature']; ?>" data-proof="<?php echo $delivery['proofimg']; ?>" class=" imagePopupBtn"  data-toggle="tooltip" title="View Signature">  <i class='fa fa-image' style="font-size:14px;"></i> </a>
                          <?php } ?>
                      </td>
					</tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="14"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
            </table>
           </div>
           </form>
       <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>

<div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Update Delivery for : <span id="popup_header"></span></h4>
    </div>
    <div class="modal-body">       
        <div class="form-body">
            <div id="qedit_error" class=""></div>
            <div class="form-group">
                <div class="row">     
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="inputsm">Select Delivery Man</label>
                      <select name="delivery_man" id="delivery_man" class="salesdropdown form-control">
                      <option value="">Select Delivery Man</option>
                      <?php
                          foreach($salesmanlist as $salesman){ ?>
                            <option value="<?php echo $salesman['id'];?>"> <?php echo $salesman['name']; ?></option>
                      <?php } ?>
                    </select>
                    </div>
                    <div class="form-group">
                      <label for="inputsm">Remarks</label>
                      <textarea name="delivery_remarks" id="delivery_remarks" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="inputsm">Delivery Date</label>
                      <input type="hidden" id="do_no">
                      <input type="text" id="delivery_date" name="delivery_date" class="form-control date" autocomplete="off">
                    </div>
                    <div class="form-group">
                      <label for="inputsm">Delivery Status</label>
                      <select id="delivery_status" name="delivery_status" class="form-control">
                        <option value="">Select Delivery Status</option>
                        <option value="Scheduling">Scheduling</option>
                        <option value="Pending_Delivery">Pending Delivery</option>
                        <option value="On_Delivery">On Delivery</option>
                        <option value="Delivered">Delivered</option>
                      </select>
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
      <a href="#" class="btn btn-primary" id="update_delivery">Save</a>   
      <a href="#" class="btn btn-danger" id="close_model" data-dismiss="modal">Close</a>   
    </div>
</div>

<div id="ajax-modal" class="modal fade modal-scroll in" tabindex="-1">
  <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="form-body" id="imagePopup"></div>
    <div class="form-body" id="imageProof"></div>
    <div class="modal-footer">
      <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
  </div>
</div>

<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script type="text/javascript">
$('#delivery_date').datepicker({
  minDate : 0,
  dateFormat: 'dd/mm/yy'
});

$('.date').datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: 'dd/mm/yy'
});

$(document).ready(function(){

    $("#filter_delivery_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Delivery Status', 
      height:'250px'
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    if(delivery.length > 0){
      for (var i = 0, len = delivery.length; i < len; i++) 
      {
        if(delivery[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+delivery[i]+"]").prop("checked",true);
        }
      }
    }
    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    if(payment.length > 0){
      for (var i = 0, len = payment.length; i < len; i++) 
      {
        if(payment[i] != ''){
            $("input[type=checkbox][class='mulinput'][value="+payment[i]+"]").prop("checked",true);
        }
      }
    }

  $('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [0,5,6,12,13,14],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
  });
  $( "#sorttable").sortable({
    placeholder : "ui-state-highlight",
      update  : function(event, ui){
        var dono = new Array();
        $('#sorttable tr').each(function(){
          dono.push($(this).attr('data-do'));
        });
        $.ajax({
         type: "POST",
         url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/getlistsort&token=<?php echo $this->session->data["token"]; ?>' ,
         data:{dono:dono},
         success:function(data)
         {
             //
         }
        });
      }
  });

});

$("#select_all").click(function(){
    if(this.checked){
        $('.selected_orders').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.selected_orders').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
    }
});

function updateDeliveryMan_Date(do_no){
  $('#popup_header').html(do_no);
  
    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/getDeliveryManAndDate&token=<?php echo $this->session->data["token"]; ?>',
        data: {do_no: do_no},
        success: function(data) {
          data = JSON.parse(data);
          if(data.status){
              $('#delivery_man').val(data.delivery_man);
              $('#do_no').val(do_no);
              $('#delivery_date').val(data.delivery_date);
              $('#delivery_remarks').val(data.delivery_remarks);
              $('#delivery_status').val(data.delivery_status);
              $('#openModel').trigger('click',true);
          }
        }
    });
}
$('#delivery_man').change(function(){
      if($('#delivery_man').val() == ''){
          $('#delivery_status').val('Pending_Delivery')
      }
});
$("#delivery_date").change(function(){
    var delDate = $("#delivery_date").val();
    var d     = new Date();
    var month = d.getMonth()+1;
    var day   = d.getDate();
    var curDate =  (day<10 ? '0' : '') + day + '/' + (month<10 ? '0' : '') + month +'/'+ d.getFullYear();

    if(delDate == curDate){
        $('#delivery_status').val('On_Delivery');
    }
    else if(delDate > curDate){
        $('#delivery_status').val('Scheduling');
    }
});

$('#update_delivery').click(function(){
    var do_no            = $('#do_no').val();
    var delivery_man     = $('#delivery_man').val();
    var delivery_date    = $('#delivery_date').val();
    var delivery_remarks = $('#delivery_remarks').val();
    var delivery_status  = $('#delivery_status').val();

    if(do_no){
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/updateDelivery&token=<?php echo $this->session->data["token"]; ?>',
          data: {do_no: do_no,delivery_man:delivery_man,delivery_date:delivery_date,delivery_remarks:delivery_remarks, delivery_status: delivery_status},
          success: function(data) {
            data = JSON.parse(data);
            if(data.status){
                location.reload();
            }
          }
      }); 
    }
});

function CancelDelivery(do_no){
  if(do_no){
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/CancelDelivery&token=<?php echo $this->session->data["token"]; ?>',
          data: {do_no: do_no},
          beforeSend: function() {
            $('#cancel_'+do_no).html('<div class="loader"></div>');
          },
          success: function(data) {
            data = JSON.parse(data);
            if(data.status){
                alert('DO Canceled '+do_no);
                $('#edit_'+do_no).html('');
                $('#cancel_'+do_no).html('');
                $('#delivery_'+do_no).html('Canceled');
            }
          }
      });
  }
}

$("#bulk_cancel").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();

    if(selected_orders.length===0){
        alert('Please select atleast one DO to cancel'); 
        return false;
    }
    if (!confirm('Are you sure cancel?')) return false;

    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/bulk_cancel&token=<?php echo $this->session->data["token"]; ?>',
        data: {selected_orders: selected_orders},
        success: function(result) {
            location.reload();  
        }
    });
});

$("#export_pdf").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();

    $.ajax({
        url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/delivery/export_pdf&token=<?php echo $this->session->data["token"]; ?>',
        type: 'POST',
        data : {selected_orders : selected_orders},
        success:function(data){
          var link  = document.createElement('a');
          link.href = 'download/sales_do_list.pdf';
          link.download = "sales_do_list.pdf";
          link.click();
          link.remove();
        }
    });
});

$('.imagePopupBtn').click(function(){
  var image = proof = '';
  image = $(this).attr("data-image");
  proof = $(this).attr("data-proof");
  if(proof !=''){
      var proof  = "<?php echo HTTPS_SERVER; ?>doc/proof/"+proof;
      $('#imageProof').html('<img src="'+proof+'" width="500" height="300">');
  }else{
    $('#imageProof').html('');
  }
  var image  = "<?php echo HTTPS_SERVER; ?>doc/signature/"+image;
  $('#imagePopup').html('<img src="'+image+'" width="500" height="300">');
  $('#ajax-modal').modal('show');
});

</script>
<?php echo $footer; ?>