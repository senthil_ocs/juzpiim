<!-- NEW CODE -->
<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<style>
  #priceAlign{text-align: right;}
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Total Sales Summary Report By Outlets</h3>     
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               <a href="<?php echo $link_cspdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF / Print</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
 <!-- company details /header  -->
               <div style="clear:both"></div>            
                <div class="row">     
                   <div class="col-md-12">      
                      <div class="innerpage-listcontent-blocks"> 
                        <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
                      </div>
                      <h3 class="page-title">Total Sales Summary Report By Outlets</h3>   
                      <div>
                            <p>
                              <?php if($filter_date_from != '' || $filter_location!=''){ ?>
                                <span style="font-size: 15px;margin-right: 15px;"> Filter Date From : <span ><?php echo $filter_date_from ?></span> -  <span ><?php echo $filter_date_to ?></span> </span>
                              <?php } ?>
                               <?php if($filter_location!= ''){ ?>
                                <span style="font-size: 15px;margin-right: 15px;"> Location :<span id="filter_location_id"><?php echo $filter_location ?></span> </span>
                              <?php } ?>
                            </p>
                      </div>    
                   </div>
                </div>
              <!-- --> 
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date">
                                    </td>                       
                         <td>
                           <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">All Location</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>//location_name

                            <?php  } } ?> 
                        </select>
                        </td>
                     
                      </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row"> 
            <div style="clear:both"></div>
              <?php if ($error_warning) { ?>
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <div class="alert alert-block alert-danger fade in "><?php echo $error_warning; ?>
                         <button type="button" class="close" data-dismiss="alert"></button>
                  </div>
                </div>
            <?php } ?>

              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks">  
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Date</td>
                          <td class="center">Outlet Name</td>
                          <td class="center">TSB-GST</td> <?php /* Total Sales Before GST */?>
                          <td class="center">GST</td>
                          <td class="center">Discount</td>
                          <td class="center">Round off</td>
                          <td class="center">Total Sales</td>
                          <td class="center">CASH</td>
                          <td class="center">NETS</td>
                          <td class="center">VISA</td>
                          <td class="center">CHEQUE</td>
                          <td class="center">CREDIT</td>
                          <td class="center">BANKTR</td>
                          <td class="center">REDMART</td>
                          <td class="center">FOODPANDA</td>
                          <td class="center">REDEEM</td>
                          <td class="center">MASTER</td>
                          <td class="center">STRIPE</td>

                          <!-- <td class="center">Amount</td>
                          <td class="center">CASH</td>
                          <td class="center">NETS</td>
                          <td class="center">VISA</td>
                          <td class="center">CHEQUE</td>
                          <td class="center">BANKTR</td>
                          <td class="center">CASHB</td>
                          <td class="center">VOUCHER</td> -->
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($sales); ?>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <?php $sale_discrepencies = ($sale['before_gst']+$sale['gst'])-$sale['total_sales']; ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['date']; ?></td>
                          <td class="center"><?php echo $sale['outlet_name']; ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['before_gst'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['gst'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['discount'],3); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['round_off'],3); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['total_sales'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['cash'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['nets'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['visa'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['cheque'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['credit'],2); ?></td>

                          <td class="center" id="priceAlign"><?php echo number_format($sale['banktr'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['redmart'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['foodpanda'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['redeem'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['master'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['stripe'],2); ?></td>

                          <!-- <td class="center" id="priceAlign"><?php echo number_format($sale['CC_payment_amount'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_cash'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_nets'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_visatr'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_cheque'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_banktr'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['CC_cashb'],2); ?></td>
                          <td class="center" id="priceAlign"><?php echo number_format($sale['voucher'],2); ?></td> -->

                          <?php /*  <td class="center"><?php echo number_format(abs($sale_discrepencies),3); ?></td> */ ?>
                        </tr>
                        <?php 
                        $strBeforegst +=$sale['before_gst'];
                        $strGst +=$sale['gst'];
                        $strdiscount +=$sale['discount'];
                        $strroundoff +=$sale['round_off'];
                        $strTotalsales +=$sale['total_sales'];
                        $strCash +=$sale['cash'];
                        $strNets +=$sale['nets'];
                        $strVisa +=$sale['visa'];
                        $strCheque +=$sale['cheque'];
                        $strCredit +=$sale['credit'];

                        $strBanktr +=$sale['banktr'];
                        $strRedmart +=$sale['redmart'];
                        $strVoucher +=$sale['voucher'];
                        $strfoodpanda+=$sale['foodpanda'];
                        $strredeem+=$sale['redeem'];
                        $strmaster+=$sale['master'];
                        $strstripe+=$sale['stripe'];

                        $strCCPaymentAmount+=$sale['CC_payment_amount'];
                        $strCC_cash+=$sale['CC_cash'];
                        $strCC_nets+=$sale['CC_nets'];
                        $strCC_cheque+=$sale['CC_cheque'];
                        $strCC_banktr+=$sale['CC_banktr'];
                        $strCC_visatr+=$sale['CC_visatr'];
                        $strCC_cashb+=$sale['CC_cashb'];


                        $strDiscrepencies +=abs($sale_discrepencies);
                        ?>
                        <?php } ?>
                        <tr>
                          <td colspan="2" align="right"><strong>Total:</td>
                          <td id="priceAlign"><strong><?php echo number_format($strBeforegst,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strGst,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strdiscount,3); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strroundoff,3); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strTotalsales,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCash,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strNets,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strVisa,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCheque,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strCredit,2); ?></strong></td>

                          <td id="priceAlign"><strong><?php echo number_format($strBanktr,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strRedmart,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strfoodpanda,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strredeem,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strmaster,2); ?></strong></td>
                          <td id="priceAlign"><strong><?php echo number_format($strstripe,2); ?></strong></td>
                          

                           <!-- <td id="priceAlign"><strong><?php echo number_format($strCCPaymentAmount,2); ?></strong></td> 
                           <td id="priceAlign"><strong><?php echo number_format($strCC_cash,2); ?></strong></td> 
                           <td id="priceAlign"><strong><?php echo number_format($strCC_nets,2); ?></strong></td> 
                           <td id="priceAlign"><strong><?php echo number_format($strCC_visatr,2); ?></strong></td>
                           <td id="priceAlign"><strong><?php echo number_format($strCC_cheque,2); ?></strong></td>
                           <td id="priceAlign"><strong><?php echo number_format($strCC_banktr,2); ?></strong></td>
                           <td id="priceAlign"><strong><?php echo number_format($strCC_cashb,2); ?></strong></td> 
                           <td id="priceAlign"><strong><?php echo number_format($strVoucher,2); ?></strong></td> -->

                        </tr>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="23"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
      $(document).ready(function(){
      var filter_date_from = $('[name="filter_date_from"]').val();
      $( "#filter_date_from_id" ).html(filter_date_from);

      var filter_date_to = $('[name="filter_date_to"]').val();
      $( "#filter_date_to_id" ).html(filter_date_to);

      var filter_location = $('[name="filter_location"] option:selected').text();
      $( "#filter_location_id" ).html(filter_location);
      });
//--></script> 
<?php echo $footer; ?>