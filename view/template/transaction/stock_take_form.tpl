<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <?php if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php endif; ?>
        <div class="page-content" >
            <h3 class="page-title">Stock Take</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>                    
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
        <div class="col-md-12">         
            <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_id; ?><span class="required">*</span></label>
                            <?php if ($error_transaction_id) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Id"></i>
                                <input type="text" name="transaction_id" id="transaction_id" value="<?php echo $transaction_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter Your Transaction Number" readonly="readonly">                                        
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_id) { ?>
                            <input type="text" name="transaction_id" id="transaction_id" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Id" readonly="readonly">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_dt; ?><span class="required">*</span></label>
                            <?php if ($error_transaction_date) { ?>
                                <div class="input-icon right div_error">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Date"></i>
                                <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class=" textbox requiredborder purchase_inputfields_error" readonly=''>
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_date) { ?>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class=" textbox purchase_inputfields" readonly=''>
                            <?php } ?>
                        </td>                        
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_location; ?><span class="required">*</span></label>
                             <?php if($error_location) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter Location"></i>
                                <input type="text" name="location" id="location" value="<?php echo $location; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter Location" readonly="readonly">       
                            </div>
                            <?php } ?>
                            <?php if (!$error_location) { ?>
                            <input type="text" name="location" id="location_code" value="<?php echo $location; ?>" class="textbox purchase_inputfields" placeholder="Enter Location" readonly="readonly">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_terminal; ?></label>
                            <input type="text" name="terminal" id="terminal" value="<?php echo $terminal; ?>" class="textbox purchase_inputfields">
                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
                <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center"><?php echo $text_product_name; ?></td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>                      
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) { 
                                        foreach ($cartPurchaseDetails as $purchase) { //printArray($purchase); ?>
                            
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td class="center"><?php echo $purchase['code']; ?></td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td class="center"><?php echo $purchase['quantity']; ?></td>
                            <td class="center">[ <a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"><?php echo $text_remove ?></a> ]</td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="11" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0;display: none;">
                 <?php if (!empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div>
                <div class="innerpage-listcontent-blocks">      
                    <table id="addproductTbl" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center" colspan="1" style="width:25%;">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center" colspan="3" style="width:70%;"><?php echo $text_product_name; ?></td>
                            <td class="center" colspan="1" style="width:5%;"><?php echo $text_qty; ?></td>
                        </tr>
                    </thead>                      
                        <?php if (!empty($products)) { ?>
                        <tbody>
                            <tr>
                                <td class="insertproduct">
                                    <input type="text" name="sku" id="sku" class="textbox small_box"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>
                                                onkeyup="getProductautoFill(this.value);">
                                    <input type="hidden" name="product_id" id="product_id"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
                                   <div id="suggesstion-box"></div>
                                </td>
                                <td class="insertproduct" colspan="3">
                                    <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" />
                                    <input type="hidden" name="uom" id="uom" value="" readonly>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="insertproduct purchase_add_button" colspan="5" align="right" style="padding: 10px 0 10px 0 !important;">
                                    <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                                    <a onclick="clearPurchaseData();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                                    
                                </td>
                            </tr>
                            
                        </tfoot> 
                        <?php } else { ?>
                        <tfoot>
                            <tr>
                                <td colspan="10" class="purchase_no_data center"><?php echo $text_select_vendor; ?></td>
                            </tr>
                        </tfoot>
                        <?php } ?>
                    </table>
                </div>
                </div>
            </div>
            </div>
            </form>
         </div>
    </div>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
    dateFormat: 'yy-mm-dd',
    timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script>
<script type="text/javascript">
$("#searchproduct-key").click(function(){
    $('#popup').bPopup({
        contentContainer:'.payment-page',
        easing: 'easeOutBack',
        speed: 990,
        transition: 'slideDown',
        width:'660',
        height:'660',
        scrollBar:true,
        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->request->get["token"]; ?>'
    });
});
function addProductGrid(){
    $("#addProductLink").on("click",function(){
         var $this = $(this);
        $this.text('See More');
        tableBody = $('#addproductTbl').find("tbody"),
        trLast = tableBody.find("tr:last"),
        trNew = trLast.clone();
        trLast.after(trNew);
});
}
function addProductToForm(pid,sku,name,price){
    $('#sku').val(sku);
    $('#product_id').val(pid);
    $('#name').val(name);
    $('#price').val(price);
    $('#raw_cost').val(price);
    removeAllOverlay();
}
function getProducts(val){
        var page =1;
        var ajaxData = "search_product_text=" + val + "&page=" + page;
        $("#ajaxloader").show();
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',
            data: ajaxData,
            success: function(result) {
                $("#ajaxloader").hide();
                if (result=='') {
                    //
                } else {
                    $('.payment-page').html(result);
                    var el = $("#searchKeynew").get(0);
                    var elemLen = el.value.length;
                    el.selectionStart = elemLen;
                    el.selectionEnd = elemLen;
                    el.focus();
                    return false;
                }
            }
        });
}

$(document).ready(function() {
    $("#close").click(function(){
        $("#popup").fadeOut("fast");
    });
});
function removeAllOverlay() {
    $('.b-close').click();
}
function getVendorProductDetails(ele) {
        var option  = $("option:selected", ele).attr("class");
        var vnamefull = $('#vendor_code').find("option:selected").attr('label');
        var vname = vnamefull.split("||");
        /*if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
        }else{
            $("#tax_class_id").val('');
             $("#vendor_allow_gst").val(0);
        }*/
        $("#vendor_name").val(vname);
}
function getVendorProductDetailsxx() { // RAGU Hided for loaded issue fix and added new above this fn
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
                $("#pre-loader-icon").hide();
            }
        }
    });
}
function selectedProduct(val) {
        //alert(val);
        var newVal = val.split("||");
        $("#product_id").val(newVal[0]);
        $("#sku").val(newVal[1]);
        $("#name").val(newVal[2]);
        $("#price").val(newVal[3]);
        $("#raw_cost").val(newVal[4]);
         $("#uom").val(newVal[5]);
        $("#suggesstion-box").hide();
    }
function getProductautoFill(sku) {
	if(sku.length < SEARCH_CHAR_CNT){
		return false;
	}
    var location = $("#location_code").val();
    var ajaxData = 'sku='+sku+'&location_code='+location;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    selectedProduct(result);
                }
                 return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function getProductDetails() {
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function isFloat(n){
    result = (n - Math.floor(n)) !== 0; 
    return result;
}
function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
    //$('#weight_class_id').val('');
    //$('#tax_class_id').val('');
    $('#quantity').val('');
    $('#price').val('');
    $('#raw_cost').val('');
    $('#uom').val('');
    /*$('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');*/
}
function addToPurchase() {
    var product_id = $('#product_id').val();
    //var price      = $('#price').val();
    var vendor_id  = $('#vendor_code').val();
    if(vendor_id==''){
    	alert("Please select Vendor before add Item");
    	return false;
    }

    var uom        = $('#uom').val();
   
    /*if(price==0){
        var con= confirm("Do you want to proceed with 0 price?");
        if(con==false){
            return false;
        }
    }*/

    var quantity   = $('#quantity').val();
     if(uom!='KG' && isFloat(quantity)){
        alert("Not allowed Decimal quantity");
        $('#quantity').val('');
        $('#quantity').focus();
        //return false;        
    }
    if (product_id && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/stock_take/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
            dataType: 'json',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    //jQuery('#page').html(result);
                    tableBody = $('#special').find("tbody"),
                    tableBody.empty().append(result['tbl_data']);
                    /*if(result['tax_str']!=''){
                        $('#TRtax').remove();
                        var newRow = result['tax_str'];
                        $("#TRsub_total").after(newRow);
                    }
                    //alert(result['sub_total_value']);
                    //location.reload();
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    if(result['sub_total_value']=='0'){
                        //jQuery("#TRtax").remove();
                        $("#TRdiscount").remove();
                    }
                    if(result['tax_value']=='0'){
                        //jQuery("#TRtax").remove();
                    }
                    if(result['discount']=='0'){
                        $("#TRdiscount").remove();
                    }*/
                    $('#sku').focus();
                        // end
                }
                clearPurchaseData();
                activeTaxClassByVendor();
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
     // code for check vendor has gst value 1
     activeTaxClassByVendor();
    
}

function removeProductData(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                /*$('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }*/
            }
        }
    });
}

</script>
<?php echo $footer; ?>

<script type="text/javascript">

$(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});

    $('html').click(function(e){
    if (e.target.id != 'country-list') {
        $("#suggesstion-box").hide(); 
    }
});

window.onload = function(e){ 
     var option  = $("#vendor_code option:selected").attr("class");
     if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
     }   
}
var isSubmitting = false
$(document).ready(function () {
    $('#formID').submit(function(){
        isSubmitting = true
    })
    var initial_data = $("#formID").serialize();
    $(window).on('beforeunload', function() {

        if (!isSubmitting && $('form').serialize() != initial_data ){
            return 'You have unsaved changes which will not be saved.'
        }
    });
});

</script>
<div id="ajax-modal" class="modal container fade" tabindex="-1">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Available Products</h4>
        </div>
        <div class="modal-body">
            Loading........
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>