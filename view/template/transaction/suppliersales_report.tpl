<!-- NEW CODE -->
<?php echo $header; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Void Sales Detail Report</h3>      -->
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
                <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <table class="table orderlist statusstock">
                      <tr>
                          <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                      </tr>
                      <tr>
                          <td align="center" colspan="2">
                            <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                            <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                          </td>
                      </tr>
                      <tr>
                          <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                      </tr>
                      <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                      <tr <?php echo $contact; ?>>
                          <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                          <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                      </tr>
                </table>
              </div>
              <h3 class="page-title">Supplier Wise sales report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
        <div>
            <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date From - <span id="filter_date_from_id"><?php echo $data['filter_date_from']?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date To - <span id="filter_date_to_id"><?php echo $data['filter_date_to']?></span> </span>
                <?php } ?>
                <?php if($data['filter_transactionno'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Transaction No - <span id="filter_sku_id"><?php echo $data['filter_transactionno']?></span> </span>
                <?php } ?>
                 <?php if($data['filter_location'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Location - <span id="filter_location_id"><?php echo $data['filter_location']?></span> </span>
                <?php } ?>
                <?php if($data['filter_supplier'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Supplier - <span id="filter_supplier"><?php echo $data['filter_supplier']?></span> </span>
                <?php } ?>
                <?php if($data['filter_channel'] != ''){
                 $filter_network_name = $this->model_transaction_sales->getNetworkName($data['filter_channel']); ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Sales Channel - <?php echo $filter_network_name; ?></span>
                <?php } ?>
            </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">          
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                          </td>
                          <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                                <?php if(count($Tolocations)>=2){?>
                                <option value="">-- Select Location --</option>
                                <?php }?>
                                <?php
                                   if(!empty($Tolocations)){
                                        foreach($Tolocations as $value){ ?>
                                        <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                                <?php  } } ?> 
                             </select>
                          </td>
                          <td>
                            <select name="filter_channel" id="filter_channel" style="min-height: 35px; padding: 7px; width: 100%;">
                              <option value="">Select Sales channel</option>
                              <?php foreach ($channels as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $data['filter_channel']){ echo 'selected'; } ?>> <?php echo $value['name']; ?></option>
                              <?php } ?>
                            </select> 
                          </td>
                          <td>
                            <select class="form-control" name="filter_supplier" id="filter_supplier">
                              <option value="All">Select Supplier</option>
                            <?php if(isset($vendors)){ ?>
                             <?php foreach($vendors as $vendor){ ?>
                              <option value="<?php echo trim($vendor['vendor_code']);?>"<?php if(trim($vendor['vendor_code']) == $data['filter_supplier'])
                              { echo "selected"; } ?> ><?php echo $vendor['vendor_name']; ?></option>
                             <?php } } ?>
                            </select>
                          </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                 </tbody>
                </table>
                </div>                        
              </div>     
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
          </div>
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <?php if(isset($overall_total)){ ?>
            <table class="table orderlist statusstock">
                <tr>
                    <td colspan="10" class="right">Total :
                        <?php echo number_format($overall_total,2); ?>
                    </td>
                </tr>
            </table>
          <?php } ?>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                          <td class="center">SNo</td>
                          <td class="center">SKU</td>
                          <td class="center">Description</td>
                          <td class="center">SKU Price</td>
                          <td class="center">Supplier</td>
                          <td class="center">Sale Qty</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
                        if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $i; ?></td>
                          <td class="center"><?php echo $sale['sku']; ?></td>
                          <td class="center"><?php echo $sale['name']; ?></td>
                          <td class="center"><?php echo $sale['sku_price']; ?></td>
                          <td class="center"><?php echo $sale['sku_vendor_code']; ?></td>
                          <td class="center"><?php echo $sale['saleQty']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="15"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                 </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".print").hide();
  $(".pagination").hide();
  window.print();
  $(".print").show();
  $(".pagination").show();
}

$(document).ready(function(){
  var filter_date_from = $('[name="filter_date_from"]').val();
    $( "#filter_date_from_id" ).html(filter_date_from);

    var filter_date_to = $('[name="filter_date_to"]').val();
    $( "#filter_date_to_id" ).html(filter_date_to);

     var filter_transactionno = $('[name="filter_transactionno"]').val();
    $( "#filter_transactionno_id" ).html(filter_transactionno);

    var filter_location = $('[name="filter_location"] option:selected').text();
    $( "#filter_location_id" ).html(filter_location);
});

</script> 
<?php echo $footer; ?>