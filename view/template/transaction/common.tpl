<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
            
            <?php if(isset($sales)){ ?>
                <div class="portlet bordernone">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i><?php echo $text_supplier; ?>
                        </div>   
                    </div>     
                    <div class="portlet-body bgnone">
                        <?php for($i=0;$i<count($sales);$i++){ ?> 
                        <a href="<?php echo $sales[$i]["link"]; ?>" class="icon-btn"  id="<?php echo  $sales[$i]["id"]; ?>">
                            <span class="shortcutkey"><?php echo $sales[$i]["altkey"];?></span>
                            <i class="<?php echo $sales[$i]["icon"]; ?>"></i>
                        <div>
                            <?php echo $sales[$i]["Text"]; ?>
                        </div>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>

            <?php if(isset($general)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_general; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($general);$i++){ ?> 
					<a href="<?php echo $general[$i]["link"]; ?>" class="icon-btn" id="<?php echo $general[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $general[$i]["altkey"];?></span>
						<i class="<?php echo $general[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $general[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
        <?php } ?>


        <?php if(isset($stock)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_stock; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($stock);$i++){ ?> 
					<a href="<?php echo $stock[$i]["link"]; ?>" class="icon-btn"  id="<?php echo $stock[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $stock[$i]["altkey"];?></span>
						<i class="<?php echo $stock[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $stock[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
            <?php } ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
$(document).ready(function(){
	Mousetrap.bind('alt+p', function(e) {
        e.preventDefault();
        var href = $('#purchase').attr('href');
        //alert(href);
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+r', function(e) {
        e.preventDefault();
        var href = $('#purchase_r').attr('href');
        //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+s', function(e) {
        e.preventDefault();
        var href = $('#sales').attr('href');
       // alert(href);
        window.location.href=href;
        return false;
    });

    Mousetrap.bind('alt+u', function(e) {

        e.preventDefault();
        var href = $('#sales_r').attr('href');
       // alert(href);
        window.location.href=href;

        return false;

    });

     Mousetrap.bind('alt+a', function(e) {
        e.preventDefault();
        var href = $('#stock').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });

     Mousetrap.bind('alt+t', function(e) {

        e.preventDefault();

        var href = $('#stocktake').attr('href');

       //alert(href);

        window.location.href=href;

        return false;

    });

     Mousetrap.bind('alt+d', function(e) {

        e.preventDefault();

        var href = $('#stockupdate').attr('href');

       //alert(href);

        window.location.href=href;

        return false;

    });

});

</script>

