<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Stock Transfer Out Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  
                   <button type="button" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" 
                   id="save_btn" onClick="printDiv('printable')" id="printbtn">
                      <i class="fa  fa-print"></i> <?php echo 'Print'; ?>
                    </button>

                    <?php if($purchaseInfo['revoke_status']!='Yes' AND $purchaseInfo['accepted_status']!='Yes'){?>
                    <a href="<?php echo $revoke_button; ?>">
                            <button name="purchase_button" type="button" class="btn btn-primary"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-close"></i> Revoke </button>
                    </a>
                    <?php }?>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['transfer_no'] ?></td>
                        </tr>
                        <tr>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['createdon'])); ?></td>
                        </tr>                       
                       <!--  <tr>
                          <td>No Item</td>
                          <td><?php echo $purchaseDetail['totalItem']; ?></td>
                        </tr>
                        <tr>
                          <td>No Total Qty</td>
                          <td><?php echo $purchaseDetail['totalQty']; ?></td>
                        </tr> -->
                        <tr>
                          <td>From Location</td>
                          <td><?php echo $purchaseInfo['transfer_from_location']; ?></td>
                        </tr>
                        <tr>
                          <td>To Location</td>
                          <td><?php echo $purchaseInfo['transfer_to_location']; ?></td>
                        </tr>
                         <tr>
                          <td>Accept Status</td>
                          <td><?php echo $purchaseInfo['accepted_status']; ?></td>
                        </tr>

                        <tr>
                          <td>Revoke Status</td>
                          <td><?php echo $purchaseInfo['revoke_status']; ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Product Name</td>
                        <td class="center">Quantity</td>
                        <td class="center">Cost</td>
                        <td class="center">Transfer value</td>
                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1;  ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['sku_description']; ?></td>
                              <td><?php echo $products['sku_qty']; ?></td>
                              <td><?php echo $products['sku_cost']; ?></td>
                              <td><?php echo $products['transfer_value']; ?></td>
                            </tr>
                            <?php $quantity+= $products['sku_qty']; 
                                  $priceTotal+= $products['sku_cost'];
                                  $netPriceTotal+= $products['transfer_value'];

                            ?>
                            <?php $i++;  } ?>
                            <tr>
                              <td colspan="3" align="right">Total</td>
                              <td><?php echo $quantity; ?></td>
                              <td></td>
                              <td><?php echo number_format($netPriceTotal,4); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
            
                <!-- PRINT AREA --> 
            <div id="printable" style="display:none;padding:10px 10px 5px 10px;">   
              <div style="width:100%;margin:0 auto;">
                <?php if (!empty($purchase_bill)) { 
                      ?>
                     <div style="display:inline;">
                       <!--  <span><strong style="text-align:center;">Receipt Printer</span></strong> -->
                     <?php echo $purchase_bill; ?>
                        
                      </div>
                 <!--  <?php for ($i = 0; $i < $info['no_labels']; $i++) { ?>
                    <div style="float:left;display:inline;padding:0px 15px;">
                        <span><strong style="text-align:center;">Receipt Printer</span>
                        <p style="font-weight:normal;"><img src="<?php echo SITE_URL;?>code128.php?text=<?php echo $info['bar_code']; ?>" /> <br/><?php echo $info['bar_code'];?></p>
                        <p><strong style="text-align:center;"> <?php echo $info['price']; ?> </strong></p> 
                    </div>   
                  <?php } ?>  -->                            
                  <?php } else { ?>              
                      <div>
                         <p><?php echo $text_no_results; ?></p>
                      </div>
                  <?php } ?>

              </div>
          </div>
          <!-- PRINT AREA END--> 

			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML; 
     var originalContents = document.body.innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalContents;
  }
</script>
<?php echo $footer; ?>