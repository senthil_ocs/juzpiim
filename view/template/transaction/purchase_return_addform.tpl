<table id="addproductTbl" class="table orderlist statusstock">
    <thead>
        <tr class="heading-purchase">
            <td class="center">SKU</td>
            <td class="center"><?php echo $text_product_name; ?></td>
            <!-- <td class="center"><?php echo $text_weight_class; ?></td> -->
            <td class="center">Purcahase Qty</td>
            <td class="center"><?php echo $text_return_qty; ?></td>
            <td class="center"><?php echo $text_price; ?></td>
            <!-- <td class="center"><?php echo $text_disc_perc; ?></td>
            <td class="center"><?php echo $text_disc_price; ?></td> -->
            <!-- <td class="center"><?php echo $text_tax_class; ?></td> -->
            <td class="center">Location</td>
            <!-- <td class="center">Expiry Date</td> -->
            <td class="center" colspan="2"><?php echo $text_total; ?></td>
        </tr>
    </thead>                      
    <?php //if (!empty($products)) { ?>
    <tbody>
        <tr>
            <td class="insertproduct">
                <select name="product_id" id="product_id" onchange="getProductDetails();" class="textbox">
                    <option value="">Select Code</option>
                    <?php foreach($products as $product): ?>
                        <option value="<?php echo $product['product_id']; ?>" <?php if (!empty($rowdata) && ($product['product_id'] == $rowdata['product_id'])): ?> selected="selected" <?php endif; ?>>
                            <?php echo $product['sku']; ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
            <td class="insertproduct">
                <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" />
            </td>
           
            <td class="insertproduct">
               <input type="text" name="total_quantity" id="total_quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['total_quantity']; } ?>" readonly="readonly" class="textbox small_box">
            </td>
            <td class="insertproduct">
            <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
            </td>
            <td class="insertproduct">
               <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" readonly="readonly" class="textbox small_price_box">
            </td>                                
           
            <td class="insertproduct" colspan="2">
                <input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="textbox row_total_box" readonly="readonly">
            </td>   
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td class="insertproduct purchase_add_button" colspan="8" align="right" style="padding: 10px 0 10px 0 !important;">
                <a onclick="clearPurchaseData();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
            </td>
        </tr>
        <?php $i = 0; ?>
        <?php if(!empty($purchase_totals)) { ?>
        <?php foreach ($purchase_totals as $total) { ?>
        <?php if ($i == 0) { ?>
        <tr id ="TR<?php echo $total['code'];?>">
            <td class="insertproduct heading-purchase">Remarks</td>
            <td class="order-nopadding">
                <input type="text" name="remarks" id="remarks" value="<?php if (!empty($remarks)){ echo $remarks; } ?>" class="textbox remarks_box">
            </td>
            <td class="insertproduct heading-purchase">Bill Disc(%)</td>
            <td class="order-nopadding">
                <input type="text" name="bill_discount_percentage" id="bill_discount_percentage"
                    value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>"
                    onblur="updateBillDiscount('0');" class="textbox small_box"
                    <?php if(!empty($hide_bill_discount_percentage)) { ?> readonly="readonly" <?php } ?>>
            </td>
            <td class="insertproduct heading-purchase">Bill Disc($)</td>
            <td class="order-nopadding">
                <input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onblur="updateBillDiscount('0');" class="textbox small_box"
                <?php if(!empty($hide_bill_discount_price)) { ?> readonly="readonly" <?php } ?>>
            </td>
            <td colspan="1" align="center" class="purchase_total_left insertproduct heading-purchase">
               <?php echo $total['title']; ?>
            </td>
            <td class="purchase_total_right insertproduct">
                <input type="text" name="<?php echo $total['code']; ?>"
                    id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($total['code'] == 'sub_total') { ?>
            <input type="hidden" name="sub_total_value"  id="sub_total_value" value="<?php echo $total['value'] ?>" /><?php } ?>
        <?php $i++; } ?>
        <tr id="TRtotal" style="background-color: rgb(244, 244, 248);">
            <td colspan="6" align="right" class="purchase_total_left">Order Totals</td>
            <td class="purchase_total_right insertproduct">
                <input type="text" name="total" id="total" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly">
            </td>
        </tr>
        <?php } ?>
    </tfoot> 
    <?php /*} else { ?>
    <tfoot>
        <tr>
            <td colspan="10" align="center" class="purchase_no_data"><?php echo $text_select_vendor; ?></td>
        </tr>
    </tfoot>
    <?php }*/ ?>
</table>
<script type="text/javascript">
$(":input").keypress(function(event){
    if (event.which == '10' || event.which == '13') {
        event.preventDefault();
    }
});
$('#expiry_date').datepicker({
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        changeYear: true,
        minDate: 0
        //yearRange: '1990:2016',
    });
</script>