<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Sales Quotation Summary Reports</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a> -->
               <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                 <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Sales Quotation Summary Reports</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display  -->
       <div>
              <p>
                <?php if($data['filter_from_date']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <span id="filter_from_date_id"><?php echo $data['filter_from_date'];?></span> </span>
                <?php } ?>
                <?php if($data['filter_to_date']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_to_date_id"></span><?php echo $data['filter_to_date'];?> </span>
                <?php } ?>
                 <?php if($data['filter_transactionno']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Transaction No - <span id="filter_transactionno_id"></span><?php echo $data['filter_transactionno'];?> </span>
                <?php } ?>

                <?php if($data['filter_channel']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Sales Channel - <?php echo $filter_channel_name; ?> </span>
                <?php } ?>
                <?php if($data['filter_location']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Location - <?php echo $data['filter_location']; ?> </span>
                <?php } ?>
              </p>
        </div>
      <!-- filter display end  -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                          </td>
                          <td>
                           <select name="filter_channel" id="filter_channel" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(!empty($channels)){?>
                            <option value="">Select Sales Channel</option>
                            <?php }?>
                            <?php
                               if(!empty($channels)){
                                    foreach($channels as $channel){ ?>
                                    <option value="<?php echo $channel['id'];?>" <?php if($data['filter_channel'] == $channel['id'])echo 'selected'; ?> > <?php echo $channel['name']; ?></option>

                            <?php  } } ?> 
                          </select>
                          </td>                    
                          <td>
                            <input type="text" placeholder='Transaction No' name="filter_transactionno" value="<?php echo $_REQUEST['filter_transactionno'] ; ?>" class="textbox" autocomplete="off"/>
                          </td>
                          <td>
                           <select name="filter_location" id="filter_location" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Location</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $location){ ?>
                                    <option value="<?php echo $location['location_code'];?>" <?php if($data['filter_location'] == $location['location_code'])echo 'selected'; ?> > <?php echo $location['location_name']; ?></option>

                            <?php  } } ?> 
                          </select>
                          </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <table class="table orderlist statusstock">
                <tr>
                    <td colspan="9" class="right">Total :
                        <?php echo number_format($sales_net_total,2); ?>
                    </td>
                </tr>
            </table>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Location Code</td>
                          <td class="center">Transaction No</td>
                          <td class="center">Transaction Date</td>
                          <td class="center">Sub Total</td>
                          <td class="center">Discount</td>
                          <td class="center">GST</td>
                          <td class="center">Actual Total</td>
                          <td class="center">Round Off</td>
                          <td class="center">Net Total</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($sales); ?>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['location_code']; ?></td>
                          <td class="center"><a href="<?php echo $sale['salesDetail']; ?>"><?php echo $sale['transaction_no']; ?></td>
                          <td class="center"><?php echo $sale['transaction_date']; ?></a></td>
                          <td class="right"><?php echo $sale['sub_total']; ?></td>
                          <td class="right"><?php echo $sale['discount']; ?></td>
                          <td class="right"><?php echo $sale['gst']; ?></td>
                          <td class="right"><?php echo $sale['actual_total']; ?></td>
                          <td class="right"><?php echo $sale['round_off']; ?></td>
                          <td class="right"><?php echo $sale['net_total']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                       
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
    document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
 
//--></script> 
<?php echo $footer; ?>