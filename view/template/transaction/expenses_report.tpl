<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Itemwise Sales Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>

              <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
             <a href="<?php echo $exportPDFAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
             
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Expenses Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
       <div>
            <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From - <span id="filter_date_from_id"><?php echo $data['filter_date_from']?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To - <span id="filter_date_to_id"><?php echo $data['filter_date_to']?></span> </span>
                <?php } ?>
             

                 <?php if($data['filter_location'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Location - <span id="filter_location_id"><?php echo $data['filter_location']?></span> </span>
                <?php } ?>

            </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                                    </td>                       
                          
                         
                          <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px;">
                              <?php if(count($Tolocations)>=2){?>
                              <option value="">-- Select Location --</option>
                              <?php }?>
                              <?php
                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                              <?php  } } ?> 
                          </select>
                          </td>
                                    <td>
                     
                                   </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            <br>
            <table class="table orderlist statusstock">
              <tr>
                
                <td class="right" colspan="6">Total</td>
                <td class="right" style="width:20%;"><?php echo $totalAmount;?></td>
                
              </tr>
            </table> 

            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Ref No</td>
                          <td class="center">Location</td>
                          <td class="center">Terminal</td>
                          <td class="center">Date</td>
                          <td class="center">Remark</td>
                          <td class="center">Shift</td>
                           <td class="center">Amount</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php  if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['payout_refno']; ?></td>
                          <td class="center"><?php echo $sale['payout_location_code']; ?></td>
                          <td class="center"><?php echo $sale['payout_terminal_code']; ?></td>
                          <td class="center"><?php echo $sale['payout_date']; ?></td>
                          <td class="center"><?php echo $sale['payout_remarks']; ?></td>
                          <td class="center"><?php echo $sale['payout_shift']; ?></td>
                          <td class="right"><?php echo $sale['payout_amount']; ?></td>

                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_department_id" ).html(filter_department);

  var filter_location = $('[name="filter_location"] option:selected').text();
  $( "#filter_location_id" ).html(filter_location);

  var filter_category = $('[name="filter_category"] option:selected').text();
  $( "#filter_category_id" ).html(filter_category);

 
});


//--></script> 
<?php echo $footer; ?>