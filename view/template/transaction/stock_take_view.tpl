<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Stock Take Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $update_action;?>" method="post" enctype="multipart/form-data" id="form" name="stocktake">
              <input type="hidden" name="stock_take_id" value="<?php echo $stocktake_id;?>">
              <input type="hidden" name="location_code" value="<?php echo $location_code;?>">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Location Code</td>
                          <td><?php echo $purchaseInfo['location_code'];?></td>
                          <td>Terminal Code</td>
                          <td><?php echo $purchaseInfo['terminal_code'];?></td>
                        </tr>
                        <tr>
                          <td>StockTake Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['stocktake_date'])); ?></td>
                          <td colspan="2"></td>
                        </tr>                       
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Product Name</td>
                         <td class="center">SKU Quantity</td>
                        <td class="center">Scanned Quantity</td>
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails)>0){ $i=1; ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) {
                            ?>
                            <tr>
                              <td class="center"><?php echo $i; ?></td>
                              <td class="center"><?php echo trim($products['sku']); ?></td>
                              <td class="center"><?php echo trim($products['sku_description']); ?></td>
                              <td class="center"><?php echo trim($products['sku_qty']); ?></td>
                              <td class="center"><?php echo trim($products['scanned_qty']); ?></td>
                            </tr>
                            <?php $i++; } ?>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
