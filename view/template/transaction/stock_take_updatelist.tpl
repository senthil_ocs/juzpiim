<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Stock Take List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                
                <button type="button" name="stock_take_button" value="submit" class="btn btn-primary" onclick="postform();"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i><span> Update All</span></button>
               
               </div>    
                    </div>
                </div>                          
      </div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $update_action; ?>" method="post" enctype="multipart/form-data" id="form" name="stocktake">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead><!-- locationcode,terminal code,added date,total items -->
                           <tr class="heading">
                            <td class="center">S.No</td>
                            <td class="center">SKU</td>
                            <td class="center">Location</td>
                            <td class="center">Description</td>
                            <td class="center">SKU Qty</td>
                            <td class="center">Scanned Qty</td>
                      
					               </tr>
                        </thead>
                        <tbody>
                            <?php 

                            if ($stock_take) { $i=1; ?>
                            <?php $class = 'odd'; 
                           
                            ?>
					<?php foreach ($stock_take as $products) {             
           ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
              <td class="center"><?php echo $i; ?></td>
              <td class="center">
              <input type="text" name="postary[sku][]" value="<?php echo trim($products['sku']); ?>" readonly></td>
               <td class="center"><input type="text" name="postary[location_code][]" value="<?php echo trim($products['location_code']); ?>" readonly>  </td>
              <td class="center"><input type="text" name="postary[sku_description][]" value="<?php echo trim($products['sku_description']); ?>" readonly style="width: 100%;"></td>
              <td class="center"><input type="text" name="postary[sku_qty][]" value="<?php echo trim($products['sku_qty']); ?>" readonly></td>
              <td class="center"><input type="text" name="postary[scanned_qty][]" value="<?php echo trim($products['scanned_qty']); ?>" readonly></td>                 
					</tr>
					<?php  $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function postform(){
    var result = confirm("Do you want to update below new Quantity");
    if (result) {
         document.stocktake.submit();
    }     
  }

function updateStock(location_code,sku,description,scanned_qty){
  var ajaxData ='location_code='+location_code+'&sku='+sku+'&description='+description+'&scanned_qty='+scanned_qty;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_take/update&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
              alert("success");
              //  $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });

}
</script> 
<?php echo $footer; ?>