<?php echo $header; ?>

<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title"><?php echo $heading_title; ?></h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                
                <?php } ?></div>

                    </div>
                </div>
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>

			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<div class="innerpage-listcontent-blocks">
				<table class="table orderlist statusstock" id="table_sort">
				  <thead>
					<tr class="heading">
            <td class="center">S.No</td>
            <td class="center"><?php echo $text_tran_no; ?></td>
            <td class="center"><?php echo $text_tran_dt; ?></td>
       		  <td class="center">Sales Channel</td>
            <td class="center">Customer</td>
  				  <td class="center">Network</td>
            <td class="center"><?php echo $text_net_total; ?></td>
            <?php if(HIDE_XERO==1){ ?> 
              <td class="center">Post</td>
            <?php } ?>
             <td class="center printHide"><?php echo $column_action; ?></td>
					</tr>
				  </thead>
				  <tbody>
					<?php if ($purchases) { ?>
					<?php $class = 'odd'; $i=1; ?>
					<?php foreach ($purchases as $purchase) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
                        <!-- <td style="text-align: center;">
                        <?php $checked = ''; if ($product['selected']) { 
                              $checked='checked="checked"'; 
                        }  ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $purchase['transaction_no']; ?>" <?php echo $checked; ?> >
                      </td> -->
                       <td class="center"><?php echo $purchase['pagecnt'] +$i; ?></td>
          					   <td class="center" data-toggle="tooltip" title="<?php echo $purchase['delivery_status']; ?>"><?php echo $purchase['transaction_no']; ?></td>
					             <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                       <td class="center"><?php echo $purchase['location']; ?></td>
                       <td class="center"><?php echo $purchase['vendor_name']; ?></td>
					             <td class="center"><?php echo $purchase['network']; ?></td>
                       <td class="center"><?php echo $purchase['total']; ?></td>
                      <?php if (!$hold): ?>
                        <!-- <td>
                           <?php if($purchase['invoice_nom']!=null){ ?>
                            <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></a>
                        <?php } ?>
                        </td> -->

                      <?php endif; ?>
                      <?php if(HIDE_XERO==1){ ?>
                        <td style="text-align: center;" class="tdclass_<?php echo $purchase['order_id'] ?>">
                       <?php if(!empty($purchase['xero_sales_id'])){ ?>
                        <span class="text">Success</span>
                        <?php }else{ ?>
                           <span class="text-primary xero" data-sales_id="<?php echo $purchase['order_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></span>
                       <?php  } ?>
                       </td>
                      <?php } ?>
                      <td class="center printHide">
                      <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="View"></i></a>
                      <?php if($purchase['xero_sales_id'] == null){ ?>
                        <!-- <a href="<?php echo $purchase['modify_button'] ?>" id="modifyBtn_<?php echo $purchase['order_id']; ?>"><i class='fa fa-edit' style='font-size:19px' data-toggle="tooltip" title="Modify"></i></a> -->
                      <?php } ?>

					</tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="10"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
				</table>
			</div>
			</form>
		  		<div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$(document).ready(function() {
  $('.xero').css("cursor",'pointer');
  
  $('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs"    : [{
            "targets"   : [2,3,5,8],
            "orderable" : false
          },{
          "targets"       : "_all",
          "defaultContent": "-"
        }]
  });
});

$(".xero").click(function(){
    var xero = $(this);
    var order_id = $(this).attr('data-sales_id');
    $('.tdclass_'+order_id).html('<div class="loader"></div>');
    var xeroUrl = '<?php echo XERO_URL; ?>salesReturn.php?order_id='+order_id;

    $.get(xeroUrl, 
        function (response, status, error) {
          if(validJSON(response)){
            var data  = JSON.parse(response);
            if(data.status == 'success'){
              xero.removeClass('xero');
              $('.tdclass_'+order_id).html('<span class="text">Success</span>');
            }else{
              alert('Faild to send xero, Please try again.');
              location.reload();
            }
          }else{
            alert('Faild to send xero, Please try again.');
            location.reload();
          }
    });
});
</script>