<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" />
        <input type="hidden" name="istagged" value="<?php echo $istagged ?>" />
        <div class="page-content" >
            <h3 class="page-title">Sales Order</h3>
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <?php if ($purchase_id && $type!='quote') { ?>
                <a href="<?php echo $convert_invoice; ?>" onclick="return confirm('Do you want to convert this Sales as Sales Invoice');" class="btn btn-info" style="margin: 5px 5px 0 0; border-radius: 4px">Convert Invoice</a>
                <?php } ?>
                <button type="submit" title="Save" name="purchase_button" value="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">
            <div style="clear:both"></div>
            <div class="col-md-12">
              <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding"  style="width: 50%;">
                            <label class="purchase_label_error" for="name">Sales Order Number<span class="required">*</span></label>
                            <input type="hidden" name="sales_quotation_trans_no" value="<?php echo $sales_quotation_trans_no; ?>">
                            <?php if ($error_transaction_no) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Number"></i>
                                <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter Your Transaction Number" readonly="readonly">
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_no) { ?>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Number" readonly="readonly">
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Sales Order Date<span class="required">*</span></label>
                            <?php if ($error_transaction_date) { ?>
                                <div class="input-icon right div_error">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Transaction Date"></i>
                                <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="date textbox requiredborder purchase_inputfields_error">
                            </div>
                            <?php } ?>
                            <?php if (!$error_transaction_date) { ?>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="date textbox purchase_inputfields">
                            <?php } ?>
                        </td>
                    </tr>

                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Customer</label>
                            <div>
                                <input type="hidden" name="vendor" id="vendor_code" value="<?php echo $vendor; ?>">
                                <input type="text" name="vendor_filter" id="vendor_filter" class="textbox purchase_inputfields" autocomplete="off" placeholder="Search Customer">
                                <div id="customerSearch"></div>
                            </div>
                        <?php if($purchase_id == 0){ ?>
                            <span><a href="<?php echo $addCust; ?>"><i class="fa fa-plus-square" data-toggle="tooltip" title="Add New Customer" style="font-size: 18px;float: right;"></i></a></span>
                        <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name"><?php echo $entry_vendor_name; ?></label>
                            <input type="text" name="vendor_name" id="vendor_name" value="<?php echo $vendor_name; ?>" class="textbox purchase_inputfields" readonly="readonly" style="margin: 0 0 0 -4px;">
                        </td>
                    </tr>


                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Reference No<span class="required">*</span></label>
                             <?php if($error_reference_no) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter ref no"></i>
                                <input type="text" name="reference_no" id="reference_no" onblur="validateRefNo();" value="<?php echo $reference_no; ?>" class="textbox requiredborder purchase_inputfields_error" placeholder="Enter reference no" required>
                            </div>
                            <?php } ?>
                            <?php if (!$error_reference_no) { ?>
                            <input type="text" name="reference_no" id="reference_no" onblur="validateRefNo();" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields" placeholder="Enter reference no" required>
                            <?php } ?>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name">Shipping Address <?php echo $shipping_id; ?></label>
                            <select name="shipping_id" id="shipping_id">
                                <option>Select Customer</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                           <label class="purchase_label" for="name">Location & Channel</label>
                           <select name="location_code" id="location_code">
                            <?php if(count($Tolocations) >= 2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ 
                                    ?>
                                    <option value="<?php echo $value['location_code'];?>" 
                                        <?php if($location_code == $value['location_code'])echo 'selected'; ?> > 
                                        <?php echo $value['location_name']; ?>                                       
                                    </option>
                            <?php } }  ?> 
                        </select>
                        
                        <select name="network_id" id="network_id">
                            <?php
                               if(!empty($channels_list)){
                                    foreach($channels_list as $value){ 
                                    ?>
                                    <option value="<?php echo $value['id'];?>" 
                                        <?php if(trim($network_id) == trim($value['id']))echo 'selected'; ?> > 
                                        <?php echo $value['name']; ?>
                                    </option>
                            <?php } }  ?> 
                        </select>

                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name">Delivery Date<span class="required">*</span></label>
                            <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date"> <span class="sm">To Date</span>
                            <input type="text" name="reference_to_date" id="reference_to_date" value="<?php echo $reference_to_date; ?>" class="date">
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="name">Currency Code</label>
                            <select name="currency_code" class="" id="currency_code" onchange="checkCompanyCurrency();" <?php if ($purchase_id && $type!='quote') { echo "disabled"; } ?>>
                            <option value="">-- Select Currency --</option>                            
                            <?php if(!empty($currencys)){
                                foreach ($currencys as $value) { ?>
                                <option value="<?php echo $value['code']; ?>" <?php if($value['code']==$currency_code){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name" id="ConversionRate">Conversion Rate</label>
                            <input type="text" name="conversion_rate" id="conversion_rate" value="<?php echo $conversion_rate; ?>" class="textbox purchase_inputfields" placeholder="Enter Conversion Rate" <?php if ($purchase_id && $type !='quote'){ echo "disabled"; } ?>>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error" for="name">Tax Class<span class="required">*</span></label>
                            <select name="tax_class_id" id="tax_class_id">
                            <?php if(!empty($tax_classes)){
                                foreach ($tax_classes as $value) { ?>
                                <option value="<?php echo $value['tax_class_id']; ?>" <?php if($value['tax_class_id']==$tax_class_id){ echo "selected"; } ?>><?php echo $value['title']; ?></option>
                            <?php } } ?>
                           </select>
                            <select name="tax_type" id="tax_type">
                                <option value="2" <?php if($tax_type=='2'){ echo "Selected"; } ?>> Inclusive </option>               
                                <option value="1" <?php if($tax_type=='1'){ echo "Selected"; } ?>> Exclusive </option>
                           </select>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="term">Term</label>
                            <select name="term_id" id="term_id" onchange="changeRefDate();">
                                <option value="">Select Term</option>
                                <?php foreach ($allTerms as $term) { ?>
                                    <option data-days="<?php echo $term['noof_days']; ?>" value="<?php echo $term['terms_id']; ?>" <?php if($term['terms_id']==$term_id) { echo "selected"; } ?> ><?php echo $term['terms_name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="order-nopadding">            
                            <label class="purchase_label_error" for="name">Attachment</label>
                                <input type="file" name="attachment" id="attachment" class="textbox purchase_inputfields fileUpload">
                                <a href="<?php echo HTTP_SERVER; ?>/uploads/SalesAttachment/<?php echo $attachment; ?>"  class="btn btn-zone btn-primary fileDisplay" target="_blank"> Preview </a>
                                <a onclick="removeAttachment();" class="btn btn-zone btn-danger fileDisplay"><i class="fa fa-times"></i> Remove</a>
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label" for="term">Sales Man</label>
                            <select name="sales_man" id="sales_man">
                                <option value="">Select sales man</option>
                                <?php foreach ($salesmanlist as $sales_mans) { ?>
                                    <option value="<?php echo $sales_mans['id']; ?>" <?php if($sales_mans['id'] == $sales_man){ echo 'selected'; } ?>> <?php echo $sales_mans['name']; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            </div>
                <div class="purchase_grid col-md-12">
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">Product SKU</td>
                            <td class="center" style="width: 25%;"><?php echo $text_product_name; ?></td>
                            <td class="center" style="width: 25%;">Description</td>
                            <td class="center" style="width: 10%;"><?php echo $text_qty; ?></td>
                            <td class="center" style="width: 10%;"><?php echo $text_raw_cost; ?></td>
                            <td class="center"><?php echo $text_net_price; ?></td>
                            <td class="center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) { ?>
                               <?php foreach ($cartPurchaseDetails as $purchase) {  ?>
                                <?php if($apply_tax_type=='2') {
                                    $totalValue = $purchase['total'];
                                } else {
                                    $totalValue = $purchase['tax'] + $purchase['total'];
                                }
                                $totalTax+=$purchase['tax'];
                            ?>
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td class="center"><?php echo $purchase['code']; ?></td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td class="center">
                                <textarea class="textbox update_qty_price description" name="cust_description[<?php echo $purchase['product_id']; ?>]" rows = "2" cols ="60" id="desc_<?php echo $purchase['product_id']; ?>" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>"><?php echo str_replace('<br />', '', nl2br($purchase['description']));  ?></textarea>
                            </td>
                            <td class="order-nopadding">
                                <input type="text" id="qty_<?php echo $purchase['product_id']; ?>" value="<?php echo $purchase['quantity']; ?>" class="textbox small_box update_qty_price" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>">
                            </td>
                            <td class="order-nopadding">
                                <?php $purchase['price'] = str_replace("$","",$purchase['price']); ?>
                                <input type="text" id="price_<?php echo $purchase['product_id']; ?>"  value="<?php echo str_replace(",","",$purchase['price']); ?>" class="textbox small_box update_qty_price" data-product_id="<?php echo $purchase['product_id']; ?>" data-code="<?php echo $purchase['code']; ?>">
                            </td>
                            <?php $purchase_discount1=''; if (($purchase['discount_mode'] == '1') && !empty($purchase['discount'])): $purchase_discount1 = $purchase['discount']; endif; ?>
                            <td class="text-right" id="subTot_<?php echo $purchase['product_id']; ?>"><?php echo $purchase['sub_total']; ?></td>
                            
                            <td class="center"><a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="11" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <!-- <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0">
                 <?php if (!empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div> -->
                <div class="innerpage-listcontent-blocks">
                    <table id="addproductTbl" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">Product SKU</td>
                            <td colspan="6" class="center"><?php echo $text_product_name; ?></td>
                            <td class="center">Description</td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center"><?php echo $text_raw_cost; ?></td>
                            <td class="center"><?php echo $text_total; ?></td>
                        </tr>
                    </thead>
                        <tbody>
                            <tr>
                                <td class="insertproduct">
                                    <input type="text" name="sku" id="sku" class="textbox small_box"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?>
                                                onkeyup="getProductautoFill(this.value);" autocomplete="off">
                                    <input type="hidden" name="product_id" id="product_id"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
                                   <div id="suggesstion-box"></div>
                                </td>
                                <td colspan="6" class="insertproduct">
                                    <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" />
                                </td>
                                <td class="insertproduct" style="max-width: 10%;">
                                    <textarea class="textbox description" name="description" id="description" rows = "4" cols ="60"><?php if (!empty($rowdata)){ echo nl2br($rowdata['description']); } ?></textarea>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="raw_cost" id="raw_cost" value="<?php if (!empty($rowdata)){ echo $rowdata['raw_cost']; } ?>" class="textbox small_price_box" readonly>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="insertproduct purchase_add_button" colspan="11" align="right" style="padding: 10px 0 10px 0 !important;">
                                    <a onclick="clearPurchaseData();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                                    <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                                </td>
                            </tr>
                            <?php $i = 0; ?>
                            <?php if(!empty($purchase_totals)) { ?>
                            <?php foreach ($purchase_totals as $total) {
                             if ($i == 0) { ?>
                            <tr>
                                <td class="insertproduct heading-purchase">Discount Remarks</td>
                                <td class="order-nopadding" colspan="8">
                                    <textarea name="discount_remarks" id="discount_remarks" class="textbox remarks_box description" placeholder="Remarks for discount" autocomplete="off"><?php echo $discount_remarks; ?></textarea>
                                </td>
                                <td colspan="2" class="insertproduct heading-purchase">
                                    <table style="width:100%;">
                                        <tr>
                                            <td class="insertproduct heading-purchase" style="border-bottom: 2px solid #dddddd;background-color: #eeeeee;">Bill Disc(%)</td>
                                            <td class="order-nopadding" style="width: 50%;">
                                                <input type="text" name="bill_discount_percentage" id="bill_discount_percentage" value="<?php if (!empty($bill_discount_percentage)){ echo $bill_discount_percentage; } ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box"
                                                <?php if(!empty($hide_bill_discount_percentage)) { ?> readonly="readonly" <?php } ?> style="margin: 1px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="insertproduct heading-purchase" style="background-color: #eeeeee;">Bill Disc($)</td>
                                            <td>
                                                <input type="text" name="bill_discount_price" id="bill_discount_price" value="<?php if (!empty($bill_discount_price)){ echo $bill_discount_price; } ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box"
                                                <?php if(!empty($hide_bill_discount_price)) { ?> readonly="readonly" <?php } ?> style="margin: 1px;">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="insertproduct heading-purchase">Delivery Remarks</td>
                                <td class="order-nopadding" colspan="8">
                                    <textarea class="textbox remarks_box description" placeholder="Remarks for delivery fee" autocomplete="off" name="delivery_remarks" id="remarks"><?php echo $delivery_remarks; ?></textarea>
                                </td>
                                <td class="insertproduct heading-purchase">Delivery Fees($)</td>
                                <td class="order-nopadding">
                                    <input type="text" name="handling_fee" id="handling_fee" value="<?php echo $handling_fee; ?>" onkeyup="updateBillDiscount('0');" class="textbox small_box" autocomplete="off">
                                </td>
                            </tr>
                            <tr id ="TR<?php echo $total['code'];?>">
                                <td class="insertproduct heading-purchase">Remarks</td>
                                <td class="order-nopadding" colspan="8">
                                    <textarea name="remarks" id="remarks" class="textbox remarks_box description" placeholder="Remarks" autocomplete="off"><?php echo $remarks; ?></textarea>
                                </td>
                                <td align="right" class="purchase_total_left insertproduct" colspan="1">Sub Total</td>
                                <td class="purchase_total_right insertproduct" align="right">
                                    <input type="text" name="<?php echo $total['code']; ?>"
                                        id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;" />
                                </td>
                            </tr>
                            <?php } ?>
                            <?php if ($total['code'] == 'sub_total') { ?>
                                    <input type="hidden" name="sub_total_value"  id="sub_total_value" value="<?php echo $total['value'] ?>" />
                            <?php } ?>
                            <?php if ($total['code'] == 'discount') { ?>
                            <tr id="TRdiscount">
                                <td class="purchase_total_left" align="right" colspan="10"><?php echo $total['title']; ?></td>
                                <td class="purchase_total_right insertproduct">
                                    <input readonly="readonly" class="textbox row_total_box" value="<?php echo $total['text'] ?>" id="discount" name="discount" type="text" style="text-align: right;">
                                </td>
                             </tr>
                             <?php } ?>
                             <?php if($total['code'] == 'tax') { ?>   
                            <tr id="TRtax">
                                <td class="purchase_total_left" id="tax_title" align="right"><?php echo $total['title']; ?></td>
                                <td class="purchase_total_right insertproduct">
                                    <input readonly="readonly" class="textbox row_total_box" value="<?php echo $total['text'] ?>" id="tax" name="tax" type="text" style="text-align: right;">
                                </td>
                             </tr>

                            <?php } if($total['code'] == 'handling'){
                                        $handling_fee = $total['text'];
                                    }
                                    if($total['code'] == 'total'){
                                        $total_value = $total['text'];
                                    }
                                    $i++; } ?>

                            <tr id="TRhandling_fee">
                                <td colspan="10" align="right" class="purchase_total_left">Delivery Fees($)</td>
                                <td class="purchase_total_right insertproduct">
                                    <input type="text" id="TOThandling_fee" value="<?php echo $handling_fee; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;">
                                </td>
                            </tr>
                            <tr id="TRtotal" style="background-color: rgb(244, 244, 248);">
                                <td colspan="10" align="right" class="purchase_total_left">Order Totals</td>
                                <td class="purchase_total_right insertproduct text" align="right">
                                    <?php $totals= round($total['text'],1); ?>
                                    <input type="text" name="total" id="total" value="<?php echo $total_value; ?>" class="textbox row_total_box" readonly="readonly" style="text-align: right;">
                                </td>
                            </tr>

                            <?php } ?>
                        </tfoot>
                    </table>
                </div>
                </div>
            </div>
            </div>
            </form>
         </div>
    </div>

<?php echo $footer; ?>     
<script type="text/javascript">

    $(document).ready(function() {
        var fileName = '<?php echo $attachment; ?>';
        if (fileName  != '') {
            $('.fileDisplay').show();
            $('.fileUpload').hide();
        } else {
            $('.fileDisplay').hide();
            $('.fileUpload').show();
        }
        if($('#tax_class_id').val() == '1'){
            $('#tax_type').hide();
        }
        $("#close").click(function(){
            $("#popup").fadeOut("fast");
        });
    });
    
    $("#vendor_filter").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
                type: 'post',
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function( data ) {
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            $('#vendor_code').val(ui.item.data_ref);
            $('#vendor_filter').val(ui.item.label);
            getCustomerDetails(ui.item.data_ref);
            return false;
        }
    });

    function getCustomerDetails(cust){
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
            data:{cust:cust},

            success:function(data){
                data = JSON.parse(data);
                if(data.term_id != '0'){
                    <?php if(!isset($this->request->get['purchase_id'])){ ?>
                        $('#term_id').val(data.term_id);
                    <?php } ?>
                }else{
                    $('#term_id').val('');
                }
                <?php if(!isset($this->request->get['purchase_id'])){ ?>
                    changeRefDate();
                <?php } ?>
                $("#vendor_code").val(data.customercode);
                $("#vendor_filter").val(data.name);
                if(data.currency_code !=''){
                    $('#currency_code').val(data.currency_code).trigger('change',true);
                }
                getShippingAddress(data.customercode);
                var shipping_id = "<?php echo $shipping_id; ?>";
                if(shipping_id > 0 && shipping_id !='' ){
                    // $("#shipping_id").val(shipping_id);
                    // setTimeout(function(){ $('#shipping_id').val(shipping_id) }, 2000);
                }
                if(data.tax_allow=='0'){
                    $("#vendor_allow_gst").val(1);
                }else{
                    $("#vendor_allow_gst").val(0);
                }
                $("#vendor_name").val(data.name);
                
                <?php if(!isset($this->request->get['purchase_id'])){ ?>
                    if(data.tax_allow =='0'){
                        $('#tax_class_id').val('2').trigger('click',true);
                    }else{
                        $('#tax_class_id').val('1');
                    }
                    GetTaxClassAndType();
                    if(data.tax_type =='1'){
                        $('#tax_type').val('1').trigger('click',true);
                    }else{
                        $('#tax_type').val('2').trigger('click',true);
                    }
                    changeCustomerPriceAllItems(data.customercode);
                <?php } ?>
            }
        });
    }

$("#searchproduct-key").click(function(){
    $('#popup').bPopup({
        contentContainer:'.payment-page',
        easing: 'easeOutBack',
        speed: 990,
        transition: 'slideDown',
        width:'660',
        height:'660',
        scrollBar:true,
        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->request->get["token"]; ?>'
    });
});

function addProductGrid(){
    $("#addProductLink").on("click",function(){
         var $this = $(this);
        $this.text('See More');
        tableBody = $('#addproductTbl').find("tbody"),
        trLast = tableBody.find("tr:last"),
        trNew = trLast.clone();
        trLast.after(trNew);
});
}
function addProductToForm(pid,sku,name,price){
    $('#sku').val(sku);
    $('#product_id').val(pid);
    $('#name').val(name);
    $('#price').val(price);
    $('#raw_cost').val(price);
    removeAllOverlay();
}

function getProducts(val){
        var page =1;
        var ajaxData = "search_product_text=" + val + "&page=" + page;
        $("#ajaxloader").show();
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',
            data: ajaxData,
            success: function(result) {
                $("#ajaxloader").hide();
                if (result=='') {
                    //
                } else {
                    $('.payment-page').html(result);
                    var el = $("#searchKeynew").get(0);
                    var elemLen = el.value.length;
                    el.selectionStart = elemLen;
                    el.selectionEnd = elemLen;
                    el.focus();
                    return false;
                }
            }
        });
}

function removeAttachment(){
    var fileName = '<?php echo $attachment; ?>';
    $.ajax({
        url:"index.php?route=transaction/sales/removeAttachment&token=<?php echo $token; ?>",
        type:"POST",
        data:{
            fileName : fileName,
            invoice_no : '<?php echo $this->request->get['purchase_id']; ?>'
        },
        success:function(out){
            $('.fileDisplay').hide();
            $('.fileUpload').show();
        }
    });
}

function removeAllOverlay() {
    $('.b-close').click();
}

function getVendorProductDetails(ele) {
    $("#customerSearch").hide();
    getCustomerDetails(ele);
}

$(document).ready(function(){
    <?php if(isset($this->request->get['purchase_id']) || isset($this->request->get['newCustId'])){ ?>
        getCustomerDetails($('#vendor_code').val());
    <?php } ?>
    // GetTaxClassAndType();
    updateTaxValues();
});

$('#tax_class_id').change(function(){
    GetTaxClassAndType();
    updateTaxValues();
});
$('#tax_type').change(function(){
    updateTaxValues();
});

function updateTaxValues(){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/getOneCartDetail&token=<?php echo $token; ?>',
        success:function(data){
            $.each(JSON.parse(data), function(propName, propVal) {
                update_qty_price(propVal.id,propVal.code);
            });
        }
    });
}

function GetTaxClassAndType(){
    if($('#tax_class_id').val() =='2'){
        $('#tax_type').show();
    }else{
        $('#tax_type').hide();
    }
}
function changeCustomerPriceAllItems(cust){
    var tax_class_id = $('#tax_class_id').val();
    var tax_type     = $('#tax_type').val();
    var location_code= $('#location_code').val();

    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/changeCustomerPriceAllItems&token=<?php echo $token; ?>',
        data:{cust:cust,tax_class_id:tax_class_id,tax_type:tax_type,location_code:location_code},

        success:function(data){
            result    = JSON.parse(data);
            tableBody = $('#special').find("tbody");
            tableBody.empty().append(result['tbl_data']);
            
            if(result['tax_str']!=''){
                $('#TRtax').remove();
                var newRow = result['tax_str'];
                $("#total").before(newRow);
            }
            $('#sub_total').val(result['sub_total']);
            $('#sub_total_value').val(result['sub_total_value']);
            $('#total').val(result['total']);
            $('#discount').val(result['discount']);
            $('#tax').val(result['tax']);
            updateTaxValues();
        }
    });
}

function selectedProduct(val) {
    
    var newVal = val.split("||");
    var sku = newVal[1];
    var location = $("#location_code").val();
    var transactiondate = $("#transaction_date").val();
    var customercode = $("#vendor_code").val(); 
    var ajaxData = 'sku='+sku+'&location_code='+location+'&transactiondate='+transactiondate+'&customercode='+customercode;
     //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/getProductDetailsbyCustomer&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            var resultVal = result.split("||");
            console.log(resultVal);
            if (resultVal[0]==newVal[1]) {
              $("#price").val(resultVal[1]);
            }else{
              $("#price").val(newVal[4]);  
            }

            $("#product_id").val(newVal[0]);
            $("#sku").val(newVal[1].replace("^","'").replace('!!','"'));
            $("#name").val(newVal[2].replace("^","'").replace('!!','"'));
            $('#description').focus();
        }
    });
    $("#suggesstion-box").hide();

    }

function getProductautoFill(sku) {
     if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }
    var customercode = $("#vendor_code").val();
    if(customercode == ''){
        alert('Please select customer');
        $("#vendor_code").focus();
        $("#sku").val('');
        return false;
    } 
    var transactiondate = $("#transaction_date").val();
    var location = $("#location_code").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location+'&transactiondate='+transactiondate+'&customercode='+customercode;
     //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#suggesstion-box").show();
                $("#suggesstion-box").html(result);
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}

function getProductDetails() {
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {

                $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });
}



function clearPurchaseData() {

    $('#sku').val('');
    $('#name').val('');
    $('#quantity').val('');
    $('#price').val('');
    $('#raw_cost').val('');
    //$('#tax_class_id').val('');
    $('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');
    $('#description').val('');
}

function addToPurchase() {

    var product_id = $('#product_id').val();
    var price   = $('#price').val();
    var quantity   = $('#quantity').val();
    if (product_id && price && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/sales/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
            dataType: 'json',
            data: ajaxData,
            success: function(result) {
                console.log(result); 
                if (result=='') {
                    //
                } else {
                    //jQuery('#page').html(result);
                    tableBody = $('#special').find("tbody"),
                    tableBody.empty().append(result['tbl_data']);

                    if(result['tax_str']!=''){
                        $('#TRtax').remove();
                        var newRow = result['tax_str'];
                        $("#TRtotal").before(newRow);
                    }

                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    if(result['sub_total_value']=='0'){
                        //jQuery("#TRtax").remove();
                        $("#TRdiscount").remove();
                    }
                    if(result['tax_value']=='0'){
                        //jQuery("#TRtax").remove();
                    }
                    if(result['discount']=='0'){
                        $("#TRdiscount").remove();
                    }
                    $('#sku').focus();
                }
                clearPurchaseData();
                $("#pre-loader-icon").hide();

            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
}

function updateRowData(fieldValue) {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    var quantity   = $('#quantity').val();

    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/sales/AjaxupdateRowData&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {

                if (result=='') {
                    //
                } else {
                    $('#raw_cost').val(result['rowdata']['sub_total'].toFixed(3));
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            //jQuery('#quantity').focus();
        }else if(raw_cost==''){
            //jQuery('#price').focus();
        }
    }
}


function updateRowDataxx(fieldValue) { // RAGU Hided for loaded issue fix and added new above this fn
    var product_id = $('#product_id').val();
    var price   = $('#price').val();
    var quantity   = $('#quantity').val();
    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
}


function removeProductData(removeId,invoice_no) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId+'&invoice_no='+invoice_no,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }

            }
        }
    });
}

function removeProductDataxx(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                //window.location.reload();
                //jQuery('#page').html(result);
            }
        }
    });
}

function updateBillDiscount(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/sales/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $("#TRdiscount").remove();
                    if(result['discountRow']){
                        $("#TRsub_total").after(result['discountRow']);
                    }
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#TOThandling_fee').val(result['handling_fee']);
                    $('#tax').val(result['tax']);
                    $('#bill_discount_percentage').val(result['discount_percentage']);
                    $('#bill_discount_price').val(result['discount_price']);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}


function updateBillDiscountxx(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/purchase/insert&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#page').html(result);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}

$('.update_qty_price').blur(function(){
    var product_id = $(this).attr("data-product_id");
    var code       = $(this).attr("data-code");
    update_qty_price(product_id,code);
});

function update_qty_price(id,sku){

    var qty         = $('#qty_'+id).val();
    var price       = $('#price_'+id).val();
    var description = $('#desc_'+id).val();
    var tax         = $('#tax_class_id').val();
    var tax_type    = $('#tax_type').val();

    $.ajax({
        type:'POST',
        url:'index.php?route=transaction/sales/ajaxUpdateqtyAndPrice&token=<?php echo $token; ?>',
        data:{product_id:id,sku:sku,price:price,quantity:qty,tax_class_id:tax,tax_type:tax_type, description:description},
        success:function(data){
            var objData = JSON.parse(data);
            
            var sku     = objData.sku;
            $("td#subTot_"+sku).html(objData.netPrice);
            $("#sub_total").val(objData.orderSubTotal);
            if(objData.ordertax_value > 0){
                $('#TRtax').remove();
                $("#TRtotal").before(objData.tax_str);
            }else{
                $('#TRtax').remove();
            }

            if(objData.tax_type == '1'){
                $('#tax_title').html('GST (Excl)');
            }else{
                $('#tax_title').html('GST (Incl)');
            }
            $("#total").val(objData.orderTotal);
        }
    });
}

function checkCompanyCurrency(){
    var currency = $('#currency_code').val();

    if(currency!=''){
        $.ajax({
            type:'POST',
            url:'index.php?route=transaction/sales/checkCompanyCurrency&token=<?php echo $token; ?>',
            data:{currency:currency},

            success:function(data){
                if(data){
                    $("#ConversionRate").html('Conversion Rate');
                    $('#conversion_rate').val('1');
                    $("#conversion_rate").prop("readonly", true);
                }else{
                    $("#ConversionRate").html('Conversion Rate<span class="required">*</span>');
                    $("#conversion_rate").prop("readonly", false);
                }
            }
        });   
    }
}

function getCustomerCurrency(vendor){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/getCustomerCurrency&token=<?php echo $token; ?>',
        data:{vendor:vendor},

        success:function(data){
            $('#currency_code').val(data).trigger('change',true);
        }
    });
}

function getShippingAddress(vendor){
    var shipping_id = "<?php echo $shipping_id; ?>";
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/quotation/getShippingAddress&token=<?php echo $token; ?>',
        data:{vendor:vendor, shipping_id: shipping_id},

        success:function(data){
            $('#shipping_id').html(data);
        }
    });
}

/*function getCustomerTerm(){
    var vendor = $('#vendor_code').val();
    if(vendor!=''){
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/quotation/getCustomerTerm&token=<?php echo $token; ?>',
            data:{vendor:vendor},

            success:function(data){
                data = JSON.parse(data);
                if(data.status){
                    $('#term_id').val(data.term);
                }else{
                    $('#term_id').val('');
                }
                changeRefDate();
            }
        });
    }
}*/

$('#formID').submit(function(){
    if($('#currency_code').val() != 'SGD' && $('#conversion_rate').val()==''){
        alert('Please Enter Conversion Rate');
        return false;
    }
});
$('#reference_to_date').change(function(){
    var fromDate = $('#reference_date').val().split('/');
    var toDate   = $('#reference_to_date').val().split('/');
    var fromDate = fromDate[2] +'-'+ fromDate[1]+'-'+fromDate[0]; 
    var toDate   = toDate[2] +'-'+ toDate[1]+'-'+toDate[0];
    
    if(fromDate > toDate){
        $('#reference_date').val($('#reference_to_date').val());
    }
});
$('#reference_date').change(function(){
    var fromDate = $('#reference_date').val().split('/');
    var toDate   = $('#reference_to_date').val().split('/');
    var fromDate = fromDate[2] +'-'+ fromDate[1]+'-'+fromDate[0];
    var toDate   = toDate[2] +'-'+ toDate[1]+'-'+toDate[0];

    if(fromDate > toDate){
        $('#reference_to_date').val($('#reference_date').val());
    }
});
function changeRefDate(){
    days = $('select[name=term_id]').find(':selected').attr('data-days');
    if(days != undefined && days !='0'){
        var myDate = new Date(new Date().getTime()+(days*24*60*60*1000));
        var month  = myDate.getMonth() + 1;
        $('#reference_date').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
        date = myDate.getDate() + 1;
        $('#reference_to_date').val(date+'/'+month+'/'+myDate.getFullYear());
    }else{
        var myDate = new Date();
        var month  = myDate.getMonth() + 1;
        $('#reference_date').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
        $('#reference_to_date').val(myDate.getDate()+'/'+month+'/'+myDate.getFullYear());
    }
}

function validateRefNo(){
    var purchase_id = '<?php echo $purchase_id; ?>';
    
    if($("#reference_no").val()){
        $.ajax({
            type : 'POST',
            url  : 'index.php?route=transaction/sales/validateRefNo&token=<?php echo $token; ?>',
            data :{ref: $("#reference_no").val(), sales_id :purchase_id},

            success:function(status){
                if(!status){
                    alert('Reference No Already used');
                    $('#reference_no').val('');
                    return false;
                }else{
                    return true;
                }
            }
        });
    }
}

</script>
<?php //echo $footer; ?>     
<div id="ajax-modal" class="modal container fade" tabindex="-1">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Available Products</h4>
        </div>
        <div class="modal-body">
            Loading........
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>