<!-- NEW CODE -->
<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Total Sales Summary Report By Outlets</h3>     
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date">
                                    </td>                       
                          <!-- <td>
                            <input type="text" placeholder='Invoice No' name="filter_transactionno" value="<?php echo $_REQUEST['filter_transactionno'] ; ?>" class="textbox" />
                                      </td> -->
                                    <td>
                     
                    </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Date</td>
                          <td class="center">Outlet Name</td>
                          <td class="center">Total Sales Before GST</td>
                          <td class="center">GST</td>
                          <td class="center">Total Sales</td>
                          <td class="center">CASH</td>
                          <td class="center">NETS</td>
                          <td class="center">VISA</td>
                          <td class="center">MASTER</td>
                          <td class="center">Credit</td>
                          <td class="center">Discrepencies</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($sales); ?>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <?php $sale_discrepencies = $sale['total_sales']-($sale['cash']+$sale['nets']+$sale['visa']+$sale['master']+$sale['credit']); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $sale['date']; ?></td>
                          <td class="center"><?php echo $sale['outlet_name']; ?></td>
                          <td class="center"><?php echo $sale['before_gst']; ?></td>
                          <td class="center"><?php echo $sale['gst']; ?></td>
                          <td class="center"><?php echo $sale['total_sales']; ?></td>
                          <td class="center"><?php echo $sale['cash']; ?></td>
                          <td class="center"><?php echo $sale['nets']; ?></td>
                          <td class="center"><?php echo $sale['visa']; ?></td>
                          <td class="center"><?php echo $sale['master']; ?></td>
                          <td class="center"><?php echo $sale['credit']; ?></td>
                          <td class="center"><?php echo number_format(abs($sale_discrepencies),3); ?></td>
                        </tr>
                        <?php 
                        $strBeforegst +=$sale['before_gst'];
                        $strGst +=$sale['gst'];
                        $strTotalsales +=$sale['total_sales'];
                        $strCash +=$sale['cash'];
                        $strNets +=$sale['nets'];
                        $strVisa +=$sale['visa'];
                        $strMaster +=$sale['master'];
                        $strCredit +=$sale['credit'];
                        $strDiscrepencies +=abs($sale_discrepencies);
                        ?>
                        <?php } ?>                        
                        <tr>
                          <td colspan="2" align="right"><strong>Total:</strong></td>
                          <td><strong><?php echo number_format($strBeforegst,3); ?></strong></td>
                          <td><strong><?php echo number_format($strGst,3); ?></strong></td>
                          <td><strong><?php echo number_format($strTotalsales,3); ?></strong></td>
                          <td><strong><?php echo number_format($strCash,3); ?></strong></td>
                          <td><strong><?php echo number_format($strNets,3); ?></strong></td>
                          <td><strong><?php echo number_format($strVisa,3); ?></strong></td>
                          <td><strong><?php echo number_format($strMaster,3); ?></strong></td>
                          <td><strong><?php echo number_format($strCredit,3); ?></strong></td>
                          <td><strong><?php echo number_format($strDiscrepencies,3); ?></strong></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
//--></script> 
<?php echo $footer; ?>