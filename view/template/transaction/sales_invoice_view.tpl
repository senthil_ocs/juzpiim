<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">View Sales Invoice Details</h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
        <?php  }?>                    
        </ul>
         <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <?php if(!$purchaseInfo['xero_sales_id'] && $purchaseInfo['payment_status'] == '0'){ ?>
                      <a href="<?php echo $modify_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-edit"></i>Modify</a>
                      
                    <?php  } ?>
                    <a href="<?php echo $download_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-print"></i> Print</a>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>                          
      </div>
      <div style="clear:both"></div>
    <div class="row">     
      <div class="col-md-12">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td style="width: 25%;">Transaction No</td>
                          <td style="width: 25%;"><?php echo $purchaseInfo['invoice_no'] ?></td>
                          <td style="width: 25%;">Transaction Date</td>
                          <td style="width: 25%;"><?php echo date('d/m/Y',strtotime($purchaseInfo['invoice_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Order No</td>
                          <td><?php echo $purchaseInfo['sales_trans_no']; ?></td>
                          <td>Order Id</td>
                          <td><?php echo $purchaseInfo['network_order_id']; ?></td>
                        </tr>
                        <tr>
                          <td>Customer Code</td>
                          <td><?php echo $vendorDetail['cust_code']; ?></td>
                          <td>Customer Name</td>
                          <td><?php echo $vendorDetail['name']; ?></td>
                        </tr>
                        <tr>
                          <td>Ref No</td>
                          <td>
                            <?php if($purchaseInfo['network_id'] == '1'){ echo '#'; } echo $purchaseInfo['network_order_id']; ?>
                          </td>
                          <td>Delivery Dates</td>
                          <?php 
                            $delivery_to_date =  '';
                          if($purchaseInfo['delivery_to_date']!='' && $purchaseInfo['delivery_to_date'] !='1970-01-01'){
                            $delivery_to_date = date("d/m/Y", strtotime($purchaseInfo['delivery_to_date']));
                          }
                          $header_remarks = '';
                          if($purchaseInfo['header_remarks']!='' && $purchaseInfo['header_remarks'] !='1970-01-01'){
                            $header_remarks = date("d/m/Y", strtotime($purchaseInfo['header_remarks']));
                          }
                          ?>
                          <td><?php echo $header_remarks.' - '.$delivery_to_date; ?></td>
                        </tr>
                        <tr>
                          <td>Currency Code</td>
                          <td><?php echo $purchaseInfo['currency_code']; ?></td>
                          <td>Currency Rate</td>
                          <td><?php echo $purchaseInfo['conversion_rate']; ?></td>
                        </tr>
                        <tr>
                          <td>Paid Amount</td>
                          <td><?php echo $purchaseInfo['paid_amount']; ?></td>
                          <td>Balance</td>
                          <td><?php echo $purchaseInfo['net_total'] - $purchaseInfo['paid_amount'];?></td>
                        </tr>
                        <tr>
                          <td>Discount Remark</td>
                          <td><?php echo nl2br($purchaseInfo['discount_remarks']); ?></td>
                          <td>Delivery Remarks</td>
                          <td><?php echo nl2br($purchaseInfo['delivery_remarks']); ?></td>
                        </tr>
                        <tr>
                          <td>Remarks</td>
                          <td><?php echo nl2br($purchaseInfo['remarks']); ?></td>
                          <td>Shipping Address</td>
                          <td><?php if($shippingAddress['name']!=''){ echo $shippingAddress['name'].' '; } echo $shippingAddress['address1'].', <br>'.$shippingAddress['address2'].', '.$shippingAddress['city'].', '.$shippingAddress['zip'].', '.$shippingAddress['country'].', '.$shippingAddress['contact_no']; ?></td>
                        </tr>
                        <tr>
                          <td>Created by</td>
                          <td><?php echo $purchaseInfo['createdby'].' at '.showDate($purchaseInfo['createdon']); ?></td>
                          <td>Modified by</td>
                          <td><?php echo $purchaseInfo['modifiedby'].' at '.showDate($purchaseInfo['modifiedon']); ?></td>
                        </tr>
                        <?php if($purchaseInfo['cancel_reason'] !=''){ ?>
                        <tr>
                          <td>Cance Reason</td>
                          <td colspan="3"><?php echo $purchaseInfo['cancel_reason']; ?></td>
                        </tr>
                        <?php } ?>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
                        <td class="center">Product Name</td>
                        <td class="center">Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Sub Total</td>
                        <td class="center">Total Price</td>
                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1; 
                          //printArray($productDetails);
                           ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) {  
                                $class = ($class == 'even' ? 'odd' : 'even'); 
                                $qty   = $products['qty']; 
                                if($products['cancel']){
                                  $products['sub_total'] = 0;
                                  $qty = 0;
                                }
                            ?>
                            <tr class="<?php echo $class; ?>" <?php if($products['cancel']){ echo "style='color: red;'"; } ?>>
                              <td class="center"><?php echo $i.' '; if($products['tagged']){ ?><i class="fa fa-tag" aria-hidden="true" style="color: #f1540c;font-size: 12px;"></i> <?php } ?></td>

                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['description']; ?>
                                <?php if(!empty($products['childItems'])){
                                    echo "<br>";
                                    foreach ($products['childItems'] as $childItems) {
                                      echo '<span style="color:#2f44a5;">'.$childItems['childsku'].' - '.$childItems['name'].'</span><br>';
                                    }
                                  }
                                ?>
                              </td>
                              <td><?php echo nl2br($products['cust_description']); ?></td>
                              <td><?php echo $products['qty']; ?></td>
                              <td><?php echo $products['sku_price']; ?></td>
                              <td><?php echo number_format(($qty *$products['sku_price']) * $products['conversion_rate'],2); ?></td>
                              <td class="text-right"><?php echo number_format($products['sub_total'] * $products['conversion_rate'],2); ?></td>
                            </tr>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="7" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['sub_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Discount</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['discount'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Delivery Fees</td>
                              <td class="text-right"><?php echo $purchaseInfo['handling_fee']; ?></td>
                            </tr>
                            <tr>
                              <?php $tax_type=$purchaseInfo['tax_type']=='0' ? '(7% Exclusive)':'(7% Inclusive)';
                                    $tax_type= $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)'; ?>
                              <td colspan="7" class="text-right"><?php echo 'GST '.$tax_type; ?></td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['gst'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['net_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">FCTotal</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['fc_nettotal'],2); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                 </form>
             </div>
          </div>
      </div>
  </div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

</script>
<?php echo $footer; ?>