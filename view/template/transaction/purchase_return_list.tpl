<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title"><?php echo $heading_title; ?></h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                	<?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                <?php if($this->user->hasPermission('modify', $route)) { ?>

                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <?php } ?>           
              </div>    
            </div>
        </div>
			</div>
<!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td><input type="hidden" name="page"> 
                          <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from']; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $data['filter_date_to']; ?>" class="textbox date" autocomplete="off">
                        </td>
                
                         <td>
                           <select name="filter_location_code" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Location --</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>
                        <td>
                          <select name="filter_supplier" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Supplier --</option>
                            <?php foreach ($suppliers as $key) { ?>
                              <option value="<?php echo $key['vendor_id'];?>" <?php if($key['vendor_id']==$filter_supplier){ echo "Selected"; } ?>><?php echo $key['vendor_name']; ?></option>
                            <?php } ?>
                        </select> 
                        </td>
                        <td>
                          <input type="text" name="filter_transactionno" value="<?php echo $filter_transactionno; ?>" placeholder="Transaction / Reference No" class="textbox"> 
                        </td>

                          <td class="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<div class="innerpage-listcontent-blocks">
				<table class="table orderlist statusstock" id="table_sort">
				  <thead>
					<tr class="heading">
                      <!-- <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td> -->
                      <td class="center">S.No</td>
                      <td class="center"><?php echo $text_tran_no; ?></td>
                      <td class="center"><?php echo $text_tran_dt; ?></td>
                      <td class="center">Location</td>
					            <td class="center">Vendor</td>
                      <td class="center"><?php echo $text_net_total; ?></td>
                      <td class="center">Ref No</td>
                      <td class="center">Ref Date</td>
                      <?php if(HIDE_XERO){?>
                      <td class="center">Post</td>
                      <?php } ?>
                      <td class="center printHide"><?php echo $column_action; ?></td>
					</tr>
				  </thead>
				  <tbody>
					<?php if ($purchases) { $i = 1; $total=0; ?>
					<?php $class = 'odd'; ?>
					<?php foreach ($purchases as $purchase) { 
          $total+=$purchase['total']; ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
                        <!-- <td style="text-align: center;">
                        <?php $checked = ''; if ($product['selected']) { 
                              $checked='checked="checked"'; 
                        }  ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $purchase['purchase_id']; ?>" <?php echo $checked; ?> >
                      </td> -->
                      <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
					            <td class="center"><?php echo $purchase['transaction_no']; ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['location_code']; ?></td>
					            <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="right"><?php echo $purchase['total']; ?></td>
                      <td class=""><?php echo $purchase['reference_no']; ?></td>
                      <td class="center"><?php echo $purchase['reference_date']; ?></td>
                     <!--  <td class="center"><?php echo date('d/m/Y g:i A',strtotime($purchase['created_date'])); ?></td> -->                      
                     <?php if(HIDE_XERO){?>
                      <td style="text-align: center;" class="tdclass_<?php echo $purchase['purchase_id'] ?>">
                     <?php if(!empty($purchase['xero_purchase_id'])){ ?>
                      <span class="text">Success</span>
                      <?php }else{ ?>
                        <span class="text-primary xero" data-purchase_id="<?php echo $purchase['purchase_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></span>
                     <?php  } ?>
                    </td>
                    <?php } ?>

                      <td class="center printHide">
                        <?php if($purchase['xero_purchase_id'] == null){ ?>
                        <a href="<?php echo $purchase['modify_button'] ?>" id="modifyBtn_<?php echo $purchase['purchase_id']; ?>"><i class='fa fa-edit' style='font-size:19px' data-toggle="tooltip" title="Modify"></i></a>
                        <?php } ?>

                        <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="View"></i></a>
                    </td>
					</tr>
          <?php $i++; ?>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="text-center" colspan="11"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
				</table>
			</div>
			</form>
		  		<div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  $( document ).ready(function() {

  $('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [2,3,5,6,7,8,9],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
  });

   $('.xero').css("cursor",'pointer');
   $( ".xero" ).click(function() {
    var xero = $(this);
    var order_id = $(this).attr('data-purchase_id');
    var location_code = '<?php echo $this->sesstion->data['location_code']; ?>';
    $('.tdclass_'+order_id).html('<div class="loader"></div>');
    var xeroUrl = '<?php echo XERO_URL; ?>purchaseReturn.php?location_code='+location_code+'&order_id='+order_id;
    
        $.get(xeroUrl, 
          function (response, status, error) {
            if(validJSON(response)){
              var data  = JSON.parse(response);
              if(data.status == 'success'){
                xero.removeClass('xero');
                $('.tdclass_'+order_id).html('<span class="text">Success</span>');
              }else{
                alert('Faild to send xero, Please try again.');
                location.reload();
              }
            }else{
              alert('Faild to send xero, Please try again.');
              location.reload();
            }
        });
  });
});

$('#delete_btn').click(function(){
    $('#form').submit();
});

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$("#list_form_validation").submit(function(){
  var fromdt = $('#from_date').val();
  var todt = $('#to_date').val();
  if(fromdt !='' || todt != ''){
      if(fromdt == ''){
         $('#from_date').focus();
        alert('Please Select the From date');
        return false;
      }
      if(todt == ''){
          $('#to_date').focus();
        alert('Please Select the To date');
         return false;
      }
  }
});
function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    $(".heading a").removeAttr("href");
    $(".printHide").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
    $(".printHide").show();
}

</script> 