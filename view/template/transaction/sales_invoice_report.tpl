<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Sales Invoice Summary Reports</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                 <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Sales Invoice Summary Reports</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display  -->
       <div>
              <p>
                <?php if($data['filter_from_date']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <?php echo $data['filter_from_date'];?>, </span>
                <?php } ?>
                <?php if($data['filter_to_date']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date To <?php echo $data['filter_to_date'];?>, </span>
                <?php } ?>
                <?php if($data['filter_location']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">Location - <span></span><?php echo $data['filter_location'];?>, </span>
                <?php } ?>
                 <?php if($data['filter_transaction']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">Invoice No - <?php echo $data['filter_transaction'];?>, </span>
                <?php } ?>
                 <?php if($data['filter_customer']!= ''){
                     $filter_customer = $this->model_transaction_sales->getB2BCustomersbyCode($data['filter_customer'])['name'];
                 ?>
                  <span style="font-size: 15px;margin-right: 15px;">Customer - <?php echo $filter_customer;?>, </span>
                <?php } ?>
                 <?php if($data['filter_delivery_status']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">Delivery - <?php echo $data['filter_delivery_status'];?>, </span>
                <?php } ?>
                 <?php if($data['filter_payment_status']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">Payment - <?php echo $data['filter_payment_status'];?>, </span>
                <?php } ?>
                 <?php if($data['filter_network']!= ''){
                 $filter_network =$this->model_transaction_sales->getNetworkName($data['filter_network']);
                 ?>
                  <span style="font-size: 15px;margin-right: 15px;">Sales Channel - <?php echo $filter_network;?>, </span>
                <?php } ?>
                 <?php if($data['filter_xero']!= ''){ 
                    $filter_xero = $data['filter_xero']=='1' ? 'Posted' : 'Not Posted';
                  ?>
                  <span style="font-size: 15px;margin-right: 15px;">Xero - <?php echo $filter_xero; ?>, </span>
                <?php } ?>

                <?php if($data['filter_agent'] != ''){
                 $agentName =$this->model_transaction_sales->getSalesMansDetails($data['filter_agent']);
                 ?>
                  <span style="font-size: 15px;margin-right: 15px;">Agent - <?php echo $agentName['name'];?> </span>
                <?php } ?>
              </p>
        </div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 120px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_from_date" value="<?php echo $data['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_to_date" value="<?php echo $data['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                          </td>
                          <td>
                           <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Location</option>
                            <?php if(!empty($Tolocations)){
                                foreach($Tolocations as $value){ ?>
                                <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>
                            <?php  } } ?> 
                        </select>
                        </td>                       
                        <td>
                          <input type="text" placeholder='Transaction / Reference' name="filter_transaction" value="<?php echo $data['filter_transaction'] ; ?>" class="textbox" autocomplete="off"/>
                        </td>
                        <td colspan="2">
                            <input type="hidden" name="filter_customer" id="filter_customer" value="<?php echo $filter_customer; ?>">
                           <input type="text" id="customer_name" class="textbox" placeholder="Search Customer"> 
                        </td>
                      </tr>
                        
                      <tr>
                         <td>
                           <select name="filter_delivery_status" id="filter_delivery_status" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Delivery Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Scheduling">Scheduling</option>
                            <option value="Assigned">Assigned</option>
                            <option value="Pending_Delivery">Pending Delivery</option>
                            <option value="On_Delivery">On Delivery</option>
                            <option value="Partial_Delivered">Partial Delivered</option>
                            <option value="Delivered">Delivered</option>
                            <option value="Canceled">Canceled</option>
                        </select>
                        </td>
                         <td>
                           <select name="filter_payment_status" id="filter_payment_status" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Payment Status</option>
                            <option value="Pending">Pending</option>
                            <option value="Paid">Paid</option>
                            <option value="Partial">Partial</option>
                            <option value="Hold">Hold</option>
                        </select>
                        </td>
                        <td>
                          <select name="filter_xero" id="filter_xero" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Xero</option>
                              <option value="1"> Posted</option>
                              <option value="2"> Not Posted</option>
                          </select> 
                        </td>
                        <td>
                          <select name="filter_network" id="filter_network" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Sales channel</option>
                            <?php foreach ($networks as $value) { ?>
                              <option value="<?php echo $value['id']; ?>" <?php if($value['id'] == $data['filter_network']){ echo 'selected'; } ?>> <?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select> 
                        </td>
                        <td>
                          <select name="filter_agent" id="filter_agent" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">Select Agent</option>
                            <?php foreach ($salesmanlist as $salesman) { ?>
                              <option value="<?php echo $salesman['id']; ?>" <?php echo $data['filter_agent'] == $salesman['id'] ? 'Selected':'';?>> <?php echo $salesman['name']; ?></option>
                            <?php } ?>
                          </select> 
                        </td>
                        <td align="center">
                          <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <table class="table orderlist statusstock">
                <tr>
                    <td colspan="9" class="right">Total :
                        <?php echo number_format($sales_net_total,2); ?>
                    </td>
                </tr>
            </table>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Sno</td>
                          <td class="center">Location Code</td>
                          <td class="center">Invoice No</td>
                          <td class="center">Invoice Date</td>
                          <td class="center">Customer</td>
                          <td class="center">Delivery</td>
                          <td class="center">Payment</td>
                          <td class="center">Sub Total</td>
                          <td class="center">Discount</td>
                          <td class="center">GST</td>
                          <td class="center">Actual Total</td>
                          <td class="center">Round Off</td>
                          <td class="center">Net Total</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($sales); ?>
                        <?php if ($sales) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($sales as $sale) { $i++;?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $i; ?></td>
                          <td class="center"><?php echo $sale['location_code']; ?></td>
                          <td class="center"><a href="<?php echo $sale['salesDetail']; ?>"><?php echo $sale['invoice_no']; ?></a></td>
                          <td class="center"><?php echo $sale['invoice_date']; ?></td>
                          <td class="center"><?php echo $sale['customer']; ?></td>
                          <td class="center"><?php echo $sale['delivery_status']; ?></td>
                          <td class="center"><?php echo $sale['payment_status']; ?></td>
                          <td class="right"><?php echo $sale['sub_total']; ?></td>
                          <td class="right"><?php echo $sale['discount']; ?></td>
                          <td class="right"><?php echo $sale['gst']; ?></td>
                          <td class="right"><?php echo $sale['actual_total']; ?></td>
                          <td class="right"><?php echo $sale['round_off']; ?></td>
                          <td class="right"><?php echo $sale['net_total']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="13"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                       
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

$(document).ready(function() {
    $('.xero').css("cursor",'pointer');
    $('#filter_delivery_status').val('<?php echo $data['filter_delivery_status']; ?>');
    $('#filter_payment_status').val('<?php echo $data['filter_payment_status']; ?>');
    $('#filter_xero').val('<?php echo $data['filter_xero']; ?>');
    $('#filter_network').val('<?php echo $data['filter_network']; ?>');

    var customer = "<?php echo $data['filter_customer']; ?>";
    if(customer !=''){
        getCustomerDetails(customer);
    }
});

  $("#customer_name").autocomplete({
      source: function( request, response ) {
          $('#filter_customer').val('');
          $.ajax({
              url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
              type: 'post',
              dataType: "json",
              data: {
                  search: request.term
              },
              success: function( data ) {
                  response(data);
              }
          });
      },
      select: function (event, ui) {
          $('#customer_name').val(ui.item.label);
          $('#filter_customer').val(ui.item.data_ref);
          return false;
      }
  });
  
  function getCustomerDetails(cust){
      $.ajax({
          type: "POST",
          url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
          data:{cust:cust},

          success:function(data){
            data = JSON.parse(data);
            $('#customer_name').val(data.name);
            $('#filter_customer').val(data.customercode);
          }
      });
  }

  function filterReport(report_type) {
      document.getElementById('type').value=report_type;
      document.report_filter.submit();
  }
  function printFunction(){
    $(".pagination").hide();
    window.print();
    $(".pagination").show();
  }
</script>