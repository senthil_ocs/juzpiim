<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <?php if (!empty($purchase_id)): ?><input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchase_id ?>" /> <?php endif; ?>
        <div class="page-content" >
            <h3 class="page-title">Stock Request Insert </h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>                    
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                  <button class="btn btn-update btn-warning" type="submit" name="purchase_button" value="hold" title="Hold" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-ban"></i> <?php echo $text_hold; ?></button>
               
                <button type="submit" title="Save" class="btn btn-primary" name="purchase_button" value="submit" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" id="cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
                <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button>
             </div>
            <?php } ?>
            <div class="row">               
            <div style="clear:both"></div>         
        <div class="col-md-12">         
            <table class="table orderlist purchase_table">
                <tbody>
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_no; ?></label>
                            <input type="text" name="transaction_no" id="transaction_no" value="<?php echo $transaction_no; ?>" class="textbox purchase_inputfields" placeholder="Enter Your Transaction Number" readonly="readonly"><!-- <?php echo $transaction_no; ?> -->
                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_tran_dt; ?><span class="required">*</span></label>
                            <input type="text" name="transaction_date" id="transaction_date" value="<?php echo $transaction_date; ?>" class="textbox purchase_inputfields" readonly>
                            
                        </td>                        
                    </tr>
                     <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_from_location; ?></label>
                              <input type="hidden" name="from_location_id" id="from_location_id" value="<?php echo $location_details['location_id'];?>">  
                              <input type="text" name="from_location_code" id="from_location_code" value="<?php echo $location_details['location_name'].' ('.$location_details['location_code'].')'; ?>" class="textbox purchase_inputfields" readonly="readonly">
                        </td>

                        
                        <td class="purchase_extra order-nopadding"></td>
                         <td class="order-nopadding"> 
                          <label class="purchase_label" for="name"><?php echo $entry_to_location; ?></label>
                           <select name="to_location_id" id="to_location_id" data-ref="to_address" class="selectdropdown" <?php echo $strDisabled; ?> style="margin:5px 0px 5px 0px;" >
                                <option value="">Select Location</option>
                                <?php foreach ($Tolocations as $locationsDetails) { ?>
                                    <?php if($locationsDetails['location_code']!=$location_details['location_code']) {?>
                                        <option value="<?php echo $locationsDetails['location_id']; ?>|<?php echo $locationsDetails['location_code']; ?>" <?php if($to_location_id == $locationsDetails['location_code']){ echo 'selected'; } ?> > 
                                        <?php echo $locationsDetails['location_name'].' ('.$locationsDetails['location_code'].')'; ?></option>
                                    <?php }?>
                                <?php } ?>
                            </select>
                        </td>
                       
                    </tr>
                   
                    <tr>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refno; ?></label>
                             <?php if(count($filter_ref_details)>=1){ ?>
                             <span id="ref_yes">
                                    <select name="reference_no" id="reference_no" class="selectdropdown" style="margin:5px 0px 5px 0px;" onChange="loadRefPurchaseItems(this.value);">
                                        <option value="">ALL</option>
                                        <?php foreach ($filter_ref_details as $ref) { ?>
                                                <?php if($_REQUEST['ref']==$ref['reference_no']){?>
                                                <option value="<?php echo $ref['reference_no'];?>|<?php echo $ref['reference_date'];?>" selected="selected"><?php echo $ref['reference_no'];?></option>
                                                <?php }else{?>
                                                <option value="<?php echo $ref['reference_no'];?>|<?php echo $ref['reference_date'];?>"><?php echo $ref['reference_no'];?></option>
                                                <?php }?>
                                        <?php }?>
                                       
                                    </select>
                              </span>
                               <?php }else{?>
                            <span id="ref_no" <?php if(count($filter_ref_details)>=1){?> style="display: none; <?php }?>">
                                    <input type="text" name="reference_no" id="reference_no" value="<?php echo $reference_no; ?>" class="textbox purchase_inputfields" placeholder="Enter ref no">
                            </span>
                            <?php }?>

                        </td>
                        <td class="purchase_extra order-nopadding"></td>
                        <td class="order-nopadding">
                            <label class="purchase_label_error" for="name"><?php echo $entry_refdate; ?></label>
                             <?php if($error_reference_date) { ?>
                                <div class="input-icon right div_error">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter ref date"></i>
                                <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox requiredborder purchase_inputfields_error">                        
                            </div>
                            <?php } ?>
                            <?php if(!$error_reference_date) { 
                                    if($_REQUEST['ref_date']!='' && $_REQUEST['ref_date']!='undefined'){
                                       $reference_date = $_REQUEST['ref_date'];
                                    }
                            ?>
                            <input type="text" name="reference_date" id="reference_date" value="<?php echo $reference_date; ?>" class="date textbox purchase_inputfields">
                            <?php } ?>
                            <input type="hidden" id="location_fill" name="location_fill" value="<?php echo $location_fill;?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
                <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center">No</td>
                            <td class="center">SKU</td>
                            <td class="center"><?php echo $text_product_name; ?></td>
                            <td class="center"><?php echo $text_qty; ?></td>
                            <td class="center"><?php echo $text_raw_cost; ?></td>
                            <td class="center">Selling Price</td>
                            <td class="center"><?php echo $text_total; ?></td> 
                            <td class="center"><?php echo $column_action; ?></td>
                        </tr>
                    </thead>                      
                        <tbody>
                            <?php if (!empty($cartPurchaseDetails)) {$i=1; ?>
                                <?php foreach ($cartPurchaseDetails as $purchase) {  ?>
                                <?php if($apply_tax_type=='2') {
                                $totalValue = round($purchase['total'],2);
                                } else {
                                $totalValue = round($purchase['tax'] + $purchase['total'],2);
                                }
                            ?>
                            <tr id="purchase_<?php echo $purchase['product_id']; ?>">
                            <td class="center"><?php echo $i++; ?></td>
                            <td class="center"><?php echo $purchase['code']; ?></td>
                            <td class="center"><?php echo $purchase['name']; ?></td>
                            <td align="center"><?php echo $purchase['quantity']; ?></td>
                            <td align="right"><?php echo $purchase['price']; ?></td>
                            <td align="right"><?php echo $purchase['raw_cost']; ?></td>
                            <td align="right"><?php echo $this->currency->format($totalValue); ?></td>
                            <td align="center">[ <a onclick="removeProductData(<?php echo $purchase['product_id']; ?>);"><?php echo $text_remove ?></a> ]</td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                            <td colspan="11" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                 <div class="purchase_add col-md-12">
                 <div id="get-products-container" class="get-products-title" style="margin:-5px 0 10px 0;display: none;">
                 <?php if (!empty($products)) { ?>
                    <a id="searchproduct-key" class="get-products-text btn btn-primary"><i class="fa fa-file-text-o"></i> Get Products</a>
                <?php }?>
                </div>
                <div class="innerpage-listcontent-blocks">      
                   
                    <table id="addproductTbl" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td class="center" style="width:10%;">SKU<?php //echo $text_inventory_code; ?></td>
                            <td class="center" style="width:70%;"><?php echo $text_product_name; ?></td>
                            <td class="center" style="width:5%;"><?php echo $text_qty; ?></td>
                            <td class="center" style="width:5%;"><?php echo $text_raw_cost; ?></td>
                            <td class="center" style="width:5%;">Selling Price</td>
                            <td class="center" style="width:5%;"><?php echo $text_total; ?></td>
                        </tr>
                    </thead>                      
                        <?php if (!empty($products)) { ?>
                        <tbody>
                            <tr>
                                <td class="insertproduct">
                                    <input type="text" name="sku" id="sku" class="textbox small_box"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['sku'];?>" <?php }?> onkeyup="getProductautoFill(this.value);">
                                    <input type="hidden" name="product_id" id="product_id"
                                    <?php if (!empty($rowdata)){?> value="<?php echo $rowdata['product_id'];?>" <?php }?>>
                                   <div id="suggesstion-box"></div>
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="name" id="name" value="<?php if (!empty($rowdata)){ echo $rowdata['name']; } ?>" class="textbox small_box" readonly="readonly" />
                                    <input type="hidden" name="uom" id="uom" value="">
                                </td>
                                
                                <td class="insertproduct">
                                    <input type="text" name="quantity" id="quantity" value="<?php if (!empty($rowdata)){ echo $rowdata['quantity']; } ?>" class="textbox small_box" onblur="updateRowData(this.value);">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="price" id="price" onblur="updateRowData(this.value);" value="<?php if (!empty($rowdata)){ echo $rowdata['price']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="raw_cost" id="raw_cost" value="<?php if (!empty($rowdata)){ echo $rowdata['raw_cost']; } ?>" class="textbox small_price_box">
                                </td>
                                <td class="insertproduct">
                                    <input type="text" name="row_total" id="row_total" value="<?php if (!empty($rowdata)){ echo $rowdata['total']; } ?>" class="textbox row_total_box" readonly="readonly">
                                </td>  
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td class="insertproduct purchase_add_button" align="right" colspan="6" style="padding: 10px 0 10px 0 !important;">
                                    <a onclick="addToPurchase();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add; ?></a>
                                    <a onclick="clearPurchaseData();" id="clear" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_clear; ?></a>
                                    
                                </td>
                            </tr>
                            <?php $i = 0; ?>
                            <?php if(!empty($purchase_totals)) { ?>
                            <?php foreach ($purchase_totals as $total) { ?>
                                <?php if ($i == 0) { ?>
                                    <tr id ="TR<?php echo $total['code'];?>">
                                        <td class="insertproduct heading-purchase"  align="right" style="text-align: right; margin-top: 2px; margin-right: 5px;">Remarks  &nbsp;&nbsp;</td>
                                        <td class="order-nopadding" style="padding: 5px;" >
                                            <textarea name="remarks" id="remarks" cols="60" rows="3"><?php if (!empty($remarks)){ echo $remarks; } ?></textarea>
                                              <input type="hidden" name="<?php echo $total['code']; ?>"
                                                id="<?php echo $total['code']; ?>" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly" />
                                        </td>
                                        <td  align="right" class="purchase_total_left" colspan="2">Order Totals</td>
		                                <td class="purchase_total_right insertproduct" colspan="2">
		                                	<input type="text" name="total" id="total" value="<?php echo $total['text']; ?>" class="textbox row_total_box" readonly="readonly">
		                                </td>
                                      
                                    </tr>
                                <?php } ?>                                
                            <?php $i++; } ?>                            
                            <?php } ?>
                        </tfoot> 
                        <?php } else { ?>
                        <tfoot>
                            <tr>
                                <td colspan="10" class="purchase_no_data center"><?php echo $text_select_vendor; ?></td>
                            </tr>
                        </tfoot>
                        <?php } ?>
                    </table>
                </div>
                </div>
            </div>
            </div>
            </form>
         </div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript">
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
    dateFormat: 'yy-mm-dd',
    timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
</script>

<script type="text/javascript">
function loadPurchaseItems(lc){
    var lcAry = lc.split("|");
    if(lc!=''){
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/stock_transfer_out/loadpurchaseItems&token=<?php echo $this->session->data["token"]; ?>',
            data: {location_code : lc},
            success: function(result) {
                if (result['success']=='true') {
                    window.location = '<?php echo HTTP_SERVER; ?>index.php?route=transaction/stock_transfer_out/insert&token=<?php echo $this->session->data["token"]; ?>&lc='+lcAry[1]; 
                } else {
                   $('#location_fill').val('0');
                   $('#ref_yes').hide();
                   $('#ref_no').show();
                   return false;

                }
            }
        });
    }
}

function loadRefPurchaseItems(ref_noStr){
    var lc = $('#to_location_id').val();  
    var lcAry = lc.split("|");

    var ref_noAry = ref_noStr.split("|");
    var ref_no    = ref_noAry[0];
    var ref_date  = ref_noAry[1];

    if(lc!=''){
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/stock_transfer_out/loadpurchaseItems&token=<?php echo $this->session->data["token"]; ?>',
            data: {location_code : lc,ref_no : ref_no},
            success: function(result) {
                if (result['success']=='true') {
                  window.location = '<?php echo HTTP_SERVER; ?>index.php?route=transaction/stock_transfer_out/insert&token=<?php echo $this->session->data["token"]; ?>&lc='+lcAry[1]+'&ref='+ref_no+'&ref_date='+ref_date; 
                } else {
                   $('#location_fill').val('0');
                   $('#ref_yes').hide();
                   $('#ref_no').show();
                   return false;

                }
            }
        });
    }
}

$("#searchproduct-key").click(function(){
    $('#popup').bPopup({
        contentContainer:'.payment-page',
        easing: 'easeOutBack',
        speed: 990,
        transition: 'slideDown',
        width:'660',
        height:'660',
        scrollBar:true,
        loadUrl: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->request->get["token"]; ?>'
    });
});
function addProductGrid(){
    $("#addProductLink").on("click",function(){
         var $this = $(this);
        $this.text('See More');
        tableBody = $('#addproductTbl').find("tbody"),
        trLast = tableBody.find("tr:last"),
        trNew = trLast.clone();
        trLast.after(trNew);
});
}
function addProductToForm(pid,sku,name,price){
    $('#sku').val(sku);
    $('#product_id').val(pid);
    $('#name').val(name);
    $('#price').val(price);
    $('#raw_cost').val(price);
    removeAllOverlay();
}
function getProducts(val){
        var page =1;
        var ajaxData = "search_product_text=" + val + "&page=" + page;
        $("#ajaxloader").show();
        $.ajax({
            type: "POST",
            url: '<?php echo HTTP_SERVER; ?>index.php?route=common/searchproducts&token=<?php echo $this->session->data["token"]; ?>',
            data: ajaxData,
            success: function(result) {
                $("#ajaxloader").hide();
                if (result=='') {
                    //
                } else {
                    $('.payment-page').html(result);
                    var el = $("#searchKeynew").get(0);
                    var elemLen = el.value.length;
                    el.selectionStart = elemLen;
                    el.selectionEnd = elemLen;
                    el.focus();
                    return false;
                }
            }
        });
}

$(document).ready(function() {
    $("#close").click(function(){
        $("#popup").fadeOut("fast");
    });

    var strLocationId = '<?php echo $location_details["location_id"] ?>|<?php echo $location_details["location_code"] ?>';
    $('#from_location_id').val(strLocationId);
    getFromLocation(strLocationId,'from_address'); 

});
function removeAllOverlay() {
    $('.b-close').click();
}

$(".selectdropdown").change(function(){
  /*  var dropDw  $(this).attr('id');*/
    var idsVl = $(this).val();
    var fromLoc = $("#from_location_id").val();
    var toLoc =$("#to_location_id").val();
    var strId = $(this).attr('data-ref');    
    if(fromLoc == toLoc) {
        alert('From and To Location not be same');
        $('#'+$(this).attr('id')).val('');
        $("#"+strId).val('');
    } else {
        getFromLocation(idsVl,strId); 
    }
   
});


function getFromLocation(ids,addressid) {
   var fromLoc = ids;
   var fromVal = fromLoc.split('|');
   var id     = fromVal[0];
   var code   = fromVal[1];
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_transfer/getAddress&token=<?php echo $token; ?>',
        data: {id : id,code:code},
        success: function(result) {
            if (result=='') {
               $('#'+addressid).val(''); 
            } else {
               $('#'+addressid).val(result);
            }
        }
    });  
}

function getVendorProductDetails(ele) {

        var option  = $("option:selected", ele).attr("class");
        var vnamefull = $('#vendor_code').find("option:selected").attr('label');
        var vname = vnamefull.split("||");
        if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
        }else{
            $("#tax_class_id").val('');
             $("#vendor_allow_gst").val(0);
        }
        $("#vendor_name").val(vname);
}
function getVendorProductDetailsxx() { // RAGU Hided for loaded issue fix and added new above this fn
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_transfer/insert&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
                $("#pre-loader-icon").hide();
            }
        }
    });
}

function selectedProduct(val) {
        var newVal = val.split("||");
        $("#product_id").val(newVal[0]);
        $("#sku").val(newVal[1].replace('^',"'").replace("!!",'"'));
        $("#name").val(newVal[2].replace("^","'").replace("!!",'"'));
        $("#price").val(newVal[3]);
        $("#raw_cost").val(newVal[4]);
        $("#uom").val(newVal[5]);
        $("#suggesstion-box").hide();
}

function getProductautoFill(sku) {
	if(sku.length < SEARCH_CHAR_CNT){
		return false;
	}
    var ajaxData = 'sku='+sku;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_request/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                // alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
               if(result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    selectedProduct(result);
                }
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function getProductDetails() {
    $("#pre-loader-icon").show();
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_transfer_out/insert&token=<?php echo $token; ?>&updateType=inventory',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
                $('#page').html(result);
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
    //$('#weight_class_id').val('');
    //$('#tax_class_id').val('');
    $('#quantity').val('');
    $('#price').val('');
    $('#raw_cost').val('');
    $('#discount_percentage').val('');
    $('#discount_price').val('');
    $('#row_total').val('');
    $('#uom').val('');
}
function addToPurchase() {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    if(price==0){
        var con= confirm("Do you want to proceed with 0 price?");
        if(con==false){
            return false;
        }
    }

    var quantity   = $('#quantity').val();
    if (product_id && price && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/stock_transfer_out/ajaxaddproducts&token=<?php echo $token; ?>&updateType=product',
            dataType: 'json',
            data: ajaxData,
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    //jQuery('#page').html(result);
                    tableBody = $('#special').find("tbody"),
                    tableBody.empty().append(result['tbl_data']);
                    if(result['tax_str']!=''){
                        $('#TRtax').remove();
                        var newRow = result['tax_str'];
                        $("#TRsub_total").after(newRow);
                    }
                    //alert(result['sub_total_value']);
                    //location.reload();
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    if(result['sub_total_value']=='0'){
                        //jQuery("#TRtax").remove();
                        $("#TRdiscount").remove();
                    }
                    if(result['tax_value']=='0'){
                        //jQuery("#TRtax").remove();
                    }
                    if(result['discount']=='0'){
                        $("#TRdiscount").remove();
                    }
                    $('#sku').focus();
                        // end
                }
                clearPurchaseData();
                activeTaxClassByVendor();
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            $('#quantity').focus();
        }else if(raw_cost==''){
            $('#price').focus();
        }
    }
     // code for check vendor has gst value 1
     activeTaxClassByVendor();
    
}
function isFloat(n){
    result = (n - Math.floor(n)) !== 0; 
    return result;
}
function activeTaxClassByVendor(){
   var option =  $("#vendor_allow_gst").val();
    if(option=='1'){
        var listVal = $('#tax_class_id option:eq(1)').val();
        $("#tax_class_id").val(listVal);
    }else{
        $("#tax_class_id").val('');
    } 
}
function updateRowData(fieldValue) {
    var product_id = $('#product_id').val();
    var price      = $('#price').val();
    var quantity   = $('#quantity').val();
    var uom        = $('#uom').val();
    if(uom!='KG' && isFloat(quantity)){
        alert("Not allowed Decimal quantity");
        $('#quantity').val('');
        $('#quantity').focus();
        //return false;        
    }
    if (fieldValue && product_id && raw_cost && quantity) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/stock_transfer_out/AjaxupdateRowData&token=<?php echo $token; ?>&updateType=rowcount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $('#row_total').val(result['rowdata']['total']);
                }
                $("#pre-loader-icon").hide();
            }
        });
    }else{
        if(quantity==''){
            //jQuery('#quantity').focus();
        }else if(raw_cost==''){
            //jQuery('#price').focus();
        }
    }
}

function removeProductData(removeId) {
    var ajaxData = $("form").serialize();
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/stock_transfer_out/AjaxRemoveProduct&token=<?php echo $token; ?>&updateType=removeProduct&removeProductId='+removeId,
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                //
            } else {
                $("#purchase_"+removeId).remove();
                $('#sub_total').val(result['sub_total']);
                $('#sub_total_value').val(result['sub_total_value']);
                $('#total').val(result['total']);
                $('#discount').val(result['discount']);
                $('#tax').val(result['tax']);
                if(result['sub_total_value']=='0'){
                    $("#TRtax").remove();
                    $("#TRdiscount").remove();
                }
                if(result['tax']=='0'){
                    $("#TRtax").remove();
                }
                if(result['discount']=='0'){
                    $("#TRdiscount").remove();
                }
            }
        }
    });
}

function updateBillDiscount(fieldValue) {
    if (fieldValue) {
        $("#pre-loader-icon").show();
        var ajaxData = $("form").serialize();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/stock_transfer_out/AjaxBillDiscountUpdate&token=<?php echo $token; ?>&updateType=billDiscount',
            data: ajaxData,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    if(result['discountRow']){
                        $("#TRsub_total").after(result['discountRow']);
                    }
                    $('#sub_total').val(result['sub_total']);
                    $('#sub_total_value').val(result['sub_total_value']);
                    $('#total').val(result['total']);
                    $('#discount').val(result['discount']);
                    $('#tax').val(result['tax']);
                    $('#bill_discount_percentage').val('');
                    $('#bill_discount_price').val('');
                }
                $("#pre-loader-icon").hide();
            }
        });
    }
}

</script>
<?php echo $footer; ?>
<script type="text/javascript">
    $('html').click(function(e){
    if (e.target.id != 'country-list') {
        $("#suggesstion-box").hide(); 
    }
});

window.onload = function(e){ 
     var option  = $("#vendor_code option:selected").attr("class");
     if(parseInt(option)=='1'){
            var listVal = $('#tax_class_id option:eq(1)').val();
            $("#tax_class_id").val(listVal);
            $("#vendor_allow_gst").val(1);
     }   
}
var isSubmitting = false
$(document).ready(function () {
    $('#formID').submit(function(){
        isSubmitting = true
    })
    var initial_data = $("#formID").serialize();
    $(window).on('beforeunload', function() {

        if (!isSubmitting && $('form').serialize() != initial_data ){
            return 'You have unsaved changes which will not be saved.'
        }
    });
});

</script>
<div id="ajax-modal" class="modal container fade" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Available Products</h4>
    </div>
    <div class="modal-body">
        Loading........
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>