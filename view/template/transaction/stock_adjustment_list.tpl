<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Stock Adjustment</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php //if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" name="purchase_button" value="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <?php //} ?>
                <?php if ($hold) { ?>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> <?php echo $button_back; ?></button></a>
                    <?php } else { ?>
                    <a href="<?php echo $show_hold_button; ?>"><button type="button" class="btn btn-action btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-ban"></i> <?php echo $button_show_hold; ?></button></a>
                    <?php } ?></div>    
                    </div>
                </div>               						
			</div>
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post" name="list_filter" id="list_form_validation"> 
                <input type="hidden" name="page" value="<?php echo $page; ?>">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $filter['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $filter['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                
                         <td>
                           <select name="filter_location_code" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Location --</option>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter['filter_location_code'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>   
                          <td class="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
            </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->

			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
                      <td class="center">S.No</td>
                      <td class="center"><?php echo "Stock Adjust Date"; ?></td>
                      <td class="center"><?php echo "Location Code"; ?></td>
                      <td class="center"><?php echo "Terminal  Code"; ?></td>
                      <td class="center"><?php echo "Add/Deduce"; ?></td>
                      <td class="center"><?php echo $text_net_total; ?></td>
                      <td class="center"><?php echo $text_created_by; ?></td>
                      <td class="center"><?php echo $text_created_on; ?></td>
                      <td class="center"><?php echo $column_action; ?></td>
                      <?php /*if ($hold): ?>
                      <td class="center"><?php echo $column_action; ?></td>
                      <?php endif;*/ ?>
					           </tr>
                        </thead>
                        <tbody>
                  <?php if ($purchases) { 
                    $class = 'odd'; $i=1; 
                    foreach ($purchases as $purchase) { 
                    $class = ($class == 'even' ? 'odd' : 'even'); ?>
        					<tr class="<?php echo $class; ?>">
        					  <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
                      <td class="center"><?php echo $purchase['stkAdjustment_date']; ?></td>
                     <td class="center"><?php echo $purchase['location_code']; ?></td>
                     <td class="center"><?php echo $purchase['terminal_code']; ?></td>
                     <td class="center"><?php if($purchase['add_or_deduct'] == 1){ echo "Deduct"; } else { echo "Add"; } ?></td>
                      <td class="right"><?php echo $purchase['total_value']; ?></td>
                      <td class="center"><?php echo $purchase['created_by']; ?></td>
                      <td class="center"><?php echo $purchase['created_date']; ?></td>
                      <!-- <td class="center"><a href="<?php echo $purchase['view_button'] ?>">View</a></td> -->
                      <td class="center">
                      <?php if ($hold): ?>
                      <a href="<?php echo $purchase['modify_button'] ?>"><i class='fa fa-edit' style='font-size:19px' data-toggle="tooltip" title="Modify"></i></a>&nbsp;&nbsp;
                      <?php endif; ?>
                       <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="View"></i></a>
                      </td> 
					</tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
</script> 
<?php echo $footer; ?>