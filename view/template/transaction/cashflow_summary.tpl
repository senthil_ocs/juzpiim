<!--- NEW CODE -->
<?php echo $header; ?>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!--  <h3 class="page-title">CashFlow Summary Report</h3>      -->
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
                <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                 <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

        <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <table class="table orderlist statusstock">
                      <tr>
                          <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                      </tr>
                      <tr>
                          <td align="center" colspan="2">
                            <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                            <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                          </td>
                      </tr>
                      <tr>
                          <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                      </tr>
                            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                      <tr <?php echo $contact; ?>>
                          <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                          <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                      </tr>
                </table>
              </div>
              <h3 class="page-title">CashFlow Summary Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->

          <div>
              <p>
                <?php if($filter_date_from != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From - <span id="filter_date_from_id"></span> </span>
                <?php } ?>
                <?php if($filter_date_to != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Date To  - <span id="filter_date_to_id"></span> </span>
                <?php } ?>
                <?php if($filter_teminal != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Terminal Code  - <span id="filter_terminal_id"></span> </span>
                <?php } ?>
                 <?php if($filter_location != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Filter Location - <span id="filter_location_id"></span> </span>
                <?php } ?>
              </p>
        </div>
       <!-- filter display end --> 
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">    
                       <td>
                        <input type="text" name="filter_date_from" placeholder="From Date" value="<?php echo $data['filter_date_from'];?>" class="textbox date" autocomplete="off" style="width: 20%;"/>
                      </td>
                       <td>
                        <input type="text" name="filter_date_to" placeholder="To Date" value="<?php echo $data['filter_date_to'];?>" class="textbox date" autocomplete="off" style="width: 20%;"/>
                      </td>  
                      <td>
                         <select name="filter_location" id="filter_location_code" onchange="getterminals(this.value);" style="min-height: 35px; padding: 7px; width: 100%;">
                          <?php if(count($Tolocations)>=2){?>
                          <option value="">-- Select Location --</option>
                          <?php }?>
                          <?php
                             if(!empty($Tolocations)){
                                  foreach($Tolocations as $value){ ?>
                                  <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>//location_name

                          <?php  } } ?> 
                          </select>
                        </td>
                        <td>
                          <select name="filter_teminal" class="textbox" style="min-height: 35px; padding: 7px;" id="filter_teminal" onchange="getCashier(this.value);">
                            <option value="0">Select Terminal</option>
                              <?php for($i=0;$i<count($terminals);$i++){ ?>
                                  <?php if ($terminals[$i]['shift_teminal'] == $_REQUEST['filter_teminal']) { ?>
                                              <option value="<?php echo $terminals[$i]['shift_teminal'];?>" selected="selected"><?php echo $terminals[$i]['shift_teminal'];?></option>
                                   <?php } else { ?>
                                              <option value="<?php echo $terminals[$i]['shift_teminal'];?>"><?php echo $terminals[$i]['shift_teminal'];?></option>
                                   <?php } ?>
                               <?php } ?>
                          </select>
                        </td>
                        <td>OrderBy:</td>
                         <td>
                          <select name="short_by" class="textbox" style="min-height: 35px; padding: 7px;" id="short_by">
                            <option value="collection_date"  <?php if($short_by == 'collection_date') echo 'selected'; ?>>Collection Date</option>
                            <option value="shift_refno"  <?php if($short_by == 'shift_refno')echo 'selected'; ?> >Shift Ref No </option>
                          </select>
                        </td>
                         <td>
                         <select name="short_by_order" class="textbox" style="min-height: 35px; padding: 7px;" id="short_by">
                            <option value="ASC" <?php if($short_by_order == 'ASC') echo 'selected'; ?>>ASC</option>
                            <option value="DESC" <?php if($short_by_order == 'DESC') echo 'selected'; ?>>DESC</option>
                          </select>
                        </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Location</td>
                          <td class="center">Date</td>
                          <td class="center">Terminal</td>
                          <td class="center">Opening Cash</td>
                          <td class="center">Cash Sales</td>
                          <td class="center">Credit Collection</td>
                          <td class="center">Payin</td>
                          <td class="center">Payout</td>
                          <td class="center">Closing Balance</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php //printArray($cashflow);
                        if ($cashflow) { $i=0; ?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($cashflow as $value) { $i++; ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>"> 
                          <td class="center"><?php echo $value['shift_location_code']; ?></td>   
                          <td class="center"><?php echo $value['collection_date']; ?></td>
                          <td class="center"><?php echo $value['terminal_code']; ?></td>
                          <td class="right"><?php echo $value['opening_cash']; ?></td>
                          <td class="right"><?php echo $value['cash_collection']; ?></td>
                          <td class="right"><?php echo $value['credit_cash_collection']; ?></td>
                          <td class="right"><?php echo $value['cash_in_machine']; ?></td>
                          <td class="right"><?php echo number_format($value['cash_out_machine'],2); ?></td>
                          <td class="right"><?php echo number_format($value['closing_cash'],2); ?></td>
                        </tr>
                        <?php } ?>
                        <!-- <tr><td colspan="4" class="text-right"><b>Total</b></td><td class="text-right"><?php echo number_format($total,2); ?></td><td colspan="2"></td></tr> -->
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
     document.report_filter.submit();
}


$(document).ready(function(){
  var filter_date_from = $('[name="filter_date_from"]').val();
    $( "#filter_date_from_id" ).html(filter_date_from);

    var filter_date_to = $('[name="filter_date_to"]').val();
    $( "#filter_date_to_id" ).html(filter_date_to);

     var filter_teminal = $('[name="filter_teminal"]').val();
    $( "#filter_terminal_id" ).html(filter_teminal);

    var filter_location = $('[name="filter_location"] option:selected').text();
    $( "#filter_location_id" ).html(filter_location);
});
/*$(window).load(function(){
$('#filter_location_code').trigger('change');
});*/
function getterminals(va){
  //alert(va);
  if(va!=''){
    $.ajax({
          type: "POST",
          url: 'index.php?route=transaction/transaction_reports/AjaxloadTerminals&token=<?php echo $token; ?>&locationCode='+va,
          data: va,
          dataType: 'json',
          success: function(result) {
              if (result=='') {
                  //
              } else {
                $('select#filter_teminal').children('option:not(:first)').remove();
                 for (var i = 0; i < result.length; i++) //The json object has lenght
                  {
                      var object = result[i]; //You are in the current object
                      $('#filter_teminal').append('<option value='+object.shift_teminal+'>' + object.shift_teminal  + '</option>'); //now you access the property.
                  }
              }
          }
      });
  }
}
function getCashier(va){
    if(va!=''){
        locationCode     =  $("#filter_location").val();
        filter_date      =  $("#filter_date_from").val();
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/transaction_reports/AjaxloadTerminalsCashier&token=<?php echo $token; ?>&locationCode='+locationCode+'&terminal='+va+'&filter_date='+filter_date,
            data: va,
            dataType: 'json',
            success: function(result) {
                if (result=='') {
                    //
                } else {
                  $('select#filter_cashier').children('option:not(:first)').remove();
                   for (var i = 0; i < result.length; i++) //The json object has lenght
                    {
                        var object = result[i]; //You are in the current object
                        $('#filter_cashier').append('<option value='+object.shift_cashier+'>' + object.shift_cashier  + '</option>'); //now you access the property.
                    }
                }
            }
        });
    }
}

</script> 
