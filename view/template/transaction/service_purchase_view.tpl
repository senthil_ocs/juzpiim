<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Service Note Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                	<?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <?php if(!$purchaseInfo['isinvoice'] && !$purchaseInfo['hold']){  ?>
                     <!--  <a href="<?php echo $convert_invoice; ?>" class="btn btn-info" style="margin: 5px 5px 0 0; border-radius: 4px">Convert Invoice</a> -->
                    <?php }  ?>
                    <a href="<?php echo $download_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-print"></i> Print</a>

                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>

                    <!-- <a href="<?php echo $delete_button; ?>" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i>Delete</a> -->
                    
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['transaction_no'] ?></td>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['transaction_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Vendor Code</td>
                          <td><?php echo $vendorDetail['vendor_code']; ?></td>
                          <td>Vendor Name</td>
                          <td><?php echo $vendorDetail['vendor_name']; ?></td>
                        </tr>
                        <tr>
                          <td>Currency Code</td>
                          <td><?php echo $purchaseInfo['currency_code']; ?></td>
                          <td>Currency Rate</td>
                          <td><?php echo $purchaseInfo['conversion_rate']; ?></td>
                        </tr>
                        <tr>
                          <td>Remark</td>
                          <td colspan="3"><?php echo $purchaseInfo['remarks']; ?></td>
                        </tr>
                        <tr>
                          <td>Created by</td>
                          <td><?php echo $purchaseInfo['created_by'].' at '.showDate($purchaseInfo['created_date']); ?></td>
                          <td>Modified by</td>
                          <td><?php echo $purchaseInfo['modified_by'].' at '.showDate($purchaseInfo['date_modified']); ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Product Name</td>
                        <td class="center">Description</td>
                        <td class="center">Quantity</td>
                        <td class="center">Quantity(FOC)</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Total</td>                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1; 
                           ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php //$barcode   = $this->model_transaction_purchase->getProductBarCode($products['product_id']); ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['name']; ?></td>
                              <td><?php echo nl2br($products['description']); ?></td>
                              <td><?php echo $products['qty']; ?></td>
                              <td><?php echo $products['foc']; ?></td>
                              <td class="text-right"><?php echo number_format($products['prices'] * $products['conversion_rate'],2); ?></td>
                              <td class="text-right"><?php echo number_format($products['net_price'] * $products['conversion_rate'],2); ?></td>
                            </tr>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="7" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['sub_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Discount</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['discount'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Delivery Fees</td>
                              <td class="text-right"><?php echo $purchaseInfo['handling_fee']; ?></td>
                            </tr>
                            <tr>
                              <?php $tax_type=$purchaseInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
                                    $tax_type= $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)'; ?>
                              <td colspan="7" class="text-right"><?php echo 'GST '.$tax_type; ?></td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['gst'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="7" class="text-right">FCTotal</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['fc_nettotal'],2); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

</script>
<?php echo $footer; ?>