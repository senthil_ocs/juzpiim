<!-- NEW CODE -->
<?php echo $header; ?>
<div class="clearfix"></div>
<div class="page-container">
   <?php echo $sidebar; ?>
   <div class="page-content-wrapper">
      <div class="page-content">
         <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
            <ul class="page-breadcrumb">
               <?php foreach ($breadcrumbs as $breadcrumb) { ?>
               <li>
                  <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
               </li>
               <?php  }?>
            </ul>
            <div class="page-toolbar">
               <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                     <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
                     
                     <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                     
                     <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                     <i class="fa fa-file-pdf-o"></i> Export PDF</a>
                     
                     <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
                  </div>
               </div>
            </div>
         </div>
         <div style="clear:both"></div>
         <div class="row">
            <div class="col-md-12">
               <div class="innerpage-listcontent-blocks"> 
                  <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
               </div>
               <h3 class="page-title">Inventory Movement Report </h3>
            </div>
         </div>
         <!-- NEW SEARCH  -->
         <div class="innerpage-listcontent-blocks">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
               <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px -10px -15px -10px; padding:16px 0 0 0;">
                  <div class="caption" id="reportsuggestion">
                     <form method="post" id="report_form" action="<?php echo $action; ?>">
                        <input type="hidden" name="type" id="type">                 
                        <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                           <tbody>
                              <tr class="filter">
                                 <td>
                                    <select name="filter_location" id="filter_location" class="textbox" required>
                                       <option value=""> Select Location </option>
                                       <?php
                                          if(!empty($Tolocations)){
                                               foreach($Tolocations as $value){ ?>
                                       <option value="<?php echo $value['location_code'];?>" 
                                          <?php if(trim($filters['filter_location']) == trim($value['location_code']))echo 'selected'; ?> ><?php echo $value['location_name']; ?>                                            
                                       </option>
                                       <?php } }  ?> 
                                    </select>
                                 </td>
                                 <td> 
                                    <input type="text" id='filter_date_from' placeholder='From Date'  name="filter_date_from" value="<?php echo 
                                       $filters['filter_date_from']; ?>" class="textbox date" autocomplete="off" readonly required>
                                 </td>
                                 <td>
                                    <input type="text" placeholder='To Date' id="filter_date_to" name="filter_date_to" value="<?php echo $filters['filter_date_to'] ; ?>" readonly class="textbox date" autocomplete="off" required>
                                 </td>
                                 <td>
                                    <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                                    <input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $_REQUEST['filter_product_id'] ; ?>">
                                    <input type="text" name="filter_product" id="filter_product" value="<?php echo $filters['filter_product'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                                    <div id="suggesstion-box" class="auto-compltee"></div>
                                 </td>

                                 <td align="center" colspan="2">
                                    <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                  </div>
                 </div>
                </form>
               <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
         </div>

         <div style="clear:both; margin: 0 0 15px 0;">
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="innerpage-listcontent-blocks">
                  <table class="table orderlist statusstock">
                     <thead>
                        <tr class="heading">
                           <td class="center">Trans. Date</td>
                           <td class="center">Trans. No</td>
                           <td class="center">Location Code</td>
                           <td class="center">Type</td>
                           <td class="center">In</td>
                           <td class="center">Out</td>
                        </tr>
                     </thead>
                     <tbody>
                        <?php if (!empty($allDatas)) {
                           $total_in = $total_out = 0;
                           foreach ($allDatas as $data) {
                           
                               $in   = $out = '';
                               if(!$data['method']){
                                   $total_in += $in = (int)$data['qty']; 
                               } else {
                                   $total_out += $out = (int)$data['qty'];
                               }
                           ?>
                        <tr>
                           <td class="center"><?php echo showDate($data['rdate']); ?></td>
                           <td class="center"><?php echo $data['invoice_no']; ?></td>
                           <td class="center"><?php echo $data['location']; ?></td>
                           <td class="center"><?php echo $data['type']; ?></td>
                           <td class="text-right"><?php echo $in; ?></td>
                           <td class="text-right"><?php echo $out; ?></td>
                        </tr>
                        <?php } ?>
                        <tr>
                           <td class="text-right" colspan="4">Total</td>
                           <td class="text-right"><?php echo $total_in; ?></td>
                           <td class="text-right"><?php echo $total_out; ?></td>
                        </tr>
                        <?php } else { ?>
                        <tr>
                           <td class="text-center" colspan="4">No Data Found</td>
                        </tr>
                        <?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
   function getProductautoFill(sku) {
       var frm_date = $("#filter_date_from").val();
       var to_date = $("#filter_date_to").val();
       if(frm_date==''){
         alert("From date should not empty");
         $("#filter_date_from").focus();
       }else if(to_date==''){
         alert("To date should not empty");
         $("#filter_date_to").focus();
       }
       $('#filter_product_id').val('');
       if(sku.length < SEARCH_CHAR_CNT){
           return false;
       }
   
       var location = $("#filter_location").val();
       var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;

       $.ajax({
           type: "POST",
           url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
           data: ajaxData,
           success: function(result) {
               if (result=='') {
   
               } else {
                   if (result.indexOf("li")>= 0){
                       $("#suggesstion-box").show();
                       $("#suggesstion-box").html(result);
                   }else{
                       $('#suggesstion-box').hide();
                       selectedProduct(result,'1');
                   }
                   return false;
               }
               $("#pre-loader-icon").hide();
           }
       });
   }
   function selectedProduct(val,cnt) {
       var newVal = val.split("||");
       $('#filter_product').val(newVal[2].replace("^","'").replace('!!','"'));
       $('#filter_product_id').val(newVal[0]);
       $('#suggesstion-box').hide();
   }
   
   $("#report_form").submit(function(){
       if($('#filter_product_id').val() ==''){
           alert('Please search Product');
           $('#filter_product').focus();
           return false;
       }
   });
</script>