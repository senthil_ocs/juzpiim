<?php echo $header; ?>
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Delivery Form</h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
         <div class="page-toolbar">
           <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0 !important">
                <button class="btn btn-primary" id="saveBtn" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i>&nbsp;&nbsp;Save</button>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</button></a>
            </div>
            </div>
          </div>
      </div>

        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption">
              <form method="POST" id="filter">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                         <td>
                           <input type="text" name="filter_customer_name" value="<?php echo $data['filter_customer_name']; ?>" placeholder="Customer Name" class="textbox">
                        </td>
                         <td>
                           <input type="text" name="filter_contact_number" value="<?php echo $data['filter_contact_number']; ?>" placeholder="Contact Number" class="textbox">
                        </td>
                         <td>
                           <input type="text" name="filter_transaction" value="<?php echo $data['filter_transaction']; ?>" placeholder="Invoice / Network Id" class="textbox">
                        </td>
                         <td>
                           <select name="filter_sales_person" style="min-height: 35px; padding: 7px; width: 100%; border: 1px solid #fff;">
                             <option value="">Select Sales Person</option>
                             <?php foreach ($salesPersonList as $value) { ?>
                                  <option value="<?php echo $value['id']; ?>" <?php if($data['filter_sales_person'] == $value['id']){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                              <?php } ?>
                           </select>
                        </td>
                        <td>
                          <input type="text" id='filter_from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $data['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $data['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td class="center" colspan="4">
                          <button class="btn btn-zone btn-primary" type="submit" name="type" value="filter"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
              </div>              
            </div>     
          </form>
        <div style="clear:both; margin: 0 0 15px 0;"></div>
      </div>
    </div>


    <?php if ($success) { ?>
    <div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert"></button>
    </div>
    <?php } ?>

    <div class="row">
      <div class="col-md-12">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
                  <div class="portlet bordernone" style="margin-top: 30px;margin-bottom: -25px;">
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Delivery man</td>
                          <td>
                            <select name="delivery_man" id="delivery_man" style="min-height: 25px; padding: 2px; width: 80%;">
                              <option value="">Select Delivery man</option>
                              <?php if(count($deliveryManList) > 0){
                                foreach ($deliveryManList as $value) {  ?>
                                  <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                              <?php } } ?>
                            </select>
                          </td>
                          <td>Delivery Date</td>
                          <td class="order-nopadding">
                            <input type="text" id='delivery_date' placeholder='Select Delivery Date' name="delivery_date"class="textbox date purchase_inputfields" autocomplete="off">
                          </td>
                          <td style="border: 1px solid #fafafa; width: 20%;"></td>
                        </tr>
                      </thead>
                  </table>
                  </div>
                </div>
                  <div class="innerpage-listcontent-blocks">
                  <div class="portlet bordernone" style="margin-bottom:0 !important">

                    <table class="table orderlist statusstock" id="table_sorts">
                    <thead>
                      <tr class="heading">
                        <td width="1" style="text-align: center;"><input type="checkbox" id="select_all" name="select_all" /></td>
                        <td class="center">S.No</td>
                        <td class="center">Invoice No</td>
                        <td class="center">Invoice Date</td>
                        <td class="center">Reference No</td>
                        <td class="center">Customer</td>
                        <td class="center">Net Total</td>
                        <td class="center">From</td>
                      </tr>
                        </thead>
                        <tbody>
                    <?php if ($allDatas) { ?>
                    <?php $class = 'odd'; $i=1; ?>
                    <?php foreach ($allDatas as $data) { 
                      foreach ($data['details'] as $invoice) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <tr class="<?php echo $class; ?>">  
                        <td style="text-align: center;">
                            <input type="checkbox" class="selecteds" name="selecteds[<?php echo $invoice['invoice_no']; ?>]" value="<?php echo $data['from']; ?>">
                        </td>
                        <td class="center"><?php echo $i; ?></td>
                        <td class="center">
                          <?php echo $invoice['invoice_no']; if($invoice['istagged']){  ?> <i class="fa fa-tag" aria-hidden="true" style="color: #f1540c;font-size: 12px;"></i><?php } ?>
                        </td>
                        <td class="center"><?php echo date('d/m/Y',strtotime($invoice['invoice_date'])); ?></td>
                        <td class="center"><?php echo $invoice['network_order_id']; ?></td>
                        <td class="center"><?php echo $invoice['name']; ?></td>
                        <td class="center"><?php echo $invoice['net_total']; ?></td>
                        <td class="center"><?php echo showText($data['from']); ?></td>
                    </tr>
                    <?php $i++; } } ?>
                    <?php } else { ?>
                    <tr>
                      <td align="center" colspan="12">No results!</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </form>
          <div class="pagination"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">

$('#delivery_date').datepicker({
    minDate : 0,
    dateFormat: 'dd/mm/yy'
});

$('#table_sorts').dataTable({
    "aaSorting" : [[ 1, "asc" ]],
         paging : false,
      searching : false,
        "bInfo" : false,
      "columnDefs": [ {
          "targets": [0],
          "orderable": false
        },
        {
        "targets": "_all",
        "defaultContent": "-"
      } ]
});

$('#saveBtn').click(function(){
    $('#form').submit();
});

$('#search').click(function(){
  $('#search').submit();
});

$("#select_all").click(function(){
    if(this.checked){
        $('.selecteds').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.selecteds').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
      $('#total_amount').html('');
    }
});
</script>