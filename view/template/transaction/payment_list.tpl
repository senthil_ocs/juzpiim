<?php echo $header; ?>
</style>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Payment List</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
				</ul>
				 <div class="page-toolbar">
           <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0 !important">
                <a href="<?php echo $payment_insert; ?>"><button type="button" name="purchase_button" value="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> New Payment</span></button></a>
            </div>
            </div>
          </div>
			</div>
      
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 95px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption" style="width: 96%;">
              <form method="POST" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 100%;">
                    <tbody>
                      <tr class="filter">                     
                        <td><input type="hidden" name="page"> <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off" >
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox salesdropdown" placeholder="Transaction / Reference No">
                        </td>
                        <td>
                          <input type="hidden" name="filter_customer" id="filter_customer2">
                          <input type="text" id="filter_customer" class="textbox salesdropdown" value="" placeholder="Search Customer">
                        </td>
                      </tr>
                      <tr class="filter">
                        <td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                              <option value="Pending" <?php if(in_array('Pending',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Pending</option>
                              <option value="Scheduling" <?php if(in_array('Scheduling',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Scheduling</option>
                              <option value="Assigned" <?php if(in_array('Assigned',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Assigned</option>
                              <option value="Pending_Delivery" <?php if(in_array('Pending_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Pending Delivery</option>
                              <option value="On_Delivery" <?php if(in_array('On_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>On Delivery</option>
                              <option value="Partial_Delivered" <?php if(in_array('Partial_Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Partial Delivered</option>
                              <option value="Delivered" <?php if(in_array('Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Delivered</option>
                              <option value="Canceled" <?php if(in_array('Canceled',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Canceled</option>
                          </select>
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_payment_status[]" id="filter_payment_status">
                            <option value="Pending" <?php if(in_array('Pending',$filters['filter_payment_status'])){ echo "selected"; } ?>>Pending</option>
                            <option value="Paid" <?php if(in_array('Paid',$filters['filter_payment_status'])){ echo "selected"; } ?>>Paid</option>
                            <option value="Partial" <?php if(in_array('Partial',$filters['filter_payment_status'])){ echo "selected"; } ?>>Partial</option>
                            <option value="Hold" <?php if(in_array('Hold',$filters['filter_payment_status'])){ echo "selected"; } ?>>Hold</option>
                            <option value="Canceled" <?php if(in_array('Canceled',$filters['filter_payment_status'])){ echo "selected"; } ?>>Canceled</option>
                          </select>
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_network[]" id="filter_network">
                            <?php foreach ($networks as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$filters['filter_network'])){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <td>
                          <select class="textbox" name="filter_location" id="filter_location">
                            <option value="">Select Location</option>
                            <?php foreach($Tolocations as $values){ ?>
                                <option value="<?php echo $values['location_code'];?>" <?php if($values['location_code']==$filters['filter_location']){ echo "selected"; } ?>><?php echo $values['location_name']; ?></option>
                            <?php  } ?>
                          </select>
                        </td>
                        <td align="right">
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                  </tbody>
                </table>
              </div>                        
            </div>     
          </form>
          <div style="clear:both; margin: 0 0 15px 0;"></div>
    </div>
  </div>

			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
      </div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
        </div>
			<?php } ?>

		<div class="row">
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
                    <input type="hidden" name="page">
            				<table class="table orderlist statusstock" id="table_sort">
                    <thead>
                        <tr class="heading">
                            <td class="center">S.No</td>
                            <td class="center"><?php echo $text_tran_no; ?></td>
                            <td class="center">Payment Date</td>
                            <td class="center">Location</td>
                					  <td class="center">Customer</td>
                            <td class="center"><?php echo $text_net_total; ?></td>
                            <td class="center">Delivery</td>
                            <td class="center">Payment</td>
                            <td class="center">Post</td>
                            <td class="center">Action</td>
               					</tr>
                      </thead>
                      <tbody>
                  <?php if ($purchases) {  
                  $class = 'odd'; $i=1;  
                  foreach ($purchases as $purchase) {  
                  $class = ($class == 'even' ? 'odd' : 'even'); ?>
        					
                  <tr class="<?php echo $class; ?>">
                      <td class="center"><?php echo $purchase['pagecnt'] +$i; ?></td>
				          	  <td class="center"><?php echo $purchase['invoice_no']; ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['location']; ?></td>
                      <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="center"><?php echo $purchase['total']; ?></td>
                      <td align="center"><?php echo showText($purchase['delivery_status']); ?></td>
                      <td align="center"><?php echo $purchase['payment_status']; ?></td>
                      <td style="text-align: center;" class="tdclass_<?php echo $purchase['payment_id']; ?>">
                       <?php if($purchase['payment_status'] =='Paid'){ 
                        if(!empty($purchase['xero_response'])){ ?>
                        <span class="text">Success</span>
                        <?php }else if($purchase['xero_post']){ ?>
                           <span class="text-primary xero" data-sales_id="<?php echo $purchase['payment_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:16px; cursor: pointer;" data-toggle="tooltip" title="Post"></i></span>
                       <?php  } else { 
                          echo '<i class="fa fa-ban" aria-hidden="true" style="font-size:16px;color:red;">'; 
                        } } else{ echo ' '; } ?>
                      </td>
                      <td>
                         <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:14px;" data-toggle="tooltip" title="View"></i></a>
                         <?php if($purchase['payment_status'] !='Canceled'){ ?>
                          &nbsp;<a href="#" class="cancelPayment" id="cancelBtn_<?php echo $purchase['payment_id']; ?>" data-payment_id="<?php echo $purchase['payment_id']; ?>"><i class="fa fa-trash " aria-hidden="true" style="font-size:15px;" data-toggle="tooltip" title="Cancel Payment"></i></a>
                         <?php } ?>
                      </td>
                    </tr>
                  <?php $i++; } ?>
                <?php } else { ?>
              <tr>
                <td align="center" colspan="12"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
  </div>
  </div>
</div>

<div id="ajax-modal" class="modal fade modal-scroll in" tabindex="-1">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="form-body" id="imagePopup"></div>
    <div class="modal-footer">
        <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
  </div>
</div>

<?php echo $footer; ?>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$(document).ready(function(){

    $("#filter_delivery_status").CreateMultiCheckBox({ 
      width: '230', 
      defaultText : 'Select Delivery Status', 
      height:'250px' 
    });
    $("#filter_network").CreateMultiCheckBox({ 
      width: '230px', 
      defaultText : 'Sales channel', 
      height:'250px' 
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '230px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    $.each( delivery, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    $.each( payment, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var network = <?php echo json_encode($filters['filter_network']); ?>;
    $.each( network, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });
    

  $('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs"    : [{
            "targets"   : [2,3,5,8],
            "orderable" : false
          },{
          "targets"       : "_all",
          "defaultContent": "-"
        }]
  });

  var customer = "<?php echo $filters['filter_customer']; ?>";
  if(customer !=''){
      getCustomerDetails(customer);
  }
});

function getCustomerDetails(cust){
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/sales/getCustomerDetails&token=<?php echo $token; ?>',
        data:{cust:cust},

        success:function(data){
          data = JSON.parse(data);
          $('#filter_customer').val(data.name);
          $('#filter_customer2').val(data.customercode);
        }
    });
}

$( "#filter_customer" ).autocomplete({
    source: function( request, response ) {
        $.ajax({
            url: "index.php?route=transaction/sales/customerSearch&token=<?php echo $token; ?>",
            type: 'post',
            dataType: "json",
            data: {
                search: request.term
            },
            success: function( data ) {
                response(data);
            }
        });
    },
    select: function (event, ui) {
        $('#filter_customer').val(ui.item.label);
        $('#filter_customer2').val(ui.item.data_ref);
        return false;
    }
});

function validJSON(text){
    if (typeof text!=="string"){
        return false;
    }
    try{
        JSON.parse(text);
        return true;
    }
    catch (error){
        return false;
    }
}

$(".xero").click(function(){
    var xero     = $(this);
    var order_id = $(this).attr('data-sales_id');
    $('.tdclass_'+order_id).html('<div class="loader"></div>');
    var xeroUrl  = '<?php echo XERO_URL; ?>payment.php?order_id='+order_id;

    $.get(xeroUrl, 
        function (response, status, error) {
          if(validJSON(response)){
            var data  = JSON.parse(response);
            if(data.status == 'success'){
              $('.tdclass_'+order_id).html('<span class="text">Success</span>');
            }else{
              $('.tdclass_'+order_id).html('Faild');
            }
          }else{
              $('.tdclass_'+order_id).html('Faild');
          }
    });
});

$('.cancelPayment').click(function(){
    if(confirm('Are you sure, Delete this payment.?')){
      
      var payment_id =  $(this).attr("data-payment_id");
      $("#cancelBtn_"+payment_id).html('<div class="loader"></div>');
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/payment/cancelPayment&token=<?php echo $this->session->data["token"]; ?>',
          data: {payment_id: payment_id},
          beforesend:function(){
          },
          success: function(r) {
             location.reload();
          }
      });
    }
});

</script>