<!-- NEW CODE -->
<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
                <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                 <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <div style="clear:both"></div>   
       <div class="row">     
        <div class="col-md-12">      
          <div class="innerpage-listcontent-blocks">
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
           <h1>Credit Pending Details</h1>
           <p>  <?php if($filter_location != '' || $filter_customer != '' || $filter_date_from!=''  || $filter_date_to!=''){ ?>
                <span style="font-size: 15px;"> Filter By </span>
                 <?php } ?>
                 <?php if($filter_location != ''){ ?>
                    <span style="font-size: 15px;margin-right: 15px;"> Location - <span id="filter_location_codeid"><?php echo $filter_location;?></span> </span>
                <?php } ?>

                <?php if($filter_customer != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Customer - <span id="filter_customerid"></span> </span>
                <?php } ?>
                <?php if($filter_date_from != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Date From - <span id="filter_date_fromid"></span> </span>
                <?php } ?>
                <?php if($filter_date_to != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Date to - <span id="filter_date_toid"></span> </span>
                <?php } ?>
              </p>
        </div>
      </div>  


      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
            <div class="caption set-bg-color"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                       <td>
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo 
                          $filter_date_from; ?>" class="textbox date" autocomplete="off"  >
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $filter_date_to ; ?>" class="textbox date" autocomplete="off" >
                            </td>  
                        <td>
                           <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                        </select>
                        </td>
                    </td>
                      <td>
                       
                      <select name="filter_customer" class="textbox" style="min-height: 35px; padding: 7px">
                        <option value="">Select customer</option>
                          <?php foreach ($customer as $cust) { ?>
                                                            <?php if (trim($cust['member_code']) == trim($filter_customer)) { ?>
                                                                <option value="<?php echo trim($cust['member_code']); ?>" selected="selected"><?php echo $cust['name']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo trim($cust['member_code']); ?>"><?php echo $cust['name']; ?></option>  
                                                            <?php } ?>   
                                                        <?php } ?>
                      </select>
                    </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
             <table class="table orderlist statusstock">
                        <tr>
                            <td colspan="4" class="right">Total :
                                <?php echo number_format($creditpending_tot,2); ?>
                            </td>
                        </tr>
                    </table>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Location</td>
                          <td class="center">Customer</td>
                          <td class="center">Invoice No</td>
                          <td class="center">Invoice Date</td>
                          <td class="center">Payment Type</td>
                          <td class="center">Payment Amount</td>
                          <td class="center">Remarks</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($creditpending) { $i=0; $total=0; ?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($creditpending as $value) { $i++; $total+=$value['payment_amount']; ?>
                        <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $value['location_code']; ?></td>
                          <td class="center"><?php echo $value['name']; ?></td>
                          <td class="center"><?php echo $value['invoice_no']; ?></td>
                          <td class="center"><?php echo $value['createdon']; ?></td>
                          <td class="center"><?php echo $value['payment_type']; ?></td>
                          <td class="text-right"><?php echo number_format($value['payment_amount'],2); ?></td>
                          <td class="center"><?php echo $value['Remarks']; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
     document.report_filter.submit();
}
//--></script> 
<?php echo $footer; ?>
<script type="text/javascript">
  $(document).ready(function(){
  var location_code = $('[name="filter_location"] option:selected').text();
  $( "#filter_location_codeid" ).html(location_code);

  var filter_customerid = $('[name="filter_customer"] option:selected').text();
  $( "#filter_customerid" ).html(filter_customerid);

  var filter_date_fromid = $('[name="filter_date_from"]').val();
  $( "#filter_date_fromid" ).html(filter_date_fromid);

   var filter_date_toid = $('[name="filter_date_to"]').val();
  $( "#filter_date_toid" ).html(filter_date_toid);
});
</script>