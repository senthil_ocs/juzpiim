<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Sales Return Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                	<?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
            <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    
                    <?php if($purchaseInfo['xero_sales_id'] ==null){  ?>
                      <!--<a href="<?php echo $modify_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-edit"></i> Modify</a>-->
                    <?php } if($purchaseInfo['status']==null){  ?>
                        
                        <a href="<?php echo $delete_button; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i>Delete</a>
                    <?php }  ?>
                    <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['invoice_no'] ?></td>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['invoice_date'])); ?></td>
                        </tr>
                        <tr>
                          <td>Customer Code</td>
                          <td><?php echo $vendorDetail['cust_code']; ?></td>
                          <td>Customer Name</td>
                          <td><?php echo $vendorDetail['name']; ?></td>
                        </tr>
                        <tr>
                          <td>Ref No</td>
                          <td><?php echo $purchaseInfo['reference_no']; ?></td>
                          <td>Ref Date</td>
                          <td><?php echo date("d/m/Y", strtotime($purchaseInfo['header_remarks'])); ?></td>
                        </tr>
                        <tr>
                          <td>Currency Code</td>
                          <td><?php echo $purchaseInfo['currency_code']; ?></td>
                          <td>Currency Rate</td>
                          <td><?php echo $purchaseInfo['conversion_rate']; ?></td>
                        </tr>
                        <tr>
                          <td>Remark</td>
                          <td colspan="3"><?php echo $purchaseInfo['remarks']; ?></td>
                        </tr>
                        <tr>
                          <td>Created by</td>
                          <td><?php echo $purchaseInfo['createdby'].' at '.showDate($purchaseInfo['createdon']); ?></td>
                          <td>Modified by</td>
                          <td><?php echo $purchaseInfo['modifiedby'].' at '.showDate($purchaseInfo['modifiedon']); ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Product Name</td>
                        <td class="center">Quantity</td>
                        <td class="center">Price(unit)</td>
                        <td class="center">Discount</td>
                        <td class="center">Sub Total</td>
                        <td class="center">GST</td>

                        <td class="center">Total Price</td>
                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1; 
                          //printArray($productDetails);
                           ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($productDetails as $products) { ?>
                            <?php //$barcode   = $this->model_transaction_purchase->getProductBarCode($products['product_id']); ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?></td>
                              <td><?php echo $products['description']; ?></td>
                              <td><?php echo $products['qty']; ?></td>
                              <td><?php echo $products['sku_price']; ?></td>
                               <td><?php echo $products['discount']; ?></td>
                              <td><?php echo number_format($products['qty'] * $products['sku_price'],3); ?></td>
                              <td><?php echo $products['gst']; ?></td>
                              <td><?php echo $products['net_total']; ?></td>
                            </tr>
                            <?php $i++; } ?>
                            <tr>
                              <td colspan="8" class="text-right">Sub Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['sub_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">Discount</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['discount'],2); ?></td>
                            </tr>
                            <tr>
                              <?php $tax_type=$purchaseInfo['tax_type']=='0' ? '(7% Exclusive)':'(7% Inclusive)';
                                    $tax_type= $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)'; ?>
                              <td colspan="8" class="text-right"><?php echo 'GST '.$tax_type; ?></td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['gst'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">Total</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['net_total'],2); ?></td>
                            </tr>
                            <tr>
                              <td colspan="8" class="text-right">FCTotal</td>
                              <td class="text-right"><?php echo number_format($purchaseInfo['fc_nettotal'],2); ?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

</script>
<?php echo $footer; ?>