<!-- NEW CODE -->
<?php echo $header; ?>
 <script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Itemwise Sales Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a> -->

              <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
             <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
             
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

       <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Aging Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
       <div>
            <p>
                <?php if($data['filter_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date From - <span id="filter_date_from_id"><?php echo $data['filter_date_from']?></span> </span>
                <?php } ?>
                <?php if($data['filter_date_to'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date To - <span id="filter_date_to_id"><?php echo $data['filter_date_to']?></span> </span>
                <?php } ?>
                 <?php if($data['filter_location'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Location - <span id="filter_location_id"><?php echo $data['filter_location']?></span> </span>
                <?php } ?>
                 <?php if($data['filter_invoice_no'] != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Invoice No - <?php echo $data['filter_invoice_no']?></span>
                <?php } ?>
                <?php if($data['filter_channel']!= ''){
                 $filter_network_name = $this->model_transaction_sales->getNetworkName($data['filter_channel']); ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Sales Channel : <?php echo $filter_network_name;?></span>
                <?php } ?>
            </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter" id="movement_report">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off" style="width: 20%;" autocomplete="off">
                                    </td> 
                                    <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px;">
                              <?php if(count($Tolocations)>=2){?>
                              <option value="">-- Select Location --</option>
                              <?php }?>
                              <?php
                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>
                              <?php  } } ?> 
                              </select>
                          </td>
                          <td>
                            <input type="text" name="filter_invoice_no" class="textbox" value="<?php echo $data['filter_invoice_no']; ?>" placeholder="Please enter Invoice No">
                          </td>
                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>
            </div>
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr  class="heading">
                          <td class="center">Sno</td>
                          <td class="center">Invoice No</td>
                          <td class="center">Invoice Date</td>
                          <td class="center">Location Code</td>
                          <td class="center">Customer</td>
                          <td class="center">Amount</td>
                          <td class="center">Paid</td>
                          <td class="center">Banance</td>
                      </tr>
                    </thead>
                    <tbody>
                        <?php if ($results) { $i=0;?>
                        <?php $class = 'odd'; ?>
                        <?php foreach ($results as $result) { $i++;
                          $balance = number_format($result['net_total'] - $result['paid_amount'],2);
                          $class = ($class == 'even' ? 'odd' : 'even'); ?>
                        
                        <tr class="<?php echo $class; ?>">          
                          <td class="center"><?php echo $result['pagecnt']+$i; ?></td>
                          <td class="center"><?php echo $result['invoice_no']; ?></td>
                          <td class="center"><?php echo date('d/m/Y',strtotime($result['invoice_date'])); ?></td>
                          <td class="center"><?php echo $result['location_code']; ?></td>
                          <td class="center"><?php echo $result['cust_name']; ?></td>
                          <td class="right"><?php echo number_format($result['net_total'],2); ?></td>
                          <td class="right"><?php echo number_format($result['paid_amount'],2); ?></td>
                          <td class="right"><?php echo $balance; ?></td>
                        </tr>
                        <?php } ?>
                        <?php } else { ?>
                        <tr>
                          <td align="center" colspan="7"><?php echo 'No Results'; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <div class="pagination"><?php echo $pagination; ?></div>
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
  document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_department_id" ).html(filter_department);

  var filter_location = $('[name="filter_location"] option:selected').text();
  $( "#filter_location_id" ).html(filter_location);

  var filter_category = $('[name="filter_category"] option:selected').text();
  $( "#filter_category_id" ).html(filter_category);

  var filter_brand = $('[name="filter_brand"] option:selected').text();
  $( "#filter_brand_id" ).html(filter_brand);

  var filter_vendor = $('[name="filter_vendor"] option:selected').text();
  $( "#filter_vendor_id" ).html(filter_vendor);

   var filter_name = $("#filter_name").val();
  $( "#filter_product_id" ).html(filter_name);
   
 
});

/*
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});
*/
  
function getProductautoFill(sku) {

    /*var frm_date = $("#filter_date_from").val();
    var to_date = $("#filter_date_to").val();
    if(frm_date==''){
      alert("From date should not empty");
      $("#filter_date_from").focus();
    }else if(to_date==''){
      alert("To date should not empty");
      $("#filter_date_to").focus();
    }*/
  var filter_name = $("#filter_name").val();
   
   if(filter_name ==''){
    $('#filter_sku').val('');
   }

  if(sku.length < SEARCH_CHAR_CNT){
      return false;
  }
    var location = $("#filter_location_code").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                  // alert("Product Not Available or Disabled.");
                  $("#suggesstion-box").hide();
                 clearPurchaseData();
                 return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
        var newVal = val.split("||");
       console.log(newVal);
        $('#filter_name').val(newVal[2].replace("^","'").replace('!!','"'));
        $('#filter_sku').val(newVal[1].replace("^","'").replace('!!','"'));
        $('#filter_product_id').val(newVal[0]);
        $('#suggesstion-box').hide();
        // if(cnt=='1'){
        //     $( "#movement_report" ).submit();
        // }
    }

function getValue(name,sku){
  console.log(sku);
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}
</script>
