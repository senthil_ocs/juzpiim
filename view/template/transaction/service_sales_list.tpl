<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Service Note</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
              	<?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                  
                  <button  class="btn btn-primary" id="export_pdf" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> PDF</span></button>

                  <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> New Service Note</span></button></a>

                  <button id="bulk_cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px; float: right;"><i class="fa fa-trash"></i><span> Cancel</span></button>
                
                <?php } ?>
                <?php if ($hold) { ?>
                    <a href="<?php echo $back_button; ?>"><button type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_back; ?></button></a>
                    <?php } else { ?>
                    <?php } ?>
                  </div>
               </div>
            </div>
      </div>

        <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 105px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption" style="width: 98%;">
              <form method="post" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                        <td>
                          <input type="hidden" name="page"> 
                          <input type="text" placeholder='Customer Name' name="filter_customer_name" value="<?php echo $filters['filter_customer_name'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" name="filter_contact_number" value="<?php echo $filters['filter_contact_number']; ?>" class="textbox" placeholder="Contact Number">
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox" placeholder="Invoice No" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" placeholder='Reference No' name="filter_order_number" value="<?php echo $filters['filter_order_number'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                           <select name="filter_sales_person" id="filter_sales_person" class="salesdropdown">
                            <option value="">Sales Person</option>
                            <?php
                                foreach($salesmanlist as $salesman){?>
                                  <option value="<?php echo $salesman['id'];?>" <?php echo $filters['filter_sales_person'] == $salesman['id'] ? 'selected' : '' ; ?>> <?php echo $salesman['name']; ?></option>

                            <?php  }  ?> 
                        </select>
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_network[]" id="filter_network">
                            <?php foreach($Tolocations as $values){ ?>
                                <option value="ln_<?php echo $values['location_code'];?>" <?php if(in_array($values['location_code'],$filters['filter_network'])){ echo "selected"; } ?>><?php echo $values['location_name']; ?></option>
                            <?php  } ?>
                            <?php foreach ($networks as $value) { ?>
                                <option value="nt_<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$filters['filter_network'])){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select>
                        </td>
                      </tr>
                      <tr class="filter">
                        <td>Delivery Date : <span style="float: right;"><input type="checkbox" name="filter_is_delivery" value="1" <?php if($filters['filter_is_delivery']=='1'){ echo "checked"; } ?> data-toggle="tooltip" title="Filter by delivery date"></span></td>
                        <td>
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                            <option value="Pending" <?php if(in_array('Pending',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Pending</option>
                            <option value="Assigned" <?php if(in_array('Assigned',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Assigned</option>
                            <option value="Delivered" <?php if(in_array('Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Delivered</option>
                            <option value="Canceled" <?php if(in_array('Canceled',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Canceled</option>
                          </select>
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_payment_status[]" id="filter_payment_status">
                            <option value="Pendings" <?php if(in_array('Pendings',$filters['filter_payment_status'])){ echo "selected"; } ?>>Pending</option>
                            <option value="Paid" <?php if(in_array('Paid',$filters['filter_payment_status'])){ echo "selected"; } ?>>Paid</option>
                            <option value="Partial" <?php if(in_array('Partial',$filters['filter_payment_status'])){ echo "selected"; } ?>>Partial</option>
                            <option value="Hold" <?php if(in_array('Hold',$filters['filter_payment_status'])){ echo "selected"; } ?>>Hold</option>
                          </select>
                        </td>
                        <td colspan="2" align="right">
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                </tbody>
              </table>
            </div>                        
          </div>     
        </form>
      <div style="clear:both; margin: 0 0 15px 0;"></div>
    </div>
  </div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
		<div class="row">
			<div class="col-md-12">
				<form method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
            				<table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr class="heading">
                      <td width="1" style="text-align: center;"><input type="checkbox" id="select_all" name="select_all" /></td>
                      <td class="center">S.No</td>
                      <td class="center"><?php echo $text_tran_no; ?></td>
                      <td class="center"><?php echo $text_tran_dt; ?></td>
                      <td class="center"> SKU </td>
                      <td class="center">Customer</td>
                      <td class="center">Status</td>
                      <td class="center">Payment</td> 
                      <td class="center"><?php echo $text_net_total; ?></td>
                      <td class="center printHide"><?php echo $column_action; ?></td>
             					</tr>
                        </thead>
                        <tbody>

                      <?php if ($purchases) { ?>
                      <?php $class = 'odd'; $i=1; ?>
            					<?php foreach ($purchases as $purchase) { ?>
            					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
            					<tr class="<?php echo $class; ?>">
                      <td style="text-align: center;">
                        <input type="checkbox" class="selected_orders" name="selected_orders[]" value="<?php echo $purchase['transaction_no']; ?>">
                      </td>
                      <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
				          	  <td class="center"><?php echo $purchase['transaction_no']; ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['sku']; ?></td>
                      <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="center"><?php if($purchase['status'] =='Canceled'){ echo $purchase['status']; } else{ echo showText($purchase['delivery_status']); } ?></td>
                      <td class="center"><?php if($purchase['status'] =='Canceled'){ echo $purchase['status']; } else{ echo $purchase['payment_status']; } ?></td> 
                      <td class="center"><?php echo $purchase['total']; ?></td>
                      
                      <td class="center printHide">
                       <?php if(!$purchase['isinvoice'] && $purchase['delivery_status'] =='Pending' && $purchase['status'] !='Canceled'){ ?>
                        <a href="<?php echo $purchase['modify_button'] ?>"><i class='fa fa-edit' data-toggle="tooltip" title="Modify"></i></a>
                      <?php } ?>
                      <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" data-toggle="tooltip" title="View"></i></a>

                      <a href="<?php echo $purchase['download_button']; ?>"><i class="fa fa-print" aria-hidden="true" data-toggle="tooltip" title="Print" style="font-size: 16px;"></i></a>
                    </td>
					</tr>
					<?php $i++; } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="11"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
            </table>
           </div>
           </form>
       <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    $("#filter_delivery_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Delivery Status', 
      height:'250px'
    });
    $("#filter_network").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Sales channel', 
      height:'250px' 
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '190px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    $.each( delivery, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    $.each( payment, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var network = <?php echo json_encode($filters['filter_network']); ?>;
    $.each( network, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });


  $('#table_sort').dataTable({
      "aaSorting" : [[ 1, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [0,3,4,8,9],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
  });
});
    
$("#select_all").click(function(){
    if(this.checked){
        $('.selected_orders').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.selected_orders').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
    }
});

$('.cancelOrder').click(function(){
    var invoice_no =  $(this).attr("data-invoice_no");
    var reason     = prompt('Reason for Cancelling Order : ');
    
    if(reason != null) {
    $("#cancelBtn_"+invoice_no).html('<div class="loader"></div>');
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales/cancelOrder&token=<?php echo $this->session->data["token"]; ?>',
          data: {invoice_no: invoice_no,reason:reason},
          success: function(result) {
           alert('Order Cancelled successfully!');
            $("#modifyBtn_"+invoice_no).hide();
            $("#cancelBtn_"+invoice_no).hide();
          }
      });
    }
});
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

$("#export_csv").on("click", function(e){
    e.preventDefault();
    if(selected_orders.length===0){
      alert('Please select atleast one order to export'); return false;
    }
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales/export_csv&token=<?php echo $this->session->data["token"]; ?>").submit();
});
$("#convert_invoice").on("click", function(e){
    e.preventDefault();
    <?php $this->session->data['filters'] = $filters; ?>
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales/bulk_convert_invoice&token=<?php echo $this->session->data["token"]; ?>").submit();
});

$("#export_pdf").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
      alert('Please select atleast one service to export'); return false;
    }
    $.ajax({
        url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/service_sales/export_pdf&token=<?php echo $this->session->data["token"]; ?>',
        type: 'POST',
        data : {selected_orders : selected_orders},
        success:function(data){
            var link  = document.createElement('a');
            link.href = 'download/service_note_list.pdf';
            link.download = "service_note_list.pdf";
            link.click();
            link.remove();
        }
    });
});

$("#bulk_cancel").on("click", function(e){
    e.preventDefault();
    var selected_orders = $("input[name='selected_orders[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_orders.length===0){
        alert('Please select atleast one invoice to cancel'); 
        return false;
    }
    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales/bulk_cancel&token=<?php echo $this->session->data["token"]; ?>',
        data: {selected_orders: selected_orders},
        success: function(result) {
            location.reload();  
        }
    });
});

</script>
<?php echo $footer; ?>