<!DOCTYPE html>

<html dir="ltr" lang="en">

<head>

<meta name="viewport" content="width=device-width; initial-scale=1.0">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Cloud POS -<?php echo $heading_title; ?></title>

<link rel="stylesheet" type="text/css" href="view/stylesheet/style.css"/>

<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css"/>

</head>

<body class="invoice-page" style="background:none;">

<div id="content">

  <div class="box">

    <div class="content">

      <div class="form-list-container">

        <div class="print-master-report">

          <table class="print_company_info">

            <tr>

              <td class="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>

            </tr>

            <tr>

              <td class="center" colspan="2"><?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>

                <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>

              </td>

            </tr>

            <tr>

              <td class="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>

            </tr>

            <tr>

              <td class="center" colspan="2"><?php echo $companyInfo['country']; ?></td>

            </tr>

            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>

            <tr <?php echo $contact; ?>>

              <td class="center"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span class="print_fax"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>

              <td class="right"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>

            </tr>

          </table>

          <div>

            <h1><?php echo $heading_title; ?></h1>

          </div>

          <table class="list">

            <thead>

              <tr class="heading">

                <td class="center">Transaction Date</td>

                <td class="center">Transaction No</td>

                <td class="center">Supplier</td>

                <td class="center">Total</td>

                <td class="center">GST</td>

                <td class="center">Net Total</td>

              </tr>

            </thead>

            <tbody>

              <?php if (!empty($purchases)) { ?>

              <?php foreach ($purchases as $purchase) { ?>

              <tr>

                <td class="center"><?php echo $purchase['transaction_date']; ?></td>

                <td class="center"><?php echo $purchase['transaction_no']; ?></td>

                <td class="left"><?php echo $purchase['vendor_name']; ?></td>

                <td class="left"><?php if(isset($purchase['sub_total'])){ echo $purchase['sub_total']; } else { echo '-';} ?></td>

                <td class="left"><?php if(isset($purchase['gst'])){ echo $purchase['gst']; } else { echo '-';} ?></td>

                <td class="left"><?php if(isset($purchase['net_total'])){ echo $purchase['net_total']; } else { echo '-';} ?></td>

              <?php } ?>

              <tr>

                <td class="center print_page_button" colspan="6"><a class="btn btn-zone" onClick="window.print();">Print</a> <a href="<?php echo $back; ?>" class="btn btn-zone">Back</a> </td>

              </tr>

              <?php } else { ?>

              <tr>

                <td class="center" colspan="6"><?php echo $text_no_results; ?></td>

              </tr>

              <?php } ?>

            </tbody>

          </table>

        </div>

      </div>

    </div>

  </div>

</div>

</body>

</html>

