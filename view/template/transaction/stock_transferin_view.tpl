<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Stock Transfer Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  <?php if($purchaseInfo['revoke_status']!='Yes' AND $purchaseInfo['accepted_status']!='Yes'){?>
                    <a onclick="acceptTransfer();">
                        <button name="purchase_button" type="button" class="btn btn-primary"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> Accept </button>
                    </a>
                  <?php }?>

                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</button></a>

                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $accept_button; ?>" method="post" enctype="multipart/form-data" id="acceptForm" name="acceptForm">
				<input type="hidden" name="purchase_id" id="purchase_id" value="<?php echo $purchaseInfo['transfer_no'] ?>">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Transaction No</td>
                          <td><?php echo $purchaseInfo['transfer_no'] ?></td>
                          <td>Transaction Date</td>
                          <td><?php echo date('d/m/Y',strtotime($purchaseInfo['createdon'])); ?></td>
                        </tr>
                        <tr>
                          <td>From Location</td>
                          <td><?php echo $purchaseInfo['transfer_from_location']; ?></td>
                          <td>To Location</td>
                          <td><?php echo $purchaseInfo['transfer_to_location']; ?></td>
                        </tr>
                        <tr>
                          <td>Accept Status</td>
                          <td><?php echo $purchaseInfo['accepted_status']; ?></td>
                          <td>Revoke Status</td>
                          <td><?php echo $purchaseInfo['revoke_status']; ?></td>
                        </tr>
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
                        <td class="center">Product Name</td>
                        <td class="center">Available Quantity</td>
                        <td class="center">Transfer Quantity</td>
                        <td class="center">Cost</td>
                        <td class="center">Transfer value</td>
                        
                      </tr>
                        </thead>
                        <tbody>
                          <?php if(count($productDetails) > 0){ $i = 1; ?>
                            <?php $class = 'odd'; $i=0;?>
                            <?php foreach ($productDetails as $products) {
                              $i++;
                              $sku = trim($products['sku']);
                             ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                            <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td><?php echo $products['sku']; ?>
                                   <input type="hidden" name="sku[]" value="<?php echo $sku;?>">
                              </td>
                              <td><?php echo $products['sku_description']; ?></td>
                              <td><?php echo $products['sku_qty']; ?></td>
                              
                              <td style="width: 10%;">
                                  <input type="text" name="qty[]" id="qty_<?php echo ltrim($sku,0);?>" value="<?php echo $products['sku_qty']; ?>" class="textbox" onkeyup="QtyValidation(this.value,<?php echo $products['sku_qty']; ?>,<?php echo ltrim($sku,0);?>,<?php echo $products['sku_cost']; ?>);">

                              </td>
                              <td><?php echo $products['sku_cost']; ?></td>
                              <td>
                              	<input type="text" name="total[]" id="total_<?php echo $sku;?>" value="<?php echo $products['transfer_value']; ?>" class="textbox" readonly>
                              </td>
                            </tr>
                            <?php $quantity+= $products['sku_qty']; 
                                  $priceTotal+= $products['sku_cost'];
                                  $netPriceTotal+= $products['transfer_value'];

                            ?>
                            <?php $i++;  } ?>
                            <?php /* ?><tr>
                              <td colspan="3" align="right">Total</td>
                              <td><?php echo $quantity; ?></td>
                              <td><?php echo number_format($priceTotal,4); ?></td>
                              <td><?php echo number_format($netPriceTotal,4); ?></td>
                            </tr>
                            <?php */ ?>
                          <?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  function acceptTransfer(){
  	document.acceptForm.submit()

  }		
  function QtyValidation(Nqty,Eqty,i,cost) {

          if(Nqty>Eqty){
              alert('Entered Qty Should not Exceed Available Qty');
              $('#qty_'+i).val(Eqty);
              var qty = Eqty;
          }else{
          	  var qty = Nqty;
          }

          var total = qty * cost;
          $('#total_'+i).val(parseFloat(total).toFixed(2));    
  }
</script>