<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Sales Invoice List</h3>
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
         <div class="page-toolbar">
           <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0 !important">
               <?php if($this->user->hasPermission('modify', $route)) { ?>
                  
                  <button id="export_csv" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> CSV</span></button>

                  <button id="export_pdf" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-download"></i><span> PDF</span></button>

                  <button id="xero_button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-upload"></i><span> XERO</span></button>

                  <button id="bulk_cancel" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i><span> Cancel</span></button>
                <?php } ?>
              </div>
            </div>
          </div>
      </div>

    <div class="innerpage-listcontent-blocks">
      <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 100px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="POST" name="list_filter" id="list_form_validation"> 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                        <td>
                          <input type="hidden" name="page"> 
                          <input type="text" placeholder='Customer Name' name="filter_customer_name" value="<?php echo $filters['filter_customer_name'] ; ?>" class="textbox" autocomplete="off" style="width: 121px;padding: 0px 5px;">
                        </td>
                        <td>
                          <input type="text" name="filter_contact_number" value="<?php echo $filters['filter_contact_number']; ?>" class="textbox" placeholder="Contact Number">
                        </td>
                        <td>
                          <input type="text" name="filter_transaction" value="<?php echo $filters['filter_transaction']; ?>" class="textbox" placeholder="Invoice No" autocomplete="off">
                        </td>
                        <td>
                          <input type="text" placeholder='Reference No' name="filter_order_number" value="<?php echo $filters['filter_order_number'] ; ?>" class="textbox" autocomplete="off">
                        </td>
                        <td>
                           <select name="filter_sales_person" id="filter_sales_person" class="salesdropdown">
                            <option value="">Sales Person</option>
                            <?php
                                foreach($sales_persons as $salesman){?>
                                  <option value="<?php echo $salesman['id'];?>" <?php echo $filters['filter_sales_person'] == $salesman['id'] ? 'selected' : '' ; ?>> <?php echo $salesman['name']; ?></option>

                            <?php  }  ?> 
                        </select>
                        </td>
                        <td>
                          <select name="filter_xero" id="filter_xero" class="salesdropdown">
                            <option value="0">Select Xero</option>
                              <option value="1"> Posted</option>
                              <option value="2"> Not Posted</option>
                          </select> 
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_network[]" id="filter_network">
                            <?php foreach ($networks as $value) { ?>
                                <option value="<?php echo $value['id']; ?>" <?php if(in_array($value['id'],$filters['filter_network'])){ echo "selected"; } ?>><?php echo $value['name']; ?></option>
                            <?php } ?>
                          </select>
                        </td>
                        <td>
                          <select class="textbox" name="filter_location" id="filter_location">
                            <option value="">Select Location</option>
                            <?php foreach($Tolocations as $values){ ?>
                                <option value="<?php echo $values['location_code'];?>" <?php if($values['location_code'] == $filters['filter_location']){ echo "selected"; } ?>><?php echo $values['location_name']; ?></option>
                            <?php  } ?>
                          </select>
                        </td>
                      </tr>
                      <tr class="filter">
                        <td>Delivery Date : <span style="float: right;"><input type="checkbox" name="filter_is_delivery" value="1" <?php if($filters['filter_is_delivery']){ echo "checked"; } ?> data-toggle="tooltip" title="Filter by delivery date"></span></td>
                        <td>
                          <input type="text" id='from_date' placeholder='From Date' name="filter_from_date" value="<?php echo $filters['filter_from_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_to_date" value="<?php echo $filters['filter_to_date'] ; ?>" class="textbox date" autocomplete="off">
                        </td>
                        <td>
                          <select class="textbox" multiple name="filter_delivery_status[]" id="filter_delivery_status">
                            <option value="Pending" <?php if(in_array('Pending',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Pending</option>
                            <option value="Scheduling" <?php if(in_array('Scheduling',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Scheduling</option>
                            <option value="Assigned" <?php if(in_array('Assigned',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Assigned</option>
                            <option value="Pending_Delivery" <?php if(in_array('Pending_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Pending Delivery</option>
                            <option value="On_Delivery" <?php if(in_array('On_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>On Delivery</option>
                            <option value="Partial_Delivered" <?php if(in_array('Partial_Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Partial Delivered</option>
                            <option value="Partial_Scheduling" <?php if(in_array('Partial_Scheduling',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Partial Scheduling</option>
                            <option value="Delivered" <?php if(in_array('Delivered',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Delivered</option>
                            <option value="Canceled" <?php if(in_array('Canceled',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Canceled</option>
                            <option value="Faild_Delivery" <?php if(in_array('Faild_Delivery',$filters['filter_delivery_status'])){ echo "selected"; } ?>>Faild Delivery</option>
                          </select>
                        </td>
                        <td>
                          <?php $filter_payment_status = $filters['filter_payment_status']; ?>
                          <select class="textbox" multiple name="filter_payment_status[]" id="filter_payment_status">
                            <option value="Pendings" <?php if(in_array('Pendings',$filter_payment_status)){ echo "selected"; } ?>>Pending</option>
                            <option value="Paid" <?php if(in_array('Paid',$filter_payment_status)){ echo "selected"; } ?>>Paid</option>
                            <option value="Partial" <?php if(in_array('Partial',$filter_payment_status)){ echo "selected"; } ?>>Partial</option>
                            <option value="Hold" <?php if(in_array('Hold',$filter_payment_status)){ echo "selected"; } ?>>Hold</option>
                            <option value="Canceled" <?php if(in_array('Canceled',$filter_payment_status)){ echo "selected"; } ?>>Canceled</option>
                          </select>
                        </td>
                        <td>
                          <input type="text" placeholder='Search Product' name="filter_product" value="<?php echo $filters['filter_product'] ; ?>" id="filter_product" class="textbox" autocomplete="off" onblur="clearSearch(this.value);">
                          <input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $filters['filter_product_id']; ?>" class="textbox">
                          <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                        <td align="right" >
                          <button class="btn btn-zone btn-primary"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</button>
                        </td>
                      </tr>
                  </tbody>
                </table>
              </div>                        
            </div>     
          </form>
          <div style="clear:both; margin: 0 0 15px 0;"></div>
      </div>
    </div>

  <div style="clear:both"></div>
      <?php if ($error_warning) { ?>
  <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert"></button>
  </div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 0px;margin-top: 12px;"><?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert"></button>
  </div>
  <?php } ?>
    
    <div class="row">
      <div class="col-md-12">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
          <input type="hidden" name="url" value="<?php echo $url; ?>">
                <div class="innerpage-listcontent-blocks">
                    <table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr class="heading">
                        <td width="1" style="text-align: center;"><input type="checkbox" id="select_all" name="select_all" /></td>
                        <td class="center">S.No</td>
                        <td class="center"><?php echo $text_tran_no; ?></td>
                        <td class="center"><?php echo $text_tran_dt; ?></td>
                        <td class="center">Order Id</td>
                        <td class="center">Customer</td>
                        <td class="center">Net Total</td>
                        <td class="center">Paid/Balance</td>
                        <td class="center">Status</td>
                        <td class="center">Payment</td>
                        <td class="center">Sales Channel</td>
                        <td class="center">Post</td>
                        <td class="center">DO</td>
                        <td class="center printHide"><?php echo $column_action; ?></td>
                      </tr>
                      </thead>
                      <tbody>
                    <?php if ($purchases) { ?>
                    <?php $class = 'odd'; $i=1; ?>
                    <?php foreach ($purchases as $purchase) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <tr class="<?php echo $class; ?>">
                      <td style="text-align: center;">
                          <input type="checkbox" class="selected_invoice" name="selected_invoice[]" value="<?php echo $purchase['order_id']; ?>">
                      </td>
                      <td class="center"><?php echo $purchase['pagecnt']+$i; ?></td>
                        <td class="center"><?php echo $purchase['transaction_no']; if($purchase['istagged']){  ?><a href="<?php echo $purchase['purchase_order_id']; ?>" target="_blank"><i class="fa fa-tag" aria-hidden="true" style="color: #f1540c;font-size: 12px;"></i></a><?php } ?></td>
                      <td class="center"><?php echo $purchase['transaction_date']; ?></td>
                      <td class="center"><?php echo $purchase['network_order_id']; ?></td>
                      <td class="center"><?php echo $purchase['vendor_name']; ?></td>
                      <td class="center"><?php echo $purchase['total']; ?></td>
                      <td class="center"><?php echo $purchase['paid_amount']; ?></td>
                      <td align="center" class="deliveryClass_<?php echo $purchase['invoice_no']; ?>"><?php echo showText($purchase['delivery_status']); ?></td>
                      <td align="center" class="paymentClass_<?php echo $purchase['invoice_no']; ?>"><?php echo $purchase['payment_status']; ?></td>
                      <td class="center"><?php if($purchase['network'] == 'Show Room'){ echo $purchase['location_name']; } else { echo $purchase['network']; } ?></td>

                      <?php if(HIDE_XERO==1){ ?>
                      <td style="text-align: center;" class="tdclass_<?php echo $purchase['order_id']; ?>">
                       <?php if(!empty($purchase['xero_sales_id'])){ ?>
                        <span class="text">Success</span>
                        <?php }else if($purchase['xero_post']){ ?>
                          <input type="hidden" id="<?php echo $purchase['order_id'] ?>" value="<?php echo $purchase['xero_sales_id'] ?>">
                          <input type="hidden" id="xero_cust_<?php echo $purchase['order_id'] ?>" value="<?php echo $purchase['xero_cust_id'] ?>">
                          
                          <input type="hidden" id="cust_id_<?php echo $purchase['order_id'] ?>" value="<?php echo $purchase['vendor_code'] ?>">

                          <span class="text-primary xero" data-sales_id="<?php echo $purchase['order_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:16px;" data-toggle="tooltip" title="Post"></i></span>
                       <?php  }else{  ?>
                        <i class="fa fa-ban" aria-hidden="true" style="font-size:16px;color:red;">
                        <?php } ?>
                      </td>
                      <?php } ?>
                      <td align="center">
                        <a href="<?php echo $purchase['multiple_do'] ?>"><i class="fa fa-truck" aria-hidden="true" style="font-size:16px;" data-toggle="tooltip" title="Multiple DO"></i></a>
                      </td>
                      <td class="center printHide">
                      <?php if($purchase['xero_sales_id']=='' && $purchase['payment_status'] == 'Pending' && $purchase['delivery_status'] != 'Delivered' && $purchase['delivery_status'] != 'Canceled'){ ?>
                      <?php } ?>
                      <a href="<?php echo $purchase['view_button'] ?>"><i class="fa fa-list-alt" aria-hidden="true" style="font-size:14px;" data-toggle="tooltip" title="View"></i></a>

                      <a href="<?php echo $purchase['download_button'] ?>"><i class='fa fa-download' style='font-size:14px' data-toggle="tooltip" title="Print"></i></a>

                      <?php if($purchase['payment_status'] != 'Paid' && $purchase['delivery_status'] != 'Canceled') { ?>
                        <span class="text-primary payment_link"  id="mailBtn_<?php echo $purchase['invoice_no']; ?>" data-purchase_id="<?php echo $purchase['invoice_no'] ?>"><i class='fa fa-envelope' style='font-size:14px;cursor: pointer;' data-toggle="tooltip" title="Payment Mail"></i></span>
                      <?php } ?>
                      <?php if($purchase['payment_status'] != 'Pending' ) { ?>
                        <a href="<?php echo $purchase['payment_view_button'] ?>"><i class="fa fa-usd" aria-hidden="true" style="font-size:14px;" data-toggle="tooltip" title="View"></i></a>
                      <?php } ?>
                      <?php if($purchase['delivery_status'] == 'Pending' && $purchase['payment_status'] != 'Paid' && $purchase['payment_status'] != 'Partial' && $purchase['xero_sales_id'] =='') { ?>
                        &nbsp;<a href="#" class="cancelOrder" id="cancelBtn_<?php echo $purchase['invoice_no']; ?>" data-invoice_no="<?php echo $purchase['invoice_no']; ?>"><i class="fa fa-trash " aria-hidden="true" style="font-size:15px;" data-toggle="tooltip" title="Cancel Invoice"></i></a>
                      <?php } ?>
                      
                    </td>
                    </tr>
                    <?php $i++; } ?>
                    <?php } else { ?>
                    <tr>
                        <td align="center" colspan="14"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </form>
          <div class="pagination"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="ajax-modal" class="modal fade modal-scroll in" tabindex="-1">
  <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
  </div>
  <div class="form-body" id="imagePopup">
  </div>
  <div class="modal-footer">
      <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
  </div>
</div>

<script type="text/javascript">
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
</script>
<?php echo $footer; ?>

<script type="text/javascript">
$(document).ready(function() {

    $('.xero').css("cursor",'pointer');
    $('#filter_xero').val('<?php echo $filters['filter_xero']; ?>');
    
    $("#filter_delivery_status").CreateMultiCheckBox({
      width: '180px', 
      defaultText : 'Select Delivery Status', 
      height:'250px' 
    });
    $("#filter_payment_status").CreateMultiCheckBox({ 
      width: '180px', 
      defaultText : 'Select Payment Status', 
      height:'250px' 
    });
    $("#filter_network").CreateMultiCheckBox({ 
      width: '180px', 
      defaultText : 'Sales channel', 
      height:'250px' 
    });

    var delivery = <?php echo json_encode($filters['filter_delivery_status']); ?>;
    $.each( delivery, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var payment = <?php echo json_encode($filters['filter_payment_status']); ?>;
    $.each( payment, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

    var network = <?php echo json_encode($filters['filter_network']); ?>;
    $.each( network, function( key, value ) {
      $("input[type=checkbox][class='mulinput'][value="+value+"]").prop("checked",true);
    });

  $('#table_sort').dataTable({
      "aaSorting" : [[ 1, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs"    : [{
            "targets"   : [0,3,4,6,7,10,11,12,13],
            "orderable" : false
          },{
          "targets"     : "_all",
          "defaultContent": "-"
        }]
  });
});

$("#select_all").click(function(){
    if(this.checked){
        $('.selected_invoice').each(function(){
            if(!(this.checked)){
                $(this).trigger('click');
            }
        });
    }else{
        $('.selected_invoice').each(function(){
            if(this.checked){
                $(this).trigger('click');
            }
        });
    }
});

function validJSON(text){
    if (typeof text!=="string"){
        return false;
    }
    try{
        JSON.parse(text);
        return true;
    }
    catch (error){
        return false;
    }
}

$(".xero").click(function(){
    var xero         = $(this);
    var order_id     = $(this).attr('data-sales_id');
    var xero_cust_id = $('#xero_cust_'+order_id).val();
    var cust_id      = $('#cust_id_'+order_id).val();

    $('.tdclass_'+order_id).html('<div class="loader"></div>');
    if(xero_cust_id =='' || xero_cust_id == null){
        
        var xeroUrl  = '<?php echo XERO_URL; ?>createContact.php?customer_id='+cust_id;
        $.get(xeroUrl, function (response) {
            if(validJSON(response)){
              var data = JSON.parse(response);
              if(data.status == 'success'){
                
                var xeroUrl = '<?php echo XERO_URL; ?>createSalesInvoice.php?order_id='+order_id;
                $.get(xeroUrl, function (response, status, error){
                      if(validJSON(response)){
                        var data  = JSON.parse(response);
                        if(data.status == 'success'){
                          xero.removeClass('xero');
                          $('.tdclass_'+order_id).html('<span class="text">Success</span>');
                        }else{
                          alert('Faild to send xero, Please try again.');
                        } 
                      }else{
                        alert('Faild to send xero, Please try again.');
                      }
                });
              }else{
                $('.tdclass_'+order_id).html('Faild');
              }
            }else{
                $('.tdclass_'+order_id).html('Faild');
           }
        });
      }else{
        var xeroUrl = '<?php echo XERO_URL; ?>createSalesInvoice.php?order_id='+order_id;
        $.get(xeroUrl, function (response, status, error){
              if(validJSON(response)){
                var data  = JSON.parse(response);
                if(data.status == 'success'){
                  xero.removeClass('xero');
                  $('.tdclass_'+order_id).html('<span class="text">Success</span>');
                }else{
                  alert('Faild to send xero, Please try again.');
                } 
              }else{
                alert('Faild to send xero, Please try again.');
              }
        });        
      }
});

function postCustomerToXero(cust_id,order_id){
    var resp = 0;
    var xeroUrl  = '<?php echo XERO_URL; ?>createContact.php?customer_id='+cust_id;
    $.get(xeroUrl, function (response) {
        if(validJSON(response)){
          var data = JSON.parse(response);
          if(data.status == 'success'){
             resp = 1;
          }else{
            $('.tdclass_'+order_id).html('Faild');
             resp = 0;
          }
        }else{
            $('.tdclass_'+order_id).html('Faild');
             resp = 0;
       }
    });
  return resp;
}

$('.payment_link').click(function(){
    var purchase_id =  $(this).attr("data-purchase_id");
    $.ajax({
        type: "GET",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/payment_link&token=<?php echo $this->session->data["token"]; ?>',
        data: {purchase_id: purchase_id},
        success: function(result) {
           alert('Email sent successfully!');
        }
    });
});
$('.imagePopupBtn').click(function(){
  var image = $(this).attr("data-image");
  var path  = "<?php echo HTTPS_SERVER; ?>doc/signature/"+image;
  $('#imagePopup').html('<img src="'+path+'" width="500" height="300">');
    $('#ajax-modal').modal('show');
});

$('.cancelOrder').click(function(){
    var invoice_no =  $(this).attr("data-invoice_no");
    var reason     = prompt('Reason for Cancelling Invoice : ');
    if(reason != null) {
      $("#cancelBtn_"+invoice_no).html('<div class="loader"></div>');
      $.ajax({
          type: "POST",
          url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/cancelOrder&token=<?php echo $this->session->data["token"]; ?>',
          data: {invoice_no: invoice_no,reason:reason},
          beforesend:function(){
          },
          success: function(result) {
             alert('Order Cancelled successfully!');
              $("#modifyBtn_"+invoice_no).hide();
              $("#mailBtn_"+invoice_no).hide();
              $("#cancelBtn_"+invoice_no).hide();
              $(".paymentClass_"+invoice_no).html('Canceled');
              $(".deliveryClass_"+invoice_no).html('Canceled');
          }
      });
    }
});

$("#xero_button").click(function() {
  $('input[type="checkbox"][name="selected_invoice\\[\\]"]:checked').map(function() {
      if( $("#"+this.value).val() =='' ){
        var order_id     = this.value;
        var xero_cust_id = $('#xero_cust_'+order_id).val();
        var cust_id      = $('#cust_id_'+order_id).val();
        $('.tdclass_'+this.value).html('<div class="loader"></div>');
        
        var cust_post= 1;
        if(xero_cust_id == ''){

            var xeroUrl  = '<?php echo XERO_URL; ?>createContact.php?customer_id='+cust_id;
                $.get(xeroUrl, function (response) {
                    if(validJSON(response)){
                      var data = JSON.parse(response);
                      if(data.status == 'success'){

                          var xeroUrl = '<?php echo XERO_URL; ?>createSalesInvoice.php?order_id='+order_id;
                          $.get(xeroUrl, function (response, status, error) {
                            if(validJSON(response)){
                              var data  = JSON.parse(response);
                              if(data.status == 'success'){
                                $('.tdclass_'+order_id).html('<span class="text">Success</span>');
                              }else{
                                $('.tdclass_'+order_id).html('Faild');
                              }
                            }else{
                                $('.tdclass_'+order_id).html('Faild');
                            }
                          });
                      }else{
                        $('.tdclass_'+order_id).html('Faild');

                      }
                    }else{
                        $('.tdclass_'+order_id).html('Faild');

                    }
                });
        }else{
          var xeroUrl      = '<?php echo XERO_URL; ?>createSalesInvoice.php?order_id='+this.value;
          $.get(xeroUrl, function (response, status, error) {
            if(validJSON(response)){
              var data  = JSON.parse(response);
              if(data.status == 'success'){
                $('.tdclass_'+order_id).html('<span class="text">Success</span>');
              }else{
                $('.tdclass_'+order_id).html('Faild');
              }
            }else{
                $('.tdclass_'+order_id).html('Faild');
            }
          });
        }
    }
  });
  // location.reload();
});

$("#export_csv").on("click", function(e){
    e.preventDefault();
    var selected_invoice = $("input[name='selected_invoice[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_invoice.length===0){
      alert('Please select atleast one invoice to export'); return false;
    }
    $('#form').attr('action', "<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/export_csv&token=<?php echo $this->session->data["token"]; ?>").submit();
});

$("#export_pdf").on("click", function(e){
    e.preventDefault();
    var selected_invoice = $("input[name='selected_invoice[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_invoice.length===0){
      alert('Please select atleast one invoice to export'); return false;
    }
    $.ajax({
        url : '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/export_pdf&token=<?php echo $this->session->data["token"]; ?>',
        type: 'POST',
        data : {selected_invoice : selected_invoice},
        success:function(m){
          var link  = document.createElement('a');
          link.href = 'download/sales_invoice_list.pdf';
          link.download = "sales_invoice_list.pdf";
          link.click();
          link.remove();
        }
    });
    // location.reload();
});

$("#bulk_cancel").on("click", function(e){
    e.preventDefault();
    var selected_invoice = $("input[name='selected_invoice[]']:checked").map(function(){return $(this).val();}).get();
    if(selected_invoice.length===0){
        alert('Please select atleast one invoice to cancel'); 
        return false;
    }
    $.ajax({
        type: "POST",
        url: '<?php echo HTTP_SERVER; ?>index.php?route=transaction/sales_invoice/bulk_cancel&token=<?php echo $this->session->data["token"]; ?>',
        data: {selected_invoice: selected_invoice},
        success: function(result) {
            location.reload();  
        }
    });
});

$('input[name=\'filter_product\']').autocomplete({
  'source': function(request, response) {

    $.ajax({
      url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_from=sales_filter&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
          $('#filter_product_id').val('');
          $("#suggesstion-box").show();
          $("#suggesstion-box").html(str);

        }else{
          $("#suggesstion-box").hide();
          $("#suggesstion-box").html('');
          $('#filter_product_id').val('');
        }
      }
    });
  }
});

function clearSearch(txt){
  if(txt ==''){
    $('#filter_product_id').val('');
  }
}

function getValue1(strhtml, product_id){
  $('#filter_product').val(strhtml.replace("^","'").replace('!!','"'));
  $('#filter_product_id').val(product_id);
  $('#suggesstion-box').hide();
}
</script>