<?php echo $header; ?>
<div class="clearfix"></div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">REPORTS <small></small></h3>
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#basicreport" data-toggle="tab">Basic Reporting</a>
				</li>
			</ul>
			
			<div class="tab-content">
				<div class="row repotsmain tab-pane active" id="basicreport">
					<div class="col-lg-6">
						<div class="portlet bordernone">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-reorder"></i>SKU Reports
								</div>
								<div class="tools"><a href="" class="collapse"></a></div> 
							</div>
							<div class="portlet-body bgnone">
								<?php  for($i=0;$i<count($reports['sku_report']);$i++){?>
									<div class="report-master">
										<?php if($reports['sku_report'][$i]['name'] =="Stock Valuation Report"){ ?>
											<a href="<?php echo $reports['sku_report'][$i]['link']; ?>">
												<?php echo $reports['sku_report'][$i]['name']; ?></a>
												<span> - <?php echo $reports['sku_report'][$i]['description']; ?>.</span>
										<!-- <span> 
										<label><input type="checkbox" name="negative" class="negative_check" id="negative" value="1" 
											<?php if ($qty_negative == "1"){?> checked <?php }?>>Neg Also</label>
										
										<label> <input type="checkbox" name="negative" class="negative_check" id="negative_only" value="2"
											<?php if ($qty_negative == "2"){?> checked <?php }?>>Neg Only
										</label>

										<label><input type="checkbox" name="negative" class="negative_check" id="zero_only" value="3"
											<?php if ($qty_negative == "3"){?> checked <?php }?>>Zero Only
										</label></span> -->
										<?php }else{ ?> 
											<a href="<?php echo $reports['sku_report'][$i]['link']; ?>">
												<?php echo $reports['sku_report'][$i]['name']; ?></a><span> - <?php echo $reports['sku_report'][$i]['description']; ?>.</span>
										<?php } ?>
										
									</div>
								<?php }?>
							</div>
						</div>
												
						<div class="portlet bordernone">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-reorder"></i>Purchase Reports
								</div>
								<div class="tools"><a href="" class="collapse"></a></div>
							</div>
							<div class="portlet-body bgnone">
								<?php for($i=0;$i<count($reports['purchase_report']);$i++){?>
									<div class="report-master">
											<a href="<?php echo $reports['purchase_report'][$i]['link']; ?>">
												<?php echo $reports['purchase_report'][$i]['name']; ?></a><span> - <?php echo $reports['purchase_report'][$i]['description']; ?>.</span>
									</div>
								<?php }?>
								<!-- <div class="report-master">
									<a href="<?php echo $po_request;?>">PO Request Report </a><span> - Your basic PO Request Report .</span>
								</div>

								<div class="report-master">
									<a href="<?php echo $purchase_summary;?>">Purchase Report</a><span> - Your basic purchase report.</span>
								</div>
								<div class="report-master">
									<a href="<?php echo $purchase_details;?>">Purchase Details Report</a><span> - Your basic purchase report.</span>
								</div>
								<br>
								<div class="report-master">
									<a href="<?php echo $purchase_return_summary;?>">Purchase Return Summary Report</a><span> - Your basic purchase return report.</span>
								</div>
								<div class="report-master">
									<a href="<?php echo $purchase_return_details;?>">Purchase Return Details Report</a><span> - Your basic purchase return details report.</span>
								</div> -->

							<!-- </div>
							<div class="portlet-body bgnone">
								<div class="report-master">	
									<a href="<?php echo $wastage_report;?>">Wastage Report</a><span> - Your basic wastage report.</span>
								</div>
								<div class="report-master">
									<a href="<?php echo $wastage_details_report;?>">Wastage details Report</a><span> - Your basic wastage details report.</span>
								</div> -->
							</div>

							

						</div>
						
						<div class="portlet bordernone">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-reorder"></i>Management Report
								</div>
								<div class="tools"><a href="" class="collapse"></a></div>
							</div>
							<div class="portlet-body bgnone">
								<?php for($i=0;$i<count($reports['management_report']);$i++){?>
									<div class="report-master">
											<a href="<?php echo $reports['management_report'][$i]['link']; ?>">
												<?php echo $reports['management_report'][$i]['name']; ?></a><span> - <?php echo $reports['management_report'][$i]['description']; ?>.</span>
									</div>
								<?php }?>								
							</div>

						</div>
					</div>
					<div class="col-lg-6">
						<div class="portlet bordernone">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-reorder"></i>Sales Report
								</div>
								<div class="tools"><a href="" class="collapse"></a></div>
							</div>
							<div class="portlet-body bgnone">
								<!--<div class="report-master">
									<a href="<?php echo $salestotalsummary; ?>">Total Sales Summary Report By Outlets</a><span> - Your basic total sales summary report by outlets.</span>
								</div>
								<div class="report-master">
									<a href="<?php echo $creditcollectionsummary; ?>">Total Credit collection Summary Report By Outlet</a><span> - Your basic total void sales details report.</span>
								</div>-->
								 <?php for($i=0;$i<count($reports['sales_report']);$i++){?>
									<div class="report-master">
											<a href="<?php echo $reports['sales_report'][$i]['link']; ?>">
												<?php echo $reports['sales_report'][$i]['name']; ?></a><span> - <?php echo $reports['sales_report'][$i]['description']; ?>.</span>
									</div>
								<?php }?> 
							</div>
						</div>
						<!-- <div class="portlet bordernone">
							<div class="portlet-title">
								<div class="caption">
									<i class="fa fa-reorder"></i>Credit Report
								</div>
								<div class="tools"><a href="" class="collapse"></a></div>
							</div>
							<div class="portlet-body bgnone">
								 <?php /*for($i=0;$i<count($reports['credit_report']);$i++){*/ ?>
									<div class="report-master">
											<a href="<?php /*echo $reports['credit_report'][$i]['link'];*/ ?>">
												<?php /*echo $reports['credit_report'][$i]['name'];*/ ?></a><span> - <?php /*echo $reports['credit_report'][$i]['description'];*/ ?>.</span>
									</div>
								<?php /*}*/ ?>
							</div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer; ?>

<script type="text/javascript">
	
$("#negative").live("click", function(){
	if($(this).is(":checked")) {
		$('#zero_only').attr('checked', false);
		$("#uniform-zero_only span").attr("class", "");
		$('#negative_only').attr('checked', false); 
		$("#uniform-negative_only span").attr("class", "");
		var updateValue = '1';
	} else {
		var updateValue = '0';
	}
	$.ajax({
        type: "POST",
        url: 'index.php?route=report/report/updateSettings&token=<?php echo $token; ?>',
        data: {value:updateValue},
        success: function(result) {
        	if(result == 1){
        		$(this).attr('checked',true);
        	} else {
        		$(this).attr('checked',false);
        	}
        }
    });
});

$("#negative_only").live("click", function(){
	if($(this).is(":checked")) {
		var updateValue = '2';
		$('#negative').attr('checked', false);
		$("#uniform-negative span").attr("class", "");
		$('#zero_only').attr('checked', false);
		$("#uniform-zero_only span").attr("class", "");

	} else {
		var updateValue = '0';
	}
	$.ajax({
        type: "POST",
        url: 'index.php?route=report/report/updateSettings&token=<?php echo $token; ?>',
        data: {value:updateValue},
        success: function(result) {
        	if(result == 1){
        		$(this).attr('checked',true);
        	} else {
        		$(this).attr('checked',false);
        	}
        }
    });
});

$("#zero_only").live("click", function(){
	if($(this).is(":checked")) {
		var updateValue = '3';
		$('#negative').attr('checked', false);
		$("#uniform-negative span").attr("class", "");

		$('#negative_only').attr('checked', false); 
		$("#uniform-negative_only span").attr("class", "");
	} else {
		var updateValue = '0';
	}
	$.ajax({
        type: "POST",
        url: 'index.php?route=report/report/updateSettings&token=<?php echo $token; ?>',
        data: {value:updateValue},
        success: function(result) {
        	if(result == 1){
        		$(this).attr('checked',true);
        	} else {
        		$(this).attr('checked',false);
        	}
        }
    });
});
</script>