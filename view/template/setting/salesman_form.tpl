<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Driver Form</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                   		  <thead>
	                        <tr><th colspan="2">Account</th></tr>
	                      </thead>
                        <tbody>                 
		                      <tr>
		                          <td width="35%"><label for="name">Name <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                            <?php if($error_name) { ?>
		                            <div class="input-icon right">
		                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
		                            <input type="text" name="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Name">
		                          </div>
		                          <?php } ?>

		                          <?php if(!$error_name) { ?>
		                          <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Name">
		                          <?php } ?>
		                          </td>
		                        </tr>
			                       <tr>
		                          <td width="35%"><label for="name">Phone No <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                              <?php if($error_phone) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Phone No"></i>
                                  <input type="text" name="phone" value="<?php echo $phone; ?>" class="textbox requiredborder" placeholder="Phone No">
                              </div>
                              <?php } elseif ($error_phone_duplication) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Phone No has already been added!"></i>
                                  <input type="text" name="phone" value="<?php echo $phone; ?>" class="textbox requiredborder" placeholder="Phone No">
                              </div>
                              <?php } else { ?>
		                            <input type="text" name="phone" value="<?php echo $phone; ?>" class="textbox" placeholder="Phone No">
                              <?php } ?>
		                          </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Email <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                              <?php if($error_email) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Email"></i>
                                  <input type="email" name="email" value="<?php echo $email; ?>" class="textbox requiredborder" placeholder="Email">
                                </div>
                              <?php } elseif ($error_email_duplication) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Email has already been added!"></i>
                                  <input type="email" name="email" value="<?php echo $email; ?>" class="textbox requiredborder" placeholder="Email">
                                </div>
                              <?php } else { ?>
		                            <input type="email" name="email" value="<?php echo $email; ?>" class="textbox" placeholder="Email">
                              <?php } ?>
		                          </td>
		                        </tr>
                            <tr>
                              <td width="35%"><label for="name">Password <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                              <?php if($error_password) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Password"></i>
                                  <input type="password" name="password" value="<?php echo $password; ?>" class="textbox requiredborder" placeholder="Password">
                                </div>
                              <?php } else { ?>
                                <input type="password" name="password" value="<?php echo $password; ?>" class="textbox" placeholder="Password">
                              <?php } ?>
                              </td>
                            </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Route <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                              <?php if($error_route_id) { ?>

                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Select route"></i>
    		                            <select name="route_id" id="route_id" class="selectpicker selectdropdown" style="min-height: 35px;width: 100%;margin: 0 !important;">
                                      <option value = "">Select route</option>  
                                      <?php foreach ($route_list as $value) { ?>
                                        <option value ='<?php echo $value['id']; ?>' ><?php echo $value['name']; ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                              <?php } ?>

                              <?php if(!$error_route_id) { ?>
                                <select name="route_id" id="route_id" class="selectpicker selectdropdown" style="min-height: 35px;width: 100%;margin: 0 !important;">
                                  <option value = "">Select route</option>  
                                  <?php foreach ($route_list as $value) { ?>
                                    <option value ='<?php echo $value['id']; ?>' ><?php echo $value['name']; ?></option>
                                  <?php } ?>
                                </select>
                              <?php } ?>
		                          </td>
		                        </tr> 
                              <tr>
                                <td width="35%"><?php echo 'Status'; ?></td>
                                <td width="65%" class="order-nopadding">
                                    <select name="status" id="status" class="selectpicker selectdropdown"style="min-height: 35px;width: 100%;margin: 0 !important;">                                   
                                        <option value="1" <?php if ($status == 1) { ?> selected="selected" <?php } ?>><?php echo 'Enabled'; ?></option>
                                        <option value="0" <?php if ($status == 0) { ?> selected="selected" <?php } ?>><?php echo 'Disabled'; ?></option>
                                    </select>
                                </td>
                              </tr>
                        </tbody>
                      </table>
                   	</div>
                   	<div class="col-md-6">
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Address</th></tr>
                        	</thead>
                        	<tbody>
          							<tr>
          								<td width="35%"><label for="address">Address</label></td>
          								<td width="65%" class="order-nopadding">
          									<input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Address1">
          									<input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox" placeholder="Address 2">
          								</td>
          							</tr>								            
                            <tr>
                              <td width="35%"><label for="city">City</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="textbox" placeholder="City">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="address2">State</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="textbox" placeholder="State">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="zipcode">Zip Code <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                              <?php if($error_zipcode) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter zipcode"></i>
                                  <input type="text" name="zipcode" value="<?php echo $zipcode; ?>" class="textbox requiredborder" placeholder="Zip code">
                                </div>
                              <?php } else { ?>
                                <input type="text" name="zipcode" value="<?php echo $zipcode; ?>" class="textbox" placeholder="Zip code">
                              <?php } ?>
                              </td>
                            </tr>
                        </tbody>
                   	  </table>
                   
                   	 </div>
                   </div>
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
  $('#route_id').val(<?php echo $route_id; ?>);
  $('#status').val(<?php echo $status; ?>);
</script>
<?php echo $footer; ?>
<script type="text/javascript">

</script>