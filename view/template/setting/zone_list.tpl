<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                            <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                            <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div>            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">
                    <div class="tabbable tabbable-custom boxless">
                    <ul class="nav nav-tabs">
                        <li>
                            <a href="<?php echo $country; ?>" >Country</a>
                        </li>
                        <li class="active">
                            <a href="#"  data-toggle="tab">Zones</a>
                        </li>
                        <li>
                            <a href="<?php echo $geo_zone; ?>">Geo Zones</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active" > 
                    <div class="innerpage-listcontent-blocks">               	
                	<table class="table orderlist statusstock">
                    	<thead>
                        	<tr class="heading">
                            	<td width="1"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                                <td>
                                	<?php if ($sort == 'c.name') { ?>
                                        <a href="<?php echo $sort_country; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_country; ?>"><?php echo $column_country; ?></a>
                                    <?php } ?>
                                </td>
                                <td>
                                	<?php if ($sort == 'z.name') { ?>
                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                    <?php } ?>
                                </td>
                                <td>
                                	<?php if ($sort == 'z.code') { ?>
                                        <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="right"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php if ($zones) { ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($zones as $zone) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <tr class="<?php echo $class; ?>">
                                    <td>
                                    	<?php if ($zone['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $zone['zone_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $zone['zone_id']; ?>" />
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $zone['country']; ?></td>
                                    <td><?php echo $zone['name']; ?></td>
                                    <td><?php echo $zone['code']; ?></td>
                                    <td>
                                    	<?php foreach ($zone['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php } else { ?>
                            	<tr>
                                    <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
            </div>
        </div>
                </div>
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>