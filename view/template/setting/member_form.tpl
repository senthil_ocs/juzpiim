<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Member Master</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                   		  <thead>
	                        <tr><th colspan="2">Biographical</th></tr>
	                      </thead>
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Member Code</label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                  <input type="text" name="member_code" class="textbox" value="<?php echo str_pad($member_code, 5, '0', STR_PAD_LEFT);?>" readonly>
                                </td>
                            </tr>                             
		                      <tr>
		                          <td width="35%"><label for="name">Name <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                            <?php if($error_name) { ?>
		                            <div class="input-icon right">
		                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
		                            <input type="text" name="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Name">
		                          </div>
		                          <?php } ?>

		                          <?php if(!$error_name) { ?>
		                          <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Name">
		                          <?php } ?>
		                          </td>
		                        </tr>
			                       <tr>
		                          <td width="35%"><label for="name">Title</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="member_title" value="<?php echo $member_title; ?>" class="textbox" placeholder="Title">
		                          </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Company</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="company" value="<?php echo $company; ?>" class="textbox" placeholder="Company">
		                          </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Date of Birth</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="birth_date" value="<?php echo $birth_date; ?>" class="textbox date" placeholder="Date of Birth" autocomplete="off">
		                          </td>
		                        </tr> 
                              <tr>
                                <td width="35%"><?php echo 'Status'; ?></td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="status" class="selectpicker selectdropdown">                                   
                                        <option value="0" <?php if ($status == 0) { ?> selected="selected" <?php } ?>><?php echo 'Enabled'; ?></option>
                                        <option value="1" <?php if ($status == 1) { ?> selected="selected" <?php } ?>><?php echo 'Disabled'; ?></option>
                                    </select>
                                </td>
                              </tr>
                        </tbody>
                      </table>
                      <table class="table orderlist">
                      	<thead>
                        	<tr>
                            	<th colspan="2">Phones</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
              							<td width="35%">
              								<label for="home_num">Home</label>
              							</td>
              							<td width="65%" class="order-nopadding">
              								<input type="text" name="home" value="<?php echo $home; ?>" class="textbox" placeholder="Home" />
              							</td>
            						  </tr>
                          <tr>
                            <td width="35%">
                              <label for="work">Work</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="work" value="<?php echo $work; ?>" class="textbox" placeholder="Work" />
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="mobile">Mobile</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="mobile" value="<?php echo $mobile; ?>" class="textbox" placeholder="Mobile" />
                            </td>
                          </tr>
                          
                        </tbody>
                      </table>
                   	</div>
                   	<div class="col-md-6">
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Address</th></tr>
                        	</thead>
                        	<tbody>
							<tr>
								<td width="35%"><label for="address">Address</label></td>
								<td width="65%" class="order-nopadding">
									<input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Address1">
									<input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox" placeholder="Address 2">
								</td>
							</tr>								            
                            <tr>
                              <td width="35%"><label for="city">City</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="textbox" placeholder="City">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="address2">State</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="textbox" placeholder="State">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="zipcode">Zip Code</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="zipcode" id="zipcode" value="<?php echo $zipcode; ?>" class="textbox" placeholder="Zip Code">
                              </td>
                            </tr>
                        </tbody>
                   	  </table>
                   
                   	 </div>
                   </div>
                    <div class="col-md-12">
                      <table class="table orderlist">
                        <thead>
                            <tr><th colspan="2">Notes</th></tr>
                        </thead>
                        <tbody>
                        <tr>
                          </td>
                          <td width="65%" class="order-nopadding">
                            <textarea style="border-right: 1px solid #ddd;" name="notes" id="notes" class="textbox" rows="5"><?php echo $notes; ?></textarea>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
            	<!-- 24-Feb-2016 Ends /!-->
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
function getTypeList(v)
{
  $.ajax({
      url:"index.php?route=setting/customers/getTypeDetails&token=<?php echo $token; ?>",
      type:"POST",
      data:{customer_type:v},     
      success:function(out){
          out = JSON.parse(out);          
          html = '<option value="">- Select Discount -</option>';
            if(out != '') {                    
                html += '<option value="' + out['discount'] + '"';
                  if (out['discount'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['discount_name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'discount\']').html(html);

          html = '<option value="">- Select Tax -</option>';
            if(out != '') {                    
                html += '<option value="' + out['taxcategory'] + '"';
                  if (out['taxcategory'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'salestax\']').html(html);      
      }
  });
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
  $('.date').datepicker({
    dateFormat: 'dd/mm/yy'
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:'+(new Date).getFullYear() 
  });
</script>