<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Touch Menu Item List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		                            <select name="filter_menu_code" id="filter_menu_code" class="textbox">
                                      <option value="">Select Touch Menu </option>
                                      <?php if(count($touchMenuCode) > 0){ ?>
                                        <?php foreach($touchMenuCode as $menuCode){ ?>
                                        <option value="<?php echo $menuCode['touch_menu_code']; ?>" <?php if( $menuCode['touch_menu_code'] == $touch_menu_code){ ?> selected="selected" <?php } ?>><?php echo $menuCode['touch_menu_description']; ?></option>
                                        <?php } ?>
                                      <?php } ?>
                                  </select>
                                </td>

		                      	<td>
		              				<input type="text" placeholder='Item Description' name="filter_description" value="<?php echo $filter_description; ?>" class="textbox" />
		                        </td> 

		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>

		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  

		            
		            </div> 
                    <div style="clear:both; margin:0 0 15px 0;"></div>
                    </div>

            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
			<div class="row">
				<div class="col-md-12">
				<?php if ($error_warning) { ?>
				<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
	             </div>
				<?php } ?>
				<?php if ($success) { ?>			
					<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
	                </div>
				<?php } ?>
			</div>		
			</div>		 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                <input type="hidden" name="oldsortCode" id="oldsortCode" value="">
				<input type="hidden" name="skuaction" value="update">
					 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left"><?php if ($sort == 'touch_menu_code') { ?>
									<a href="<?php echo $sort_touch_menu_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Touch Menu Desc'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_touch_menu_code; ?>"><?php echo 'Touch Menu Desc'; ?></a>
									<?php } ?>
								</td>
                                <td class="left"><?php echo 'Sku Code'; ?></td>
                                 <td class="left">Description</td>
                                <td class="left"><?php echo 'Status'; ?></td>
								<td class="left"><?php if ($sort == 'sort_sort_code') { ?>
									<a href="<?php echo $sort_sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Sort Code'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_sort_code; ?>"><?php echo 'Sort Code'; ?></a>
									<?php } ?>
									</td>
                                <td class="left"><?php echo 'Created By'; ?></td>
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="left">Action</td>
								
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($touchmenuitem) {

                             ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($touchmenuitem as $menu) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					 <td style="text-align: center;" width="2%">
										<?php if ($menu['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['id']; ?>" />
										<?php } ?>
									</td>
									<td class="left" width="10%"><?php echo $menu['touch_menu_description']; ?></td>
									<td class="left" width="10%"><?php echo $menu['sku_code']; ?></td>
									<td class="left" width="23%"><?php echo $menu['description']; ?></td>
                                    <td class="left" width="10%"><?php echo $menu['status']; ?></td>
                                    <td class="left" width="20%"><div class="input-group col-xs-5">
												    <input type="text" class="form-control" value="<?php echo $menu['sort_code']; ?>" name="sort_code" id="sort_code">
												   
												    <span class="input-group-btn" style="width:0;">
												        <button class="btn btn-default" type="button" 
												        onclick="changeSortOrder(<?php echo $menu['sort_code']; ?>,<?php echo $menu['id']; ?>);">Update</button>
												    </span>
												</div></td>

									<td class="left" width="7%"><?php echo $menu['createdby']; ?></td>
									<td class="left" width="10%"><?php echo date('d/m/Y g:i A',strtotime($menu['createdon'])); ?></td>
									<td class="right" width="8%">
										<?php foreach ($menu['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filterReport() {
	url = 'index.php?route=setting/touchmenuitem&token=<?php echo $token; ?>';
	var filter_menu_code = $('select[name=\'filter_menu_code\']').attr('value');
	if (filter_menu_code) {
		url += '&filter_menu_code=' + encodeURIComponent(filter_menu_code);
	}
	var filter_description = $('input[name=\'filter_description\']').attr('value');
	if (filter_description) {
		url += '&filter_description=' + encodeURIComponent(filter_description);
	}
	location = url;
}

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function changeSortOrder(oldsortCode,id){
	$('#oldsortCode').val(oldsortCode);
	var newsortCode = $('#sort_code').val();
    if(newsortCode==oldsortCode){
    	alert("Change your sort code order");
    	return false;
    }else{
        var ajaxData = "itemId=" + id + "&newvalue=" + newsortCode;
		$.ajax({
        type: "POST",
        url: 'index.php?route=setting/touchmenuitem/updateSortOrder&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
               alert("Sort order updated Successfully");
            }
        }
    });
    	
    }
}
</script>
<?php echo $footer; ?>