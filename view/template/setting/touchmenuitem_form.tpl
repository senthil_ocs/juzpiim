<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Touch Menu Item Master</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="button" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" return onclick="formsubmit();"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-12">
                   		<table class="table orderlist">
                        <tbody>                          
                            <tr>
                              <td width="35%"><label for="name">Touch Menu Code <span class="required">*</span></label></td>
                              <td class="order-nopadding" width="80%">
                                <?php if(!isset($this->request->get['menu_code'])){ ?>
                                  <?php if($error_touch_menu_code) { ?>
                                  <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Touch Menu Code"></i>
                                  <select name="touch_menu_code" id="touch_menu_code" class="textbox requiredborder">
                                      <option value="">-Select Touch Menu Code-</option>
                                      <?php if(count($touchMenuCode) > 0){ ?>
                                        <?php foreach($touchMenuCode as $menuCode){ ?>
                                        <option value="<?php echo $menuCode['touch_menu_code']; ?>" <?php if( $menuCode['touch_menu_code'] == $touch_menu_code){ ?> selected="selected" <?php } ?>><?php echo $menuCode['touch_menu_code']; ?></option>
                                        <?php } ?>
                                      <?php } ?>
                                  </select>
                                </div>
                                <?php } ?>
                               
                                <?php if(!$error_touch_menu_code) { ?>
                                  <select name="touch_menu_code" id="touch_menu_code" class="textbox">
                                      <option value="">-Select Touch Menu Code-</option>
                                      <?php if(count($touchMenuCode) > 0){ ?>
                                        <?php foreach($touchMenuCode as $menuCode){ ?>
                                        <option value="<?php echo $menuCode['touch_menu_code']; ?>" <?php if( $menuCode['touch_menu_code'] == $touch_menu_code){ ?> selected="selected" <?php } ?>><?php echo $menuCode['touch_menu_description']; ?></option>
                                        <?php } ?>
                                      <?php } ?>
                                  </select>
                                <?php } ?>
                              <?php } else { ?>
                              <input type="text" class="textbox" name="touch_menu_code" id="touch_menu_code" readonly value="<?php echo $touch_menu_code; ?>">
                              <?php } ?>
                              </td>
                            </tr>

                            <tr>
                              <td width="35%"><label for="name">Product SKU <span class="required">*</span></label>
                               <p style="font-size: 12px;">Enter Product name / sku / barcode</p>  
                              <input type="hidden" name="sku_code" id="sku_code" value="<?php echo $sku_code; ?>">
                              </td>
                            
                              <td class="order-nopadding" width="80%">
                              <?php if($error_touch_menu_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-original-title=" Enter Product Name"></i>
                                <input type="text" name="filter_name" id="filter_name" value="<?php echo $sku_code; ?>" class="textbox requiredborder" placeholder="Product Name">
                                <div id="suggesstion-box" class="auto-compltee"></div>
                              <?php }?>

                               <?php if(!$error_touch_menu_code) { ?>
                                <input type="text" name="filter_name" id="filter_name" value="<?php echo $sku_code; ?>" <?php if($editForm){?> readonly <?php }?>
                                  class="textbox" placeholder="Product Name" autocomplete="off">
                                  <div id="suggesstion-box" class="auto-compltee"></div>
                               <?php }?>
                               </td>
                            </tr>

                            <tr>
                              <td width="35%"><label for="name">Product Image</label></td>
                              <td width="65%" class="order-nopadding">
                              <input type="file" name="product_image" id="product_image" value="">
                              </td>
                            </tr>

                            <tr>
                              <td width="35%"><label for="name">Description</label></td>
                              <td width="65%" class="order-nopadding">
                              <textarea name="touch_menu_description" id="touch_menu_description" class="textbox" placeholder="Touch Menu Description"><?php echo $touch_menu_description; ?></textarea>
                              </td>
                            </tr>

                            <?php $statusAry = array("0"=>"Disabled","1"=>"Enabled"); ?>
                            <tr>
                              <td width="20%"><label for="status">Status</label></td>
                               <td width="80%">
                                <select name="touch_menu_status" class="input-text">
                                    <?php foreach($statusAry as $key=>$value): ?>
                                      <option value="<?php echo $key; ?>" <?php if($key==$status):?> selected="selected" <?php endif; ?>>
                                        <?php echo $value; ?>
                                      </option>
                                    <?php endforeach; ?>
                                </select>
                              </td> 
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Sort Code</label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_sort_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Sort Code"></i>
                                <input type="text" name="sort_code" id="sort_code" value="<?php echo $sort_code; ?>" class="textbox requiredborder" placeholder="Sort Code">
                              </div>
                              <?php } ?>
                              <?php if(!$error_sort_code) { ?>
                              <input type="text" name="sort_code" id="sort_code" value="<?php echo $sort_code; ?>" class="textbox" placeholder="Sort Code">
                              <?php } ?>
                              </td>
                            </tr>
                        </tbody>
                    </table>
                   	</div>
                   	
                   </div>
           		</div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
function formsubmit() {
	var sku = $('#sku_code').val();
	if(sku!=''){
		$("#formID").submit();
	}else{
		alert("Add Products");
		return false;
	}
}
$("#sku_code").change(function(){
  var menuCode = $('#touch_menu_code').val();
  var skucode = $(this).val();
  jQuery.ajax({
      type:'POST',
      url: 'index.php?route=setting/touchmenuitem/getSkuDetails&token=<?php echo $token; ?>',
      data: {menucode:menuCode,sku:skucode},
      success: function(result) {
        var response = result.split('||');
        if(response[0] != ''){
          $("#touch_menu_description").val(response[0]);
        }
        if(response[1] != ''){
          $("#sort_code").val(response[1]);
        } else {
          $("#sort_code").val('1');
        }
      }
  });
});
$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }  
});

function getValue(pname,sku){
  $('#filter_name').val(sku)
  $('#sku_code').val(sku)
  $('#touch_menu_description').val(pname)
  $('#suggesstion-box').hide();
  var menuCode = $('#touch_menu_code').val();
  var skucode = sku;
  jQuery.ajax({
      type:'POST',
      url: 'index.php?route=setting/touchmenuitem/getSkuDetails&token=<?php echo $token; ?>',
      data: {menucode:menuCode,sku:skucode},
      success: function(result) {
        var response = result.split('||');
        if(response[0] != ''){
          $("#touch_menu_description").val(response[0]);
        }
        if(response[1] != ''){
          $("#sort_code").val(response[1]);
        } else {
          $("#sort_code").val('1');
        }
      }
  });

}
</script>