<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Member List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
		                        </td> 
		                        <td>
		              				<input type="text" placeholder='company' name="filter_company" value="<?php echo $_POST['filter_company'] ; ?>" class="textbox" style="width: 250px;"  />
		                   		</td>
		                   		<td>
		              				<input type="text" placeholder='Phone' name="filter_phone" value="<?php echo $_POST['filter_phone'] ; ?>" class="textbox"/>
		                   		</td>                   
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            <!-- <div class="tools" style="float:left;padding: 0 0 0 16px;">
		               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
		            </div> -->
		            </div> 
	              <?php if (($_POST['filter_date_from'] !="") || ($_POST['filter_date_to'] != "") || ($_POST['filter_Type'] != "")){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
           <!-- <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: -38px !important; margin-left:15px; width: 15%;">
                <tbody>
                  <tr class="filter">
                   	<td>
                   		<input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_POST['filter_date_from'] ; ?>" class="textbox date">
            		</td>
              		<td>
                   		<input type="text" id='' placeholder='To Date' name="filter_date_to" value="<?php echo $_POST['filter_date_to'] ; ?>" class="textbox date">
            		</td> 
            		
                  </tr>
                </tbody>
              </table>
            </div> -->
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
            
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left">Member Code</td>
								<td class="left"><?php if ($sort == 'name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo 'Name'; ?></a>
									<?php } ?>
								</td>
								<td class="left"><?php echo 'Phone No'; ?></td>
								<td class="left"><?php if ($sort == 'company') { ?>
									<a href="<?php echo $sort_company; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Email'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_company; ?>"><?php echo 'Company'; ?></a>
									<?php } ?>
									</td>
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($members) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($members as $value) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					 <td style="text-align: center;">
										<?php if ($customer['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $value['member_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $value['member_id']; ?>" />
										<?php } ?>
									</td>
									<td class="left"><?php echo $value['member_code']; ?></td>
									<td class="left"><?php echo $value['name']; ?></td>
									<td class="left"><?php echo $value['mobile']; ?></td>
									<td class="left"><?php echo $value['company']; ?></td>
									<td class="left"><?php echo $value['createdon']; ?></td>
									<td class="right">
										<?php foreach ($value['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport() {
	document.report_filter.submit();
}
</script>
<?php echo $footer; ?>