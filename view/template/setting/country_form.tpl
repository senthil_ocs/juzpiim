<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                             <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                       	</div>    
                    </div>
                </div>
			</div>            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">                	
                	<table class="table orderlist">
                    	<tbody>
                        	<tr>
                            	<td width="35%">
                                <?php echo $entry_name; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
                                    	<input type="text" placeholder="Enter your name" name="name" id="name" class="textbox requiredborder" value="<?php echo $name; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_name) { ?>
                               			<input type="text" placeholder="Enter your name" name="name" id="name" class="textbox" value="<?php echo $name; ?>" />
                               		<?php } ?>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_iso_code_2; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" placeholder="Enter your iso code 2" name="iso_code_2" id="iso_code_2" class="textbox" value="<?php echo $iso_code_2; ?>" <?php if($iso_code_2 != '') { ?> readonly <?php } ?>>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_iso_code_3; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" placeholder="Enter your iso code 3" name="iso_code_3" id="iso_code_3" class="textbox" value="<?php echo $iso_code_3; ?>" <?php if($iso_code_3 != '') { ?> readonly <?php } ?>>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_status; ?>
                                </td>
                                <td width="65%">
                                    <select name="status" class="selectpicker">
									<?php if ($status) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>