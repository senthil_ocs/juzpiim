<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php if($breadcrumbs[0]['text']) { ?>
                    	<li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo $breadcrumbs[0]['href']; ?>"><?php echo $breadcrumbs[0]['text']; ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    <?php } ?>
                    <?php if($breadcrumbs[1]['text']) { ?>
                    	<li>
                            <a href="<?php echo $breadcrumbs[1]['href']; ?>"><?php echo $breadcrumbs[1]['text']; ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    <?php } ?>
                    <?php if($breadcrumbs[2]['text']) { ?>
                    	<li>
                            <a href="<?php echo $breadcrumbs[2]['href']; ?>"><?php echo $breadcrumbs[2]['text']; ?></a>
                        </li>
                    <?php } ?>
                     <?php if($breadcrumbs[3]['text']) { ?>
                    	<li>
                            <?php echo $breadcrumbs[3]['text']; ?>
                        </li>
                    <?php } ?>
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                       	</div>    
                    </div>
                </div>
			</div>            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">
                	<div class="innerpage-listcontent-blocks">
                	<table class="table orderlist">
                    	<tbody>
                        	<tr>
                            	<td width="35%">
                                <?php echo $entry_name; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding geozone-bor-bot">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
                                    	<input type="text" placeholder="Enter your name" name="name" id="name" class="textbox requiredborder" value="<?php echo $name; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_name) { ?>
                               			<input type="text" placeholder="Enter your name" name="name" id="name" class="textbox" value="<?php echo $name; ?>" />
                               		<?php } ?>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_description; ?>
                                </td>
                                <td width="65%" class="order-nopadding geozone-bor-bot">
                                	<?php if($error_description) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
                                    	<input type="text" placeholder="Enter your name" name="description" id="description" class="textbox requiredborder" value="<?php echo $description; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_description) { ?>
                               			<input type="text" placeholder="Enter your description" name="description" id="description" class="textbox" value="<?php echo $description; ?>" />
                               		<?php } ?>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table id="zone-to-geo-zone" class="table table-striped table-bordered table-hover dataTable no-footer nemtbl"   role="grid" >
                        <thead>
                            <tr class="heading">               
                                <th><?php echo $entry_country; ?></th>
                                <th><?php echo $entry_zone; ?></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <?php $zone_to_geo_zone_row = 0; ?>
                    <?php foreach ($zone_to_geo_zones as $zone_to_geo_zone) { ?>
                        <tbody id="zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>">
                            <tr>
                                <td class="left"><select name="zone_to_geo_zone[<?php echo $zone_to_geo_zone_row; ?>][country_id]" id="country<?php echo $zone_to_geo_zone_row; ?>" onchange="$('#zone<?php echo $zone_to_geo_zone_row; ?>').load('index.php?route=setting/geo_zone/zone&token=<?php echo $token; ?>&country_id=' + this.value + '&zone_id=0');" class="input-text">
                                <?php foreach ($countries as $country) { ?>
                                <?php  if ($country['country_id'] == $zone_to_geo_zone['country_id']) { ?>
                                <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                </select></td>
                                <td class="left"><select name="zone_to_geo_zone[<?php echo $zone_to_geo_zone_row; ?>][zone_id]" id="zone<?php echo $zone_to_geo_zone_row; ?>" class="input-text">
                                    <!-- NEW CODE -->
                                    <?php foreach ($zones as $zone) { ?>
                                <?php  if ($zone['zone_id'] == $zone_to_geo_zone['zone_id']) { ?>
                                <option value="<?php echo $zone['zone_id']; ?>" selected="selected"><?php echo $zone['name']; ?></option>
                                <?php } else { ?>
                                <option value="<?php echo $zone['zone_id']; ?>"><?php echo $zone['name']; ?></option>
                                <?php } ?>
                                <?php } ?>
                                <!-- END NEW CODE -->
                                </select></td>
                                <td class="left"><a onclick="$('#zone-to-geo-zone-row<?php echo $zone_to_geo_zone_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
                            </tr>
                        </tbody>
                    <?php $zone_to_geo_zone_row++; ?>
                    <?php } ?>
                    <tfoot>
                        <tr>
                            <td colspan="2"></td>
                            <td class="left"><a onclick="addGeoZone();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_geo_zone; ?></a></td>
                        </tr>
                    </tfoot>
                    </table>
                    </div>
                </div>
                
            </div>
        	
        
            <!-- -->
		</div>
	</div>
    </form>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<script type="text/javascript"><!--
$('#zone-id').load('index.php?route=setting/geo_zone/zone&token=<?php echo $token; ?>&country_id=' + $('#country-id').attr('value') + '&zone_id=0');
//--></script>
<?php $zone_to_geo_zone_row = 0; ?>
<?php foreach ($zone_to_geo_zones as $zone_to_geo_zone) { ?>
<script type="text/javascript"><!--
$('#zone<?php echo $zone_to_geo_zone_row; ?>').load('index.php?route=setting/geo_zone/zone&token=<?php echo $token; ?>&country_id=<?php echo $zone_to_geo_zone['country_id']; ?>&zone_id=<?php echo $zone_to_geo_zone['zone_id']; ?>');
//--></script>
<?php $zone_to_geo_zone_row++; ?>
<?php } ?>
<script type="text/javascript"><!--
var zone_to_geo_zone_row = <?php echo $zone_to_geo_zone_row; ?>;

function addGeoZone() {
    html  = '<tbody id="zone-to-geo-zone-row' + zone_to_geo_zone_row + '">';
    html += '<tr>';
    html += '<td class="left"><select class="input-text" name="zone_to_geo_zone[' + zone_to_geo_zone_row + '][country_id]" id="country' + zone_to_geo_zone_row + '" onchange="$(\'#zone' + zone_to_geo_zone_row + '\').load(\'index.php?route=setting/geo_zone/zone&token=<?php echo $token; ?>&country_id=\' + this.value + \'&zone_id=0\');">';
    <?php foreach ($countries as $country) { ?>
    html += '<option value="<?php echo $country['country_id']; ?>"><?php echo addslashes($country['name']); ?></option>';
    <?php } ?>   
    html += '</select></td>';
    html += '<td class="left"><select name="zone_to_geo_zone[' + zone_to_geo_zone_row + '][zone_id]" id="zone' + zone_to_geo_zone_row + '" class="input-text"></select></td>';
    html += '<td class="left"><a onclick="$(\'#zone-to-geo-zone-row' + zone_to_geo_zone_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
    html += '</tr>';
    html += '</tbody>';
    
    $('#zone-to-geo-zone > tfoot').before(html);
        
    $('#zone' + zone_to_geo_zone_row).load('index.php?route=setting/geo_zone/zone&token=<?php echo $token; ?>&country_id=' + $('#country' + zone_to_geo_zone_row).attr('value') + '&zone_id=0');
    
    zone_to_geo_zone_row++;
}
//--></script> 
<?php echo $footer; ?>
<script type="text/javascript">
  $(document).ready(function(){
    addGeoZone();
  });
</script>