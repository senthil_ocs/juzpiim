<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Email List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>

                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>

                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		            	<form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Report Id' name="filter_report_id" value="<?php echo $_POST['filter_report_id'] ; ?>" class="textbox" />
		                        </td> 
		                        <td>
		              				<input type="text" placeholder='Email ID' name="filter_email_id" value="<?php echo $_POST['filter_email_id'] ; ?>" class="textbox" style="width: 250px;"  />
		                   		</td>                  
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		           
		            </div> 
	              
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>

            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
						 
		<div class="row">
			<div class="col-md-12">	
				<?php if ($error_warning) { ?>
					<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
		             </div>
				<?php } ?>
				<?php if ($success) { ?>			
					<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
	                </div>
				<?php } ?>		
			</div>
			<div class="col-md-12">

				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				  <!--<input type="hidden" name="oldsortCode" id="oldsortCode" value="">
				<input type="hidden" name="skuaction" value="update">-->
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left"><?php if ($sort == 'report_id') { ?>
									<a href="<?php echo $sort_report_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Report Id'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_report_id; ?>"><?php echo 'Report Id'; ?></a>
									<?php } ?>
								</td>
                                <td class="left"><?php echo 'Report Name'; ?></td>
								<td class="left"><?php echo 'Email Id'; ?></td>
                                <td class="left"><?php echo 'Status'; ?></td>
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($email) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($email as $menu) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					 <td style="text-align: center;">
										<?php if ($menu['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['report_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['report_id']; ?>" />
										<?php } ?>
									</td>
									<td class="left"><?php echo $menu['report_id']; ?></td>
									<td class="left"><?php echo $menu['report_name']; ?></td>
									<td class="left"><?php echo $menu['email_id']; ?></td>
									<td class="left"><?php if($menu['active']==1){
										echo Active;
									} else{
									echo Inactive; }?></td>
									<td class="left"><?php echo date('d/m/Y g:i A',strtotime($menu['createdon'])); ?></td>
									<td class="right">
										<?php foreach ($menu['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});

function filterReport() {
	document.report_filter.submit();
}

function changeSortOrder(oldsortCode,code){
	$('#oldsortCode').val(oldsortCode);
	var newsortCode = $('#sort_code').val();
    if(newsortCode==oldsortCode){
    	alert("Change your sort code order");
    	return false;
    }else{
        var ajaxData = "itemId=" + code + "&newvalue=" + newsortCode;
		$.ajax({
        type: "POST",
        url: 'index.php?route=setting/touchmenu/updateSortOrder&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //
            } else {
               alert("Sort order updated Successfully");
            }
        }
    });
    	
    }
}
</script>
<?php echo $footer; ?>