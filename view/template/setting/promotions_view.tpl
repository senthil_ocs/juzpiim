<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">View Promotions Details</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                    <a href="<?php echo $back_button; ?>"><button name="purchase_button" type="button" class="btn btn-action btn-danger"  style="margin: 5px 5px 0 0; border-radius: 4px" value="hold"><i class="fa fa-times"></i> Back</button></a>
                    </div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                      <thead>
                        <tr>
                          <td>Promotion Code</td>
                          <td><?php echo $promotionInfo['PromotionCode'] ?></td>
                        </tr>
                        <tr>
                          <td>Description</td>
                          <td><?php echo $promotionInfo['Description']; ?></td>
                        </tr>
                        <tr>
                          <td>From Date</td>
                          <td><?php echo $promotionInfo['FromDate']; ?></td>
                        </tr>
                        <tr>
                          <td>To Date</td>
                          <td><?php echo $promotionInfo['ToDate']; ?></td>
                        </tr>
                        <tr>
                          <td>From Time</td>
                          <td><?php echo $promotionInfo['FromTime'].':00'; ?></td>
                        </tr>
                        <tr>
                          <td>To Time</td>
                          <td><?php echo $promotionInfo['ToTime'].':00'; ?></td>
                        </tr>
                        <tr>
                          <td>Promotion Percentage</td>
                          <td><?php echo $promotionInfo['PromotionPerc']; ?> %</td>
                        </tr>
                        <tr>
                          <td>Promotion Price</td>
                          <td><?php echo $promotionInfo['PromotionPrice']; ?></td>
                        </tr>
                         <tr>
                          <td>Days</td>
                          <td><?php echo $PromotionDays['day']; ?></td>
                        </tr> 
                      </thead>
                  </table>
                  <table class="table orderlist statusstock">
                    <thead>
                      <tr class="heading">
                        <td class="center">S.No</td>
                        <td class="center">Sku</td>
	                      <td class="center">Product Name</td>
                        <td class="center">Selling price</td>
                        <td class="center">Promotion Price</td>
                      </tr>
                        </thead>
                        <tbody>
                         <?php $i = 1;
                          $class = 'odd'; 
                          $class = ($class == 'even' ? 'odd' : 'even'); ?>
                          <?php foreach($skuDetails as $key => $skuDetail) {?>
                          <tr class="<?php echo $class; ?>">
                            <td class="center"><?php echo $i; ?></td>
                            <td><?php echo $skuDetails[$key]['sku']; ?></td>
                            <td><?php echo $skuDetails[$key]['name']; ?></td>
                             <td><?php echo $skuDetails[$key]['sku_price']; ?></td>
                              <td><?php echo $skuDetails[$key]['promotion_price']; ?></td>
                           </tr>
                        <?php $i++; }?>
                        </tbody>
                    </table>
                    </div>
                </form>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();  
}

</script>
<?php echo $footer; ?>