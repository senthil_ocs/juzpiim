<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="promotionform">
      <input type="hidden" name="page_action" value="update">
        <div class="page-content" >
            <h3 class="page-title">Promotions</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="button" title="Save" onclick="submitformaction();" class="savepromo btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?>
                </button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   <div class="col-md-12">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="3">Location</th></tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td width="20%"><label for="name">Location</label></td>
                                <td width="80%" class="order-nopadding">
                                <?php if($locations) {
                                    foreach ($locations as $count => $location) { 
                                        if($count % 3 ==0){ echo '<br>'; } 
                                ?>
                                <div class="col-md-4 col-xs-6 col-sm-6" >
                                 <input type="checkbox" name="location[]" value="<?php echo $location['location_code'];?>" <?php if(in_array($location['location_code'],$location_code)){ echo 'checked'; } ?> > &nbsp;<?php echo $location['location_name']; ?>
                                 <br><br>
                                 </div> 
                                  <?php  }
                                } ?>
                                  
                                
                                </td>  
                              </tr>
                          </tbody>
                      </table>
                    </div>
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                   		  <thead>
	                        <tr><th colspan="2">Basic Details</th></tr>
	                      </thead>
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Promotion Code<span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding"> 
                                <input type="text" name="promotion_code" value="<?php echo $promotion_code; ?>" class="textbox" placeholder="Promotion Code" readonly required>
                              </td>
                            </tr>                             
		                      <tr>
		                          <td width="35%"><label for="name">Description<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                          <input type="text" name="description" value="<?php echo $description; ?>" class="textbox" placeholder="Description" required>
		                          </td>
		                        </tr>
			                       <tr>
		                          <td width="35%"><label for="name">From Date<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                               <input type="text" name="from_date" value="<?php echo $from_date; ?>" class="textbox date" autocomplete="off" placeholder="From Date" id="from_date" required readonly>
                              </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">To Date<span class="required">*</span></label></td>
		                            <td width="65%" class="order-nopadding"> 
                               <input type="text" name="to_date" value="<?php echo $to_date; ?>" class="textbox date" autocomplete="off" placeholder="To Date" id="to_date" required readonly>
                                </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">From Time<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                              <select name="from_time">
                                <?php for($i=1;$i<=24;$i++){?>
                                      <option value="<?php echo $i;?>" <?php if($from_time == $i){ echo 'selected';} ?>><?php echo $i.':00';?></option>
                                <?php }?>
                              </select>  
		                          </td>
		                        </tr> 
                              <tr>
                                <td width="35%"><label for="name">To Time<span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding">
                                                             
                              <select name="to_time">
                                <?php for($j=1;$j<=24;$j++){?>
                                      <option value="<?php echo $j;?>" <?php if($to_time == $j){ echo 'selected';} ?>><?php echo $j.':00';?></option>
                                <?php }?>
                              </select>  

                              
                                </td>
                              </tr>                              
                        </tbody>
                      </table>                     
                    </div>
                    <div class="col-md-6">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="3">Pick Days</th></tr>
                          </thead>
                          <tbody>
                          <tr>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Sunday',$day)){ echo 'checked'; }else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Sunday'; ?>" class=""><label for="custom">Sunday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Monday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Monday'; ?>" class="" ><label for="custom">Monday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Tuesday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Tuesday'; ?>" class=""><label for="custom">Tuesday</label>                        
                            </td>
                           </tr>                           
                            <tr>
                             <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Wednesday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Wednesday'; ?>" class=""><label for="custom">Wednesday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Thursday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Thursday'; ?>" class=""><label for="custom">Thursday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Friday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Friday'; ?>" class=""><label for="custom">Friday</label>                        
                            </td>
                            </tr>
                             <tr>
                             <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" <?php if(in_array('Saturday',$day)){ echo 'checked';}else if(empty($day)){ echo 'checked'; } ?> value="<?php echo 'Saturday'; ?>" class="" ><label for="custom">Saturday</label></td>
                            <td style="border-top:none;">
                            <td style="border-top:none;">                       
                            </td> 
                            </tr> 
                        </tbody>
                      </table>
                     </div>
                      <div class="col-md-6">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="3">Price Details</th></tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td width="35%"><label for="name">Promotion Percentage</label></td>
                                <td width="65%" class="order-nopadding">
                                   <input type="number" step="any"  name="promotion_prescription" value="<?php echo $promotion_prescription; ?>" class="textbox promotion_amount" placeholder="Promotion Perc" readonly>
                                </td>
                              </tr>
                              <tr>
                                <td width="35%"><label for="name">Promotion Price</label></td>
                                <td width="65%" class="order-nopadding">
                                <input type="number" step="any"  name="promotion_price" value="<?php echo $promotion_price; ?>" class="textbox promotion_amount" placeholder="Promotion Price" readonly>
                                <input type="hidden" name="selected_product_cnt" id="selected_product_cnt" value="">
                                <input type="hidden" name="exist_selected_product_cnt" id="exist_selected_product_cnt" value="<?php echo count($PromotionSkuInfo); ?>">
                                </td>
                              </tr>
                          </tbody>
                      </table>
                    </div>
                    <div class="col-md-6"></div>
         </div>
          <div class="col-md-12">
            <h2> Products List</h2>
            <h4> Existing Items</h4>
              <table class="table">
                <thead>
                  <tr class="heading">
                      <td>SKU</td>
                      <td>Name</td>
                      <td>Selling Price</td>
                      <td>Promotion Price</td>
                      <td>Action</td>
                  </tr>
                </thead>
              <tbody id="resultexistTable">
              
                   <?php 
                   if($PromotionSkuInfo){
                    foreach($PromotionSkuInfo as $products){ ?>
                        <tr id=<?php echo "exist_sku_".$products['sku'];?>>
                          <td><input type="text" name="exist_sku_[]" readonly value='<?php echo $products['sku']; ?>'></td>
                          <td><input type="text" name="exist_name[]" readonly value='<?php echo $products['name']; ?>'></td>
                          <td><input type="text" name="exist_sku_price[]" class="sku_price" readonly value='<?php echo $products['sku_price']; ?>'></td>
                          <td><input type="text" name="exist_pro_price[]" class="pro_price" readonly value='<?php echo $products['promotion_price']; ?>'></td>
                          <td><a onclick="removeexistSKU('<?php echo $products['sku'];?>')">Remove</a></td>
                        
                        </tr>

                     
                <?php } } ?>
                
              </tbody>
            </table>
          </div>
          <div class="col-md-12">
             <h4>New Items </h4>
            <table class="table orderlist statusstock">
              <tbody>
                <tr class="filter">  
                    <td> 
                          <select name="filter_department" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $data['filter_department']) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                      <td>
                        <select name="filter_category" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if (trim($category['category_code']) == $data['filter_category']) { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                          <?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                    <?php } ?>   
                                <?php } ?>
                            <?php } ?>
                        </select> 
                    </td>
                    <td>
                        <input type="text" placeholder="SKU / Name" name="filter_name" value="<?php echo $data['filter_name'];?>" class="textbox" 
                        autocomplete="off">
                    </td>
                    

                  <td align="center" colspan="4">
                      <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="loadData();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> Load Data</button>
                  </td>
                </tr>
              </tbody>
            </table>            
          </div>  
          <div class="col-md-12">
                <div class="innerpage-listcontent-blocks"> 
                 <table class="table orderlist statusstock" id="resultTable">
                        <thead>
                           <tr class="heading">
                            <td>SKU</td>
                            <td>Name</td>
                            <td>Selling Price</td>
                            <td>Promotion Price</td>
                            <td>Action</td>

                           </tr>
                        </thead>
                 </table>
              </div>
         </div>
       </form>
    </div>
</div>

<?php echo $footer; ?>
<script type="text/javascript">
  $('.date').datepicker({ dateFormat: 'dd-mm-yy' });

  $('.promotion_prescription').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
  });
   $('.promotion_price').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
  });

  function loadData() {
  var promotion_price        = $('input[name=\'promotion_price\']').attr('value');  
  var promotion_percentage   = $('input[name=\'promotion_prescription\']').attr('value');  
  var filter_department      = $('select[name=\'filter_department\']').attr('value');  
  var filter_category        = $('select[name=\'filter_category\']').attr('value');  
  var error = 0;
  if(promotion_price=='' && promotion_percentage==''){
    alert('Please Enter Promotion Price or Promotion Percentage');
    return false;
  }else if(promotion_price!='' && promotion_percentage!=''){
    alert('Please Enter Promotion Price or Promotion Percentage only');
    return false;
  }
  
        $("#resultTable").find("tr:gt(0)").remove();
        var ajaxData = $("form").serialize();
        $.ajax({
        type: "POST",
        url: 'index.php?route=setting/promotions/ajaxGetPromotionProductsList&token=<?php echo $token; ?>',
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result!='') {
               $('#selected_product_cnt').val('1');
               $.each(result, function(i, item) {
                var sku       = item.sku;
                var name      = item.name;
                var sku_price = item.sku_price;
                var pro_price = item.promotion_price; 
                var TR = '<tr id="sku_'+sku+'"><td><input type="text" name="sku[]" readonly value='+sku+'></td><td><input type="text" name="name[]" readonly value="'+name+'"></td><td><input type="text" name="sku_price[]" class="sku_price" value='+sku_price+' readonly></td><td><input type="text" name="pro_price[]" class="pro_price" value='+pro_price+'></td><td><a onclick="removeSKU('+sku+')">Remove</td></tr>';
                $('#resultTable tr:last').after(TR);
               });
            } else {
                
            }
        }
    });
  
}

function removeSKU(sku){
    $('#resultTable tr#sku_'+sku).remove();
  }
function removeexistSKU(sku){
    $('#resultexistTable tr#exist_sku_'+sku).remove();
  }

 $(".promotion_amount").change(function(){
    var price = $(this).val();
    price =parseFloat(price);
   var name = $(this).attr('name');
    if(name =='promotion_prescription' ){
       $( ".sku_price" ).each(function( index ) {
           var sellingPrice = $(this).val();
           var pro_price = parseFloat(sellingPrice)-(sellingPrice*(price/100));

           $(this).parent('td').next('td').find('input').val(pro_price.toFixed(2));
       });

    }else{
         $( ".sku_price" ).each(function( index ) {
            var row_id = $(this).parent('td').parent('tr').attr('id');
            var sellingPrice = $(this).val();
            var sellingPrice = parseFloat(sellingPrice);
            if(sellingPrice < price){
              $( "tr#"+row_id ).remove();
            }
            $(".pro_price").val(price);
          });
    }

  

});
 function submitformaction(){
 
  var checked=[];
 $("input[name='location[]']:checked").each(function ()
  {
      checked.push(parseInt($(this).val()));
  });
   if(checked.length !=0){
      $('#promotionform').submit();
   }else{
    alert("Enter form Mandatory Details");
      return false;
   }
  
}
 $(".promotion_amount").change(function(){
    var prices = $(this).val();
    $('.promotion_amount').val('');
    $(this).val(prices);
 });
</script>
