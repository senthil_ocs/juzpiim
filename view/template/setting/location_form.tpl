<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title"><?php echo $heading_title; ?></h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div class="row">
            <div style="clear:both"></div>         
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_code; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<?php if($error_code) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
										<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox requiredborder" placeholder="Enter Code" <?php if($code != '') { ?> readonly <?php } ?>>         
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_code) { ?>
                               		   <input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox" placeholder="Enter Code" <?php if($code != '') { ?> readonly <?php } ?>>
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="category"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
								    <?php if($error_name) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                        <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter Name">         
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_name) { ?>
                                       <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter name">
                                    <?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                    <label for="category"><?php echo $entry_type; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <?php if($error_type) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Select Type"></i>
                                        <select name="type" id="type" class="textbox requiredborder">
                                            <option value=""> Select Type </option>
                                            <option value="HQ" <?php if($type == 'HQ'){ ?> selected <?php } ?>>HQ</option>
                                            <option value="Outlet" <?php if($type == 'Outlet'){ ?> selected <?php } ?>>Outlet</option>
                                            <option value="WareHouse" <?php if($type == 'WareHouse'){ ?> selected <?php } ?>>WareHouse</option>
                                        </select>       
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_type) { ?>
                                       <select name="type" id="type" class="textbox">
                                            <option value=""> Select Type </option>
                                            <option value="HQ" <?php if($type == 'HQ'){ ?> selected <?php } ?>>HQ</option>
                                            <option value="Outlet" <?php if($type == 'Outlet'){ ?> selected <?php } ?>>Outlet</option>
                                            <option value="WareHouse" <?php if($type == 'WareHouse'){ ?> selected <?php } ?>>WareHouse</option>
                                        </select>
                                    <?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="remarks"><?php echo $entry_address; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                <input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Enter Address 1">
                                <input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox" placeholder="Enter Address 2">                            
                                </td>                                
                            </tr>
							<tr>
							<td width="20%">
								<label for="status"><?php echo $entry_country; ?><span class="required">*</span></label>
							</td>
							<td width="80%" class="order-nopadding">
								<?php if($error_country) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter country"></i>
                                        <input type="text" name="country" id="country" value="<?php echo $country; ?>" class="textbox requiredborder" placeholder="Enter country">         
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_country) { ?>
                                       <input type="text" name="country" id="country" value="<?php echo $country; ?>" class="textbox" placeholder="Enter country">
                                    <?php } ?>
							</td>
						</tr>
                        <tr>
                            <td width="20%">
                                <label for="status"><?php echo $entry_postcode; ?><span class="required">*</span></label>
                            </td>
                            <td width="80%" class="order-nopadding">
                                <?php if($error_postcode) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Postcode"></i>
                                        <input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" class="textbox requiredborder" placeholder="Enter postcode">         
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_postcode) { ?>
                                       <input type="text" name="postcode" id="postcode" value="<?php echo $postcode; ?>" class="textbox" placeholder="Enter postcode">
                                    <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <label for="status"><?php echo $entry_phone; ?></label>
                            </td>
                            <td width="80%" class="order-nopadding">
                                 <input type="text" name="phone" id="phone" value="<?php echo $phone; ?>" class="textbox" placeholder="Enter phone">
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <label for="status">Email</label>
                            </td>
                            <td width="80%" class="order-nopadding">
                                 <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="textbox" placeholder="Enter email">
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <label for="status">Business Reg No</label>
                            </td>
                            <td width="80%" class="order-nopadding">
                                 <input type="text" name="business_reg_no" id="business_reg_no " value="<?php echo $business_reg_no; ?>" class="textbox" placeholder="Enter Registration No">
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>