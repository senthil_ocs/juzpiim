<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Xero Settings List</h3>
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?>
            			</div>
                    </div>
                </div>
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
        		<div class="portlet bordernone" style="margin-bottom:0 !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <!-- <form method="post" name="report_filter" id="report_filter" action=""> -->
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -10px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox" />
		                        </td> 
			                  	<td>
					              	<input type="text" placeholder='Xero Account Code' name="filter_xero_acc_code" value="<?php echo $_REQUEST['filter_xero_acc_code'] ; ?>" class="textbox" />
					            </td>                 
			                   	<td>
					              	<select name="filter_available_to" id="filter_available_to" class="textbox"  style="min-height: 35px; padding: 7px">
					                    <option value="">Select Available To</option>
					                    <option value="Purchase"<?php if( $filteravailableto == 'Purchase') { ?> selected="selected" <?php } ?>>Purchase</option>
					                    <option value="Sales"<?php if( $filteravailableto == "Sales") { ?> selected="selected" <?php } ?>>Sales</option>
					                    <option value="Both"<?php if( $filteravailableto == "Both") { ?> selected="selected" <?php } ?>>Both</option>
					                </select>
			            		</td>
			            		<td>
			            			<select name="filter_status" id="filter_status" class="textbox"  style="min-height: 35px; padding: 7px">
					                    <option value="">Select Status</option>
					                    <option value="1" <?php if( $filterstatus == 1) { ?> selected="selected" <?php } ?>>Enabled</option>
					                    <option value="0" <?php if( $filterstatus == '0') { ?> selected="selected" <?php } ?>>Disabled</option>
								    </select>
			                    </td>
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>
		            </div> 
	              <?php if (($_REQUEST['filter_date_from'] !="") || ($_REQUEST['filter_date_to'] != "") || ($_REQUEST['filter_days'] != "") || ($_REQUEST['filter_status'] !="" )){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- </form> -->
           
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
		<div class="row">
			<div class="col-md-12">
                <div class="innerpage-listcontent-blocks">
				<table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
							<td class="left"><?php if ($sort == 'name') { ?>
							<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
							<?php } ?></td>

							<td class="left"><?php if ($sort == 'available_to') { ?>
							<a href="<?php echo $sort_available_to; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_available_to; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_available_to; ?>"><?php echo $column_available_to; ?></a>
							<?php } ?></td>

							<td class="left"><?php if ($sort == 'xero_account_code') { ?>
							<a href="<?php echo $sort_xero_account_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_xero_account_code; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_xero_account_code; ?>"><?php echo $column_xero_account_code; ?></a>
							<?php } ?></td>

							<td class="left"><?php if ($sort == 'status') { ?>
							<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
							<?php } ?></td>
							<td class="left"><?php if ($sort == 'isNetwork') { ?>
							<a href="<?php echo $sort_isNetwork; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_isNetwork; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_isNetwork; ?>"><?php echo $column_isNetwork; ?></a>
							<?php } ?></td>
							<td class="right"><?php echo $column_action; ?></td>
						</tr>
                        </thead>
                        <tbody>
                            <?php if ($xeroSettingsValues) { ?>
                            <?php $class = 'odd'; ?>
						<?php foreach ($xeroSettingsValues as $xeroSettingValue) {  ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $class; ?>">
								<td style="text-align: center;"><?php if ($xeroSettingValue['selected']) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $xeroSettingValue['id']; ?>" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $xeroSettingValue['id']; ?>" />
								<?php } ?></td>
								<td class="left"><?php echo $xeroSettingValue['name']; ?></td>
								<td class="left"><?php echo $xeroSettingValue['available_to']; ?></td>
								<td class="left"><?php echo $xeroSettingValue['xero_acc_code']; ?></td>
								<td class="left"><?php echo $xeroSettingValue['status']; ?></td>
								<td class="left"><?php echo $xeroSettingValue['isNetwork'] > 0  ? 'Yes' : 'No' ;; ?></td>
								<td class="right"><?php foreach ($xeroSettingValue['action'] as $action) { ?>
								[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
								<?php } ?>
								</td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
						<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
/*function filterReport(report_type) {
    document.getElementById('type').value=report_type;
	//document.form.submit();
	$("#form").submit();
}*/
function filterReport() {
    url = 'index.php?route=setting/xero_settings&token=<?php echo $token; ?>';
    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    var filter_xero_acc_code = jQuery('input[name=\'filter_xero_acc_code\']').attr('value');
    if (filter_xero_acc_code) {
        url += '&filter_xero_acc_code=' + encodeURIComponent(filter_xero_acc_code);
    }

    var filter_available_to = $('#filter_available_to').val();
    if (filter_available_to) {
        url += '&filter_available_to=' + encodeURIComponent(filter_available_to);
    }

    var filter_status = $('#filter_status').val();
    if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
    }
    
    location = url;
}
</script>
<?php echo $footer; ?>
<!-- END NEW CODE -->