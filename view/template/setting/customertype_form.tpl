<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Customer Master</h3>           
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-12 ">
                   		<table class="table orderlist">
                        <tbody>
                             <tr>
                                <td width="35%"><label for="name">Name <span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding">
                                <?php if($error_name) { ?>
                                  <div class="input-icon right">
                                    <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                    <input type="text" name="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Name">
                                  </div>
                                <?php } ?>
                                <?php if(!$error_name) { ?>
                                <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Name">
                                <?php } ?>
                              </td>
                            </tr>
                            <tr>
                             <td width="35%"><label for="name">Discount</label>
                                  </td>
                                  <td width="65%" class="order-nopadding1">
                                    <select name="discount" class="selectpicker selectdropdown">
                                      <option value="">- None -</option>
                                      <?php if(!empty($discounts)) { ?>
                                      <?php foreach($discounts as $discountslist) { ?>                               
                                      <option value="<?php echo $discountslist['discount_id']; ?>" <?php if($discountslist['discount_id'] == $discount) { ?> selected="selected" <?php } ?>><?php echo $discountslist['discount_name']; ?></option>
                                      <?php } } ?>
                                    </select>
                                  </td>
                            </tr>
                            <tr>
                             <td width="35%"><label for="name">Tax Category</label></td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="taxcategory" class="selectpicker selectdropdown">
                                      <option value="">- None -</option>
                                      <?php if(!empty($taxrates)) { ?>
                                      <?php foreach($taxrates as $taxrate) { ?>
                                      <option value="<?php echo $taxrate['tax_rate_id']; ?>" <?php if($taxrate['tax_rate_id'] == $taxcategory) { ?> selected="selected" <?php } ?>>
                                       <?php echo $taxrate['name']; ?>
                                     </option>
                                      <?php } ?>
                                      <?php } ?>
                                    </select>
                                  </td>
                              </tr>
                        </tbody>
                    </table>
                   	</div>
                   </div>
           		</div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>