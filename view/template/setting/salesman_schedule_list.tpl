<?php echo $header; ?>
<head>
	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
</head>
<style type="text/css">
.li-post-group {
    background: #f5f5f5;
    padding: 5px 10px;
    border-bottom: solid 1px #CFCFCF;
    margin-top: 5px;
}
.li-post-title {
    border-left: solid 4px #304d49;
    background: #a7d4d21f;
    padding: 5px;
    color: #304d49;
    margin: 0px;
}
.list-unstyled {
    padding-left: 0;
    list-style: none;
}
</style>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Schedule List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>				
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important; width: 100%;">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0;">
		            <div class="caption" style="width: 99%;"> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 100%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td  style="width: 15%;"> <input type="text" id='from_date' placeholder='From Date' name="filter_date" value="<?php echo $filters['filter_date'] ; ?>" class="textbox date" autocomplete="off">
		                        </td>                  
		                        <td align="center" colspan="3"  style="width: 10%;">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>                  
		                        <td align="right">
		                          	<button id="setPriorityId" type="button" style="min-height: 36px; border-radius:0 !important;" type="button" class="btn btn-zone btn-primary" data-toggle="modal" data-target="#ajax-modal-qedit" data-backdrop="static" data-keyboard="false">Set Priority</button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		                  
		            </div>  
		            </div> 
	              <?php if (($_POST['filter_from_date'] !="") || ($_POST['filter_Type'] != "")){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
            
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?> 
		<div class="row">			
			<div class="col-md-12">
				<div>
					<h4 class="text-center"> Schedule List for Driver: <?php echo $deliveryManName; ?></h4>
				</div>
			</div>
		</div>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
                           		<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left">SNo</td>
								<td class="left">DO No</td>
								<td class="left">Order No</td>
								<td class="left">Invoice Date</td>
								<td class="left">Customer Name</td>
								<td class="left">Total Items</td>
								<td class="left">Signature</td>
								<td class="left"><?php echo 'Approve'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($salesmanList) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($salesmanList as $value) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); 
						$status = $value['delivery_status'] == 'Assigned' ? 1 : 0;
					?>
					<tr class="<?php echo $class; ?>">
									<td style="text-align: center;">
										<?php if ($menu['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['report_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $menu['report_id']; ?>" />
										<?php } ?>
									</td>
									<td class="left"><?php echo $value['sort_id']; ?></td>
									<td class="left"><?php echo $value['do_no']; ?></td>
									<td class="left"><?php echo $value['invoice_no']; ?></td>
									<td class="left"><?php echo date('d/m/Y',strtotime($value['invoice_date'])); ?></td>
									<td class="left"><?php echo $value['name']; ?></td>
									<td class="left"><?php echo $value['total_items']; ?></td>
									<td align="center">
									<?php if($value['delivery_status']=='Delivered'){ ?>
										<a href="#" data-image="<?php echo $value['signature']; ?>" class=" imagePopupBtn"  data-toggle="tooltip" title="View Signature"><i class='fas fa-file-signature' style="font-size:18px;"></i></a>
									<?php } else{ echo "Not Delivered"; } ?>
									</td>
									<td>
										<label class="checkbox-inline" >

										<?php if($value['delivery_status']!='Delivered'){ 
											if($status){ ?>
										  <input type="checkbox" checked data-toggle="toggle" data-size="small" id="toggle" data-onstyle="success" data-offstyle="danger"
										   data-ref="<?php echo $value['do_no']; ?>" class="toggle">
										<?php } else{ ?>
											<input type="checkbox" data-toggle="toggle" data-size="small" id="toggle" data-onstyle="success" data-offstyle="danger"
										   data-ref="<?php echo $value['do_no']; ?>" class="toggle">
										<?php } }else{ echo "Delivered"; } ?>
										</label>
	                                </td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="9"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1">
    <form action="<?php echo "sort_id"; ?>" class="form-horizontal" method="POST" id="sort_id">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Set Priority</h4>
    </div>
    <div class="modal-body">  
    	<div class="row">			
			<div class="col-md-12">
				<div>
					<h4 class="text-center"> Schedule List for Driver: <?php echo $deliveryManName; ?></h4>
				</div>
			</div>
		</div>      
        <div class="form-body">
            <div id="qedit_error" class=""></div>
            <div class="form-group">
				<div class="alert icon-alert with-arrow alert-success form-alter" role="alert" style="width: 100%;">
					<i class="fa fa-fw fa-check-circle"></i>
					<strong> Success ! </strong> <span class="success-message"> Set priority has been updated successfully </span>
				</div>
				<div class="alert icon-alert with-arrow alert-danger form-alter" role="alert" style="width: 100%;">
					<i class="fa fa-fw fa-times-circle"></i>
					<strong> Note !</strong> <span class="warning-message"> Empty list cant be ordered </span>
				</div>
				<ul class="list-unstyled ui-sortable " id="post_list">
					<?php foreach ($salesmanList as $value) { 
						if($value['delivery_status'] !='Delivered'){ 
					?>
					<li data-post-id="<?php echo $value['do_no']; ?>" class="" style="">
						<div class="li-post-group">
							<h5 class="li-post-title" style="cursor: pointer;">Invoice : <?php echo $value['do_no']; ?></h5>
						</div>
					</li>
					<?php } } ?>
				</ul>
            </div>
        </div>
    </div>
    <div class="modal-footer">
	    <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
	</div>
    </form>
</div>

<div id="ajax-modal" class="modal fade modal-scroll in" tabindex="-1">
 	<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    </div>
    <div class="form-body" id="imagePopup">
    </div>
    <div class="modal-footer">
	    <a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>   
	</div>
</div>

<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js" ></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

<script type="text/javascript">
$("#setPriorityId").on("click", function(){
 	$(".alert-danger").hide();
 	$(".alert-success").hide();
});
$('#ajax-modal-qedit').on('hidden.bs.modal', function (e) {
	filterReport();
});
$( "#post_list" ).sortable({
	placeholder : "ui-state-highlight",
	update  : function(event, ui)
	{
		var post_order_ids = new Array();
		$('#post_list li').each(function(){
			post_order_ids.push($(this).data("post-id"));
		});
		$.ajax({
			url:"<?php echo HTTP_SERVER; ?>index.php?route=setting/salesman/setpriority&token=<?php echo $token; ?>",
			method:"POST",
			data:{post_order_ids:post_order_ids},
			success:function(data)
			{
			 if(data){
			 	$(".alert-danger").hide();
			 	$(".alert-success ").show();
			 	setTimeout(function(){ $(".alert-success").hide(); }, 1000);
			 }else{
			 	$(".alert-success").hide();
			 	$(".alert-danger").show();
			 	setTimeout(function(){ $(".alert-danger").hide(); }, 1000);
			 }
			}
		});
	}
})
$('.toggle').change(function() {
    var salerOrderId = $(this).attr("data-ref");
    var checkStatus = $(this).prop('checked')==true ? 'Assigned' : 'Pending';

    $.ajax({
        url:"<?php echo HTTP_SERVER; ?>index.php?route=setting/salesman/setstatus&token=<?php echo $token; ?>",
        type:"POST",
        data:{
          salerOrderId : salerOrderId,
          checkStatus: checkStatus
        },
        success:function(result){
        	console.log(result);
        }
    });

});

$('.imagePopupBtn').click(function(){
	var image = $(this).attr("data-image");
	var path  = "<?php echo HTTPS_SERVER; ?>doc/signature/"+image;
	$('#imagePopup').html('<img src="'+path+'" width="500" height="300">');
    $('#ajax-modal').modal('show');
});

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport() {
	document.report_filter.submit();
}
$('.toggle').bootstrapToggle({
	  on: 'Approved',
	  off: 'Not Approved',
});
</script>
<?php echo $footer; ?>