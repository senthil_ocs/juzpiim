<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Xero Settings</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>        
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_name; ?>&nbsp;<span class="required" aria-required="true">* </span>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
							 		
                                   <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter name">
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Description</td>
                                <td width="80%" class="order-nopadding">
                                    <textarea name="description" id="description" class="textbox" placeholder="Enter description"><?php echo $description; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_available_to; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="80%" class="order-nopadding">

                                <select name="available_to" class="selectpicker selectdropdown">
                                    <option value="Purchase" <?php echo $available_to == 'Purchase' ? 'Selected' : ''; ?>>Purchase</option>
                                    <option value="Sales" <?php echo $available_to == 'Sales' ? 'Selected' : ''; ?>>Sales</option>
                                    <option value="Both" <?php echo $available_to == 'Both' ? 'Selected' : ''; ?>>Both</option>
                                </select>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                    <?php echo $entry_xero_account_code; ?>&nbsp;<span class="required" aria-required="true">* </span>
                                </td>
                                <td width="80%" class="order-nopadding">

                                    <?php if($error_xero_acc_code) { ?>
                                        <div class="input-icon right">
                                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Xero Account Code"></i>
                                            <input type="text" name="xero_acc_code" id="xero_acc_code" value="<?php echo $xero_acc_code; ?>" class="textbox requiredborder" placeholder="Enter Xero Account Code">
                                        </div>
                                    <?php } ?>
                                    <?php if(!$error_xero_acc_code) { ?>
                                        <input type="text" name="xero_acc_code" id="xero_acc_code" value="<?php echo $xero_acc_code; ?>" class="textbox" placeholder="Enter Xero Account Code">
                                    <?php } ?>      
                                </td>                                      
                            </tr> 
                            <tr>
                                <td width="20%">
                                    <?php echo $entry_isNetwork; ?>
                                </td>
                                <td width="80%" class="order-nopadding">
                                 <input type="checkbox" name="isNetwork" id="isNetwork" value="1">
                                </td>                                
                            </tr> 
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%">
                                <select name="status" id="status" class="selectpicker selectdropdown">
                                    <option value="1" <?php echo $status == '1' ? 'selected="selected"' : ''; ?> ><?php echo $text_enabled; ?></option>
                                    <option value="0" <?php echo $status == '0' ? 'selected="selected"' : ''; ?>><?php echo $text_disabled; ?></option>
                                </select>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
    var isNetwork = '<?php echo $isNetwork; ?>';
    console.log(isNetwork);
    if(isNetwork > 0) {
        $('#isNetwork').trigger('click'); 
    }
</script>
<?php echo $footer; ?>