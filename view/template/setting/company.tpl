<?php echo $header; ?>
<!-- BEGIN HEADER -->
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php echo $sidebar; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
    <form action="<?php echo $action; ?>" method="post" class="company-details-form" id="formID" enctype="multipart/form-data">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                       	</div>    
                    </div>
                </div>
			</div>
            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                <div class="col-md-12">
                	<div class="innerpage-listcontent-blocks">
                	<table class="table orderlist invetorty-tab">
                    	<tbody>
                        	<tr>
                            	<td width="35%">
                                <?php echo $entry_companyname; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                        <input type="text" name="name" id="name" class="textbox" value="<?php echo $name; ?>" style="height:36px;">
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_company_logo; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<div class="form-group last">
											<div class="fileinput fileinput-new" data-provides="fileinput">
												<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
													<?php if(isset($this->data['preview_logo'])) { ?>
                                                        <img src="<?php echo $this->data['preview_logo']; ?>" alt="" />
                                                    <?php } ?>
												</div>
												<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
												</div>
												<div>
													<span class="btn btn-default btn-file">
													<span class="fileinput-new">
													Select image </span>
													<span class="fileinput-exists">
													Change </span>
													<input type="file" name="logo" id="logo">
                                                    
													</span>
													<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
													Remove </a>
                                                    
												</div>
											</div>
									</div>
                                </td>
                            </tr>
                             <tr>
                            	<td width="35%"><?php echo $entry_address; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" name="address1" id="address1" class="textbox" value="<?php echo $address1; ?>" placeholder="Address Line 1" />
                                    <input type="text" name="address2" id="address2" class="textbox" value="<?php echo $address2; ?>" placeholder="Address Line 2" />
                                </td>
                            </tr>
                             <tr>
                            	<td width="35%"><?php echo $entry_country; ?>
                                </td>
                                <td width="65%">                                    
                                	<select name="country" id="input-country" class="selectdropdown" onchange="getZone(this.value,'state');">
                                        <option value="">Select Country</option>
                                        <?php if (!empty($countrys)) { ?>
                                        <?php foreach ($countrys as $cmpcountry) { ?>
                                            <option value="<?php echo $cmpcountry['country_id']; ?>" <?php if ($cmpcountry['country_id'] == $country) { echo "selected"; }?>><?php echo $cmpcountry['name']; ?></option>
                                        <?php } } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_city; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="city" id="city" class="textbox" value="<?php echo $city; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_state; ?>
                                </td>
                                <td width="65%">
                                		<select name="state" id="state" class="selectdropdown">
                                       <!--  <option value="">Select State</option> -->
                                        <?php foreach ($states as $companystate) { ?>
                                        <?php if ($companystate['zone_id'] == $state) { ?>
                                        <option value="<?php echo $companystate['zone_id']; ?>" selected="selected"><?php echo $companystate['name']; ?></option>
                                        <?php //} else { ?>
                                        <!-- <option value="<?php echo $companystate['zone_id']; ?>"><?php echo $companystate['name']; ?></option> -->
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_postal; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="postal_code" id="postal_code" class="textbox" value="<?php echo $postal_code; ?>"  />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_email; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="email" id="email" class="textbox" value="<?php echo $email; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_weburl; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="web_url" id="web_url" class="textbox" value="<?php echo $web_url; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_phone; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="phone" id="phone" class="textbox" value="<?php echo $phone; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_fax; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="fax" id="fax" class="textbox" value="<?php echo $fax; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_remarks; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                	<textarea name="remarks" id="remarks" class="textarea" placeholder="Enter your remarks"><?php echo $remarks; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%"><?php echo $entry_businessregno; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                		<input type="text" name="business_reg_no" id="business_reg_no" class="textbox" value="<?php echo $business_reg_no; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Location</td>
                                <td width="65%" class="order-nopadding">
                                    <?php 
                                        $strDisabled = "";
                                        if($user_group_id['user_group_id'] != TOP_GROUP){ $strDisabled = ''; } 
                                    ?>
                                    <select name="location_id" id="location_id" class="selectdropdown" <?php echo $strDisabled; ?>>
                                        <option value="">Select Location</option>
                                        <?php foreach ($locations as $locationsDetails) { ?>
                                            <option value="<?php echo $locationsDetails['location_id']; ?>|<?php echo $locationsDetails['location_code']; ?>" 
                                            <?php if($locationsDetails['location_id'] == $location_id || $locationsDetails['location_code'] == $location_code){ ?> selected="selected" <?php } ?>><?php echo $locationsDetails['location_name'].' ('.$locationsDetails['location_code'].')'; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td width="35%">Currency</td>
                                <td width="65%" class="order-nopadding">
                                        <select class="selectdropdown" name="currency_code">
                                            <option value="">Select Currency</option>
                                            <?php if (!empty($currencys)) {
                                                foreach ($currencys as $value) { ?>
                                                <option value="<?php echo $value['code']; ?>" <?php if($value['code']==$currency_code){ echo "Selected"; } ?>><?php echo $value['title']; ?></option>
                                            <?php } } ?>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Term 1</td>
                                <td width="65%" class="order-nopadding">
                                    <textarea class="textbox" name="term1" placeholder="Enter Terms and conditions (1)"><?php echo $term1; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Term 2</td>
                                <td width="65%" class="order-nopadding">
                                    <textarea class="textbox" name="term2" placeholder="Enter Terms and conditions (2)"><?php echo $term2; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Term 3</td>
                                <td width="65%" class="order-nopadding">
                                    <textarea class="textbox" name="term3" placeholder="Enter Terms and conditions (3)"><?php echo $term3; ?></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
                
            </div>
        	/
		</div>
	</div>
    </form>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<script type="text/javascript">
    function getZone(v){
        $.ajax({
            url:"index.php?route=setting/company/getZones&token=<?php echo $token; ?>",
            type:"POST",
            data:{country:v},            
            success:function(out){  
                //alert(out);  
                out = JSON.parse(out);   
                htmlnew = '<option value="">- Select State -</option>';
                if(out != '') {
                    for(i=0; i <out.length; i++) {
                        htmlnew += '<option value="' + out[i]['zone_id'] + '"';
                            if (out[i]['zone_id'] == '') {
                                htmlnew += ' selected="selected"';
                            }
                        htmlnew += '>' + out[i]['name'] + '</option>';
                    }
                }
                 else {
                     htmlnew += '<option value="0" selected="selected"> --- None --- </option>';
                }
                $('select[name=\'state\']').html(htmlnew);                         
            }
        });
    }
</script>
<?php echo $footer; ?>