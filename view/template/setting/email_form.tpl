<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Email</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-12">
                   		<table class="table orderlist">
                        <tbody>                          
                            <tr>
                              <td width="35%"><label for="name">Report Name <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_report_name) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Report Name"></i>
                                <input type="text" name="report_name" value="<?php echo $report_name; ?>" class="textbox requiredborder" placeholder="Report Name">
                              </div>
                              <?php } ?>
                              <?php if(!$error_report_name) { ?>
                              <input type="text" name="report_name" value="<?php echo $report_name; ?>" class="textbox" placeholder="Report Name" <?php if($editForm) {?>  <?php }?>>
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Email Id <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_email_id) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Email Id"></i>
                                <input type="email" name="email_id" value="<?php echo $email_id; ?>" class="textbox requiredborder" placeholder="Email Id">
                              </div>
                              <?php } ?>
                              <?php if(!$error_email_id) { ?>
                              <input type="email" name="email_id" value="<?php echo $email_id; ?>" class="textbox" placeholder="Email Id"
                              <?php if($editForm) {?>  <?php }?>>
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="20%"><label for="status">Status</label></td>
                                <td width="80%" class="order-nopadding1">
                                    <select name="active" class="selectpicker selectdropdown">   
                                        <option value="1" <?php if ($active == '1') { ?> selected="selected" <?php }?>><?php echo 'Active'; ?></option>                                
                                        <option value="0" <?php if ($active == '0') { ?> selected="selected" <?php } ?>><?php echo 'Inactive'; ?></option>
                                        
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                   	</div>
                   	
                   </div>
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
  JavaScript

function validateEmail(email_id) 
{
  var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return re.test(email_id);
}
</script>>
<?php echo $footer; ?>