<?php echo $header; ?>
<!-- BEGIN HEADER -->
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php echo $sidebar; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
<!--             <div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.html">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Setting</a>
					</li>
				</ul>
			</div> -->
		<?php if(isset($general)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_general; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($general);$i++){ ?>  
					<a href="<?php echo $general[$i]["link"]; ?>" class="icon-btn" id="<?php echo  $general[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $general[$i]["altkey"];?></span>
						<i class="<?php echo $general[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $general[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
        <?php } ?>
        
        <?php if(isset($touchmenu)) { ?>
            <div class="portlet bordernone">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-reorder"></i>Touch Menu
                    </div>   
                </div>     
                <div class="portlet-body bgnone">
                    <?php for($i=0;$i<count($touchmenu);$i++){ ?>  
                    <a href="<?php echo $touchmenu[$i]["link"]; ?>" class="icon-btn" id="<?php echo  $touchmenu[$i]["id"]; ?>">
                        <span class="shortcutkey"><?php echo $touchmenu[$i]["altkey"];?></span>
                        <i class="<?php echo $touchmenu[$i]["icon"]; ?>"></i>
                        <div>
                            <?php echo $touchmenu[$i]["Text"]; ?>
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <?php if(isset($sales)){ ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_sales; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($sales);$i++){ ?> 
					<a href="<?php echo $sales[$i]["link"]; ?>" class="icon-btn"  id="<?php echo  $sales[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $sales[$i]["altkey"];?></span>
						<i class="<?php echo $sales[$i]["icon"]; ?>"></i>
					<div>
						<?php echo $sales[$i]["Text"]; ?>
					</div>
					</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>

         <?php if(isset($supplier)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_supplier; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($supplier);$i++){ ?> 
					<a href="<?php echo $supplier[$i]["link"]; ?>" class="icon-btn" id="<?php echo  $supplier[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $supplier[$i]["altkey"];?></span>
						<i class="<?php echo $supplier[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $supplier[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		 <?php if(isset($developeraccess)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i>Developer Access
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($developeraccess);$i++){ ?> 
					<a href="<?php echo $developeraccess[$i]["link"]; ?>" class="icon-btn" id="<?php echo  $developeraccess[$i]["id"]; ?>">
						<span class="shortcutkey"><?php echo $developeraccess[$i]["altkey"];?></span>
						<i class="<?php echo $developeraccess[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $developeraccess[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
        <?php if(isset($addpayment)){ ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_utilities; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($addpayment);$i++){ ?> 
					<a href="<?php echo $addpayment[$i]["link"]; ?>" class="icon-btn" id="<?php echo $addpayment[$i]["id"];;?>">
						<span class="shortcutkey"><?php echo $addpayment[$i]["altkey"];?></span>
						<i class="<?php echo $addpayment[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $addpayment[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
            <!-- -->
		</div>
	</div>
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
	Mousetrap.bind('alt+c', function(e) {
        e.preventDefault();
        var href = $('#company').attr('href');
        //alert(href);
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+l', function(e) {
        e.preventDefault();
        var href = $('#location').attr('href');
        //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+u', function(e) {
        e.preventDefault();
        var href = $('#user').attr('href');
       // alert(href);
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+g', function(e) {
        e.preventDefault();
        var href = $('#usergroup').attr('href');
       // alert(href);
        window.location.href=href;
        return false;
    });

     Mousetrap.bind('alt+s', function(e) {
        e.preventDefault();
        var href = $('#generalsetup').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+t', function(e) {
        e.preventDefault();
        var href = $('#taxsetup').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+e', function(e) {
        e.preventDefault();
        var href = $('#termsetup').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });

      Mousetrap.bind('alt+r', function(e) {
        e.preventDefault();
        var href = $('#currencysetup').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+m', function(e) {
        e.preventDefault();
        var href = $('#countrymaster').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+k', function(e) {
        e.preventDefault();
        var href = $('#stockstatus').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });



       Mousetrap.bind('alt+p', function(e) {
        e.preventDefault();
        var href = $('#paymenttype').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+o', function(e) {
        e.preventDefault();
        var href = $('#customergroup').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+a', function(e) {
        e.preventDefault();
        var href = $('#customermaster').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });

      Mousetrap.bind('alt+l', function(e) {
        e.preventDefault();
        var href = $('#suppliermaster').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+i', function(e) {
        e.preventDefault();
        var href = $('#adjustpayment').attr('href');
       //alert(href);
        window.location.href=href;
        return false;
    });
   
});
</script>