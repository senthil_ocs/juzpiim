<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form" name="form">
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Email Template List</h3>
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <?php } ?>
            			</div>
                    </div>
                </div>
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
        		<div class="portlet bordernone" style="margin-bottom:0 !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post">
		                  <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -10px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $data['filter_name'] ; ?>" class="textbox" />
		                        </td>
			            		<td>
			            			<select name="filter_status" id="filter_status" class="textbox"  style="min-height: 35px; padding: 7px">
					                    <option value="">Select Status</option>
					                    <option value="1" <?php if( $data['filter_status'] == 1) { ?> selected="selected" <?php } ?>>Enabled</option>
					                    <option value="0" <?php if( $data['filter_status'] == '0') { ?> selected="selected" <?php } ?>>Disabled</option>
								    </select>
			                    </td>
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		              </form>
		            </div>
		            </div> 
             <div style="clear:both; margin:0 0 15px 0;"></div>
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
		<div class="row">
			<div class="col-md-12">
                <div class="innerpage-listcontent-blocks">
				<table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
                            <td>Sno</td>
                            <td>Name</td>
                            <td>Subject</td>
                            <td>Description</td>
                            <td>Status</td>
							<td class="right">Action</td>
						</tr>
                        </thead>
                        <tbody>
                            <?php if ($email_templates) { ?>
                            <?php $class = 'odd'; $m=0; ?>
						<?php foreach ($email_templates as $email_template) {  ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); $m++; ?>
							<tr class="<?php echo $class; ?>">

								<td class="left"><?php echo $m; ?></td>
								<td class="left"><?php echo $email_template['name']; ?></td>
								<td class="left"><?php echo $email_template['subject']; ?></td>
								<td class="left"><?php echo $email_template['description']; ?></td>
								<td class="left"><?php echo $email_template['status'] == '1' ? "Enabled":"Disabled"; ?></td>
								<td class="right"><a href="<?php echo $email_template['edit_btn']; ?>">Edit</a></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
						<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
/*function filterReport(report_type) {
    document.getElementById('type').value=report_type;
	//document.form.submit();
	$("#form").submit();
}*/
function filterReport() {
    url = 'index.php?route=setting/email_template&token=<?php echo $token; ?>';
    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    var filter_status = $('#filter_status').val();
    if (filter_status) {
        url += '&filter_status=' + encodeURIComponent(filter_status);
    }
    location = url;
}
</script>
<?php echo $footer; ?>