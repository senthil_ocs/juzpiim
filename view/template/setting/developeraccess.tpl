<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
    <?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">
            <?php echo $heading_title; ?>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <?php if(!empty($breadcrumbs)){ 
                        foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } } ?>                    
                </ul>
                <!-- <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                        </div>    
                    </div>
                </div> -->
            </div>            
            <div class="row">
                 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                    <?php if ($error_warning) { ?>
                         <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error_warning; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div class="innerpage-listcontent-blocks">
                <div class="col-md-12">                  
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_pi_revert; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">Purchase Invoice No: &nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" name="purchase_invoice_no" id="purchase_invoice_no" class="textbox" placeholder="Purchase Invoice No" value="<?php echo $purchase_invoice_no; ?>" style="height:36px;" />
                                </td>
                               
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="page-toolbar">
                                    <div class="btn-group pull-right">
                                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Revert" name="submit" value="purchase_invoice_revert"><span>Revert</span></button>
                                            <?php } ?>
                                            <a href="<?php echo $action; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $action; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                                        </div>    
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>  
                     <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_po_revert; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">Purchase Order No: &nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" name="purchase_order_no" id="purchase_order_no" class="textbox" placeholder="Purchase Order No" value="<?php echo $purchase_order_no; ?>" style="height:36px;" />
                                </td>
                               
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="page-toolbar">
                                    <div class="btn-group pull-right">
                                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Revert" name="submit" value="purchase_order_revert"><span>Revert</span></button>
                                            <?php } ?>
                                            <a href="<?php echo $action; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $action; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                                        </div>    
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>                  
                                        
                    
                </div>                
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    function validatePaymentMethod(id){
        if($('#allow_default_'+id).val() == '1'){
            $('#payment_method_td_'+id).show();
        }else{
            $('#payment_method_td_'+id).hide();
        }
    }
    $(document).ready(function(){
        $(".networks").trigger('change',true);
    });

    $("#shopee_sync").click(function(){
        if($("input[name='shopee_sync']").val() !='' ){
            $.ajax({
                type: "POST",
                url: '<?php echo HTTP_SERVER; ?>index.php?route=setting/setting/shopee_sync&token=<?php echo $this->session->data["token"]; ?>',
                data: {from_date : $("input[name='shopee_sync']").val()},
                success: function(res) {
                    res = JSON.parse(res);
                    if(res.status){
                        location.reload();
                    }else{
                        alert(res.msg);
                    }
                }
            });
        }
    });

    $("#shopify_sync").click(function(){
        if($("input[name='shopify_sync']").val() !='' ){
            $.ajax({
                type: "POST",
                url: '<?php echo HTTP_SERVER; ?>index.php?route=setting/setting/shopify_sync&token=<?php echo $this->session->data["token"]; ?>',
                data: {from_date : $("input[name='shopify_sync']").val()},
                success: function(res) {
                    res = JSON.parse(res);
                    if(res.status){
                        location.reload();
                    }else{
                        alert(res.msg);
                    }
                }
            });
        }
    });
</script>