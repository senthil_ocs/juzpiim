<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Email template Form</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert"></button></div>
            <?php } ?>        
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_name; ?>&nbsp;<span class="required" aria-required="true">* </span>
                                </td>
                                <td width="80%" class="order-nopadding">
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter name">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Subject&nbsp;<span class="required" aria-required="true">* </span>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="subject" id="subject" value="<?php echo $subject; ?>" class="textbox" placeholder="Enter Subject">
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">Description</td>
                                <td width="80%" class="order-nopadding">
                                    <textarea name="description" id="description" class="textbox" placeholder="Enter Description"><?php echo $description; ?></textarea>
                                </td>
                            </tr>
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%" aria-hidden="true" data-toggle="tooltip" title="Added or Available keys :  [invoice_number],  [trading_name], [contact_name], [currency_code], [amount], [invoice_date], [break]">
                                
                                <select name="status" id="status" class="selectpicker selectdropdown">
                                    <option value="1" <?php echo $status == '1' ? 'selected' : ''; ?> ><?php echo $text_enabled; ?></option>
                                    <option value="0" <?php echo $status == '0' ? 'selected' : ''; ?>><?php echo $text_disabled; ?></option>
                                </select>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>