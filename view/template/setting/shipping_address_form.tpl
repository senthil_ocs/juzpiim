<?php echo $header; ?>
<style type="text/css">
    .smallbox{
        width: 70px !important;
    }
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Shipping Address</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>
            <div class="row">          
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="title">Customer Name</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                               		<input type="text" value="<?php echo $customer['name']; ?>" class="textbox" readonly>
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <label for="title">Customer Code</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" value="<?php echo $customer['cust_code']; ?>" class="textbox" readonly>
                                    <input type="hidden" name="customer_id" value="<?php echo $customer['customercode']; ?>">
                                </td>                                
                            </tr>                                                  
                        </tbody>
                    </table>                   
                    <table id="tax-rule" class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
                            <td class="left">Shipping Code</td>
							<td class="left">Name</td>
                            <td class="left">Adderss 1</td>
							<td class="left">Adderss 2</td>
                            <td class="left">City</td>
                            <td class="left">Country</td>
                            <td class="left">Zip</td>
                            <td class="left">Contact No</td>
							<td class="left">Notes</td>
							<td></td>
						</tr>
                        </thead>
                       <?php $address_row = 0; ?>
					<?php foreach ($shipping_address as $address) { ?>
					<tbody id="address_row_<?php echo $address_row; ?>">
						<tr>
							<td class="left">
                                <input type="text" class="textbox smallbox" name="code[<?php echo $address_row; ?>]" value="<?php echo $address['shipping_code']; ?>">
                                
                                <input type="hidden" name="isdefault[<?php echo $address_row; ?>]" value="<?php echo $address['isdefault']; ?>">
                                <input type="hidden" name="shipping_id[<?php echo $address_row; ?>]" value="<?php echo $address['id']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="name[<?php echo $address_row; ?>]" value="<?php echo $address['name']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="address1[<?php echo $address_row; ?>]" value="<?php echo $address['address1']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="address2[<?php echo $address_row; ?>]" value="<?php echo $address['address2']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="city[<?php echo $address_row; ?>]" value="<?php echo $address['city']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="country[<?php echo $address_row; ?>]" value="<?php echo $address['country']; ?>">
                            </td>
                            <td class="left">
                                <input type="number" class="textbox smallbox" name="zip[<?php echo $address_row; ?>]" value="<?php echo $address['zip']; ?>">
                            </td>
                            <td class="left">
                                <input type="number" class="textbox smallbox" name="contact[<?php echo $address_row; ?>]" value="<?php echo $address['contact_no']; ?>">
                            </td>
                            <td class="left">
                                <input type="text" class="textbox smallbox" name="notes[<?php echo $address_row; ?>]" value="<?php echo $address['notes']; ?>">

                            </td>
                            <td>
                                <?php if($address['isdefault']){ echo "Default"; } else{ ?>
                                <a onclick="removeRow('<?php echo $address_row; ?>')" class="btn btn-zone btn-danger"><i class="fa fa-times"></i>Remove</a>
                                <?php } ?>
                            </td>
						</tr>
					</tbody>
					<?php $address_row++; ?>
					<?php } ?>
					<tfoot>
						<tr>
							<td colspan="9"></td>
							<td class="left"><a onclick="addRule();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> Add</a></td>
						</tr>
					</tfoot>
                    </table>                    
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
var address_row = <?php echo $address_row; ?>;

function addRule() {
	html  = '<tbody >';
	html += '  <tr id="address_row_' + address_row + '">';
    html += '    <td><input type="text" class="textbox smallbox" name="code['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="name['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="address1['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="address2['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="city['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="country['+address_row+']"/></td>';
    html += '    <td><input type="number" class="textbox smallbox" name="zip['+address_row+']"/></td>';
    html += '    <td><input type="number" class="textbox smallbox" name="contact['+address_row+']"/></td>';
    html += '    <td><input type="text" class="textbox smallbox" name="notes['+address_row+']"/></td>';
	html += '    <td><a onclick="$(\'#address_row_' + address_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i>Remove</a></td>';
	html += '  </tr>';
	$('#tax-rule > tfoot').before(html);
	address_row++;
}
function removeRow(rowId){
    $('input[name="code['+rowId+']"]').val('');
    $('input[name="address1['+rowId+']"]').val('');
    $('input[name="address2['+rowId+']"]').val('');
    $('input[name="city['+rowId+']"]').val('');
    $('input[name="country['+rowId+']"]').val('');
    $('input[name="zip['+rowId+']"]').val('');
    $('input[name="contact['+rowId+']"]').val('');
    $('input[name="notes['+rowId+']"]').val('');
    $('#address_row_'+rowId).remove();
}

</script>
<?php echo $footer; ?>