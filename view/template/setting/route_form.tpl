<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">  
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Route</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
              <!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                    <div class="col-md-12">
                      <table class="table orderlist">
                        <tbody>                          
                            <tr>
                              <td width="35%"><label for="name">Name <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_name) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                <input type="text" name="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Name">
                              </div>
                              <?php } ?>
                              <?php if(!$error_name) { ?>
                              <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Name" <?php if($editForm) {?>  <?php }?>>
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Code <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                 <?php if($error_code) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Code has already been added!"></i>
                                  <input type="text" name="code" value="<?php echo $code; ?>" class="textbox requiredborder" placeholder="Code">
                                </div>
                                <?php } elseif($error_code_duplication) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Code has already been added!"></i>
                                <input type="text" name="code" value="<?php echo $code; ?>" class="textbox requiredborder" placeholder="Code">
                              </div>
                              <?php } else { ?>
                                <input type="text" name="code" value="<?php echo $code; ?>" class="textbox" placeholder="Code">
                                <input type="text" name="getcode" id="getcode"value="<?php echo $getcode ?>" hidden>
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Description </label></td>
                              <td width="65%" class="order-nopadding">
                              <textarea type="text" name="description" class="textbox " placeholder="Description"><?php echo $description; ?></textarea>
                              </td>
                            </tr>
                            <tr>
                              <?php 
                                if($status == 1 || $status == ''){
                                  $check1 = 'selected="selected"';
                                } else {
                                  $check2 = 'selected="selected"';
                                } 
                              ?>
                              <td width="20%"><label for="status">Status</label></td>
                                <td width="80%" class="order-nopadding1">
                                    <select name="status" class="selectpicker selectdropdown">   
                                        <option value="1" <?php echo $check1; ?> ><?php echo 'Active'; ?></option>                                
                                        <option value="0" <?php echo $check2; ?> ><?php echo 'Inactive'; ?></option>
                                    </select>
                                </td>
                            </tr>
                            <table id="zipcode" class="table orderlist invetorty-tab">
                              <tr><th colspan="4">ZipCode</th></tr>
                                <?php $zipcode_row = 0; ?>
                                <?php foreach ($zipcode as $zipcodes) { ?>
                                <tbody id="zipcode-row<?php echo $zipcode_row; ?>">
                                <tr>
                                  <td width="35%" ></td>
                                  <td width="23%" class="order-nopadding"><input type="text" name="zipcode[]" id="row_<?php echo $zipcode_row; ?>" value="<?php echo $zipcodes['zip_code']; ?>" class="textbox" placeholder="ZipCode"></td>
                                  <td width="2%" class="order-nopadding"><a onclick="$('#zipcode-row<?php echo $zipcode_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> </a></td>
                                  </td>
                                  <td width="40%"></td>
                                </tr>
                                </tbody>
                                <?php $zipcode_row++; ?>
                                <?php } ?>
                                <tfoot>
                                  <tr>
                                    <td ></td>
                                    <td colspan="2" class="right" width="22%"><a onclick="addZipcode();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> Add Zip Code</a></td>
                                    <td></td>
                                  </tr>
                                </tfoot>
                          </table>
                        </tbody>
                    </table>
                    </div>
                    
                   </div>
              </div>
          </form>
         </div>
    </div>
</div>
<script type="text/javascript">
var zipcode_row= <?php echo $zipcode_row; ?>;
function addZipcode() {
    var count = 0;
    var getid = zipcode_row - 1;
    var idname = '#row_'+getid;
    var checkzipcode = $(idname).val();
    var getcode = $('#getcode').val();
    if(checkzipcode == ''){
      count++;
    }
    for (i = 0; i < getid; i++) {
      var idval = '#row_'+i;
      var idduplication = $(idval).val();
      if(checkzipcode == idduplication && idduplication != undefined){
        html  = '<tr id="alertZipcode"><td></td><td colspan ="2"><div class="alert alert-warning  alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'+checkzipcode+' Zipcode Duplication!</strong></div></td><td></td></tr>';
        $('#zipcode-row'+getid).remove();
        $('#zipcode tfoot').before(html);
        setTimeout(function(){ $('#alertZipcode').remove(); }, 1000);
        break;
      } 
      if(idduplication == ''){
        count++;
      }
    }
    if(count == 0){
      $.ajax({
        url:"<?php echo HTTP_SERVER; ?>index.php?route=setting/route/checkZipcode&token=<?php echo $token; ?>",
        type:"POST",
        data:{
          zipcode : checkzipcode,
          getcode : getcode,
        },
        success:function(result){
          if(result>0){
            html  = '<tr id="alertZipcode"><td></td><td colspan ="2"><div class="alert alert-warning  alert-dismissible fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>'+checkzipcode+' Zipcode has already been added!</strong></div></td><td></td></tr>';
            $('#zipcode-row'+getid).remove();
            $('#zipcode tfoot').before(html);
            setTimeout(function(){ $('#alertZipcode').remove(); }, 2000);
          } else {
            html  = '<tbody id="zipcode-row' + zipcode_row + '">';
            html += '  <tr>'; 
            html += '  <td width="35%"></td>'; 
            html += '    <td width="23%" class="order-nopadding"><input class="textbox" type="text" name="zipcode[]" id="row_'+zipcode_row+'" value=""  placeholder="ZipCode"></td>';
            html += '    <td width="2%" class="order-nopadding"><a onclick="$(\'#zipcode-row' + zipcode_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
            html += ' <td width="40%"></td></tr>';
            html += '</tbody>';

            $('#zipcode tfoot').before(html);
            $('#row_'+zipcode_row).focus();
            zipcode_row++;
          }
        }
      });
    }
  }

</script>
<?php echo $footer; ?>