<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Customer Master</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Customer Code<span class="required">*</span></label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                  <input type="text" name="customer_code" class="textbox" value="<?php echo $customer_code;?>" readonly>
                                </td>
                            </tr>
                         <tr>
                          <td width="35%"><label for="name">Title</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="customer_title" value="<?php echo $customer_title; ?>" class="textbox" placeholder="Title">
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Name<span class="required">*</span></label></td>
                          <td width="65%" class="order-nopadding">
                            <div class="">
                            <input type="text" name="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter Name">
                          </div>
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Date of Birth</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="birth_date" value="<?php echo $birth_date; ?>" class="textbox birth_date" placeholder="Date of Birth" autocomplete="off">
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Company</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="company" value="<?php echo $company; ?>" class="textbox" placeholder="Company" id="company">
                          </td>
                        </tr>
                        <tr id="registration_no_tr">
                          <td width="35%"><label for="name">Business Registration No</label></td>
                          <td width="65%" class="order-nopadding">
                            <input type="text" name="registration_no" id="registration_no" value="<?php echo $registration_no; ?>" class="textbox" placeholder="Registration No">
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Currency Code</label></td>
                          <td width="65%" class="order-nopadding">
                            <select class="selectpicker selectdropdown" name="currency_code">
                              <?php if(!empty($currency_collection)){ 
                                foreach ($currency_collection as $currency) { ?>
                                    <option value="<?php echo $currency['code']; ?>" <?php if($currency_code==$currency['code']){ echo 'Selected'; } ?>><?php echo $currency['title']; ?></option>
                              <?php } } ?>
                            </select>
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="name">Tax Allowed</label></td>
                          <td width="65%" class="order-nopadding1">
                            <input style="margin-left:0px" type="radio" name="tax_allow" class="tax_allow" value="0" <?php if($tax_allow == 0) { ?> checked="checked"<?php } ?>>Yes
                            <input style="margin-left:0px" type="radio" class="tax_allow" name="tax_allow" value="1" <?php if($tax_allow == 1) { ?> checked="checked"<?php } ?>>No
                          </td>
                        </tr>
                        <tr id="tax_type_tr">
                          <td width="35%">Tax Type</td>
                          <td width="65%" class="order-nopadding1">
                              <select name="tax_type" class="selectpicker selectdropdown">
                                  <option value="0" <?php if ($tax_type == 0) { ?> selected="selected" <?php } ?>>Inclusive</option>
                                  <option value="1" <?php if ($tax_type == 1) { ?> selected="selected" <?php } ?>>Exclusive</option>
                              </select>
                          </td>
                        </tr>
                        <tr>
                          <td width="35%"><label for="term_id">Term</label></td>
                          <td width="65%" class="order-nopadding">
                            <select class="" name="term_id">
                              <option value="">Select Term</option>
                              <?php if(!empty($allTerms)){ 
                                foreach ($allTerms as $term) { ?>
                                    <option value="<?php echo $term['terms_id']; ?>" <?php if($term['terms_id']==$term_id) { echo "selected"; } ?> ><?php echo $term['terms_name']; ?></option>
                              <?php } } ?>
                            </select>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                      <table class="table orderlist">
                      	<thead>
                        	<tr>
                            	<th colspan="2">Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
              							<td width="35%">
              								<label for="home_num">Home</label>
              							</td>
              							<td width="65%" class="order-nopadding">
              								<input type="text" name="home" value="<?php echo $home; ?>" class="textbox" placeholder="Home" />
              							</td>
            						  </tr>
                          <tr>
                            <td width="35%">
                              <label for="work">Work</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="work" value="<?php echo $work; ?>" class="textbox" placeholder="Work" />
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="mobile">Phone<span class="required">*</span></label>
                            </td>
                            <td width="65%" class="order-nopadding">
                                <div class=" right">
                                  <input type="text" name="contacts" class="textbox" value="<?php echo $contacts; ?>" placeholder="Enter Mobile">
                                </div>
                            </td>
                          </tr>
                          <tr>
                            <td width="35%">
                              <label for="fax">Fax</label>
                            </td>
                            <td width="65%" class="order-nopadding">
                              <input type="text" name="fax" value="<?php echo $fax; ?>" class="textbox" placeholder="Fax" />
                            </td>
                          </tr>
                        </tbody>
                      </table>
                   	</div>
                   	<div class="col-md-6">
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Address</th></tr>
                        	</thead>
                        	<tbody>
	                        	<tr>
            									<td width="35%"><label for="address">Address<span class="required">*</span></label></td>
            									<td width="65%" class="order-nopadding">
            										<input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Address1">
            									</td>
            								</tr>								            
                            <tr>
                              <td width="35%"><label for="city">City</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="textbox" placeholder="City">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="address2">State</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="textbox" placeholder="State">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="zipcode">Zip Code<span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <div class=" right">
                                  <input type="text" name="zipcode" id="zipcode" class="textbox" placeholder="Zip Code" value="<?php echo $zipcode; ?>" >
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="zipcode">Country</label></td>
                              <td width="65%" class="order-nopadding">
                                <select name="country">
                                  <option value="">Select Country</option>
                                  <?php if(!empty($countryes)){ 
                                    foreach ($countryes as $countrys) {  ?>
                                      <option value="<?php echo $countrys['country_id']; ?>" <?php if($countrys['country_id'] == $country){ echo 'selected';} ?>><?php echo $countrys['name']; ?></option>
                                   <?php  } } ?>
                                </select>
                              </td>
                            </tr>
                        </tbody>
                   	  </table>
                      <table class="table orderlist">
                        <thead>
                          <tr><th colspan="2">Others</th></tr>
                        </thead>
                          <tbody>
                            <tr>
                              <td width="35%"><label for="website">Website</label></td>
                              <td width="65%" class="order-nopadding">
                            <div class=" right">
                            <input type="text" name="website" id="website" value="<?php echo $website; ?>" class="textbox" placeholder="Website">
                          </div>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="email">Email</label></td>
                              <td width="65%" class="order-nopadding">
                            <div class="input-icon right">
                              <?php if ($error_email_duplication) { ?>
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Email has already been added!"></i>
                              <?php } ?>
                              <input type="email" name="email" id="email" value="<?php echo $email; ?>" class="textbox <?php if ($error_email_duplication) { ?>requiredborder <?php } ?>" placeholder="Email" style="padding: 0px 0px 0px 11px !important;">
                          </div>
                              </td>
                            </tr>
                        </tbody>
                      </table>
                      <table class="table orderlist">
                        <thead>
                            <tr><th colspan="2">Notes</th></tr>
                        </thead>
                        <tbody>
                        <tr>
                          </td>
                          <td width="65%" class="order-nopadding">
                            <textarea style="border-right: 1px solid #ddd;" name="notes" id="notes" class="textbox" rows="3"><?php echo $notes; ?></textarea>
                          </td>
                        </tr>
                        </tbody>
                    </table>
                   	 </div>
                   </div>
                    <div class="col-md-6">
                    </div>
            	<!-- 24-Feb-2016 Ends /!-->
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">

  $(document).ready(function(){
      if($('#company').val()!=''){
        $('#registration_no_tr').show();
      }else{
        $('#registration_no_tr').hide();
        $('#registration_no').val('');
      }

      var tax_allow = $("input[name='tax_allow']:checked").val();
      if(tax_allow=='0'){
          $('#tax_type_tr').show();
      }else{
          $('#tax_type_tr').hide();
      }
  });
  $('#company').blur(function(){
      if($('#company').val()!=''){
        $('#registration_no_tr').show();
        $('#registration_no').focus();

      }else{
        $('#registration_no').val('');
        $('#registration_no_tr').hide();
      }
  });

$('.tax_allow').change(function(){
      var tax_allow = $("input[name='tax_allow']:checked").val();
      if(tax_allow=='0'){
          $('#tax_type_tr').show();
      }else{
          $('#tax_type_tr').hide();
      }
});
</script>

<script type="text/javascript">
function getTypeList(v)
{
  $.ajax({
      url:"index.php?route=setting/customers/getTypeDetails&token=<?php echo $token; ?>",
      type:"POST",
      data:{customer_type:v},     
      success:function(out){
          out = JSON.parse(out);          
          html = '<option value="">- Select Discount -</option>';
            if(out != '') {                    
                html += '<option value="' + out['discount'] + '"';
                  if (out['discount'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['discount_name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'discount\']').html(html);

          html = '<option value="">- Select Tax -</option>';
            if(out != '') {                    
                html += '<option value="' + out['taxcategory'] + '"';
                  if (out['taxcategory'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'salestax\']').html(html);      
      }
  });
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
  var year = '2020';
  $('.date').datepicker({
    dateFormat: 'dd/mm/yy',
    maxDate : -1,
    minDate : '-60y',
    changeMonth: true,
    changeYear: true
  });

  var dateToday = new Date();
  var start = new Date();
  start.setFullYear(start.getFullYear() - 70);
  var end = new Date();
  end.setFullYear(end.getFullYear() - 15);
  $('.birth_date').datepicker({
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
    yearRange: start.getFullYear() + ':' + end.getFullYear(),
    maxDate: dateToday,
  });

</script>