<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Customer List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button id="xero_button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-upload"></i><span> XERO</span></button>

                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name / Mobile Number' name="filter_name" value="<?php echo $data['filter_name'] ; ?>" class="textbox" />
		                        </td> 
		                        <td>
		              				<input type="text" placeholder='Email' name="filter_email" value="<?php echo $data['filter_email'] ; ?>" class="textbox" style="width: 250px;"  />
		                   		</td> 
		                   		<td>
		              				<select class="textbox" name="xero_button">
		              					<option value="">Select Xero</option>
		              					<option value="1" <?php if($data['xero_button'] == '1'){ echo "Selected"; } ?>>Posted</option>
		              					<option value="0" <?php if($data['xero_button'] == '0'){ echo "Selected"; } ?>>Un Posted</option>
		              				</select>
		                   		</td>                  
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            <div class="tools" style="float:left;padding: 0 0 0 16px;">
		               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
		            </div>
		            </div> 
	              <?php if (($data['filter_date_from'] !="") || ($data['filter_date_to'] != "") || ($data['filter_Type'] != "")){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
           <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: -38px !important; margin-left:15px; width: 15%;">
                <tbody>
                  <tr class="filter">
                   	<td>
                   		<input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" readonly>
            		</td>
              		<td>
                   		<input type="text" id='' placeholder='To Date' name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" readonly>
            		</td> 
            		<td>
            			<select name="filter_Type" class="textbox"  style="min-height: 35px; padding: 7px">
		                    <option value="">Select Type</option>
		                    <option value="1" <?php if( $filtertype == 1) { ?> selected="selected" <?php } ?>>ragu Group</option>
		                    <option value="2" <?php if( $filtertype == 2) { ?> selected="selected" <?php } ?>> Testing Group</option>
					    </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
            
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success!='') { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock" id="table_sort">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;">
									<input type="checkbox" class="selected" name="selected[]">
								</td>
								<td class="left">Customer Code</td>
								<td class="left"><?php if ($sort == 'name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo 'Name'; ?></a>
									<?php } ?>
								</td>
								<td class="left"><?php echo 'Phone No'; ?></td>
								<td class="left"><?php echo 'Address'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td>
								<?php if(HIDE_XERO){ ?>
								<td class="text-center">Xero</td>
								<?php } ?>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($customers) {  
	                        	$class = 'odd';  
	                        	foreach ($customers as $customer) {
	                        	$class = ($class == 'even' ? 'odd' : 'even'); ?>

								<tr class="<?php echo $class; ?>">
									<td style="text-align: center;">
									<?php if($customer['xero_id'] ==''){ ?>
									<?php } ?>
										<input type="checkbox" class="selected_invoice" name="selected[]" value="<?php echo $customer['customer_id']; ?>" />
									</td>
									<td class="left"><?php echo $customer['customercode']; ?></td>
									<td class="left"><?php echo $customer['name']; ?></td>
									<td class="left"><?php echo $customer['mobile']; ?></td>
									<td class="left"><?php echo $customer['address']; ?></td>
									<td class="right">
									 <a href="<?php echo $customer['shipping_address'] ?>"><i class='fa fa-truck'  data-toggle="tooltip" title="Shipping Addresss" style='font-size:19px'></i></a>
									 
									 <a href="<?php echo $customer['edit_button'] ?>"><i class='fa fa-edit'  data-toggle="tooltip" title="Edit" style='font-size:19px'></i></a>
									</td>
									<?php if(HIDE_XERO==1){ ?>
									<td style="text-align: center;" class="tdclass_<?php echo $customer['customer_id'] ?>">

									 <?php if(!empty($customer['xero_id'])){ ?>
									  <span class="text-primary">Success</span>
									  <?php }else{ ?>
									  	<input type="hidden" id="<?php echo $customer['customer_id'] ?>" value="<?php echo $customer['xero_id'] ?>">
										<span class="text-primary xero" data-customer_id="<?php echo $customer['customer_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></span>
									 <?php  } ?>
									</td>
									<?php } ?>

								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
				</form>
				<div class="pagination"><?php echo $pagination; ?></div>
			</div>
		</div>
	</div>
</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript"> 
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
	document.report_filter.submit();
}

$(document).ready(function(){
	$('.xero').css("cursor",'pointer');
	$('#table_sort').dataTable({
      "aaSorting" : [[ 0, "asc" ]],
           paging : false,
        searching : false,
          "bInfo" : false,
        "columnDefs": [ {
            "targets": [0,5,6],
            "orderable": false
          },
          {
          "targets": "_all",
          "defaultContent": "-"
        } ]
    });
});

function validJSON(text){
    if (typeof text!=="string"){
        return false;
    }
    try{
        JSON.parse(text);
        return true;
    }
    catch (error){
        return false;
    }
}

$("#xero_button").click(function() {
	$('input[type="checkbox"][name="selected\\[\\]"]:checked').map(function() { 
		
		if( $("#"+this.value).val() =='' ){
			$('.tdclass_'+this.value).html('<div class="loader"></div>');
			var xeroUrl  	= '<?php echo XERO_URL; ?>createContact.php?customer_id='+this.value;
			var customer_id = this.value;

			$.get(xeroUrl,function (response, status, error) {
				if(validJSON(response)){
		            var data  = JSON.parse(response);
		            if(data.status == 'success'){
		              $('.tdclass_'+customer_id).html('<span class="text">Success</span>');
		            }else{
		           		$('.tdclass_'+customer_id).html('Faild');
		            }
		        }else{
	           		$('.tdclass_'+customer_id).html('Faild');
		        }
			});
		}
	});
	// location.reload();
});

$(".xero" ).click(function() {
	var xero 	 = $(this);
	var customer_id = $(this).attr('data-customer_id');
    $('.tdclass_'+customer_id).html('<div class="loader"></div>');
	var xeroUrl  = '<?php echo XERO_URL; ?>createContact.php?customer_id='+customer_id;

    $.get(xeroUrl, 
        function (response, status, error) {
	        if(validJSON(response)){
	            var data = JSON.parse(response);
	            if(data.status == 'success'){
	              xero.removeClass('xero');
	              $('.tdclass_'+customer_id).html('<span class="text">Success</span>');
	            }else{
	              alert('Faild to send xero, Please try again.');
	              location.reload();
	            }
	        }else{
	            alert('Faild to send xero, Please try again.');
	            location.reload();
	        }
		});
 	});
</script>