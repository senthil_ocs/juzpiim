<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Customer Type List</h3>			
			<div class="page-bar"  style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                    <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:-15px !important;">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 0px -20px; padding:16px 0 0 0">
            <div class="caption" style=""> 
              <form method="post" name="report_filter" id="report_filter" action="">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">  
                      	<td>
              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
                        </td>                  
                        <td align="center" colspan="4">
                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>  
            <div class="tools" style="float:left;padding: 0 0 0 25px;">
               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
            </div>
            </div>
             <?php if (($_POST['filter_discount'] !="") || ($_POST['filter_taxcategory'] !='')){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>   
           <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: -23px !important; margin-left:15px; width: 15%;">
                <tbody>
                  <tr class="filter">                   	 
            		<td>
            			<select name="filter_discount" class="textbox" style="min-height: 35px; padding: 7px">		                    
                    <option value="">Select Discount</option>
                    <?php if(!empty($discounts)) { ?>
                    <?php foreach($discounts as $discount) { ?>
                    <?php if($discount['discount_id'] == $_POST['filter_discount']) { ?>
                    <option value="<?php echo $discount['discount_id']; ?>" selected="selected"><?php echo $discount['discount_name']; ?></option>
                    <?php } else { ?>
                    <option value="<?php echo $discount['discount_id']; ?>"><?php echo $discount['discount_name']; ?></option>
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                  </select>
                    </td>
                    <td>
                    	<select name="filter_taxcategory" id="filter_taxcategory" class="textbox" style="min-height: 35px; padding: 7px">
	                       <option value="">Select TaxCategory</option>	                        
                              <?php if(!empty($taxrates)) { ?>
                              <?php foreach($taxrates as $taxrate) { ?>
                              <?php if($taxrate['tax_rate_id'] == $_POST['filter_taxcategory']) { ?>
                              <option value="<?php echo $taxrate['tax_rate_id']; ?>" selected="selected">
                               <?php echo $taxrate['name']; ?>
                             </option>
                              <?php } else { ?>
                              <option value="<?php echo $taxrate['tax_rate_id']; ?>">
                               <?php echo $taxrate['name']; ?>
                             </option>
                              <?php } ?>
                              <?php } } ?>
                        </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left"><?php if ($sort == 'name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'First Name'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo 'First Name'; ?></a>
									<?php } ?>
								</td>								
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($customerstype) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($customerstype as $customer) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					 <td style="text-align: center;">
										<?php if ($customer['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $customer['customertype_id']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $customer['customertype_id']; ?>" />
										<?php } ?>
									</td>
									<td class="left"><?php echo $customer['name']; ?></td>
									<td class="left"><?php echo $customer['added_date']; ?></td>
									<td class="right">
										<?php foreach ($customer['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
</script>
<?php echo $footer; ?>