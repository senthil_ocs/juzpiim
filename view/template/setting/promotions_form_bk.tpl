<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Promotions</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="savepromo btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?>
                </button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                   		  <thead>
	                        <tr><th colspan="2">Basic Details</th></tr>
	                      </thead>
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Promotion Code<span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding"> 
                                <?php if($error_promotion_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter Code"></i>
                                <input type="text" name="promotion_code" value="<?php echo $promotion_code; ?>" class="textbox requiredborder" placeholder="Promotion Code">
                              </div>
                              <?php } ?>

                              <?php if(!$error_promotion_code) { ?>
                              <input type="text" name="promotion_code" value="<?php echo $promotion_code; ?>" class="textbox" placeholder="Promotion Code">
                              <?php } ?>
                              </td>
                            </tr>                             
		                      <tr>
		                          <td width="35%"><label for="name">Description<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                            <?php if($error_description) { ?>
		                            <div class="input-icon right">
		                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
		                            <input type="text" name="description" value="<?php echo $description; ?>" class="textbox requiredborder" placeholder="Description">
		                          </div>
		                          <?php } ?>

		                          <?php if(!$error_description) { ?>
		                          <input type="text" name="description" value="<?php echo $description; ?>" class="textbox" placeholder="Description">
		                          <?php } ?>
		                          </td>
		                        </tr>
			                       <tr>
		                          <td width="35%"><label for="name">From Date<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                                 <?php if($error_from_date) {  ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Date"></i>
		                              <input type="text" name="from_date" value="<?php echo $from_date; ?>" class="textbox date requiredborder" autocomplete="off" placeholder="From Date" id="from_date">
                                </div>
                                  <?php } ?>

                              <?php if(!$error_from_date) { ?>
                               <input type="text" name="from_date" value="<?php echo $from_date; ?>" class="textbox date" autocomplete="off" placeholder="From Date" id="from_date">
                              <?php } ?>
		                          </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">To Date<span class="required">*</span></label></td>
		                            <td width="65%" class="order-nopadding"> 
                                <?php if($error_to_date) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter Date"></i>
                                        <input type="text" name="to_date" value="<?php echo $to_date; ?>" class="textbox date requiredborder" autocomplete="off" placeholder="To Date" id="to_date">
                                         </div>
                                  <?php } ?>

                              <?php if(!$error_to_date) { ?>
                               <input type="text" name="to_date" value="<?php echo $to_date; ?>" class="textbox date" autocomplete="off" placeholder="To Date" id="to_date">
                              <?php } ?>                                  
                                </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">From Time<span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
                                <?php if($error_from_time) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Time"></i>
		                            <input type="text" name="from_time" value="<?php echo $from_time; ?>" class="textbox requiredborder" placeholder="From Time" id="from_time">
                                 </div>
                                  <?php } ?>

                              <?php if(!$error_from_time) { ?>
                               <input type="text" name="from_time" value="<?php echo $from_time; ?>" class="textbox" placeholder="From Time" id="from_time">
                              <?php } ?>
		                          </td>
		                        </tr> 
                              <tr>
                                <td width="35%"><label for="name">To Time<span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding">
                                   <?php if($error_to_time) { ?>
                                <div class="input-icon right">
                                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Time"></i>
                                        <input type="text" name="to_time" value="<?php echo $to_time; ?>" class="textbox requiredborder" placeholder="To Time" id="to_time">
                                         </div>
                                  <?php } ?>

                              <?php if(!$error_to_time) { ?>
                               <input type="text" name="to_time" value="<?php echo $to_time; ?>" class="textbox" placeholder="To Time" id="to_time">
                              <?php } ?>
                                </td>
                              </tr>
                              <tr>
                                <td width="35%"><label for="name">Promotion Percentage</label></td>
                                <td width="65%" class="order-nopadding">
                                   <input type="text" name="promotion_prescription" value="<?php echo $promotion_prescription; ?>" class="textbox" placeholder="Promotion Perc">
                                </td>
                              </tr>
                              <tr>
                                <td width="35%"><label for="name">Promotion Price</label></td>
                                <td width="65%" class="order-nopadding">
                                <input type="text" name="promotion_price" value="<?php echo $promotion_price; ?>" class="textbox " placeholder="Promotion Price">
                                </td>
                              </tr>
                        </tbody>
                      </table>
                     
                    </div>
                    <div class="col-md-6">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="3">Pick Days</th></tr>
                          </thead>
                          <tbody>
                          <tr>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Sunday'; ?>" class="" checked><label for="custom">Sunday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Monday'; ?>" class="" checked ><label for="custom">Monday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Thuesday'; ?>" class="" checked ><label for="custom">Thuesday</label>                        
                            </td>
                           </tr>                           
                            <tr>
                             <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Wednesday'; ?>" class="" checked><label for="custom">Wednesday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Thursday'; ?>" class="" checked><label for="custom">Thursday</label></td>
                            <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Friday'; ?>" class="" checked><label for="custom">Friday</label>                        
                            </td>
                            </tr>
                             <tr>
                             <td style="border-top:none;"><input type="checkbox" name="days[]" id="Cust_Display_Port" value="<?php echo 'Saturday'; ?>" class="" checked><label for="custom">Saturday</label></td>
                            <td style="border-top:none;">
                            <td style="border-top:none;">                       
                            </td> 
                            </tr> 
                        </tbody>
                      </table>
                   
                     </div>
                     <div class="col-md-6"></div>
                     <div class="col-md-6">
                        <table class="table orderlist invetorty-tab">
                          <thead>
                            <tr><th colspan="3">Select Sku</th></tr>
                          </thead>
                          <table id="customFields" class="table orderlist invetorty-tab">
                               <thead>
                                <tr>
                                  <td class="center">Sku</td>
                                   <td class="center">Name</td>
                                  <td class="center">Action</td>
                                </tr>
                                </thead>
                                  
                                 <tbody">
                                <tr>
                                    <td class="center"><input type="text" name="sku_0" id="sku_0" value="" class="textbox" onkeyup="getProductautoFill(this.value);">
                                    <!--  <input type="hidden" name="product_id" id="product_id_0" value=""> -->
                                     <div id="suggesstion-box_0" style="position:absolute"></div>
                                    </td>
                                     <td class="center"><input type="text" name="name_0" id="name_0" value="" class="textbox">
                                     </td>
                                     <td class="center"> 
                                      <a href="javascript:void(0);" class="addCF btn btn-zone btn-primary"  id="addMe"><i class="fa fa-plus"></i>Add
                                      </a>
                                      <a href="javascript:void(0);" class="remCF btn btn-zone btn-danger hidden">
                                      Remove</a>
                                    </td>
                                  </tr>
                                </tbody>
                                <!--  <?php $barcode_row = 1; ?>
                                <?php foreach ($product_barcodes as $product_barcode) { ?> 
                                 <tbody id="barcode-row<?php echo $barcode_row; ?>">
                                  <tr>
                                    <td class="center"><input type="text" name="product_barcode[<?php echo $barcode_row; ?>][barcode]" value="<?php echo $product_barcode['barcode']; ?>" class="textbox" required></td>
                                    <td class="center"><a onclick="$('#barcode-row<?php echo $barcode_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
                                  </tr>
                                </tbody> 
                                <?php $barcode_row++; ?>
                                <?php } ?>--->
                                

                             </table>                       
                         </table>
                   
                     </div>
            	<!-- 24-Feb-2016 Ends /!-->
           		</div>
        	</form>
         </div>
    </div>
</div>

<script type="text/javascript">

function getTypeList(v)
{
  $.ajax({
      url:"index.php?route=setting/terminal/getTypeDetails&token=<?php echo $token; ?>",
      type:"POST",
      data:{customer_type:v},     
      success:function(out){
          out = JSON.parse(out);          
          html = '<option value="">- Select Discount -</option>';
            if(out != '') {                    
                html += '<option value="' + out['discount'] + '"';
                  if (out['discount'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['discount_name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'discount\']').html(html);

          html = '<option value="">- Select Tax -</option>';
            if(out != '') {                    
                html += '<option value="' + out['taxcategory'] + '"';
                  if (out['taxcategory'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'salestax\']').html(html);      
      }
  });
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
  $('.date').datepicker({ dateFormat: 'yy-mm-dd' }).val()({
    changeMonth: true,
    changeYear: true,
    //yearRange: '1980:'+(new Date).getFullYear() 
  }); 
</script>
<script type="text/javascript">
   var row = 0;
  function selectedProduct(val) {
        //alert(row);
        var newVal = val.split("||");
        $("#product_id"+row).val(newVal[0]);
        $("#sku_"+row).val(newVal[1]);
        $("#name_"+row).val(newVal[2]);
        $("#suggesstion-box_"+row).hide();
    row++;
    }

  function getProductautoFill(sku) {
    if(sku.length<=3){
      return false;
    }
    var fdate = $("#from_date").val();
    var tdate = $("#to_date").val();
    var ftime = $("#from_time").val();
    var ttime = $("#to_time").val();

    if(!fdate && !tdate && !ftime && !ttime){
      alert("Please Enter Date And Time");
      return false;
    }
    var location ='SK308';
    var ajaxData = 'sku='+sku+'&location_code='+location+'&fdate='+fdate+'&tdate='+tdate+'&ftime='+ftime+'&ttime='+ttime;
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box_0").hide();
                clearPurchaseData();
                return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box_"+row).show();
                    $("#suggesstion-box_"+row).html(result);
                }else{
                    selectedProduct(result);
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}

    var barcode_row = <?php echo $barcode_row; ?>;
    $(document).on("click",".addCF",function() {
      console.log($(this).closest("tr").prev().children().find("#addMe"));    
      $(this).closest("tr").children().find("#addMe").remove();
      $("#customFields").append('<tr><td class="center"><input type="text" name="sku_' + barcode_row + '" id="sku_' + barcode_row + '" value="" class="textbox" onkeyup="getProductautoFill(this.value);"><div id="suggesstion-box_' + barcode_row + '" style="position:absolute"></div></td> <td class="center"><input type="text" name="name_' + barcode_row + '" id="name_' + barcode_row + '" value="" class="textbox"></td><td class="center"><a href="javascript:void(0);" class="addCF1 addCF btn btn-zone btn-primary"  id="addMe"><i class="fa fa-plus"></i>Add</a><a href="javascript:void(0);" class="remCF1 remCF btn btn-zone btn-danger hidden">Remove</a></td></tr>');
        barcode_row++;
        $('.addCF').addClass('hidden');
        $('.remCF').removeClass('hidden');
        $('.addCF1').removeClass('hidden');
      });
   $(document).ready(function(){
      $("#customFields").on('click','.remCF',function(){
          $(this).parent().parent().remove();
      });
   });
</script>