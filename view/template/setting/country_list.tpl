<?php echo $header; ?>
<!-- BEGIN HEADER -->
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php echo $sidebar; ?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
            
        
        
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                             <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div>
            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">
                	<div class="tabbable tabbable-custom boxless">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#" data-toggle="tab">Country</a>
                        </li>
                        <li>
                            <a href="<?php echo $zones; ?>">Zones</a>
                        </li>
                        <li>
                            <a href="<?php echo $geo_zone; ?>">Geo Zones</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                    <div class="tab-pane active">       
                    <div class="innerpage-listcontent-blocks">             
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                        	<tr class="heading">
                            	<td width="1"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                                <td>
                                	<?php if ($sort == 'name') { ?>
                                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                    <?php } ?>
                                </td>
                                <td>
                                	<?php if ($sort == 'iso_code_2') { ?>
                                        <a href="<?php echo $sort_iso_code_2; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_2; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_iso_code_2; ?>"><?php echo $column_iso_code_2; ?></a>
                                    <?php } ?>
                                </td>
                                <td>
                                	<?php if ($sort == 'iso_code_3') { ?>
                                        <a href="<?php echo $sort_iso_code_3; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_iso_code_3; ?></a>
                                    <?php } else { ?>
                                        <a href="<?php echo $sort_iso_code_3; ?>"><?php echo $column_iso_code_3; ?></a>
                                    <?php } ?>
                                </td>
                                <td class="right"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php if ($countries) { ?>
                            <?php $class = 'odd'; ?>
                            <?php foreach ($countries as $country) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                <tr class="<?php echo $class; ?>">
                                    <td>
                                    	<?php if ($country['selected']) { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                            <input type="checkbox" name="selected[]" value="<?php echo $country['country_id']; ?>" />
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $country['name']; ?></td>
                                    <td><?php echo $country['iso_code_2']; ?></td>
                                    <td><?php echo $country['iso_code_3']; ?></td>
                                    <td>
                                    	<?php foreach ($country['action'] as $action) { ?>
                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <?php } else { ?>
                            	<tr>
                                    <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
            </div>
        </div>
                </div>
            </div>
        	
        
            <!-- -->
		</div>
	</div>
    </form>
    
	<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->

<?php echo $footer; ?>