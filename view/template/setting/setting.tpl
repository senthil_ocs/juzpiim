<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
    <?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
    <div class="page-content-wrapper">
        <?php if ($error_warning) { ?>
                <div class="warning"><?php echo $error_warning; ?></div>
            <?php } ?>
        <div class="page-content">
            <h3 class="page-title">
            <?php echo $heading_title; ?> <small></small>
            </h3>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <?php if(!empty($breadcrumbs)){ 
                        foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } } ?>                    
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                        </div>    
                    </div>
                </div>
            </div>            
            <div class="row">
                 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div class="innerpage-listcontent-blocks">
                <div class="col-md-6">                  
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_general; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%"><?php echo $entry_admin_limit; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding">
                                        <?php if($error_admin_limit) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Username"></i>
                                    <?php } ?>
                                        <input type="text" name="config_admin_limit" id="config_admin_limit" class="textbox" value="<?php echo $config_admin_limit; ?>" style="height:36px;" />
                                     </div>   
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><?php echo $entry_next_sku_no; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                        <?php echo $next_sku_no; ?>
                                     </div>   
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_apply_tax; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr>
                                    <td width="35%">
                                    <?php echo $text_method_pos; ?>&nbsp;<span class="required">*</span>
                                    </td>
                                    <td width="65%" class="order-nopadding1">
                                        <select name="config_apply_tax" id="config_apply_tax" class="selectpicker">
                                            <option value="0" <?php if($config_apply_tax=='0'):?> selected="selected" <?php endif; ?>>Zero Tax</option>
                                            <option value="1" <?php if($config_apply_tax=='1'):?> selected="selected" <?php endif; ?>>Exclusive Tax</option>
                                            <option value="2" <?php if($config_apply_tax=='2'):?> selected="selected" <?php endif; ?>>Inclusive Tax</option>
                                        </select>
                                    </td>
                                </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $text_method_credit; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_apply_tax_credit_sales" id="config_apply_tax_credit_sales" class="selectpicker">
                                        <option value="0" <?php if($config_apply_tax_credit_sales=='0'):?> selected="selected" <?php endif; ?>>Zero Tax</option>
                                        <option value="1" <?php if($config_apply_tax_credit_sales=='1'):?> selected="selected" <?php endif; ?>>Exclusive Tax</option>
                                        <option value="2" <?php if($config_apply_tax_credit_sales=='2'):?> selected="selected" <?php endif; ?>>Inclusive Tax</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $text_method_purchase; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_apply_tax_purchase" id="config_apply_tax_purchase" class="selectpicker">
                                        <option value="0" <?php if($config_apply_tax_purchase=='0'):?> selected="selected" <?php endif; ?>>Zero Tax</option>
                                        <option value="1" <?php if($config_apply_tax_purchase=='1'):?> selected="selected" <?php endif; ?>>Exclusive Tax</option>
                                        <option value="2" <?php if($config_apply_tax_purchase=='2'):?> selected="selected" <?php endif; ?>>Inclusive Tax</option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>                    
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_language_cost; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_admin_language; ?>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                     <select name="config_admin_language" id="config_admin_language" class="selectpicker" >
                                        <?php
                                        if(!empty($languages)){
                                        foreach ($languages as $language) { ?>
                                        <option value="<?php echo $language['code']; ?>" <?php if ($language['code'] == $config_admin_language) { echo "selected"; } ?>><?php echo $language['name']; ?></option>
                                        <?php } } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_language; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_language" id="config_language" class="selectpicker">
                                    <?php foreach ($languages as $language) { ?>
                                    <?php if ($language['code'] == $config_language) { ?>
                                    <option value="<?php echo $language['code']; ?>" selected="selected"><?php echo $language['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $language['code']; ?>"><?php echo $language['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_currency; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_currency" id="config_currency" class="selectpicker">
                                    <?php foreach ($currencies as $currency) { ?>
                                    <?php if ($currency['code'] == $config_currency) { ?>
                                    <option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['title']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $currency['code']; ?>"><?php echo $currency['title']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2">Transaction No Prefix</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">Purchase</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_purchase_prefix" value="<?php echo $config_purchase_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Purchase Invoice</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_purchase_invoice_prefix" value="<?php echo $config_purchase_invoice_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Purchase return</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_purchase_return_prefix" value="<?php echo $config_purchase_return_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Purchase Service</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_purchase_prefix" value="<?php echo $config_sales_purchase_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Sales Quote</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_quote_prefix" value="<?php echo $config_sales_quote_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Sales</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_prefix" value="<?php echo $config_sales_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Sales Invoices</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_invoice_prefix" value="<?php echo $config_sales_invoice_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Sales Return</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_return_prefix" value="<?php echo $config_sales_return_prefix; ?>">
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Sales Serive</td>
                                <td width="65%" class="order-nopadding1">
                                     <input type="text" class="textbox" name="config_sales_service_prefix" value="<?php echo $config_sales_service_prefix; ?>">
                                </td>
                            </tr>
                        </tbody>
                    </table>  
                    <?php if($allowCronJob){ ?>
                        <table class="table orderlist">
                            <thead>
                                <tr>
                                    <th colspan="3">Cron</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td width="35%">Shopee</td>
                                    <td width="35%" class="order-nopadding1">
                                         <input type="text" name="shopee_sync" class="date" value="<?php echo date('d/m/Y'); ?>">
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary" id="shopee_sync">Sync</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%">Shopify</td>
                                    <td width="35%" class="order-nopadding1">
                                         <input type="text" class="date" value="<?php echo date('d/m/Y'); ?>">
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary" id="shopify_sync">Sync</a>
                                    </td>
                                </tr> 
                            </tbody>
                        </table>
                    <?php } ?>
                </div>                
                <div class="col-md-6">                
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_status; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_order_status; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_order_status_id" id="config_order_status_id" class="selectpicker">
                                    <?php foreach ($order_statuses as $order_status) { ?>
                                    <?php if ($order_status['order_status_id'] == $config_order_status_id) { ?>
                                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_stock_status; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_stock_status_id" id="config_stock_status_id" class="selectpicker">
                                    <?php foreach ($stock_statuses as $stock_status) { ?>
                                    <?php if ($stock_status['stock_status_id'] == $config_stock_status_id) { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2"><?php echo $text_others; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_weight_class ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_weight_class" id="config_weight_class" class="selectpicker">
                                    <?php foreach ($weight_classes as $weight_class) { ?>
                                    <?php if ($weight_class['unit'] == $config_weight_class) { ?>
                                    <option value="<?php echo $weight_class['unit']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                                    <?php } else { ?>
                                    <option value="<?php echo $weight_class['unit']; ?>"><?php echo $weight_class['title']; ?></option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_inventory_increment; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="config_inventory_increment" id="config_inventory_increment"   class="selectpicker" >
                                        <option value="0" <?php if(empty($config_inventory_increment)): ?> selected="selected" <?php endif; ?>>Auto</option>
                                        <option value="1" <?php if(!empty($config_inventory_increment)): ?> selected="selected" <?php endif; ?>>Manual</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_purchase_increment; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" name="config_purchase_increment" id="config_purchase_increment" class="textbox" value="<?php echo $config_purchase_increment; ?>" style="height:36px;" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_invoice_prefix; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <input type="text" name="config_invoice_prefix" id="config_invoice_prefix" class="textbox" value="<?php echo $config_invoice_prefix; ?>" style="height:36px;" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $text_average_cost_method; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                        <select name="config_average_cost_method" id="config_average_cost_method" class="selectpicker" >
                                            <option value="0" <?php if($config_average_cost_method=='0'):?> selected="selected" <?php endif; ?>>FIFO Method</option>
                                            <option value="1" <?php if($config_average_cost_method=='1'):?> selected="selected" <?php endif; ?>>Weighted Average</option>
                                        </select>
                                </td>
                            </tr>                          
                            <tr>
                                <td width="35%">
                                <?php echo $text_manage_inventory; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                        <select name="config_manage_inventory" id="config_manage_inventory" class="selectpicker" >
                                            <option value="0" <?php if($config_manage_inventory=='0'):?> selected="selected" <?php endif; ?>>No</option>
                                            <option value="1" <?php if($config_manage_inventory=='1'):?> selected="selected" <?php endif; ?>>Yes</option>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $text_vendor_update; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                        <select name="config_vendor_update" id="config_vendor_update" class="selectpicker" >
                                            <option value="0" <?php if($config_vendor_update=='0'):?> selected="selected" <?php endif; ?>>No</option>
                                            <option value="1" <?php if($config_vendor_update=='1'):?> selected="selected" <?php endif; ?>>Yes</option>
                                        </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $text_autosearch; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                        <input oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "1" min="1" name="config_autosearch_string" value="<?php echo $config_autosearch_string; ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Save Error Logs</td>
                                <td width="65%" class="order-nopadding1">
                                        <select name="config_error_log" id="config_error_log">
                                            <option value="1" <?php if($config_error_log == '1'){ echo 'Selected'; }?> >Yes</option>
                                            <option value="0" <?php if($config_error_log == '0'){ echo 'Selected'; }?>>No</option>
                                        </select>
                                </td>
                            </tr>

                        </tbody>
                    </table>      
                    <table class="table orderlist">
                        <thead>
                            <tr>
                                <th colspan="2">Networks Settings</th>
                                <th>XERO Post</th>
                                <th>Allow Default</th>
                                <th>Payment Method</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($networks as  $value) { ?>
                                <tr>
                                    <td width="25%"><?php echo $value['name']; ?></td>
                                    <td width="25%" class="order-nopadding1">
                                        <select name="network_status[<?php echo $value['id']; ?>]" id="network_status[<?php echo $value['id']; ?>]">
                                            <option value="Active" <?php echo $value['status'] == 'Active' ? 'Selected' : ''; ?>>Active</option>
                                            <option value="Inactive" <?php echo $value['status'] == 'Inactive' ? 'Selected' : ''; ?>>Inactive</option>
                                        </select>
                                    </td>
                                    <td width="25%">
                                        <?php $checked = ''; if($value['xero_post']=='1'){ $checked='checked'; } ?>
                                        <input type="checkbox" name="xero_post[<?php echo $value['id']; ?>]" value="1" <?php echo $checked; ?>>
                                    </td>
                                    <td width="25%" >
                                        <select name="allow_default[<?php echo $value['id']; ?>]" class="networks" id="allow_default_<?php echo $value['id']; ?>" onchange="validatePaymentMethod('<?php echo $value['id']; ?>');">
                                            <option value="1" <?php if($value['allow_default'] == '1'){ echo "selected"; } ?>>Yes</option>
                                            <option value="0" <?php if($value['allow_default'] == '0'){ echo "selected"; } ?>>No</option>
                                        </select>
                                    </td>
                                    <td width="25%">
                                        <div id="payment_method_td_<?php echo $value['id']; ?>">
                                            <select name="payment_method[<?php echo $value['id']; ?>]">
                                                <?php if(!empty($payment_methods)){
                                                    foreach ($payment_methods as $payment_method) { ?>
                                                        <option value="<?php echo $payment_method['code']; ?>" <?php if($payment_method['code'] == $value['payment_method']){ echo "selected"; }?>><?php echo $payment_method['name']; ?></option>
                                                <?php } } ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>  
                    </table>          
                </div>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
    function validatePaymentMethod(id){
        if($('#allow_default_'+id).val() == '1'){
            $('#payment_method_td_'+id).show();
        }else{
            $('#payment_method_td_'+id).hide();
        }
    }
    $(document).ready(function(){
        $(".networks").trigger('change',true);
    });

    $("#shopee_sync").click(function(){
        if($("input[name='shopee_sync']").val() !='' ){
            $.ajax({
                type: "POST",
                url: '<?php echo HTTP_SERVER; ?>index.php?route=setting/setting/shopee_sync&token=<?php echo $this->session->data["token"]; ?>',
                data: {from_date : $("input[name='shopee_sync']").val()},
                success: function(res) {
                    res = JSON.parse(res);
                    if(res.status){
                        location.reload();
                    }else{
                        alert(res.msg);
                    }
                }
            });
        }
    });

    $("#shopify_sync").click(function(){
        if($("input[name='shopify_sync']").val() !='' ){
            $.ajax({
                type: "POST",
                url: '<?php echo HTTP_SERVER; ?>index.php?route=setting/setting/shopify_sync&token=<?php echo $this->session->data["token"]; ?>',
                data: {from_date : $("input[name='shopify_sync']").val()},
                success: function(res) {
                    res = JSON.parse(res);
                    if(res.status){
                        location.reload();
                    }else{
                        alert(res.msg);
                    }
                }
            });
        }
    });
</script>