<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Terminal</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
                   		  <thead>
	                        <tr><th colspan="2">Basic Details</th></tr>
	                      </thead>
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Terminal Code <span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding"> 
                                <?php if($error_terminal_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                                <input type="text" name="terminal_code" value="<?php echo $terminal_code; ?>" class="textbox requiredborder" placeholder="Terminal Code">
                              </div>
                              <?php } ?>

                              <?php if(!$error_terminal_code) { ?>
                              <input type="text" name="terminal_code" value="<?php echo $terminal_code; ?>" class="textbox" placeholder="Terminal Code">
                              <?php } ?>
                              </td>
                            </tr>                             
		                      <tr>
		                          <td width="35%"><label for="name">Terminal Name <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                            <?php if($error_terminal_name) { ?>
		                            <div class="input-icon right">
		                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
		                            <input type="text" name="terminal_name" value="<?php echo $terminal_name; ?>" class="textbox requiredborder" placeholder="Terminal Name">
		                          </div>
		                          <?php } ?>

		                          <?php if(!$error_terminal_name) { ?>
		                          <input type="text" name="terminal_name" value="<?php echo $terminal_name; ?>" class="textbox" placeholder="Terminal Name">
		                          <?php } ?>
		                          </td>
		                        </tr>
			                       <tr>
		                          <td width="35%"><label for="name">System Name</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="system_name" value="<?php echo $system_name; ?>" class="textbox" placeholder="System Name">
		                          </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Cash Machine</label></td>
		                            <td width="65%" class="order-nopadding1">
                                    <select name="cash_machine" class="selectpicker selectdropdown">                                   
                                        <option value="0" <?php if ($cash_machine == 0) { ?> selected="selected" <?php } ?>><?php echo 'Yes'; ?></option>
                                        <option value="1" <?php if ($cash_machine == 1) { ?> selected="selected" <?php } ?>><?php echo 'No'; ?></option>
                                    </select>
                                </td>
		                        </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Cash Machine Port</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="cash_machine_port" value="<?php echo $cash_machine_port; ?>" class="textbox" placeholder="Cash Machine Port">
		                          </td>
		                        </tr> 
                              <tr>
                                <td width="35%"><label for="name">Coin Machine</label></td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="coin_machine" class="selectpicker selectdropdown">                                   
                                        <option value="0" <?php if ($coin_machine == 0) { ?> selected="selected" <?php } ?>><?php echo 'Yes'; ?></option>
                                        <option value="1" <?php if ($coin_machine == 1) { ?> selected="selected" <?php } ?>><?php echo 'No'; ?></option>
                                    </select>
                                </td>
                              </tr>
                              <tr>
                                <td width="35%"><label for="name">Coin Machine Port</label></td>
                                <td width="65%" class="order-nopadding1">
                                    <input type="text" name="coin_machine_port" value="<?php echo $coin_machine_port; ?>" class="textbox" placeholder="Coin Machine Port">
                                </td>
                              </tr>
                        </tbody>
                      </table>
                     
                    </div>
                    <div class="col-md-6">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="2">Custom Details</th></tr>
                          </thead>
                          <tbody>
              <tr>
                          <td width="35%"><label for="custom">Custom Display</label></td>
                          <td width="65%" class="order-nopadding">
                                    <select name="Cust_display" class="selectpicker selectdropdown">                                   
                                        <option value="1" <?php if ($Cust_display == 1) { ?> selected="selected" <?php } ?>><?php echo 'Yes'; ?></option>
                                        <option value="0" <?php if ($Cust_display == 0) { ?> selected="selected" <?php } ?>><?php echo 'No'; ?></option>
                                    </select>
                </td>
              </tr>                           
                            <tr>
                              <td width="35%"><label for="custom">Custom Display Port</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="Cust_Display_Port" id="Cust_Display_Port" value="<?php echo $Cust_Display_Port; ?>" class="textbox" placeholder="Custom Display Port">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="custom">Custom Display Type</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="Cust_Display_Type" id="Cust_Display_Type" value="<?php echo $Cust_Display_Type; ?>" class="textbox" placeholder="Custom Display Type">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="custom">Custom Display Message</label></td>
                              <td width="65%" class="order-nopadding">
                          <input type="text" name="Cust_Display_Msg1" value="<?php echo $Cust_Display_Msg1; ?>" class="textbox" placeholder="Message 1">
                        <input type="text" name="Cust_Display_Msg2" value="<?php echo $Cust_Display_Msg2; ?>" class="textbox" placeholder="Message 2">
                              </td>
                            </tr>
                        </tbody>
                      </table>
                   
                     </div>
                     <div class="col-md-6"></div>
                     <div class="col-md-6">
                        <table class="table orderlist">
                          <thead>
                            <tr><th colspan="2">Others</th></tr>
                          </thead>
                          <tbody>
              <tr>
                          <td width="35%"><label for="custom">Speaker Enabled</label></td>
                          <td width="65%" class="order-nopadding">
                                    <select name="speaker_enabled" class="selectpicker selectdropdown">                                   
                                        <option value="1" <?php if ($speaker_enabled == 1) { ?> selected="selected" <?php } ?>><?php echo 'Yes'; ?></option>
                                        <option value="0" <?php if ($speaker_enabled == 0) { ?> selected="selected" <?php } ?>><?php echo 'No'; ?></option>
                                    </select>
                            </td>
                            </tr>                           
                            <tr>
                              <td width="35%"><label for="custom">Weighing Scale Enabled </label></td>
                              <td width="65%" class="order-nopadding">
                                    <select name="weighingscale_enabled" class="selectpicker selectdropdown">                                   
                                        <option value="1" <?php if ($weighingscale_enabled == 1) { ?> selected="selected" <?php } ?>><?php echo 'Yes'; ?></option>
                                        <option value="0" <?php if ($weighingscale_enabled == 0) { ?> selected="selected" <?php } ?>><?php echo 'No'; ?></option>
                                    </select>
                            </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="custom">Weighing Scale Port</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="weighingscale_port" id="weighingscale_port" value="<?php echo $weighingscale_port; ?>" class="textbox" placeholder="Weighing Scale Port">
                              </td>
                            </tr>
                            <?php if($this->request->get['route'] == 'setting/terminal/insert') { ?>
                            <tr display="none"></tr>
                            <?php } else {?> 
                             <tr display="block">
                              <td width="35%"><label for="custom">Last Sync Date</label></td>
                              <td width="65%" class="order-nopadding">
                          <input type="text" name="last_sync_date" class="textbox"  placeholder="Last Sync Date" value="<?php echo $last_sync_date; ?>" readonly="readonly">
                        <!--  -->
                              </td>
                            </tr>
                            <?php }?>
                        </tbody>
                      </table>
                   
                     </div>
            	<!-- 24-Feb-2016 Ends /!-->
           		</div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
function getTypeList(v)
{
  $.ajax({
      url:"index.php?route=setting/terminal/getTypeDetails&token=<?php echo $token; ?>",
      type:"POST",
      data:{customer_type:v},     
      success:function(out){
          out = JSON.parse(out);          
          html = '<option value="">- Select Discount -</option>';
            if(out != '') {                    
                html += '<option value="' + out['discount'] + '"';
                  if (out['discount'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['discount_name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'discount\']').html(html);

          html = '<option value="">- Select Tax -</option>';
            if(out != '') {                    
                html += '<option value="' + out['taxcategory'] + '"';
                  if (out['taxcategory'] == '') {
                     html += ' selected="selected"';
                  }
                    html += '>' + out['name'] + '</option>';
                  } else {
                    html += '<option value="0" selected="selected"> --- None --- </option>';
            }
                $('select[name=\'salestax\']').html(html);      
      }
  });
}
</script>
<?php echo $footer; ?>
<script type="text/javascript">
  $('.date').datepicker({ dateFormat: 'dd/mm/yy' }).val()({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:'+(new Date).getFullYear() 
  });
</script>