<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Terms</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>        
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_name; ?>&nbsp;<span class="required" aria-required="true">* </span>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if($error_termsname) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
							 		
                                   <input type="text" name="terms_name" id="terms_name" value="<?php echo $terms_name; ?>" class="textbox requiredborder" placeholder="Enter terms name">
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_termsname) { ?>
                               		<input type="text" name="terms_name" id="terms_name" value="<?php echo $terms_name; ?>" class="textbox" placeholder="Enter terms name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_code; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="80%" class="order-nopadding">
                        		<?php if ($error_termscode) { ?>
									<div class="input-icon right">
									<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter code name"></i>							 	
								<input type="text" name="terms_code" id="terms_code" value="<?php echo $terms_code; ?>" class="textbox requiredborder" placeholder="Enter first name" <?php if($terms_code != '') { ?> readonly <?php } ?>>
								</div>
								<?php } ?>
								<?php if (!$error_termscode) { ?>
								<input type="text" name="terms_code" id="terms_code" value="<?php echo $terms_code; ?>" class="textbox" placeholder="Enter code name" <?php if($terms_code != '') { ?> readonly <?php } ?>>
								<?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<?php echo $entry_days; ?>
                                </td>
                                <td width="80%" class="order-nopadding">
                                 <input type="text" name="noof_days" id="noof_days" value="<?php echo $noof_days; ?>" class="textbox" placeholder="Enter the day">
                                </td>                                
                            </tr> 
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%">
								<select name="status" class="selectpicker selectdropdown">
									<?php if ($status) { ?>
										<option value="0"><?php echo $text_disabled; ?></option>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<?php } else { ?>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
										<option value="1"><?php echo $text_enabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>