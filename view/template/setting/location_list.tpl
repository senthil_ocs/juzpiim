<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Location List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?>
            		</div>    
                   </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left"><?php if ($sort == 'location_name') { ?>
									<a href="<?php echo $location_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
									<?php } else { ?>
									<a href="<?php echo $location_name; ?>"><?php echo $column_name; ?></a>
									<?php } ?>
								</td>								
								<td class="left"><?php echo $column_location_code; ?></td>
								<td class="left"><?php echo $column_type; ?></td>
								<td class="left"><?php if ($sort == 'country') { ?>
								<a href="<?php echo $country; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country; ?></a>
								<?php } else { ?>
									<a href="<?php echo $country; ?>"><?php echo $column_country; ?></a>
									<?php } ?>
								</td>
								<td class="left"><?php echo $column_phone; ?></td>
								<td class="right"><?php echo $column_action; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($locations) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($locations as $location) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
						 <td style="text-align: center;">
							<?php if ($location['selected']) { ?>
							<input type="checkbox" name="selected[]" value="<?php echo $location['location_id']; ?>" checked="checked">
							<?php } else { ?>
							<input type="checkbox" name="selected[]" value="<?php echo $location['location_id']; ?>">
							<?php } ?>
						</td>
						<td class="left"><?php echo $location['name']; ?></td>									
						<td class="left"><?php echo $location['code']; ?></td>
						<td class="left"><?php echo $location['type']; ?></td>
						<td class="left"><?php echo $location['country']; ?></td>
						<td class="left"><?php echo $location['phone']; ?></td>
						<td class="right">
							<?php foreach ($location['action'] as $action) { ?>
								[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
							<?php } ?>
						</td>
					</tr>
						<?php } ?>
					<?php } else { ?>
					<tr>
						<td align="center" colspan="6"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>