<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Touch Menu Master</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-12">
                   		<table class="table orderlist">
                        <tbody>                          
                            <tr>
                              <td width="35%"><label for="name">Touch Menu Code <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_touch_menu_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Touch Menu Code"></i>
                                <input type="text" name="touch_menu_code" value="<?php echo $touch_menu_code; ?>" class="textbox requiredborder" placeholder="Touch Menu Code">
                              </div>
                              <?php } ?>
                              <?php if(!$error_touch_menu_code) { ?>
                              <input type="text" name="touch_menu_code" value="<?php echo $touch_menu_code; ?>" class="textbox" placeholder="Touch Menu Code"
                              <?php if($editForm) {?> readonly <?php }?>>
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Touch Menu Description</label></td>
                              <td width="65%" class="order-nopadding">
                              <textarea name="touch_menu_description" class="textbox" placeholder="Touch Menu Description"><?php echo $touch_menu_description; ?></textarea>
                              </td>
                            </tr>
                            <?php $statusAry = array("0"=>"Disabled ","1"=>"Enabled"); ?>
                            <tr>
                              <td width="20%"><label for="status">Status</label></td>
                               <td width="80%">
                                <select name="touch_menu_status" class="input-text">
                                    <?php foreach($statusAry as $key=>$value): ?>
                                      <option value="<?php echo $key; ?>" <?php if($key==$status):?> selected="selected" <?php endif; ?>>
                                        <?php echo $value; ?>
                                      </option>
                                    <?php endforeach; ?>
                                </select>
                              </td> 
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Sort Code <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_sort_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Sort Code"></i>
                                <input type="text" name="sort_code" value="<?php echo $sort_code; ?>" class="textbox requiredborder" placeholder="Sort Code">
                              </div>
                              <?php } ?>
                              <?php if(!$error_sort_code) { ?>
                              <input type="text" name="sort_code" value="<?php echo $sort_code; ?>" class="textbox" placeholder="Sort Code" <?php if($editForm) {?> readonly <?php }?>>
                              <?php } ?>
                              </td>
                            </tr>
                        </tbody>
                    </table>
                   	</div>
                   	
                   </div>
           		</div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>