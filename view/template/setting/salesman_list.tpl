<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Driver List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
		                        </td>
		                   		<td>
		              				<input type="text" placeholder='Phone' name="filter_phone" value="<?php echo $_POST['filter_phone'] ; ?>" class="textbox"/>
		                   		</td>                   
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            </div> 
	              <?php if (($_POST['filter_date_from'] !="") || ($_POST['filter_date_to'] != "") || ($_POST['filter_Type'] != "")){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
            
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td>Driver Id</td>
								<td class="left"><?php if ($sort == 'name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo 'Name'; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo 'Name'; ?></a>
									<?php } ?>
								</td>
								<td class="left"><?php echo 'Phone No'; ?></td>
								<td class="left"><?php echo 'Route'; ?></td>
								<td class="left"><?php echo 'Email'; ?></td>
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($salesmanList) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($salesmanList as $value) { ?>

					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
									<td><?php echo $value['id']; ?></td>
									<td class="left"><?php echo $value['name']; ?></td>
									<td class="left"><?php echo $value['phone']; ?></td>
									<td class="left"><?php echo $value['route']; ?></td>
									<td class="left"><?php echo $value['email']; ?></td>
									<td class="left"><?php echo date('d/m/Y h:i a',strtotime($value['addedon'])); ?></td>
									<td class="right">
										<?php foreach ($value['action'] as $action) { ?>
											<!-- <a href="<?php /*echo $action['href'];*/ ?>"><i class="fa fa-edit" data-toggle="tooltip" title="Modify"></i></a> -->
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
											[ <a href="<?php echo $action['hrefSchedule'];?> "><?php echo $action['textSchedule']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport() {
	document.report_filter.submit();
}
</script>
<?php echo $footer; ?>