<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Promotion List</h3>			
			<div class="page-bar " style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a> &nbsp; &nbsp;
               <!--  <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a> -->
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
				<!-- Filter Starts  -->
			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
        		<div class="portlet bordernone" style="margin-bottom:0px !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $filter_date_from; ?>" class="textbox date" autocomplete="off">
		                        </td>                       
		                        <td>
		                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $filter_date_to; ?>" class="textbox date" autocomplete="off">
		                        </td> 
		                        <td>
		                           <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
		                            <?php if(count($Tolocations)>=2){?>
		                            <?php }?>
		                            <?php
		                               if(!empty($Tolocations)){
		                                    foreach($Tolocations as $value){ ?>
		                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

		                            <?php  } } ?> 
		                        </select>
		                        </td>
		                        <td>
		              				<input type="text" placeholder='SKU' id="sku" name="filter_sku" value="<?php echo $filter_sku; ?>" class="textbox" autocomplete="off">
		                        </td> 

		                                   
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            
		            </div> 
	            
            <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
            
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div style="clear:both"></div>
			
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-warning fade in setting-warning"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>	

		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="POST" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								
								<!-- <td class="left">Terminal Code</td>
								<td class="left"><?php echo 'Terminal Name'; ?></td>
								<td class="left"><?php echo 'System Name'; ?></td>
								<td class="left"><?php echo 'Cash Machine'; ?></td>
								<td class="left"><?php echo 'Cash Machine Port'; ?></td>
								<td class="left"><?php echo 'Coin Machine'; ?></td>
								<td class="left"><?php echo 'Coin Machine Port'; ?></td>
								
								<td class="left"><?php echo 'Added Date'; ?></td>
								<td class="right"><?php echo 'Action'; ?></td> -->
								<!-- <td class="left">Terminal Code</td> -->
								<td class="left"><?php echo 'Promotion Code'; ?></td>
								<td class="left"><?php echo 'Description'; ?></td>
								<td class="left"><?php echo 'From Date'; ?></td>
								<td class="left"><?php echo 'To Date'; ?></td>
								<td class="left"><?php echo 'From Time'; ?></td>
								<td class="left"><?php echo 'To Time'; ?></td>
								
								<td class="left"><?php echo 'Promotion Percentage'; ?></td>
								<td class="left"><?php echo 'Promotion Price'; ?></td>
								 <td class="right"><?php echo 'Action'; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($terminals) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($terminals as $value) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $class; ?>">
					
									<td class="left"><?php echo $value['PromotionCode']; ?></td>
									<td class="left"><?php echo $value['Description']; ?></td>
									<td class="left"><?php echo $value['FromDate']; ?></td>
									<td class="left"><?php echo $value['ToDate']; ?></td>
									<td class="left"><?php echo $value['FromTime'].':00'; ?></td>
									<td class="left"><?php echo $value['ToTime'].':00'; ?></td>
									<td class="left"><?php echo ($value['PromotionPerc'] =='.00')?'':$value['PromotionPerc']; ?></td>
									<td class="left"><?php echo ($value['PromotionPrice'] =='.00')?'':$value['PromotionPrice']; ?></td>
									  <td class="right">
										<?php foreach ($value['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo 'Views'; ?></a> ]
											[ <a href="<?php echo $action['edit']; ?>"><?php echo 'Edit'; ?></a> ]
											[ <a class="promodelete" data-href="<?php echo $action['delete']; ?>"><?php echo 'Delete'; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td align="center" colspan="10"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport() {
	document.report_filter.submit();
}

$('.promodelete').click(function (event) {
    if (confirm('Are you sure you want to move this?')) {
        var url = $(this).attr('data-href');
        $.ajax({
            url: url,
            type: "GET",
            success: function () {
              location.reload();
            }
        });
    }
});

</script>
<?php echo $footer; ?>