<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
    <?php echo $sidebar; ?>
    <div class="page-content-wrapper">
        <div class="page-content">
        <h3 class="page-title">Order Details</h3>          
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
                </ul>
                <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">              
                    <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-reply"></i><span> Back</span></button></a>
                    </div>    
                </div>
                </div>                                    
            </div>
            <div style="clear:both"></div>                           
        <div class="row">           
            <div class="col-md-12">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
                <table class="table orderlist statusstock order_info">
                        <tbody>
                            <tr>                                
                                <td class="label"><label><?php echo $text_order_id; ?></label></td>
                                <td class="value">#<?php echo $order_id; ?></td>
                            </tr>
                            <tr>
                                <td class="label"><label>Invoice Number </label></td>
                                <td class="value"><?php echo $invoice_no; ?></td>
                            </tr>
                            <tr>
                            <td class="label"><label>Customer </label></td>
                            <td class="value"><?php echo $customer; ?></td>
                            </tr>
                            <tr>
                                <td class="label"><label><?php echo $text_order_status; ?></label></td>
                                <td class="value"><?php echo $order_status; ?></td>
                            </tr>
                            <tr>
                                <td class="label"><label>Payment Method </label></td>
                                <td class="value">
                                <?php 
                                $payment_name = '';
                                foreach ($payment_method as $paymode) {
                                    $payment_name .= $paymode['payment_name'].',';
                                }
                                echo rtrim($payment_name, ",");
                                ?>
                                </td>
                            </tr>
                            <?php if($silp_number): ?>
                                <tr>
                                    <td class="label"><label>Slip number </label></td>
                                    <td class="value"><?php echo $silp_number; ?></td>
                                </tr>
                            <?php endif; ?>                                            
                            <?php if($cash_amount): ?>
                                <tr>
                                    <td class="label"><label>Cash Amount </label></td>
                                    <td class="value"><?php echo $cash_amount; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if($card_amount): ?>
                                <tr>
                                    <td class="label"><label>Card Amount </label></td>
                                    <td class="value"><?php echo $card_amount; ?></td>
                                </tr>
                            <?php endif; ?>
                            <?php if($balance_amount): ?>
                                <tr>
                                    <td class="label"><label>Balance Amount </label></td>
                                    <td class="value"><?php echo $balance_amount; ?></td>
                                </tr>
                            <?php endif; ?>                                            
                            <tr>
                                <td class="label"><label>Order Place Date </label></td>
                                <td class="value"><?php echo $order_date; ?></td>
                            </tr>
                            <tr>
                                <td class="label"><label>Order Place IP </label></td>
                                <td class="value"><?php echo $order_ip; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="tab-product" class="vtabs-content">
                            <table cellspacing="0" class="table orderlist statusstock">
                                <thead>
                                    <tr>
                                        <td align="center" class="order_head"><?php echo $column_product; ?></td>
                                        <td align="center" class="order_head"><?php echo $column_quantity; ?></td>
                                        <td align="center" class="order_head"><?php echo $column_price; ?></td>
                                        <td align="center" class="order_head"><?php echo $column_total; ?></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($products as $product) { ?>
                                    <tr>
                                        <!-- <td align="center"><a href="<?php //echo $product['href']; ?>"><?php //echo $product['name']; ?></a></td> -->
                                        <td align="center"><?php echo $product['name']; ?></td>
                                        <td align="center"><?php echo $product['quantity']; ?></td>
                                        <td align="center"><?php echo $product['price']; ?></td>
                                        <td align="center"><?php echo $product['total']; ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                                <tbody id="totals">
                                <?php foreach ($totals as $totals) { ?>
                                    <tr>
                                        <td colspan="3" align="right" style="font-weight:bolder"><?php echo $totals['title']; ?> </td>
                                        <td class="amount" align="center"><?php echo $totals['text']; ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
</div>
  <script type="text/javascript">
	$('#tabs a').tabs(); 
  </script>
<?php echo $footer; ?>