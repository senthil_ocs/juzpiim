<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Order List</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
				<div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  <a onclick="$('#form').attr('action', '<?php echo $invoice; ?>'); $('#form').attr('target', '_blank'); $('#form').submit();" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-print"></i> <?php echo $button_invoice; ?></a>               
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                	  </div>    
                    </div>
                </div> 				           						
			</div>
			<!-- SEARCH CODE-->			         
      <div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
            <div class="page-bar portlet-title invent_search">
                <div class="caption">                  
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px;">
                    <tbody>
                      <tr class="filter">                      	
                        <td><input type="text" placeholder='Order Id' name="filter_order_id" value="<?php echo $filter_order_id; ?>" class="textbox"></td>
                        <td><input type="text" placeholder='Customer' name="filter_customer" value="<?php echo $filter_customer; ?>" class="textbox"></td>
                        <td class="filter_value"><input placeholder="Total" type="text" name="filter_total" value="<?php echo $filter_total; ?>" class="textbox" /></td>                                       
                      <td><button style="min-height: 36px; border-radius:0 !important;width:100%" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                    </tr>
                        </tbody>
                    </table>
                        </div>                        
                    <div class="tools" style="float:left;padding: 0 0 0 35px;">
                        <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
                    </div>
                    </div>
                    <?php if($filter_date_added !="" || $filter_date_modified != "" || $filter_order_status_id != ''){ 
                      $showHide  = 'display:block;';
                    } else {
                      $showHide  = 'display:none;';
                    } ?>       
                    <div class="page-bar portlet-body bgnone" style="display: none;background-color: #eee !important; margin: 0px 0 0 -20px;<?php echo $showHide; ?>">
                     <table class="table orderlist statusstock" style="margin-bottom: -12px !important; margin-left:25px; width: 68%;">
                    <tbody>
                      <tr class="filter">
                        <td><input type="text" placeholder="Added Date" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date textbox">
                        </td>
                      <td><input type="text" placeholder="Modified Date" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" size="12" class="date textbox">
                     </td>
                      <td>
                       <select name="filter_order_status_id" class="textbox" style="min-height: 35px; padding: 7px;">
                          <option value="*">Select Status</option>
                          	<?php foreach ($order_statuses as $order_status) { ?>
                          	<?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
                          <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                          <?php } else { ?>
                          	<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                          <?php } ?>
                          <?php } ?>
                        </select>
                        </td>
                     </tr>
                        </tbody>
                    </table>
                    </div>
                     <div style="clear:both; margin: 0 0 15px 0;">
                    </div>
                </div>
              </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>           
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form" style="padding:20px 0 0 0">
              <div class="innerpage-listcontent-blocks">
                <table class="table orderlist statusstock">
                  <thead>
                    <tr class="heading">
                      <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                      <td class="center"><?php if ($sort == 'o.order_id') { ?>
                        <a href="<?php echo $sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_order_id; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_order; ?>"><?php echo $column_order_id; ?></a>
                        <?php } ?></td>
                        <td class="center">Cashier</td>                      
                      <td class="center"><?php if ($sort == 'status') { ?>
                        <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?></td>
                      <td class="center"><?php if ($sort == 'o.total') { ?>
                        <a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
                        <?php } ?></td>
                      <td class="center"><?php if ($sort == 'o.date_added') { ?>
                        <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
                        <?php } ?></td>
                      <td class="left"><?php if ($sort == 'o.date_modified') { ?>
                        <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
                        <?php } ?></td>
                      <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($orders) { ?>
                       <?php $class = 'odd'; ?>
                     <?php foreach ($orders as $order) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <tr class="<?php echo $class; ?>">
                     <td style="text-align: center;"><?php if ($order['selected']) { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $order['order_id']; ?>" />
                        <?php } ?></td>
                      <td class="center"><?php echo $order['order_id']; ?></td>
                      <td class="center"><?php echo $order['customer']; ?></td>
                      <td class="center"><?php echo $order['status']; ?></td>
                      <td class="right"><?php echo $order['total']; ?></td>
                      <td class="center"><?php echo $order['date_added']; ?></td>
                      <td class="center"><?php echo $order['date_modified']; ?></td>
                      <td class="right"><?php foreach ($order['action'] as $action) { ?>
                        [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                        <?php } ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td align="center" colspan="8"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </form>
          <div class="pagination"><?php echo $pagination; ?></div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=sale/order&token=<?php echo $token; ?>';
	
	var filter_order_id = $('input[name=\'filter_order_id\']').attr('value');
	
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	
	var filter_customer = $('input[name=\'filter_customer\']').attr('value');
	
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	
	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').attr('value');
	
	if (filter_order_status_id != '*') {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}	

	var filter_total = $('input[name=\'filter_total\']').attr('value');

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	var filter_date_modified = $('input[name=\'filter_date_modified\']').attr('value');
	
	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}
				
	location = url;
}
//--></script>  
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

$('input[name=\'filter_customer\']').catcomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						category: item.customer_group,
						label: item.name,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_customer\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<?php echo $footer; ?>