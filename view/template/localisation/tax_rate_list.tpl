<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Tax Rates List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">	

			<div class="col-md-12">
				<div class="tabbable tabbable-custom boxless">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#tab_1" data-toggle="tab">Tax Rates</a>
						</li>
						<li>
							<a href="<?php echo $tax_classlink; ?>" >Tax Classes</a>
						</li>
					</ul>
					<div class="tab-content">
					<div class="tab-pane active" >
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
					  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
					  <td class="left"><?php if ($sort == 'tr.name') { ?>
						<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?></td>
					  <td class="right"><?php if ($sort == 'tr.rate') { ?>
						<a href="<?php echo $sort_rate; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_rate; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_rate; ?>"><?php echo $column_rate; ?></a>
						<?php } ?></td>
					  <td class="left"><?php if ($sort == 'tr.type') { ?>
						<a href="<?php echo $sort_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_type; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_type; ?>"><?php echo $column_type; ?></a>
						<?php } ?></td> 
					  <td class="left"><?php if ($sort == 'gz.name') { ?>
						<a href="<?php echo $sort_geo_zone; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_geo_zone; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_geo_zone; ?>"><?php echo $column_geo_zone; ?></a>
						<?php } ?></td>                         
					  <td class="left"><?php if ($sort == 'tr.date_added') { ?>
						<a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
						<?php } ?></td>      
					  <td class="left"><?php if ($sort == 'tr.date_modified') { ?>
						<a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
						<?php } ?></td>                                                                                                                
					  <td class="right"><?php echo $column_action; ?></td>
					</tr>
                        </thead>
                        <tbody>
                           <?php if ($tax_rates) { ?>
                           <?php $class = 'odd'; ?>
					<?php foreach ($tax_rates as $tax_rate) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;"><?php if ($tax_rate['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $tax_rate['tax_rate_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $tax_rate['tax_rate_id']; ?>" />
		
						<?php } ?></td>
					  <td class="left"><?php echo $tax_rate['name']; ?></td>
					  <td class="right"><?php echo $tax_rate['rate']; ?></td>
					  <td class="left"><?php echo $tax_rate['type']; ?></td>
					  <td class="left"><?php echo $tax_rate['geo_zone']; ?></td>
					  <td class="left"><?php echo $tax_rate['date_added']; ?></td>
					  <td class="left"><?php echo $tax_rate['date_modified']; ?></td>
					  <td class="right"><?php foreach ($tax_rate['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="9"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
            </div>
            </div>
            </div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>