<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>	
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?>
			</h3>
            <div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i><?php echo $text_general; ?>
					</div>   
				</div>     
			<div class="portlet-body bgnone">

				<a href="<?php echo $tax_rate; ?>" class="icon-btn">
					<i class="fa fa-bar-chart-o"></i>
					<div>
						 <?php echo $text_tax_rate; ?>
					</div>
					
				</a>
				<a href="<?php echo $tax_class; ?>" class="icon-btn">
					<i class="fa fa-bank"></i>
					<div>
						 <?php echo $tax_classes; ?>
					</div>
				</a>				
			</div>
		</div>
		</div>
	</div>	
</div>
<?php echo $footer; ?>