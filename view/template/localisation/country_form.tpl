<?php echo $header; ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  <div class="box">
  	<div class="content">
		<div class="form-information">
			<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
				</div>
			</div>
			<div class="form-new">
				<table cellspacing="0" class="form-list">
					<tbody>
						<tr>
							<td class="label">
								<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text validate[required]" placeholder="Enter your name">
								 <?php if ($error_name) { ?>
									<span class="error"><?php echo $error_name; ?></span>
								 <?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="iso_code_2"><?php echo $entry_iso_code_2; ?></label>
							</td>
							<td class="value">
								<input type="text" name="iso_code_2" id="iso_code_2" value="<?php echo $iso_code_2; ?>" class="input-text" placeholder="Enter your iso code 2">
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="iso_code_2"><?php echo $entry_iso_code_3; ?></label>
							</td>
							<td class="value">
								<input type="text" name="iso_code_3" id="iso_code_3" value="<?php echo $iso_code_3; ?>" class="input-text" placeholder="Enter your iso code3">
							</td>
						</tr>
						
						<tr>
							<td class="label">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td class="value">
								<select name="status" class="input-text">
									<?php if ($status) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>
</form>
<?php echo $footer; ?>