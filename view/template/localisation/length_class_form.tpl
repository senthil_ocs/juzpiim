<?php echo $header; ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  <div class="box">
  	<div class="content">
		<div class="form-information">
			<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
				</div>
			</div>
			<div class="form-new">
				<table cellspacing="0" class="form-list">
					<tbody>
						<tr>
							<td class="label">
								<label for="title"><?php echo $entry_title; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<?php foreach ($languages as $language) { ?>
									<input type="text" name="length_class_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($length_class_description[$language['language_id']]) ? $length_class_description[$language['language_id']]['title'] : ''; ?>" class="input-text validate[required]" />
									<?php if (isset($error_title[$language['language_id']])) { ?>
										<span class="error"><?php echo $error_title[$language['language_id']]; ?></span><br />
									<?php } ?>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="unit"><?php echo $entry_unit; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<?php foreach ($languages as $language) { ?>
									<input type="text" name="length_class_description[<?php echo $language['language_id']; ?>][unit]" value="<?php echo isset($length_class_description[$language['language_id']]) ? $length_class_description[$language['language_id']]['unit'] : ''; ?>" class="input-text validate[required]" />
									<?php if (isset($error_unit[$language['language_id']])) { ?>
										<span class="error"><?php echo $error_unit[$language['language_id']]; ?></span><br />
									<?php } ?>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="value"><?php echo $entry_value; ?></label>
							</td>
							<td class="value">
								<input type="text" name="value" value="<?php echo $value; ?>" class="input-text" />
								<span class="help">Set to 1.00000 if this is your default length.</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>
</form>
<?php echo $footer; ?>