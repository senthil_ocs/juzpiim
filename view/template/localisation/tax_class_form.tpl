<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Tax Class</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>
            <div class="row">          
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="title"><?php echo $entry_title; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_title) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Title"></i>
										<input type="text" name="title" id="title" value="<?php echo $title; ?>" class="textbox requiredborder" placeholder="Enter your title">                            
                               		</div>
                               		<?php } ?>
                               		<?php if (!$error_title) { ?>
                               		<input type="text" name="title" id="title" value="<?php echo $title; ?>" class="textbox" placeholder="Enter your title">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                <label for="description"><?php echo $entry_description; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_description) { ?>
                                	<div class="input-icon right">
									<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Description"></i>
									<input type="text" name="description" id="description" value="<?php echo $description; ?>" class="textbox requiredborder" placeholder="Enter your description">
									</div>
									<?php } ?>
									<?php if (!$error_description) { ?>
									<input type="text" name="description" id="description" value="<?php echo $description; ?>" class="textbox" placeholder="Enter your description">
									<?php } ?>
                                </td>                                
                            </tr>                                                  
                        </tbody>
                    </table>                   
                    <table id="tax-rule" class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
							<td class="left"><?php echo $entry_rate; ?></td>
							<td class="left"><?php echo $entry_based; ?></td>
							<td class="left"><?php echo $entry_priority; ?></td>
							<td></td>
						</tr>
                        </thead>
                       <?php $tax_rule_row = 0; ?>
					<?php foreach ($tax_rules as $tax_rule) { ?>
					<tbody id="tax-rule-row<?php echo $tax_rule_row; ?>">
						<tr>
							<td class="left"><select name="tax_rule[<?php echo $tax_rule_row; ?>][tax_rate_id]" class="selectpicker selectdropdown">
							<?php foreach ($tax_rates as $tax_rate) { ?>
							<?php  if ($tax_rate['tax_rate_id'] == $tax_rule['tax_rate_id']) { ?>
							<option value="<?php echo $tax_rate['tax_rate_id']; ?>" selected="selected"><?php echo $tax_rate['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $tax_rate['tax_rate_id']; ?>"><?php echo $tax_rate['name']; ?></option>
							<?php } ?>
							<?php } ?>
							</select></td>
							<td class="left">
							<select name="tax_rule[<?php echo $tax_rule_row; ?>][based]" class="selectpicker selectdropdown">				
							<?php  if ($tax_rule['based'] == 'store') { ?>
							<option value="store" selected="selected"><?php echo $text_store; ?></option>
							<?php } else { ?>
							<option value="store"><?php echo $text_store; ?></option>
							<?php } ?>                                    
							</select></td>
							<td class="left"><input type="text" name="tax_rule[<?php echo $tax_rule_row; ?>][priority]" value="<?php echo $tax_rule['priority']; ?>" size="1" class="textbox" /></td>
							<td class="left"><a onclick="$('#tax-rule-row<?php echo $tax_rule_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i>   <?php echo $button_remove; ?></a></td>
						</tr>
					</tbody>
					<?php $tax_rule_row++; ?>
					<?php } ?>
					<tfoot>
						<tr>
							<td colspan="3"></td>
							<td class="left"><a onclick="addRule();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_rule; ?></a></td>
						</tr>
					</tfoot>
                    </table>                    
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
var tax_rule_row = <?php echo $tax_rule_row; ?>;

function addRule() {
	html  = '<tbody id="tax-rule-row' + tax_rule_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select name="tax_rule[' + tax_rule_row + '][tax_rate_id]" class="selectpicker selectdropdown">';
    <?php foreach ($tax_rates as $tax_rate) { ?>
    html += '      <option value="<?php echo $tax_rate['tax_rate_id']; ?>"><?php echo addslashes($tax_rate['name']); ?></option>';
    <?php } ?>
    html += '    </select></td>';
	html += '    <td class="left"><select name="tax_rule[' + tax_rule_row + '][based]" class="input-text">';
    html += '      <option value="store"><?php echo $text_store; ?></option>';
    html += '    </select></td>';
	html += '    <td class="left"><input type="text" name="tax_rule[' + tax_rule_row + '][priority]" value="" size="1" class="textbox" /></td>';
	html += '    <td class="left"><a onclick="$(\'#tax-rule-row' + tax_rule_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#tax-rule > tfoot').before(html);
	
	tax_rule_row++;
}
</script>
<?php echo $footer; ?>