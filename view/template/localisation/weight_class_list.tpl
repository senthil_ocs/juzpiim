<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Weight Class List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo $home; ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>						
						<a href="<?php echo $settings; ?>">Settings</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Weight Class List</a>						
					</li>					
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
							  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
							  <td class="left"><?php if ($sort == 'title') { ?>
								<a href="<?php echo $sort_title; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_title; ?>"><?php echo $column_title; ?></a>
								<?php } ?></td>
							  <td class="left"><?php if ($sort == 'unit') { ?>
								<a href="<?php echo $sort_unit; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_unit; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_unit; ?>"><?php echo $column_unit; ?></a>
								<?php } ?></td>
							  <td class="right"><?php if ($sort == 'value') { ?>
								<a href="<?php echo $sort_value; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_value; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_value; ?>"><?php echo $column_value; ?></a>
								<?php } ?></td>
							  <td class="right"><?php echo $column_action; ?></td>
							</tr>
                        </thead>
                        <tbody>
                          <?php if ($weight_classes) { ?>
                          <?php $class = 'odd'; ?>
					<?php foreach ($weight_classes as $weight_class) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;"><?php if ($weight_class['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $weight_class['weight_class_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $weight_class['weight_class_id']; ?>" />
						<?php } ?></td>
					  <td class="left"><?php echo $weight_class['title']; ?></td>
					  <td class="left"><?php echo $weight_class['unit']; ?></td>
					  <td class="right"><?php echo $weight_class['value']; ?></td>
					  <td class="right"><?php foreach ($weight_class['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>