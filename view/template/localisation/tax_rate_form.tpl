<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Tax Rates</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
            <div style="clear:both"></div>          
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
										<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">                              
                               		</div>
                               		<?php } ?>
                               		<?php if (!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="rate"><?php echo $entry_rate; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_rate) { ?>
                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Rate"></i>
                        			<input type="text" name="rate" id="rate" value="<?php echo $rate; ?>" class="textbox requiredborder" placeholder="Enter your rate">
									</div>
									<?php } ?>
									<?php if (!$error_rate) { ?>
									<input type="text" name="rate" id="rate" value="<?php echo $rate; ?>" class="textbox" placeholder="Enter your rate">
									<?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="type"><?php echo $entry_type; ?></label>
                                </td>
                                <td width="80%">			
                                  <select name="type" class="selectpicker selectdropdown">
									<?php if ($type == 'P') { ?>
										<option value="P" selected="selected"><?php echo $text_percent; ?></option>
									<?php } else { ?>
										<option value="P"><?php echo $text_percent; ?></option>
									<?php } ?>
									<?php if ($type == 'F') { ?>
										<option value="F" selected="selected"><?php echo $text_amount; ?></option>
									<?php } else { ?>
										<option value="F"><?php echo $text_amount; ?></option>
									<?php } ?>
								</select>                           
                                </td>                                
                            </tr> 
                            <tr>
                               <td width="20%">
                                	<label for="geo_zone"><?php echo $entry_geo_zone; ?></label>
                                </td>
                                <td width="80%">			
                                  <select name="geo_zone_id" class="selectpicker selectdropdown">
									<?php foreach ($geo_zones as $geo_zone) { ?>
										<?php  if ($geo_zone['geo_zone_id'] == $geo_zone_id) { ?>
											<option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
										<?php } else { ?>
											<option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
										<?php } ?>
									<?php } ?>
								</select>                            
                                </td>                               
                            </tr>                           
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>