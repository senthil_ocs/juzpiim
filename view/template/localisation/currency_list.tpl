<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title"><?php echo $heading_title; ?></h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
					  <td width="1" style="text-align: center;">
                      	<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
                      </td>
					  <td class="left"><?php if ($sort == 'title') { ?>
						<a href="<?php echo $sort_title; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_title; ?>"><?php echo $column_title; ?></a>
						<?php } ?></td>
					  <td class="left"><?php if ($sort == 'code') { ?>
						<a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
						<?php } ?></td>
					  <td class="right"><?php if ($sort == 'value') { ?>
						<a href="<?php echo $sort_value; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_value; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_value; ?>"><?php echo $column_value; ?></a>
						<?php } ?></td>
					  <td class="left"><?php if ($sort == 'date_modified') { ?>
						<a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_modified; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_date_modified; ?>"><?php echo $column_date_modified; ?></a>
						<?php } ?></td>
					  <td class="right"><?php echo $column_action; ?></td>
					</tr>
                        </thead>
                        <tbody>
                            <?php if ($currencies) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($currencies as $currency) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;">
                      	<?php if (!empty($currency['default_field'])) { ?>
                        &nbsp;
					  	<?php } elseif ($currency['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $currency['currency_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $currency['currency_id']; ?>" />
						<?php } ?>
                      </td>
					  <td class="left"><?php echo $currency['title']; ?></td>
					  <td class="left"><?php echo $currency['code']; ?></td>
					  <td class="right"><?php echo $currency['value']; ?></td>
					  <td class="left"><?php echo $currency['date_modified']; ?></td>
					  <td class="right">
                      	<?php if (!empty($currency['default_field'])) { ?>
                        	Default
                        <?php } else { ?>
							<?php foreach ($currency['action'] as $action) { ?>
                                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                            <?php } ?>
						<?php } ?>
                      </td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>