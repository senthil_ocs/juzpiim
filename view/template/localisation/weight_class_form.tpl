<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Weight Class</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo $settings; ?>">Settings</a>
                        <i class="fa fa-angle-right"></i>                      
                    </li>
                    <li>
                        <a href="<?php echo $weight_class; ?>">Weight Class List</a>
                        <i class="fa fa-angle-right"></i>                                             
                    </li>
                    <li>
                        <a href="#">Weight Class</a>                                                 
                    </li>                   
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>           
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="title"><?php echo $entry_title; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php foreach ($languages as $language) { ?>									
									<?php if (isset($error_title[$language['language_id']])) { ?>
									<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Title"></i>
										<input type="text" name="weight_class_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($weight_class_description[$language['language_id']]) ? $weight_class_description[$language['language_id']]['title'] : ''; ?>" class="textbox requiredborder" placeholder="Enter Title">
									</div>
									<?php } ?>
                                	<?php if (!isset($error_title[$language['language_id']])) { ?>
	                                	<input type="text" name="weight_class_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($weight_class_description[$language['language_id']]) ? $weight_class_description[$language['language_id']]['title'] : ''; ?>" class="textbox" placeholder="Enter Title">
                               		<?php } ?>
                               		<?php } ?>                              					 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="unit"><?php echo $entry_unit; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php foreach ($languages as $language) { ?>									
									<?php if (isset($error_unit[$language['language_id']])) { ?>
									<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Unit"></i>
										<input type="text" name="weight_class_description[<?php echo $language['language_id']; ?>][unit]" value="<?php echo isset($weight_class_description[$language['language_id']]) ? $weight_class_description[$language['language_id']]['unit'] : ''; ?>" class="textbox requiredborder" placeholder="Enter Unit">
									</div>
									<?php } ?>
									<?php if (!isset($error_unit[$language['language_id']])) { ?>
										<input type="text" name="weight_class_description[<?php echo $language['language_id']; ?>][unit]" value="<?php echo isset($weight_class_description[$language['language_id']]) ? $weight_class_description[$language['language_id']]['unit'] : ''; ?>" class="textbox" placeholder="Enter Unit">
									<?php } ?>
								<?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="value"><?php echo $entry_value; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                <div class="input-icon right">
                                    <i style="color: orange;" data-html="true" class="fa fa-info tooltips" data-container="body" data-original-title="Set to 1.00000 if this is your default weight."></i>			
                                  <input style="padding-left:10px !important;" type="text" name="value" value="<?php echo $value; ?>" class="textbox" placeholder="Enter value">
								  </div>                    
                                </td>                                
                            </tr>                                                    
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>