<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title"><?php echo $heading_title; ?></h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
            <div style="clear:both"></div>         
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="title"><?php echo $entry_title; ?><span class="required" aria-required="true">* </span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if($error_title) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter title"></i>
										<input type="text" name="title" value="<?php echo $title; ?>" class="textbox requiredborder" oninput="this.value = this.value.toUpperCase()" placeholder="Enter title">                                   
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_title) { ?>
                               		<input type="text" name="title" value="<?php echo $title; ?>" class="textbox" placeholder="Enter title" oninput="this.value = this.value.toUpperCase()">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="code"><?php echo $entry_code; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_code) { ?>
                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                        			<input type="text" name="code" value="<?php echo $code; ?>" class="textbox requiredborder" oninput="this.value = this.value.toUpperCase()" placeholder="Enter code">				
									</div>
									<?php } ?>
									<?php if (!$error_code) { ?>
                                    <div class="input-icon right">
                                         <a href='http://www.xe.com/iso4217.php' target='_blank'><i style="color: orange;" data-html="true" class="fa fa-info tooltips" data-container="body" data-original-title="Do not change if this is your default currency. Must be valid ISO Code"></i></a>
									<input style="padding-left:10px !important;" type="text" name="code" value="<?php echo $code; ?>" oninput="this.value = this.value.toUpperCase()" class="textbox" placeholder="Enter code">									
                                  </div>
									<?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="symbol-left"><?php echo $entry_symbol_left; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">			
                                   <input type="text" name="symbol_left" value="<?php echo $symbol_left; ?>" class="textbox" placeholder="Enter symbol left">                              
                                </td>                                
                            </tr> 
                            <tr>
                               <td width="20%">
                                	<label for="symbol-right"><?php echo $entry_symbol_right; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">			
                                   <input type="text" name="symbol_right" value="<?php echo $symbol_right; ?>" class="textbox" placeholder="Enter symbol right">                              
                                </td>                               
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="decimal"><?php echo $entry_decimal_place; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="decimal_place" value="<?php echo $decimal_place; ?>" class="textbox" placeholder="Enter decimal place">                               
                                </td>                                
                            </tr>
                             <tr>
                                <td width="20%">
                                	<label for="password"><?php echo $entry_value; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <div class="input-icon right">
                                    <i style="color: orange;" data-html="true" class="fa fa-info tooltips" data-container="body" data-original-title="Set to 1.00000 if this is your default currency."></i>
                                	<input style="padding-left:10px !important;" type="text" name="value" value="<?php echo $value; ?>" class="textbox" placeholder="Enter value">									
                                    </div>                   
                                </td>                                
                            </tr>                             
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%">
								<select name="status" class="selectpicker selectdropdown">
									<?php if ($status) { ?>
									<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>