<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
				<?php echo $heading_title; ?><small></small>
			</h3>
            <div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption" >
						<i class=""></i> CURRENT SALE
					</div>   
				</div> 
				<div class="portlet-body bgnone">
					<a href="#" class="icon-btn">
						<i class="fa fa-mail-forward"></i>
						<div>Continue Sale</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-star"></i>
						<div>Special Order</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-umbrella"></i>
						<div>Email Receipt</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-ticket"></i>
						<div>Refund</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-usd"></i>
						<div>New Sale</div>
					</a>
				</div>
			</div>
       		<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class=""></i> CURRENT REGISTER IS 'REGISTER 1'
					</div>   
				</div> 
				<p class="sub-para">The register is where you do sales and refunds.You are currently using 'Register 1' at 'MANIKANDAN'.</p>
				<div class="portlet-body bgnone">
					<a href="#" class="icon-btn">
						<i class="fa fa-exchange"></i>
						<div>Switch Register</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-power-off"></i>
						<div>Special Order</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-minus-square"></i>
						<div>Payout / Drop</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-plus-square"></i>
						<div>Add Amount</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-list-alt"></i>
						<div>Customer Display</div>
					</a>
				</div>
			</div>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class=""></i> RECENT ACTIVITY
					</div>   
				</div> 
				<h4 style=" font-size:14px; font-weight: 500 !important;margin-left:15px;">In Progress</h4>
				<div class="portlet-body bgnone" style="margin:-11px 5px ! important;">
					<a href="#" class="icon-btn-small"><i class="fa fa-usd"></i></a>
					<a href="#" class="icon-btn-small"><i class="fa fa-print"></i></a>
					<a href="#" class="icon-btn-small"><i class="fa fa-minus-square"></i></a>
					<label> #43 - Mani Sekhar, $108.25</label>
					<h4 style=" font-size: 14px;font-weight: 500 !important;">Today's Transaction</h4>
					<a href="#" style=" color: #646464 !important;font-size:13px;">View All</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<style type="text/css">
.sub-para{color: #666;font-size: 13px; margin: 15px 0px 0px 15px;}
.icon-btn-small{background-color:#eeeeee !important;background-image:none !important;border:1px solid #ddd;border-radius: 4px;box-shadow:none !important;color:#646464 !important;cursor:pointer;display:inline-block !important;filter: none !important; height: auto;margin:0 3px 10px 0;min-height:44px;padding:13px 12px;position:relative;text-align:center;text-shadow:none !important; transition: all 0.3s ease 0s !important;width: 40px;}
.cap-size{font-size: 14px ! important;}
</style>