<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cloud POS -<?php echo $heading_title; ?></title>
<link rel="stylesheet" type="text/css" href="view/stylesheet/style.css"/>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css"/>
</head>
<body class="invoice-page" style="background:none;">
<div id="content">
  <div class="box">
    <div class="content">
      <div class="form-list-container">
        <div class="print-master-report">
          <table class="print_company_info">
            <tr>
              <td class="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
            </tr>
            <tr>
              <td class="center" colspan="2"><?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
              </td>
            </tr>
            <tr>
              <td class="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
            </tr>
            <tr>
              <td class="center" colspan="2"><?php echo $companyInfo['country']; ?></td>
            </tr>
            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
            <tr <?php echo $contact; ?>>
              <td class="center"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span class="print_fax"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
              <td class="right"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
            </tr>
          </table>
          <div class="heading">
            <h1><?php echo $heading_title; ?></h1>
          </div>
          <table class="list">
            <thead>
              <tr>
                <td class="center">Inventory Code</td>
                <td class="center">Product Name</td>
                <td class="center">Selling Price</td>
                <td class="center">Promo Price</td>
                <td class="center">Promotion Period</td>
                <td class="center">Qty On Hand</td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($products)) { ?>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="center"><?php echo $product['sku']; ?></td>
                <td class="center"><?php echo $product['name']; ?></td>
                <td class="left"><?php echo $product['price']; ?></td>
                <td class="left"><?php if ($product['special']) { ?>
                  <span style="color: #b00;"><?php echo $product['special']; ?></span>
                  <?php } ?></td>
                <td class="center"><?php echo $product['date_start']." - ".$product['date_end']; ?></td>
                <td class="right"><?php if ($product['quantity'] <= 0) { ?>
                  <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                  <?php } elseif ($product['quantity'] <= 5) { ?>
                  <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                  <?php } else { ?>
                  <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td class="center print_page_button" colspan="6"><a class="btn btn-zone" onClick="window.print();">Print</a> <a href="<?php echo $back; ?>" class="btn btn-zone">Back</a> </td>
              </tr>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
