<?php echo $header; ?>

<div id="content">
  <div class="box">
    <div class="content">
      <div class="form-list-container">
        <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
        <?php } ?>
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <div class="heading">
            <h1><?php echo $heading_title; ?></h1>
            <div class="action">
              <button class="btn btn-action master_print_button" type="submit" title="Save"><?php echo $button_print; ?></button>
            </div>
          </div>
          
          <table class="top_search">
            <tbody>
              <tr class="filter">
                <td><?php echo $text_promotion_period; ?></td>
                <td><?php echo $text_from_date; ?></td>
                <td><input type="text" name="date_start" value="<?php echo $date_start; ?>" class="date input-text" /></td>
                <td><?php echo $text_to_date; ?></td>
                <td><input type="text" name="date_end" value="<?php echo $date_end; ?>" class="date input-text" /></td>
                <td align="center"><button type="button" onclick="searchReport();" class="btn btn-zone"><?php echo $button_search; ?></button></td>
              </tr>
            </tbody>
          </table>
          
          <table class="list">
            <thead>
              <tr>
                <td class="center"><?php echo $column_invt_code; ?></td>
                <td class="center"><?php echo $column_name; ?></td>
                <td class="left"><?php echo $column_selling_price; ?></td>
                <td class="left"><?php echo $column_promo_price; ?></td>
                <td class="center"><?php echo $column_promotion_period; ?></td>
                <td class="right"><?php echo $column_quantity_on_hand; ?></td>
              </tr>
            </thead>
            <tbody>
              <?php if ($products) { ?>
              <?php foreach ($products as $product) { ?>
              <tr>
                <td class="center"><?php echo $product['sku']; ?></td>
                <td class="center"><?php echo $product['name']; ?></td>
                <td class="left"><?php echo $product['price']; ?></td>
                <td class="left"><?php if ($product['special']) { ?>
                  <span style="color: #b00;"><?php echo $product['special']; ?></span>
                  <?php } ?></td>
                <td class="center"><?php echo $product['date_start']." - ".$product['date_end']; ?></td>
                <td class="right"><?php if ($product['quantity'] <= 0) { ?>
                  <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                  <?php } elseif ($product['quantity'] <= 5) { ?>
                  <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                  <?php } else { ?>
                  <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                  <?php } ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </form>
        <div class="pagination"><?php echo $pagination; ?></div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
function searchReport() {
	url = 'index.php?route=merchadising/promotions_report&token=<?php echo $token; ?>';
	
	var date_start = $('input[name=\'date_start\']').attr('value');
	if (date_start) {
		url += '&date_start=' + encodeURIComponent(date_start);
	}

	var date_end = $('input[name=\'date_end\']').attr('value');
	if (date_end) {
		url += '&date_end=' + encodeURIComponent(date_end);
	}

	location = url;
}
//--></script>

<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>

<?php echo $footer; ?>