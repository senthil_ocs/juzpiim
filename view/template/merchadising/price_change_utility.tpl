<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="content">
		<div class="form-list-container">
			<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="success"><?php echo $success; ?></div>
			<?php } ?>
			<form action="<?php echo $price_utility; ?>" method="post" enctype="multipart/form-data" id="form">
            <div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
                    <select name="change_utility" id="change_utility" class="input-text change_utility_box" >
                    	<option value="">Select</option>
                        <option value="status">Change Status</option>
                        <option value="price">Change Price</option>
                        <option value="quantity">Change Quantity</option>
                    </select>
                    <a onclick="$('form').submit();" class="btn btn-action"><?php echo $button_submit; ?></a>
				</div>
			</div>
			
            <table class="list">
              <thead>
                <tr>
                  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                  <td class="center"><?php echo $column_image; ?></td>
                  <td class="center"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="left"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                  <td class="right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>
                  <td class="left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <tr class="filter">
                  <td></td>
                  <td></td>
                  <td><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" class="input-text" /></td>
                  <td align="left"><input type="text" name="filter_price" value="<?php echo $filter_price; ?>" size="8" class="input-text" /></td>
                  <td align="right"><input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" style="text-align: right;" class="input-text"  /></td>
                  <td><select name="filter_status" class="input-text" >
                      <option value="*"></option>
                      <?php if ($filter_status) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <?php } ?>
                      <?php if (!is_null($filter_status) && !$filter_status) { ?>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select></td>
                  <td align="right"><button type="button" onclick="filter();" class="btn btn-zone"><?php echo $button_filter; ?></button></td>
                </tr>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td style="text-align: center;"><?php if ($product['selected']) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                  <td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
                  <td class="center"><?php echo $product['name']; ?></td>
                  
                  <td class="left"><?php if ($product['special']) { ?>
                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                    <span style="color: #b00;"><?php echo $product['special']; ?></span>
                    <?php } else { ?>
                    <?php echo $product['price']; ?>
                    <?php } ?></td>
                  <td class="right"><?php if ($product['quantity'] <= 0) { ?>
                    <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                    <?php } elseif ($product['quantity'] <= 5) { ?>
                    <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                    <?php } else { ?>
                    <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                    <?php } ?></td>
                  <td class="left"><?php echo $product['status']; ?></td>
                  <td class="right">&nbsp;</td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="center" colspan="10"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
			</form>
		  <div class="pagination"><?php echo $pagination; ?></div>
		</div>
	  </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=merchadising/price_change_utility&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_inventory_id = $('input[name=\'filter_inventory_id\']').attr('value');
	
	if (filter_inventory_id) {
		url += '&filter_inventory_id=' + encodeURIComponent(filter_inventory_id);
	}
	
	var filter_barcodes = $('input[name=\'filter_barcodes\']').attr('value');
	
	if (filter_barcodes) {
		url += '&filter_barcodes=' + encodeURIComponent(filter_barcodes);
	}
	
	var filter_price = $('input[name=\'filter_price\']').attr('value');
	
	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}
	
	var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');
	
	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}	

	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
0//--></script> 
<?php echo $footer; ?>