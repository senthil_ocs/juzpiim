<?php echo $header; ?>

<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  
  <input type="hidden" name="change_utility" id="change_utility" value="<?php echo $change_utility ?>" />
  <input type="hidden" name="product_ids" id="product_ids" value="<?php echo $product_ids ?>" />
  
  <div class="box">
    <div class="content">
      <div class="form-information">
        <div class="heading product-form">
          <h1><?php echo $heading_title; ?></h1>
          <div class="action">
            <button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
            <button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
          </div>
        </div>
        <div class="form-new product-form">
          <?php if ($error_warning) { ?>
          <div class="warning" style="width:94%"><?php echo $error_warning; ?></div>
          <?php } ?>
          <table cellspacing="0" class="tab-list">
            <tbody>
              <tr>
                <td class="value"><div id="tab-general">
                    <table cellspacing="0" class="form-list">
                      <tbody>
                    
                        <?php if ($change_utility == 'status') { ?>
                        <tr>
                            <td class="label"><label for="status"><?php echo $entry_change_status; ?></label></td>
                            <td class="value"><select name="status" class="input-text">
                            <?php if ($status) { ?>
                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                            <option value="0"><?php echo $text_disabled; ?></option>
                            <?php } else { ?>
                            <option value="1"><?php echo $text_enabled; ?></option>
                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                            <?php } ?>
                            </select></td>
                        </tr>
                        <?php } ?>
                        
                        <?php if ($change_utility == 'price') { ?>
                        <tr>
                            <td class="label"><label for="price"><?php echo $entry_change_price; ?><span class="required">*</span></label></td>
                            <td class="value">
                                <input type="text" name="price" value="<?php echo $price; ?>" class="input-text validate[required]"/>
                                <?php if ($error_price) { ?>
                                    <span class="error"><?php echo $error_price; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                        
                        <?php if ($change_utility == 'quantity') { ?>
                        <tr>
                            <td class="label"><label for="quantity"><?php echo $entry_change_quantity; ?></label></td>
                            <td class="value"><input type="text" name="quantity" value="<?php echo $quantity; ?>" size="2" class="input-text validate[required,custom[integer],min[0]]" /></td>
                        </tr>
                        <?php } ?>
                                                                       
                      </tbody>
                    </table>
                  </div></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</form>
<?php echo $footer; ?>