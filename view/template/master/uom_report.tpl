<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">Uom Master List</h3>
        	<div class="page-bar">
			<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
			              	<a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
	              			<a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
	            		</div>   
                    </div>
                </div>
			</div>            
            <div class="row">            	
                 <div style="clear:both"></div>
                <div class="col-md-12">                	      
                    <div class="innerpage-listcontent-blocks">             
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                      <tr class="heading">					   
					  <td class="left"><?php echo $column_code; ?></td>
					  <td class="left"><?php echo $column_name; ?></td>
					  <td class="left"><?php echo $column_description; ?></td>					 
					</tr>
                        </thead>
                        <tbody>
                        	<?php if ($uoms) { ?>
                        	<?php $class = 'odd'; ?>
					<?php foreach ($uoms as $uom) { ?>					
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">					
					  <td class="left"><?php echo $uom['code']; ?></td>
					  <td class="left"><?php echo $uom['name']; ?></td>
					  <td class="left"><?php echo $uom['description']; ?></td>					  
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="3"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
               </div>
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>