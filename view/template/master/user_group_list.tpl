<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">User Group Report</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				</ul>
				<div class="page-toolbar">
	                <div class="btn-group pull-right">
			            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
			              	<a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
	              			<a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
	            		</div>    
	          		</div>
                </div>               						
			</div>
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>			 
		<div class="row">			
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="table orderlist statusstock">
                        <thead>
                             <!--<tr>
								<td class="left"><?php if ($sort == 'name') { ?>   
								  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
								  <?php } else { ?>
								  <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
								  <?php } ?></td>
								<td class="right"><?php echo $column_action; ?></td>!-->
								<tr  class="heading">
				                <td class="center"><?php echo $column_name; ?></td>
							</tr>

            			</thead>
                        <tbody>
                             <?php if ($user_groups) { ?>
                             <?php $class = 'odd'; ?>
							  <?php foreach ($user_groups as $user_group) { ?>
							  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							  <tr class="<?php echo $class; ?>">
								<td class="left"><?php echo $user_group['name']; ?></td>
							  </tr>
							  <?php } ?>
							  <?php } else { ?>
							  <tr>
								<td class="center" colspan="3"><?php echo $text_no_results; ?></td>
							  </tr>
							  <?php } ?>
                        </tbody>
                    </table>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>