<?php echo $header; ?>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
  <div class="box">
  	<div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      <div class="buttons">
	  	<button class="button" type="submit" title="Save"><span><?php echo $button_save; ?></span></button>
		<a onclick="backAction()" class="button"><span><?php echo $button_cancel; ?></span></a>
	</div>
    </div>
	<div class="content">
		<div class="form-information">
			<div class="form-new">
				<div class="fields">
					<label for="code"><?php echo $entry_code; ?><span class="required">*</span></label>
					<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="input-text required-entry" placeholder="Enter your code">
					<?php if ($error_code) { ?>
						<span class="error"><?php echo $error_code; ?></span>
					<?php } ?>
				</div>
				<div class="fields">
					<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
					<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text required-entry" placeholder="Enter your name">
					<?php if ($error_name) { ?>
						<span class="error"><?php echo $error_name; ?></span>
					<?php } ?>
				</div>
				<div class="fields">
					<label for="method"><?php echo $entry_method; ?><span class="required">*</span></label>
					<select name="method" class="input-text validate-select">
						<option value="1" <?php if($method=='1'):?> selected="selected"<?php endif; ?>>Add</option>
						<option value="-1" <?php if($method=='-1'):?> selected="selected" <?php endif; ?> >Deduct</option>
					</select>
					
				</div>
				<div class="fields">
					<label for="position"><?php echo $entry_position; ?></label>
					<input type="text" name="position" value="<?php echo $position; ?>" class="input-text" placeholder="Enter position" />
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//<![CDATA[
			var newForm = new VarienForm('form', true);
		//]]>
	</script>
  </div>
</form>
<?php echo $footer; ?>