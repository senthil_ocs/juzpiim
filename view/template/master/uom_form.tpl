<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Uom Master</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
           
	<div style="clear:both"></div>
            <div class="row">
                <div class="col-md-12"> 
                 <?php if ($error_warning) { ?>
                    <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                    <button type="button" class="close" data-dismiss="alert"></button>
                 </div>
                <?php } ?>
                </div>
            <div style="clear:both"></div>         
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_code; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_code) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
										<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox requiredborder" placeholder="Enter your code" <?php if($code != '') { ?> readonly <?php } ?>>                      
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_code) { ?>
                               		<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox" placeholder="Enter your code" <?php if($edit_form != '') { ?> readonly <?php } ?>>
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
									<?php if ($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
										<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">                      
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>	
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="description"><?php echo $entry_description; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                               <textarea type="text" name="description" row="5" class="textbox"><?php echo $description; ?></textarea>                            
                                </td>                                
                            </tr>							
                        </tbody>
                    </table>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>