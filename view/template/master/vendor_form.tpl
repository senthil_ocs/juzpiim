<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Supplier</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo $purchase; ?>">Purchase</a>
                        <i class="fa fa-angle-right"></i>                      
                    </li>
                    <li>
                        <a href="<?php echo $supplier; ?>">Supplier List</a>
                        <i class="fa fa-angle-right"></i>                                             
                    </li>
                    <li>
                        <a href="#">Supplier</a>                                                 
                    </li>                   
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div class="row">
            	<!--24- Feb-2016!-->
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6">
                   		<table class="table orderlist">
	                   		<thead>
	                        	<tr><th colspan="2">Vendors</th></tr>
	                        </thead>
                        <tbody>
                        	<tr>
                            	<td width="35%"><label for="code"><?php echo $entry_code; ?><span class="required">*</span></label></td>
                                <td width="65" class="order-nopadding">
                                     <?php if($error_code) { ?>
	                                	<div class="input-icon right">
											<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
											<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox requiredborder" placeholder="Enter your code" <?php if($code != '') { ?> readonly <?php } ?>>                              
                               			</div>
                               		<?php } ?>
                               		<?php if(!$error_code) { ?>
                               		<input type="text" name="code" id="code" value="<?php echo $code; ?>" class="textbox" placeholder="Enter your code" <?php if($code != '') { ?> readonly <?php } ?>>
                               		<?php } ?>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
                  											<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                  											<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">
                               			</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><label for="name">Currency Code<span class="required">*</span></label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                  <select class="selectpicker selectdropdown" name="currency_code">
                                    <?php if(!empty($currency_collection)){ 
                                      foreach ($currency_collection as $currency) { ?>
                                          <option value="<?php echo $currency['code']; ?>" <?php if($currency_code==$currency['code']){ echo 'Selected'; } ?>><?php echo $currency['title']; ?></option>
                                    <?php } } ?>
                                  </select>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%"><label for="name">Term</label>
                                </td>
                                <td width="65%" class="order-nopadding">
                                  <select class="" name="term_id">
                                  <option value="">Select Term</option>
                                  <?php if(!empty($allTerms)){ 
                                    foreach ($allTerms as $term) { ?>
                                        <option value="<?php echo $term['terms_id']; ?>" <?php if($term['terms_id']==$term_id) { echo "selected"; } ?> ><?php echo $term['terms_name']; ?></option>
                                  <?php } } ?>
                                </select>
                                </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="tax_no">Tax No</label>
                              </td>
                              <td width="65%" class="order-nopadding">
                                  <input type="text" name="tax_no" id="tax_no" value="<?php echo $tax_no; ?>" class="textbox" placeholder="Enter Tax No">
                              </td>
                            </tr>
                        </tbody>
                    </table>
                     <table class="table orderlist">
                    	<thead>
                        	<tr><th colspan="2">Address Info</th></tr>
                        </thead>
                    	<tbody>
                        	<tr>
                            	<td width="35%">
                                <label for="description"><?php echo $entry_address; ?></label>
                                </td>
                                <td width="65%" class="order-nopadding1">
                                	<?php if ($error_address) { ?>
                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter Address"></i>			
	                               		 <input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox requiredborder" placeholder="Enter Address 1">
										<input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox requiredborder" placeholder="Enter Address 2">
									</div>
								<?php } ?>
								<?php if(!$error_address) { ?>
								<input type="text" name="address1" value="<?php echo $address1; ?>" class="textbox" placeholder="Enter Address 1">
								<input type="text" name="address2" value="<?php echo $address2; ?>" class="textbox" placeholder="Enter Address 2">									
								<?php } ?>       
                                </td>
                            </tr>
                            <tr>
	                            <td width="35%"><label for="country"><?php echo $entry_country; ?></label></td>
								<td width="65%" class="order-nopadding1">
									<select name="country" class="selectpicker selectdropdown">
										<?php if(!empty($country_collection)): ?>
											<?php foreach($country_collection as $countrydetails): ?>
												<option value="<?php echo $countrydetails['country_id']; ?>" <?php if($countrydetails['country_id']==$country):?> selected="selected" <?php endif; ?>><?php echo $countrydetails['name']; ?></option>
											<?php endforeach; ?>
										<?php endif; ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="35%"><label for="zipcode"><?php echo $entry_zipcode; ?></label></td>
								<td width="65%" class="order-nopadding">
									<input type="text" name="zipcode" value="<?php echo $zipcode; ?>" class="textbox" placeholder="Enter zipcode">
								</td>
							</tr>
                        </tbody>
                    </table>
                      <table class="table orderlist">
                      	<thead>
                        	<tr>
                            	<th colspan="2">Contact Info</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
							<td width="35%">
								<label for="phone"><?php echo $entry_phone; ?></label>
							</td>
							<td width="65%" class="order-nopadding">
								<input type="text" name="phone" value="<?php echo $phone; ?>" class="textbox" placeholder="Enter phone" />
							</td>
						</tr>
						<tr>
							<td width="35%">
								<label for="fax"><?php echo $entry_fax; ?></label>
							</td>
							<td width="65%" class="order-nopadding">
								<input type="text" name="fax" value="<?php echo $fax; ?>" class="textbox" placeholder="Enter fax" />
							</td>
						</tr>
  						<tr>
  							<td width="35%">
  								<label for="email"><?php echo $entry_email; ?> <span class="required">*</span></label>
  							</td>
  							<td width="65%" class="order-nopadding">
                <div class="input-icon right">
                <?php if ($error_email_duplication) { ?>
                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Email has already been added!"></i>
                <?php } ?>
                <?php if ($error_email) { ?>
                  <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Enter Email!"></i>
                <?php } ?>
                <input type="email" name="email" id="email" value="<?php echo $email; ?>" class="textbox <?php if ($error_email_duplication || $error_email) { ?>requiredborder <?php } ?>" placeholder="Enter your email" style="padding: 0px 0px 0px 11px !important;">
                </div>
  							</td>
  						</tr>
                        </tbody>
                      </table>
                   	</div>
                   	<div class="col-md-6">
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Others</th></tr>
                        	</thead>
                        	<tbody>
	                        	<tr>
									<td width="35%"><label for="attn"><?php echo $entry_attn; ?></label></td>
									<td width="65%" class="order-nopadding">
										<input type="text" name="attn" id="attn" value="<?php echo $attn; ?>" class="textbox" placeholder="Enter your attn">
									</td>
								</tr>
								<tr>
									<td width="35%"><label for="remarks"><?php echo $entry_remarks; ?></label></td>
									<td width="65%" class="order-nopadding">
										<input type="text" name="remarks" id="remarks" value="<?php echo $remarks; ?>" class="textbox" placeholder="Enter your remarks">
									</td>
								</tr>
                
                <tr>
                  <td width="35%"><label for="remarks"><?php echo 'GST'; ?></label></td>
                  <td width="65%">
                    <input type="checkbox" name="gst" id="gst" value="1" class="textbox" <?php if($gst == 1) { ?> checked="checked" <?php } ?>>
                     <select name="tax_method" id="tax_method" class="selectpicker" style="<?php if($gst != 1) { ?> display: none;<?php } ?>">
                        <option value="1" <?php if($tax_method=='1'):?> selected="selected" <?php endif; ?>>Exclusive Tax</option>
                        <option value="2" <?php if($tax_method=='2'):?> selected="selected" <?php endif; ?>>Inclusive Tax</option>
                    </select>

                  </td>
                </tr>


									<td width="35%"><?php echo 'Status'; ?></td>
									<td width="80%">
                    <select name="status" class="selectpicker selectdropdown">
                        <option value="0" <?php if ($status == 0) { ?> selected="selected" <?php } ?>><?php echo 'Enabled'; ?></option>
                        <option value="1" <?php if ($status == 1) { ?> selected="selected" <?php } ?>><?php echo 'Disabled'; ?></option>
                    </select>
                  </td>
								</tr>
            	</tbody>
       	  	</table>
       	  </div>
       </div>
   		</div>
    </form>
  </div>
</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
   $("input[name=gst]:checkbox").click(function() {
      if(this.checked==true) {
        $("#tax_method").show();
      } else {
        $("#tax_method").hide();
      } 

    });

</script>