<?php echo $header; ?>
<div id="content">
<div class="company-information">
<?php if ($error_warning) { ?>
	<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
	<div class="success"><?php echo $success; ?></div>
<?php } ?>
  
<h1>Company Details</h1>
<form action="<?php echo $action; ?>" method="post" class="company-details-form" id="formID" enctype="multipart/form-data">
	<div class="form-information left-side">
        <?php /*?><div class="fields">
            <label for="location_code"><?php echo $entry_location; ?></label>
            <input type="text" name="location_code" id="location_code" value="<?php echo $location_code; ?>" class="input-text" readonly="readonly" disabled="disabled" placeholder="Enter your Location Code">
        </div><?php */?>
        <div class="fields">
            <label for="name"><?php echo $entry_companyname; ?></label>
            <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text" readonly="readonly" disabled="disabled" placeholder="Enter your name">
        </div>
		<div class="fields">
            <label for="logo">Company Logo</label>
			<div class="image-files">
				<?php if($preview_logo): ?>
					<img src="<?php echo $preview_logo; ?>" alt="" id="preview_logo" class="image"/>
				<?php endif; ?>
				<input type="file" name="logo" id="logo" value="<?php echo $logo; ?>" class="input-text">
			</div>
        </div>
        <div class="fields">
            <label for="address1"><?php echo $entry_address; ?></label>
            <input type="text" name="address1" id="address1" value="<?php echo $address1; ?>" class="input-text validate[required] " placeholder="Address Line 1">
            <input type="text" name="address2" value="<?php echo $address2; ?>" class="input-text" placeholder="Address Line 2">
        </div>
        <div class="fields">
            <div class="field">
                <label for="country"><?php echo $entry_country; ?></label>
                <input type="text" name="country" id="country" value="<?php echo $country; ?>" class="input-text validate[required] " placeholder="Enter your Country">
            </div>
            <div class="field last">
                <label for="city" class="a-right"><?php echo $entry_city; ?></label>
                <input type="text" name="city" id="city" value="<?php echo $city; ?>" class="input-text" placeholder="Enter your city">
            </div>
        </div>
        <div class="fields">
            <div class="field">
                <label for="state"><?php echo $entry_state; ?></label>
                <input type="text" name="state" id="state" value="<?php echo $state; ?>" class="input-text" placeholder="State or Province">
            </div>
            <div class="field last">
                <label for="postal_code" class="a-right"><?php echo $entry_postal; ?></label>
                <input type="text" name="postal_code" id="postal_code" value="<?php echo $postal_code; ?>" class="input-text validate[required] " placeholder="Enter Postal Code">
            </div>
        </div>
        <div class="fields">
            <label for="email"><?php echo $entry_email; ?></label>
            <input type="text" name="email" id="email" value="<?php echo $email; ?>" class="input-text validate[required,custom[email]]" placeholder="Enter your E-mail">
        </div>
        <div class="fields">
            <label for="web_url"><?php echo $entry_weburl; ?></label>
            <input type="text" name="web_url" id="web_url" value="<?php echo $web_url; ?>" class="input-text validate[required,custom[url]]" placeholder="Web url">
        </div>
        <div class="fields">
            <div class="field">
                <label for="Phone"><?php echo $entry_phone; ?></label>
                <input type="text" name="phone" id="phone" value="<?php echo $phone; ?>" class="input-text" placeholder="Phone Number">
            </div>
            <div class="field last">
                <label for="rax" class="a-right"><?php echo $entry_fax; ?></label>
                <input type="text" name="fax" id="fax" value="<?php echo $fax; ?>" class="input-text" placeholder="Fax Number">
            </div>
        </div>
        <div class="fields">
            <label for="remarks"><?php echo $entry_remarks; ?></label>
            <textarea name="remarks" id="remarks" class="input-text" placeholder="Enter your remarks"><?php echo $remarks; ?></textarea>
        </div>
        <div class="fields">
            <label for="business_reg_no"><?php echo $entry_businessregno; ?></label>
            <input type="text" name="business_reg_no" id="business_reg_no" value="<?php echo $business_reg_no; ?>" class="input-text validate[required,custom[number]]" placeholder="Enter your Business Reg No">
        </div>
        <?php /*?><div class="fields">
            <label for="gst_reg"><?php echo $entry_gstreg; ?></label>
            <input type="text" name="gst_reg" id="gst_reg" value="<?php echo $gst_reg; ?>" class="input-text validate[required,custom[number]]" placeholder="GST Reg Number">
        </div>
        <div class="fields">
            <div class="field">
                <label for="gst_perc"><?php echo $entry_gstperc; ?></label>
                <input type="text" name="gst_perc" id="gst_perc" value="<?php echo $gst_perc; ?>" class="input-text validate[required,custom[number]]" placeholder="GST Perc">
                <select name="gst_perc" id="gst_perc" class="input-text validate-select">
                    <option value="I">I</option>
                    <option value="E">E</option>
                    <option value="Z">Z</option>
                </select>
            </div>
            <div class="field last">
                <label for="post_gst_type" class="a-right"><?php echo $entry_postgsttype; ?></label>
                <select name="post_gst_type" id="post_gst_type" class="input-text validate-select">
                    <option value="I">I</option>
                    <option value="E">E</option>
                    <option value="Z">Z</option>
                </select>
            </div>
        </div>
        <div class="fields">
            <div class="field">
                <label for="purchase_gst_type"><?php echo $entry_purchasegsttype; ?></label>
                <select name="purchase_gst_type" id="purchase_gst_type" class="input-text validate-select">
                    <option value="I">I</option>
                    <option value="E">E</option>
                    <option value="Z">Z</option>
                </select>
            </div>
            <div class="field last">
                <label for="sales_gst_type" class="a-right"><?php echo $entry_salesgsttype; ?></label>
                <select name="sales_gst_type" id="sales_gst_type" class="input-text validate-select">
                    <option value="I">I</option>
                    <option value="E">E</option>
                    <option value="Z">Z</option>
                </select>
            </div>
        </div>
        <div class="fields">
            <label for="display_message1"><?php echo $entry_displaymessages1; ?></label>
            <input type="text" name="display_message1" id="display_message1" value="<?php echo $display_message1; ?>" class="input-text" placeholder="Enter your display message 1">
        </div>
        <div class="fields">
            <label for="display_message2"><?php echo $entry_displaymessages2; ?></label>
            <input type="text" name="display_message2" id="display_message2" value="<?php echo $display_message2; ?>" class="input-text" placeholder="Enter your display message 2">
        </div><?php */?>
	</div>
    <div class="form-information">
    	<?php /*?><div class="fields">
        	<div class="field">
            	<label for="max_discount"><?php echo $entry_discount; ?></label>
            	<input type="text" name="max_discount" id="max_discount" value="<?php echo $max_discount; ?>" class="input-text validate-percents" placeholder="Max Discount">
			</div>
        </div>
        <div class="fields">
        	<div class="field">
            	<label for="price_change"><?php echo $entry_pricechange; ?></label>
            	<input type="text" name="price_change" id="price_change" value="<?php echo $price_change; ?>" class="input-text validate-percents" placeholder="Price Change">
			</div>
        </div>
        <div class="fields">
            <label for="bill_message"><?php echo $entry_billmessage; ?></label>
            <textarea name="bill_message" id="bill_message" class="input-text" placeholder="Enter your bill message" rows="5" cols="1"><?php echo $bill_message; ?></textarea>
            <span class="promotion-chcekbox">
            	<input type="checkbox" name="print_promotion" value="1"  /> <?php echo $entry_promotions; ?>.
			</span>
        </div><?php */?>
        <?php /*?><div class="fields">
        	<div class="field">
            	<label for="EODDate"><?php echo $entry_lastdate; ?></label>
                <input type="text"  name="last_eod_date" id="last_eod_date" value="<?php echo $last_eod_date; ?>" class="input-text"  readonly="readonly" disabled="disabled" placeholder="Enter EOD Date"/>
			</div>
        </div><?php */?>
        <div class="fields">
            <?php /*?><label><?php echo $entry_configemail; ?></label><?php */?>
            <div class="sub-field">
                <?php /*?><div class="fields">
                    <label for="config_server"><?php echo $entry_server; ?></label>
                    <input type="text"  name="config_server" id="config_server" value="<?php echo $config_server; ?>" class="input-text" placeholder="Enter Server Details"/>
                </div>
                <div class="fields">
                    <label for="config_name"><?php echo $entry_displayname; ?></label>
                    <input type="text"  name="config_name" id="config_name" value="<?php echo $config_name; ?>" class="input-text" placeholder="How to Display your name"/>
                </div>
                <div class="fields">
                    <label for="config_email"><?php echo $entry_fromemailid; ?></label>
                    <input type="text"  name="config_email" id="config_email" value="<?php echo $config_email; ?>" class="input-text validate-email" placeholder="Enter from Email id"/>
                </div>
                <div class="fields">
                    <label for="password"><?php echo $entry_password; ?></label>
                    <input type="password"  name="config_password" id="config_password" value="" class="input-text validate-password" placeholder="Must be 6 characters"/>
                </div>
                <div class="fields">
                    <label for="c_password"><?php echo $entry_cpassword; ?></label>
                    <input type="password"  name="c_password" id="c_password" value="" class="input-text validate-cpassword" placeholder="Retype your password"/>
                </div><?php */?>
                <div class="fields action">
                    <button type="submit" class="btn btn-update"><?php echo $button_update; ?></button>
                    <button type="reset" class="btn btn-reset"><?php echo $button_cancel; ?></button>
                </div>
			</div>
        </div>
    </div>
</form>
<script type="text/javascript">
//<![CDATA[
	var companyDetailsForm = new VarienForm('company-details-form', true);
	jQuery(function() {
		//jQuery("#EODDate").datepicker({ dateFormat: 'yy-mm-dd' });
	});
//]]>
</script>
</div>
</div>
<?php echo $footer; ?>