<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="heading">
      <h1><?php echo $heading_title; ?></h1>
      <div class="buttons">
	  	<a href="<?php echo $insert; ?>" class="button"><span><?php echo $button_insert; ?></span></a>
		<a onclick="$('form').submit();" class="button"><span><?php echo $button_delete; ?></span></a>
	  </div>
    </div>
    <div class="content">
	<?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr class="heading">
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  <td class="left"><?php if ($sort == 'code') { ?>
                <a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
              <td class="left"><?php if ($sort == 'method') { ?>
                <a href="<?php echo $sort_method; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_method; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_method; ?>"><?php echo $column_method; ?></a>
                <?php } ?></td>
				 <td class="left"><?php if ($sort == 'position') { ?>
                <a href="<?php echo $sort_position; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_position; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_position; ?>"><?php echo $column_position; ?></a>
                <?php } ?></td>
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if ($stocks) { ?>
			<?php foreach ($stocks as $stock) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($stock['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $stock['stock_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $stock['stock_id']; ?>" />
                <?php } ?></td>
			   <td class="left"><?php echo $stock['code']; ?></td>
              <td class="left"><?php echo $stock['name']; ?></td>
              <td class="left">
			  	<?php if($stock['method']=="1"):?>
					Add
				<?php else: ?>
					Deduct
				<?php endif;?>
			  </td>
			  <td class="left"><?php echo $stock['position']; ?></td>
              <td class="right"><?php foreach ($stock['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<?php echo $footer; ?>