<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="content">
		<div class="form-list-container">
			<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="success"><?php echo $success; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<a href="<?php echo $insert; ?>" class="btn btn-action"><?php echo $button_insert; ?></a>
					<a onclick="$('form').submit();" class="btn btn-action"><?php echo $button_delete; ?></a>
				</div>
			</div>
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
                    <thead>
                      <tr>
                        <td width="1" style="text-align: center;"><input type="checkbox" onclick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                        <td class="left"><?php if ($sort == 'CG.cashiergroup_name') { ?>
                          <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                          <?php } else { ?>
                          <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                          <?php } ?></td>
                        <td class="right"><?php echo $column_action; ?></td>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if ($cashiergroups) { ?>
                      <?php foreach ($cashiergroups as $cashiergroup) { ?>
                      <tr>
                        <td style="text-align: center;"><?php if ($cashiergroup['selected']) { ?>
                          <input type="checkbox" name="selected[]" value="<?php echo $cashiergroup['cashier_group_id']; ?>" checked="checked" />
                          <?php } else { ?>
                          <input type="checkbox" name="selected[]" value="<?php echo $cashiergroup['cashier_group_id']; ?>" />
                          <?php } ?></td>
                        <td class="left"><?php echo $cashiergroup['name']; ?></td>
                        <td class="right"><?php foreach ($cashiergroup['action'] as $action) { ?>
                          [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                          <?php } ?></td>
                      </tr>
                      <?php } ?>
                      <?php } else { ?>
                      <tr>
                        <td class="center" colspan="8">There is no records found</td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
			</form>
		  <div class="pagination"><?php echo $pagination; ?></div>
		</div>
	  </div>
  </div>
</div>
<?php echo $footer; ?>