<?php echo $header; ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  <div class="box">
  	<div class="content">
		<div class="form-information">
			<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
				</div>
			</div>
			<div class="form-new">
				<table cellspacing="0" class="form-list">
					<tbody>
						<tr>
							<td class="label">
								<label for="name"><?php echo $entry_group; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<select name="group" class="input-text validate[required]">
                                <option value="">-- Select cashier group --</option>
                                <?php if(!empty($cashier_collection)): ?>
                                    <?php foreach($cashier_collection as $cashier_group): ?>
                                        <option value="<?php echo $cashier_group['cashier_group_id']; ?>" <?php if($cashier_group['cashier_group_id']==$group):?> selected="selected" <?php endif; ?>>
                                            <?php echo $cashier_group['cashier_group_name']; ?>
                                        </option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text validate[required]" placeholder="Enter your name">
								<?php if ($error_name) { ?>
                                    <span class="error"><?php echo $error_name; ?></span>
                                <?php } ?>
							</td>
						</tr>
                        <tr>
							<td class="label">
								<label for="email"><?php echo $entry_email; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<input type="text" name="email" value="<?php echo $email; ?>" class="input-text validate[required,custom[email]]" placeholder="Enter email address" />
								<?php if ($error_email) { ?>
                                    <span class="error"><?php echo $error_email; ?></span>
                                <?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="description"><?php echo $entry_password; ?></label>
							</td>
							<td class="value">
								<input type="password" name="password" class="input-text validate[required]" placeholder="Enter password" id="password"/>
								<?php if ($error_password) { ?>
                                    <span class="error"><?php echo $error_password; ?></span>
                                <?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="cpassword"><?php echo $entry_cpassword; ?></label>
							</td>
							<td class="value">
								<input type="password" name="cpassword" class="input-text validate[required,equals[password]]" placeholder="Enter Confirm password" />
							</td>
						</tr>
                        <tr>
							<td class="label">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td class="value">
								<select name="status" class="input-text">
									<?php if ($status) { ?>
										<option value="0"><?php echo $text_disabled; ?></option>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
									<?php } else { ?>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
										<option value="1"><?php echo $text_enabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>
</form>
<?php echo $footer; ?>