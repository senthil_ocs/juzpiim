<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="content">
		<div class="form-list-container">
			<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="success"><?php echo $success; ?></div>
			<?php } ?>
			<form action="<?php echo $print; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="heading">
                    <h1><?php echo $heading_title; ?></h1>
                    <div class="action">
                        <button class="btn btn-action master_print_button" type="submit" title="Save"><?php echo $button_print; ?></button>
                    </div>
                </div>			
				<table class="list">
				  <thead>
					<tr>
                      <td class="center" colspan="2"><?php echo $heading_title; ?></td>
					</tr>
				  </thead>
				  <tbody>
					<tr class="master_name_list">
                      <td class="right"><input type="radio" name="master_name" id="master_name_cashier" value="Cashier" checked="checked" /></td>
                      <td class="left"><label for="master_name_cashier">Cashier</label></td>
					</tr>
                    <tr class="master_name_list">
                      <td class="right"><input type="radio" name="master_name" id="master_name_salesman" value="SalesMan" /></td>
                      <td class="left"><label for="master_name_salesman">SalesMan</label></td>
					</tr>
                    <tr class="master_name_list">
                      <td class="right"><input type="radio" name="master_name" id="master_name_paymode" value="Paymode" /></td>
                      <td class="left"><label for="master_name_paymode">Paymode</label></td>
					</tr>
                    <tr class="master_name_list">
                      <td class="right"><input type="radio" name="master_name" id="master_name_tax" value="Tax" /></td>
                      <td class="left"><label for="master_name_tax">Tax</label></td>
					</tr>
                    <tr class="master_name_list">
                      <td class="right"><input type="radio" name="master_name" id="master_name_vendor" value="Vendor" /></td>
                      <td class="left"><label for="master_name_vendor">Vendor</label></td>
					</tr>
				  </tbody>
				</table>
			</form>
		</div>
	  </div>
  </div>
</div>
<?php echo $footer; ?>