<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
     <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  } ?>                    
        </ul>
         <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>
      <div style="clear:both"></div>            
      <div class="row">     
      <div class="col-md-12">      
      <div class="innerpage-listcontent-blocks"> 
        <table class="table orderlist statusstock">
              <tr>
              <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
            </tr>
            <tr>
              <td align="center" colspan="2">
            <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                    <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                </td>
            </tr>
            <tr>
              <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
            </tr>
            <tr>
              <td align="center" colspan="2"><?php echo $companyInfo['country']; ?></td>
            </tr>
            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
            <tr <?php echo $contact; ?>>
              <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
            </tr>
      </table>
      <div>
         <h1><?php echo ucfirst($master_name); ?> List</h1>
      </div>
          <?php if (strtolower($master_name) == 'paymode')  {  ?>
          <table class="table orderlist statusstock">
            <thead>
              <tr class="heading">
                <td class="center">Payment Name</td>
                <td class="center">Description</td>
                <td class="center">Position</td>
                <td class="center">Status</td>
              </tr>
            </thead>
             <tbody>
              <?php if (!empty($paymodeList)) { ?>
               <?php $class = 'odd'; ?>
              <?php foreach ($paymodeList as $paymode) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">
                    <td class="center"><?php echo $paymode['payment_name']; ?></td>
                    <td class="center"><?php echo $paymode['description']; ?></td>
                    <td class="center"><?php echo $paymode['position']; ?></td>
                    <td class="center"><?php if ($paymode['status']) { ?> Yes <?php } else { ?>No<?php } ?></td>
                  </tr>
              <?php } ?>              
              <?php } else { ?>
              <tr>
                  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>          
          
      <?php } elseif (strtolower($master_name) == 'salesman') { ?>   
          <table class="table orderlist statusstock">
            <thead>
              <tr>
                <tr style="background: #A8A8A8; color: #000; font-weight: bold;">
                <td class="center">ID</td>
                <td class="center">Sales Man Name</td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($salesmanList)) { ?>
              <?php foreach ($salesmanList as $salesman) { ?>
                  <tr>
                    <td class="center"><?php echo $salesman['salesman_id']; ?></td>
                    <td class="center"><?php echo $salesman['salesman_name']; ?></td>
                  </tr>
              <?php } ?>
              <tr>
                  <td class="center print_page_button" colspan="2">
                      <a class="btn btn-zone" onClick="window.print();">Print</a>
                      <a href="<?php echo $back; ?>" class="btn btn-zone">Back</a>
                  </td>
              </tr>
              <?php } else { ?>
              <tr>
                  <td class="center" colspan="2"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
              
            </tbody>
          </table>
          
          <?php } elseif (strtolower($master_name) == 'vendor') { ?>   
          <table class="table orderlist statusstock">
            <thead>
             <tr class="heading">
                <td class="center">Ventor Code</td>
                <td class="center">Ventor Name</td>
                <td class="center">Phone</td>
                <td class="center">Email</td>
                <td class="center">Address</td>
                <td class="center">Country</td>
                <td class="center">Zip Code</td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($vendorList)) { ?>
              <?php foreach ($vendorList as $vendor) { ?>
                  <tr>

                    <td class="center"><?php echo $vendor['code']; ?></td>
                    <td class="center"><?php echo $vendor['vendor_name']; ?></td>
                    <td class="center"><?php echo $vendor['phone']; ?></td>
                    <td class="center"><?php echo $vendor['email']; ?></td>
                    <td class="center">
          <?php 
          if ($vendor['address1']) { echo $vendor['address1']; }
          if ($vendor['address2']) { echo ', '.$vendor['address2']; }
          ?>
                    </td>
                    <td class="center"><?php echo $vendor['country']; ?></td>
                    <td class="center"><?php echo $vendor['postal_code']; ?></td>
                  </tr>
              <?php } ?>
              
              <?php } else { ?>
              <tr>
                  <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>
              
            </tbody>
          </table>          
          
          <?php } elseif (strtolower($master_name) == 'cashier') { ?>   
          <table class="table orderlist statusstock">
            <thead>
              <tr class="heading">
                <td class="center">Cashier Name</td>
                <td class="center">Cashier Group</td>
                <td class="center">Cashier Email</td>
                <td class="center">Status</td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($cashierList)) { ?>
              <?php foreach ($cashierList as $cashier) { ?>
                  <tr>
                    <td class="center"><?php echo $cashier['cashier_name']; ?></td>
                    <td class="center"><?php echo $cashier['group']; ?></td>
                    <td class="center"><?php echo $cashier['email']; ?></td>
                    <td class="center"><?php echo $cashier['status']; ?></td>
                  </tr>
              <?php } ?>
              <tr>
                  <td class="center print_page_button" colspan="4">
                      <a class="btn btn-zone" onClick="window.print();">Print</a>
                      <a href="<?php echo $back; ?>" class="btn btn-zone">Back</a>
                  </td>
              </tr>
              <?php } else { ?>
              <tr>
                  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>    
           
          
          <?php } elseif (strtolower($master_name) == 'tax') { ?>   
          <table class="table orderlist statusstock">
            <thead>
              <tr class="heading">
                <td class="center">Tax Name</td>
                <td class="center">Tax Rate</td>
                <td class="center">Type</td>
                <td class="center">Geo Zone</td>
              </tr>
            </thead>
            <tbody>
              <?php if (!empty($taxList)) { ?>
              <?php foreach ($taxList as $tax) { ?>
                  <tr>
                    <td class="center"><?php echo $tax['name']; ?></td>
                    <td class="center"><?php echo $tax['rate']; ?></td>
                    <td class="center"><?php echo $tax['type'] == 'F' ? 'Fixed Amount' : 'Percentage'; ?></td>
                    <td class="center"><?php echo $tax['geo_zone']; ?></td>
                  </tr>
              <?php } ?>
             
              <?php } else { ?>
              <tr>
                  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>     
          <?php } ?>
       </div>          
      </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>