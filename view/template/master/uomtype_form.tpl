<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Uom Master</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <?php if ($error_warning) { ?>
				<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
		    <?php } ?>
	<div style="clear:both"></div>
            <div class="row">
            <div style="clear:both"></div>         
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                    <label for="name">UOM<span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <?php if ($error_uom_code) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
                                        <select name="uom_code" id="uom_code" class="textbox requiredborder" >
                                            <option value=""> -Select UOM- </option>
                                            <?php if(count($UomDetails) > 0){ ?>
                                                <?php foreach($UomDetails as $uom){ ?>
                                                    <option value="<?php echo $uom['code']; ?>" <?php if($uom_code == $uom['code']){ ?> selected="selected" <?php } ?>><?php echo $uom['code']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_uom_code) { ?>
                                    <input type="hidden" name="uom_code" value="<?php echo $uom_code;?>">
                                    <select name="uom_code" id="uom_code" class="textbox" <?php if($edit_form){?> disabled <?php } ?>>
                                            <option value=""> -Select UOM- </option>
                                            <?php if(count($UomDetails) > 0){ ?>
                                                <?php foreach($UomDetails as $uom){ ?>
                                                    <option value="<?php echo $uom['code']; ?>" <?php if($uom_code == $uom['code']){ ?> selected="selected" <?php } ?>><?php echo $uom['code']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>  
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
									<?php if ($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
										<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">                      
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>	
                                </td>                                
                            </tr>
                            <tr>
                                <td width="20%">
                                    <label for="name"><?php echo $entry_value; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <?php if ($error_value) { ?>
                                        <div class="input-icon right">
                                        <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                                        <input type="text" name="value1" id="value1" value="<?php echo $value1; ?>" class="textbox requiredborder" placeholder="Enter your value1" style="min-width:49%;"> 
                                        x 
                                        <input type="text" name="value2" id="value2" value="<?php echo $value2; ?>" class="textbox requiredborder" placeholder="Enter your value2" style="min-width:49%;"> 
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_value) { ?>
                                    <input type="text" name="value1" id="value1" value="<?php echo $value1; ?>" class="textbox" placeholder="Enter your value1" style="min-width:49%;" <?php if($edit_form){?> readonly <?php } ?>>
                                    x
                                    <input type="text" name="value2" id="value2" value="<?php echo $value2; ?>" class="textbox" placeholder="Enter your value2" style="min-width:49%;" <?php if($edit_form){?> readonly <?php } ?>>
                                    <?php } ?>
                                </td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>