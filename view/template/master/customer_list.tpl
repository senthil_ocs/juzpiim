<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Customer Report</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  } ?>
				</ul>
				<div class="page-toolbar">
	                <div class="btn-group pull-right">
			            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
			              	<a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
	              			<a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
	            		</div>    
	          		</div>
                </div>              						
			</div>
			
			<?php if ($error_warning) { ?>
				<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
             	</div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>	
				<!-- Filter Starts  -->
			<div class="innerpage-listcontent-blocks">
        		<div class="portlet bordernone" style="margin-bottom:0 !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name / Mobile Number' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
		                        </td> 
		                        <td>
		              				<input type="text" placeholder='Email' name="filter_email" value="<?php echo $_POST['filter_email'] ; ?>" class="textbox" style="width: 300px;"  />
		                   		</td>                  
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            <div class="tools" style="float:left;padding: 0 0 0 16px;">
		               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
		            </div>
		            </div> 
	              <?php if (($_POST['filter_date_from'] !="") || ($_POST['filter_date_to'] != "") || ($_POST['filter_Type'] != "")){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
           <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: 0 !important; margin-left:15px; width: 15%;">
                <tbody>
                  <tr class="filter">
                   	<td>
                   		<input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_POST['filter_date_from'] ; ?>" class="textbox date">
            		</td>
              		<td>
                   		<input type="text" id='' placeholder='To Date' name="filter_date_to" value="<?php echo $_POST['filter_date_to'] ; ?>" class="textbox date">
            		</td> 
            		<td>
            			<select name="filter_Type" class="textbox"  style="min-height: 35px; padding: 7px">
		                    <option value="">Select Type</option>
		                    <option value="1" <?php if( $filtertype == 1) { ?> selected="selected" <?php } ?>>ragu Group</option>
		                    <option value="2" <?php if( $filtertype == 2) { ?> selected="selected" <?php } ?>> Testing Group</option>
					    </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
           
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->			 
			<div class="row">			
				<div class="col-md-12">
					<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	                	<div class="innerpage-listcontent-blocks"> 
							<table class="table orderlist statusstock">
	                        	<thead>
		                            <tr class="heading">
						                <td class="center">First Name</td>
						                <td class="center">Last Name</td>
						                <td class="center">Phone Number</td>
						                <td class="center">Email</td>
						                <td class="center">Added Date</td>
									</tr>
	                        	</thead>
		                        <tbody>
		                            <?php if ($customers) { ?>
			                            <?php $class = 'odd'; ?>
										<?php foreach ($customers as $customer) { ?>
										<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
										<tr class="<?php echo $class; ?>">
											<td class="left"><?php echo $customer['firstname']; ?></td>
											<td class="left"><?php echo $customer['lastname']; ?></td>								
											<td class="left"><?php echo $customer['mobile']; ?></td>
											<td class="left"><?php echo $customer['email']; ?></td>
											<td class="left"><?php echo $customer['added_date']; ?></td>
										</tr>
										<?php } ?>
										<?php } else { ?>
										<tr>
											<td align="center" colspan="7"><?php echo $text_no_results; ?></td>
										</tr>
									<?php } ?>
		                        </tbody>
	                    	</table>
	                    </div>
	                </form>
	              <div class="pagination"><?php echo $pagination; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
	document.report_filter.submit();
}
</script>
<?php echo $footer; ?>