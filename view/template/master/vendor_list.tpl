<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">Supplier List</h3>
        	<div class="page-bar">
			<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo $home; ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>						
						<a href="<?php echo $purchase; ?>">Purchase</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo $vendorsLink; ?>">Supplier List</a>						
					</li>					
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                        	<?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                             <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div>
        <div class="innerpage-listcontent-blocks">
        	<div class="portlet bordernone" style="margin-bottom:0 !important">
          	<div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
            <form method="post" name="report_filter" id="report_filter"> 
                <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">
                      <td>
                      <input type="text" placeholder='Supplier Name' name="filter_supplier" value="<?php echo $_REQUEST['filter_supplier'] ; ?>" class="textbox"> 
                      </td>                      
                        <td>
                          <input type="text" placeholder='Phone No' name="filter_phone" value="<?php echo $_REQUEST['filter_phone'] ; ?>" class="textbox">
                        </td> 
                        <td class="center" colspan="4">
                          <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                </table>
            </form>
            </div>                        
            </div>     

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success" style="margin-bottom: 12px;margin-top: 12px;">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">                	      
                    <div class="innerpage-listcontent-blocks">             
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                        	<tr class="heading">
					  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
					  <td class="left"><?php if ($sort == 'vendor_code') { ?>
						<a href="<?php echo $sort_code; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_code; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_code; ?>"><?php echo $column_code; ?></a>
						<?php } ?>
					  </td>
					  <td class="left"><?php if ($sort == 'name') { ?>
						<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?>
					  </td>
					  <td class="left">
						<?php if ($sort == 'country') { ?>
						<a href="<?php echo $sort_country; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_country; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_country; ?>"><?php echo $column_country; ?></a>
						<?php } ?>
					  </td>
					  <td class="left">
						<?php if ($sort == 'phone') { ?>
						<a href="<?php echo $sort_phone; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_phone; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_phone; ?>"><?php echo $column_phone; ?></a>
						<?php } ?>
					  </td>
					  <td class="left">
						<?php if ($sort == 'email') { ?>
						<a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
						<?php } ?>
					  </td>
					  <td class="left">
						<?php if ($sort == 'zipcode') { ?>
						<a href="<?php echo $sort_zipcode; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_zipcode; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_zipcode; ?>"><?php echo $column_zipcode; ?></a>
						<?php } ?>
					  </td>
					  <td class="right"><?php echo $column_action; ?></td>
					  <?php if(HIDE_XERO){?>
					  <td style="text-align: center;">Xero</td>
					  <?php }?>
					</tr>
                        </thead>
                        <tbody>
                        	<?php if ($vendors) { ?>
                        	<?php $class = 'odd'; ?>
					<?php foreach ($vendors as $vendor) { ?>					
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;"><?php if ($vendor['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $vendor['vendor_id']; ?>" />
						<?php } ?></td>
					  <td class="left"><?php echo $vendor['code']; ?></td>
					  <td class="left"><?php echo $vendor['name']; ?></td>
					  <td class="left"><?php echo $vendor['country']; ?></td>
					  <td class="left"><?php echo $vendor['phone']; ?></td>
					  <td class="left"><?php echo $vendor['email']; ?></td>
					  <td class="left"><?php echo $vendor['zipcode']; ?></td>
					  <td class="right"><?php foreach ($vendor['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
					  <?php if(HIDE_XERO){?>
						  <td style="text-align: center;" class="tdclass_<?php echo $vendor['vendor_id'] ?>">
						 	 <?php if($vendor['xero_id'] !=''){ ?>
						 	 	<span class="text-primary">Success</span>
						 	  <?php }else{ ?>
						 	  	<span class="text-primary xero" data-vendor_id="<?php echo $vendor['vendor_id'] ?>" ><i class="fa fa-upload" aria-hidden="true" style="font-size:19px;" data-toggle="tooltip" title="Post"></i></span>
						 	 <?php  } ?>
						</td>
					  <?php }?>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
               </div>
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
$( document ).ready(function() {
   $('.xero').css("cursor",'pointer');
   $( ".xero" ).click(function() {

   	var xero = $(this);
   	var location_code = $(this).attr('data-location');
   	var vendor_id = $(this).attr('data-vendor_id');
    $('.tdclass_'+vendor_id).html('<div class="loader"></div>');
    
   	var xeroUrl = '<?php echo XERO_URL; ?>createContactAllLocation.php?location_code='+location_code+'&vendor_id='+vendor_id;
	  $.get(xeroUrl, 
        function (response, status, error) {
        	if(validJSON(response)){
          		var data = JSON.parse(response);
				if(data.status == 'success'){
					xero.removeClass('xero');
					$('.tdclass_'+vendor_id).html('<span class="text">Success</span>');
				}else{
	              alert('Faild to send xero, Please try again.');
	              location.reload();
	            }
        	}else{
	            alert('Faild to send xero, Please try again.');
	            location.reload();
	        } 
	    });
	});
});
	
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

function filterReport() {
    $('#report_filter').submit();
}
</script>
