<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="content">
		<div class="form-list-container">
        	<?php if(!empty($error_alert)) { ?>
            	<?php foreach($error_alert as $alert) { ?>
                	<div class="attention"><?php echo $alert; ?></div>
                <?php } ?>	
            <?php } ?>
			<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="success"><?php echo $success; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<a href="<?php echo $insert; ?>" class="btn btn-action"><?php echo $button_insert; ?></a>
					<a onclick="$('form').submit();" class="btn btn-action"><?php echo $button_delete; ?></a>
				</div>
			</div>
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
                  <thead>
                    <tr>
                      <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                      <td class="left"><?php if ($sort == 'CG.cashier_group_name') { ?>
                        <a href="<?php echo $sort_group; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_group; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_group; ?>"><?php echo $column_group; ?></a>
                        <?php } ?></td>
                      <td class="left"><?php if ($sort == 'CA.cashier_name') { ?>
                        <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                        <?php } ?></td>
                        <td class="left"><?php if ($sort == 'CA.email') { ?>
                        <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                        <?php } ?></td>
                        <td class="left"><?php if ($sort == 'CA.status') { ?>
                        <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?></td>
                      <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($cashiers) { ?>
                    <?php foreach ($cashiers as $cashier) { ?>
                    <tr>
                      <td style="text-align: center;"><?php if ($cashier['selected']) { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $cashier['cashier_id']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $cashier['cashier_id']; ?>" />
                        <?php } ?></td>
                      <td class="left"><?php echo $cashier['group']; ?></td>
                      <td class="left"><?php echo $cashier['name']; ?></td>
                      <td class="left"><?php echo $cashier['email']; ?></td>
                      <td class="left"><?php echo $cashier['status']; ?></td>
                      <td class="right"><?php foreach ($cashier['action'] as $action) { ?>
                        [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                        <?php } ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td class="center" colspan="6"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
			</form>
		  <div class="pagination"><?php echo $pagination; ?></div>
		</div>
	  </div>
  </div>
</div>
<?php echo $footer; ?>