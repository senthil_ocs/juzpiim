<?php echo $header; ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  <div class="box">
  	<div class="content">
		<div class="form-information">
			<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
				</div>
			</div>
			<div class="form-new">
				<table cellspacing="0" class="form-list">
					<tbody>
						<tr>
							<td class="label">
								<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text validate[required]" placeholder="Enter your name">
								<?php if ($error_name) { ?>
									<span class="error"><?php echo $error_name; ?></span>
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="label">
								<label for="description"><?php echo $entry_description; ?></label>
							</td>
							<td class="value">
								<input type="text" name="description" value="<?php echo $description; ?>" class="input-text" placeholder="Enter description" />
								<?php if ($error_description) { ?>
									<span class="error"><?php echo $error_description; ?></span>
								<?php } ?>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>
</form>
<?php echo $footer; ?>