<?php echo $header; ?>
<div id="content">
  <div class="box">
    <div class="content">
		<div class="form-list-container">
			<?php if ($error_warning) { ?>
			<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<?php if ($success) { ?>
			<div class="success"><?php echo $success; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<a href="<?php echo $insert; ?>" class="btn btn-action"><?php echo $button_insert; ?></a>
					<a onclick="$('form').submit();" class="btn btn-action"><?php echo $button_delete; ?></a>
				</div>
			</div>
			<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
				<table class="list">
				  <thead>
					<tr>
					  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
					  <td class="left"><?php if ($sort == 'name') { ?>
						<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?></td>
					  <td class="left"><?php if ($sort == 'description') { ?>
						<a href="<?php echo $sort_description; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_description; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_description; ?>"><?php echo $column_description; ?></a>
						<?php } ?></td>
					  <td class="right"><?php echo $column_action; ?></td>
					</tr>
				  </thead>
				  <tbody>
					<?php if ($bins) { ?>
					<?php foreach ($bins as $bin) { ?>
					<tr>
					  <td style="text-align: center;"><?php if ($bin['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $bin['bin_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $bin['bin_id']; ?>" />
						<?php } ?></td>
					  <td class="left"><?php echo $bin['name']; ?></td>
					  <td class="left"><?php echo $bin['description']; ?></td>
					  <td class="right"><?php foreach ($bin['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
						<?php } ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="5"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
				  </tbody>
				</table>
			</form>
		  <div class="pagination"><?php echo $pagination; ?></div>
		</div>
	  </div>
  </div>
</div>
<?php echo $footer; ?>