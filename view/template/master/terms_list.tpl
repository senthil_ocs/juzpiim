<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">Terms Report</h3>
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				</ul>
				<div class="page-toolbar">
	                <div class="btn-group pull-right">
			            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
			              	<a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
	              			<a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
	            		</div>    
	          		</div>
                </div>
			</div>
			<!-- Filter Starts  -->
			<div class="innerpage-listcontent-blocks">
        		<div class="portlet bordernone" style="margin-bottom:0 !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <form method="post" name="report_filter" id="report_filter" action="">
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
		                        </td> 
		                        <td>
		              				<input type="text" placeholder='Code' name="filter_code" value="<?php echo $_POST['filter_code'] ; ?>" class="textbox" />
		                   		</td>                  
		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>  
		            <div class="tools" style="float:left;padding: 0 0 0 16px;">
		               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
		            </div>
		            </div> 
	              <?php if (($_POST['filter_date_from'] !="") || ($_POST['filter_date_to'] != "") || ($_POST['filter_days'] != "") || ($_POST['filter_status'] !="" )){ 
	           		 		$showHide  = 'style="display:block;"';
	        			} else {
	            			$showHide  = 'style = "display:none;"';
	        		}?>  
           <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: 0 !important; margin-left:15px; width: 15%;">
                <tbody>
                  <tr class="filter">
                  	<td>
		              	<input type="text" placeholder='Days' name="filter_days" value="<?php echo $_POST['filter_days'] ; ?>" class="textbox" />
		            </td>
                   	<td>
                   		<input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $_POST['filter_date_from'] ; ?>" class="textbox date">
            		</td>
              		<td>
                   		<input type="text" id='' placeholder='To Date' name="filter_date_to" value="<?php echo $_POST['filter_date_to'] ; ?>" class="textbox date">
            		</td> 
            		<td>
            			<select name="filter_status" class="textbox"  style="min-height: 35px; padding: 7px">
		                    <option value="">Select Status</option>
		                    <option value="1" <?php if( $filterstatus == 1) { ?> selected="selected" <?php } ?>>Enabled</option>
		                    <option value="0" <?php if( $filterstatus == '0') { ?> selected="selected" <?php } ?>>Disabled</option>
					    </select>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            </form>
           
            </div>
            </div>
             <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
			<div class="row">
				<div class="col-md-12">
					<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
		                <div class="innerpage-listcontent-blocks">
							<table class="table orderlist statusstock">
		                        <thead>
		                            <tr class="heading">
		                            	<td class="center">Name</td>
		                            	<td class="center">Code</td>
		                            	<td class="center">Days</td>
		                            	<td class="center">Status</td>
		                            	<td class="center">Date Added</td>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php if ($termsValues) { ?>
		                            <?php $class = 'odd'; ?>
									<?php foreach ($termsValues as $termsValue) {  ?>
									<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
									<tr class="<?php echo $class; ?>">
										<td class="left"><?php echo $termsValue['terms_name']; ?></td>
										<td class="left"><?php echo $termsValue['terms_code']; ?></td>
										<td class="left"><?php echo $termsValue['noof_days']; ?></td>
										<td class="left"><?php echo $termsValue['status']; ?></td>
										<td class="left"><?php echo $termsValue['date_added']; ?></td>
									</tr>
									<?php } ?>
									<?php } else { ?>
									<tr>
										<td align="center" colspan="5"><?php echo $text_no_results; ?></td>
									</tr>
									<?php } ?>
		                        </tbody>
		                   	</table>
		                </div>
	                </form>
	                <div class="pagination"><?php echo $pagination; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
	document.report_filter.submit();
}
</script>
<?php echo $footer; ?>
<!-- END NEW CODE -->