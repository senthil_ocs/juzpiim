<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">
    	<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
		<div class="page-content">
			<h3 class="page-title">Payment List</h3>
        	<div class="page-bar">
			<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo $home; ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>						
						<a href="<?php echo $settings; ?>">Settings</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Payment List</a>						
					</li>					
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                        	<?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                             <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div>
            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">                	      
                    <div class="innerpage-listcontent-blocks">             
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                        	<tr class="heading">
					  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>					  
					  <td class="center"><?php if ($sort == 'name') { ?>
						<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?></td>
						<td class="center"><?php if ($sort == 'position') { ?>
						<a href="<?php echo $sort_position; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_position; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_position; ?>"><?php echo $column_position; ?></a>
						<?php } ?></td>
						<td class="center"><?php if ($sort == 'status') { ?>
						<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
						<?php } ?></td>
					  <td class="right"><?php echo $column_action; ?></td>
					</tr>
                        </thead>
                        <tbody>
                        	<?php if ($payments) { ?>
                        	<?php $class = 'odd'; ?>
					<?php foreach ($payments as $payment) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;">
                      <?php if (!empty($payment['default_field'])) { ?>
                        &nbsp;
					  	<?php } elseif ($payment['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $payment['payment_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $payment['payment_id']; ?>" />
						<?php } ?>
                      </td>
					  <td><?php echo $payment['name']; ?></td>
					  <td><?php echo $payment['position']; ?></td>
					  <td><?php if ($payment['status']) { ?> Yes <?php } else { ?>No<?php } ?></td>
					  <td class="right">
					  	<?php if (!empty($payment['default_field'])) { ?>
                        	Default
                        <?php } else { ?>
							<?php foreach ($payment['action'] as $action) { ?>
                                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                            <?php } ?>
						<?php } ?>
                      </td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td class="center" colspan="7"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
               </div>
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>