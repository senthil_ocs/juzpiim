<?php echo $header; ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
  <div class="box">
  	<div class="content">
		<div class="form-information">
			<?php if ($error_warning) { ?>
				<div class="warning"><?php echo $error_warning; ?></div>
			<?php } ?>
			<div class="heading">
				<h1><?php echo $heading_title; ?></h1>
				<div class="action">
					<button class="btn btn-update" type="submit" title="Save"><?php echo $button_save; ?></button>
					<button class="btn btn-reset" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><?php echo $button_cancel; ?></button>
				</div>
			</div>
			<div class="form-new">
				<table cellspacing="0" class="form-list">
					<tbody>
						<tr>
							<td class="label">
								<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
							</td>
							<td class="value">
								<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="input-text validate[required]" placeholder="Enter your name">
								<?php if ($error_name) { ?>
                                    <span class="error"><?php echo $error_name; ?></span>
                                <?php } ?>
							</td>
						</tr>
                        <tr>
							<td class="label">
								<label for="access"><?php echo $entry_access; ?></label>
							</td>
							<td class="value">
								<div class="scrollbox">
									<?php $class = 'odd'; ?>
									<?php foreach ($permissions as $key=>$permission) { ?>
										<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
										<div class="<?php echo $class; ?>">
											<?php if (in_array($key, $access)) { ?>
												<input type="checkbox" name="permission[access][]" value="<?php echo $key; ?>" checked="checked" />
												<?php echo $permission; ?>
											<?php } else { ?>
												<input type="checkbox" name="permission[access][]" value="<?php echo $key; ?>" />
											<?php echo $permission; ?>
											<?php } ?>
										</div>
									<?php } ?>
								</div>
							</td>
						</tr>
                        <tr>
							<td class="label">
								<label for="remarks"><?php echo $entry_remarks; ?></label>
							</td>
							<td class="value">
                            	<textarea name="remarks" class="input-text" placeholder="Enter your remarks"><?php echo $remarks; ?></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>
</form>
<?php echo $footer; ?>