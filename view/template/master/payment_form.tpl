<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Payment</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo $settings; ?>">Settings</a>
                        <i class="fa fa-angle-right"></i>                      
                    </li>
                    <li>
                        <a href="<?php echo $payment; ?>">Payment List</a>
                        <i class="fa fa-angle-right"></i>                                             
                    </li>
                    <li>
                        <a href="#">Payment</a>                                                 
                    </li>                   
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
            <div style="clear:both"></div>         
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
										<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">	                                   
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="description"><?php echo $entry_description; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<textarea name="description" class="textbox" placeholder="Enter your description" class="textbox"> <?php echo $description; ?> </textarea>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="position"><?php echo $entry_position; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">			
                                 <input type="text" name="position" value="<?php echo $position; ?>" class="textbox" placeholder="Enter position">                              
                                </td>                                
                            </tr>                        
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%">
								<?php if ($status) { ?>
									<input type="radio" name="status" value="1" checked="checked" style="margin-left: -10px !important;">
									<?php echo $text_yes; ?>
									<input type="radio" name="status" value="0" style="margin-left: -10px !important;">
									<?php echo $text_no; ?>
								<?php } else { ?>
									<input type="radio" name="status" value="1" style="margin-left: -10px !important;" checked="checked">
									<?php echo $text_yes; ?>
									<input type="radio" name="status" value="0" style="margin-left: -10px !important;">
									<?php echo $text_no; ?>
								<?php } ?>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>