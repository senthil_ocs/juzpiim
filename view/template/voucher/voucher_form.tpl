<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Voucher</h3>  
            <div class="page-bar" >
               <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                          <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php } ?>
               </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="button" title="Save" class="btn btn-primary" onclick="submitForm();" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
            <div class="alert alert-block alert-danger fade in setting-success col-lg-12" style="width:97% !important; margin-left:15px !important"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button>
            </div>
            <?php } ?>
            <div style="clear:both"></div>
            <div class="row">
            	<!--24- Feb-2016!-->
              <div class="innerpage-listcontent-blocks">
                <div class="col-md-8">
                  <table class="table orderlist">
                    <tbody>                          
                      <tr>
                        <td width="35%"><label for="name">Type</label></td>
                        <td width="65%" class="order-nopadding">
                          <label><input type="radio" class="type" name="type" value="direct" id="Direct" checked="checked">Direct</label>
                          <label><input type="radio" class="type" name="type" value="range" id="Range">Range</label>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
                <div class="innerpage-listcontent-blocks">
                   	<div class="col-md-8">
                   		<table class="table orderlist" id="type-direct">
                   		  <thead>
	                        <tr><th colspan="2">Direct</th></tr>
	                      </thead>
                        <tbody>  
                          <tr>
                              <td width="20%" style="vertical-align: top;"><label for="name">Location<span class="required">*</span></label></td>
                              <td width="80%" class="order-nopadding">
                            <?php if($locations) {
                                  foreach ($locations as $count => $location) { 
                                      if($count % 3 ==0){ echo '<br>'; } 
                              ?>
                              <div class="col-md-6 col-xs-6 col-sm-6" >
                               <input type="checkbox" name="location[]" value="<?php echo $location['location_code'];?>" <?php if($location['location_code'] == $user_location_code) { echo 'checked'; } ?>> &nbsp;<?php echo $location['location_name']; ?>
                               <br><br>
                               </div> 
                                <?php  }
                              } ?>
                              </td>
                            </tr>                        
                          <tr>
                           <td width="35%"><label for="name">Voucher Code <span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding"> 
                                <?php if($error_voucher_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                                <input type="text" name="voucher_code" value="<?php echo $voucher_code; ?>" class="textbox requiredborder" placeholder="Voucher Code">
                              </div>
                              <?php } ?>

                              <?php if(!$error_voucher_code) { ?>
                              <input type="text" name="voucher_code" value="<?php echo $voucher_code; ?>" class="textbox" placeholder="Voucher Code">
                              <?php } ?>
                              </td>
                            </tr>                             
		                      <tr>
		                          <td width="35%"><label for="name">Voucher Amount <span class="required">*</span></label></td>
		                          <td width="65%" class="order-nopadding">
		                            <?php if($error_voucher_amount) { ?>
		                            <div class="input-icon right">
		                            <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Amount"></i>
		                            <input type="text" name="voucher_amount" id="voucher_amount" onkeypress="javascript:return isNumber(event)" value="<?php echo $voucher_amount; ?>" class="textbox requiredborder" placeholder="Voucher Amount">
		                          </div>
		                          <?php } ?>

		                          <?php if(!$error_voucher_amount) { ?>
		                          <input type="text" name="voucher_amount" id="voucher_amount" onkeypress="javascript:return isNumber(event)" value="<?php echo $voucher_amount; ?>" class="textbox" placeholder="Voucher Amount">
		                          <?php } ?>
		                          </td>
		                        </tr>
                            <tr>
                              <td width="35%"><label for="name">Min Amount </label></td>
                              <td width="65%" class="order-nopadding">
                              <input type="text" name="min_amount" id="min_amount" onkeypress="javascript:return isNumber(event)" value="<?php echo $min_amount; ?>" class="textbox" placeholder="Min Amount">
                           
                              </td>
                            </tr>
			                      <tr>
		                          <td width="35%"><label for="name">Valid From</label></td>
		                          <td width="65%" class="order-nopadding">
		                            <input type="text" name="valid_from" value="<?php echo $valid_from; ?>" autocomplete="off" class="date" placeholder="Valid From">
		                          </td>
		                        </tr>
                            <tr>
                              <td width="35%"><label for="name">Valid To</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="valid_to" value="<?php echo $valid_to; ?>" autocomplete="off" class="date" placeholder="Valid To">
                              </td>
                            </tr>
		                        <tr>
		                          <td width="35%"><label for="name">Status</label></td>
		                            <td width="65%" class="order-nopadding1">
                                    <select name="is_active" class="selectpicker selectdropdown">                             
                                        <option value="0" <?php if ($is_active == 0) { ?> selected="selected" <?php } ?>><?php echo 'Inactive'; ?></option>
                                        <option value="1" selected<?php if ($is_active == 1) { ?> selected="selected" <?php } ?>><?php echo 'Active'; ?></option>
                                    </select>
                                </td>
		                        </tr>
                        </tbody>
                      </table>
                      <table class="table orderlist" id="type-range" style="display: none;">
                        <thead>
                          <tr><th colspan="2">Range</th></tr>
                        </thead>
                        <tbody>                          
                          <tr>
                           <td width="35%"><label for="name">Range Start Code <span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding"> 
                                <?php if($error_from_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                                <input type="text" name="from_code" id="from_code" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_code; ?>" class="textbox requiredborder" placeholder="Range Start Code">
                              </div>
                              <?php } ?>

                              <?php if(!$error_from_code) { ?>
                              <input type="text" name="from_code" id="from_code" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_code; ?>" class="textbox" placeholder="Range Start Code">
                              <?php } ?>
                              </td>
                            </tr>                             
                          <tr>
                          <tr>
                           <td width="35%"><label for="name">Range End Code <span class="required">*</span></label></td>
                                <td width="65%" class="order-nopadding"> 
                                <?php if($error_to_code) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Code"></i>
                                <input type="text" name="to_code" id="to_code" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_code; ?>" class="textbox requiredborder" placeholder="Range End Code">
                              </div>
                              <?php } ?>

                              <?php if(!$error_to_code) { ?>
                              <input type="text" name="to_code" id="to_code" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_code; ?>" class="textbox" placeholder="Range End Code">
                              <?php } ?>
                              </td>
                            </tr>  
                            <tr>
                              <td width="35%"><label for="name">Prefix</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="prefix" value="<?php //echo $valid_from; ?>" class="textbox" placeholder="Enter Prefix">
                              </td>
                            </tr>                           
                          <tr>
                              <td width="35%"><label for="name">Voucher Amount <span class="required">*</span></label></td>
                              <td width="65%" class="order-nopadding">
                                <?php if($error_voucher_amount1) { ?>
                                <div class="input-icon right">
                                <i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Amount"></i>
                                <input type="text" name="voucher_amount1" id="voucher_amount1" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_amount; ?>" class="textbox requiredborder" placeholder="voucher Amount">
                                </div>
                              <?php } ?>

                              <?php if(!$error_voucher_amount1) { ?>
                              <input type="text" name="voucher_amount1" id="voucher_amount1" onkeypress="javascript:return isNumber(event)" value="<?php //echo $voucher_amount; ?>" class="textbox" placeholder="Voucher Amount">
                              <?php } ?>
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Valid From</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="valid_from1" autocomplete="off" value="<?php //echo $valid_from; ?>" class="date" placeholder="Valid From">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Valid To</label></td>
                              <td width="65%" class="order-nopadding">
                                <input type="text" name="valid_to1" autocomplete="off" value="<?php //echo $valid_to; ?>" class="date" placeholder="Valid To">
                              </td>
                            </tr>
                            <tr>
                              <td width="35%"><label for="name">Status</label></td>
                                <td width="65%" class="order-nopadding1">
                                    <select name="is_active1" class="selectpicker selectdropdown">                  
                                        <option value="0" <?php if ($is_active == 0) { ?> selected="selected" <?php } ?>><?php echo 'Inactive'; ?></option>
                                        <option value="1" selected<?php if ($is_active == 1) { ?> selected="selected" <?php } ?>><?php echo 'Active'; ?></option>
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                      </table>                     
                    </div>
           		</div>
        	</form>
         </div>
    </div>
</div>

<?php echo $footer; ?>
<script type="text/javascript">

  function submitForm(){
  var checked = []
    $("input[name='location[]']:checked").each(function ()
    {
        checked.push(parseInt($(this).val()));
    });
    if(checked !=0){
      $("#formID").submit();
    }else{
       alert("Enter form Mandatory Details");
       return false;
    }
  }

  $('.date').datepicker({ dateFormat: 'yy-mm-dd' }).val()({
    changeMonth: true,
    changeYear: true,
    yearRange: '1980:'+(new Date).getFullYear() 
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
   $('input[type="radio"]').click(function() {
       if($(this).attr('id') == 'Direct') { 
            $('#type-direct').show();           
            $('#type-range').hide();           
       }
       else if($(this).attr('id') == 'Range') {
            $('#type-range').show();   
            $('#type-direct').hide();   
       }
   });
});
  function isNumber(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31 
        && (charCode < 48 || charCode > 57))
         return false;
        $('#errmsg').show();
      return true;
   }
</script>