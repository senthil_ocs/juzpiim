<?php echo $header; ?>
<head>
    <link href="view/assets/chart/css/line_chart.css" rel="stylesheet" type="text/css">
</head>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
          <h5 class="txt text-primary">Date wise Purchase amount (Sub Total)</h5>
          <!-- <div id="chartdiv"></div> -->
    </div>
  </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="view/assets/chart/js/core.js"></script>
<script src="view/assets/chart/js/charts.js"></script>
<script src="view/assets/chart/js/animated.js"></script>
<script type="text/javascript">

var data = <?php echo $chartData; ?>;
am4core.useTheme(am4themes_animated);
var chart = am4core.createFromConfig({
  // Set settings and data
  "paddingRight": 40,
  "data": data,

  // Create X axes
  "xAxes": [{
    "type": "DateAxis",
    "renderer": {
      "grid": {
        "location": 0
      }
    }
  }],

  // Create Y axes
  "yAxes": [{
    "type": "ValueAxis",
    "tooltip": {
      "disabled": true
    },
    "renderer": {
      "minWidth": 35
    }
  }],

  // Create series
  "series": [{
    "id": "s1",
    "type": "LineSeries",
    "dataFields": {
      "dateX": "date",
      "valueY": "value"
    },
    "tooltipText": "{valueY.value}"
  }],

  // Add cursor
  "cursor": {
    "type": "XYCursor"
  },

  // Add horizontal scrollbar
  "scrollbarX": {
    "type": "XYChartScrollbar",
    "series": ["s1"]
  }
}, "chartdiv", "XYChart");
</script>
<?php echo $footer; ?>