<?php echo $header; ?>
<head>
    <link href="view/assets/chart/css/column_chart.css" rel="stylesheet" type="text/css">
</head>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <ul class="nav nav-tabs" style="margin: 0px 6%;">
              <li class="txt text-danger"><h4>Purchase</h4></li>
              <li id="purchase_week"><a> Week </a></li>
              <li id="purchase_month"><a> Month </a></li>
              <li id="purchase_year"><a> Year </a></li>
            </ul>
            <div id="purchase" class="commonChart"></div>
        </div>
        <div class="row">
            <ul class="nav nav-tabs" style="margin: 0px 6%;">
              <li class="txt text-danger"><h4>Purchase Return</h4></li>
              <li id="purchaseReturn_week"><a> Week </a></li>
              <li id="purchaseReturn_month"><a> Month </a></li>
              <li id="purchaseReturn_year"><a> Year </a></li>
            </ul>
            <div id="purchaseReturn" class="commonChart"></div>
        </div>
        <div class="row">
            <ul class="nav nav-tabs" style="margin: 0px 6%;">
              <li class="txt text-danger"><h4>Sales</h4></li>
              <li id="sales_week"><a> Week </a></li>
              <li id="sales_month"><a> Month </a></li>
              <li id="sales_year"><a> Year </a></li>
            </ul>
            <div id="sales" class="commonChart"></div>
        </div>
        <div class="row">
            <ul class="nav nav-tabs" style="margin: 0px 6%;">
              <li class="txt text-danger"><h4>Sales Return</h4></li>
              <li id="salesReturn_week"><a> Week </a></li>
              <li id="salesReturn_month"><a> Month </a></li>
              <li id="salesReturn_year"><a> Year </a></li>
            </ul>
            <div id="salesReturn" class="commonChart"></div>
          </div>
    </div>
  </div>
</div>

<input type="hidden" id="purchase_hidden_value">
<input type="hidden" id="purchaseReturn_hidden_value">
<input type="hidden" id="sales_hidden_value">
<input type="hidden" id="salesReturn_hidden_value">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="view/assets/chart/js/core.js"></script>
<script src="view/assets/chart/js/charts.js"></script>
<script src="view/assets/chart/js/animated.js"></script>
<script src="view/assets/chart/js/column_chart.js"></script>

<script type="text/javascript">
  
  $(document).ready(function(){
      defaultActiveTabs();
      purchaseChart(getPurchaseData('week','purchase'));
      purchaseReturnChart(getPurchaseData('week','purchaseReturn'));
      salesChart(getSalesData('week','sales'));
      salesReturnChart(getSalesData('week','sales'));
  });

$('#purchase_week').click(function(){
    $('#purchase_month').removeClass('active');
    $('#purchase_year').removeClass('active');
    $(this).addClass('active');
    purchaseChart(getPurchaseData('week','purchase'));
});
$('#purchase_month').click(function(){
    $('#purchase_week').removeClass('active');
    $('#purchase_year').removeClass('active');
    $(this).addClass('active');
    purchaseChart(getPurchaseData('month','purchase'));
});
$('#purchase_year').click(function(){
    $('#purchase_week').removeClass('active');
    $('#purchase_month').removeClass('active');
    $(this).addClass('active');
    purchaseChart(getPurchaseData('year','purchase'));
});


$('#purchaseReturn_week').click(function(){
    $('#purchaseReturn_month').removeClass('active');
    $('#purchaseReturn_year').removeClass('active');
    $(this).addClass('active');
    purchaseReturnChart(getPurchaseData('week','purchaseReturn'));
});
$('#purchaseReturn_month').click(function(){
    $('#purchaseReturn_week').removeClass('active');
    $('#purchaseReturn_year').removeClass('active');
    $(this).addClass('active');
    purchaseReturnChart(getPurchaseData('month','purchaseReturn'));
});
$('#purchaseReturn_year').click(function(){
    $('#purchaseReturn_week').removeClass('active');
    $('#purchaseReturn_month').removeClass('active');
    $(this).addClass('active');
    purchaseReturnChart(getPurchaseData('year','purchaseReturn'));
});

$('#sales_week').click(function(){
    $('#sales_month').removeClass('active');
    $('#sales_year').removeClass('active');
    $(this).addClass('active');
    salesChart(getSalesData('week','sales'));
});
$('#sales_month').click(function(){
    $('#sales_week').removeClass('active');
    $('#sales_year').removeClass('active');
    $(this).addClass('active');
    salesChart(getSalesData('month','sales'));
});
$('#sales_year').click(function(){
    $('#sales_week').removeClass('active');
    $('#sales_month').removeClass('active');
    $(this).addClass('active');
    salesChart(getSalesData('year','sales'));
});

$('#salesReturn_week').click(function(){
    $('#salesReturn_month').removeClass('active');
    $('#salesReturn_year').removeClass('active');
    $(this).addClass('active');
    salesReturnChart(getSalesData('week','salesReturn'));
});
$('#salesReturn_month').click(function(){
    $('#salesReturn_week').removeClass('active');
    $('#salesReturn_year').removeClass('active');
    $(this).addClass('active');
    salesReturnChart(getSalesData('month','salesReturn'));
});
$('#salesReturn_year').click(function(){
    $('#salesReturn_week').removeClass('active');
    $('#salesReturn_month').removeClass('active');
    $(this).addClass('active');
    salesReturnChart(getSalesData('year','salesReturn'));
});

function defaultActiveTabs(){
  $('#purchase_week').addClass('active');
  $('#purchaseReturn_week').addClass('active');
  $('#sales_week').addClass('active');
  $('#salesReturn_week').addClass('active');
}

function getPurchaseData(type,ptype){
  var response= [];
    $.ajax({
        type: 'POST',
        url : '<?php echo HTTP_SERVER; ?>index.php?route=common/home/getPurchaseChartData&token=<?php echo $this->request->get["token"]; ?>',
        data : {type:type,ptype:ptype},

        success:function(resp){
          response = JSON.parse(resp);
        },
        async: false
    });
    return response;
}

function getSalesData(type,stype){
  var response= [];
    $.ajax({
        type: 'POST',
        url : '<?php echo HTTP_SERVER; ?>index.php?route=common/home/getSalesChartData&token=<?php echo $this->request->get["token"]; ?>',
        data : {type:type,stype:stype},

        success:function(resp){
          response = JSON.parse(resp);
        },
        async: false
    });
    return response;
}
</script>
<?php echo $footer; ?>