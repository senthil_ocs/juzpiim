<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Product List</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo $home; ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>						
						<a href="<?php echo $inven_comm; ?>">Product</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo $inventory; ?>">Product List</a>						
					</li>					
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                  	<?php
                  	//if($add_inventory=='1'){ ?>
                    <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> ADD PRODUCT</span></button></a>
                    <?php //} ?> 
                 		<?php if($this->user->hasPermission('modify', $route)) { ?>

           				<?php } ?>
           				</div>    
                    </div>
                </div>               						
			</div>			
			<div style="clear:both"></div>
			
		 <!-- NEW SEARCH -->
           <div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
            	<div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            		<div class="caption">
            			<table class="table orderlist statusstock" style="margin: -6px 0 0 30px;">
            				<tr class="filter"> 
            					<td>
            						<select name="filter_location_code" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
			                            <option value="">-- Select Location --</option>
			                            <?php
			                               if(!empty($Tolocations)){
			                                    foreach($Tolocations as $value){ ?>
			                                    <option value="<?php echo $value['location_code'];?>" <?php if($filter_location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

			                            <?php  } } ?> 
			                        </select>
            					</td>
            					<td>
            						<select name="filter_department" id="filter_department" class="selectdropdown" style="min-height: 35px; padding: 7px; width: 100%;">
	                                	<option value=""> Select Department </option>
	                                    <?php foreach ($department_collection as $department) { ?>
	                                    	<?php if (trim($department['department_code']) == trim($filter_department)) { ?>
	                                        	<option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
	                                        <?php } else { ?>
	                                        	<option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>	
	                                        <?php } ?>   
	                                    <?php } ?>
	                                </select> 
            					</td> 
            					<td>
            						<select name="filter_category" id="filter_category" class="selectdropdown" style="min-height: 35px; padding: 7px; width: 100%;">
                                    	<option value=""> Select Category </option>
                                    </select>
            					</td> 
            					<td>
            						<select name="filter_brand" id="filter_brand" class="selectdropdown" style="min-height: 35px; padding: 7px; width: 100%;">
                                    	<option value=""> Select Brand </option>
                                    	<?php if (!empty($brand_collection)) { ?>
		                                  <?php foreach ($brand_collection as $brand) { ?>
		                                      <?php if (trim($brand['brand_code']) == $_GET['filter_brand']) { ?>
		                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
		                                      <?php } else { ?>
		                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
		                                      <?php } ?>   
		                                  <?php } ?>
		                              <?php } ?>
                                    </select>
            					</td> 
            					<td>
            						<select name="filter_vendor" id="filter_vendor" class="selectdropdown" style="min-height: 35px; padding: 7px;">
                                    	<option value=""> Select Vendor </option>
                                    	 <?php if (!empty($vendor_collection)) { ?>
	                                    <?php foreach ($vendor_collection as $vendor) { ?>
	                                        <?php if (trim($vendor['vendor_code']) == $_GET['filter_vendor']) { ?>
	                                            <option value="<?php echo $vendor['vendor_code']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
	                                        <?php } else { ?>
	                                            <option value="<?php echo $vendor['vendor_code']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
	                                        <?php } ?>   
	                                    <?php } ?>
	                                <?php } ?>
                                    </select>
            					</td> 
            					<!-- <td>
            						<select name="filter_sub_category" id="filter_sub_category" class="selectdropdown">
                                    	<option value=""> Select Sub Category </option>
                                    </select>
            					</td> -->
            					
            					<td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" 
            					placeholder="Product Name" autocomplete="off">
            					<input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $filter_product_id; ?>">
            					
                                <div id="suggesstion-box" class="auto-compltee"></div>
            					</td>

            					<!-- <td><input type="text" name="filter_barcodes" id="filter_barcodes" value="<?php echo $filter_barcodes; ?>" class="textbox" placeholder="Scan Barcode">
            						
            					</td> -->
								<td>
									<select name="filter_status" id="filter_status" class="selectdropdown" style="min-height: 35px; padding: 7px;">
										<option value="1"<?php if($filter_status=='1')echo 'selected'; ?>><?php  echo 'Enabled';?></option> 	
										<option value="0"<?php if($filter_status=='0')echo 'selected'; ?>><?php  echo 'Disabled';?></option>
										<option value="is"<?php if($filter_status=='is')echo 'selected'; ?>>In Stock</option>
										<option value="os"<?php if($filter_status=='os')echo 'selected'; ?>>Out Stock</option>
									</select>
								</td>
            					<td>
            						<!-- <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $filter_sku; ?>" class="textbox"> -->
            						<input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $filter_product_id; ?>" class="textbox">

            						<button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
            					</tr> 
            			</table>		
            		</div>	
            	</div>
            </div>
        </div>
                	<!-- END NEW SEARCH -->	
                	 <div style="clear:both; margin: 0 0 15px 0;">
                    </div>			 
			<div class="row">
					<div class="col-md-12">
					<?php if ($error_warning) { ?>
					<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
		             </div>
					<?php } ?>
					<?php if ($success) { ?>			
						<div class="alert alert-block alert-success fade in setting-success" style="margin: 10px;"><?php echo $success; ?>
							<button type="button" class="close" data-dismiss="alert"></button>
		                </div>
					<?php } ?>
					</div>
				<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks">
				<table class="table orderlist statusstock" id="table_sort">
                    <thead>
                      <tr class="heading">
						  <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
						  <td class="center">Product Id</td>
						  <td class="center"><?php echo $column_name; ?></td>	
						  <td class="left">Cost</a></td>
						  <td class="left"><?php echo $column_price; ?></a></td>
						  <td class="center"><?php echo $column_quantity; ?></td>
						  <td class="left"><?php echo $column_status; ?></td>
					  	  <td class="text-center">Sales Channel</td>
					      <td class="right"><?php echo $column_action; ?></td>
					  </tr>
                     </thead>
                    <tbody>                        
					<?php if ($products) { ?>
					  <?php $class = 'odd'; ?>
					<?php foreach ($products as $product) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					  <td style="text-align: center;"><?php if ($product['selected']) { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
						<?php } else { ?>
						<input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
						<?php } ?>
					</td>
					  <td class="center"><?php echo $product['sku']; ?></td>
                      <td class="center"><?php echo $product['name']; ?></td>
                      <td class="center"><?php echo $product['sku_cost']; ?></td>
                      
					  <td class="right"><?php if ($product['special']) { ?>
						<span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
						<span style="color: #b00;"><?php echo $product['special']; ?></span>
						<?php } else { ?>
						<?php echo $product['price']; ?>
						<?php } ?></td>
					  <td class="center"><?php if ($product['quantity'] <= 0) { ?>
						<span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
						<?php } elseif ($product['quantity'] <= 5) { ?>
						<span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
						<?php } else { ?>
						<span style="color: #008000;"><?php echo $product['quantity']; ?></span>
						<?php } ?></td>
					  <td class="left"><?php echo $product['status']; ?><br>
					  	<?php if($product['quantity'] > 0){ ?>
						  <h6><span class="label label-success" style="background-color: #0fbb34;">In-Stock</span></h6>
					  	<?php }else{ ?>
						  <h6><span class="label label-danger">Out-Stock</span></h6>
					  	<?php } ?>
						</td>
						<td style="text-align: center;"><?php foreach ($product['market_place'] as $market_place) { ?>[ <a class="<?php echo $market_place['btn_log']; ?>" href="<?php echo $market_place['href']; ?>">Channel</a> ]<?php } ?></td>
					  <td class="right"><?php foreach ($product['action'] as $action) { ?>
						[ <a class="<?php echo $action['btn_log']; ?>" href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ] <?php } foreach ($product['multiple_images'] as $multiple_images) { ?> [ <a class="<?php echo $multiple_images['btn_log']; ?>" href="<?php echo $multiple_images['href']; ?>"><?php echo $multiple_images['text']; ?></a> ]
						<?php }  if($product['delitable']){?> 
							&nbsp;<a onclick="deleteProduct('<?php echo $product['product_id']; ?>');"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete This!" style="font-size: 16px;color: red;"></i></a>
						<?php } ?>
						</td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="11"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<script type="text/javascript">
function filter() {
	url = 'index.php?route=inventory/inventory&token=<?php echo $token; ?>';
	
	var filter_name = $('#filter_name').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_sku = $('#filter_sku').attr('value');
	if (filter_sku) {
		url += '&filter_sku=' + encodeURIComponent(filter_sku);
	}
	var filter_product_id = $('#filter_product_id').attr('value');
	if (filter_product_id) {
		url += '&filter_product_id=' + encodeURIComponent(filter_product_id);
	}

	//var filter_department = $('input[name=\'filter_department\']').attr('value');
	var filter_department = $('#filter_department').attr('value');
	if (filter_department) {
		url += '&filter_department=' + encodeURIComponent(filter_department);
	}

	//var filter_category = $('input[name=\'filter_category\']').attr('value');
	var filter_category = $('#filter_category').attr('value');
	if (filter_category) {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}

	//var filter_sub_category = $('input[name=\'filter_sub_category\']').attr('value');
	var filter_sub_category = $('#filter_sub_category').attr('value');
	if (filter_sub_category) {
		url += '&filter_sub_category=' + encodeURIComponent(filter_sub_category);
	}

	var filter_barcodes = $('#filter_barcodes').attr('value');
	if (filter_barcodes) {
		url += '&filter_barcodes=' + encodeURIComponent(filter_barcodes);
	}
	var filter_brand = $('#filter_brand').attr('value');
	if (filter_brand) {
		url += '&filter_brand=' + encodeURIComponent(filter_brand);
	}
	var filter_vendor = $('#filter_vendor').attr('value');
	if (filter_vendor) {
		url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
	}

	var filter_location_code = $('#filter_location_code').attr('value');
	if (filter_location_code) {
		url += '&filter_location_code=' + encodeURIComponent(filter_location_code);
	}
	var filter_status = $('#filter_status').attr('value');
	if (filter_status) {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	location = url;
}

$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
$(document).ready(function() {
	getCategory();
	setTimeout(function(){ getSubcategory(); }, 1500);
	
	$('#table_sort').dataTable({
		"aaSorting" : [[ 1, "asc" ]],
		   paging : false,
		searching : false,
		  "bInfo" : false,
		"columnDefs": [ {
		    "targets": [0,6,7,8],
		    "orderable": false
		  },
		  {
		  "targets": "_all",
		  "defaultContent": "-"
		} ]
	});
});
$('#filter_department').change(function(){
    getCategory();
});
function getCategory(){
    var department_code = $('#filter_department').val();
	var category = '<?php echo $filter_category; ?>';

    if(department_code!=''){
        $.ajax({
            type : 'POST',
            url: 'index.php?route=inventory/inventory/ajaxGetCategory&token=<?php echo $token; ?>',
            data: { department_code:department_code,category:category},
            success:function(data){
                $('#filter_category').html(data);
            } 
        });
    } else {
        $('#filter_category').html('<option value="">Select Category</option>');
    	getSubcategory();
    }
}
$('#filter_category').change(function(){
	getSubcategory();
});
function getSubcategory(){
	var cate = $('#filter_category').val();
	var subcate = '<?php echo $filter_sub_category; ?>';

	if(cate!=''){
		$.ajax({
			type : 'POST',
			url: 'index.php?route=inventory/inventory/ajaxGetSubcategory&token=<?php echo $token; ?>',
			data: { cate:cate,subcate:subcate},
			success:function(data){
				$('#filter_sub_category').html(data);
			} 
		});
	} else {
        $('#filter_sub_category').html('<option value="">Select Sub Category</option>');
    }
}
function getkeyCode(){
	$(document).keydown(function(e){
	    if (e.keyCode == 37) { 
	       alert( "left pressed" );
	       return false;
	    }
	});
}
/*
var chosen = "";
$('#filter_name').keydown(function(e) {
	if (e.keyCode == 40) { 
		if(chosen === "") {
            chosen = 0;
        } else if((chosen+1) < $('ul#ui-autocomplete li').length) {
            chosen++; 
        }
        $('ul#ui-autocomplete li').removeClass('selected');
        $('ul#ui-autocomplete li:eq('+chosen+')').addClass('selected');
        return false;
    }
    if (e.keyCode == 38) { 
        if(chosen === "") {
            chosen = 0;
        } else if(chosen > 0) {
            chosen--;            
        }
        $('ul#ui-autocomplete li').removeClass('selected');
        $('ul#ui-autocomplete li:eq('+chosen+')').addClass('selected');
        return false;
    }
});
*/
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'html',
			success: function(str) {
				if(str){
					$('#filter_product_id').val('');
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);
				}else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
				}
			}
		});
	} 
}); 

function getValue(strhtml){
	$('#filter_name').val(strhtml)
	$('#suggesstion-box').hide();
}

function getValue1(strhtml,sku,product_id){
    $("#filter_name").val(strhtml.replace("^","'").replace('!!','"'));
	// $('#filter_sku').val(sku.replace("^","'").replace('!!','"'));
	$('#filter_product_id').val(product_id);
	$('#suggesstion-box').hide();
}

$(document).ready(function(){
	$('.xero').css("cursor",'pointer');
});

function deleteProduct(product_id){
	if(confirm('Are you sure, Delete this?')){
		if(product_id !=''){
			$.ajax({
				type: 'POST',
				url: 'index.php?route=inventory/inventory/deleteProduct&token=<?php echo $token; ?>',
				data: { product_id : product_id},
				success: function(res) {
          			var res = JSON.parse(res);
					if(res.status){
						location.reload();
					}else{
	                    alert(res.msg);
					}
				}
			});
		}
	}
}

$(".xero" ).click(function() {
	var xero = $(this);
	var product_id = $(this).attr('data-product_id');
	var xeroUrl = '<?php echo XERO_URL; ?>createItem.php?product_id='+product_id;

    $.get(xeroUrl,
        function (response, status, error) {
          var error = error.responseText.replace(/(<([^>]+)>)/ig,"").trim();
          if(error !=''){
            console.log(error);
            $('.tdclass_'+order_id).html('<div>Faild</div>');
          } 
          var data = JSON.parse(response);
          if(data.status == 'success'){
            xero.removeClass('xero');
            $('.tdclass_'+order_id).html('<span class="text">Success</span>');
          }
    });
 });
</script> 
