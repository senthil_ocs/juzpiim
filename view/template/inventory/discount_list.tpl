<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    
	<div class="page-content-wrapper">
    	<div class="page-content">
			<h3 class="page-title">Pair Discount List</h3>
        	<div class="page-bar">
			<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                        	<?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                           <!--   <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a> -->
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
		       </form>
			<!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              
              <form method="post"  name="list_filter" id="movement_report">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $_REQUEST['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $_REQUEST['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                          <td>
                             <select name="filter_location" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                              <?php if(count($Tolocations)>=2){?>
                              <?php }?>
                              <?php
                                 if(!empty($Tolocations)){
                                      foreach($Tolocations as $value){ ?>
                                      <option value="<?php echo $value['location_code'];?>" <?php if($filter_location == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                              <?php  } } ?> 
                          </select>
                          </td>
                          <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                            <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                          <td align="center" colspan="4">
                            <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>">
                             <!-- <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('sales_detail');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button> -->
                             <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit"  class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button> 
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
            </div>     
          

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
        </div>  
            <!--  -->     
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">                	      
                    <div class="innerpage-listcontent-blocks">             
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                      <tr class="heading">
					   
					  <td class="left"><?php if ($sort == 'discount_code') { ?>
						<a href="<?php echo $sort_discount_code; ?>" class="<?php echo strtolower($order); ?>">Code</a>
						<?php } else { ?>
						<a href="<?php echo $sort_discount_code; ?>">Code</a>
						<?php } ?></td>
					  	
					  	<td class="left"><?php if ($sort == 'discount_name') { ?>
						<a href="<?php echo $sort_discount_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
						<?php } else { ?>
						<a href="<?php echo $sort_discount_name; ?>"><?php echo $column_name; ?></a>
						<?php } ?></td>
            <td class="left">
              <a href="<?php echo $sort_discount_name; ?>">Price</a>
            </td>

						<td class="left">
							<a href="<?php echo $sort_discount_name; ?>">Qty</a>
						</td>
					  <td class="left"><?php echo 'From Date'; ?></td>	
					  <td class="left"><?php echo 'To Date'; ?></td>
					   <td class="left">Created Date</td>
					  <td class="right"><?php echo $column_action; ?></td>
					</tr>
                        </thead>
                        <tbody>
                        	<?php if ($discounts) { ?>
                        	<?php $class = 'odd'; ?>
					<?php foreach ($discounts as $discount) { ?>					
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
						<td class="left"><?php echo $discount['pair_discount_code']; ?></td>
					  <td class="left"><?php echo $discount['pair_discount_description']; ?></td>
					  <td class="left"><?php echo $discount['pair_discount_price']; ?></td>
					  <td class="left"><?php echo $discount['pair_discount_qty']; ?></td>
					  <td class="left"><?php echo $discount['pair_discount_from']; ?></td>
					  <td class="left"><?php echo $discount['pair_discount_to']; ?></td>
					  <td class="left"><?php echo $discount['createdon']; ?></td>
					  <td class="right"><?php foreach ($discount['action'] as $action) { ?>
						[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
            [ <a class="discountdelete" data-href="<?php echo $action['delete']; ?>"><?php echo 'Delete'; ?></a> ]
						<?php } ?></td>
					</tr>
					<?php } ?>
					<?php } else { ?>
					<tr>
					  <td align="center" colspan="8"><?php echo $text_no_results; ?></td>
					</tr>
					<?php } ?>
                        </tbody>
                    </table>
                   </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
               </div>
            </div>
		</div>
	</div>
    </form>
</div>

<?php echo $footer; ?>
<script type="text/javascript">
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});

function filterReport(report_type) {
  document.report_filter.submit();
}

/**/
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});
  
function getProductautoFill(sku) {
  var filter_name = $("#filter_name").val();
   
   if(filter_name ==''){
    $('#filter_sku').val('');
   }

    if(sku.length<=3){
        return false;
    }


    var location = $("#filter_location").val();
    var ajaxData = 'sku='+sku+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //alert("Product Not Available or Disabled.");
                //$("#suggesstion-box").hide();
               // clearPurchaseData();
               // return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
        var newVal = val.split("||");
        //alert(newVal);
       //console.log(newVal);
        $('#filter_name').val(newVal[2]);
        $('#filter_sku').val(newVal[1]);
        $('#suggesstion-box').hide();
        if(cnt=='1'){
            $( "#movement_report" ).submit();
        }

    }

function getValue(name,sku){
  console.log(sku);
  
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}

$('.discountdelete').click(function (event) {
    if (confirm('Are you sure you want to Delete this?')) {
        var url = $(this).attr('data-href');
        $.ajax({
            url: url,
            type: "GET",
            success: function () {
              location.reload();
            }
        });
    }
});

</script>