<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Barcode</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				        </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" id="save_btn"><i class="fa  fa-print"></i> <?php echo 'Print'; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Clear</button></a></div>    
                    </div>
                </div>                            
            </div>
            <div class="row">
            <div style="clear:both"></div>         
            <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_bar_code; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<input type="text" name="bar_code" id="bar_code" value="<?php echo $bar_code; ?>" onchange="getInventoryDetail(this.value,'inventory_code','name','price');" class="textbox" placeholder="Enter Bar Code" autofocus>					 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_inventory_code; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
								<input type="text" name="inventory_code" id="inventory_code" value="<?php echo $inventory_code; ?>" class="textbox" readonly>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                               <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" readonly>             
                                </td>                                
                            </tr>
							<tr>
							<td width="20%">
								<label for="name"><?php echo $entry_price; ?></label>
							</td>
							<td width="80%" class="order-nopadding">
								<input type="text" name="price" id="price" value="<?php echo $price; ?>" class="textbox" readonly>
							</td>
						</tr>
						<tr>
							<td width="20%">
								<label for="name"><?php echo $entry_remarks; ?></label>
							</td>
							<td width="80%" class="order-nopadding">
								<input type="text" name="remarks" id="remarks" value="" class="textbox">
							</td>
						</tr>
						<tr>
							<td width="20%">
								<label for="name"><?php echo $entry_labels; ?></label>
							</td>
							<td width="80%" class="order-nopadding">
								<input type="text" name="no_labels" id="no_labels" value="1" class="textbox">
							</td>
						</tr>
         </tbody>
      </table>
            <!-- BARCODE PRINT --> 
            <div id="printable">   
              <div style="width:60%;margin:0 auto;">
                <?php if (!empty($info)) { ?>
                  <?php for ($i = 0; $i < $info['no_labels']; $i++) { ?>
                    <div style="float:left;display:inline;padding:0px 15px;">
                        <span><strong style="text-align:center;">Receipt Printer</span>
                        <p style="font-weight:normal;"><img src="<?php echo SITE_URL;?>code128.php?text=<?php echo $info['bar_code']; ?>" /> <br/><?php echo $info['bar_code'];?></p>
                        <p><strong style="text-align:center;"> <?php echo $info['price']; ?> </strong></p> 
                    </div>   
                  <?php } ?>                             
                  <?php } else { ?>              
                      <div>
                         <p><?php echo $text_no_results; ?></p>
                      </div>
                  <?php } ?>

              </div>
          </div>
          
           <?php if (!empty($info)) { ?>
          <div style="width:100%; text-align:center;float:left;">
          <a align="center" class="btn btn-zone btn-primary" onClick="printDiv('printable')" id="printbtn"><i class="fa fa-print"></i> Print</a>
        </div>
        <?php } ?>          
        </div>
      </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">

  function getInventoryDetail(v,id,id1,id2){                   
              $.ajax({
                  url:"index.php?route=inventory/barcode/getDetails&token=<?php echo $token; ?>",
                  type:"POST",
                  data:{bar_code:v},
                  success:function(out){
                      out = JSON.parse(out);                            
                      if(out == ''){
                        alert('Enter a valid barcode');
                        $("#bar_code").focus();
                      } else {                              
                      document.getElementById(id).value = out['sku'];
                      document.getElementById(id1).value = out['name'];
                      document.getElementById(id2).value = out['price'];

                    }                           
                  }
              });
          }

    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
    }
</script>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function() {
    Mousetrap.bind('alt+p', function(e) {
       e.preventDefault();
       $("#printbtn").trigger('click',function(){
       });
   });
});
</script>