<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">
    	
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                             <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div> 

            <div class="row">

            	  <div class="page-bar innerpage-listcontent-blocks" style="margin-top: -26px; min-width: 100%; border-top:1px solid #ccc;">
                    <div class="portlet bordernone" style="margin-bottom:0 !important">
                        <div class="page-bar portlet-title" style="min-height: 45px; min-width: 100%; margin: 0; padding: 15px 0 0 0;">
                            <div class="caption">                  
                                <table class="table orderlist statusstock" style="margin: -6px 0 0 30px;">
                                    <tbody>
                                        <tr class="filter">                     
                                            <td>
                                             <select class="textbox" name="filter_department" id="filter_department">
                                                <option value=""> Select Department </option>
                                                <?php foreach ($department_collection as $department) { ?>
                                                        <option value="<?php echo trim($department['department_code']); ?>" <?php if($department['department_code']==$filter_department){ echo "Selected"; } ?>><?php echo $department['department_name']; ?></option>
                                                <?php } ?>
                                            </select>   </td>                  
                                            <td>
                                                <select class="textbox" name="filter_category" id="filter_category">
                                                    <option value="">Select Category</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" placeholder="Sub Category Name"></td>
                                            <td align=""><button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                      
                        </div>
                        <div style="clear:both; margin: 0 0 15px 0;"></div>
                    </div>
                </div>


            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>

                     <?php if ($error_warning) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error_warning; ?>
                        </div>
                    <?php } ?>

                 </div>
                 <div class="innerpage-listcontent-blocks">
                <div class="col-md-12">
                	<div class="innerpage-listcontent-blocks">
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                        	<tr class="heading">
                                <td class="left">
                                    <input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" />
                                </td>
                                <td class="left">Department</td>
                                <td class="left">Category</td>
                                <td class="left">Sub Category Name</td>
                                <td class="left">Sub Category Code</td>
                                <td class="left">Status</td>
                                <td class="right">Action</td>
                            </tr>
                        </thead>
                        <tbody>
						<?php if ($subCategorys) { $i='1';?>
							<?php foreach ($subCategorys as $category) { ?>
                            	<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
								<tr class="<?php echo $class; ?>">
                                    <td class="left">
                                        <?php if ($category['selected']) { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $category['id']; ?>" checked="checked" />
                                        <?php } else { ?>
                                        <input type="checkbox" name="selected[]" value="<?php echo $category['id']; ?>" />
                                        <?php } ?>
                                    </td>
                                    <td class="left"><?php echo $category['department_name']; ?></td>
                                    <td class="left"><?php echo $category['category_name']; ?></td>
                                    <td class="left"><?php echo $category['name']; ?></td>
									<td class="left"><?php echo $category['subCate_code']; ?></td>
									<td class="left"><?php echo $category['status']; ?></td>
									<td class="right">
										<?php foreach ($category['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>" <?php if($action['text']=='Delete'){ ?> onclick="return confirm('Are you sure delete?');" <?php } ?>><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php $i++; } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
                    </table>
                    </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
                </div>
            </div>
		</div>
	</div>
    </form>
</div>
<script type="text/javascript"><!--
function filter() {
    url = 'index.php?route=inventory/category/subCateList&token=<?php echo $token; ?>';
    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    var filter_category = jQuery('select[name=\'filter_category\']').attr('value');
    var filter_department = jQuery('select[name=\'filter_department\']').attr('value');

    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    if (filter_category) {
        url += '&filter_category=' + encodeURIComponent(filter_category);
    }
    if (filter_department) {
        url += '&filter_department=' + encodeURIComponent(filter_department);
    }
    location = url;
}
//--></script>

<?php echo $footer; ?>
<script type="text/javascript">

$(document).ready(function() {
    getCategory();
});
$('#filter_department').change(function(){
    getCategory();
});
function getCategory(){
    var department_code = $('#filter_department').val();
    var category = '<?php echo $filter_category; ?>';

    if(department_code!=''){
        $.ajax({
            type : 'POST',
            url: 'index.php?route=inventory/category/ajaxGetCategory&token=<?php echo $token; ?>',
            data: { department_code:department_code,category:category},
            success:function(data){
                $('#filter_category').html(data);
            } 
        });
    } else {
        $('#filter_category').html('<option value="">Select Category</option>');
    }
}
$('input[name=\'filter_name\']').autocomplete({
	delay: 50,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

</script> 

