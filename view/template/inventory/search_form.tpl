<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Search</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <!-- NEW CODE -->
            <div class="row">
			<div class="col-md-12">
				<div class="portlet">
                        <div class="portlet-body">
                            <div class="tabbable tabs-left invetory-tableft">
                                <ul class="nav nav-tabs new_invent">
                                    <li class="active">
                                        <a class="inventory_tabs" href="#tab_6_1" data-toggle="tab">GENERAL</a>
                                        <b></b>
                                    </li>
                                    <li>
                                        <a class="inventory_tabs" href="#tab_6_2" data-toggle="tab">COST</a>
                                    </li>
                                    <li>
                                        <a class="inventory_tabs" href="#tab_6_3" data-toggle="tab">DESCRIPTION</a>
                                    </li>
                                    <li>
                                        <a class="inventory_tabs" href="#tab_6_4" data-toggle="tab">SPECIAL</a>
                                    </li>
                                    <li>
                                        <a class="inventory_tabs" href="#tab_6_5" data-toggle="tab">BARCODE</a>
                                    </li>
                                    <li>
                                        <a class="inventory_tabs" href="#tab_6_6" data-toggle="tab">STOCK</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_6_1">
                                        <div class="invetory-tabtable">
                                           <table class="table orderlist">                       
					                        <tbody>
					                            <tr>
					                                <td width="20%">
					                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">                          								
					                                	<?php if($error_name) { ?>
						                                	<div class="input-icon right">
															<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Name"></i>
															<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">         
					                               		</div>
					                               		<?php } ?>
					                               		<?php if (!$error_name) { ?>
					                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
					                               		<?php } ?>						 
					                                </td>
					                            </tr>
					                            <tr>
					                                <td width="20%">
					                                	<label for="code"><?php echo $entry_sku; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
													   <?php if (empty($auto_inventory) || !empty($edit_form)) : ?>
                                                    	<input type="text" name="sku" id="sku" value="<?php echo $sku; ?>" class="textbox" placeholder="Enter your code" readonly>
													<?php else: ?>
														<input type="text" name="sku" id="sku" value="<?php echo $sku; ?>" class="textbox" placeholder="Enter your code">
													<?php endif;?>													
					                                </td>                                
					                            </tr> 
					                            <tr>
					                                <td width="20%">
					                                	<label><?php echo $entry_department; ?></label>
					                                </td>
					                                <td width="80%">
					                                 <select name="product_department[]" class="input-text">
                                                    	<option value="">-- Select Department --</option>
                                                        <?php foreach ($department_collection as $department) { ?>
                                                        	<?php if (in_array($department['department_id'], $product_department)) { ?>
                                                            	<option value="<?php echo $department['department_id']; ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                                            <?php } else { ?>
                                                            	<option value="<?php echo $department['department_id']; ?>"><?php echo $department['department_name']; ?></option>	
                                                            <?php } ?>   
                                                        <?php } ?>
                                                    </select>                      
					                                </td>                                
					                            </tr>
												<tr>
												<td width="20%">
													<label for="status"><?php echo $entry_category; ?></label>
												</td>
												<td width="80%">
													<select name="product_category[]" class="input-text validate[required]">
                                                    	<option value="">-- Select category --</option>
                                                        <?php foreach ($category_collection as $category) { ?>
                                                        	<?php if (in_array($category['category_id'], $product_category)) { ?>
                                                            	<option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['category_name']; ?></option>
                                                            <?php } else { ?>
                                                            	<option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>	
                                                            <?php } ?>   
                                                        <?php } ?>
                                                    </select>
												</td>
											</tr>
					                        <tr>
					                            <td width="20%">
					                                <label for="status"><?php echo $entry_brand; ?></label>
					                            </td>
					                            <td width="80%">
					                                <select name="product_brand[]" class="input-text validate[required]">
														<option value="">-- Select Brand --</option>
                                                        <?php foreach ($brand_collection as $brand) { ?>
                                                        	<?php if (in_array($brand['brand_id'], $product_brand)) { ?>
                                                            	<option value="<?php echo $brand['brand_id']; ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                                            <?php } else { ?>
                                                            	<option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>	
                                                            <?php } ?>   
                                                        <?php } ?>
                                                      </select>
					                            </td>
					                        </tr>
					                        <tr>
					                            <td width="20%">
					                                <label for="status"><?php echo $entry_bin; ?></label>
					                            </td>
					                            <td width="80%">
					                               <select name="bin" class="input-text validate[required]" >
														<option value="">-- Select Bin --</option>
														<?php if(!empty($bin_collection)): ?>
															<?php foreach($bin_collection as $bindetails): ?>
																<option value="<?php echo $bindetails['bin_id']; ?>" <?php if($bindetails['bin_id']==$bin):?> selected="selected" <?php endif; ?>>
																	<?php echo $bindetails['bin_name']; ?></option>
															<?php endforeach; ?>
														<?php endif; ?>
													</select>
					                            </td>
					                        </tr>
					                        <tr>
					                            <td width="20%">
					                                <label for="status"><?php echo $entry_vendor; ?></label>
					                            </td>
					                            <td width="80%">
					                               <select name="vendor" class="input-text validate[required]" >
														<option value="">-- Select Vendor --</option>
														<?php if(!empty($vendor_collection)): ?>
															<?php foreach($vendor_collection as $vendordetails): ?>
																<option value="<?php echo $vendordetails['vendor_id']; ?>" <?php if($vendordetails['vendor_id']==$vendor):?> selected="selected" <?php endif; ?>>
																	<?php echo $vendordetails['vendor_name']; ?>
																</option>
															<?php endforeach; ?>
														<?php endif; ?>
													</select>
					                            </td>
					                        </tr>
					                        <?php $statusAry = array("1"=>"Active","0"=>"InActive","2"=>"Discontinued"); ?>
											<tr>
												<td width="20%"><label for="status">Status</label></td>
												 <td width="80%">
													<select name="status" class="input-text">
															<?php foreach($statusAry as $key=>$value): ?>
																<option value="<?php echo $key; ?>" <?php if($key==$status):?> selected="selected" <?php endif; ?>>
																	<?php echo $value; ?>
																</option>
															<?php endforeach; ?>
													</select>
												</td>	
											</tr>
					                        </tbody>
					                    </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_2">
                                        <div class="invetory-tabtable">
                                            <table class="table orderlist">                       
					                        <tbody>
					                            <tr>
					                                <td width="20%">
					                                	<label for="price"><?php echo $entry_price; ?><span class="required">*</span></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">                          								
					                                	 <?php if ($error_price) { ?>
						                                	<div class="input-icon right">
															<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Price"></i>
															<input type="text" name="price" value="<?php echo $price; ?>" class="textbox requiredborder" placeholder="Enter Your Price">    
					                               		</div>
					                               		<?php } ?>
					                               		 <?php if (!$error_price) { ?>
					                               		<input type="text" name="price" value="<?php echo $price; ?>" class="textbox" placeholder="Enter Your Price">                                
					                               		<?php } ?>						 
					                                </td>
					                            </tr>
					                            <tr>
					                                <td width="20%">
					                                	<label for="unit_cost"><?php echo $entry_unit_cost; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
													  <input type="text" name="unit_cost" value="<?php echo $unit_cost; ?>" class="textbox" readonly="readonly">
					                                </td>                                
					                            </tr> 
					                            <tr>
					                                <td width="20%">
					                                	<label for="average_cost"><?php echo $entry_average_cost; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
					                                 <input type="text" name="average_cost" value="<?php echo $average_cost; ?>" class="textbox" readonly="readonly"/>                   
					                                </td>                                
					                            </tr>
												<tr>
												<td width="20%">
													<label for="tax_class_id"><?php echo $entry_tax_class; ?></label>
												</td>
												<td width="80%">
													<select name="tax_class_id" class="input-text validate[required]">
                                                    <option value="0"><?php echo $text_none; ?></option>
                                                    <?php foreach ($tax_classes as $tax_class) { ?>
                                                    <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                                                    <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                                    <?php } else { ?>
                                                    <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                                    <?php } ?>
                                                    <?php } ?>
                                                    </select>
												</td>
											</tr>
					                        <tr>
					                            <td width="20%">
					                                <label for="image"><?php echo $entry_image; ?></label>
					                            </td>
					                            <td width="80%">
                                                    <div class="form-group last">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
	                                                            <img src="<?php echo $thumb; ?>" alt="" />
                                                            </div>
                                                        	<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                                        	</div>
                                                            <div>
                                                                <span class="btn btn-default btn-file">
                                                                    <span class="fileinput-new">
                                                                    Select image </span>
                                                                    <span class="fileinput-exists">
                                                                    Change </span>
                                                                	<input type="file" name="image" id="image">
                                                                
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">
                                                                Remove </a>
                                                            
                                                            </div>
                                                        </div>
                                                    </div>

					                            </td>
					                        </tr>
					                        <tr>
					                            <td width="20%">
					                               <label for="sort_order"><?php echo $entry_sort_order; ?>
					                            </td>
					                            <td width="80%" class="order-nopadding">
					                               <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" size="2" class="textbox">
					                            </td>
					                        </tr>					                       
					                        </tbody>
					                    </table>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_3">
                                    	<div class="invetory-tabtable">
                                        <table class="table orderlist">                       
					                        <tbody>
					                            <tr>
					                                <td width="20%">
					                                	<label for="shortdescription"><?php echo $entry_shortdescription; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">                          			<textarea name="short_description" rows="3" class="textbox"><?php echo isset($short_description) ? $short_description : ''; ?></textarea>					 
					                                </td>
					                            </tr>
					                            <tr>
					                                <td width="20%">
					                                	<label for="description"><?php echo $entry_description; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
													  <textarea name="description" rows="3" class="textbox"><?php echo isset($description) ? $description : ''; ?></textarea>
					                                </td>                                
					                            </tr> 
					                            <tr>
					                                <td width="20%">
					                                	<label for="remarks">Remarks</label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
					                                <textarea name="remarks" rows="3" class="textbox"><?php echo isset($remarks) ? $remarks : ''; ?></textarea>                  
					                                </td>                                
					                            </tr>													                       
					                        </tbody>
					                    </table>
                                    	</div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_4">
                                        <div class="invetory-tabtable">
                                            <table id="special" class="table orderlist invetorty-tab">
                                                <thead>
                                                   <tr>                                                   	
														<td class="center"><?php echo $entry_special; ?></td>
		                                                <td class="center"><?php echo $entry_special_label; ?></td>
														<td class="center">Action</td>
														<td colspan="1"></td>
														<td></td>
													</tr> 
                                                </thead>    
                                                <?php $special_row = 0; ?>
												<?php foreach ($product_specials as $product_special) { ?>
												<tbody id="special-row<?php echo $special_row; ?>">
													<tr>
														<td class="center"><input class="textbox" type="text" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" class="textbox" /></td>
		                                                <td class="center"><input class="textbox" type="text" name="product_special[<?php echo $special_row; ?>][label]" value="<?php echo $product_special['label']; ?>" /></td>
														<td class="center"><a onclick="$('#special-row<?php echo $special_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
														<td colspan="1"></td>
														<td></td>
													</tr>
												</tbody>
												<?php $special_row++; ?>
												<?php } ?>
												<tfoot>
													<tr>
														<td colspan="4"></td>
														<td class="left"><a onclick="addSpecial();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_special; ?></a></td>
													</tr>
												</tfoot>
                                            </table>
                                    	</div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_5">
                                        <div class="invetory-tabtable">
                                           <table id="barcode" class="table orderlist invetorty-tab">
                                               <thead>
											<tr>
												<td class="center"><?php echo $entry_barcode; ?></td>
												<td class="center">Action</td>
											</tr>
										</thead>
										<?php $barcode_row = 0; ?>
										<?php foreach ($product_barcodes as $product_barcode) { ?>
										<tbody id="barcode-row<?php echo $barcode_row; ?>">
											<tr>
												<td class="center"><input type="text" name="product_barcode[<?php echo $barcode_row; ?>][barcode]" value="<?php echo $product_barcode['barcode']; ?>" class="textbox"></td>
												<td class="center"><a onclick="$('#barcode-row<?php echo $barcode_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i><?php echo $button_remove; ?></a></td>
											</tr>
										</tbody>
										<?php $barcode_row++; ?>
										<?php } ?>
										<tfoot>
											<tr>
												<td colspan="1" >
													<?php if ($error_barcode) { ?>
                                        			<span class="error"><?php echo $error_barcode; ?></span>
                                    				<?php } ?>
												</td>
												<td class="center"><a onclick="addBarcode();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i><?php echo $button_add_barcode; ?></a></td>
											</tr>
										</tfoot>
                                            </table>
                                    	</div>
                                    </div>
                                    <div class="tab-pane fade" id="tab_6_6">
                                        <div class="invetory-tabtable">
                                           <table class="table orderlist">                       
					                        <tbody>
					                            <tr>
					                                <td width="20%">
					                                	<label for="quantity"><?php echo $entry_quantity; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
					                                	<input type="text" name="quantity" value="<?php echo $quantity; ?>" size="2" class="textbox" readonly />						 
					                                </td>
					                            </tr>
					                            <tr>
					                                <td width="20%">
					                                	<label for="minimum"><?php echo $entry_minimum; ?></label>
					                                </td>
					                                <td width="80%" class="order-nopadding">
					                                	<div class="input-icon right">
                                        				<i style="color: orange;" data-html="true" class="fa fa-info tooltips" data-container="body" data-original-title="Force a minimum ordered amount"></i>
													   <input style="padding-left:10px !important;" type="text" name="minimum" value="<?php echo $minimum; ?>" size="2" class="textbox">			
					                                </td>                                
					                            </tr> 
					                            <tr>
					                                <td width="20%">
					                                	<label for="subtract"><?php echo $entry_subtract; ?></label>
					                                </td>
					                                <td width="80%">
					                                 <select name="subtract" class="input-text validate[required]">
														<?php if ($subtract) { ?>
														<option value="1" selected="selected"><?php echo $text_yes; ?></option>
														<option value="0"><?php echo $text_no; ?></option>
														<?php } else { ?>
														<option value="1"><?php echo $text_yes; ?></option>
														<option value="0" selected="selected"><?php echo $text_no; ?></option>
														<?php } ?>
													</select>                      
					                                </td>                                
					                            </tr>
												<tr>
												<td width="20%">
													<label for="stock_status_id"><?php echo $entry_stock_status; ?></label>
												</td>
												<td width="80%">
													<select name="stock_status_id" class="input-text validate[required]">
														<?php foreach ($stock_statuses as $stock_status) { ?>
														<?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
														<option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
														<?php } else { ?>
														<option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
														<?php } ?>
														<?php } ?>
												   </select>
												</td>
											</tr>
					                        <tr>
					                            <td width="20%">
					                               <label for="weight_class_id"><?php echo $entry_weight_class; ?></label>
					                            </td>
					                            <td width="80%">
					                               <select name="weight_class_id" class="input-text">
														<?php foreach ($weight_classes as $weight_class) { ?>
														<?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
														<option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
														<?php } else { ?>
														<option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
														<?php } ?>
														<?php } ?>
												  </select>
					                            </td>
					                        </tr>
					                        </tbody>
					                    </table>
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>           
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript"><!--
	var barcode_row = <?php echo $barcode_row; ?>;
	function addBarcode() {
		html  = '<tbody id="barcode-row' + barcode_row + '">';
		html += '  <tr>'; 
		html += '    <td class="center"><input class="textbox" type="text" name="product_barcode[' + barcode_row + '][barcode]" value=""  placeholder="Barcode"></td>';
		html += '    <td class="center"><a onclick="$(\'#barcode-row' + barcode_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
		html += '  </tr>';
		html += '</tbody>';
		
		$('#barcode tfoot').before(html);
		barcode_row++;
	}
	//--></script> 
<script type="text/javascript"><!--
	var special_row = <?php echo $special_row; ?>;
	function addSpecial() {
		html  = '<tbody id="special-row' + special_row + '">';
		html += '  <tr>'; 
		html += '    <td class="center"><input class="textbox" type="text" name="product_special[' + special_row + '][price]" value="" placeholder="Enter Price"></td>';
		html += '    <td class="center"><input class="textbox" type="text" name="product_special[' + special_row + '][label]" value="" placeholder="Enter Special"></td>';
		html += '    <td class="center"><a onclick="$(\'#special-row' + special_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
		html += '<td colspan="1"></td>';
		html += '<td></td>';
		html += '  </tr>';
		html += '</tbody>';
		
		$('#special tfoot').before(html);
	 
		$('#special-row' + special_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
		
		special_row++;
	}
	//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('.content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$('.date').datepicker({dateFormat: 'yy-mm-dd'});
$('.datetime').datetimepicker({
	dateFormat: 'yy-mm-dd',
	timeFormat: 'h:m'
});
$('.time').timepicker({timeFormat: 'h:m'});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
</script>
<script type="text/javascript">
	function department_change(element) {
		if (element.value != '') {
			brandHtml = '<option value="">-- Select brand --</option>';
			jQuery('select[name=\'brand\']').html(brandHtml);
			jQuery.ajax({
				url: 'index.php?route=inventory/inventory/category&token=<?php echo $token; ?>&department_id=' + element.value,
				dataType: 'json',
				beforeSend: function() {
				//jQuery('select[name=\'department\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
			},
				complete: function() {
					//jQuery('.wait').remove();
				},          
				success: function(json) {
				html = '<option value="">-- Select category --</option>';
				if (json['category'] != '') {
					for (i = 0; i < json['category'].length; i++) {
						html += '<option value="' + json['category'][i]['category_id'] + '"';
						if (json['category'][i]['category_id'] == '<?php echo $category; ?>') {
							html += ' selected="selected"';
						}
						html += '>' + json['category'][i]['name'] + '</option>';
					}
				}
				jQuery('select[name=\'category\']').html(html);
			},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}
	
	function category_change(element) {
		if (element.value != '') {
			jQuery.ajax({
				url: 'index.php?route=inventory/inventory/brand&token=<?php echo $token; ?>&category_id=' + element.value,
				dataType: 'json',
				beforeSend: function() {
				//jQuery('select[name=\'department\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
			},
				complete: function() {
					//jQuery('.wait').remove();
				},          
				success: function(json) {
				html = '<option value="">-- Select brand --</option>';
				if (json['brand'] != '') {
					for (i = 0; i < json['brand'].length; i++) {
						html += '<option value="' + json['brand'][i]['brand_id'] + '"';
						if (json['brand'][i]['brand_id'] == '<?php echo $brand; ?>') {
							html += ' selected="selected"';
						}
						html += '>' + json['brand'][i]['name'] + '</option>';
					}
				}
				jQuery('select[name=\'brand\']').html(html);
			},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}
	jQuery(document).ready(function() {
		jQuery('select[name$=\'[department]\']').trigger('change');
		jQuery('select[name$=\'[category]\']').trigger('change');

		jQuery('#name').change(function(){
			<?php if(empty($edit_form)){ ?>
			$("textarea[name=short_description]").val($(this).val());
			$("textarea[name=description]").val($(this).val());
			<?php } else {?>
			if($("textarea[name=short_description]").val()==""){
               $("textarea[name=short_description]").val($(this).val());
            } 
            if($("textarea[name=description]").val()!=""){
			   $("textarea[name=description]").val($(this).val());
		    }
			<?php } ?>
		});
	});
</script>
<?php echo $footer; ?>