<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Brand List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?></div>    
                    </div>
                </div>               						
			</div>
			<div style="clear:both"></div>
			
			<div class="row">	
			<div class="page-bar innerpage-listcontent-blocks" style="margin-top: -26px; min-width: 100%; border-top:1px solid #ccc;">
                    <div class="portlet bordernone" style="margin-bottom:0 !important">
                        <div class="page-bar portlet-title" style="min-height: 45px; min-width: 100%; margin: 0; padding: 15px 0 0 0;">
                            <div class="caption">                  
                                <table class="table orderlist statusstock" style="margin: -6px 0 0 30px;">
                                    <tbody>
                                        <tr class="filter">                       
                                            <td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" placeholder="Brand Name"></td>
                                            <td align=""><button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                      
                        </div>
                        <div style="clear:both; margin: 0 0 15px 0;"></div>
                    </div>
                </div>

             	<div class="col-md-12">
					<?php if ($error_warning) { ?>
					<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
		             </div>
					<?php } ?>
					<?php if ($success) { ?>			
						<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
							<button type="button" class="close" data-dismiss="alert"></button>
		                </div>
					<?php } ?>				 
				</div>
				<div style="clear:both"></div>
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
								<td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
								<td class="left"><?php echo $column_code; ?></td>
								<td class="left"><?php if ($sort == 'B.brand_name') { ?>
									<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
									<?php } else { ?>
									<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
									<?php } ?>
								</td>	
								<td class="left"><a href="<?php echo $sort_name; ?>"><?php echo $column_status; ?></a></td>
								<td class="right"><?php echo $column_action; ?></td>
							</tr>
                        </thead>
                        <tbody>
                            <?php if ($brands) { ?>
                            <?php $class = 'odd'; ?>
					<?php foreach ($brands as $brand) { ?>
					<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
					<tr class="<?php echo $class; ?>">
					 <td style="text-align: center;">
										<?php if ($brand['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $brand['code']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $brand['code']; ?>" />
										<?php } ?>
									</td>
									<td class="left"><?php echo $brand['code']; ?></td>
									<td class="left"><?php echo $brand['name']; ?></td>
									<td class="left"><?php if($brand['status']): ?> Active <?php else: ?> inActive <?php endif; ?></td>
									<td class="right">
										<?php foreach ($brand['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
    url = 'index.php?route=inventory/brand&token=<?php echo $token; ?>';
    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    location = url;
}
//--></script>
<?php echo $footer; ?>