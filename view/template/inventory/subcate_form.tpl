<?php echo $header; ?>
<div class="clearfix">
</div>
<?php $style ="min-height: 35px;padding: 7px;width: 100%;"; ?>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
	<div class="page-content-wrapper">    	
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				      </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save" onclick="return validation();"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                       	</div>    
                    </div>
                </div>
			</div>
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                     <?php if ($error_warning) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error_warning; ?>
                        </div>
                    <?php } ?>
                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">
                	
                	<table class="table orderlist statusstock country-lisblk invetorty-tab1">
                    	<tbody>
                            <tr>
                                <td width="35%">Parent Category&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                  <select name="category_id" style="min-height: 35px;padding: 7px;width: 100%;" id="category_id" class="textbox <?php if($error_department) { echo "requiredborder"; } ?> ">
                                      <option value="">-- Select Category --</option>
                                      <?php foreach ($categoryCollection as $categorys) { ?>
                                              <option value="<?php echo $categorys['category_code']; ?>" <?php if($categorys['category_code'] == $category_id){ echo "Selected"; }?> ><?php echo $categorys['category_name']; ?></option>
                                      <?php } ?>
                                  </select> 
                                </td>
                            </tr>
                            <tr>
                              <td width="35%">
                                Sub Catetegory Code&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                                      <input type="text" placeholder="Enter Subcategory Code" name="subCate_code" id="subCate_code" class="textbox" value="<?php echo $subCate_code; ?>" />
                                </td>
                            </tr>
                            <tr>
                            	<td width="35%">
                                Sub Category Name&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                  <input type="hidden" name="id" value="<?php echo $id; ?>">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										                    <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
                                    	<input type="text" placeholder="Enter name" name="name" id="name" class="textbox requiredborder" value="<?php echo $name; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_name) { ?>
                               			<input type="text" placeholder="Please Enter name" name="name" id="name" class="textbox" value="<?php echo $name; ?>" />
                               		<?php } ?>
                                </td>
                            </tr>

                            <tr>
                            	<td width="35%"><?php echo $entry_status; ?>
                                </td>
                                 <td width="65%">
                                <select name="status" class="textbox" style="min-height: 35px;padding: 7px;width: 100%;">
                                    <option value="1" <?php if($status=='1'){?> selected="selected" <?php }?>>Active</option>
                                    <option value="0" <?php if($status=='0'){?> selected="selected" <?php }?>>In Active</option>
                                 </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>                
            </div>
		</div>
	</div>
    </form>
</div>
<script type="text/javascript">
  function validation(){
    var cate = $('#category_id').val();
    var name = $('#name').val();
    var subCate_code = $('#subCate_code').val();
    
    if(cate==''){
        alert('Please Select Category');
        return false;
    }
    if(name==''){
        alert('Please Enter Name');
        return false;
    }
    if(subCate_code==''){
        alert('Please Enter SubCate Code');
        return false;
    }
    return true;
  }
</script>
<?php echo $footer; ?>