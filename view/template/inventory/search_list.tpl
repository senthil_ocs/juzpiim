<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Inventory Search</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
        <div class="page-toolbar">
           <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
               <!--  <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a> -->
              </div>    
            </div>
        </div>
			</div>
			<!-- SEARCH CODE-->           
      <div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
            <div class="page-bar portlet-title invent_search">
                <div class="caption">                  
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px;">
                    <tbody>
                      <tr class="filter">
                        <td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" 
                          placeholder="Product Name / Barcode" autocomplete="off">
                                <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>

                        <td><input type="text" placeholder='Price Range From' name="filter_price" value="<?php echo $filter_price; ?>" class="textbox" /></td>
                        <td><input type="text" placeholder='Price Range To' name="filter_priceto" value="<?php echo $filter_priceto; ?>" class="textbox" /></td>
                        <td>
                        <select name="filter_location_code" id="filter_location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                                  <option value="">-- Select Location --</option>
                                  <?php
                                     if(!empty($Tolocations)){
                                          foreach($Tolocations as $value){ ?>
                                          <option value="<?php echo $value['location_code'];?>" <?php if($filter_location_code == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                                  <?php  } } ?> 
                              </select>
                      </td>                                   
                      <td align=""><button style="min-height: 36px; border-radius:0 !important; width:100%" type="button" onclick="searchInventory();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                    </tr>
                        </tbody>
                    </table>
                        </div>                        
                    <div class="tools" style="float:left;padding: 0 0 0 35px;">
                        <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
                    </div>
                    </div>
                    <?php if (($filter_vendor !="") || ($filter_brand != "") || ($filter_department != '') || ($filter_category != '')){ 
                      $showHide  = 'display:block;';
                    } else {
                      $showHide  = 'display:none;';
                    } ?>       
                    <div class="page-bar portlet-body bgnone" style="display: none;background-color: #eee !important; margin: 0px 0 0 0px;<?php echo $showHide; ?>">
                     <table class="table orderlist statusstock" style="margin-bottom: -10px !important;width: 68%;">
                    <tbody>
                      <tr class="filter">                                           
                      <td> 
                       <select name="filter_department" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="*">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if ($department['department_id'] == $filter_department) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                      <td>
                        <select name="filter_category" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="*">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if ($category['category_code'] == $filter_category) { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                          <?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                    <?php } ?>   
                                <?php } ?>
                            <?php } ?>
                        </select> 
                    </td>
                    <td>
                      <select name="filter_brand" class="textbox" style="min-height: 35px; padding: 7px;">
                              <option value="*">Select Brand</option>
                              <?php if (!empty($brand_collection)) { ?>
                                  <?php foreach ($brand_collection as $brand) { ?>
                                      <?php if (trim($brand['brand_code']) == $filter_brand) { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                      <?php } else { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
                                      <?php } ?>   
                                  <?php } ?>
                              <?php } ?>
                          </select> 
                     </td>
                     <td>
                      <select name="filter_vendor" class="textbox" style="min-height: 35px; padding: 7px">
                                <option value="*">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if ($vendor['vendor_code'] == $filter_vendor) { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                     </tr>
                        </tbody>
                    </table>
                    </div>
                     <div style="clear:both; margin: 0 0 15px 0;">
                    </div>
                </div>
              </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div> 
            <div class="row">
            <div class="col-md-12">
          <?php if ($error_warning) { ?>
          <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert"></button>
                 </div>
          <?php } ?>
          <?php if ($success) { ?>      
            <div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
              <button type="button" class="close" data-dismiss="alert"></button>
                    </div>
          <?php } ?>
          </div> 
          <div class="col-md-12">         
            <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form" style="padding:20px 0 0 0">
              <div class="innerpage-listcontent-blocks">
                <table class="table orderlist statusstock">
                  <thead>
                    <tr class="heading">
                       <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                      <td class="center"><?php echo $column_code; ?></td>
                      <td class="center"><?php echo 'Product Name'; ?></td>
                       <td class="center"><?php echo $column_quantity; ?></td>
                        <td class="center"><?php echo 'Rer Quantity'; ?></td>
                      <td class="right"><?php if ($sort == 'p.quantity') { ?>
                        <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_quantity; ?>"><?php echo 'QOH'; ?></a>
                        <?php } ?></td>
                      <td class="left"><?php if ($sort == 'p.price') { ?>
                        <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                        <?php } ?></td>
                      <td class="center">Cost</td>
                      <td class="center"><?php echo $text_department; ?></td>
                      <td class="center"><?php echo $text_category; ?></td>
                      <td class="center"><?php echo $text_brand; ?></td>
                      <td class="left"><?php if ($sort == 'p.status') { ?>
                        <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                        <?php } else { ?>
                        <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                        <?php } ?></td>
                      <td class="right"><?php echo $column_action; ?></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if ($products) { ?>
                       <?php $class = 'odd'; ?>
                    <?php foreach ($products as $product) { ?>
                    <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                    <tr class="<?php echo $class; ?>">
                       <td style="text-align: center;"><?php if ($product['selected']) { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                        <?php } else { ?>
                        <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                        <?php } ?>
                      </td>
                      <td class="center"><?php echo $product['sku']; ?></td>
                      <td class="center"><?php echo $product['name']; ?></td>
                       <td class="center"><?php echo $product['sku_qty']; ?></td>
                       <td class="center"><?php echo $product['reserved_qty']; ?></td>
                      <td class="right"><?php if ($product['quantity'] <= 0) { ?>
                        <span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
                        <?php } elseif ($product['quantity'] <= 5) { ?>
                        <span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
                        <?php } else { ?>
                        <span style="color: #008000;"><?php echo $product['quantity']; ?></span>
                        <?php } ?></td>                     
                      <td class="left"><?php if ($product['special']) { ?>
                        <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                        <span style="color: #b00;"><?php echo $product['special']; ?></span>
                        <?php } else { ?>
                        <?php echo $product['price']; ?>
                        <?php } ?></td>
                      <td class="center"><?php echo $product['sku_cost']; ?></td>
                      <td class="center"><?php echo $product['department_code']; ?></td>
                      <td class="center"><?php echo $product['category_code']; ?></td>
                      <td class="center"><?php echo $product['brand_code']; ?></td>
                      <td class="left"><?php echo $product['status']; ?></td>
                      <td class="right"><?php foreach ($product['action'] as $action) { ?>
                        [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                        <?php } ?></td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td align="center" colspan="10"><?php echo $text_no_results; ?></td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </form>
          <div class="pagination"><?php echo $pagination; ?></div>
        </div>
      </div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function searchInventory() {
	url = 'index.php?route=inventory/search&token=<?php echo $token; ?>';
	
	var filter_barcodes = $('input[name=\'filter_barcodes\']').attr('value');
	if (filter_barcodes) {
		url += '&filter_barcodes=' + encodeURIComponent(filter_barcodes);
	}
	
	var filter_department = $('select[name=\'filter_department\']').attr('value');	
	if (filter_department != '*') {
		url += '&filter_department=' + encodeURIComponent(filter_department);
	}
	
	var filter_category = $('select[name=\'filter_category\']').attr('value');	
	if (filter_category != '*') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
	
	var filter_price = $('input[name=\'filter_price\']').attr('value');
	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}
	var filter_priceto = $('input[name=\'filter_priceto\']').attr('value');
	if (filter_priceto) {
		url += '&filter_priceto=' + encodeURIComponent(filter_priceto);
	}
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_vendor = $('select[name=\'filter_vendor\']').attr('value');	
	if (filter_vendor != '*') {
		url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
	}
	
	var filter_brand = $('select[name=\'filter_brand\']').attr('value');	
	if (filter_brand != '*') {
		url += '&filter_brand=' + encodeURIComponent(filter_brand);
	}

  var filter_location_code = $('select[name=\'filter_location_code\']').attr('value');  
  if (filter_location_code != '*') {
    url += '&filter_location_code=' + encodeURIComponent(filter_location_code);
  }

	location = url;
}
//--></script> 
<?php echo $footer; ?>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script>
<script type="text/javascript">
function getCategory(v)
{
  //alert(v);
  $.ajax({
     url:"index.php?route=inventory/search/getCategoryByDept&token=<?php echo $token; ?>",
    type:"POST",
    data:{filter_department:v},            
    success:function(out){
      //alert(out);
      out = JSON.parse(out);
      html = '<option value="">Select Category</option>';
                if(out != '') {
                    for(i=0; i <out.length; i++) {
                        html += '<option value="' + out[i]['category_id'] + '"';
                            if (out[i]['category_id'] == '') {
                                html += ' selected="selected"';
                            }
                        html += '>' + out[i]['category_name'] + '</option>';
                    }
                } else {
                     html += '<option value="" selected="selected"> -None- </option>';
                }
                    $('select[name=\'filter_category\']').html(html);
    }
  });
}
</script>

<script type="text/javascript">
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});

$('#autocomplete-suggestion').click(function(event){
  alert("alert");
});

function getValue(strhtml){
  $('#filter_name').val(strhtml)
  $('#suggesstion-box').hide();
}
function getValue1(strhtml,sku){
  $('#filter_name').val(strhtml)
  $('#filter_sku').val(sku)
  $('#suggesstion-box').hide();
}

</script>