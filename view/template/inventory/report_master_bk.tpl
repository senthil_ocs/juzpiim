  <?php echo $header; ?>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <!-- <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="window.print();"><i class="fa fa-print"></i> Print</a> -->
                 <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                  <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                  <i class="fa fa-file-pdf-o"></i> Export PDF</a>
                  

                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>

      <div style="clear:both"></div>            
        <div class="row">     
          <div class="col-md-12">      
            <div class="innerpage-listcontent-blocks">
              <table class="table orderlist statusstock">
                  <tr>
                    <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2">
                      <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                            <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                    </td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2"><?php echo $companyInfo['country']; ?></td>
                  </tr>
                    <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
                  <tr <?php echo $contact; ?>>
                     <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                    <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
                  </tr>
                </table>
              <div>
              <h1>SKU Master Report</h1>
              <p>

                <?php if($filter_department != '' || $filter_category != '' || $filter_brand != '' || $filter_vendor != '') { ?>
                  <span style="font-size: 15px;"> Filter By </span>
                <?php  }?>

                <?php if($filter_department != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Department : <span id="filter_departmentid"></span> </span>
                <?php } ?>
                <?php if($filter_category != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Category : <span id="filter_categoryid"></span> </span>
                <?php } ?>
                <?php if($filter_brand != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Brand : <span id="filter_brandid"></span> </span>
                <?php } ?>
                <?php if($filter_vendor != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Vendor : <span id="filter_vendorid"></span> </span>
                <?php } ?>
              </p>
            </div>
          </div>
            
            <div class="caption set-bg-color"> 
              <form method="post" name="report_filter">
                <input name="type" id="type" type="hidden">                 
                  <table class="table" style="margin: -6px 0 0 25px; width: 90%;">
                    <tbody>
                     <tr class="filter" style="border-top:none;">                                           
                      <td> 
                       <select name="filter_department" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $filter_department) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                      <td>
                        <select name="filter_category" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if (trim($category['category_code']) == $filter_category) { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                          <?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                    <?php } ?>   
                                <?php } ?>
                            <?php } ?>
                        </select> 
                    </td>
                    <td>
                      <select name="filter_brand" class="textbox" style="min-height: 35px; padding: 7px;">
                              <option value="">Select Brand</option>
                              <?php if (!empty($brand_collection)) { ?>
                                  <?php foreach ($brand_collection as $brand) { ?>
                                      <?php if (trim($brand['brand_code']) == $filter_brand) { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                      <?php } else { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
                                      <?php } ?>   
                                  <?php } ?>
                              <?php } ?>
                          </select> 
                     </td>
                     <td>
                      <select name="filter_vendor" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if (trim($vendor['vendor_id']) == $filter_vendor) { ?>
                                            <option value="<?php echo $vendor['vendor_id']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_id']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    <td align="center">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>

                      </tr>
                    </tbody>
                  </table>
              </form>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
             <div class="caption">
            <table class="table orderlist statusstock">
              <tr>
                <td colspan="6" align="right" style="width: 65%;">Total</td>
                <td align="right" style="width: 10%;"><?php echo $products_sum['total_price']; ?></td>
                <td align="right" style="width: 10%;"><?php echo $products_sum['total_selling_price']; ?></td>
              </tr>

            </table>  
            <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                  <td class="center" style="width: 5%;">S.No</td>
                  <td class="center" style="width: 10%;">Inv.Code</td>
                  <td class="center" style="width: 10%;">Department </td>
                  <td class="center" style="width: 10%;">Category </td>
                  <td class="center" style="width: 10%;">Brand </td>
                  <td class="center" style="width: 20%;">Description</td>
                  <td class="center" style="width: 10%;">Cost</td>
                  <td class="center" style="width: 10%;">Selling Price</td>
                </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $i; ?></td>
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['department_name']; ?></td>
                    <td class="center"><?php echo $product['category_name']; ?></td>
                    <td class="center"><?php echo $product['sku_brand_code']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="right"><?php echo $product['price']; ?></td>
                    <td class="right"><?php echo $product['selling_price']; ?></td>
                  </tr>
              <?php $i++; }  ?>  
                <?php  if(count($products_sum)>=1 && $last_page=='1'){ ?>
                <tr>
                 <td class="right" style="text-align: right;" colspan="6">Total</td>
                 <td class="right"><?php echo $products_sum['total_price']; ?></td>
                 <td class="right"><?php echo $products_sum['total_selling_price']; ?></td>
                </tr>  
                <?php } ?>           
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table> 
      </div>          
        <div class="pagination"><?php echo $pagination; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
function filterReport() {
  url = 'index.php?route=inventory/reports/masterReport&token=<?php echo $token; ?>';
  
  
  var filter_department = $('select[name=\'filter_department\']').attr('value');  
  if (filter_department != '*') {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }
  
  var filter_category = $('select[name=\'filter_category\']').attr('value');  
  if (filter_category != '*') {
    url += '&filter_category=' + encodeURIComponent(filter_category);
  }
  
  var filter_vendor = $('select[name=\'filter_vendor\']').attr('value');  
  if (filter_vendor != '*') {
    url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
  }
  
  var filter_brand = $('select[name=\'filter_brand\']').attr('value');  
  if (filter_brand != '*') {
    url += '&filter_brand=' + encodeURIComponent(filter_brand);
  }

  location = url;

}
function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_departmentid" ).html(filter_department);

  var filter_category = $('[name="filter_category"] option:selected').text();
  $( "#filter_categoryid" ).html(filter_category);

  var filter_brand = $('[name="filter_brand"] option:selected').text();
  $( "#filter_brandid" ).html(filter_brand);

  var filter_vendor = $('[name="filter_vendor"] option:selected').text();
  $( "#filter_vendorid" ).html(filter_vendor);
});


//--></script> 
<?php echo $footer; ?>    