<?php echo $header; ?>
<head>
<link href="view/assets/editor/richtext.min.css" type="text/css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<style type="text/css">
div.scrollmenu {
    overflow: auto;
    white-space: nowrap;
}
</style>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	

     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID" novalidate>
        <div class="page-content" >
            <h3 class="page-title">New Product</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo $inven_comm; ?>">Product</a>
                        <i class="fa fa-angle-right"></i>                      
                    </li>
                    <li>
                        <a href="#">New Product</a>                                                                     
                    </li>                                      
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>
            <!-- NEW CODE -->
         
            <div class="row">
            	<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="alert alert-block alert-danger fade in "><?php echo $error_warning; ?> <?php echo "-".$sku_errorMsg;?>
						<button type="button" class="close" data-dismiss="alert"></button>
            	    </div>
        		</div>
			<?php } ?>
			<div class="col-md-12" style="float:left; padding-left: 5px;padding-right: 0px;">
				<div class="col-md-12" style="float:left;padding-left: 5px;padding-right: 0px;">
				<div class="innerpage-listcontent-blocks">
                   	<div class="col-md-6 mb2">
                   		<table class="table orderlist">
                   		<thead><tr><th colspan="2">Product Basic Details</th></tr></thead>
	                        <tbody>                          
	                        	<tr>
	                                <td width="20%">
	                                	<label for="code">Product Code<span class="required">*</span></label>
	                                </td>
	                                <td width="80%" class="order-nopadding">
									   <?php if (empty($auto_inventory) || !empty($edit_form)) : ?>
                                    	<input type="text" name="sku" id="sku" value="<?php echo $sku; ?>" placeholder="Enter your code" class="textbox readonly-text" required>
									<?php else: ?>
										<input type="text" name="sku" id="sku" value="<?php echo $sku; ?>" placeholder="Enter your code" class="textbox readonly-text" required>
									<?php endif;?>	
									<input type="hidden" name="transaction_set" id="transaction_set" value="<?php echo $transaction_set; ?>">
	                                </td>                                
	                            </tr> 
	                        	<tr>
	                                <td width="20%">
	                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
	                                </td>
	                                <td width="80%" class="order-nopadding">
										<?php if (empty($edit_form)) : ?>
										<textarea name="name" id="name" onkeyup="descriptionAutoFill();" class="textbox" required><?php echo $name; ?></textarea>
										<?php else: ?>
										<textarea name="name" id="name" class="textbox" required><?php echo $name; ?></textarea>	
										<?php endif;?>	
	                                </td>
	                            </tr>
	                            <tr>
	                                <td width="20%">
	                                	<label for="description"><?php echo $entry_description; ?></label>
	                                </td>
	                                <td width="80%" class="order-nopadding">
		                                <div style="width: 440px;">
		                                <input type="hidden" name="short_description" id="short_description">
										<textarea name="description" id="description" rows="2" class="textbox content"><?php echo isset($description) ? $description : ''; ?></textarea>
		                                </div>
	                                </td>
	                            </tr>
	                        	<tr>
	                                <td width="20%">
	                                	<label for="name"><?php echo $entry_image; ?></label>
	                                </td>
	                                <td width="80%" class="order-nopadding">  
                                		<input type="file" name="image" id="image" class="textbox imgUpload" >
                                		<?php $imgurl = HTTP_SERVER.'uploads/'.$image; ?>
                                		<img src="" id="imgPreview" width="250px">
                                		<img src="<?php echo $imgurl; ?>" class="imgDisplay" width="150px">
                                		<a onclick="removeImage();" class="btn btn-zone btn-danger imgDisplay"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a>
	                                </td>
	                            </tr>
	                            <tr>
	                                <td width="20%">
	                                	<label><?php echo $entry_department; ?><span class="required">*</span></label>
	                                </td>
	                                <td width="80%">
	                                 <select name="product_department" id="product_department" class="input-text" required>
                                    	<option value="">-- Select Department --</option>
                                        <?php foreach ($department_collection as $department) { ?>
                                        	<?php if (trim($department['department_code']) == trim($product_department)) { ?>
                                            	<option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                            <?php } else { ?>
                                            	<option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>	
                                            <?php } ?>   
                                        <?php } ?>
                                    </select>                      
	                                </td>                                
	                            </tr>
								<tr>
									<td width="20%">
										<label for="status"><?php echo $entry_category; ?><span class="required">*</span></label>
									</td>
									<td width="80%">
										<select name="product_category" class="input-text validate[required]" required id="product_category">
	                                    	<option value="">-- Select category --</option>
	                                        <?php foreach ($category_collection as $category) { ?>
	                                        	<?php //if (in_array($category['category_code'], $product_category)) { ?>
	                                        	<?php if (trim($category['category_code']) == trim($product_category)) { ?>
	                                            	<option value="<?php echo trim($category['category_code']); ?>" selected="selected"><?php echo $category['category_name']; ?></option>
	                                            <?php } else { ?>
	                                            	<option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>	
	                                            <?php } ?>   
	                                        <?php } ?>
	                                    </select>
									</td>
								</tr>
								<tr>
									<td>Sub Category</td>
									<td>
										<select type="text" name="sub_category" id="sub_category" class="input-text">
											<option value="">Select Sub Category</option>
										</select>
									</td>
								</tr>
	                       		<tr>
		                            <td width="20%">
		                                <label for="status"><?php echo $entry_brand; ?></label>
		                            </td>
		                            <td width="80%">
		                                <select name="product_brand" class="input-text validate[required]">
											<option value="">-- Select Brand --</option>
	                                        <?php foreach ($brand_collection as $brand) { ?>
	                                        	<?php //if (in_array($brand['brand_code'], $product_brand)) { ?>
	                                        	<?php if (trim($brand['brand_code']) == $product_brand) { ?>
	                                            	<option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
	                                            <?php } else { ?>
	                                            	<option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>	
	                                            <?php } ?>   
	                                        <?php } ?>
	                                      </select>
		                            </td>
	                        	</tr>
		                        <tr>
		                            <td width="20%">
		                                <label for="status"><?php echo $entry_vendor; ?><span class="required">*</span></label>
		                            </td>
		                            <td width="80%">
		                               <select name="vendor" class="input-text validate[required]" required >
											<option value="">-- Select Vendor --</option>
											<?php if(!empty($vendor_collection)): ?>
												<?php foreach($vendor_collection as $vendordetails): ?>
													<option value="<?php echo trim($vendordetails['vendor_code']); ?>"
													 <?php if(trim($vendordetails['vendor_code'])==$vendor):?> selected="selected" <?php endif; ?>>
														<?php echo $vendordetails['vendor_name']; ?>
													</option>
												<?php endforeach; ?>
											<?php endif; ?>
										</select>
		                            </td>
	                       	    </tr>
	                        		<?php $statusAry = array("1"=>"Enabled","0"=>"Disabled"); ?>
								<tr>
									<td width="20%"><label for="status">Status</label></td>
									 <td width="80%">
										<select name="status" class="input-text">
												<?php foreach($statusAry as $key=>$value): ?>
													<option value="<?php echo $key; ?>" <?php if($key==$status):?> selected="selected" <?php endif; ?>>
														<?php echo $value; ?>
													</option>
												<?php endforeach; ?>
										</select>
									</td>	
								</tr>
								<tr>    
	                                <td width="20%">
	                                    <label for="status">UOM <span class="required">*</span></label>
	                                </td>
	                                <td width="80%">
	                                    <select name="sku_uom" id="sku_uom" class="input-text validate[required]" required onchange="getUOMTypes(this.value);">
	                                        <option value="PCS">PCS</option>
	                                        <?php foreach ($uom_collection as $uom) { ?>
	                                                <option value="<?php echo $uom['code']; ?>" <?php if ($uom['code'] == $product_uom) { ?> selected="selected" <?php } ?>   ><?php echo $uom['uom_name']; ?></option>
	                                        <?php } ?>
	                                      </select>
										  <?php /*?>
	                                      <select name="sku_uom_type" id="sku_uom_type" class="input-text">
	                                        <option value="">-- Select Type --</option>

	                                        <?php foreach ($uom_type_list as $uom) {?>
	                                                <option value="<?php echo $uom['uom_type_value']; ?>" <?php if ($uom['uom_type_value'] == $sku_uom_type) { ?> selected="selected" <?php } ?>   ><?php echo $uom['uom_type_value']; ?></option>
	                                        <?php } ?>
	                                      </select>
										  <?php */ ?>
	                                </td>
                            	</tr>
                            	<tr>
	                                <td width="20%">
	                                    <label for="status">Ispackage</label>
	                                </td>
	                                <td width="80%">
	                                	<input type="checkbox" value="1" name="ispackage" <?php echo $ispackage; ?> id="ispackage">
	                                </td>
                            	</tr>
                            	<tr>
	                                <td width="20%">
	                                    <label for="status">Isserviced</label>
	                                </td>
	                                <td width="80%">
	                                	<input type="checkbox" value="1" name="isserviced" <?php echo $isserviced; ?> id="isserviced">
	                                </td>
                            	</tr>
                            </tbody>
                    	</table>
           			</div>
                   	<div class="col-md-6 mb2">
                   		<table class="table orderlist">
              			<thead>
	                	<tr>
	                    	<th colspan="4">Price and Stock Details</th>
	                    </tr>
	                	</thead>
	                	<tbody>
	                    <tr>
                            <td width="20%">
                            <label for="price"><?php echo $entry_price; ?><span class="required">*</span></label>
                            </td>
                            <td width="30%" class="order-nopadding">
                            	<input type="hidden" name="old_price" value="<?php echo $price; ?>">
                            	<input type="number" name="price" id="price" value="<?php echo $price; ?>"onkeyup="descriptionAutoFill();" class="textbox" 
                            	placeholder="Enter Your Price" style="padding-left:10px !important;" required>
                           		
                            </td>
                        </tr>
						<?php /*?>
                       <tr>
							<td width="15%">
								<label for="tax_class_id">Tax Allowed</label>
							</td>
							<td width="35%">
	                             <select name="sku_gstallowed" class="input-text validate[required]">
									<?php if ($sku_gstallowed) { ?>
									<option value="1" selected="selected"><?php echo $text_yes; ?></option>
									<option value="0"><?php echo $text_no; ?></option>
									<?php } else { ?>
									<option value="1"><?php echo $text_yes; ?></option>
									<option value="0" selected="selected"><?php echo $text_no; ?></option>
									<?php } ?>
								</select>                      
	                        </td>
							<td width="15%"></td>
							<td width="35%"></td>
	                    </tr>
						<?php */?>
	                    <tr>
                            <td width="15%">
                            	<label for="minimum"><?php echo $entry_minimum; ?></label>
                            </td>
                            <td width="35%" class="order-nopadding">
                            	<div class="input-icon right">
                				<i style="color: orange;" data-html="true" class="fa fa-info tooltips" data-container="body" data-original-title="Force a minimum ordered amount"></i>
							   <input style="padding-left:10px !important;" type="text" name="minimum" id="minimum" onkeyup="descriptionAutoFill();" 
							   value="<?php echo $minimum; ?>" size="2" class="textbox">			
                            </td>                                
							<?php /*?>
                            <td width="15%">
                            	<label for="subtract"><?php echo $entry_subtract; ?></label>
                            </td>
                            <td width="35%">
                             <select name="subtract" class="input-text validate[required]">
								<?php if ($subtract) { ?>
								<option value="1" selected="selected"><?php echo $text_yes; ?></option>
								<option value="0"><?php echo $text_no; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_yes; ?></option>
								<option value="0" selected="selected"><?php echo $text_no; ?></option>
								<?php } ?>
							</select>                      
                            </td>          
							<?php */ ?>                    
                        </tr>
                	</tbody>
                </table>
                   	  	<table class="table orderlist">
                   	  		<thead>
	                        	<tr><th colspan="2">Bar Code And Specials Details</th></tr>
                        	</thead>
                        	<table id="barcode" class="table orderlist invetorty-tab">
                               <thead>
							<tr>
								<td class="center"><?php echo $entry_barcode; ?></td>
								<td class="center">Action</td>
							</tr>
							</thead>
							<?php $barcode_row = 0; ?>
							<?php foreach ($product_barcodes as $product_barcode) { ?>
							<tbody id="barcode-row<?php echo $barcode_row; ?>">
								<tr>
									<td class="center"><input type="text" name="product_barcode[<?php echo $barcode_row; ?>][barcode]" value="<?php echo $product_barcode['barcode']; ?>" class="textbox"></td>
									<td class="center"><a onclick="$('#barcode-row<?php echo $barcode_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
								</tr>
							</tbody>
							<?php $barcode_row++; ?>
							<?php } ?>
							<tfoot>
								<tr>
									<td colspan="1" >
										<?php /*if ($error_barcode) { ?>
	                        			<span class="error"><?php echo $error_barcode; ?></span>
	                    				<?php }*/ ?>
									</td>
									<td class="center"><a onclick="addBarcode();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> <?php echo $button_add_barcode; ?></a></td>
								</tr>
							</tfoot>
                   	  </table>

                   	  <table id="product" class="table orderlist invetorty-tab">
                   	  		<thead>
	                        	<tr><th colspan="3">Packaged Items</th></tr>
                        	</thead>
                            <thead>
								<tr>
									<td class="center">Product</td>
									<td class="center">Qty</td>
									<td class="center">Action</td>
								</tr>
							</thead>
							<?php $product_row = 0; ?>
							<?php foreach ($child_pruducts as $products) { ?>
							<tbody id="product-row<?php echo $product_row; ?>">
								<tr>
									<td class="center"><input type="text" value="<?php echo $products['product_sku']; ?>" class="textbox" oninput="getProductDetails(this.value,<?php echo $product_row; ?>);" id="childProduct_<?php echo $product_row; ?>">
									<input type="hidden" value="<?php echo $products['sku']; ?>" id="childProductValue_<?php echo $product_row; ?>" name="child_pruducts[]" class="childProducts">
									<div id="suggesstion-boxs<?php echo $product_row; ?>"></div>
									</td>
									 <td class="center"><input class="textbox" type="text" name="child_products_qty[]" id="childProductqty_'+product_row+'" value="<?php echo $products['quantity']; ?>"  placeholder="Quantity" ></td>
									<td class="center"><a onclick="$('#product-row<?php echo $product_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
								</tr>
							</tbody>
							<?php $product_row++; ?>
							<?php } ?>
							<tfoot>
								<tr>
									<td colspan="2" >
									</td>
									<td class="center"><a onclick="addProduct();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i>Add Product</a></td>
								</tr>
							</tfoot>
                   	  </table>
                   	 </div>
                   	 <div class="col-md-12">
                   	 	<table class="table orderlist" id="stockTable">
                   		<thead>
                   			<tr><th colspan="10">Stock Details</th></tr>
                   		</thead>
	                        <tbody>   	                 			
                   				<tr>
	                                <td >Location Code</td>
	                                <td>Sku Qty</td>
	                                <!-- <td>Res Qty</td>
	                                <td>QOH Qty</td> -->
	                                <td>Sku Sales Qty</td>
	                                <td>Sku Cost</td>
	                                <td>Sku Price</td>
	                                <td>Sku Average</td>
	                                <td>Sku Min</td>
	                                <td>Sku Reorder</td> 
	                            </tr>
	                            
	                            <?php if($edit_form==1){
	                            	$i=0;
	                            	foreach($location_codes as $loc) { ?>
			                            <tr>
			                                <td class="order-nopadding">
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][location_Code]" value="<?php echo $loc['location_code']; ?>" class="" placeholder="LOCATION CODE" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
			                                	<input type="hidden" value="<?php echo $loc['sku']; ?>">
			                                </td>
			                                <td class="order-nopadding">
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_qty]" value="<?php echo $loc['sku_qty']; ?>" class="" placeholder="SKU QTY" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
			                                </td>
			                                <td class="order-nopadding">
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_salesQty]" value="<?php echo $loc['sku_salesQty']; ?>" class="" placeholder="SKU SALES QTY" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
			                                </td>
			                                <td class="order-nopadding">
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_cost]" value="<?php echo $loc['sku_cost']; ?>" class="" placeholder="SKU COST" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
			                                </td>
			                                <td class="order-nopadding">
			                                	<input type="number" name="location_code[<?php  echo $i; ?>][sku_price]" 
			                                	id="location_code_<?php  echo $i; ?>_sku_price" value="<?php echo $loc['sku_price']; ?>"
			                                	 class="location_code_<?php  echo $i; ?>_sku_price" placeholder="SKU PRICE" style="padding-left:10px !important;line-height: 36px;width:90%;" required />
			                                </td>
			                                <td class="order-nopadding">
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_avg]" value="<?php echo $loc['sku_avg']; ?>" class="" placeholder="SKU AVERAGE" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
			                                </td>
			                                <td class="order-nopadding">                          			
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_min]" id="location_code_<?php  echo $i; ?>_sku_min" value="<?php echo $loc['sku_min']; ?>" class="location_code_<?php echo $i; ?>_sku_min" placeholder="SKU MIN" style="padding-left:10px !important;line-height: 36px;width:90%;" />			 
			                                </td>
			                                <td  class="order-nopadding">                          			
			                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_reorder]" value="<?php echo $loc['sku_reorder']; ?>" class="" placeholder="SKU REORDER" style="padding-left:10px !important;line-height: 36px;width:90%;" />			 
			                                </td> 
			                            </tr>
			                            <?php $i++;?>
		                            <?php }?>
		                        <?php }else{
	                            	$i=0;
	                            	foreach($locations as $loc) { ?>
						                    <tr class="<?php echo $loc; ?>">
				                                <td class="order-nopadding">
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][location_code]" value="<?php echo $loc['location_code']; ?>" class="" placeholder="LOCATION CODE" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
				                                </td>
				                                <td class="order-nopadding">
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_qty]" value="" class="" placeholder="SKU QTY" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
				                                </td>
				                                <td  class="order-nopadding">
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_salesQty]" value="" class="" placeholder="SKU SALES QTY" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
				                                </td>
				                                <td class="order-nopadding">
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_cost]" value="" class="" placeholder="SKU COST" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
				                                </td>
				                                <td class="order-nopadding">
				                                	<input type="number" name="location_code[<?php  echo $i; ?>][sku_price]" id="location_code_<?php  echo $i; ?>_sku_price" id="location_code[<?php  echo $i; ?>][sku_price]" class="<?php if ($error_skuprice) {?> requiredborder  <?php } ?>" placeholder="SKU PRICE" style="padding-left:10px !important;line-height: 36px;width:90%;" required />
				                                </td>
				                                <td class="order-nopadding">
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_avg]" value="" class="" placeholder="SKU AVERAGE" style="padding-left:10px !important;line-height: 36px;width:90%;" readonly/>
				                                </td>
				                                <td class="order-nopadding">                          			
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_min]" id="location_code_<?php  echo $i; ?>_sku_min" value="" class="location_code_<?php echo $i; ?>_sku_min" placeholder="SKU MIN" style="padding-left:10px !important;line-height: 36px;width:90%;" />			 
				                                </td>
				                                <td class="order-nopadding">                          			
				                                	<input type="text" name="location_code[<?php  echo $i; ?>][sku_reorder]" value="" class="" placeholder="SKU REORDER" style="padding-left:10px !important;line-height: 36px;width:90%;" />			 
				                                </td> 
				                            </tr>
			                            <?php $i++;?>
		                            <?php }?>
		                        <?php }?>
	                   	 	</tbody>
                    	</table>
                   	 </div>
                   </div>
				</div>           
        	</form>
         </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(){

		var imageFileName = '<?php echo $image; ?>';
		if (imageFileName == ''){
			$('.imgDisplay').hide();
		} else {
			$('.imgUpload').hide();
		}
		if($('#ispackage').is(":checked")){
			$("#product").show();
		}else{
			$("#product").hide();
			clearChildProducts();
		}
	});
	$('#ispackage').change(function(){
		if($('#ispackage').is(":checked")){
			$("#product").show();
		}else{
			$("#product").hide();
			clearChildProducts();
		}
	});

	function clearChildProducts(){
		var product_id = '<?php echo $this->request->get['product_id']; ?>';
	}
	function readURL(input) {
	  	if (input.files && input.files[0]) {
		    var reader = new FileReader();
		    reader.onload = function(e) {
		      	$('#imgPreview').attr('src', e.target.result);
		    }
		    reader.readAsDataURL(input.files[0]); 
	  	}
	}

	$("#image").change(function() {
	  	readURL(this);
	});
	function removeImage(){
		var imageFileName = '<?php echo $image; ?>';
	    $.ajax({
	        url:"index.php?route=inventory/inventory/removeImage&token=<?php echo $token; ?>",
	        type:"POST",
	        data:{
	        	imageFileName : imageFileName,
	        	product_id : '<?php echo $this->request->get['product_id']; ?>'
	        },
	        success:function(out){
				$('.imgDisplay').hide();
				$('.imgUpload').show();
				console.log(out);
	        }
	    });
	}
	var barcode_row = <?php echo $barcode_row; ?>;
	function addBarcode() {
		html  = '<tbody id="barcode-row' + barcode_row + '">';
		html += '  <tr>'; 
		html += '    <td class="center"><input class="textbox" type="text" name="product_barcode[' + barcode_row + '][barcode]" id="row_'+barcode_row+'" value=""  placeholder="Barcode"></td>';
		html += '    <td class="center"><a onclick="$(\'#barcode-row' + barcode_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
		html += '  </tr>';
		html += '</tbody>';
		
		$('#barcode tfoot').before(html);
		$('#row_'+barcode_row).focus();
		barcode_row++;
	}

	var product_row = <?php echo $product_row; ?>;
	function addProduct() {
		html  = '<tbody id="product-row' + product_row + '">';
		html += '  <tr>'; 
		html += '    <td class="center"><input class="textbox" type="text" id="childProduct_'+product_row+'" value=""  placeholder="Product" oninput="getProductDetails(this.value,'+product_row+');"><input type="hidden" id="childProductValue_'+product_row+'" name="child_pruducts[]" class="childProducts"><div id="suggesstion-boxs'+product_row+'"></div></td>';
		html += '    <td class="center"><input class="textbox" type="text" name="child_products_qty[]" id="childProductqty_'+product_row+'" value="1"  placeholder="Quantity" ></td>';
		html += '    <td class="center"><a onclick="$(\'#product-row' + product_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
		html += '  </tr>';
		html += '</tbody>';
		
		$('#product tfoot').before(html);
		$('#row_'+product_row).focus();
		product_row++;
	}
</script> 
<script type="text/javascript">

  function descriptionAutoFill(){
  		var name  = $("#name").val();
    	if(name!=''){
    	    $('#description').val(name);
        }
        var price  = $("#price").val();
        var qty  = $("#minimum").val();
        var tlen = $('#stockTable tr').length;
        if(price!=''){
        	for(i=0;i<tlen;i++){
        	 $('#location_code_'+i+'_sku_price').val(price);
        	 $('#location_code_'+i+'_sku_min').val(qty);
        	 $('.location_code_'+i+'_sku_price').val(price);
        	 $('.location_code_'+i+'_sku_min').val(qty);
        	 $('.network_price').val(price);
        	}
        }
  }
  function getUOMTypes(code)
    {
  	  var ajaxData = 'uom_code='+code	
      $.ajax({
          url:"index.php?route=master/uomtype/getTypeDetails&token=<?php echo $token; ?>",
          type:"POST",
          data:ajaxData,
          success:function(out){
              out = JSON.parse(out);                            
              if(out == ''){
                html = '<option value="">None</option>';
               $('select[name=\'uom_type\']').html(html);
              } else {                              
                  if(out['Details'].length>=1){
                    html = '';
                    for (i = 0; i < out['Details'].length; i++) {
                       html += '<option value="' + out['Details'][i]['uom_type_value'] + '"'; 
                      /* if(i==0){
                         html += ' selected="selected"';
                       }*/
                       html += '>' + out['Details'][i]['uom_type_value'] + '</option>';                        
                    }
                  }else{
                    html = '<option value="1*1">1*1</option>';
                  }
                  $('select[name=\'sku_uom_type\']').html(html);
              }                           
            }
       });
    }

	function getProductDetails(sku,inputId){
	    if(sku.length < SEARCH_CHAR_CNT){
	        return false;
	    }
	    var location 	= '<?php echo $this->session->data['location_code']; ?>';
	    var selectedSku = $(".childProducts").map(function(){ return $(this).val(); }).get();
	    selectedSku.push("<?php echo $sku; ?>");
	    var ajaxData 	= 'sku='+encodeURIComponent(sku)+'&location_code='+location+'&product_row='+inputId+'&selectedSku='+selectedSku;

	    $.ajax({
	        type: "POST",
	        url: 'index.php?route=inventory/inventory/getProductDetails&token=<?php echo $token; ?>',
	        data: ajaxData,
	        success: function(result) {
	        
	            if (result=='') {
	                alert("Product Not Available or Disabled.");
	                $("#suggesstion-boxs"+inputId).hide();
	                $('#childProduct_'+inputId).val('');
	                return false;
	            } else {
	                if (result.indexOf("li")>= 0){
	                    $("#suggesstion-boxs"+inputId).show();
	                    $("#suggesstion-boxs"+inputId).html(result);
	                }else{
	                    // selectChildProduct(result);
	                }
	                return false;
	            }
	            $("#pre-loader-icon").hide();
	        }
	    });
}
function clearPurchaseData() {
    $('#sku').val('');
    $('#name').val('');
}
function selectChildProduct(sku,inputId){
    var newSku = sku.split("_");
	$('#childProductValue_'+inputId).val(newSku[0]);
	$('#childProduct_'+inputId).val(newSku[1]);
	$("#suggesstion-boxs"+inputId).hide();
}
function selectedProduct(val,type='') {
    var newVal = val.split("||");
        var price = parseFloat(newVal[3]);
        var raw_cost = parseFloat(newVal[4]);
        var sku_qty = parseFloat(newVal[6]);
        $("#product_id").val(newVal[0]);
        $("#parent_sku").val(newVal[1]);
        $("#suggesstion-box").hide();
}

$(document).ready(function(){
	getSubcategory();
	getCategory();
	descriptionAutoFill();
});
$('#product_category').change(function(){
	getSubcategory();
});
function getSubcategory(){
	var cate = $('#product_category').val();
	var subcate = '<?php echo $sub_category; ?>';

	if(cate!=''){
		$.ajax({
			type : 'POST',
			url: 'index.php?route=inventory/category/ajaxGetSubcategory&token=<?php echo $token; ?>',
			data: { cate:cate,subcate:subcate},
			success:function(data){
				$('#sub_category').html(data);
			} 
		});
	}
}
$('#product_department').change(function(){
	getCategory();
});
function getCategory(){
	var department = $('#product_department').val();
	var category   = '<?php echo $product_category; ?>';
	if(department !=''){
		$.ajax({
			type : 'POST',
			url: 'index.php?route=inventory/category/ajaxGetCategory&token=<?php echo $token; ?>',
			data: { department_code:department,category:category},
			success:function(data){
				$('#product_category').html(data);
			} 
		});
	getSubcategory();
	}
}

function getProductautoFill(sku) {
    if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }
    var location = '<?php echo $this->session->data['location_code']; ?>';
    var ajaxData = 'sku='+sku+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                alert("Product Not Available or Disabled.");
                $("#suggesstion-box").hide();
                clearPurchaseData();
                return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                    //$("#sku_qty").focus();
                }else{
                    selectedProduct(result);
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
$('#ispackage').click(function(){
	if($('#ispackage').is(":checked")){
		$("#product").show();
	}else{
		$("#product").hide();
	}
});
</script>
<?php echo $footer; ?>
<script src="view/assets/editor/jquery.richtext.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var str  = $('#description').text();
		var text = str.replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&gt;/g, '');
		$('#short_description').val(text);
        $('.content').richText({
            heightPercentage: 80
        });
    });
    function updateShortDescription(){
    	var str  = $('#description').text();
		var text = str.replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&gt;/g, '');
		$('#short_description').val(text);
    }
</script>