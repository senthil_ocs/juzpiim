<?php echo $header; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
     <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
         <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                 <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="printFunction();"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>
      <div style="clear:both"></div>            
      <div class="row">     
      <div class="col-md-12">      
      <div class="innerpage-listcontent-blocks">
        <table class="table orderlist statusstock">
          <tr>
            <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
          </tr>
          <tr>
            <td align="center" colspan="2">
              <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                    <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
            </td>
          </tr>
          <tr>
            <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
          </tr>
          <tr>
            <td align="center" colspan="2"><?php echo $companyInfo['country']; ?></td>
          </tr>
            <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
          <tr <?php echo $contact; ?>>
             <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
            <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
          </tr>
      </table>
       <div>
        <?php if (strtolower($stock_report) == 'barcode') { ?>
              <h1><?php echo ucfirst('Stock List'); ?></h1>
              <?php } elseif (strtolower($stock_report) == 'stock') { ?>
              <h1><?php echo ucfirst('Stock Report'); ?></h1>
              <?php } ?>

              <p>
                <?php if($filter_department != '' || $stock_date != ''){ ?>
                <span style="font-size: 15px;"> Filter By </span>
                 <?php } ?>
                <?php if($filter_department != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Department - <span id="filter_departmentid"></span> </span>
                <?php } ?>
                <?php if($stock_date != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Stock Date - <span id="stock_date"></span> </span>
                <?php } ?>
              </p>
          </div>

          <div class="caption set-bg-color"> 
              <form method="post" name="report_filter">
                <input name="type" id="type" type="hidden">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                     <tr class="filter">      

                      <td> 
                       <select name="filter_department" class="textbox" style="min-height: 35px; padding: 7px; width: 30px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $filter_department) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>

                        <td> 
                          <input style="min-height: 42px;padding: 7px;width: 65px;" type="text" id='datetimepicker' placeholder='Date' name="stock_date" value="<?php echo $stock_date ; ?>" class="textbox date" autocomplete="off">
                        </td> 

                        <td align="center" colspan="2">
                            <button style="width: 20%; min-height: 36px; border-radius:0 !important; float: left;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                        </tr>
                    </tbody>
                  </table>
              </form>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;"></div>

       <?php if (strtolower($stock_report) == 'stock') { ?>
          <table class="table orderlist statusstock">
            <thead>
              <tr class="heading">
                <td class="center" style="width: 5%;">S.No</td>
                <td class="center" style="width: 10%;">Inv.Code</td>
                <td class="center" style="width: 30%;">Description</td>
                <td class="center" style="width: 30%;">Department name</td>
                <td class="center" style="width: 10%;">Quantity</td>
                <td class="center" style="width: 10%;">Avg Cost</td>
                <td class="center" style="width: 10%;">Stock Value</td>
              </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
               <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">
                    <td class="center"><?php echo $i; ?></td>
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="center"><?php echo $product['departmentname']; ?></td>
                    <td class="center"><?php echo $product['quantity']; ?></td>
                    <td class="right"><?php echo $product['sku_price']; ?></td>
                    <td class="right"><?php echo $product['total']; ?></td>
                  </tr>
              <?php $i++; } ?> 
                   <?php if(count($products_sum)>=1){ ?>
                   <tr>
                     <td class="right" colspan="4">Total</td>
                     <td class="right"><?php echo $products_sum['total_qty']; ?></td>
                     <td class="right"><?php echo $products_sum['sku_avg_total']; ?></td>
                     <td class="right"><?php echo $products_sum['sku_price_total']; ?></td>
                   </tr>  
                   <?php } ?>
                        
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="8"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>               
      <?php } elseif(strtolower($stock_report) == 'barcode_list') { ?>
          <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                <td class="center">Inventory Code</td>
                <td class="center">Description</td>
                <td class="center">Barcode</td>
                <td class="center">Bin</td>
              </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php if($product['barcodes'] != '') { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="center"><?php echo $product['barcodes']; ?></td>
                    <td class="center"><?php echo $product['bin']; ?></td>                    
                  </tr>
              <?php } ?> 
              <?php } ?>             
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>  
     <?php }else if(strtolower($stock_report) == 'master'){ ?> 

            <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                  <td class="center" style="width: 5%;">S.No</td>
                  <td class="center" style="width: 10%;">Inv.Code</td>
                  <td class="center" style="width: 10%;">Department </td>
                  <td class="center" style="width: 10%;">Category </td>
                  <td class="center" style="width: 10%;">Brand </td>
                  <td class="center" style="width: 30%;">Description</td>
                  <td class="center" style="width: 10%;">Price</td>
                </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $i; ?></td>
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['sku_department_code']; ?></td>
                    <td class="center"><?php echo $product['sku_category_code']; ?></td>
                    <td class="center"><?php echo $product['sku_brand_code']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="center"><?php echo $product['sku_price']; ?></td>
                  </tr>
              <?php $i++; }  ?>             
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table> 

      <?php }?>     
      </div>
        <div class="pagination"><?php echo $pagination; ?></div>       
      </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function filterReport() {
  url = 'index.php?route=inventory/reports/printStock&token=<?php echo $token; ?>&stock_report=stock&page={page}';
  
  var filter_department = $('select[name=\'filter_department\']').attr('value');
  var stock_date = $('input[name=\'stock_date\']').attr('value');  

  if (filter_department != '*') {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }
  if (stock_date != '*') {
    url += '&stock_date=' + encodeURIComponent(stock_date);
  }

  location = url;
}
function printFunction(){
  $(".pagination").hide();
  $(".set-bg-color").hide();
  window.print();
  $(".pagination").show();
  $(".set-bg-color").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_departmentid" ).html(filter_department);
  var stock_date = $('[name="stock_date"]').val();
  $( "#stock_date" ).html(stock_date);
});

</script>
<?php echo $footer; ?>