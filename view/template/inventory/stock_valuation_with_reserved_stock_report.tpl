<!-- NEW CODE -->
<?php echo $header; ?>

<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>

              <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>

              <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i> Export PDF</a>

              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <div style="clear:both"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="innerpage-listcontent-blocks"> 
           <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
          
          <h3 class="page-title">Stock Valuation With Reserved Stock Report</h3>     
         <div>
              <p>
                <?php if($filter_location!='' || $filter_date_from!='' || $filter_date_to!='' || $filter_product!=''){ ?>
                    <span style="font-size: 15px;"> Filter By </span>
                 <?php } ?>
                 <?php if($filter_location != ''){ ?>
                    <span style="font-size: 15px;margin-right: 15px;"> Location : <span id="filter_location_codeid"><?php echo $filter_location;?></span> </span>
                <?php } ?>
                 <?php if($filter_product!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  SKU : <span id="stock_date"><?php echo $filter_product;?></span> </span>
                <?php } ?>
              </p>
          </div>
        </div>
      </div>

      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px -10px -15px -10px; padding:16px 0 0 0;">
            <div class="caption" id="reportsuggestion"> 
                <form method="post">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock">
                    <tbody>
                    <tr class="filter">
                       <td>
                         <select name="filter_location" id="filter_location" class="textbox">
                          <option value="">-- Select Location --</option>
                          <?php if(!empty($locations)){
                              foreach($locations as $value){ ?>
                                <option value="<?php echo $value['location_code'];?>"><?php echo $value['location_name']; ?></option>
                          <?php  } } ?> 
                      </select>
                      </td>
                      <td>
                         <select name="filter_vendor" id="filter_vendor" class="textbox">
                          <option value="">-- Select vendor --</option>
                          <?php
                             if(!empty($vendors)){
                                foreach($vendors as $value){ ?>
                                  <option value="<?php echo $value['vendor_code'];?>"> <?php echo $value['vendor_name']; ?></option>
                          <?php  } } ?> 
                          </select>
                      </td>                      
                      <td>
                         <select name="filter_department" id="filter_department" class="textbox">
                          <option value="">-- Select Department --</option>
                          <?php
                             if(!empty($departments)){
                                foreach($departments as $value){ ?>
                                  <option value="<?php echo $value['department_code'];?>"> <?php echo $value['department_name']; ?></option>
                          <?php  } } ?> 
                          </select>
                      </td>
                      <td>
                          <input type="text" name="filter_name" id="filter_name" class="textbox ui-autocomplete-input" placeholder="Search Product" autocomplete="off" value="<?php echo $data['filter_name']; ?>">
                         
                          <div id="suggesstion-box" class="auto-compltee"></div>
                      </td>                      
                      <td align="center" colspan="4">
                          <input type="hidden" name="page" value="<?php echo $data['page']; ?>">
                          <button style="width: 100%;" class="btn btn-zone btn-primary">&nbsp;<i class="fa fa-search"></i>&nbsp;Search</button>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                  </div>                        
                  </div>     
                </form>

            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                        <tr class="heading">
                          <td class="center">No</td>
                          <td class="center">Product ID</td>
                          <td class="center">sku</td>
                          <td class="center">Name</td>
                          <td class="center">Actual Stock</td>
                          <td class="center">Reserved Stock</td>
                          <td class="center">Available</td>
                        </tr>
                    </thead>
                    <tbody>           
                      <?php if (!empty($products)) { $i = 1; 
                            $class = 'odd'; 
                            foreach ($products as $product) {

                            $class = ($class == 'even' ? 'odd' : 'even');  
                            $aval = $product['sku_qty'] - $product['reservedQty']; ?>

                          <tr class="<?php echo $class; ?>">
                              <td class="center"><?php echo $i; ?></td>
                              <td class="center"><?php echo $product['product_id']; ?></td>
                              <td class="center"><?php echo $product['sku']; ?></td>
                              <td class="center"><?php echo $product['name']; ?></td>
                              <td class="center"><?php echo $product['sku_qty']; ?></td>
                              <td class="center"><?php echo $product['reservedQty']; ?></td>
                              <td class="center"><?php echo $product['availableQty']; ?></td>
                          </tr>
                        <?php $i++; } } else { ?>
                        <tr>
                            <td align="center" colspan="15">No Result!</td>
                        </tr>
                        <?php } ?>              
                      </tbody>
                  </table>
                </div>
                <div class="pagination"><?php echo $pagination; ?></div>    
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
      $('#filter_location').val('<?php echo $data['filter_location']; ?>');
      $('#filter_department').val('<?php echo $data['filter_department']; ?>');
      $('#filter_vendor').val('<?php echo $data['filter_vendor']; ?>');
      $('#filter_product_id').val('<?php echo $data['filter_product_id']; ?>');
      $('#filter_name').val('<?php echo $data['filter_name']; ?>');
  });

  function getProductautoFill(sku) {
    var filter_name = $("#filter_name").val();
    if(filter_name ==''){
        $('#filter_product_id').val('');
    }
    if(sku.length < SEARCH_CHAR_CNT){
        $('#filter_product_id').val('');
        return false;
    }
    var location = $("#filter_location").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;

    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
              //
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
        }
    });
  }

function selectedProduct(val,cnt) {
    var newVal = val.split("||");
    console.log(newVal);
    $('#filter_name').val(newVal[2]);
    $('#filter_product_id').val(newVal[0]);
    $('#suggesstion-box').hide();
    if(cnt=='1'){
        $( "#movement_report" ).submit();
    }
}
</script>
