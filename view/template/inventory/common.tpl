<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
			<?php if(isset($master)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> <?php echo $text_master; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($master);$i++){ ?> 
					<a href="<?php echo $master[$i]["link"]; ?>" class="icon-btn" id="<?php echo $master[$i]["id"];?>">
						<span class="shortcutkey"><?php echo $master[$i]["altkey"];?></span>
						<i class="<?php echo $master[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $master[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>					
				</div>
			</div>
        <?php } ?>
        <?php if(isset($inventory)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> Product</div>   
				</div>     
				<div class="portlet-body bgnone">
					<?php for($i=0;$i<count($inventory);$i++){ ?> 
					<a href="<?php echo $inventory[$i]["link"]; ?>" class="icon-btn"  id="<?php echo $inventory[$i]["id"];?>">
						<span class="shortcutkey"><?php echo $inventory[$i]["altkey"];?></span>
						<i class="<?php echo $inventory[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $inventory[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>					
				</div>
			</div>
        <?php } ?>
        <?php if(isset($pricing)) { ?>
			<div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-reorder"></i> <?php echo $text_pricing; ?>
					</div>   
				</div>     
				<div class="portlet-body bgnone">					
					<?php for($i=0;$i<count($pricing);$i++){ ?> 
					<a href="<?php echo $pricing[$i]["link"]; ?>" class="icon-btn"   id="<?php echo $pricing[$i]["id"];?>">
						<span class="shortcutkey"><?php echo $pricing[$i]["altkey"];?></span>
						<i class="<?php echo $pricing[$i]["icon"]; ?>"></i>
						<div>
							<?php echo $pricing[$i]["Text"]; ?>
						</div>
					</a>
					<?php } ?>					
				</div>
			</div>
        <?php } ?>
		</div>
	</div>
</div>
<?php echo $footer; ?>

<div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1">
    <form action="<?php echo $reprint_order; ?>" class="form-horizontal" method="POST" id="reprintform">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Quick Edit</h4>
    </div>
    <div class="modal-body">        
        <div class="form-body">
            <div id="qedit_error" class=""></div>
            <div class="form-group">
                <div class="row">
                <label class="col-md-3 control-label">Location</label>
                <div class="col-md-5">
                    <input type="text" name="location_code" id="location_code" class="form-control" placeholder="Enter Location" value="<?php echo $location_code; ?>" readonly>
                </div>
                <label class="col-md-2 control-label">
                </label>
                </div><br>
                <div class="row">
                <label class="col-md-3 control-label">Barcode</label>
                <div class="col-md-5">
                    <input type="text" name="qedit_barcode" id="qedit_barcode" class="form-control" placeholder="Enter Barcode">
                </div>
                <label class="col-md-2 control-label">
                        <button class="btn btn-secondary" type="button" style="margin-top: -10px; margin-left: -10px; float: left;" onclick="getProductautoFill();">
                        <i class="fa fa-search"></i>
                      </button> 
                </label>
                </div>
                <br>
                <div id="resDiv" style="margin-top: 2%; float:left;">
                    <table class="table orderlist">                       
                        <tbody>
                        <tr>
                            <td width="20%"> <label for="price">Selling Price </label></td>
                            <td width="80%" class="order-nopadding">                                                    
                            <input type="text" name="price" id="price" value="" class="textbox" placeholder="Enter Your Price">   
                            <input type="hidden" name="sku" id="sku" value="">                               
                            <input type="hidden" name="old_price" id="old_price" value="">                               
                        </td>
                        </tr>
                        <tr>
                            <td width="20%">
                            <label for="description">Description </label>
                            </td>
                            <td width="80%" class="order-nopadding">
                            <textarea name="description" id="description" rows="3" class="textbox"></textarea>
                            </td>                                
                        </tr>                                  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer form-action">
            <input type="hidden" name="location_code" id="location_code" value="<?php echo $location_code;?>">                       
            <button type="button" name="submit" id="qedit_submit" class="btn btn-primary" onclick="updateqd();">Update</button>
    </div>
    </form>
</div>
<script type="text/javascript">

function updateqd() {
    var location    = $("#location_code").val();
    var sku         = $("#sku").val();
    var description = $("#description").val();
    var price       = $("#price").val();
    var old_price       = $("#old_price").val();
    if(price==''|| description==''){
        alert("form should not empty");
        return false;
    }
    var ajaxData = 'sku='+sku+'&location='+location+'&description='+description+'&price='+price+'&old_price='+old_price;
    $.ajax({
        type: "POST",
        url: 'index.php?route=inventory/common/updateqe&token=<?php echo $token; ?>',
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result=='') {
                alert("Not updated.");
                return false;
            } else {
                    $("#price").val('');
                    $("#description").val('');
                    $("#sku").val('');
                    $("#qedit_barcode").val('');
                    $("#old_price").val('');
                alert('updated');

                $("#qedit_barcode").focus();

            }           
        }
    });   
}
 
 function getProductautoFill() {
    var qedit_barcode = $('#qedit_barcode').val();
    if(qedit_barcode.length<=2){
        return false;
    }
    var location = $("#location_code").val();
    var ajaxData = 'qedit_barcode='+qedit_barcode+'&location_code='+location;
    $.ajax({
        type: "POST",
        url: 'index.php?route=inventory/common/getProductIdByBarcode&token=<?php echo $token; ?>',
        data: ajaxData,
        dataType: 'json',
        success: function(result) {
            if (result!='0') {
                 $("#price").val(result.sku_price);
                 $("#description").val(result.name);
                 $("#sku").val(result.sku);
                 $("#old_price").val(result.sku_price);
               
            } else {
                 alert("Product Not Available or Disabled.");
                return false;   
            }           
        }
    });   
}
$( ".close" ).click(function() {
     $("#price").val('');
     $("#description").val('');
     $("#sku").val('');
     $("#qedit_barcode").val('');
     $("#old_price").val('');
     
});

$(document).ready(function(){
	Mousetrap.bind('alt+d', function(e) {
        e.preventDefault();
        var href = $('#department').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+c', function(e) {
        e.preventDefault();
        var href = $('#category').attr('href');
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+r', function(e) {
        e.preventDefault();
        var href = $('#brand').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+u', function(e) {
        e.preventDefault();
        var href = $('#ucom').attr('href');
        window.location.href=href;
        return false;
    });

     Mousetrap.bind('alt+n', function(e) {
        e.preventDefault();
        var href = $('#ninventory').attr('href');
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+m', function(e) {
        e.preventDefault();
        var href = $('#minventory').attr('href');
        window.location.href=href;
        return false;
    });
     Mousetrap.bind('alt+p', function(e) {
        e.preventDefault();
        var href = $('#barcode').attr('href');
        window.location.href=href;
        return false;
    });
   Mousetrap.bind('alt+a', function(e) {
        e.preventDefault();
        var href = $('#search').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+q', function(e) {
        e.preventDefault();
       $('#qedit').trigger('click');
        return false;
    });
     Mousetrap.bind('alt+i', function(e) {
        e.preventDefault();
        var href = $('#invmoment').attr('href');
        window.location.href=href;
        return false;
    });
       Mousetrap.bind('alt+o', function(e) {
        e.preventDefault();
        var href = $('#discount').attr('href');
        window.location.href=href;
        return false;
    });
});
</script>