<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Quick Edit Inventory</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="<?php echo $inven_comm; ?>">Inventory</a>
                        <i class="fa fa-angle-right"></i>                      
                    </li>
                    <li>
                        <a href="#">Quick Edit</a>                                                                     
                    </li>                                      
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>
                    </div>
                </div>                            
            </div>
            <!-- NEW CODE -->
            <div class="row">
            	<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="alert alert-block alert-danger fade in "><?php echo $error_warning; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
	             	</div>
        		 </div>
				<?php } ?>

			  <?php if ($success) { ?>
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="alert alert-block alert-success fade in "><?php echo $success; ?>
						<button type="button" class="close" data-dismiss="alert"></button>
	             	</div>
        		 </div>
				<?php } ?>

			<div class="col-md-12" style="float:left">

                        <div class="portlet-body">
                            <div class="invetory-tabtable">
                                <table class="table orderlist">                       
		                        <tbody>
		                            <tr>
		                                <td width="20%">
		                                	<label for="price"><?php echo $entry_price; ?><span class="required">*</span></label>
		                                </td>
		                                <td width="80%" class="order-nopadding">                          								
		                                	 <?php if ($error_price) { ?>
			                                	<div class="input-icon right">
												<i class="fa fa-warning tooltips required" data-container="body" data-original-title=" Enter Price"></i>
												<input type="text" name="price" value="<?php echo $price; ?>" class="textbox requiredborder" placeholder="Enter Your Price">    
		                               		</div>
		                               		<?php } ?>
		                               		 <?php if (!$error_price) { ?>
		                               		<input type="text" name="price" value="<?php echo $price; ?>" class="textbox" placeholder="Enter Your Price">                                
		                               		<?php } ?>						 
		                                </td>
		                            </tr>
		                           
		                        <tr>
		                                <td width="20%">
		                                	<label for="description"><?php echo $entry_description; ?></label>
		                                </td>
		                                <td width="80%" class="order-nopadding">
										  <textarea name="description" rows="3" class="textbox"><?php echo isset($description) ? $description : ''; ?></textarea>
		                                </td>                                
		                            </tr>			                       
		                        </tbody>
		                    </table>
                            </div>
                        </div>
                    </div>
				</div>
			</div> 
        	</form>
         </div>
    </div>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.draggable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.resizable.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/ui.dialog.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/external/bgiframe/jquery.bgiframe.js"></script>
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('.content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script> 
<script type="text/javascript">

	jQuery(document).ready(function() {

		jQuery('#name').change(function(){
			<?php if(empty($edit_form)){ ?>
			$("textarea[name=short_description]").val($(this).val());
			$("textarea[name=description]").val($(this).val());
			<?php } else {?>
			if($("textarea[name=short_description]").val()==""){
               $("textarea[name=short_description]").val($(this).val());
            } 
            if($("textarea[name=description]").val()!=""){
			   $("textarea[name=description]").val($(this).val());
		    }
			<?php } ?>
		});
	});
</script>
<?php echo $footer; ?>