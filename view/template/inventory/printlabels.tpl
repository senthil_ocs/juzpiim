<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cloud POS - <?php echo $heading_title; ?></title>
<link rel="stylesheet" type="text/css" href="view/stylesheet/style.css"/>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css"/>
</head>
<body class="invoice-page" style="background:none;">
<div id="content">
  <div class="box">
    <div class="content">
      <div class="form-list-container">
        <div class="print-master-report">
          
          <table class="list barcode_listing">
            <tbody>
              <?php if (!empty($info)) { ?>
              <?php for ($i = 0; $i < $info['no_labels']; $i++) { ?>
                  <tr>
                    <td class="center"><strong>Receipt Printer</strong></td>
                  </tr>
                  <tr>
                    <td class="center"><img src="<?php echo SITE_URL.'oblakpos/admin/' ?>code128.php?text=<?php echo $info['bar_code']; ?>" /><br/><?php echo $info['bar_code']; ?></td>
                  </tr>
                  <tr>
                    <td class="center"><strong><?php echo $info['price']; ?></strong></td>
                  </tr>
                  
                  <tr>
                    <td class="center">&nbsp;</td>
                  </tr>
              <?php } ?>
                  <tr>
                      <td class="center print_page_button last" colspan="3">
                          <a class="btn btn-zone" onClick="window.print();">Print</a>
                          <a href="<?php echo $back; ?>" class="btn btn-zone">Back</a>
                      </td>
                  </tr>
			  <?php } else { ?>              
                  <tr>
                      <td class="center last"><?php echo $text_no_results; ?></td>
                  </tr>
              <?php } ?>
              
            </tbody>
          </table>               
          
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
