<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
    <?php if ($error_warning) { ?>
        <div class="warning"><?php echo $error_warning; ?></div>
    <?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Market Places</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>                        
                        <a href="<?php echo $inven_comm; ?>">Inventory</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Market Places</a>                      
                    </li>                   
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Cancel</button></a></div>    
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>
            <div class="row">          
        <div class="col-md-12">         
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                    <label for="title">Product SKU</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="title" id="title" value="<?php echo $product_info['sku']; ?>" class="textbox" readonly>             
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                <label for="description">Name</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="description" id="description" value="<?php echo $product_info['sku_shortdescription']; ?>" class="textbox" readonly>
                                </td>                                
                            </tr>                                                  
                        </tbody>
                    </table>                   
                    <table id="tax-rule" class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
                            <td class="left">Title</td>
                            <td class="left">Price</td>
                            <td class="left">SKU Cost</td>
                            <td class="left">Quantity</td>
                            <td class="left">Action</td>
                        </tr>
                        </thead>
                    <?php foreach ($networks as $value) { ?>
                    <tbody>
                        <tr>
                            <td><?php echo $value['name']; ?></td>
                            <td>
                                <?php foreach ($product_networks as $prod_val) { 
                                    $product_val[] = $prod_val['network_id']; 
                                    if($prod_val['network_id'] == $value['id']) {
                                        $product_price = $prod_val['price'];
                                    }
                                }
                                if(in_array($value['id'],  $product_val)) {  ?>
                                        <input type="number" class="network_price" name="price_<?php echo $value['name']; ?>" id="price_<?php echo $value['name']; ?>" value="<?php echo $product_price; ?>"> 

                                <?php } else{ ?>
                                        <input type="number" class="network_price" name="price_<?php echo $value['name']; ?>" id="price_<?php echo $value['name']; ?>" value="<?php echo $product_info['price']; ?>"> 
                                <?php } ?>
                            </td>
                            <td>
                                <?php foreach ($product_networks as $prod_val) { 
                                    $product_val[] = $prod_val['network_id']; 
                                    if($prod_val['network_id'] == $value['id']) {
                                        $sku_cost = $prod_val['sku_cost'];
                                    }
                                }
                                if(in_array($value['id'],  $product_val)) {  ?>
                                        <input type="number" class="network_cost" name="cost_<?php echo $value['name']; ?>" id="cost_<?php echo $value['name']; ?>" value="<?php echo $sku_cost; ?>"> 

                                <?php } else{ ?>
                                        <input type="number" class="network_cost" name="cost_<?php echo $value['name']; ?>" id="cost_<?php echo $value['name']; ?>" value="<?php echo $product_info['sku_cost']; ?>"> 
                                <?php } ?>
                            </td>
                            <td>
                                <input type="number" class="network_Qty" name="qty_<?php echo $value['name']; ?>" id="qty_<?php echo $value['name']; ?>" 
                                <?php  foreach ($product_networks as $prod_val) { 
                                    if($prod_val['network_id'] == $value['id']) {
                                        echo "value='".$prod_val['quantity']."'";
                                    }
                                } ?>>
                            </td>
                            <td>
                                <?php foreach ($product_networks as $prod_val) { 
                                    $product_val[] = $prod_val['network_id']; 
                                    $product_response[] = $prod_val['network_response_id']; 
                                }
                                    if(in_array($value['id'],  $product_val)) { 
                                        if($value['id'] == 1) { ?>
                                            <button type="button" class="btn btn-primary network_submit" id="network_<?php echo $value['name']; ?>" name="network[]" network_response="<?php echo $product_response[0]; ?>" network="<?php echo $value['name']; ?>">Update</button>
                                        <?php } else { ?>
                                            <button type="button" class="btn btn-primary network_submit" id="network_<?php echo $value['name']; ?>" name="network[]" network_response="<?php echo $product_response[1]; ?>" network="<?php echo $value['name']; ?>">Update</button>
                                        
                                <?php }} else{ ?>
                                    <button type="button" class="btn btn-primary network_submit" id="network_<?php echo $value['name']; ?>" name="network[]" network="<?php echo $value['name']; ?>" network_response="">Post</button>
                                   
                                <?php } ?>
                            </td>
                        </tr>
                    </tbody>
                    <?php } ?>
                    </table>                    
                </div>
            </div>
            </form>
         </div>
    </div>
</div>
<script type="text/javascript">
$('.network_submit').on('click', function(){
    var network     = $(this).attr('network');
    var network_response = $(this).attr('network_response');
    var price       = $('#price_'+network).val();
    var qty         = $('#qty_'+network).val();
    var cost        = $('#cost_'+network).val();
    var sku         = '<?php echo $product_info['sku']; ?>';
    var prod_id     = '<?php echo $product_info['product_id']; ?>';
    var prod_dept   = '<?php echo $product_info['sku_department_code']; ?>';
    var name        = '<?php echo $product_info['name']; ?>';
    name            = name.replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
    
    $.ajax({
            url:"index.php?route=inventory/inventory/updateMarketPlace&token=<?php echo $token; ?>",
            type:"POST",
            data:{
                price       : price,
                qty         : qty,
                sku         : sku,
                sku_cost    : cost,
                prod_id     : prod_id,
                network     : network,
                prod_dept   : prod_dept,
                name        : name,
                network_response : network_response
            },
            success:function(out){
                out = JSON.parse(out);
                if(out.status !== true){
                    alert(out.message);
                }else{
                    location.reload(); 
                }
            }
        });
});
</script>
<?php echo $footer; ?>