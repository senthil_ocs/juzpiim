<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                 <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>
                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>

      <div style="clear:both"></div>            
        <div class="row">     
          <div class="col-md-12">      
          <div class="innerpage-listcontent-blocks"> 
          <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
              <h1>Stock Valuation Summary Report</h1>
              <p>
                <?php if($data['filter_date_from']) { ?>
                  <span style="font-size: 15px;margin-right: 15px;" >Filter By From Date - To Date : <?php echo $data['filter_date_from'] ?> - <?php echo $data['filter_date_to']; ?></span>
                <?php } ?>
                <?php if($data['filter_location_code']) { ?>
                  <span style="font-size: 15px;margin-right: 15px;" > Location :  <span id="filter_location_codeid"><?php echo $data['filter_location_code'];?></span></span>
                <?php } ?>
                <?php if($data['filter_dept_code']) { ?>
                  <span style="font-size: 15px;margin-right: 15px;" > Department : <span id="filter_dept_codeid"><?php echo $data['filter_dept_code'];?></span></span>
                <?php } ?>
                <?php if($data['filter_sku']) { ?>
                  <span style="font-size: 15px;margin-right: 15px;" > SKU : <span id="filter_product_id"><?php echo $data['filter_sku']; ?></span></span>
                <?php } ?>
              </p>
            </div>
          </div>
            
            <div class="caption set-bg-color"> 
              <form method="post" name="report_filter" id="movement_report">
                <input name="type" id="type" type="hidden">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                     <tr class="filter">                                           
                      <td> <input type="text" id='from_date' placeholder='From Date' name="filter_date_from" value="<?php echo $data['filter_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' id="to_date" name="filter_date_to" value="<?php echo $data['filter_date_to'] ; ?>" class="textbox date" autocomplete="off">
                        </td> 
                         <td>
                           <select name="filter_location_code" id="location_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location_code'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>//location_name

                            <?php  } } ?> 
                        </select>
                        </td>
                        <td>
                           <select name="filter_dept_code" id="dept_code" style="min-height: 35px; padding: 7px; width: 100%;">
                            <option value="">-- Select Dept Code --</option>
                            <?php
                               if(!empty($TodepartmentNew)){
                                    foreach($TodepartmentNew as $value){ ?>
                                    <option value="<?php echo $value['department_code'];?>" <?php if($data['filter_dept_code'] == $value['department_code'])echo 'selected'; ?> > <?php echo $value['department_name']; ?></option>//location_name

                            <?php  } } ?> 
                        </select>
                        </td>
                         <!-- <td>
                          <input type="text" placeholder='Sku' id="sku" name="filter_sku" value="<?php echo $data['filter_sku'] ; ?>" class="textbox">
                        </td> --> 
                         <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                            <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                         <td style="display: none;">
                          <input type="text" name="filter_barcodes" id="filter_barcodes" value="" class="textbox" placeholder="Scan Barcode">
                        </td> 
                        <td align="center" colspan="4">
                         <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>"> 
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>

                      </tr>
                    </tbody>
                  </table>
              </form>
            </div> 
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            <table class="table orderlist statusstock">
              <tr>
                <td colspan="10" align="right"></td>
                <td class="right">Stock Value : <?php echo number_format($products_sum['total_Stock_Value'],2);?></td>
              </tr>
            </table>  

            <div class="caption">
            <table class="table statusstock">
            <thead>
                <tr class="heading">
                  <td class="center">No</td>
                  <td class="center">Location</td>
                  <td class="center">Stock Date </td>
                  <td class="center">Sku </td>
                  <td class="center">Sku Description </td>
                  <td class="center">Opening Stock</td>
                  <td class="center">Purchase</td>
                  <td class="center">Purchase Return </td>
                  <td class="center">Sales </td>
                  <td class="center">Sales Return</td>
                  <td class="center">Transfer Qty</td>
                  <td class="center">Closing Stock</td>
                  <td class="center">Avg Cost</td>
                  <td class="center">Stock Value</td>
                </tr>
            </thead>
            <tbody>           
              <?php if (!empty($productstock)) { $i = 1; ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($productstock as $product) { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <!-- <td class="center"><?php echo $i; ?></td> -->
                    <td class="center"><?php echo $i+$product['pagecnt']; ?></td>
                    <td class="center"><?php echo $product['Location_Code']; ?></td>
                    <td class="center"><?php echo $product['Stock_Date']; ?></td>
                    <td class="center"><?php echo $product['Sku']; ?></td>
                    <td class="center"><?php echo $product['Sku_Description']; ?></td>
                     
                    <td class="center"><?php echo $product['Opening_Stock']; ?></td>
                    <td class="right"><?php echo $product['Purchase']; ?></td>
                    <td class="right"><?php echo $product['Purchase_Return']; ?></td>
                    <td class="right"><?php echo $product['Sales']; ?></td>
                    <td class="right"><?php echo $product['Sales_Return']; ?></td>
                    <td class="center"><?php echo $product['transfer_qty']; ?></td>
                    <td class="center"><?php echo $product['Closing_Stock']; ?></td>
                    <td class="center"><?php echo $product['Avg_Cost']; ?></td>
                    <td class="right"><?php echo $product['Stock_Value']; ?></td>
                  </tr>
              <?php $i++; }  ?>
              <?php  if(count($products_sum)>=1){ ?>
                <!-- <tr> -->
                 <!-- <td class="right" style="text-align: right;" colspan="13">Total</td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Opening_Stock']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Purchase']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Purchase_Return']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Sales']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Sales_Return']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_transfer_qty']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Closing_Stock']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Avg_Cost']; ?></td> -->
                 <!-- <td class="right"><?php echo $products_sum['total_Stock_Value']; ?></td> -->
                <!-- </tr>   -->
                <?php } ?>  
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="15"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table> 
      </div>          
        <div class="pagination"><?php echo $pagination; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?> 
<script type="text/javascript"><!--
/*function filterReport() {
  url = 'index.php?route=inventory/reports/stockvaluationreport&token=<?php echo $token; ?>';
  
  
  var filter_location_code = $('select[name=\'filter_location_code\']').attr('value');  
  if (filter_location_code != '*') {
    url += '&filter_location_code=' + encodeURIComponent(filter_location_code);
  }
  var filter_date_from = $('input[name=\'filter_date_from\']').attr('value');  
  if (filter_date_from != '*') {
    url += '&filter_date_from=' + encodeURIComponent(filter_date_from);
  }
  var filter_date_to = $('input[name=\'filter_date_to\']').attr('value');  
  if (filter_date_to != '*') {
    url += '&filter_date_to=' + encodeURIComponent(filter_date_to);
  }
  location = url;

}*/

function filterReport(report_type) {
  document.getElementById('type').value=report_type;
  var fromdt = $('#from_date').val();
  var todt = $('#to_date').val();
  var loc = $('#location_code').val();
    if(fromdt == ''){
         $('#from_date').focus();
        alert('Please Select the From date');
        return false;
    }
    if(todt == ''){
          $('#to_date').focus();
        alert('Please Select the To date');
         return false;
    }
    if(loc == ''){
         $('#location_code').focus();
        alert('Please Select the Location Code');
        return false;
    }
  document.report_filter.submit();
}
function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
}
$(document).ready(function(){
  var filter_location_code = $('[name="filter_location_code"] option:selected').text();
  $( "#filter_location_codeid" ).html(filter_location_code);

  var filter_dept_code = $('[name="filter_dept_code"] option:selected').text();
  $( "#filter_dept_codeid" ).html(filter_dept_code);

  var filter_brand = $('[name="filter_brand"] option:selected').text();
  $( "#filter_brandid" ).html(filter_brand);

  var filter_name = $("#filter_name").val();
  $( "#filter_product_id" ).html(filter_name);
});
 


 /*
  $('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
    $.ajax({
      url: 'index.php?route=inventory/inventory/autocompleteForTouchForm&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
      dataType: 'html',
      success: function(str) {
        if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

        }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
        }
      }
    });
  }
  
});
 */
  
function getProductautoFill(sku) {

    /*var frm_date = $("#filter_date_from").val();
    var to_date = $("#filter_date_to").val();
    if(frm_date==''){
      alert("From date should not empty");
      $("#filter_date_from").focus();
    }else if(to_date==''){
      alert("To date should not empty");
      $("#filter_date_to").focus();
    }*/
  var filter_name = $("#filter_name").val();
   
   if(filter_name ==''){
    $('#filter_sku').val('');
   }

    if(sku.length < SEARCH_CHAR_CNT){
        return false;
    }


    var location = $("#filter_location").val();
    var ajaxData = 'sku='+sku+'&location_code='+location;
    //alert(ajaxData);
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
                //alert("Product Not Available or Disabled.");
                //$("#suggesstion-box").hide();
               // clearPurchaseData();
               // return false;
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}
function selectedProduct(val,cnt) {
        var newVal = val.split("||");
        //alert(newVal);
       //console.log(newVal);
        $('#filter_name').val(newVal[2]);
        $('#filter_sku').val(newVal[1]);
        $('#suggesstion-box').hide();
        if(cnt=='1'){
            $( "#movement_report" ).submit();
        }

    }

function getValue(name,sku){
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}
</script> 
   