<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Inventory Movement</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
				        </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" id="save_btn"><i class="fa  fa-level-up"></i> <?php echo 'Submit'; ?></button>               
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Clear</button></a></div>    
                    </div>
                </div>                            
            </div>
            <div class="row">
            <div style="clear:both"></div>         
            <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_inventory_code; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
								                  <input type="text" name="inventory_code" id="inventory_code" value="<?php echo $inventory_code; ?>" class="textbox" placeholder="Enter Inventory Code" onchange="getInventoryDetail(this.value,'bar_code','name','price');" autofocus>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                               <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" readonly>             
                                </td>                                
                            </tr>
                            <tr>
                                <td width="20%">
                                  <label for="name"><?php echo $entry_bar_code; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">                                          
                                  <input type="text" name="bar_code" id="bar_code" value="<?php echo $bar_code; ?>" class="textbox" readonly>           
                                </td>
                            </tr>
              							<tr>
              							<td width="20%">
              								<label for="name"><?php echo $entry_price; ?></label>
              							</td>
              							<td width="80%" class="order-nopadding">
              								<input type="text" name="price" id="price" value="<?php echo $price; ?>" class="textbox" readonly>
              							</td>
              						</tr>
                       </tbody>
                    </table>
            <!-- BARCODE PRINT --> 
            <?php if(!empty($product) || !empty($purchase) || !empty($purchaseReturn) || !empty($sales) || !empty($salesReturn)) { ?>
            <button type="button" id="pdf_btn" class="btn btn-danger" style="margin: 5px 15px 15px 0; border-radius: 4px; float:right"><i class="fa fa-file-pdf-o"></i> PDF</button>
             <button type="button" class="btn btn-primary" style="margin: 5px 15px 15px 0; border-radius: 4px; float:right" onClick="printDiv('printable')" id="printbtn"><i class="fa fa-print"></i> PRINT</button>             
             <?php } ?>
            <div id="printable">
              <!-- INVENTORY SUMMARY -->
              <h3 style="margin:0 0 10px 13px"><?php echo 'INVENTORY SUMMARY'; ?></h3>
              <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td align="center"><?php echo 'Added Date'; ?></td> 
                            <td align="center"><?php echo 'Inventory Code'; ?></td>
                            <td align="center"><?php echo 'Product Name'; ?></td>
                            <td align="center"><?php echo 'Qty Left'; ?></td>
                            <td align="center"><?php echo 'Price'; ?></td>                                                      
                        </tr>
                    </thead>                      
                        <tbody>                          
                            <?php if (!empty($product)) { ?>
                            <tr>
                              <td align="center"><?php echo date("Y-m-d g:i A", strtotime($product['date_added'])); ?></td>
                              <td align="center"><?php echo $product['sku']; ?></td>
                              <td align="center"><?php echo $product['name']; ?></td>
                              <td align="center"><?php echo $product['quantity']; ?></td>
                              <td align="center"><?php echo $product['price']; ?></td>                                          
                            </tr>
                            <?php } else { ?>
                            <tr>
                            <td colspan="5" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                <!-- PURCHASE -->
               <h3 style="margin:0 0 10px 13px"><?php echo 'PURCHASE'; ?></h3>
              <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td align="center"><?php echo 'Transaction Date'; ?></td>
                            <td align="center"><?php echo 'Transaction No'; ?></td>
                            <td align="center"><?php echo 'Qty'; ?></td>
                            <td align="center"><?php echo 'Price'; ?></td>
                        </tr>
                    </thead>                      
                        <tbody>                          
                            <?php if (!empty($purchase)) { ?>
                            <?php for($i=0; $i<count($purchase); $i++) { ?>
                            <tr>
                              <td align="center"><?php echo date("Y-m-d g:i A", strtotime($purchase[$i]['transaction_date'])); ?></td>
                              <td align="center"><?php echo $purchase[$i]['transaction_no']; ?></td>
                              <td align="center"><?php echo $purchase[$i]['quantity']; ?></td>
                              <td align="center"><?php echo $purchase[$i]['price']; ?></td>                   
                            </tr> 
                            <?php } ?>                          
                            <?php } else { ?>
                            <tr>
                            <td colspan="5" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                <!-- PURCHASE RETURN -->
               <h3 style="margin:0 0 10px 13px"><?php echo 'PURCHASE RETURN'; ?></h3>
              <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td align="center"><?php echo 'Transaction Date'; ?></td>
                            <td align="center"><?php echo 'Transaction No'; ?></td>
                            <td align="center"><?php echo 'Qty'; ?></td>
                            <td align="center"><?php echo 'Price'; ?></td>                           
                        </tr>
                    </thead>                      
                        <tbody>                          
                            <?php if (!empty($purchaseReturn)) { ?>
                            <?php for($i=0; $i<count($purchaseReturn); $i++) { ?>
                            <tr>
                              <td align="center"><?php echo date("Y-m-d g:i A", strtotime($purchaseReturn[$i]['transaction_date'])); ?></td> 
                              <td align="center"><?php echo $purchaseReturn[$i]['transaction_no']; ?></td>
                              <td align="center"><?php echo $purchaseReturn[$i]['quantity']; ?></td>
                              <td align="center"><?php echo $purchaseReturn[$i]['price']; ?></td>                  
                            </tr>
                            <?php } ?>                           
                            <?php } else { ?>
                            <tr>
                            <td colspan="5" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                <!-- SALES -->
              <h3 style="margin:0 0 10px 13px"><?php echo 'SALES'; ?></h3>
              <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td align="center"><?php echo 'Added Date'; ?></td> 
                            <td align="center"><?php echo 'Invoice No'; ?></td>
                            <td align="center"><?php echo 'Qty'; ?></td>
                            <td align="center"><?php echo 'Price'; ?></td>                          
                        </tr>
                    </thead>                      
                        <tbody>                          
                            <?php if (!empty($sales)) { ?>
                            <?php for($i=0; $i<count($sales); $i++) { ?>
                            <tr>
                              <td align="center"><?php echo date("Y-m-d g:i A", strtotime($sales[$i]['date_added'])); ?></td> 
                              <td align="center"><?php echo $sales[$i]['invoice_no']; ?></td>
                              <td align="center"><?php echo $sales[$i]['quantity']; ?></td>
                              <td align="center"><?php echo $sales[$i]['price']; ?></td>                  
                            </tr>
                            <?php } ?>                           
                            <?php } else { ?>
                            <tr>
                            <td colspan="5" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
                <!-- SALES RETURN -->
                <h3 style="margin:0 0 10px 13px"><?php echo 'SALES RETURN'; ?></h3>
              <div class="purchase_grid col-md-12">       
                    <table id="special" class="table orderlist statusstock">
                    <thead>
                        <tr class="heading-purchase">
                            <td align="center"><?php echo 'Added Date'; ?></td>  
                            <td align="center"><?php echo 'Invoice No'; ?></td>
                            <td align="center"><?php echo 'Qty'; ?></td>
                            <td align="center"><?php echo 'Price'; ?></td>                         
                        </tr>
                    </thead>                      
                        <tbody>                          
                            <?php if (!empty($salesReturn)) { ?>
                            <?php for($i=0; $i<count($salesReturn); $i++) { ?>
                            <tr>
                              <td align="center"><?php echo date("Y-m-d g:i A", strtotime($salesReturn[$i]['date_added'])); ?></td> 
                              <td align="center"><?php echo $salesReturn[$i]['invoice_no']; ?></td>
                              <?php if($salesReturn[$i]['quantity'] == '-1') { ?>
                              <td align="center"><?php echo '1'; ?></td>
                              <?php } ?>
                              <td align="center"><?php echo $salesReturn[$i]['price']; ?></td>                  
                            </tr>
                            <?php } ?>                           
                            <?php } else { ?>
                            <tr>
                            <td colspan="5" align="center" class="purchase_no_data"><?php echo $text_no_data; ?></td>
                            </tr>
                            <?php } ?>  
                        </tbody>
                    </table>
                </div>
          </div>    
        </div>
      </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">

  function getInventoryDetail(v,id,id1,id2)
  {
      $.ajax({
          url:"index.php?route=inventory/inventorymovement/getDetails&token=<?php echo $token; ?>",
          type:"POST",
          data:{inventory_code:v},
          success:function(out){
              out = JSON.parse(out);                            
              if(out == ''){
                alert('Enter a valid Inventory Code');
                $("#inventory_code").focus();
              } else {                              
                  document.getElementById(id).value = out['barcode'];
                  document.getElementById(id1).value = out['name'];
                  document.getElementById(id2).value = out['price'];
              }                           
            }
       });
   }

    function printDiv(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;
       document.body.innerHTML = printContents;
       window.print();
       document.body.innerHTML = originalContents;
    }
</script>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function() {
    Mousetrap.bind('alt+p', function(e) {
       e.preventDefault();
       $("#printbtn").trigger('click',function(){
       });
   });

    $('#pdf_btn').click(function () {
    var pdf = new jsPDF('p', 'pt', 'letter')
    , source = $('#printable')[0]
    , specialElementHandlers = {
        '#bypassme': function(element, renderer){      
            return true
        }
    }

    margins = {
        top: 60,
        bottom: 60,
        left: 40,
        width: 522
      };

    pdf.fromHTML(
        source 
        , margins.left
        , margins.top
        , {
            'width': margins.width
            , 'elementHandlers': specialElementHandlers
        },
        function (dispose) {
            pdf.save('inventorymovement.pdf');
          },
        margins
      )
    });
});
</script>