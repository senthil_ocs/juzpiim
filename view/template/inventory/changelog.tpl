<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">Change Log</h3>			
			<div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php echo $home; ?>">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>						
						<a href="<?php echo $inventory; ?>">Inventory List</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="<?php echo $inventory_changelog; ?>">Change Log</a>						
					</li>					
				</ul>
				            						
			</div>			
			<div style="clear:both"></div>
				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>
			  	<!-- NEW SEARCH -->
           <div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
            <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
                <div class="caption">                  
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 30px;">
                     <tbody>
	                      <tr class="filter">                       
	                        <td><input type="text" name="filter_fromdate" value="<?php echo $filter_fromdate; ?>" class="textbox date" placeholder="From Date"></td>

	                        <td><input type="text" name="filter_todate" value="<?php echo $filter_todate; ?>" class="textbox date" placeholder="To Date"></td>
	                        
	                      <td align=""><button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
	                    </tr>
                      </tbody>
                    </table>
                 </div>                        
                
            </div>
                    
             <div style="clear:both; margin: 0 0 15px 0;">
            </div>
           </div>
          </div>
                	<!-- END NEW SEARCH -->	
    	 <div style="clear:both; margin: 0 0 15px 0;">
        </div>
             			 
			<div class="row">		
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
					<div class="tbl_heading"> General</div>
                 	<div class="innerpage-listcontent-blocks">
					<table class="table orderlist statusstock">
	                    <thead>
	                      <tr class="heading">
							  <td class="center">Date</td>
		                      <td class="center">Action</td>
		                      <td class="center">Updated By</td>
		                      <td class="center">Sku</td>
		                      <td class="center">Price</td>
		                      <td class="center">Weight</td>
		                      <td class="center">Tax Class</td>
		                      <td class="center">Bin</td>
		                      <td class="center">Vendor</td>
		                      <td class="center">Status</td>
							</tr>
	                    </thead>
	                    <tbody>                        
						<?php if ($productchange) {?>
						  <?php $class = 'odd'; ?>
						<?php foreach ($productchange as $product) {
							switch ($product['action']) {
								case 'insert':
									$strClass="success";
									break;
								case 'update':
									$strClass="update";
									break;
								case 'delete':
									$strClass="danger";
									break;
								default:
									$strClass="active ";
									break;
							}
						 ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $strClass; ?>">
							  <td class="center"><?php echo date('d-m-Y h:i A',strtotime($product['date'])); ?></td>
		                      <td class="center"><?php echo $product['action']; ?></td>
		                      <td class="center"><?php echo $product['updated_by']; ?></td>
		                      <td class="center"><?php echo $product['sku']; ?></td>
							  <td class="right"><?php  echo $product['price']; ?></td>
							  <td class="center"><?php echo $product['weight']; ?></td>
		                      <td class="center"><?php echo $product['tax']; ?></td>
		                      <td class="center"><?php echo $product['bin']; ?></td>
							  <td class="center"><?php echo $product['vendor']; ?></td>
							  <td class="center"><?php echo $product['status']; ?></td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
							  <td align="center" colspan="10"><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
	                        </tbody>
	                    </table>
                    </div>

                     <div class="tbl_heading"> Description</div>
                    <div class="innerpage-listcontent-blocks">
					<table class="table orderlist statusstock">
	                    <thead>
	                      <tr class="heading">
							  <td class="center">Date</td>
		                      <td class="center">Action</td>
		                      <td class="center">Updated By</td>
							  <td class="center">Name</td>
		                      <td class="center">Short Description</td>
		                      <td class="center">Description</td>
		                      <td class="center">Remarks</td>
							</tr>
	                    </thead>
	                    <tbody>                        
						<?php if ($productdescription) { ?>
						  <?php $class = 'odd'; ?>
						<?php foreach ($productdescription as $productdesc) { 
							switch ($productdesc['action']) {
								case 'insert':
									$strClass="success";
									break;
								case 'update':
									$strClass="update";
									break;
								case 'delete':
									$strClass="danger";
									break;
								default:
									$strClass="active ";
									break;
							}
						 ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $strClass; ?>">
							  <td class="center"><?php echo date('d-m-Y h:i A',strtotime($productdesc['dt_datetime'])) ?></td>
		                      <td class="center"><?php echo $productdesc['action']; ?></td>
		                      <td class="center"><?php echo $productdesc['updated_by']; ?></td>
		                      <td class="center"><?php echo $productdesc['name']; ?></td>
		                      <td class="center"><?php echo $productdesc['short_description']; ?></td>
		                      <td class="center"><?php echo $productdesc['description']; ?></td>
		                      <td class="center"><?php echo $productdesc['remarks']; ?></td>
							</tr>
							<?php }?>
							<?php } else { ?>
							<tr>
							  <td align="center" colspan="7"><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
	                        </tbody>
	                    </table>
                    </div>

                 <div class="tbl_heading">Barcode</div>
                    <div class="innerpage-listcontent-blocks">
					<table class="table orderlist statusstock">
	                    <thead>
	                      <tr class="heading">
							  <td class="center">Date</td>
		                      <td class="center">Action</td>
		                      <td class="center">Updated By</td>
							  <td class="center">Barcode</td>
							</tr>
	                    </thead>
	                    <tbody>                        
						<?php if ($productbarcode) { ?>
						  <?php $class = 'odd'; ?>
						<?php foreach ($productbarcode as $barcode) {
							switch ($barcode['action']) {
								case 'insert':
									$strClass="success";
									break;
								case 'update':
									$strClass="update";
									break;
								case 'delete':
									$strClass="danger";
									break;
								default:
									$strClass="active";
									break;
							}
						 ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $strClass; ?>">
							  <td class="center"><?php echo date('d-m-Y h:i A',strtotime($barcode['dt_datetime'])); ?></td>
		                      <td class="center"><?php echo $barcode['action']; ?></td>
		                      <td class="center"><?php echo $barcode['updated_by']; ?></td>
		                      <td class="center"><?php echo $barcode['barcode']; ?></td>
							</tr>
							<?php } ?>
							<?php } else { ?>
							<tr>
							  <td align="center" colspan="10"><?php echo $text_no_results; ?></td>
							</tr>
						<?php } ?>
	                        </tbody>
	                    </table>
                    </div> 
                </form>
                <div class="pagination"><?php //echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=inventory/changelog&token=<?php echo $token; ?>&product_id=<?php echo $product_id; ?>';
	
	var filter_fromdate = $('input[name=\'filter_fromdate\']').attr('value');
	
	if (filter_fromdate) {
		url += '&filter_fromdate=' + encodeURIComponent(filter_fromdate);
	}
	
	var filter_todate = $('input[name=\'filter_todate\']').attr('value');
	
	if (filter_todate) {
		url += '&filter_todate=' + encodeURIComponent(filter_todate);
	}
	
	location = url;
}
//--></script> 
<?php echo $footer; ?>