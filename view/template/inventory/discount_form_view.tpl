<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Pair Discount</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <button type="button" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="submitform();"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
               
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
           
	<div style="clear:both"></div>
            <div class="row">
            <div style="clear:both"></div>         
        <div class="col-md-12"> 
         <?php if ($error_warning) { ?>
				<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
		    <?php } ?>       	
                    <table class="table orderlist">                       
                        <tbody>
                             <tr>
                                <td width="20%">
                                    <label for="name">Location Code *</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                <?php if($locations) {
                                    foreach ($locations as $count => $location) { 
                                        if($count % 3 ==0){ echo '<br>'; } 
                                ?>
                                <div class="col-md-4 col-xs-6 col-sm-6" >
                                 <input type="checkbox" name="location[]" value="<?php echo $location['location_code'];?>" <?php if(in_array($location['location_code'],$location_code)){ echo 'checked'; } ?> > &nbsp;<?php echo $location['location_name']; ?>
                                 <br><br>
                                 </div> 
                                  <?php  }
                                } ?>
                                  
                                
                                </td>  
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="name">Discount Code *</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="discount_code" id="discount_code" value="<?php echo $discount_code; ?>" readonly class="textbox" placeholder="Enter Discount Code">
                                </td>  
                            </tr>

                            <tr>
                                <td width="20%">
                                	<label for="name">Description</label>
                                </td>

                                <td width="80%" class="order-nopadding">
                                   <textarea name="discount_description" id="discount_description" rows="2" class="textbox"><?php echo $discount_description;?></textarea>
                                </td>                                
                            </tr>

                            <tr>
                                <td width="20%">
                                    <label for="name">Quantity *</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="discount_quantity" id="discount_quantity" value="<?php echo $discount_quantity; ?>" class="textbox" placeholder="Enter Discount Quantity" readonly>                                   
                                </td>  
                            </tr>
                            <tr>
                                <td width="20%">
                                    <label for="name">Price *</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="discount_price" id="discount_price" value="<?php echo $discount_price; ?>" class="textbox" placeholder="Enter Discount Price">
                                    
                                </td>  
                            </tr>   

                             <tr>
                                <td width="20%">
                                    <label for="name">From Date</label>
                                </td>

                                <td width="80%" class="order-nopadding">
                                  <input type="text" id="discount_fromdate" name="discount_fromdate" value="<?php echo $discount_fromdate; ?>" placeholder='Choose From Date' name="filter_date_from" class="textbox date" >
                                </td>                                
                            </tr>

                            <tr>
                                <td width="20%">
                                    <label for="name">To Date</label>
                                </td>

                                <td width="80%" class="order-nopadding">
                                  <input type="text" id="discount_todate" placeholder='Choose To Date' name="discount_todate" value="<?php echo $discount_todate; ?>" class="textbox date">
                                </td>                                
                            </tr>

                            <tr>
                                <td width="20%">
                                    <label for="name">Mixed Item</label>
                                </td>
                                <td width="80%">
                                    <input type="checkbox" name="mixed" id="mixeditem" value="<?php if($mixeditem){ echo 'false'; } ?>" <?php if($mixeditem){ echo "checked='checked'"; } ?> disabled>
                                </td>
                            </tr>

                            <tr>
                                <td width="20%">
                                    <label for="name">Assigned Products</label>
                                </td>

                                <td width="80%">
                                       

                                        <table id="special" class="table orderlist invetorty-tab">
                                            <thead>
                                               <tr>                                                     
                                                    <td width="10%" class="center">SKU</td>
                                                    <td width="70%" class="center">Name</td>
                                                    <td class="center">Action</td>
                                                    <td colspan="1"></td>
                                                    <td></td>
                                                    <input type="hidden" id="firstval" value="<?php echo $product_specials[0]['price'] ?>">
                                                </tr> 
                                            </thead> 
                                             <?php $special_row = 0; ?>
												<?php foreach ($product_specials as $product_special) {
                                                    $sku = $product_special['sku'];
                                                    $price = $product_special['price'];
                                                 ?>
												<tbody id="special-row<?php echo $special_row; ?>">
													<tr><input type="hidden" id="<?php echo $sku; ?>_price" value="<?php echo $price; ?>">
														<td class="center"><input class="textbox" type="text" name="product_special[<?php echo $special_row; ?>][sku]" value="<?php echo $product_special['sku']; ?>" class="textbox" /></td>
						                                <td class="center"><input class="textbox" type="text" name="product_special[<?php echo $special_row; ?>][name]" value="<?php echo $product_special['name']; ?>" /></td>
														<td class="center"><a onclick="$('#special-row<?php echo $special_row; ?>').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>
														<td colspan="1"></td>
														<td></td>
													</tr>
												</tbody>
												<?php $special_row++; ?>
												<?php } ?>
											<tfoot>
						                    </tfoot>
                                        </table>         
                                </td>                                
                            </tr>
                            <tr>
                                <td width="20%">
                                    <label for="name">Select Products</label>
                                </td>

                             

                                <td width="50%" class="order-nopadding">
                                   <input type="text" name="filter_name" id="filter_name" value="" class="textbox" autocomplete="off" style="min-width: 30% !important;" placeholder="Enter Product Details">

                                   <input type="hidden" name="psku" id="psku" value="">
                                   <input type="hidden" name="discount_value" id="discount_value" value="">
                                   <input type="hidden" name="sku_price" id="sku_price" value="">
                                   <a onclick="addProducts();" class="btn btn-zone btn-primary"><i class="fa"></i> Add Products </a>
                                       <div id="suggesstion-box" class="auto-compltee"></div>
                                </td>                                
                            </tr>
                            </tbody>
                    </table>
                </div>
            </div>
             <input type="hidden" id="product_code" value=""> 
        	</form>
         </div>
    </div>
</div>




<script type="text/javascript"><!--
    var special_row = <?php echo $special_row; ?>;
    function removeproduct(e){
        var temparray=[];
        var inputs= $("input[name='product_special["+e+"][sku]']" ).val();
        var pcd_code=$('#product_code').val();
            temparray =pcd_code.split(",");
   
         temparray.splice($.inArray(inputs, temparray),1);
         var pstr = temparray.toString();
            $('#product_code').val(pstr);

            $('#special-row'+e).remove();
    }
    function addProducts() {
        var temp_arr=[];
        var sku  = $('#psku').val();
        var name = $('#filter_name').val();
        var sku_price = $('#sku_price').val();
        if(sku=='' || name==''){
            alert("select Product for assign");
            return false;   
        }

        if($('#mixeditem').prop("checked") == true){
            if($('#firstval').val() != "" ){
                if($('#firstval').val() != sku_price){
                    alert("For Mixed Item Add same selling Price items");
                    $('#filter_name').val('');
                    return false;
                }
            }
        }

        var pcd_code=$('#product_code').val();
       
       

        if(pcd_code == ''){
            $('#product_code').val(sku);
        }else{
         temp_arr =pcd_code.split(",");
         var counted=jQuery.inArray(sku, temp_arr);
            if(counted>=0){
                alert('Cannot Added Product');
                 $('#psku').val('');
                $('#filter_name').val('');
                return false;
            }else{
                var temppdcode=pcd_code+','+sku;
                $('#product_code').val(temppdcode);
            }
            
        }
             

        html  = '<tbody id="special-row' + special_row + '">';
        html += '  <tr>'; 
        html += '    <td class="center"><input class="textbox" type="text" name="product_special[' + special_row + '][sku]" value="'+sku+'" readonly></td>';
        html += '    <td class="center"><input class="textbox" type="text" name="product_special[' + special_row + '][name]" value="'+name+'" readonly></td>';
        //html += '    <td class="center" ><a onclick="$(\'#special-row' + special_row + '\').remove();" class="btn btn-zone btn-danger removeproduct "><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
        html += '    <td class="center" onclick="removeproduct('+special_row+');"><a href="#" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> <?php echo $button_remove; ?></a></td>';
        html += '<td colspan="1"></td>';
        html += '<td></td>';
        html += '  </tr>';
        html += '</tbody>';
       
        $('#special tfoot').before(html);
        special_row++;
        $('#psku').val('');
        $('#filter_name').val('');

    }
    //--></script> 
<?php echo $footer; ?>
<script type="text/javascript">
    $('input[name=\'filter_name\']').autocomplete({
    'source': function(request, response) {
        $.ajax({
            url: 'index.php?route=inventory/discount/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
            dataType: 'html',
            success: function(str) {
                if(str){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(str);

                }else{
                    $("#suggesstion-box").hide();
                    $("#suggesstion-box").html('');
                }
            }
        });
    }
    
});

$('#autocomplete-suggestion').click(function(event){
  alert("alert");
});

function getValue(name,sku,sku_price){
    $('#filter_name').val(name)
    $('#psku').val(sku)
    $('#sku_price').val(sku_price)
    $('#suggesstion-box').hide();
}
function submitform(){
    var discount_code      = $('#discount_code').val();
    var discount_quantity  = $('#discount_quantity').val();
    var discount_price     = $('#discount_price').val();
    var discount_fromdate  = $('#discount_fromdate').val();
    var discount_todate     = $('#discount_todate').val();
    var checked = []
    $("input[name='location[]']:checked").each(function ()
    {
        checked.push(parseInt($(this).val()));
    });
    if(discount_price!='' && discount_quantity!='' && discount_code!='' && discount_fromdate!='' && discount_todate!='' && checked.length !=0){
         characterReg = /[`~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            if (characterReg.test(discount_code)) {
               $('#discount_code').focus();
               alert('Cannot Allow the Special Character');
            }else{
               
                     $("#formID").submit();
            
            }
    }else{
        alert("Enter form Mandatory Details");
        return false;
    }

}
</script>