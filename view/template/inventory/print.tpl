<?php echo $header; ?>
<div class="clearfix"></div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <h3 class="page-title"></h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
              <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  } ?>                    
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <div style="clear:both"></div>            
      <div class="row">     
        <div class="col-md-12">      
          <div class="innerpage-listcontent-blocks"> 
            <table class="table orderlist statusstock">
              <tr>
                <td align="center" colspan="2"><h2><?php echo $companyInfo['name']; ?></h2></td>
              </tr>
              <tr>
                <td align="center" colspan="2">
                    <?php if ($companyInfo['address1']) { echo $companyInfo['address1']; } ?>
                    <?php if ($companyInfo['address2']) { echo ', '.$companyInfo['address2']; } ?>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="2"><?php if ($companyInfo['city']) { echo $companyInfo['city']; } ?></td>
              </tr>
              <tr>
                <td align="center" colspan="2"><?php echo $companyInfo['country']; ?></td>
              </tr>
                <?php if (!empty($companyInfo['phone'])) { $contact = ' class="print_contact"'; } else { $contact = ' class="print_empty_contact"'; } ?>
              <tr <?php echo $contact; ?>>
                <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> <?php echo $companyInfo['phone']; ?></span> <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> <?php echo $companyInfo['fax']; ?></span></td>
                <td align="center"><span class="print_date"><strong>Print Date:</strong> <?php echo $companyInfo['print_date']; ?></span></td>
              </tr>
            </table>
            <div>
              <h1><?php echo ucfirst($master_name); ?> List</h1>
            </div>
            <?php if (strtolower($master_name) == 'department') { ?>
              <table class="table orderlist statusstock">
                <thead>
                  <tr class="heading">
                    <td class="center">Department Name</td>
                    <td class="center">Remarks</td>
                    <td class="center">SalesMan Required</td>
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($departmentList)) { ?>
                   <?php $class = 'odd'; ?>
                  <?php foreach ($departmentList as $department) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                      <tr class="<?php echo $class; ?>">
                        <td class="center"><?php echo $department['department_name']; ?></td>
                        <td class="center"><?php echo $department['remarks']; ?></td>
                        <td class="center"><?php if($department['salesman_required']): ?> Yes <?php else: ?> No <?php endif; ?></td>
                      </tr>
                  <?php } ?>              
                  <?php } else { ?>
                  <tr>
                    <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                  </tr>
                  <?php } ?>              
                </tbody>
              </table>               
            <?php } elseif (strtolower($master_name) == 'category') { ?>
              <table class="table orderlist statusstock">
                <thead>
                  <tr class="heading">
                    <td class="center">Category Name</td>
                    <td class="center">Department</td>
                    <td class="center">Remarks</td>
                    <td class="center">Status</td>
                  </tr>
                </thead>
                <tbody>
                  <?php if (!empty($categoryList)) { ?>
                  <?php $class = 'odd'; ?>
                  <?php foreach ($categoryList as $category) { ?>
                  <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                      <tr class="<?php echo $class; ?>">
                        <td class="center"><?php echo $category['category_name']; ?></td>
                        <td class="center"><?php echo $category['department_name']; ?></td>
                        <td class="center"><?php echo $category['remarks']; ?></td>
                        <td class="center"><?php if($category['status']): ?> Yes <?php else: ?> No <?php endif; ?></td>
                      </tr>
                  <?php } ?>              
                  <?php } else { ?>
                  <tr>
                    <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                  </tr>
                  <?php } ?>              
                </tbody>
             </table>        
          <?php } elseif (strtolower($master_name) == 'brand') { ?>
            <table class="table orderlist statusstock">
              <thead>
                <tr class="heading">
                  <td class="center">Brand Name</td>
                  <td class="center">Category</td>
                  <td class="center">Remarks</td>
                  <td class="center">Status</td>
                </tr>
              </thead>
              <tbody>
                <?php if (!empty($brandList)) { ?>
                <?php $class = 'odd'; ?>
                <?php foreach ($brandList as $brand) { ?>
                <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">
                    <td class="center"><?php echo $brand['name']; ?></td>
                    <td class="center"><?php echo $brand['category']; ?></td>
                    <td class="center"><?php echo $brand['remarks']; ?></td>
                    <td class="center"><?php if($brand['status']): ?> Yes <?php else: ?> No <?php endif; ?></td>
                  </tr>
                <?php } ?>              
                <?php } else { ?>
                <tr>
                  <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>              
              </tbody>
            </table>
          <?php } ?>
          </div>          
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>