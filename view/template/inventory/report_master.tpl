<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                 <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printpage();"><i class="fa fa-print"></i> Print</a>
                 <a href="<?php echo $link_csvexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                  <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                    <i class="fa fa-file-pdf-o"></i> Export PDF</a>

                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>

      <div style="clear:both"></div>            
        <div class="row">     
          <div class="col-md-12">      
            <div class="innerpage-listcontent-blocks">
              <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              <div>
              <h1>SKU Master Report</h1>
              <p>

                <?php if($filter_department != '' || $filter_category != '' || $filter_brand != '' || $filter_vendor != '') { ?>
                  <span style="font-size: 15px;"> Filter By </span>
                <?php  }?>

                <?php if($filter_location != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Location : <span id="filter_locationid"></span> </span>
                <?php } ?>

                <?php if($filter_department != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Department : <span id="filter_departmentid"></span> </span>
                <?php } ?>
                <?php if($filter_category != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Category : <span id="filter_categoryid"></span> </span>
                <?php } ?>
                <?php if($filter_brand != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Brand : <span id="filter_brandid"></span> </span>
                <?php } ?>
                <?php if($filter_vendor != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Vendor : <span id="filter_vendorid"></span> </span>
                <?php } ?>
                <?php if($filter_name) { ?>
                  <span style="font-size: 15px;margin-right: 15px;">  SKU : <?php echo $filter_name;?> </span>
                  <?php } ?>
              </p>
            </div>
          </div>
            
            <div class="caption set-bg-color"> 
              <form method="post" name="report_filter" id="filter_validation">
                <input name="type" id="type" type="hidden">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                     <tr class="filter" style="border-top:none;">        
                     <td>
                        <select name="filter_location" id="filter_location" style="min-height: 35px; padding: 7px;">
                          <?php if(count($location)>=2){?>
                            <option value="">Select Location </option>
                          <?php }?>  
                            <?php foreach($location as $locations){ ?>
                              <option <?php if($filter_location == $locations['location_code'] ){ ?> selected <?php } ?> value="<?php echo $locations['location_code']; ?>"><?php echo $locations['location_name']; ?></option>
                            <?php } ?> 
                        </select>
                      </td>

                      <td> 
                       <select name="filter_department" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $filter_department) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                       
                      <td>
                        <select name="filter_category" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                            <option value="">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if (trim($category['category_code']) == $filter_category) { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                          <?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                    <?php } ?>   
                                <?php } ?>
                            <?php } ?>
                        </select> 
                      </td>
                      
                      <td>
                      <select name="filter_brand" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                              <option value="">Select Brand</option>
                              <?php if (!empty($brand_collection)) { ?>
                                  <?php foreach ($brand_collection as $brand) { ?>
                                      <?php if (trim($brand['brand_code']) == $filter_brand) { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                      <?php } else { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
                                      <?php } ?>   
                                  <?php } ?>
                              <?php } ?>
                          </select> 
                     </td>
                     <td>
                      <select name="filter_vendor" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                                <option value="">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if (trim($vendor['vendor_code']) == $filter_vendor) { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                        <td><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                            <input type="text" name="filter_name" id="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox ui-autocomplete-input" placeholder="Product Name or Barcode" autocomplete="off" onkeyup="getProductautoFill(this.value);">
                            <div id="suggesstion-box" class="auto-compltee"></div>
                        </td>
                         <td style="display: none;">
                          <input type="text" name="filter_barcodes" id="filter_barcodes" value="" class="textbox" placeholder="Scan Barcode">
                        </td> 
                      <td align="center">
                       <input type="hidden" name="filter_sku" id="filter_sku" value="<?php echo $_REQUEST['filter_sku'] ; ?>">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>

                      </tr>
                    </tbody>
                  </table>
              </form>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
        
            <div class="caption">
            <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                  <td class="center" style="width: 5%;">S.No</td>
                  <td class="center" style="width: 10%;">Product Code</td>
                  <td class="center" style="width: 10%;">Vendor</td>
                  <td class="center" style="width: 10%;">Department </td>
                  <td class="center" style="width: 10%;">Category </td>
                  <td class="center" style="width: 10%;">Sub Category </td>
                  <td class="center" style="width: 10%;">Brand </td>
                  <td class="center" style="width: 20%;">Description</td>
                  <td class="center" style="width: 10%;">Cost</td>
                  <td class="center" style="width: 10%;">Cost With GST </td>
                  <td class="center" style="width: 10%;">Selling Price</td>
                   
                </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $product['pagecnt']+$i; ?></td>
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['vendor_name']; ?></td>
                    <td class="center"><?php echo $product['department_name']; ?></td>
                    <td class="center"><?php echo $product['category_name']; ?></td>
                    <td class="center"><?php echo $product['subCategoryName']; ?></td>
                    <td class="center"><?php echo $product['sku_brand_code']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="right"><?php echo number_format($product['price'],2); ?></td>
                    <td class="right"><?php echo number_format($product['cost_value'],2); ?></td>
                    <td class="right"><?php echo number_format($product['selling_price'],2); ?></td>
                    
                  </tr>
              <?php $i++; }  ?>  
                
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="9"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table> 
         </div>          
        <div class="pagination"><?php echo $pagination; ?></div>
      </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php echo $footer; ?>   
<script type="text/javascript"><!--
function filterReport() {
  url = 'index.php?route=inventory/reports/masterReport&token=<?php echo $token; ?>';
 
  var filter_location = $('select[name=\'filter_location\']').attr('value');  
  if (filter_location != '*') {
    url += '&filter_location=' + encodeURIComponent(filter_location);
  }

  var filter_department = $('select[name=\'filter_department\']').attr('value');  
  if (filter_department != '*') {
    url += '&filter_department=' + encodeURIComponent(filter_department);
  }
  
  var filter_category = $('select[name=\'filter_category\']').attr('value');  
  if (filter_category != '*') {
    url += '&filter_category=' + encodeURIComponent(filter_category);
  }
  
  var filter_vendor = $('select[name=\'filter_vendor\']').attr('value');  
  if (filter_vendor != '*') {
    url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
  }
  
  var filter_brand = $('select[name=\'filter_brand\']').attr('value');  
  if (filter_brand != '*') {
    url += '&filter_brand=' + encodeURIComponent(filter_brand);
  } 
  
  var filter_sku = $('input[name=\'filter_sku\']').attr('value');  
  if (filter_sku != '*') {
    url += '&filter_sku=' + encodeURIComponent(filter_sku);
  }
  var filter_name = $('input[name=\'filter_name\']').attr('value');  
  if (filter_name != '*') {
    url += '&filter_name=' + encodeURIComponent(filter_name);
  } 
  location = url; 
}

function printpage(){
    $(".set-bg-color").hide();
    $(".pagination").hide();
    window.print();
    $(".set-bg-color").show();
    $(".pagination").show();
}

$(document).ready(function(){
  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_departmentid" ).html(filter_department);

  var filter_location = $('[name="filter_location"] option:selected').text();
  $( "#filter_locationid" ).html(filter_location);

  var filter_category = $('[name="filter_category"] option:selected').text();
  $( "#filter_categoryid" ).html(filter_category);

  var filter_brand = $('[name="filter_brand"] option:selected').text();
  $( "#filter_brandid" ).html(filter_brand);

  var filter_vendor = $('[name="filter_vendor"] option:selected').text();
  $( "#filter_vendorid" ).html(filter_vendor);

   var filter_name = $("#filter_name").val();
  $( "#filter_product_id" ).html(filter_name);
}); 
  
function getProductautoFill(sku) {
 
  var filter_name = $("#filter_name").val(); 
   if(filter_name ==''){
    $('#filter_sku').val('');
   }

   if(sku.length < SEARCH_CHAR_CNT){
      return false;
   } 

    var location = $("#filter_location").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location; 
    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
          //alert(result);
            if (result=='') { 
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
            $("#pre-loader-icon").hide();
        }
    });
}

function selectedProduct(val,cnt) {
    var newVal = val.split("||");
   //console.log(newVal);
    $('#filter_name').val(newVal[2].replace("^","'").replace('!!','"'));
    $('#filter_sku').val(newVal[1].replace("^","'").replace('!!','"'));
    $('#suggesstion-box').hide();
    if(cnt=='1'){
        $( "#filter_validation").submit();
    } 
}

function getValue(name,sku){
  console.log(sku); 
  $('#filter_name').val(name);
  $('#filter_sku').val(sku);
  $('#suggesstion-box').hide();
}
</script>