<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title">Reports</h3>
      <div style="clear:both"></div>
    <div class="row">     
      <div class="col-md-6">
          <div class="innerpage-listcontent-blocks">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">               
        <table class="table orderlist statusstock">
            <thead>
              <tr>
                <td class="center" colspan="2"><h3><strong><?php echo $text_invet_masters; ?></strong></h3></td>
              </tr>
            </thead>
            <tbody>
                <tr class="master_name_list">
                    <td class="right"><input type="radio" name="master_name" id="master_name_department" value="department" checked="checked" /></td>
                    <td class="left"><label for="master_name_department"><?php echo $text_department; ?></label></td>
                </tr>
                <tr class="master_name_list">
                    <td class="right"><input type="radio" name="master_name" id="master_name_category" value="category" /></td>
                    <td class="left"><label for="master_name_category"><?php echo $text_category; ?></label></td>
                </tr>
                <tr class="master_name_list">
                    <td class="right"><input type="radio" name="master_name" id="master_name_brand" value="brand" /></td>
                    <td class="left"><label for="master_name_brand"><?php echo $text_brand; ?></label></td>
                </tr>
                <tr class="master_print_button">
                    <td class="center" colspan="2"><button type="submit" value="masters" class="btn btn-zone"><?php echo $button_print; ?></button></td>
                </tr>
            </tbody>
        </table>
      </div>
   </form>
     <div class="innerpage-listcontent-blocks">
   <form action="<?php echo $stock_action; ?>" method="post" id="form">
        <table class="table orderlist statusstock">
          <thead>
            <tr>
              <td class="center" colspan="2"><h3><strong><?php echo $text_stock_reports; ?></strong></h3></td>
            </tr>
          </thead>
          <tbody>            
            <tr class="master_name_list">
              <td class="right"><input type="radio" name="stock_report" id="stock_report" value="barcode" checked="checked" /></td>
              <td class="left"><label for="master_name_department"><?php echo $text_stockreports; ?></label></td>
            </tr>
            <tr class="master_name_list">
              <td class="right"><input type="radio" name="stock_report" id="stock_report" value="barcode_list" /></td>
              <td class="left"><label for="master_name_department"><?php echo $text_barcode_list; ?></label></td>
            </tr>            
            <tr class="master_name_list">
              <td class="right"><input type="radio" name="stock_report" id="stock_report" value="stockvalues" /></td>
              <td class="left"><label for="master_name_department"><?php echo $text_stockreports_valuatioin; ?></label></td>
            </tr>
            <tr class="master_print_button">
              <td class="center" colspan="2"><button type="submit" value="stock" class="btn btn-zone"><?php echo $button_print; ?></button></td>
            </tr>
          </tbody>
        </table>
      </form>
    </div>
  </div>
  </div>
</div>
</div>
</div>
<?php echo $footer; ?>