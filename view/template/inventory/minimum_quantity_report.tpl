<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd;margin-bottom: 25px !important;">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="window.print();"><i class="fa fa-print"></i>&nbsp;Print</a>

              <a href="<?php echo $export_csv; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i>&nbsp;Export CSV</a>

              <a href="<?php echo $export_pdf; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i>&nbsp;Export PDF</a>

              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i>&nbsp;Back</a>
            </div>    
          </div>
        </div>                          
      </div>
      <div style="clear:both"></div>
      <div class="row">
        <div class="col-md-12">
          <div class="innerpage-listcontent-blocks"> 
           <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
          </div>
          
          <h3 class="page-title">Minimum Quantity Report</h3>     
         <div>
              <p>
                <?php if($filter_location!='' || $filter_brand!='' ||  $filter_vendor!='' ||  $filter_category!=''  || $filter_department!='' || $filter_date_from!='' || $filter_date_to!='' || $filter_product!=''){ ?>
                    <span style="font-size: 15px;"> Filter By </span>
                 <?php } ?>
                 <?php if($filter_location != ''){ ?>
                    <span style="font-size: 15px;margin-right: 15px;"> Location : <span id="filter_location_codeid"><?php echo $filter_location;?></span> </span>
                <?php } ?>
                
                <?php if($filter_department != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Department : <span id="filter_departmentid"></span> </span>
                <?php } ?>
                <?php if($filter_category != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Category : <span id="filter_categoryid"></span> </span>
                <?php } ?>
                <?php if($filter_brand != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Brand : <span id="filter_brandid"></span> </span>
                <?php } ?>
                <?php if($filter_vendor != ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Vendor : <span id="filter_vendorid"></span> </span>
                <?php } ?>
                 <?php if($filter_product!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  SKU : <span id="stock_date"><?php echo $filter_product;?></span> </span>
                <?php } ?>


              </p>
          </div>
        </div>
      </div>

      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px -10px -15px -10px; padding:16px 0 0 0;">
            <div class="caption" id="reportsuggestion"> 
                <form method="post">
                <input type="hidden" name="type" id="type">                 
                  <table class="table orderlist statusstock">
                    <tbody>
                    <tr class="filter">
                       <td>
                          <select name="filter_location" id="filter_location" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="">-- Select Location --</option>
                            <?php if(!empty($locations)){
                                foreach($locations as $value){ ?>
                                  <option value="<?php echo $value['location_code'];?>" <?php if($value['location_code'] == $data['filter_location']) { echo "selected"; } ?>><?php echo $value['location_name']; ?></option>
                            <?php  } } ?>
                          </select>
                      </td>
                      <td>
                         <select name="filter_department" id="filter_department" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                          <option value="">-- Select Department --</option>
                          <?php
                             if(!empty($departments)){
                                foreach($departments as $value){ ?>
                                  <option value="<?php echo $value['department_code'];?>" <?php if($value['department_code'] == $data['filter_department']){ echo "selected"; } ?>> <?php echo $value['department_name']; ?></option>
                          <?php  } } ?> 
                          </select>
                      </td>
                       <td>
                        <select name="filter_category" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                            <option value="">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if (trim($category['category_code']) == $data['filter_category']) { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>" selected="selected">
                                          <?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo trim($category['category_code']); ?>"><?php echo $category['category_name']; ?></option>  
                                    <?php } ?>   
                                <?php } ?>
                            <?php } ?>
                        </select> 
                      </td>
                      
                      <td>
                      <select name="filter_brand" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                              <option value="">Select Brand</option>
                              <?php if (!empty($brand_collection)) { ?>
                                  <?php foreach ($brand_collection as $brand) { ?>
                                      <?php if (trim($brand['brand_code']) == $data['filter_brand']) { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                      <?php } else { ?>
                                          <option value="<?php echo trim($brand['brand_code']); ?>"><?php echo $brand['brand_name']; ?></option>  
                                      <?php } ?>   
                                  <?php } ?>
                              <?php } ?>
                          </select> 
                     </td>
                     <td>
                      <select name="filter_vendor" class="textbox" style="width: 20%; min-height: 35px; padding: 7px;">
                                <option value="">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if (trim($vendor['vendor_code']) == $data['filter_vendor']) { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_code']; ?>"><?php echo $vendor['vendor_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                     </td>
                      <td>
                          <input type="text" name="filter_name" id="filter_name" class="textbox ui-autocomplete-input" placeholder="Search Product" autocomplete="off" onkeyup="getProductautoFill(this.value);" value="<?php echo $data['filter_name']; ?>">
                          <div id="suggesstion-box" class="auto-compltee"></div>
                      </td>
                      <td align="center" colspan="4">
                          <input type="hidden" name="filter_product_id" id="filter_product_id" value="<?php echo $data['filter_product_id']; ?>">
                          <input type="hidden" name="page" value="<?php echo $data['page']; ?>">
                          <button style="width: 100%; min-height: 36px; border-radius:0 !important;" class="btn btn-zone btn-primary">&nbsp;<i class="fa fa-search"></i>&nbsp;Search</button>
                      </td>
                    </tr>
                    </tbody>
                  </table>
                </div>                        
              </div>     
            </form>
          <div style="clear:both; margin: 0 0 15px 0;"></div>
          </div>
        </div>

            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                  <table class="table orderlist statusstock">
                    <thead>
                        <tr class="heading">
                          <td class="center">No</td>
                         <!--  <td class="center">Location</td> -->
                          <td class="center">Product ID </td>
                          <td class="center">Sku </td>
                          <td class="center">Name</td>
                          <td class="center">Actual Stock</td>
                          <td class="center">Reserved Stock</td>
                          <td class="center">Available Stock</td>
                          <td class="center">Minimum Stock</td>
                         <!--  <td class="center">Sku Description </td>
                          <td class="center">Department</td> -->
                        </tr>
                    </thead>
                    <tbody>           
                      <?php if (!empty($products)) { $i = 1; 
                      $class = 'odd'; 
                      foreach ($products as $product) {
                      $class = ($class == 'even' ? 'odd' : 'even'); ?>
                          <tr class="<?php echo $class; ?>">
                            <td class="center"><?php echo $i; ?></td>
                           <!--  <td class="center"><?php echo $product['location_Code']; ?></td> -->
                            <td class="center"><?php echo $product['product_id']; ?></td>
                            <td class="center"><?php echo $product['sku']; ?></td>
                            <td class="center"><?php echo $product['name']; ?></td>
                            <!-- <td class="center"><?php echo $product['sku_shortdescription']; ?></td> -->
                            <!-- <td class="center"><?php echo $product['department_name']; ?></td> -->
                            <td class="center"><?php echo $product['sku_qty']; ?></td>
                            <td class="center"><?php echo $product['reservedQty']; ?></td>
                            <td class="center"><?php echo $product['availableQty']; ?></td>
                            <td class="center"><?php echo $product['sku_min']; ?></td>
                          </tr>
                        <?php $i++; }  ?> 
                        <?php } else { ?>
                        <tr>
                            <td align="center" colspan="15">No Result!</td>
                        </tr>
                        <?php } ?>              
                      </tbody>
                  </table>
                </div>
                <div class="pagination"><?php echo $pagination; ?></div>    
              </div>
           </div>
          </div>
        </div>
      </div>
    </div>
</div>

<?php echo $footer; ?>
<script type="text/javascript">

  function getProductautoFill(sku) {
    var filter_name = $("#filter_name").val();
    if(filter_name ==''){
        $('#filter_product_id').val('');
    }
    if(sku.length < SEARCH_CHAR_CNT){
        $('#filter_product_id').val('');
        return false;
    }
    var location = $("#filter_location").val();
    var ajaxData = 'sku='+encodeURIComponent(sku)+'&location_code='+location;

    $.ajax({
        type: "POST",
        url: 'index.php?route=transaction/purchase/getProductDetailsbyScan&token=<?php echo $token; ?>',
        data: ajaxData,
        success: function(result) {
            if (result=='') {
              //
            } else {
                if (result.indexOf("li")>= 0){
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                }else{
                    $('#suggesstion-box').hide();
                    selectedProduct(result,'1');
                }
                return false;
            }
        }
    });
  }

function selectedProduct(val,cnt) {
    var newVal = val.split("||");
    console.log(newVal);
    $('#filter_name').val(newVal[2].replace("^","'").replace('!!','"'));
    $('#filter_product_id').val(newVal[0]);
    $('#suggesstion-box').hide();
}
</script>
