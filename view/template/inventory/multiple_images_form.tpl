<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $update_image; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">Multiple Images</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo $home; ?>">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>                        
                        <a href="<?php echo $inven_comm; ?>">Inventory</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Multiple Images</a>                      
                    </li>                   
                </ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> Save</button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Cancel</button></a></div>    
                    </div>
                </div>                            
            </div>  
            <div style="clear:both"></div>
            <div class="row">          
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                    <label for="title">Product SKU</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="sku" id="sku" value="<?php echo $product_info['sku']; ?>" class="textbox" readonly>             
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                <label for="description">Description</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="text" name="description" id="description" value="<?php echo $product_info['sku_description']; ?>" class="textbox" readonly>
                                </td>                                
                            </tr>                                                  
                        </tbody>
                    </table>                   
                    <table id="multiple_image" class="table orderlist statusstock">
                        <thead>
                           <tr class="heading">
							<td class="left">Title</td>
							<td class="text-center">Images</td>
							<td class="left">Priority</td>
							<td></td>
						</tr>
                        </thead>
                       <?php $multiple_image_row = 0; ?>
					<?php foreach ($multiple_images as $multiple_image) { ?>
                    <?php $imgurl = HTTP_SERVER.'uploads/'.$multiple_image['image']; ?>
					<tbody id="multiple-image-row<?php echo $multiple_image_row; ?>">
						<tr>
							<td class="left"><input type="text" name="multiple_image[<?php echo $multiple_image_row; ?>][id]"  value="<?php echo $multiple_image['id']; ?>" hidden><input type="text" name="multiple_image[<?php echo $multiple_image_row; ?>][title]" value="<?php echo $multiple_image['title']; ?>" size="1" class="textbox" /></td>
							<td class="text-center"><img src="<?php echo $imgurl; ?>" class="imgDisplay" width="100px"></td>
							<td class="left"><input type="text" name="multiple_image[<?php echo $multiple_image_row; ?>][priority]" value="<?php echo $multiple_image['sort_order']; ?>" size="1" class="textbox" /></td>
							<td class="left"><a image_name="<?php echo $multiple_image['image']; ?>" image_id="<?php echo $multiple_image['id']; ?>" remove_row="multiple-image-row<?php echo $multiple_image_row; ?>" class="btn btn-zone btn-danger remove_field"><i class="fa fa-times"></i> Remove</a></td>
						</tr>
					</tbody>
					<?php $multiple_image_row++; ?>
					<?php } ?>
					<tfoot>
						<tr>
							<td colspan="3"></td>
							<td class="left"><a onclick="addImage();" class="btn btn-zone btn-primary"><i class="fa fa-plus"></i> Add Image</a></td>
						</tr>
					</tfoot>
                    </table>                    
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<script type="text/javascript">
var multiple_image_row = <?php echo $multiple_image_row; ?>;

function addImage() {
	html  = '<tbody id="multiple-image-row' + multiple_image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="multiple_image[' + multiple_image_row + '][title]" value="" size="1" class="textbox" /></td>';
	html += '    <td class="left"><input type="file" name="multiple_image[' + multiple_image_row + ']" value="" size="1" class="textbox" /></td>';
	html += '    <td class="left"><input type="text" name="multiple_image[' + multiple_image_row + '][priority]" value="" size="1" class="textbox" /></td>';
	html += '    <td class="left"><a onclick="$(\'#multiple-image-row' + multiple_image_row + '\').remove();" class="btn btn-zone btn-danger"><i class="fa fa-times"></i> Remove</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#multiple_image > tfoot').before(html);
	
	multiple_image_row++;
}
$('.remove_field').on('click',function(){
    var image_id = $(this).attr('image_id');
    var image_name = $(this).attr('image_name');
    var remove_row = $(this).attr('remove_row');
    $.ajax({
        url:"index.php?route=inventory/inventory/removeMultipleImage&token=<?php echo $token; ?>",
        type:"POST",
        data:{
            image_id : image_id,
            image_name : image_name
        },
        success:function(out){
            $('#'+remove_row).remove();
            console.log(out);
        }
    });
});
</script>
<?php echo $footer; ?>