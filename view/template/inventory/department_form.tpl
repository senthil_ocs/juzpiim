<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
	<div class="page-content-wrapper">    	
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <button class="btn btn-update btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" type="submit" title="Save"><i class="fa fa-save"></i><span>  <?php echo $button_save; ?></span></button>
                            <?php } ?>
                            <a href="<?php echo $cancel; ?>"><button class="btn btn-reset btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px" type="button" title="Back" onclick="backAction('<?php echo $cancel; ?>')"><i class="fa fa-times"></i><span>  <?php echo $button_cancel; ?></span></button></a>
                       	</div>    
                    </div>
                </div>
			</div>            
            <div class="row">
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>

                    <?php if ($error_warning) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error_warning; ?>
                        </div>
                    <?php } ?>

                 </div>
                 <div style="clear:both"></div>
                <div class="col-md-12">                	
                	<table class="table orderlist statusstock country-lisblk invetorty-tab1">
                    	<tbody>
                        	<tr>
                                <td width="35%">
                                <?php echo $entry_code; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                    <input type="text" placeholder="Enter Department Code" name="code" id="code" class="textbox" value="<?php echo $code; ?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">
                                <?php echo $entry_name; ?>&nbsp;<span class="required">*</span>
                                </td>
                                <td width="65" class="order-nopadding">
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
                                    	<input type="text" placeholder="Enter name" name="name" id="name" class="textbox requiredborder" value="<?php echo $name; ?>" />
                                    </div>
                                    <?php } ?>
                                    <?php if(!$error_name) { ?>
                               			<input type="text" placeholder="Enter name" name="name" id="name" class="textbox" value="<?php echo $name; ?>" />
                               		<?php } ?>
                                </td>
                            </tr>

                            
                            <tr>
                            	<td width="35%"><?php echo $entry_remarks; ?>
                                </td>
                                <td width="65%" class="order-nopadding">
                                    <textarea name="remarks" class="textarea" placeholder="Enter your remarks"><?php echo $remarks; ?></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td width="35%">Status
                                </td>
                                <td width="65%">
                                 <select name="status">
                                    <option value="1" <?php if($status=='1'){?> selected="selected" <?php }?>>Active</option>
                                    <option value="0" <?php if($status=='0'){?> selected="selected" <?php }?>>In Active</option>
                                 </select>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>                
            </div>
		</div>
	</div>
    </form>
</div>
<?php echo $footer; ?>