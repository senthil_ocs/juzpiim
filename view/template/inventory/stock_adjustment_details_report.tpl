<!-- NEW CODE -->
<?php echo $header; ?>
<script src="<?php DIR_SERVER;?>view/javascript/jquery-min.js"></script>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <!-- <h3 class="page-title">Sales Detail Report</h3>      -->
      <div class="page-bar">
        <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li>
              <?php echo $breadcrumb['separator']; ?>
                <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            </li>
          <?php  }?>
        </ul>
        <div class="page-toolbar">
          <div class="btn-group pull-right">
            <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
              <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onClick="printFunction();"><i class="fa fa-print"></i> Print</a>
              <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
              <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-file-pdf-o"></i></i> Export PDF</a>
              <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
            </div>    
          </div>
        </div>                          
      </div>

        <!-- company details /header  -->
       <div style="clear:both"></div>            
        <div class="row">     
           <div class="col-md-12">      
              <div class="innerpage-listcontent-blocks"> 
                 <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
              </div>
              <h3 class="page-title">Stock Adjustment Details Report</h3>       
           </div>
        </div>
      <!-- --> 
      <!-- filter display -->
         <div>
              <p>
                <?php if($data['stock_date_from']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Filter Date From  - <span id="stock_date_from_id"><?php echo $data['stock_date_from'];?></span> </span>
                <?php } ?>
                <?php if($data['stock_date_to']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;"> Date To - <span id="stock_date_to_id"></span><?php echo $data['stock_date_to'];?> </span>
                <?php } ?>
                <?php if($data['filter_location_code']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Location - <span id="filter_transactionno_id"></span><?php echo $data['filter_location_code'];?> </span>
                <?php } ?>
                <?php if($data['filter_transactionNo']!= ''){ ?>
                  <span style="font-size: 15px;margin-right: 15px;">  Transaction No - <?php echo $data['filter_transactionNo'];?> </span>
                <?php } ?>

              </p>
        </div>
      <!-- filter display end -->
      <!-- NEW SEARCH  -->
      <div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
            <div class="caption"> 
              <form method="post" name="report_filter">
                <input type="hidden" name="type" id="type">                 
                   <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                      <tr class="filter">                     
                        <td> <input type="text" id='datetimepicker' placeholder='From Date' name="filter_date_from" value="<?php echo $data['stock_date_from'] ; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                        <td>
                          <input type="text" placeholder='To Date' name="filter_date_to" value="<?php echo $data['stock_date_to']; ?>" class="textbox date" autocomplete="off">
                        </td>                       
                          
                        <td>
                          <select name="filter_location" id="filter_location" style="min-height: 35px; padding: 7px; width: 100%;">
                            <?php if(count($Tolocations)>=2){?>
                            <option value="">-- Select Location --</option>
                            <?php }?>
                            <?php
                               if(!empty($Tolocations)){
                                    foreach($Tolocations as $value){ ?>
                                    <option value="<?php echo $value['location_code'];?>" <?php if($data['filter_location_code'] == $value['location_code'])echo 'selected'; ?> > <?php echo $value['location_name']; ?></option>

                            <?php  } } ?> 
                          </select>
                          </td>
                          <td>
                          <input type="text" placeholder='Transaction No' name="filter_transactionNo" value="<?php echo $data['filter_transactionNo']; ?>" class="textbox" autocomplete="off">
                          </td> 

                          <td>
                     
                        </td>

                          <td align="center" colspan="4">
                            <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('purchase_summary');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                      </tr>
                    </tbody>
                  </table>
            </div>                        
           
            </div>     
            
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!--  -->
            <div style="clear:both; margin: 0 0 15px 0;">
            </div>
            <div class="row">       
              <div class="col-md-12">       
                <div class="innerpage-listcontent-blocks"> 
                   <?php if ($stockAdjustmentDetails) { ?>
                      <?php foreach ($stockAdjustmentDetails as $key => $stkDetail) { 
                         // $type = $stockAdjustmentDetails['type'];
                        // printArray($stkDetail); die;
                        ?>
                    <table class="table orderlist statusstock">
                    <tr <?php echo $contact; ?>>
                       <td align="left" style="border-right: none !important;"><span class="print_phone"><strong>Transaction No:</strong> 0000<?php echo $stkDetail['Location_Code']; ?></span> </td>
                      <td align="right"><span class="print_date"><strong>Date:</strong> <?php echo $stkDetail['Stock_Date']; ?></span></td>
                    </tr>
                    </table>
                        <table class="table orderlist statusstock">
                          <thead>
                            <tr class="heading">

                                <td class="center">SKU</td>
                                <td class="center">Description</td>
                                <td class="center">Qty</td>
                                <td class="center">Price</td>
                                <td class="center">Sub Total</td>
                                <td class="center">GST</td>
                                <td class="center">Net Total</td>
                            </tr>
                          </thead>
                          <tbody>

                          <?php $class = 'odd'; ?>
                          <?php
                            $qty=$sku_price=$tot_subtotal=$tot_discount=$tot_gst=$tot_nettotal=0; 
                            foreach ($stkDetail['details'] as $value) { $i++; ?>
                          <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                          <?php 
                          
                          $qty+=$value['qty'];
                          $sku_price+=$value['qty'];
                          $tot_subtotal+=$value['unit_cost'];
                          $tot_discount+=$value['total_value'];
                          $tot_gst+=$value['gst'];
                          $tot_nettotal+=$value['net_value'];
                          ?>
                        <tr class="<?php echo $class; ?>">
                          <td class="center"><?php echo $value['sku']; ?></td>
                          <td class="center"><?php echo $value['sku_description']; ?></td>
                          <td class="right"><?php if($stkDetail['type']=='1'){?> - <?php }?>
                          <?php echo $value['qty']; ?></td>
                          <td class="right"><?php echo $value['unit_cost']; ?></td>
                          <td class="right"><?php echo $value['total_value']; ?></td>
                          <td class="right"><?php echo $value['gst']; ?></td>
                          <td class="right"><?php echo $value['net_value']; ?></td>
                        </tr>
                        <?php } ?>
                        <tr><td colspan="2" class="text-right"><b>Total</b></td>
                          <td class="text-right"><?php if($stkDetail['type']=='1'){?> - <?php }?>
                                                <?php echo $sku_price; ?></td>
                          <td class="text-right"><?php echo number_format($tot_subtotal,2); ?></td>
                          <td class="text-right"><?php echo number_format($tot_discount,2); ?></td>
                          <td class="text-right"><?php echo number_format($tot_gst,2); ?></td>
                          <td class="text-right"><?php echo number_format($tot_nettotal,2); ?></td>
                        </tr>
                      <?php }?>

                      <?php } else { ?>
                      <table class="table orderlist statusstock">
                        <tr>
                          <td align="center" colspan="10"><?php echo 'No Results'; ?></td>
                        </tr>
                      </table>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>          
                <!-- <div class="pagination"><?php echo $pagination; ?></div> -->
              </div>
           </div>
          </div>
          </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
/*$(document).ready(function() {
  $('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
*/
function filterReport(report_type) {
    url = 'index.php?route=inventory/reports/stockAdjestmentDetailsReport&token=<?php echo $token; ?>&stock_report=stock';
  
  
  var location_code   = $('#filter_location').val();  
  var stock_date_from = $('input[name=\'filter_date_from\']').attr('value');  
  var stock_date_to   = $('input[name=\'filter_date_to\']').attr('value');
  var filter_transactionNo = $('input[name=\'filter_transactionNo\']').attr('value');  

  if (location_code != '') {
    url += '&filter_location_code=' + encodeURIComponent(location_code);
  }
  if (stock_date_from != '' && stock_date_to !='' ) {
    url += '&filter_date_from=' + encodeURIComponent(stock_date_from);
    url += '&filter_date_to=' + encodeURIComponent(stock_date_to);
  }
  if (filter_transactionNo != '') {
    url += '&filter_transactionNo=' + encodeURIComponent(filter_transactionNo);
  }
  location = url;

  // document.report_filter.submit();
}
function printFunction(){
  $(".pagination").hide();
  window.print();
  $(".pagination").show();
}
//--></script> 
<?php echo $footer; ?>