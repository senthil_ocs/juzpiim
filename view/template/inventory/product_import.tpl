<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
        <div class="page-content" >
            <h3 class="page-title">Product Import</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                            <?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                </ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($action !=''){  ?>
                                <a href="<?php echo $action; ?>"><button type="submit" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> Submit</button></a>
                            <?php } if(!empty($products)){ ?>
                                <a href="<?php echo $deleteAllBtn; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash"></i> Delete</button></a>
                            <?php } ?>
                            <a href="<?php echo $back; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</button></a>
                        </div>    
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>
                     <?php if ($error) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error; ?>
                        </div>
                    <?php } ?>
                </div>
                
                <div style="clear:both"></div>
                <form method="post" action="<?php echo $fileUploadAction; ?>" enctype="multipart/form-data">
                    <div class="col-md-12">
                        <table class="table orderlist">                       
                            <tbody>
                                <tr data-toggle="tooltip" title=".csv file only support">
                                    <td>
                                        <a href="<?php echo HTTPS_SERVER.'doc/product_import/productsSample.csv'; ?>" download class="btn btn-alert"><i class="fa fa-download"></i> Download Sample</a>
                                        
                                    </td>
                                    <td>
                                        <input type="file" name="file">
                                        <span style="font-size: 10px;"> .csv file only support</span> 
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary" <?php if(!empty($products)) { echo "disabled"; }?>>Import</button>  
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>

            <div class="row">
            <div class="col-md-12">
                <div class="innerpage-listcontent-blocks">
                    <table class="table orderlist statusstock">
                        <thead>
                            <tr class="heading">
                                <td>Sno</td>
                                <td>SKU</td>
                                <td>Name</td>
                                <td>Description</td>
                                <td>Department</td>
                                <td>Category</td>
                                <td>Sub Category</td>
                                <td>Brand</td>
                                <td>Vendor</td>
                                <td>Price</td>
                                <td>Cost</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($products)) { $m = 1;
                            foreach ($products as $product) { ?>
                            <tr>
                                <td><?php echo $m; ?></td>
                                <td><?php echo $product['sku']; ?></td>
                                <td><?php echo $product['name']; ?></td>
                                <td><?php echo $product['description']; ?></td>
                                <td><?php echo $product['department']; ?></td>
                                <td><?php echo $product['category']; ?></td>
                                <td><?php echo $product['subcategory']; ?></td>
                                <td><?php echo $product['brand']; ?></td>
                                <td><?php echo $product['vendor']; ?></td>
                                <td><?php echo $product['price']; ?></td>
                                <td><?php echo $product['cost']; ?></td>
                                <td>
                                    <a href="<?php echo $product['deleteBtn']; ?>"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;color: red;"></i></a>
                                </td>
                            </tr>
                            <?php $m++; } }else{ ?>
                            <tr>
                                <td colspan="12" align="center">Please import file!</td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="ajax-modal-qedit" class="modal fade modal-scroll in" tabindex="-1" style="font-family: unset;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"> Match Product for : <span id="match_product_sku"></span></h4>
        </div>
        <div class="modal-body">       
            <div class="form-body">
                <div id="qedit_error" class=""></div>
                <div class="form-group">
                    <div class="row">     
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="inputsm">Search Product</label>
                          <input type="text" class="form-control" id="match_sku" name="match_sku" onkeyup="getProductautoFill(this.value);" autocomplete="off">
                          <input type="hidden" id="match_product_id">
                          <div id="suggesstion-box"></div>
                        </div>
                        <div class="form-group">
                          <label for="inputsm">Price</label>
                          <input type="text" class="form-control" name="match_price" id="match_price" readonly>
                          <input type="hidden" id="match_id">
                        </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update_match_product">Update</button>
          <button type="button" class="btn btn-danger"  id="close_model" data-dismiss="modal">Cancel</button>
        </div>
    </div>

<?php echo $footer; ?>
<script type="text/javascript">
    function getProductautoFill(sku) {
        if(sku.length < SEARCH_CHAR_CNT){
            return false;
        }
        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/lazada_order_import/getProductDetails&token=<?php echo $token; ?>',
            data: {filter_name : sku},
            success: function(result) {
                if (result=='') {
                    //
                } else {
                    $("#suggesstion-box").show();
                    $("#suggesstion-box").html(result);
                    return false;
                }
            }
        });
    }

    function selectedProduct(sku,id) {
        $("#match_sku").val(sku);
        $("#match_product_id").val(id)
        $("#suggesstion-box").hide();
    }

    function openMatchPopup(sku,price,id){
        $('#match_product_sku').html(sku);
        $("#match_sku").val('');
        $("#match_product_id").val('');
        $("#match_price").val(price);
        $("#match_id").val(id);
        $('#openModel').trigger('click',true);
        $("#match_sku").focus();
    }

    $(document).on('click','#update_match_product',function(){
        if($("#match_product_id").val() ==''){
            alert('Please select product');
            return false;
        }
        var id = $("#match_id").val();

        $.ajax({
            type: "POST",
            url: 'index.php?route=transaction/qsm_order_import/updateQSMProduct&token=<?php echo $token; ?>',
            data: {sku : $('#match_product_sku').html(), product_id : $("#match_product_id").val()},
            success: function(res) {
                if(res){
                    location.reload();
                }
            }
        });
    });
</script>