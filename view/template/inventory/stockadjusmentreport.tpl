<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
  <div class="page-content-wrapper">
    <div class="page-content">
    <h3 class="page-title"></h3>      
      <div class="page-bar">
     <ul class="page-breadcrumb">
          <?php foreach ($breadcrumbs as $breadcrumb) { ?>
              <li>
                <?php echo $breadcrumb['separator']; ?>
                  <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
              </li>
          <?php  }?>
        </ul>
         <div class="page-toolbar">
            <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <a class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px" onclick="printFunction();">
                    <i class="fa fa-print"></i> Print</a>
                <a href="<?php echo $exportAction; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-table"></i> Export CSV</a>
                <a href="<?php echo $link_pdfexport; ?>" class="btn btn-zone btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px">
                <i class="fa fa-file-pdf-o"></i> Export PDF</a>
                 <a href="<?php echo $back; ?>" class="btn btn-zone btn-danger "  style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> Back</a>
              </div>    
            </div>
        </div>                          
      </div>
      <div style="clear:both"></div>            
      <div class="row">     
      <div class="col-md-12">      
      <div class="innerpage-listcontent-blocks">
        <?php include(DIR_SERVER.'view/template/common/report_header.tpl');?>
       <div>
        <?php if (strtolower($stock_report) == 'barcode') { ?>
              <h1><?php echo ucfirst('Stock List'); ?></h1>
              <?php } elseif (strtolower($stock_report) == 'stock') { ?>
              <h1><?php echo ucfirst('Stock Adjustment Report'); ?></h1>
              <?php } ?>
              <p>
                
                <?php if($filter_department != '' || $stock_date != '' || $location_code!=''){ ?>
                <span style="font-size: 15px;"> Filter By </span>
                 <?php } ?>
                 <?php if($location_code != ''){ ?>
                    <span style="font-size: 15px;margin-right: 15px;"> Stock date from - <?php echo $stock_date_from; ?> </span>

                    <span style="font-size: 15px;margin-right: 15px;"> Stock date to - <?php echo $stock_date_to; ?> </span>

                    <span style="font-size: 15px;margin-right: 15px;"> Location - <span id="filter_location_codeid"></span> </span>
                <?php } ?>

              </p>

          </div>
          <div class="caption set-bg-color"> 
              <form method="post" name="report_filter">
                <input name="type" id="type" type="hidden">                 
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                    <tbody>
                     <tr class="filter"> 
                      
                        <td> 
                          <input type="text" id='datetimepicker' name="stock_date_from" value="<?php echo $stock_date_from ; ?>" 
                              class="textbox date" autocomplete="off" readonly>
                        </td>

                        <td> 
                          <input type="text" id='datetimepicker' name="stock_date_to" value="<?php echo $stock_date_to ; ?>" 
                              class="textbox date" autocomplete="off" readonly>
                        </td>

                        <?php if(count($location)>0){ ?>
                            <td>
                              <select name="location_code" id="location_code" class="textbox">
                                <?php if(count($location)>=2){?>
                                <option value="">Select Location </option>
                                <?php }?>
                                  <?php foreach($location as $locations){ ?>
                                    <option <?php if($location_code == $locations['location_code'] ){ $location_name=$locations['location_name']; ?> selected <?php } ?> value="<?php echo $locations['location_code']; ?>"><?php echo $locations['location_name']; ?></option>
                                  <?php } ?>
                               
                              </select>
                            </td>
                        <?php } ?>
                        <!-- <td>
                            <select name="filter_department" class="textbox">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if (trim($department['department_code']) == $filter_department) { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo trim($department['department_code']); ?>"><?php echo $department['department_name']; ?></option>  
                                        <?php } ?>   
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td> -->
                        <td>
                            <button style="width:100%; min-height: 36px; border-radius:0 !important; float: left;" type="button" onclick="filterReport();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                          </td>
                        </tr>
                    </tbody>
                  </table>
              </form>
            </div>
            <div style="clear:both; margin: 0 0 15px 0;"></div>

            <table class="table orderlist statusstock">
              <tr>
                <td colspan="5" align="right" style="width: 75%;">Total</td>
                <!-- <td align="right" style="width: 10%;"><?php echo $products_sum['tot_quantity']; ?></td> -->
                <!-- <td  align="right" style="width: 10%;"></td> -->
                <td align="right" style="width: 10%;"><?php echo number_format($products_sum['tot_value'],2); ?></td>
              </tr>
            </table>  


       <?php if (strtolower($stock_report) == 'stock') { ?>
          <table class="table orderlist statusstock">
            <thead>
              <tr class="heading">
                <td class="center" style="width: 5%;">S.No</td>
                <td class="center" style="width: 10%;">Location</td>
                <td class="center" style="width: 10%;">Date</td>
                <td class="center" style="width: 25%;">Total</td>
                <td class="center" style="width: 25%;">GST</td>
                <td class="center" style="width: 25%;">Net Total</td>
              </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
               <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">
                    <td class="center"><?php echo $product['pagecnt']+$i; ?></td>
                    <td class="center"><?php echo $product['location_code']; ?></td>
                    <td class="center"><?php echo $product['stkAdjustment_date']; ?></td>
                    <td class="right"><?php echo $product['total_value']; ?></td>
                    <td class="right"><?php echo $product['gst']; ?></td>
                    <td class="right"><?php echo $product['net_total']; ?></td>

                  </tr>
              <?php $i++; } ?>             
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="8"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>               
      <?php } elseif(strtolower($stock_report) == 'barcode_list') { ?>
          <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                <td class="center">Inventory Code</td>
                <td class="center">Description</td>
                <td class="center">Barcode</td>
                <td class="center">Bin</td>
              </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
              <?php if($product['barcodes'] != '') { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="center"><?php echo $product['barcodes']; ?></td>
                    <td class="center"><?php echo $product['bin']; ?></td>                    
                  </tr>
              <?php } ?> 
              <?php } ?>             
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table>  
     <?php }else if(strtolower($stock_report) == 'master'){ ?> 

            <table class="table orderlist statusstock">
            <thead>
                <tr class="heading">
                  <td class="center" style="width: 5%;">S.No</td>
                  <td class="center" style="width: 10%;">Inv.Code</td>
                  <td class="center" style="width: 10%;">Department </td>
                  <td class="center" style="width: 10%;">Category </td>
                  <td class="center" style="width: 10%;">Brand </td>
                  <td class="center" style="width: 30%;">Description</td>
                  <td class="center" style="width: 10%;">Price</td>
                </tr>
            </thead>
            <tbody>           
              <?php if (!empty($products)) { $i = 1; ?>
              <?php $class = 'odd'; ?>
              <?php foreach ($products as $product) { ?>
               <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                  <tr class="<?php echo $class; ?>">                  
                    <td class="center"><?php echo $i; ?></td>
                    <td class="center"><?php echo $product['sku']; ?></td>
                    <td class="center"><?php echo $product['sku_department_code']; ?></td>
                    <td class="center"><?php echo $product['sku_category_code']; ?></td>
                    <td class="center"><?php echo $product['sku_brand_code']; ?></td>
                    <td class="center"><?php echo $product['name']; ?></td>
                    <td class="center"><?php echo $product['sku_price']; ?></td>
                  </tr>
              <?php $i++; }  ?>             
              <?php } else { ?>
              <tr>
                  <td align="center" colspan="5"><?php echo $text_no_results; ?></td>
              </tr>
              <?php } ?>              
            </tbody>
          </table> 

      <?php }?>     
      </div>   
        <div class="pagination"><?php echo $pagination; ?></div>       
      </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function filterReport() {
  url = 'index.php?route=inventory/reports/stockAdjestmentReport&token=<?php echo $token; ?>&stock_report=stock';
  
  
  var location_code   = $('select[name=\'location_code\']').attr('value');  
  var stock_date_from = $('input[name=\'stock_date_from\']').attr('value');  
  var stock_date_to   = $('input[name=\'stock_date_to\']').attr('value');

  if (location_code != '') {
    url += '&location_code=' + encodeURIComponent(location_code);
  }
  if (stock_date_from != '' && stock_date_to !='' ) {
    url += '&stock_date_from=' + encodeURIComponent(stock_date_from);
    url += '&stock_date_to=' + encodeURIComponent(stock_date_to);
  }
  location = url;
}
function printFunction(){
  $(".pagination").hide();
  $(".set-bg-color").hide();

  window.print();
  $(".pagination").show();
  $(".set-bg-color").show();
}

</script>
<?php echo $footer; ?>
<script type="text/javascript">
  $(document).ready(function(){
  var location_code = $('[name="location_code"] option:selected').text();
  $( "#filter_location_codeid" ).html(location_code);

  var filter_department = $('[name="filter_department"] option:selected').text();
  $( "#filter_departmentid" ).html(filter_department);

  var stock_date = $('[name="stock_date"]').val();
  $( "#stock_date" ).html(stock_date);
});
</script>