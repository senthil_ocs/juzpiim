<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
	<div class="page-content-wrapper">    	
		<div class="page-content">
			<h3 class="page-title">
			<?php echo $heading_title; ?> <small></small>
			</h3>
        	<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>                    
				</ul>
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                            <?php if($this->user->hasPermission('modify', $route)) { ?>
                            <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                             <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                             <?php } ?>
                       	</div>    
                    </div>
                </div>
			</div>            
            <div class="row">

                 <div class="page-bar innerpage-listcontent-blocks" style="margin-top: -26px; min-width: 100%; border-top:1px solid #ccc;">
                    <div class="portlet bordernone" style="margin-bottom:0 !important">
                        <div class="page-bar portlet-title" style="min-height: 45px; min-width: 100%; margin: 0; padding: 15px 0 0 0;">
                            <div class="caption">                  
                                <table class="table orderlist statusstock" style="margin: -6px 0 0 30px;">
                                    <tbody>
                                        <tr class="filter">                       
                                            <td><input type="text" name="filter_name" id="filter_name" value="<?php echo $filter_name; ?>" class="textbox" placeholder="Department Name"></td>
                                            <td align=""><button style="min-height: 36px; border-radius:0 !important;width: 100%;" type="button" onclick="filter();" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                      
                        </div>
                        <div style="clear:both; margin: 0 0 15px 0;"></div>
                    </div>
                </div>
                    
            	 <div class="col-md-12">
                    <?php if ($success) { ?>
                        <div class="alert alert-block alert-success fade in setting-success">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $success; ?>
                        </div>
                    <?php } ?>

                     <?php if ($error_warning) { ?>
                        <div class="alert alert-block alert-danger fade in setting-danger">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <?php echo $error_warning; ?>
                        </div>
                    <?php } ?>

                 </div>
                 <div class="innerpage-listcontent-blocks">
                <div class="col-md-12">
                	<div class="innerpage-listcontent-blocks">
                	<table class="table orderlist statusstock country-lisblk">
                    	<thead>
                        	<tr class="heading">
                                <td width="1" style="text-align: center;"><input type="checkbox" onClick="jQuery('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
                                <td class="left"><?php echo $column_code; ?></td>
                                
                                <td class="left"><?php if ($sort == 'name') { ?>
                                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                                    <?php } ?>
                                </td>

                                <td class="left"><?php if ($sort == 'remarks') { ?>
                                    <a href="<?php echo $sort_remarks; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_remarks; ?></a>
                                    <?php } else { ?>
                                    <a href="<?php echo $sort_remarks; ?>"><?php echo $column_remarks; ?></a>
                                    <?php } ?>
                                </td>
                                
                                <td><?php if ($sort == 'salesman') { ?>
								<a href="<?php echo $sort_salesman; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_salesman; ?></a>
								<?php } else { ?>
								<a href="<?php echo $sort_salesman; ?>"><?php echo $column_salesman; ?></a>
								<?php } ?>
							</td>
                                <td class="right"><?php echo $column_action; ?></td>
                            </tr>
                        </thead>
                        <tbody>
						<?php if ($departments) { ?>
							<?php foreach ($departments as $department) { ?>
                            <?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
								<tr class="<?php echo $class; ?>">
									<td>
										<?php if ($department['selected']) { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $department['code']; ?>" checked="checked" />
										<?php } else { ?>
										<input type="checkbox" name="selected[]" value="<?php echo $department['code']; ?>" />
										<?php } ?>
									</td>
								     <td><?php echo $department['code']; ?></td>
                                    <td><?php echo $department['name']; ?></td>
                                	<td><?php echo $department['remarks']; ?></td>
                                    <td>
										<?php if($department['status']==1): ?> Active <?php else: ?> inActive <?php endif; ?>
									</td>
									<td>
										<?php foreach ($department['action'] as $action) { ?>
											[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } else { ?>
						<tr>
							<td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
						<?php } ?>
					</tbody>
                    </table>
                    </div>
                    <div class="pagination"><?php echo $pagination; ?></div>
                </div>
                </div>
            </div>
		</div>
	</div>
    </form>
</div>
<script type="text/javascript"><!--
function filter() {
    url = 'index.php?route=inventory/department&token=<?php echo $token; ?>';
    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    location = url;
}
//--></script>
<?php echo $footer; ?>