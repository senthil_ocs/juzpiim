
	<?php if($show) { ?>
  <h3 class="page-title">POS <small>/ Customer List</small></h3>
  <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd; margin-left: -30px;">
        <ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                  <?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <li>
                  <i class="fa fa-angle-right"></i><a><?php echo 'Customer List'; ?></a>
                </li>
            <?php  }?>                    
        </ul> 
        <div class="page-toolbar">
             <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <button type="button" class="btn btn-primary backssale" id="backssale" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-reply"></i><span> CURRENT SALE</span></button>
                </div>    
                </div>
            </div>                              
      </div>
      <div class="innerpage-listcontent-blocks">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
              <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -30px; padding:16px 0 0 0">
                <div class="caption"> 
                  <form method="post" name="report_filter" id="cus_report_filter" action="<?php echo $action ?>">
                    <input type="hidden" name="type" id="type">  
                     <input type="hidden"  id="page_cus" name="page">                    
                      <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                        <tbody>
                          <tr class="filter">  
                            <td>
                          <input type="text" placeholder='Name / Mobile Number' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
                          <input type="hidden" name="search_form" value='1'>
                            </td> 
                            <td>
                          <input type="text" placeholder='Email' name="filter_email" value="<?php echo $_POST['filter_email'] ; ?>" class="textbox" style="width: 300px;"  />
                          </td>                  
                            <td align="center" colspan="4">
                                <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="submit" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      </form>
                    </div>  
                  </div> 
                <div style="clear:both; margin:0 0 15px 0;"></div>               
              </div>
            </div>
            <div style="clear:both; margin:0px;"></div>
		      <div class="row posrow">           
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl custtpl">
            <div class="table-responsive" id="addCustomertopos" style="border-bottom: none !important;">
            <?php } ?>  
              <table class="table table-advance table-hover table-borderd"  style="width:100%">
                <thead >
                  <tr>
                    <th class="th-row"  style="width:175px;">&nbsp;</th>
                    <th class="th-row" style="">NAME</th>
                    <th class="th-row" style="">MOBILE</th>
                    <th class="th-row" style="">EMAIL</th>
                  </tr>
                </thead>
                <tbody id="customerlistbody">
                   <?php if ($customers) { $i=0;  ?>                        
                      <?php foreach ($customers as $customer) { $i++;?>
                  <tr style="font-size:14px;">
                    <td class="td-row">
                      <button data-id="addProduct-<?php echo $customer['customer_id'];?>" class="btn btn-update addcustosale" name="addProductToCart" type="button"><i class="icon-plus "></i> &nbsp;Attach to Sale</button>
                    </td>
                    <td class="td-row"><?php echo $customer['name'];?></td>
                    <td class="td-row"><?php echo $customer['mobile']; ?></td>
                    <td class="td-row"><?php echo $customer['email']; ?></td>
                  <tr>
                  <?php } ?>
                  <?php } else { ?>
                  </tr>
                    <td align="center" colspan="7">No Results</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
               <div class="pagination"><?php echo $pagination; ?></div>
         
           <?php if($show) { ?>
           </div>

        </div>
      </div>
			  
</div>
<script type="text/javascript">
  $(document).ready(function(){
     $("#cus_report_filter").ajaxForm({
        success:function(out)
        {
          $("#addCustomertopos").html(out);
        }
     });

     $( "#addCustomertopos" ).delegate( ".addcustosale", "click", function(e) {
         var c = $(this).attr('data-id');
         c= c.replace("addProduct-",'');
         attachcustomer(c);
         $(".backssale").trigger('click');

     });
     $( ".custtpl" ).delegate( ".pagination > .links a", "click", function(e) {
        e.preventDefault();
        var page = $(this).attr('class');
       ;
        $("#page_cus").val(page);
        $("#cus_report_filter").submit(); 
     });
  });
</script>
<?php } ?>