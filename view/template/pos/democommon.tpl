<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title"><?php echo $heading_title; ?></h3>
            <div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption" >
						<i class=""></i> CURRENT SALE
					</div>   
				</div> 
				<div class="portlet-body bgnone">
					<a href="#" class="icon-btn">
						<i class="fa fa-mail-forward"></i>
						<div>Continue Sale</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-star"></i>
						<div>Special Order</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-umbrella"></i>
						<div>Email Receipt</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-ticket"></i>
						<div>Refund</div>
					</a>
					<a href="#" class="icon-btn">
						<i class="fa fa-usd"></i>
						<div>New Sale</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<style type="text/css">
.sub-para{color: #666;font-size: 13px; margin: 15px 0px 0px 15px;}
.icon-btn-small{background-color:#eeeeee !important;background-image:none !important;border:1px solid #ddd;border-radius: 4px;box-shadow:none !important;color:#646464 !important;cursor:pointer;display:inline-block !important;filter: none !important; height: auto;margin:0 3px 10px 0;min-height:44px;padding:13px 12px;position:relative;text-align:center;text-shadow:none !important; transition: all 0.3s ease 0s !important;width: 40px;}
.cap-size{font-size: 14px ! important;}
</style>