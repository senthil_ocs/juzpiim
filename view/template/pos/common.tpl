<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title"><?php echo 'POS'; ?></h3>
            <div class="portlet bordernone">
				<div class="portlet-title">
					<div class="caption" >
						<i class=""></i> CURRENT SALE
					</div>   
				</div> 
				<div class="portlet-body bgnone">
					<?php if($count_sales == 0) { ?>
					<?php } else { ?>
					<a href="<?php echo $continue_sale; ?>" class="icon-btn" id="continuesale">
						<span class="kbd_shortcut">ALT+S</span>
						<i class="fa fa-mail-forward"></i>
						<div>Continue Sale</div>
					</a>
					<?php } ?>
					<a href="<?php echo $new_sale; ?>" class="icon-btn" id="newsale">
					   <span class="kbd_shortcut">ALT+N</span>
						<i class="fa fa-usd"></i>
						<div>New Sale</div>

					</a>
					<!-- <a href="#" class="icon-btn">
						<i class="fa fa-star"></i>
						<div>Special Order</div>
					</a> -->
					<!-- <a href="#" class="icon-btn">
						<i class="fa fa-umbrella"></i>
						<div>Email Receipt</div>
					</a> -->	
					<?php if(isset($refund_sale)){ ?>				
					<a href="<?php echo $refund_sale; ?>" class="icon-btn" id="refund">
						 <span class="kbd_shortcut">ALT+R</span>
						<i class="fa fa-ticket"></i>
						<div>Refund</div>
					</a>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
	Mousetrap.bind('alt+n', function(e) {
        e.preventDefault();
        var href = $('#newsale').attr('href');
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+r', function(e) {
        e.preventDefault();    
         var href = $('#refund').attr('href');   
        window.location.href=href;
        return false;
    });
    Mousetrap.bind('alt+s', function(e) {
        e.preventDefault();    
        var href = $('#continuesale').attr('href');    
        window.location.href=href;
        return false;
    });
});
</script>