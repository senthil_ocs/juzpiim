<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
	<div class="page-content">
		<h3 class="page-title">POS</h3>
		<div class="row posrow">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 form form-actions top bgpos1" style="height:40px;">
        <button  class="btn btn-primary" style="float:left; margin:4px 5px;">Current Sales</button>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb1">
								
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb13">
			
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mb14">				
					
				</div>
			</div>
				<!-- Filter Starts  -->
			<div class="innerpage-listcontent-blocks">
        <div class="portlet bordernone" style="margin-bottom:0 !important">
          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -6px 2px; padding:16px 0 0 0">
            <div class="caption" style=""> 
              <form method="post" name="report_filter" id="report_filter" action="">
                <input type="hidden" name="type" id="type">               
                  <table class="table orderlist statusstock" style="margin: -10px 0 0 25px; width: 97%; border-right:none ! important;">
                    <tbody>
                      <tr class="filter">  
                      	<td>
              			<input type="text" placeholder='Item Search' name="filter_itemsearch" value="<?php echo $_POST['filter_itemsearch'] ; ?>" class="textbox" style="width:300px;border: 1px solid #aaaaaa;border-right: none ! important; " />
                        </td>
                        <td>
              			<input type="text" placeholder='Tag(s)' name="filter_tag" value="<?php echo $_POST['filter_tag'] ; ?>" class="textbox" style="width:125px;border: 1px solid #aaaaaa;border-right: none ! important; "  />
                        </td>                   
            			<td>
              			<input type="text" placeholder='Exclude' name="filter_exclude" value="<?php echo $_POST['filter_exclude'] ; ?>" class="textbox" style="width:125px;border: 1px solid #aaaaaa;border-right: none ! important; "  />
                        </td>		                      
                        <td align="center" colspan="4">
                         <button style="min-height: 38px;margin-left: -3px;border-radius:0 !important;border:1px solid #3178BD ! important;" type="button" onclick="addProducts();" class="btn btn-zone btn-primary"><i class="fa fa-search"></i> &nbsp; Search</button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
            </div>  
            <div class="tools" style="float:left;padding: 0 0 0 15px;">
               <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
            </div>
            </div> 
              <?php if (($_POST['filter_date_from'] !="") || ($_POST['filter_date_to'] != "") || ($_POST['filter_email'] != "")){ 
           		 		$showHide  = 'style="display:block;"';
        			} else {
            			$showHide  = 'style = "display:none;"';
        		}?>  
           <div class="page-bar portlet-body bgcolor" <?php echo $showHide; ?>>
              <table class="table orderlist statusstock" style="margin-bottom: 0 !important; margin-left:15px; width: 15%;">
                <tbody>
               
                  <tr class="filter" style="background-color:#EEEEEE ! important; ">
                   	<td style="background-color:#EEEEEE;">
                   		<label class="checkbox">
                          <input type="checkbox" name="ds_hobbies[]" value=""/> 
                          <i></i>Archive
                        </label>
            		</td>
              		<td style="background-color:#EEEEEE;border-right:none">
              			<label style="margin:13px;">Manufacturer</label>
                   		<select class="dropdwn">
                   			<option value="All Manufacturers">All Manufacturers</option>
                   			<option value="None">None</option>
                   			<option value="Tesing">Testing</option>
                   		</select>	
            		</td> 
            		<td style="background-color:#EEEEEE;border-right:none">
              			<label style="margin:13px;">Default Vendor</label>
                   		<select class="dropdwn">
                   			<option value="Default Vendor">Default Vendor</option>
                   			<option value="ABCD">ABCD</option>
                   			
                   		</select>	
            		</td> 
            		<td style="background-color:#EEEEEE;border-right:none">
              			<label style="margin:13px;">Tax Class</label>
                   		<select class="dropdwn">
                   			<option value="All Tax Classes">All Tax Classes</option>
                   			<option value="Default/Item">Default/Item</option>
                   			
                   		</select>	
            		</td> 
                  </tr>
                </tbody>
              </table>
            </div>
            </form>
            <div style="clear:both; margin: 0 0 15px 0;"></div>
            </div>
            </div>
            <!-- Filter Ends -->
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl postbl_new" style="width: 100%;">
          <div class="table-responsive">  
            <table class="table table-advance table-hover table-borderd" id="addproducts"  style="width:100%">
            <thead >
              <tr>
                <td style="width:145px;"> &nbsp;</td>
                <td class="invntrycode" style="">INVENTORY CODE</td>
                <td class="prodname" style="">Product Name</td>
                <td class="qty" style="">QTY</td>
                <td class="price" style="">PRICE</td>
              </tr>
            </thead>
            <tbody>
               <?php if ($product_collection) { $i=0; ?>
                    
                  <?php foreach ($product_collection as $product_collections) { $i++;?>
              <tr style="font-size:14px;">
                <td>
              <input class="checkbox1" type="checkbox" name="check[]" value="<?php echo $product_collections['product_id']; ?>"> &nbsp; &nbsp;

                  <button data-id="addProduct-13" class="btn btn-update addProductToCart" name="addProductToCart" type="button"><i class="icon-plus "></i> &nbsp;Add</button>
                </td>
                <td ><?php echo $product_collections['sku'];?></td>
                <td class="productname-td"><?php echo $product_collections['name'];?></td>
                <td align="right"><?php echo $product_collections['quantity']; ?></td>
                <td align="right"><?php echo $product_collections['price']; ?></td>
              <tr>
              <?php } ?>
              <?php } else { ?>
              </tr>
                <td align="center" colspan="5">No Results</td>
              </tr>
              <?php } ?>
             
              <tr>
                <td colspan="5">
                  <input type="checkbox" id="selectAll"/>&nbsp;&nbsp; 
                  <button data-id="addProduct-13" class="btn btn-update addProductToCart" name="addProductToCart" type="button"><i class="icon-plus "></i> Add Selected</button></td>
            </tr>
            </tbody>
            </table>
          </div>
        </div>
			  
		</div>
	</div>
	</div>

</div>

<?php echo $footer; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('#selectAll').on('click',function() {
    if(this.checked){
      $('.checkbox1').each(function(){
        this.checked = true;
      });
    } else {
      $('.checkbox1').each(function(){
        this.checked = false;
      });
    }
  });
    
  $('.checkbox1').on('click',function() {
    if($('.checkbox1:checked').length == $('.checkbox1').length){
      $('#selectAll').prop('checked',true);
    } else {
      $('#selectAll').prop('checked',false);
    }
  });
});
</script>
