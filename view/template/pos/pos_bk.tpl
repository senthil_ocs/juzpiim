<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
			Dashboard <small>statistics and more</small>
			</h3>			
                <div class="row posrow">					
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 form form-actions top bgpos" >
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 mb10">
								<input type="text" placeholder="Item" class="positembox">
								<button class="btn btn-primary postbtn " id="searchproducts_pos">
									<i class="fa fa-search"></i>&nbsp;Search<span class="skey">Insert</span></button>
							</div>
							<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 posbtngrp mb10">
								<a class="btn btn-default postbtn" id="reprint">Reprint<span class="skey">F1</span></a>
								<a class="btn btn-default postbtn" id="disc">Disc<span class="skey">F2</span></a>
								<a class="btn btn-default postbtn" id="voidbill">Void<span class="skey">F3</span></a>
								<a class="btn btn-default postbtn" id="tender">Tender<span class="skey">F4</span></a>
								<a class="btn btn-default postbtn" id="clearpos">Clear<span class="skey">F5</span></a>
								<a class="btn btn-default postbtn" id="release">Release<span class="skey">F6</span></a>
								<a class="btn btn-default postbtn" id="hold">Hold<span class="skey">F7</span></a>
								<?php if($this->session->data['is_exchange_manually']){ ?>
								<a class="btn btn-default postbtn postbtnactive" id="exitem">Ex Item<span class="skey">F8</span></a>
								<?php } else{ ?>
									<a class="btn btn-default postbtn" id="exitem">Ex Item<span class="skey">F8</span></a>
								 <?php } ?>
								<a class="btn btn-default postbtn" id="change">$ Chg<span class="skey">F9</span></a>
								<a class="btn btn-default postbtn">Refund<span class="skey">F10</span></a>
							</div>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl">
					<div class="table-responsive">  
						<table class="table table-advance table-hover table-borderd" id="producttobilling">
						<thead>
							<tr>
							<th>S No</th>
							<th>Bar Code</th>
							<th>Description</th>
							<th style="width:39px;">Qty</th>
							<th  style="text-align:right">Price</th>
							<th style="text-align:right">Amount</th>
							<th style="text-align:right">Disc.</th>
							<th style="text-align:right">Net Amount</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
						</table>
					</div>
				</div> 
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 pricepostbl">
						<table class="table table-advance table-hover" id="totalpricetbl">
						<tbody>
							<tr>
								<th>Subtotal</th>
								<td id="popdisplay_subtotal">$10,000.00</td>
							</tr>
							<tr class="discount">
								<th>Discounts</th>
								<td id="discountValue"></td>
							</tr>
							<tr data-automation="totalsTaxLineItem">
								<th id="taxName_1" data-automation="taxName">Tax</th>
								<td id="taxValue_1" data-automation="taxValue">$825.00</td>
							</tr>
							<tr class="total">
								<th id="totalName">Total</th>
								<td id="sale_total">$10,825.00</td>
							</tr>
						</tbody>
						</table>
						<div class="btn-group" style="margin:0px 10px">
							<button type="button" class="btn btn-default postbtn minw">Discount</button>
							<button type="button" class="btn btn-default postbtn minw">Tax</button>
						</div>
					</div>				
		<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 mb10">
			<button type="button" class="btn btn-default postbtn">$2</button>
			<button type="button" class="btn btn-default postbtn">$10</button>
			<button type="button" class="btn btn-default postbtn">$50</button>
			<button type="button" class="btn btn-default postbtn">$100</button>
			<button type="button" class="btn btn-default postbtn">EXACT CASH</button>
		</div>
		</div>
<!-- 05-03-2016 -->
<script type="text/javascript">
function startTime()
{
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m=checkTime(m);
	s=checkTime(s);
	document.getElementById('txt').innerHTML=h+":"+m+":"+s;
	t=setTimeout('startTime()',500);
}
function checkTime(i)
{
	if (i<10){
		i="0" + i;
	}
	return i;
}
</script>
<div class="row posrow" style="margin-top:20px">
 <p class="positembox pos_date"><b><?php echo date('d/m/Y'); ?> &nbsp;&nbsp;<span id="txt"></span></b></p>
 <p class="positembox pos_terminal"><b>Terminal :</b> <?php echo $_SERVER['REMOTE_ADDR']; ?></p> 
 <p class="positembox pos_loaction"><b>Cashier :</b> <?php echo $logged; ?></p>
 <?php if(isset($LastOrder)) { ?>
 <p class="positembox pos_terminal"><b>Bill No :</b> <?php echo $LastOrder[0]['invoice_no']; ?></p>
 <p class="positembox pos_invoice"><b>Last Invoice :</b> $<?php echo number_format($LastOrder[0]['total'],2); ?></p>
 <?php } ?>
</div>	
<!-- 05-03-2016 -->
		
	</div>

	</div>	
</div>
<div class="clearfix">
</div>
<script type="text/javascript">

</script>
<?php echo $footer; ?>
<div id="ajax-modal" class="modal container fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Available Products</h4>
		</div>
		<div class="modal-body">
			Loading........
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- TENDER MODAL -->
	<div id="ajax-modal1" class="modal fade modal-scroll in " tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Bill Settlement Screen</h4>
		</div>
		<div class="modal-body">
			Loading........
		</div>
		<div class="modal-footer form-action">
             <button type="button" name="submit" id="doSaveOrder" class="btn btn-primary">Enter</button>
             <button type="button" name="cancel" id="doCancelOrder" data-dismiss="modal" class="btn btn-danger">Close</button>
		</div>
	</div>
<!-- DISC MODAL -->
<div id="ajax-modal-disc" class="modal fade" tabindex="-1">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Bill Discount</h4>
	</div>
	<div class="modal-body-disc modal-body">
		Loading........
	</div>
	<div class="modal-footer form-action">                       
            <button type="button" name="submit" id="doSaveOrder1" class="btn btn-primary">Enter</button>
            <button type="button" name="cancel" id="doCancelOrder1" data-dismiss="modal" class="btn btn-danger">Close</button>
	</div>
</div>
<!--PRINT MODEL -->
<div id="ajax-modal-print" class="modal fade modal-scroll in" tabindex="-1">
	<form action="<?php echo $reprint_order; ?>" class="form-horizontal" method="POST" id="reprintform">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Reprint Invoice</h4>
	</div>
	<div class="modal-body">		
		<div class="form-body">
			<div class="form-group">
				<label class="col-md-4 control-label">Invoice Number</label>
				<div class="col-md-6">
					<input type="text" name="invoice_id" id="invoice_id" class="form-control" placeholder="Enter text">
					<span class="help-block">
					A block of help text. </span>
				</div>
			</div>
		</div>

	</div>
	<div class="modal-footer form-action">                       
            <button type="submit" name="submit" id="doSaveOrder2" class="btn btn-primary">Enter</button>
            <button type="button" name="cancel" id="doCancelOrder2" data-dismiss="modal" class="btn btn-danger">Close</button>
	</div>
	</form>
</div>

<!-- PRINT POPUP -->
<div id="ajax-modal-reprintinvoice" class="modal fade modal-scroll in" tabindex="-1">	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Reprint Invoice</h4>
	</div>
	<div class="modal-body" id="print-body">
	<div class="alert" id="reprintmessage"></div>
	</div>
	<div class="modal-footer form-action">                       
           <button class="btn btn-primary" name="submit" id="printin" type="button" >Print</button>
           <button class="btn btn-danger" name="cancel" data-dismiss="modal" type="button" id="doCancelOrder">Cancel</button>
	</div>	
</div>
<!-- END PRINT POPUP -->

<!--- VOID MODEL -->

	<div id="ajax-modal-void" class="modal fade" tabindex="-1">
		<form id="voidform" action="<?php echo $void_order; ?>" method="POST">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">VOID INVOICE</h4>
			</div>
			<div class="modal-body">
				<div class="alert" id="voidmessage"></div>	
				<div class="form-body" id="form-body">
					<div class="form-group">
						<label class="col-md-4 control-label">Invoice Number</label>
						<div class="col-md-6">
							<input type="text" class="form-control" name="void_bill_no" id="void_bill_no" placeholder="Enter text"
							<input type="hidden" class="form-control" name="is_verify" id="is_verify" value="1" placeholder="Enter text">				
						</div>
					</div>
				</div>
	 		</div>
			<div class="modal-footer form-action">
			<button type="submit" id="voidinvoice" class="btn btn-success">Cancel Invoice</button>
			<button type="button" data-dismiss="modal" class="btn btn-danger">Close</button>
			
			</div>
		</form>
	</div>

	<div id="ajax-modal-voidconfirm" class="modal fade" tabindex="-1">
		<form id="voidformc" action="<?php echo $void_order; ?>" method="POST">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">VOID CONFIMATION</h4>
			</div>
			<div class="modal-body">
				<div class="alert" id="voidmessagec"></div>	
						<input type="hidden" class="form-control" name="void_bill_no" id="void_billno" placeholder="Enter text">
						<input type="hidden" class="form-control" name="is_verify" id="is_verify" value="0" placeholder="Enter text">
	 		</div>
			<div class="modal-footer form-action">
				<button type="submit" id="voidinvoiceconfirm" class="btn btn-success">Yes</button>
				<button type="button" data-dismiss="modal" class="btn btn-danger">No</button>
			</div>
		</form>
	</div>
	<!--- CLEAR MODER -->

<div id="ajax-modal-clear" class="modal fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Confrimation</h4>
		</div>
		<div class="modal-body">
			<p>Do you want clear now?</p>
		</div>
		<div class="modal-footer form-action">
		<button type="button" data-dismiss="modal" class="btn btn-default">No</button>
		<button type="button" id="clearallpos" class="btn btn-success">Yes,Continue Clear</button>
		</div>
	</div>
	<!-- HOLD MODER -->
	<div id="ajax-modal-hold" class="modal fade" tabindex="-1">
		<form action="<?php echo $urlforhold; ?>" id="holdform" class="form-horizontal" method="POST">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">HOLD</h4>
	</div>
	<div class="modal-body">
		<div class="alert" id="holdmessage"></div>		
		<div class="form-body" id="form-body">
			<div class="form-group">
				<label class="col-md-4 control-label">Invoice Number</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="hold_bill_no" id="hold_bill_no" placeholder="Enter text">					
				</div>
			</div>
		</div>
	
	
	</div>
	<div class="modal-footer form-action">                       
           <button type="submit" name="submit" id="holdorder" class="btn btn-primary">Enter</button>
           <button type="button" name="cancel" id="holdorder2" data-dismiss="modal" class="btn btn-danger">Close</button>
	</div>
	 </form>
</div>
<!-- RELEASE MODER -->
<div id="ajax-modal-release" class="modal fade" tabindex="-1">
		<form action="<?php echo $urlforrelease; ?>" id="releaseform" class="form-horizontal" method="POST">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">RELEASE</h4>
	</div>
	<div class="modal-body">
		<div class="alert" id="releasemessage"></div>
		<div class="form-body" id="form-body1">
			<div class="form-group">
				<label class="col-md-4 control-label">Invoice Number</label>
				<div class="col-md-6">
					<input type="text" name="relaease_bill_no" id="relaease_bill_no" class="form-control" placeholder="Enter text">					
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer form-action">                       
           <button type="submit" name="submit" id="releaseorder" class="btn btn-primary">Enter</button>
           <button type="button" name="cancel" id="releaseorder2" data-dismiss="modal" class="btn btn-danger">Close</button>
	</div>
	 </form>
</div>
<!-- CHANGE MODER -->
<div id="ajax-modal-change" class="modal fade" tabindex="-1">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h4 class="modal-title">Price Change Utility</h4>
	</div>
	<div class="modal-body-change modal-body">
		Loading........
	</div>
	<div class="modal-footer form-action">                       
            <button type="button" name="submit" id="doSaveOrder3" class="btn btn-primary">Enter</button>
            <button type="button" name="cancel" id="doCancelOrder3" data-dismiss="modal" class="btn btn-danger">Close</button>
	</div>
</div>