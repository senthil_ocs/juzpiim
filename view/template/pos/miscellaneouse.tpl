<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
	<div class="page-content">
		<div id="posterminal">
		<h3 class="page-title">POS</h3>
		<div class="row posrow">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 form form-actions top bgpos1">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb1">
					<input type="text" placeholder="NO CUSTOMER SELECTED" class="cust_disp" readonly>					
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb13">
				<input type="text" placeholder="Search Customers" class="cust_search">
					<button class="btn btn-primary prod_btn"><i class="fa fa-search"></i> Search</button>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mb14">				
					<a href="<?php echo $add_customer; ?>"><button class="btn btn-primary cust_new"><i class="fa fa-plus"></i> New</button></a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 form form-actions top bgpos">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 mb11">
					<input type="text" placeholder="Barcode" class="barcode_search">
					<button class="btn btn-primary prod_btn" id="barcode"><i class="fa fa-plus"></i> ADD</button>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 form form-actions top bgpos">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 mb11">
					<input type="text" placeholder="Item" class="positembox">
					<button class="btn btn-primary prod_btn" id="searchproducts_pos"><i class="fa fa-search"></i> Search</button>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 form form-actions top bgpos2">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb11">					
					<a href="<?php echo $add_newproduct; ?>"><button class="btn btn-primary cust_new"><i class="fa fa-plus"></i> New</button></a>
					<button class="btn btn-primary cust_new">Misc.</button>
				</div>
			</div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl postbl_new">
				<div class="col-lg-12 bgcolor-set_height">Add Miscellaneouse Charge </div>
					<div class="table-responsive">  
						<table class="table table-striped" id="producttobilling">
							<tbody>
								<tr>
									<td class="comments" rowspan="4" style="background: white !important;width:15px !important;">
										<textarea id="Miscellaneous-Charge" name="Miscellaneous-Charge" class="txt-area-box">Miscellaneous Charge</textarea>
									</td>
									<td class="td-row" style="width:15px;">Price</td>
									<td class="td-row">
										
											<input type="textbox" id="price" name="price" class="small-txt" value="$">
									
									</td>
									<td class="td-row" style="width:15px;">Discount</td>
									<td class="td-row">
										<select id="price" name="price" class="small-drop-down">
											<option value="--">--</option>
										</select>
									</td>		
								</tr>
								<tr>
									<td class="td-row" style="width:15px;">Cost</td>
									<td class="td-row">
										<input type="textbox" id="price" name="price" class="small-txt" value="$">
									</td>
									<td class="td-row" style="width:15px;">Tax Class</td>
									<td class="td-row">
										<select id="price" name="price" class="small-drop-down" >
											<option value="item">item</option>
										</select>
									</td>		
								</tr>
								<tr>
									<td class="td-row" style="width:15px;">Qty</td>
									<td class="td-row">
										<input type="textbox" id="price" name="price" class="small-txt">
									</td>
									<td class="td-row" style="width:15px;">Employee</td>
									<td class="td-row">
										<select id="price" name="price" class="emp-drop-down">
											<option value="MANI KANDAN">MANI KANDAN</option>
										</select>
									</td>		
								</tr>
								<tr>
									<td class="td-row" style="width:15px;"></td>
									<td class="td-row"><div style="width:60px;"><input type="checkbox" id="" name="">&nbsp;Tax</div></td>
								</tr>	
							</tbody>
							<tbody style="border-top:none ! important;">
								<tr><td class="td-row"><input type="submit" value="Submit" class="buttons">&nbsp;&nbsp;<input type="reset" value="Cancel" class="buttons"> </td></tr>
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pricepostbl pricepostbl-new">
				<table class="table table-advance table-hover" id="totalpricetbl">
				<tbody>
					<tr>
						<th>Subtotal</th>
						<td id="popdisplay_subtotal">$0.00</td>
					</tr>
					<tr class="discount">
						<th>Discounts</th>
						<td id="discountValue"></td>
					</tr>
					<tr data-automation="totalsTaxLineItem">
						<th id="taxName_1" data-automation="taxName">Tax</th>
						<td id="taxValue_1" data-automation="taxValue">None</td>
					</tr>
					<tr class="total">
						<th id="totalName">Total</th>
						<td id="sale_total">$0.00</td>
					</tr>
				</tbody>
				</table>
				<div class="btn-group" style="margin:0px 10px">
					<button type="button" class="btn btn-default postbtn minw" id="disc">Discount</button>
					<button type="button" class="btn btn-default postbtn minw">Tax</button>
				</div>
				<div  class="addingposbuttons">
					<button type="button" class="btn btn-default postbtn minw minw-new" id="pospayment">Payment</button>
					<button type="button" class="btn btn-default postbtn minw minw-new2" id="clearpos">Cancel</button>
					<button type="button" class="btn btn-default postbtn minw minw-new3" id="hold">Save as Quote <i class="fa fa-arrow-right"></i></button>
				</div>
				<div class="finishbuttons">
					<table class="table table-advance table-hover" id="totalpricetbl2">
				<tbody>
					<tr>
						<th colspan="2">PAYMENT TOTAL</th>
					</tr>
					<tr>
						<th>Payments</th>
						<td id="pymentsubtotal">$0.00</td>
					</tr>
					<tr class="discount">
						<th>Balance</th>
						<td id="pymentbalance">$0.00</td>
					</tr>
				</tbody>
				</table>
					<button id="finishSaleButton" class="btn btn-default postbtn minw-new3">Finish Sale</button>
					<button id="backToSaleButton" class="btn btn-default postbtn minw minw-new3"><i class="icon-arrow-left"></i> Back To Edit Sale</button>
				</div>
			</div>				
		</div>
	</div>
	<div id="addProducts" style="display:none">
	</div>
	<div id="Makepayment" >
		<form action="<?php echo $payment_tender; ?>" method="post" enctype="multipart/form-data" id="bill-payment-form" class="form-horizontal">
 <div class="col-md-4">
      <div class="portlet whole-container" style="">
        <div class="portlet-title payment-title" style="">
          <div class="caption">
            <label class="payment-caption">PAYMENT</label>
          </div>
          <div class="tools">
          </div>
        </div>
        <div class="portlet-body form">
          <!-- BEGIN FORM-->
            <div class="form-body bg-color" style="">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                  <tr>
                    <th class="cash-td">Total Amout: <span>$</span></th>
                    <td>
                        <div class="payment-slider">
                          <input type="text" type="text"  id="totalamthidden"  value="" size="10" readonly="readonly" class="cash-txt" style=""> 
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="quickcash" colspan="2" style="">
                      <button type="button" class="td-btn" value='1' style="">$1</button>&nbsp;
                      <button type="button" class="td-btn" value='5'>$5</button>&nbsp;
                      <button type="button" class="td-btn" value='10'>$10</button>&nbsp;
                      <button type="button" class="td-btn" value='20'>$20</button> <br> <br> 
                      <button type="button" class="td-btn" value='50'>$50</button>&nbsp;
                      <button type="button" class="td-btn" value='100'>$100</button>
                    </td>
                  </tr>
                  <tr class="tr-footr">
                    <th class="chk-td">Slip Number<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="silp_number" id="cash_payment" size="10" value="" class="cash-txt">
                      </div>
                     </td>
                  </tr>   
                   <tr class="tr-footr">
                    <th class="chk-td">Cash :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="cash_amount" id="debit_payment" size="10" value="0" class="cash-txt"><button class="payment-max-btn" type="button" id="debit_max_btn">Max</button>
                      </div>
                    </td>
                  </tr> 
                  <tr class="tr-footr">
                    <th class="chk-td">Card :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="card_amount" id="credit_payment" size="10" value="0" class="cash-txt"><button class="payment-max-btn" type="button" id="credit_max_btn">Max</button>
                      </div>
                    </td>
                  </tr>    
                  
                  <tr class="tr-footr">
                    <th class="chk-td">Visa :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="visa_amount" id="visa_amount" size="10" value="0" class="cash-txt"><button class="payment-max-btn" type="button" id="visa_max_btn">Max</button>
                      </div>
                    </td>
                  </tr>     
                </tbody>
              </table>
            </div>
            <div class="form-actions payment-footer" style="">
              <label class="cash-td total-lbl" style="">Total</label>
              <input autocomplete="off" name="paid_amount" readonly="readonly" id="payments_total" value="$0.00" size="10" class="total-result">
            </div>
          </form>
                        <!-- END FORM-->
        </div>
      </div>
    </div>
	</div>
	</div>

	
	</div>
</div>
<?php echo $footer; ?>

<div id="ajax-modal" class="modal container fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Available Products</h4>
		</div>
		<div class="modal-body">
			Loading........
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- CLEAR -->
	<div id="ajax-modal-clear" class="modal fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Confrimation</h4>
		</div>
		<div class="modal-body">
			<p>Cancel the current Transaction?</p>
		</div>
		<div class="modal-footer form-action">
		<button type="button" id="clearallpos" class="btn btn-success">Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>		
		</div>
	</div>
<style type="text/css">
.bgcolor-set_height{background-color: #EEEEEE;min-height:50px;line-height: 50px;color:#696666;font-size:16px;font-weight: bold;}
.comments{vertical-align: top;padding-top: 10px;width:15px !important;}
.small-txt{width: 90px; height: 30px; border:1px solid #DDDDD;}
.small-drop-down{width:60px; height: 30px; border:1px solid #DDDDD;}
.td-row{width:15px;}.emp-drop-down{width:120px;height:32px;}
.buttons{width:80px; height: 35px;  background-color:#F1F1F1; border:1px solid #AAAAAA; border-radius:3px;}
.txt-area-box{height: 151px; width: 303px;}
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    border-top: none ! important;
    line-height: 1.42857; padding: 0px !important;vertical-align: middle;
}
</style>