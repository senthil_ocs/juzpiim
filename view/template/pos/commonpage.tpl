<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">
				<?php echo $heading_title; ?><small></small>
			</h3>
            <div class="portlet bordernone">
					<div class="portlet-title portlet-height">
						<div class="caption caption-size">
							<i class="fa fa-star-o" style="font-size: 30px;line-height:30px;"></i> Thank You !
						</div>   
					</div> 
					<p class="adjustment-para"></p>
					<h4 class="sub-title"> Next Step</h4>
					<p class="sub-para">The transaction has been recorded. Next you probably want to do one of the following:</p>    
					<div class="portlet-body bgnone">
						<a href="#" class="icon-btn">
							<i class="fa fa-print"></i>
							<div>Print</div>
						</a>
						<a href="#" class="icon-btn">
							<i class="fa fa-print"></i>
							<div>Print Gift Receipt</div>
						</a>
						<a href="#" class="icon-btn">
							<i class="fa fa-envelope"></i>
							<div>Email Receipt</div>
						</a>
						<a href="#" class="icon-btn">
							<i class="fa fa-usd"></i>
							<div>New Sale</div>
						</a>
					</div>
				</div>
       		<div class="portlet-body bordernone">
					<div class="row">
						<iframe id="" src="" width="100%" height="400px" class="iframe-tag"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php echo $footer; ?>
<style type="text/css">
.sub-para{color: #666;font-size: 14px; margin: 15px 0px 0px 15px;}
.caption-size{font-size: 20px ! important;line-height: 30px ! important;}
.portlet-height{min-height: 50px ! important;}
.adjustment-para{min-height: 30px;}
.txt-area-fld{margin-left:15px;}
.iframe-tag{max-width: 600px; border: 1px solid #888; padding: 10px; margin: 20px;}
.sub-title{margin-left:15px;border-bottom:1px solid #DDDDDD;line-height:32px;}
</style>