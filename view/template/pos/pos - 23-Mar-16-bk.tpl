<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
	<div class="page-content">
		<div id="posterminal">
		<h3 class="page-title">POS</h3>
		<div class="row posrow">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 form form-actions top bgpos1">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb1">
					<input type="text" placeholder="NO CUSTOMER SELECTED" class="cust_disp" readonly>					
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb13">
				<input type="text" placeholder="Search Customers" class="cust_search">
					<button class="btn btn-primary prod_btn"><i class="fa fa-search"></i> Search</button>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mb14">				
					<button class="btn btn-primary cust_new"><i class="fa fa-plus"></i> New</button>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 form form-actions top bgpos">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 mb11">
					<input type="text" placeholder="Barcode" class="cust_search">
					<button class="btn btn-primary prod_btn" ><i class="fa fa-plus"></i> ADD</button>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 form form-actions top bgpos">
				<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 mb11">
					<input type="text" placeholder="Item" class="positembox">
					<button class="btn btn-primary prod_btn" id="searchproducts_pos"><i class="fa fa-search"></i> Search</button>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 form form-actions top bgpos2">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb11">					
					<button class="btn btn-primary cust_new"><i class="fa fa-plus"></i> New</button>
					<button class="btn btn-primary cust_new">Misc.</button>
				</div>
			</div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl postbl_new">
					<div class="table-responsive">  
						<table class="table table-advance table-hover table-borderd" id="producttobilling">
						<thead>
							<tr>
							<th></th>
							<th>Bar Code</th>
							<th>Description</th>
							<th style="width:39px;">Qty</th>
							<th style="text-align:right">Price</th>
							<th style="text-align:right">Amount</th>
							<th style="text-align:right">Disc.</th>
							<th style="text-align:right">Net Amount</th>
						</tr>
						</thead>
						<tbody>
						</tbody>

						</table>
					</div>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 pricepostbl pricepostbl-new">
				<table class="table table-advance table-hover" id="totalpricetbl">
				<tbody>
					<tr>
						<th>Subtotal</th>
						<td id="popdisplay_subtotal">$0.00</td>
					</tr>
					<tr class="discount">
						<th>Discounts</th>
						<td id="discountValue"></td>
					</tr>
					<tr data-automation="totalsTaxLineItem">
						<th id="taxName_1" data-automation="taxName">Tax</th>
						<td id="taxValue_1" data-automation="taxValue">None</td>
					</tr>
					<tr class="total">
						<th id="totalName">Total</th>
						<td id="sale_total">$0.00</td>
					</tr>
				</tbody>
				</table>
				<div class="btn-group" style="margin:0px 10px">
					<button type="button" class="btn btn-default postbtn minw">Discount</button>
					<button type="button" class="btn btn-default postbtn minw">Tax</button>
				</div>
				<div  class="addingposbuttons">
					<button type="button" class="btn btn-default postbtn minw minw-new" id="pospayment">Payment</button>
					<button type="button" class="btn btn-default postbtn minw minw-new2" id="clearpos">Cancel</button>
					<button type="button" class="btn btn-default postbtn minw minw-new3">Save as Quote <i class="fa fa-arrow-right"></i></button>
				</div>
				<div class="finishbuttons">
					<button id="finishSaleButton" class="btn btn-default postbtn minw-new3">Finish Sale</button>
					<button id="backToSaleButton" class="btn btn-default postbtn minw minw-new3"><i class="icon-arrow-left"></i> Back To Edit Sale</button>
				</div>
			</div>				
		</div>
	</div>
	<div id="addProducts" style="display:none">
	</div>
	<div id="Makepayment" >
		<form action="<?php echo $payment_tender; ?>" method="post" enctype="multipart/form-data" id="bill-payment-form" class="form-horizontal">
 <div class="col-md-4">
      <div class="portlet whole-container" style="">
        <div class="portlet-title payment-title" style="">
          <div class="caption">
            <label class="payment-caption">PAYMENT</label>
          </div>
          <div class="tools">
          </div>
        </div>
        <div class="portlet-body form">
          <!-- BEGIN FORM-->
            <div class="form-body bg-color" style="">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                  <tr>
                    <th class="cash-td">Total Amout: <span>$</span></th>
                    <td>
                        <div class="payment-slider">
                          <input type="text" type="text" name="paid_amount" id="totalamthidden"  value="" size="10" readonly="readonly" class="cash-txt" style=""> 
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="quickcash" colspan="2" style="">
                      <button type="button" class="td-btn" value='1' style="">$1</button>&nbsp;
                      <button  type="button" class="td-btn" value='5'>$5</button>&nbsp;
                      <button type="button" class="td-btn" value='10'>$10</button>&nbsp;
                      <button type="button" class="td-btn" value='20'>$20</button> <br> <br> 
                      <button type="button" class="td-btn" value='50'>$50</button>&nbsp;
                      <button type="button" class="td-btn" value='100'>$100</button>
                    </td>
                  </tr>
                  <tr class="tr-footr">
                    <th class="chk-td">Slip Number<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="silp_number" id="cash_payment" size="10" value="" class="cash-txt">
                      </div>
                     </td>
                  </tr>   
                   <tr class="tr-footr">
                    <th class="chk-td">Cash :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="cash_amount" id="debit_payment" size="10" value=" " class="cash-txt"><button class="payment-max-btn" type="button">Max</button>
                      </div>
                    </td>
                  </tr> 
                  <tr class="tr-footr">
                    <th class="chk-td">Card :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="card_amount" id="credit_payment" size="10" value="" class="cash-txt"><button class="payment-max-btn" type="button">Max</button>
                      </div>
                    </td>
                  </tr>    
                  
                  <tr class="tr-footr">
                    <th class="chk-td">Visa :<span>$</span></th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="visa_amount" id="visa_amount" size="10" value=" " class="cash-txt"><button class="payment-max-btn" type="button">Max</button>
                      </div>
                    </td>
                  </tr>     
                </tbody>
              </table>
            </div>
            <div class="form-actions payment-footer" style="">
              <label class="cash-td total-lbl" style="">Total</label>
              <input autocomplete="off" disabled="disabled" id="payments_total" value="$0.00" size="10" class="total-result">
            </div>
          </form>
                        <!-- END FORM-->
        </div>
      </div>
    </div>
	</div>
	</div>

	
	</div>
</div>
<?php echo $footer; ?>

<div id="ajax-modal" class="modal container fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Available Products</h4>
		</div>
		<div class="modal-body">
			Loading........
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
	<!-- CLEAR -->
	<div id="ajax-modal-clear" class="modal fade" tabindex="-1">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Confrimation</h4>
		</div>
		<div class="modal-body">
			<p>Cancel the current Transaction?</p>
		</div>
		<div class="modal-footer form-action">
		<button type="button" id="clearallpos" class="btn btn-success">Yes</button>
		<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>		
		</div>
	</div>