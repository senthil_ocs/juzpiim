<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
<?php echo $sidebar; ?>
<div class="page-content-wrapper">
  <div class="page-content">
		<h3 class="page-title">POS</h3>
      <div class="row">
        <div class="col-md-4">
        <h4>Angle offset and arc</h4>
        <input class="knob" data-angleoffset=-125 data-anglearc=250 data-fgcolor="#000" value="0">
        </div>
      </div>
		<div class="col-md-4">
      <div class="portlet whole-container" style="">
        <div class="portlet-title payment-title" style="">
          <div class="caption" >
            <label class="payment-caption">PAYMENT</label>
          </div>
          <div class="tools">
          </div>
        </div>
        <div class="portlet-body form">

        <!-- BEGIN FORM-->
          <!-- <form action="#">
            <div class="form-body bg-color" style="">
              <table cellspacing="0" cellpadding="0" border="0" width="100%">
                <tbody>
                  <tr>
                    <th class="cash-td">Cash</th>
                    <td>
                      <div class="payment-slider">
                        <input type="text" name="cash_payment" id="cash_payment" size="10" value="0.00" class="cash-txt" style=""><button type="button" class="payment-max-btn">Max</button> 
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td class="quickcash" colspan="2" style="">
                      <button type="button" class="td-btn" id="quickcash-one" value="1">$1</button>&nbsp;
                      <button type="button" class="td-btn" id="quickcash-two" value="5">$5</button>&nbsp;
                      <button type="button" class="td-btn" id="quickcash-three" value="10">$10</button>&nbsp;
                      <button type="button" class="td-btn" id="quickcash-four" value="20">$20</button> <br/> <br/> 
                      <button type="button" class="td-btn" id="quickcash-five" value="50">$50</button>&nbsp;
                      <button type="button" class="td-btn" id="quickcash-six"  value="100">$100</button>
                    </td>
                  </tr>
                  <tr class="tr-footr">
                    <th class="chk-td">Check</th>
                    <td>
                      <div class="payment-slider" >
                        <input type="text" name="cash_payment" id="cash_payment" size="10" value="0.00" class="cash-txt"><button  type="button" class="payment-max-btn">Max</button>
                      </div>
                     </td>
                  </tr>    
                  <tr class="tr-footr">
                    <th class="chk-td">Credit Card</th>
                    <td>
                      <div class="payment-slider" >
                        <input type="text" name="cash_payment" id="credit_card" size="10" value="0.00" class="cash-txt"><button type="button" class="payment-max-btn">Max</button>
                      </div>
                    </td>
                  </tr>    
                  <tr class="tr-footr">
                    <th class="chk-td">Debit Card</th>
                    <td>
                      <div class="payment-slider" >
                        <input type="text" name="cash_payment" id="debit_card" size="10" value="0.00" class="cash-txt"><button type="button" class="payment-max-btn">Max</button>
                      </div>
                    </td>
                  </tr>     
                </tbody>
              </table>
            </div>
            <div class="form-actions payment-footer" style="">
              <label class="cash-td total-lbl" style="">Total</label>
              <input autocomplete="off" disabled="disabled" id="payments_total" value="$0.00" size="10" class="total-result">
            </div>
          </form> -->

          <!-- END FORM-->
        </div>

      </div>
    </div>
	</div>
	</div>


</div>
<?php echo $footer; ?>
<script type="text/javascript">
$(document).ready(function(){
    
  $(".td-btn").click(function() {
    var id = $(this).attr('value'); 
    // alert(id);
    var cashValue=$('#cash_payment').val();
    //alert(cashValue);
    var Result =  parseFloat(cashValue)+parseFloat(id);
    //alert(Result);
    //$('#cash_payment').val(Result);
    $('#credit_card').val(Result);
    $('#payments_total').val(Result);
  });
  var s= 0;
  setInterval(function(){
    $("input.knob").val(s);
      $(".knob").knob({
        'dynamicDraw': true,
        'thickness': 0.2,
        'tickColorizeValues': true,
        'skin': 'tron'
      }); 
      s = s+1;
      if(s>=100){
        s =0;
      }
  },1000);
});
</script>
<script src="view/assets/plugins/jquery-knob/js/jquery.knob.js"></script>
<script src="view/assets/scripts/app.js"></script>
<script src="view/assets/scripts/ui-knob.js"></script>
<script type="text/javascript">

</script>