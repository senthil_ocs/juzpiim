<div class="portlet bordernone">
	<div class="portlet-title portlet-height">
		<div class="caption caption-size">
			<i class="fa fa-star-o fa-5x" style="font-size: 30px;line-height:30px;"></i> Thank You !
		</div>
	</div>
	<p class="adjustment-para"></p>
	<h4 style="margin-left:15px;border-bottom:1px solid #DDDDDD;line-height:32px;">Next Step</h4>
	<p class="sub-para">The transaction has been recorded. Next you probably want to do one of the following:</p>
	<div class="portlet-body bgnone">
		<!--<a href="<?php echo $purchase; ?>" class="icon-btn" id="printin">!-->
		<a href="<?php echo $printpdf; ?>" class="icon-btn" id="printin" target="_blank">
			<i class="glyphicon glyphicon-print"></i>
			<div>Print</div>
		</a>
		<!--<a href="#" class="icon-btn">
			<i class="glyphicon glyphicon-print"></i>
			<div>Print Gift Receipt</div>
		</a>-->
		<!--<a href="<?php echo $sendpdf; ?>" target="_blank" class="icon-btn printin" id="email_popup" >!-->

		<a href="" target="_blank" class="icon-btn printin" id="email_popup" >
			<i class="glyphicon glyphicon-envelope"></i>
			<div>Email Receipt</div>
		</a>

		<a href="<?php echo $newsale; ?>" class="icon-btn">
			<i class="glyphicon glyphicon-usd"></i>
			<div>New Sale</div>
		</a>
	</div>
</div>

<div class="row">
	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 iframe-tag" id="print-body">
		<?php echo $invoiceprint; ?>
	</div>
</div>
<!-- Pop-Up -->
<div id="ajax-modal-email" class="modal fade" tabindex="-1">
	<form method="post" id="email-popup" class="" action="<?php echo $sendpdf; ?>" onsubmit="return dovalidateEmail();">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Email Receipt</h4>
		</div>
		<div class="modal-body">
			<div class="alert" id="success-msg" style="display:none;"></div>
			<label>Enter Email Address</label>
			<input type="text" id="email" name="email" class="form-control" value="<?php echo $_POST['email']?>">
			<div id="er_email" style="color:red;font-size:14px;"></div>
		</div>
		<div class="modal-footer form-action">
			<button type="submit" id="Send" class="btn btn-success">Send</button>
			<button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
		</div>
	</form>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("#email-popup").ajaxForm({
		success:function(response){
		   	//alert(response);
		    if(response=='0'){
				$("#success-msg").removeClass('alert-success');
		        $("#success-msg").html('<p>Sorry the email could not be sent. Please tryagain.</p>');
		        $("#success-msg").addClass('alert-danger').show();
			} else {
				$("#success-msg").removeClass('alert-danger');
		        $("#success-msg").html('<p>Email Sent Successfully</p>');
		        $("#success-msg").addClass('alert-success').show();
		        setTimeout(function(){ $("#ajax-modal-email").modal('hide') }, 2000);
			}
		}
	});
});
</script>
<script type="text/javascript">
function dovalidateEmail() {
	count=0;
	var email=document.getElementById('email');
	if (email.value=="") {
		document.getElementById('er_email').innerHTML="<span id='er_ename'> * Please enter email address</span>";
		count++;
	} else if (email.value.indexOf("@", 0) < 0) {
		document.getElementById('er_email').innerHTML="<span id='er_ename'> * Please enter a valid email address</span>";
		count++;
	} else if (email.value.indexOf(".", 0) < 0) {
		document.getElementById('er_email').innerHTML="<span id='er_ename'> * Please enter a valid email address</span>";
		count++;
	} else if (email.value.indexOf("com", 0) < 0) {
		document.getElementById('er_email').innerHTML="<span id='er_ename'> * Please enter a valid email address</span>";
		count++;
	} else {
		document.getElementById('er_email').innerHTML="";
	}

	if(count!=0) {
	  return false;
	} else {
	  return true;
	}
}
</script>