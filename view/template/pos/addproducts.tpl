		<?php if($show){ ?>
    <h3 class="page-title">POS <small>/ Product List</small></h3>
    <div class="page-bar" style="margin-bottom:0 !important; border-bottom:1px solid #ddd; margin-left: -30px;">
        <ul class="page-breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li>
                  <?php echo $breadcrumb['separator']; ?>
                    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                </li>
                <li>
                  <i class="fa fa-angle-right"></i><a><?php echo 'Product List'; ?></a>
                </li>
            <?php  }?>
        </ul>
        <div class="page-toolbar">
             <div class="btn-group pull-right">
              <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <button type="button" id="backssale" class="btn btn-primary backssale" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-reply"></i><span> CURRENT SALE</span></button>
                </div>
                </div>
            </div>
      </div>
        <div class="innerpage-listcontent-blocks">
        <form id="products_search_forms" action="<?php echo $action; ?>" method="POST">
          <input type="hidden" name="search_form" value='1'>
           <input type="hidden"  id="page_prod" name="page" >
        <div class="portlet bordernone" style="margin-bottom:0 !important">
            <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -30px; padding:16px 0 0 0" >
                <div class="caption">
                  <table class="table orderlist statusstock" style="margin: -6px 0 0 20px;">
                    <tbody>
                      <tr class="filter">
                        <td><input type="text" placeholder="Product Name" name="filter_name" value="<?php echo $filter_name; ?>" class="textbox"></td>
                        <td><input type="text" placeholder="Barcode" name="filter_barcodes" value="<?php echo $filter_barcodes; ?>" class="textbox" /></td>
                        <td> <input type="text" placeholder='Price Range From' name="filter_price" value="<?php echo $filter_price; ?>" class="textbox" /></td>
                        <td><input type="text" placeholder='Price Range To' name="filter_priceto" value="<?php echo $filter_priceto; ?>" class="textbox" /></td>
                      <td><button style="min-height: 36px; border-radius:0 !important; width:100%" type="submit" class="btn btn-zone btn-primary"><i class="fa fa-search"></i> <?php echo ' Search'; ?></button></td>
                    </tr>
                        </tbody>
                    </table>
                        </div>
                    <div class="tools" style="float:left;padding: 0 0 0 35px;">
                        <a style="text-decoration: none; padding:0 0 0 16px; font-weight: bold; color:#3071A9" href="" class="expand">Advanced</a>
                    </div>
                    </div>
                    <?php if (($_POST['filter_vendor'] !="") || ($_POST['filter_brand'] != "") || ($_POST['filter_department'] != '') || ($_POST['filter_category'] != '')){
                      $showHide  = 'display:block;';
                    } else {
                      $showHide  = 'display:none;';
                    } ?>
                    <div class="page-bar portlet-body bgnone" style="display: none;background-color: #eee !important; margin: 0px 0 0 -30px;<?php echo $showHide; ?>">
                     <table class="table orderlist statusstock" style="margin-bottom: 0 !important; margin-left:10px; width: 68%;">
                    <tbody>
                      <tr class="filter">
                      <td>
                       <select name="filter_department" class="textbox" onchange="getCategory(this.value);" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Department</option>
                                <?php if (!empty($department_collection)) { ?>
                                    <?php foreach ($department_collection as $department) { ?>
                                        <?php if ($department['department_id'] == $filter_department) { ?>
                                            <option value="<?php echo $department['department_id']; ?>" selected="selected"><?php echo $department['department_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $department['department_id']; ?>"><?php echo $department['department_name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                      <td>
                        <select name="filter_category" class="textbox" style="min-height: 35px; padding: 7px;">
                            <option value="">Select Category</option>
                            <?php if (!empty($category_collection)) { ?>
                                <?php foreach ($category_collection as $category) { ?>
                                    <?php if ($category['category_id'] == $filter_category) { ?>
                                        <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['category_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </td>
                    <td>
                        <select name="filter_brand" class="textbox" style="min-height: 35px; padding: 7px;">
                                <option value="">Select Brand</option>
                                <?php if (!empty($brand_collection)) { ?>
                                    <?php foreach ($brand_collection as $brand) { ?>
                                        <?php if ($brand['brand_id'] == $filter_brand) { ?>
                                            <option value="<?php echo $brand['brand_id']; ?>" selected="selected"><?php echo $brand['brand_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                     </td>
                    <td>
                      <select name="filter_vendor" class="textbox" style="min-height: 35px; padding: 7px">
                                <option value="">Select Vendor</option>
                                <?php if (!empty($vendor_collection)) { ?>
                                    <?php foreach ($vendor_collection as $vendor) { ?>
                                        <?php if ($vendor['vendor_id'] == $filter_vendor) { ?>
                                            <option value="<?php echo $vendor['vendor_id']; ?>" selected="selected"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $vendor['vendor_id']; ?>"><?php echo $vendor['vendor_name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                     </tr>
                        </tbody>
                    </table>
                    </div>
                     <div style="clear:both; margin: 0 0 15px 0;">
                    </div>
                  </div>
                </form>
              </div>
                <?php } ?>
		<div class="row posrow">
          <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 postbl postbl_new" id="addProductssts">
          <div class="table-responsive" style="border-bottom:none !important">
            <table class="table table-advance table-hover table-borderd" id="addproducts2" >
            <thead >
              <tr>
                <th style="width:150px;"> &nbsp;</th>
                <th class="invntrycode">INVENTORY CODE</th>
                <th class="prodname">Product Name</th>
                <th class="qty">QTY</th>
                <th style="text-align:center;" class="price">PRICE</th>
              </tr>
            </thead>
            <tbody>
               <?php if ($product_collection) { $i=0; ?>

              <?php foreach ($product_collection as $product_collections) { $i++;?>
              <tr style="font-size:14px;"  class="product-result-row" key="<?php echo $product_collections['product_id']; ?>" id="aprow-<?php echo $product_collections['product_id'] ?>">
                <td>
                   <?php if($product_collections['quantity']>0 ){ ?>
                    <button data-id="addProduct-<?php echo $product_collections['product_id'] ?>" class="btn btn-update addProductToCart" name="addProductToCart" type="button"><i class="icon-plus "></i> &nbsp;Add</button>
                    <?php } else{ ?>
                     <p style="color:red">Out of Stock</p>
                   <?php  } ?>
                </td>
                <td class="sku" ><?php echo $product_collections['sku'];?></td>
                <td class="productname-td barcodes"><?php echo $product_collections['name'];?></td>
                <td align="right" class="name"><?php echo $product_collections['quantity']; ?></td>
                <td style="text-align:center;" class="price"><?php echo $product_collections['price']; ?></td>
              </tr>
              <?php } ?>
              <?php } else { ?>
              <tr>
                <td align="center" colspan="5">No Results</td>
              </tr>
              <?php } ?>
            </tbody>
            </table>
            <div class="pagination"><?php echo $pagination; ?></div>
          </div>
        </div>
			  <?php if($show){ ?>
		</div>
  </div>
<script type="text/javascript">
$(document).ready(function(){

  $('#selectAll').on('click',function() {
    if(this.checked){
      $('.checkbox1').each(function(){
        this.checked = true;
      });
    } else {
      $('.checkbox1').each(function(){
        this.checked = false;
      });
    }
  });

  $('.checkbox1').on('click',function() {
    if($('.checkbox1:checked').length == $('.checkbox1').length){
      $('#selectAll').prop('checked',true);
    } else {
      $('#selectAll').prop('checked',false);
    }
  });



    $("#products_search_forms").ajaxForm({
        success:function(out)
        {
          $("#addProductssts").html(out);
        }
     });
  });
</script>
<script type="text/javascript">
function getCategory(v)
{
  $.ajax({
    url:"index.php?route=pos/pos/getCategoryByDept&token=<?php echo $token; ?>",
    type:"POST",
    data:{filter_department:v},
    success:function(out){
      out = JSON.parse(out);
      html = '<option value="">Select Category</option>';
       if(out != '') {
          for(i=0; i <out.length; i++) {
            html += '<option value="' + out[i]['category_id'] + '"';
              if (out[i]['category_id'] == '') {
                  html += ' selected="selected"';
              }
            html += '>' + out[i]['category_name'] + '</option>';
          }
      } else {
           html += '<option value="" selected="selected"> -None- </option>';
      }
          $('select[name=\'filter_category\']').html(html);
  }
});
}
</script>
<?php } ?>