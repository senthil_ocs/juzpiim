<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
	<div class="page-content">
		<h3 class="page-title">POS</h3>
		<div class="row posrow">
			<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 form form-actions top bgpos1" style="height:40px;width:100% ! important;">
       
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb1">
								
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mb13">
			
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mb14">				
					
				</div>
			</div>
			 <!-- Filter Starts  -->
      <div class="innerpage-listcontent-blocks">
            <div class="portlet bordernone" style="margin-bottom:0 !important">
              <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
                <div class="caption" style=""> 
                  <form method="post" name="report_filter" id="report_filter" action="">
                    <input type="hidden" name="type" id="type">                 
                      <table class="table orderlist statusstock" style="margin: -6px 0 0 25px; width: 97%;">
                        <tbody>
                          <tr class="filter">  
                            <td>
                          <input type="text" placeholder='Name / Mobile Number' name="filter_name" value="<?php echo $_POST['filter_name'] ; ?>" class="textbox" />
                            </td> 
                            <td>
                          <input type="text" placeholder='Email' name="filter_email" value="<?php echo $_POST['filter_email'] ; ?>" class="textbox" style="width: 300px;"  />
                          </td>                  
                            <td align="center" colspan="4">
                                <button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>  
                  </div> 
                <div style="clear:both; margin:0 0 15px 0;"></div>
               </form>
              </div>
            </div>
            <div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- Filter Ends -->
            <div class="col-lg-12 col-md-12 col-md-12 col-md-12 postbl" style="width: 100% ! important;">
            <!--<div class="table-responsive">!-->
              <table border="0" class="table table-striped" id="addproducts">
                <thead >
                  <tr>
                    <th style=""> &nbsp;</th>
                    <th class="th-row" style="">NAME</th>
                    <th class="th-row" style="">LAST NAME</th>
                    <th class="th-row" style="">HOME</th>
                    <th class="th-row" style="">WORK</th>
                    <th class="th-row" style="">MOBILE</th>
                    <th class="th-row" style="">EMAIL</th>
                  </tr>
                </thead>
                <tbody>
                   <?php if ($customers) { $i=0; ?>
                        
                      <?php foreach ($customers as $customer) { $i++;?>
                  <tr style="font-size:14px;">
                    <td class="td-row">
                      <button data-id="addProduct-13" class="btn btn-update addProductToCart" name="addProductToCart" type="button"><i class="icon-plus "></i>&nbsp;Attach to Sale</button>
                    </td>
                    <td class="td-row"><?php echo $customer['firstname'];?></td>
                    <td class="td-row"><?php echo $customer['lastname'];?></td>
                    <td class="td-row"><?php echo $customer['home'];?></td>
                    <td class="td-row"><?php echo $customer['work']; ?></td>
                    <td class="td-row"><?php echo $customer['mobile']; ?></td>
                    <td class="td-row"><?php echo $customer['email']; ?></td>
                  <tr>
                  <?php } ?>
                  <?php } else { ?>
                  </tr>
                    <td align="center" colspan="7">No Results</td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
         <!-- </div>!-->
        </div>
		</div>
	</div>
	</div>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
  if (e.keyCode == 13) {
    filter();
  }
});
function filterReport(report_type) {
    document.getElementById('type').value=report_type;
  document.report_filter.submit();
}
</script>
<?php echo $footer; ?>
<style type="text/css">
.th-row{/*width:120px;*/font-size: 14px; background-color: #F8F8F8 ! important;}
.td-row{background-color: white ! important;}
.btn:hover{background-image:none !important;background-color:#3071A9 !important; color: white;}
</style>
