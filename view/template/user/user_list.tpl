<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?php echo $sidebar; ?>
	<div class="page-content-wrapper">
		<div class="page-content">
		<h3 class="page-title">User List</h3>			
			<div class="page-bar">
				<ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
				 <div class="page-toolbar">
                 <div class="btn-group pull-right">
                  <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <a href="<?php echo $insert; ?>"><button type="button" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-plus"></i><span> INSERT</span></button></a>
                <a onclick="$('form').submit();"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-trash-o"></i><span> DELETE</span></button></a>
                <?php } ?>
            			</div>    
                    </div>
                </div>               						
			</div>

			<div class="page-bar innerpage-listcontent-blocks" style="margin-bottom:0 !important;">
        		<div class="portlet bordernone" style="margin-bottom:0 !important">
		          <div class="page-bar portlet-title" style="min-height: 60px;margin: 0px 0 -15px -20px; padding:16px 0 0 0">
		            <div class="caption" style=""> 
		              <!-- <form method="post" name="report_filter" id="report_filter" action=""> -->
		                <input type="hidden" name="type" id="type">                 
		                  <table class="table orderlist statusstock" style="margin: -10px 0 0 25px; width: 97%;">
		                    <tbody>
		                      <tr class="filter">  
		                      	<td>
		              				<input type="text" placeholder='Name' name="filter_name" value="<?php echo $_REQUEST['filter_name'] ; ?>" class="textbox" />
		                        </td> 

		                        <td>
		              				<select name="filter_location_code" id="filter_location_code" class="selectpicker selectdropdown <?php echo $location_code; ?>" >
		              					<option value="0"> Select location </option>
                                    <?php foreach ($location_codes as $location) { ?>
                                        <option value="<?php echo $location['location_code']; ?>" 
                                        	<?php if($_REQUEST['filter_location_code']==$location['location_code']){?> selected="selected" <?php }?>>
                                        	<?php echo $location['location_name']; ?>
                                        		
                                        	</option>
                                    <?php } ?>
                                	</select>
		                   		</td> 

		                        <td align="center" colspan="4">
		                          	<button style="width: 100%; min-height: 36px; border-radius:0 !important;" type="button" onclick="filterReport('getList');" class="btn btn-zone btn-primary">&nbsp;&nbsp;<i class="fa fa-search"></i> <?php echo ' Search'; ?></button>
		                        </td>
		                      </tr>
		                    </tbody>
		                  </table>
		            </div>
		            </div> 
             		<div style="clear:both; margin:0 0 15px 0;"></div>
            <!-- </form> -->
            	</div>
            	</div>

			<div style="clear:both"></div>

				<?php if ($error_warning) { ?>
			<div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
				<button type="button" class="close" data-dismiss="alert"></button>
             </div>
			<?php } ?>
			<?php if ($success) { ?>			
				<div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
					<button type="button" class="close" data-dismiss="alert"></button>
                </div>
			<?php } ?>				 
		<div class="row">				
			<div class="col-md-12">
				<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                <div class="innerpage-listcontent-blocks"> 
				<table class="table orderlist statusstock">
                        <thead>
                        <tr class="heading">
							<td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
							</td>

							<td class="left"><?php if ($sort == 'username') { ?>
							<a href="<?php echo $sort_username; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_username; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_username; ?>"><?php echo $column_username; ?></a>
							<?php } ?>
							</td>

							<td class="left">
								<?php if ($sort == 'location_code') { ?>
									<a href="<?php echo $sort_location; ?>"  class="<?php echo strtolower($order); ?>">Location</a>
								<?php } else { ?>
									<a href="<?php echo $sort_location; ?>">Location</a>
								<?php } ?>
							</td>

							<td class="left"><?php if ($sort == 'status') { ?>
							<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
							<?php } ?>
							</td>

							<td class="left"><?php if ($sort == 'date_added') { ?>
							<a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_date_added; ?></a>
							<?php } else { ?>
							<a href="<?php echo $sort_date_added; ?>"><?php echo $column_date_added; ?></a>
							<?php } ?>
							</td>

							<td class="right"><?php echo $column_action; ?></td>
						</tr>
                        </thead>
                        <tbody>
                            <?php if ($users) { ?>
                            <?php $class = 'odd'; ?>
						<?php foreach ($users as $user) { ?>
						<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
							<tr class="<?php echo $class; ?>">
								<td style="text-align: center;"><?php if ($user['selected']) { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" checked="checked" />
								<?php } else { ?>
								<input type="checkbox" name="selected[]" value="<?php echo $user['user_id']; ?>" />
								<?php } ?></td>
								<td class="left"><?php echo $user['username']; ?></td>
								<td class="left"><?php echo $user['location_name']; ?></td>
								<td class="left"><?php echo $user['status']; ?></td>
								<td class="left"><?php echo $user['date_added']; ?></td>
								<td class="right"><?php foreach ($user['action'] as $action) { ?>
								[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
								<?php } ?>
								</td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
						<td class="center" colspan="5"><?php echo $text_no_results; ?></td>
						</tr>
					<?php } ?>
                        </tbody>
                    </table>
                    </div>
                </form>
                <div class="pagination"><?php echo $pagination; ?></div>
			</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function filterReport() {
    url = 'index.php?route=user/user&token=<?php echo $token; ?>';

    var filter_name = jQuery('input[name=\'filter_name\']').attr('value');
    if (filter_name) {
        url += '&filter_name=' + encodeURIComponent(filter_name);
    }
    var filter_location_code = $( "#filter_location_code option:selected" ).val();
    console.log(filter_location_code);
    if (filter_location_code) {
        url += '&filter_location_code=' +filter_location_code;
    }
    location = url;
}
</script>
<?php echo $footer; ?>
<!-- END NEW CODE -->