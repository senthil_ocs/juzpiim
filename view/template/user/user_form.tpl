<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">User</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div>
            <div style="clear:both"></div>
            <?php if ($error_warning) { ?>
                <div class="alert alert-block alert-danger fade in setting-danger"><?php echo $error_warning; ?><button type="button" class="close" data-dismiss="alert"></button></div>
            <?php } ?> 
            <div style="clear:both"></div>        
        <div class="col-md-12">        	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="username"><?php echo $entry_username; ?><span class="required" aria-required="true">* </span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if($error_username) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>
							 		
                                   <input type="text" name="username" id="username" value="<?php echo $username; ?>" class="textbox requiredborder" placeholder="Enter user name">
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_username) { ?>
                               		<input type="text" name="username" id="username" value="<?php echo $username; ?>" class="textbox" placeholder="Enter user name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="firstname"><?php echo $entry_firstname; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                        		<?php if ($error_firstname) { ?>
									<div class="input-icon right">
									<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Firstname"></i>							 	
								<input type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>" class="textbox requiredborder" placeholder="Enter first name">
								</div>
								<?php } ?>
								<?php if (!$error_firstname) { ?>
								<input type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>" class="textbox" placeholder="Enter first name">
								<?php } ?>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="lastname"><?php echo $entry_lastname; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	 <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="textbox" placeholder="Enter last name" />
                                    
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="email"><?php echo $entry_email; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	 <input type="text" name="email" value="<?php echo $email; ?>" class="textbox" placeholder="Enter Email">
                                    
                                </td>                                
                            </tr>

                            <tr>
                                <td width="20%">
                                    <label for="user_group">Location<span class="required">*</span></label>
                                </td>
                                <td width="80%">

                                <select name="location_code" class="selectpicker selectdropdown <?php echo $location_code; ?>" >
                                    <option value=''>Select Location</option>
                                    <?php foreach ($location_codes as $location) { ?>
                                        <?php if ($location['location_code'] == $location_code) { ?>
                                            <option value="<?php echo  $location['location_code']; ?>" selected="selected"><?php echo $location['location_name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $location['location_code']; ?>" ><?php echo $location['location_name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>

                                </td>
                            </tr>

                            <tr>
                                <td width="20%">
                                	<label for="user_group"><?php echo $entry_user_group; ?></label>
                                </td>
                                <td width="80%">
                                    <select name="user_group_id" class="selectpicker selectdropdown">
									<?php foreach ($user_groups as $user_group) { ?>
										<?php if ($user_group['user_group_id'] == $user_group_id) { ?>
											<option value="<?php echo $user_group['user_group_id']; ?>" selected="selected"><?php echo $user_group['name']; ?></option>
										<?php } else { ?>
											<option value="<?php echo $user_group['user_group_id']; ?>"><?php echo $user_group['name']; ?></option>
										<?php } ?>
									<?php } ?>
								</select>                                    
                                </td>                                
                            </tr>

                             <tr>
                                <td width="20%">
                                	<label for="password"><?php echo $entry_password; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                                   
                                	<?php if ($error_password) { ?>
									<div class="input-icon right">
                                        <?php if($password == '') { ?>
									<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Password"></i>
                                    <?php } elseif(count($password) < 8) { ?>
                                    <i class="fa fa-warning tooltips required" data-container="body" data-original-title="Should be Minimum 8 characters"></i>
                                    <?php } ?>						
                                    <input type="password" name="password" value="<?php echo $password; ?>" class="textbox requiredborder" placeholder="Enter password" id="password" /> 
                                    </div> 
                                    <?php } ?>
                                    <?php if (!$error_password) { ?>
                                    <input type="password" name="password" value="<?php echo $password; ?>" class="textbox" placeholder="Enter password" id="password" /> 
                                    <?php } ?>                    
                                </td>                                
                            </tr>
                             <tr>
                                <td width="20%">
                                	<label for="confirm"><?php echo $entry_confirm; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                	<?php if ($error_confirm) { ?>
									<div class="input-icon right">
									<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Confrim Passowrd"></i>
                                	<input type="password" name="confirm" value="<?php echo $confirm; ?>" class="textbox requiredborder"placeholder="Enter confirm password"/>
								<?php } ?>
								<?php if (!$error_confirm) { ?>
								<input type="password" name="confirm" value="<?php echo $confirm; ?>" class="textbox" placeholder="Enter confirm password"/>
								<?php } ?>
								</div>				   	                                                                 
                                </td>                                
                            </tr>
                            <!-- <tr>
                                <td width="20%">
                                    <label for="sales_agent">Sales Agent</label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <input type="checkbox" name="sales_agent" id="sales_agent" <?php echo $sales_agent == 1 ? 'checked' : ''; ?>>
                                </td>
                            </tr> -->
                            <tr>
							<td width="20%">
								<label for="status"><?php echo $entry_status; ?></label>
							</td>
							<td width="80%">
								<select name="status" class="selectpicker selectdropdown">
									<?php if ($status) { ?>
										<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
										<option value="0"><?php echo $text_disabled; ?></option>
									<?php } else { ?>
										<option value="1"><?php echo $text_enabled; ?></option>
										<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
                        </tbody>
                    </table>
                </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>