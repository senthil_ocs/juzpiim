<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">User Group</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
             <div class="row">
             <div style="clear:both"></div>      

        <div class="col-md-12">
        			<div class="innerpage-listcontent-blocks">                	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>							 		
                                   <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                    <label for="name"><?php echo 'Access'; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%">         
                                   <input type="checkbox" name="accesstype[]" class="posaccess" <?php if(in_array(1, $accesstype)){ ?> checked="checked" <?php } ?>  value="1" >POS
                                   <input type="checkbox" name="accesstype[]" class="adminaccess" value="2" <?php if(in_array(2, $accesstype) || empty($accesstype)){ ?> checked="checked" <?php } ?> >ADMIN                
                                </td>
                            </tr>
                            <tr id="adminaccessrow">
                                <td width="20%">
                                	<label for="access"><?php echo 'Permission (Admin)'; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                    <div class="row">
                                       
                                <?php foreach ($permissions as $key => $value){ ?>
                                    <div class="col-lg-6 matop">
                                        <div class="portlet " >
                                            <div class="portlet-title">
                                            <div class="caption">
                                                <?php echo ucfirst($key); ?>
                                            </div>
                                            <div class="tools">
                                                <input type="checkbox" id="<?php echo $key ?>-read" class="readall">
                                                <label for="<?php echo $key ?>-read">Read</label>
                                                <input type="checkbox" id="<?php echo $key ?>-modify" class="modifyall">
                                                <label for="<?php echo $key ?>-modify">Modify</label>
                                            </div> 
                                            </div>
                                            <div class="portlet-body" id="<?php echo $key ?>">
                                                <?php for($i=0; $i<count($value);$i++) { ?>
                                                <div class="report-master pos" style="width:100% !important">
                                                    <span><b><?php echo $value[$i];  ?></b></span>
                                                    <div style="float:right;">
                                                    <input style="margin-left:-10px !important" class="read <?php echo $key ?>" id="r-<?php echo $value[$i];  ?>" type="checkbox" name="permission[access][]" value="<?php echo trim($value[$i]); ?>" <?php if(in_array(trim($value[$i]), $access)){ ?> checked="checked" <?php }?>>&nbsp;<label for="r-<?php echo $value[$i];  ?>"  >Read</label>
                                                    <input class="modify <?php echo $key ?>" style="margin-left:-10px !important" type="checkbox" id="w-<?php echo trim($value[$i]);  ?>"  name="permission[modify][]" value="<?php echo trim($value[$i]); ?>" <?php if(in_array(trim($value[$i]), $modify)){ ?> checked="checked" <?php }?>>&nbsp;<label for="w-<?php echo $value[$i];  ?>" >Modify</label>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                <?php } ?>
                            </div>
                                </td>                                
                            </tr> 
                            <tr id="posaccessrow">
                                <td width="20%">
                                	<label for="modify"><?php echo 'Permission (POS)'; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                		<div class="row">
                                       
                                <?php foreach ($pos_permissions as $key => $value){ 
                                    $key ="POS "//.$key?>
                                    <div class="col-lg-6 matop">
                                        <div class="portlet " >
                                            <div class="portlet-title">
                                            <div class="caption">
                                                <?php echo ucfirst($key); ?>
                                            </div>
                                            <div class="tools">
                                                <input type="checkbox" id="<?php echo $key ?>-read" class="readall">
                                                <label for="<?php echo $key ?>-read">Read</label>
                                                <input type="checkbox" id="<?php echo $key ?>-modify" class="modifyall">
                                                <label for="<?php echo $key ?>-modify">Modify</label>
                                            </div> 
                                            </div>
                                            <div class="portlet-body" id="<?php echo $key ?>">
                                                <?php for($i=0; $i<count($value);$i++) { ?>
                                                <div class="report-master pos" style="width:100% !important">
                                                    <span><b><?php echo $value[$i];  ?></b></span>
                                                    <div style="float:right;">
                                                    <input style="margin-left:-10px !important" class="read <?php echo $key ?>" data-ref="" id="r-<?php echo $value[$i];  ?>" type="checkbox" name="permission[access][]" value="<?php echo trim($value[$i]); ?>" <?php if(in_array(trim($value[$i]), $access)){ ?> checked="checked" <?php }?>>&nbsp;<label for="r-<?php echo $value[$i];  ?>"  >Read</label>
                                                    <input class="modify" style="margin-left:-10px !important" type="checkbox" id="w-<?php echo trim($value[$i]);  ?>" name="permission[modify][]" value="<?php echo trim($value[$i]); ?>" <?php if(in_array(trim($value[$i]), $modify)){ ?> checked="checked" <?php }?>>&nbsp;<label for="w-<?php echo $value[$i];  ?>" >Modify</label>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                <?php } ?>
                            </div>		                                 
                                </td>                                
                            </tr>                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>