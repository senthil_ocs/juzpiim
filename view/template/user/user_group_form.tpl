<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	<?php if ($error_warning) { ?>
		<div class="warning"><?php echo $error_warning; ?></div>
	<?php } ?>
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">User Group</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
             <div class="row">
             <div style="clear:both"></div>         
        <div class="col-md-12">
        			<div class="innerpage-listcontent-blocks">                	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<?php if($error_name) { ?>
	                                	<div class="input-icon right">
										<i class="fa fa-warning tooltips required" data-container="body" data-original-title="Please Enter Name"></i>							 		
                                   <input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox requiredborder" placeholder="Enter your name">
                               		</div>
                               		<?php } ?>
                               		<?php if(!$error_name) { ?>
                               		<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name">
                               		<?php } ?>						 
                                </td>
                            </tr>
                            <tr>
                                <td width="20%">
                                	<label for="access"><?php echo $entry_access; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                        		<div id="scrollbox_0" class="scroller group_padding" style="height:350px">
									<?php $class = 'odd'; ?>
									<?php foreach ($permissions as $permission) { ?>
										<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                        <div class="col-lg-4 col_padding">
										<div class="<?php echo $class; ?>">
											<?php if (in_array($permission, $access)) { ?>
												<input type="checkbox" class="accper" name="permission[access][]" value="<?php echo $permission; ?>" checked="checked" />
												<?php echo $permission; ?>
											<?php } else { ?>
												<input type="checkbox" class="accper" name="permission[access][]" value="<?php echo $permission; ?>" />
											<?php echo $permission; ?>
											<?php } ?>
										</div>
                                    </div>
									<?php } ?>
									<span>
									<a class="Auser_sel" id="#scrollbox_0" onclick="$('#scrollbox_0 :checkbox').attr('checked', 'true');">
									Select All</a>
									/
									<a class="Ruser_sel" id="#scrollbox_0" onclick="$('#scrollbox_0 :checkbox').removeAttr('checked');">
									Unselect All</a>
									</span>
								</div>
                                </td>                                
                            </tr> 
                            <tr>
                                <td width="20%">
                                	<label for="modify"><?php echo $entry_modify; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                		<div id="scrollbox_0" class="scroller group_padding" style="height:350px">
									<?php $class = 'odd'; ?>
									<?php foreach ($permissions as $permission) { ?>
										<?php $class = ($class == 'even' ? 'odd' : 'even'); ?>
                                         <div class="col-lg-4 col_padding">
										<div class="<?php echo $class; ?>">
											<?php if (in_array($permission, $modify)) { ?>
												<input type="checkbox" class="accpet" name="permission[modify][]" value="<?php echo $permission; ?>" checked="checked" />
												<?php echo $permission; ?>
											<?php } else { ?>
												<input type="checkbox" class="accpet" name="permission[modify][]" value="<?php echo $permission; ?>" />
												<?php echo $permission; ?>
											<?php } ?>
										</div>
                                    </div>
									<?php } ?>
									<span>
									<a class="Buser_sel" id="#scrollbox_0" onclick="$('#scrollbox_0 :checkbox').attr('checked', 'true');">
									Select All</a>
									/
									<a class="Cuser_sel" id="#scrollbox_0" onclick="$('#scrollbox_0 :checkbox').removeAttr('checked');">
									Unselect All</a>
									</span>
								</div>			                                 
                                </td>                                
                            </tr>                           
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>