<?php echo $header; ?>
<div class="clearfix">
</div>
<div class="page-container">
  <?php echo $sidebar; ?>
<div class="page-content-wrapper">
	
     <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="formID">
        <div class="page-content" >
            <h3 class="page-title">User Group Permission set</h3>           
            <div class="page-bar" >
                <ul class="page-breadcrumb">
                    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                        <li>
                        	<?php echo $breadcrumb['separator']; ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                        </li>
                    <?php  }?>
                    
				</ul>
                 <div class="page-toolbar">
                <div class="btn-group pull-right">
                <div class="btn-group tabletools-dropdown-on-portlet" style="margin:3px -20px 0 0  !important">
                <?php if($this->user->hasPermission('modify', $route)) { ?>
                <button type="submit" title="Save" class="btn btn-primary" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
                <?php } ?>
                <a href="<?php echo $cancel; ?>"><button type="button" class="btn btn-danger" style="margin: 5px 5px 0 0; border-radius: 4px"><i class="fa fa-times"></i> <?php echo $button_cancel; ?></button></a></div>    
                    </div>
                </div>                            
            </div> 
             <div class="row">
                <div style="clear:both"></div>  
               <div class="col-md-12">
                    <?php if ($error_warning) { ?>
                    <div class="alert alert-block alert-danger fade in setting-success"><?php echo $error_warning; ?>
                        <button type="button" class="close" data-dismiss="alert"></button>
                     </div>
                    <?php } ?>
                    <?php if ($success) { ?>            
                        <div class="alert alert-block alert-success fade in setting-success"><?php echo $success; ?>
                            <button type="button" class="close" data-dismiss="alert"></button>
                        </div>
                    <?php } ?>
              </div>
             <div style="clear:both"></div>      

        <div class="col-md-12">
        			<div class="innerpage-listcontent-blocks">                	
                    <table class="table orderlist">                       
                        <tbody>
                            <tr>
                                <td width="20%">
                                	<label for="name"><?php echo $entry_name; ?><span class="required">*</span></label>
                                </td>
                                <td width="80%" class="order-nopadding">                          								
                                	<input type="text" name="name" id="name" value="<?php echo $name; ?>" class="textbox" placeholder="Enter your name" readonly>
                               		
                                </td>
                            </tr>
                            <tr id="posaccessrow">
                                <td width="20%" style="vertical-align: top;">
                                    <label for="modify"><?php echo 'Permission (Reports)'; ?></label>
                                </td>
                                <td width="80%" class="order-nopadding">
                                        <div class="row">
                                <?php foreach ($reports_permissions as $key => $value){ 
                                    $key =$key?>
                                    <div class="col-lg-7 matop">
                                        <div class="portlet " >
                                            <div class="portlet-title">
                                            <div class="caption">
                                                <?php echo ucfirst($key); ?>
                                            </div>
                                            <div class="tools">
                                                <input type="checkbox" id="<?php echo $key ?>-read" class="readall">
                                                <label for="<?php echo $key ?>-read">Allow Access</label>
                                              
                                            </div> 
                                            </div>
                                            <div class="portlet-body" id="<?php echo $key ?>">
                                                <?php for($i=0; $i<count($value);$i++) { ?>
                                                <div class="report-master pos" style="width:100% !important">
                                                    <span><b><?php echo $value[$i]['name'];  ?></b></span>
                                                    <div style="float:right;">
                                                    <input style="margin-left:-10px !important" class="read <?php echo $key ?>" data-ref="" id="r-<?php echo $value[$i]['id'];  ?>" type="checkbox" name="permission[access][]" value="<?php echo trim($value[$i]['id']); ?>" <?php if(in_array(trim($value[$i]['id']), $access)){ ?> checked="checked" <?php }?>>&nbsp;<label for="r-<?php echo $value[$i]['id'];  ?>"  >Allow</label>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                <?php } ?>
                                 <?php foreach ($reports_permissions_pos as $key => $value){ 
                                    $key =$key?>
                                    <div class="col-lg-5 matop">
                                        <div class="portlet " >
                                            <div class="portlet-title">
                                            <div class="caption">
                                                <?php echo 'POS Section'; //ucfirst($key); ?>
                                            </div>
                                            <div class="tools">
                                                <input type="checkbox" id="<?php echo $key ?>-read" class="readall">
                                                <label for="<?php echo $key ?>-read">Allow Access</label>
                                              
                                            </div> 
                                            </div>
                                            <div class="portlet-body" id="<?php echo $key ?>">
                                                <?php for($i=0; $i<count($value);$i++) { ?>
                                                <div class="report-master pos" style="width:100% !important">
                                                    <span><b><?php echo $value[$i]['name'];  ?></b></span>
                                                    <div style="float:right;">
                                                    <input style="margin-left:-10px !important" class="read <?php echo $key ?>" data-ref="" id="r-<?php echo $value[$i]['id'];  ?>" type="checkbox" name="permission[access_pos][]" value="<?php echo trim($value[$i]['id']); ?>" <?php if(in_array(trim($value[$i]['id']), $access_pos)){ ?> checked="checked" <?php }?>>&nbsp;<label for="r-<?php echo $value[$i]['id'];  ?>"  >Allow</label>
                                                    </div>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>  
                                    </div>
                                <?php } ?>
                            </div>                                       
                                </td>                                
                            </tr>      
                           
                           
                                                     
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        	</form>
         </div>
    </div>
</div>
<?php echo $footer; ?>