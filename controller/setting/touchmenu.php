<?php 
class ControllerSettingTouchMenu extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/touchmenu');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/touchmenu');
		$this->getList();
		
	}
	public function insert() {
		$this->language->load('setting/touchmenu');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/touchmenu');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			//printArray($this->request->post); exit;
			$cciid = $this->model_setting_touchmenu->addTouchMenu($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();		
	}
	public function update() {
		$this->language->load('setting/touchmenu');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/touchmenu');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_setting_touchmenu->editTouchMenu($this->request->post,$this->request->get['id']);
			
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();
	}
	public function delete() {
    	$this->language->load('setting/touchmenu');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/touchmenu');
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_setting_touchmenu->deleteTouchMenu($id);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
		
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$this->redirect($this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
  	}
	public function getList(){
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->get['filter_menu_code'])) {
			$url.= '&touch_menu_code='.$this->request->get['filter_menu_code'];
		}
		if (isset($this->request->post['filter_sort_code'])) {
			$url.= '&sort_code='.$this->request->post['filter_sort_code'];
		} 
		
		

		if (isset($this->request->get['filter_menu_code'])) {
			$filter_menu_code = $this->request->get['filter_menu_code'];
		} else {
			$filter_menu_code ="";
		}
		if (isset($this->request->get['filter_sort_code'])) {
			$filter_sort_code = $this->request->get['filter_sort_code'];
		} else {
			$filter_sort_code ="";
		}
		
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/touchmenu/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/touchmenu/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['touchmenu'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_menu_code' => $filter_menu_code,
			'filter_sort_code' => $filter_sort_code,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$customer_total = $this->model_setting_touchmenu->getTotalTouchMenu($data);
		
		$results = $this->model_setting_touchmenu->getTouchMenu($data);
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/touchmenu/update', 'token=' . $this->session->data['token'] . '&id=' . $result['touch_menu_code'] . $url, 'SSL')
			);
			if($result['status'] == 0){
				$strStatus = $this->language->get('text_disabled');
			} else {
				$strStatus = $this->language->get('text_enabled');
			}
			$this->data['touchmenu'][] = array(
				'touch_menu_code'	=> $result['touch_menu_code'],
				'touch_menu_description'	=> $result['touch_menu_description'],
				'status' 			=> $strStatus,
				'sort_code' 		=> $result['sort_code'],
				'createdby'      	=> $result['createdby'],
				'createdon'			=> $result['createdon']->format('Y-m-d'),
				'selected'          => isset($this->request->post['selected']) && in_array($result['touch_menu_code'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Touch Menu'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['sort_touch_menu_code'] = $this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . '&sort=touch_menu_code' . $url, 'SSL');
		$this->data['sort_sort_code'] = $this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . '&sort=sort_code' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['route'] = 'setting/touchmenu';

		$this->data['token'] = $this->session->data['token'];
		
		$this->template = 'setting/touchmenu_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zipcode'] = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_attn'] = $this->language->get('entry_attn');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_gst'] = $this->language->get('entry_gst');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['touch_menu_code'])) {
			$this->data['error_touch_menu_code'] = $this->error['touch_menu_code'];
		} else {
			$this->data['error_touch_menu_code'] = '';
		}		
		if (isset($this->error['sort_code'])) {
			$this->data['error_sort_code'] = $this->error['sort_code'];
		} else {
			$this->data['error_sort_code'] = '';
		}
	
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Touch Menu List'),
			'href'		=> $this->url->link('setting/touchmenu', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$type = $this->request->get['type'];
		if (!isset($this->request->get['id'])) {
		   	$this->data['action'] = $this->url->link('setting/touchmenu/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		    	$this->data['action'] = $this->url->link('setting/touchmenu/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('setting/touchmenu', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_setting_touchmenu->getTouchMenuById($this->request->get['id']);
			$this->data['editForm'] = '1';
		}		
		if (isset($this->request->post['touch_menu_code'])) {
			$this->data['touch_menu_code'] = $this->request->post['touch_menu_code'];
		} elseif (!empty($customer_info)) {
			$this->data['touch_menu_code'] = $customer_info['touch_menu_code'];
		} else {
			$this->data['touch_menu_code'] = '';
		}
		if (isset($this->request->post['touch_menu_description'])) {
			$this->data['touch_menu_description'] = $this->request->post['touch_menu_description'];
		} elseif (!empty($customer_info)) {
			$this->data['touch_menu_description'] = $customer_info['touch_menu_description'];
		} else {
			$this->data['touch_menu_description'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($customer_info)) {
			$this->data['status'] = $customer_info['status'];
		} else {
			$this->data['status'] = '';
		}
		if (isset($this->request->post['sort_code'])) {
			$this->data['sort_code'] = $this->request->post['sort_code'];
		} elseif (!empty($customer_info)) {
			$this->data['sort_code'] = $customer_info['sort_code'];
		} else {
			$this->data['sort_code'] = $this->model_setting_touchmenu->getLastTouchMenuById();
		}
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/touchmenu';
		$this->template = 'setting/touchmenu_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/touchmenu')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$duplication = $this->model_setting_touchmenu->getTouchMenuByMenuCode($this->request->post['touch_menu_code'],$this->request->get['id']);
		//printArray($duplication);
		if($duplication['cnt'] > 0){
			$this->error['warning'] = $this->language->get('Touch Menu Code Already Exists!');
		}
		//printArray($duplication); exit;
		if (empty($this->request->post['touch_menu_code'])) {
			$this->error['touch_menu_code'] = $this->language->get('Please Enter Touch Menu Code');
		}
		/*if (empty($this->request->post['sort_code'])) {
			$this->error['sort_code'] = $this->language->get('Please Enter Sort Code');
		}*/
		//
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function updateSortOrder(){
		$code 		= $this->request->post['itemId'];
		$sort_code  = $this->request->post['newvalue'];
		$this->load->model('setting/touchmenu');
		
		if(isset($code) && isset($sort_code)){
			$this->model_setting_touchmenu->updateSortOrder(trim($code),$sort_code);
			echo 1;
		}else{
			echo 0;
		}
	}
	
}
?>