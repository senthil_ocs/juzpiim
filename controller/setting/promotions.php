<?php 
class ControllerSettingPromotions extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('setting/promotions');
		$this->document->setTitle('Promotions');
		$this->load->model('setting/promotions');
		$this->getList();
		
	}

	public function insert() {

		$this->language->load('setting/promotions');
		$this->document->setTitle('Promotions');
		$this->load->model('setting/promotions');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            
            $data = $this->request->post;
            // printArray($data); die;
            
			$term_id = $this->model_setting_promotions->addPromotions($data);
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if($term_id == flase){
				$this->session->data['success'] = 'Promotion code exists';
				$this->redirect($this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
				$this->session->data['success'] = 'Promotion Added success';
				$this->redirect($this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}		
		}
		$this->getForm();		
	}


	public function update() {

		$this->language->load('setting/promotions');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/promotions');
		
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			

			$this->model_setting_promotions->editTerminal($this->request->post,$this->request->get['promotion_code']);
			
			$this->session->data['success'] ='Promotion Update success';

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
			
		}else{
			$this->data['page_action'] ='edit';
		}

		$this->getForm();
	}

	public function delete() {
    	$this->language->load('setting/promotions');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/promotions');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $terminal_code) {
				$this->model_setting_promotions->deleteTerminal($terminal_code);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}

  	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/purchase');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$_productDetails           = $this->model_transaction_purchase->getProductByName($this->data);

			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
				$str = $var;
			}
			echo $str;
	}


	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
			$this->data['filter_date_to'] = $filter_date_to;
		} else {
			$filter_date_to = null;
			$this->data['filter_date_to'] = '';

		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
			$this->data['filter_date_from'] = $filter_date_from;
		} else {
			$filter_date_from = null;
			$this->data['filter_date_from'] = '';
		}
		if (isset($this->request->post['filter_location'])) {
			$filter_location = $this->request->post['filter_location'];
		} else {
			$filter_location = $this->session->data['location_code'];
		}
		if (isset($this->request->post['filter_sku'])) {
			$filter_sku = $this->request->post['filter_sku'];
			
		} else {
			$filter_sku = '';

		}
		$this->data['filter_sku'] = $filter_sku;
		$this->data['filter_location'] = $filter_location;

		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_terminal_name'])) {
			$url.= '&filter_terminal_name='.$this->request->post['filter_terminal_name'];
		}
		
		if (isset($this->request->post['filter_terminal_name'])) {
			$filter_terminal_name = $this->request->post['filter_terminal_name'];
		} else {
			$filter_terminal_name ="";
		}
		
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/promotions/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/promotions/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 $this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['terminals'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_sku'		=> $filter_sku,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_terminal_name' => $filter_terminal_name
		);
		
		 $promotions_total = $this->model_setting_promotions->getTotalPromotions($data);

		$results = $this->model_setting_promotions->getPromotions($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/promotions/view', 'token=' . $this->session->data['token'] . '&promotion_code=' . $result['PromotionCode'] . $url, 'SSL'),
				'delete' => $this->url->link('setting/promotions/promotionsDelete', 'token=' . $this->session->data['token'] . '&promotion_code=' . $result['PromotionCode'] . $url, 'SSL'),
				'edit' => $this->url->link('setting/promotions/promotionsEdit', 'token=' . $this->session->data['token'] . '&promotion_code=' . $result['PromotionCode'] . $url, 'SSL')
			);
			$this->load->model('setting/location');
			$this->data['location_list'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
			$this->data['terminals'][] = array(
				'PromotionCode'			=> $result['PromotionCode'],
				'Description'      	    => $result['Description'],
				'FromDate'			    => date_format($result['FromDate'],"d/m/Y"),
				'ToDate'			    => date_format($result['ToDate'],"d/m/Y"),
				'FromTime'		        => $result['FromTime'],
				'ToTime'			    => $result['ToTime'],
				'PromotionPerc'		    => $result['PromotionPerc'],
				'PromotionPrice'		=> $result['PromotionPrice'],
				'CreatedBy'		        => $result['CreatedBy'],				
				'createdon'				=> date_format($result['Createdon'],"d/m/Y"),
				'ModyfiedBy'		    => $result['ModyfiedBy'], 
				'ModyfiedOn'			=> date_format($result['ModyfiedOn'],"d/m/Y"),
				'selected'          	=> isset($this->request->post['selected']) && in_array($result['PromotionCode'], $this->request->post['selected']),
				'action'            	=> $action
			);
		}
		//print_r($result);exit();
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Promotion List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		//$this->data['sort_terminal_name'] = $this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . '&sort=terminal_name' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $promotions_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/promotions_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}

	public function promotionsEdit()
	{
		$this->load->model('setting/promotions');
		if($this->request->get['promotion_code']){
			$this->language->load('setting/promotions');

			$this->document->setTitle($this->language->get('heading_title'));

			$this->data['page_action'] ='edit';
			$this->getForm();

		}
		
	}
	public function getForm() {
		$this->load->model('setting/promotions');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_terminal_name'] = $this->language->get('entry_terminal_name');
		$this->data['entry_system_name'] = $this->language->get('entry_system_name');
		$this->data['entry_cash_machine'] = $this->language->get('entry_cash_machine');
		$this->data['entry_cash_machine_port'] = $this->language->get('entry_cash_machine_port');
		$this->data['entry_coin_machine'] = $this->language->get('entry_coin_machine');
		$this->data['entry_coin_machine_port'] = $this->language->get('entry_coin_machine_port');
		$this->data['entry_Cust_display'] = $this->language->get('entry_Cust_display');
		$this->data['entry_Cust_Display_Port'] = $this->language->get('entry_Cust_Display_Port');
		$this->data['entry_Cust_Display_Type'] = $this->language->get('entry_Cust_Display_Type');
		$this->data['entry_Cust_Display_Msg1'] = $this->language->get('entry_Cust_Display_Msg1');
		$this->data['entry_Cust_Display_Msg2'] = $this->language->get('entry_Cust_Display_Msg2');
		$this->data['entry_speaker_enabled'] = $this->language->get('entry_speaker_enabled');
		$this->data['entry_weighingscale_enabled'] = $this->language->get('entry_weighingscale_enabled');
		$this->data['entry_weighingscale_port'] = $this->language->get('entry_weighingscale_port');
		$this->data['entry_last_sync_date'] = $this->language->get('entry_last_sync_date');
		$this->data['button_add_barcode'] = $this->language->get('button_add_barcode');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_image'] = $this->language->get('button_add_image');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}			


		if (isset($this->error['promotion_code'])) {
			$this->data['error_promotion_code'] = $this->error['promotion_code'];
		} else {
			$this->data['error_promotion_code'] = '';
		}

		if (isset($this->error['from_date'])) {
			$this->data['error_from_date'] = $this->error['from_date'];
		} else {
			$this->data['error_from_date'] = '';
		}

		if (isset($this->error['to_date'])) {
			$this->data['error_to_date'] = $this->error['to_date'];
		} else {
			$this->data['error_to_date'] = '';
		}

		if (isset($this->error['from_time'])) {
			$this->data['error_from_time'] = $this->error['from_time'];
		} else {
			$this->data['error_from_time'] = '';
		}

		if (isset($this->error['to_time'])) {
			$this->data['error_to_time'] = $this->error['to_time'];
		} else {
			$this->data['error_to_time'] = '';
		}

		if (isset($this->error['promotion_prescription'])) {
			$this->data['error_promotion_prescription'] = $this->error['promotion_prescription'];
		} else {
			$this->data['error_promotion_prescription'] = '';
		}

		if (isset($this->error['promotion_price'])) {
			$this->data['error_promotion_price'] = $this->error['promotion_price'];
		} else {
			$this->data['error_promotion_price'] = '';
		}

	    
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Promotions List'),
			'href'		=> $this->url->link('setting/promotions', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['promotion_code'])) {
			if(	$this->data['page_action'] == 'edit'){
				$this->data['action'] = $this->url->link('setting/promotions/edit', 'token=' . $this->session->data['token'] . $url, 'SSL');
			}else{
				$this->data['action'] = $this->url->link('setting/promotions/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			}
		   
		} else {
		
			$this->data['action'] = $this->url->link('setting/promotions/update', 'token=' . $this->session->data['token'] . '&promotion_code=' . $this->request->get['promotion_code'] . $url, 'SSL');
		}	

		$this->data['cancel'] = $this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL');

		
		
		if (isset($this->request->get['promotion_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$terminal_info = $this->model_setting_promotions->getTerminal($this->request->get['promotion_code']);
		}
		
		if($this->data['page_action'] == 'edit'){
			$terminal_info = $this->model_setting_promotions->viewPromotions($this->request->get['promotion_code']);
			$PromotionSkuInfo['PromotionSkuInfo'] = $this->model_setting_promotions->viewPromotionSku($this->request->get['promotion_code']);
			$terminal_info = array_merge($terminal_info,$PromotionSkuInfo);
		}

		if(isset($this->request->post['promotion_code'])){
			$this->data['promotion_code'] =$this->request->post['promotion_code'];
		} elseif (!empty($terminal_info)) {
			$this->data['promotion_code'] = $terminal_info['PromotionCode'];
		} else {			
			$this->data['promotion_code'] ='';
		}	
		
		if(isset($this->request->post['description'])){
			$this->data['description'] =$this->request->post['description'];
		} elseif (!empty($terminal_info)) {
			$this->data['description'] = $terminal_info['Description'];
		} else {			
			$this->data['description'] ='';
		}

		if(isset($this->request->post['from_date'])) {
			$this->data['from_date'] = $this->request->post['from_date'];
		} elseif(!empty($terminal_info)) {
			$this->data['from_date'] = $terminal_info['FromDate'];
		} else {
			$this->data['from_date'] == '';
		}

		if (isset($this->request->post['to_date'])) {
			$this->data['to_date'] = $this->request->post['to_date'];
		} elseif (!empty($terminal_info)) {
			$this->data['to_date'] = $terminal_info['ToDate'];
		} else {
			$this->data['to_date'] = '';
		}		
		
		if (isset($this->request->post['from_time'])) {
			$this->data['from_time'] = $this->request->post['from_time'];
		} elseif (!empty($terminal_info)) {
			$this->data['from_time'] =$terminal_info['FromTime'];
		} else {
			$this->data['from_time'] = '';
		}

		if (isset($this->request->post['to_time'])) {
			$this->data['to_time'] = $this->request->post['to_time'];
		} elseif (!empty($terminal_info)) {
			$this->data['to_time'] = $terminal_info['ToTime'];
		} else {
			$this->data['to_time'] = '';
		}

		if (isset($this->request->post['promotion_prescription'])) {
			$this->data['promotion_prescription'] = $this->request->post['promotion_prescription'];
		} elseif (!empty($terminal_info)) {
			$this->data['promotion_prescription'] = $terminal_info['PromotionPerc'];
		} else {
			$this->data['promotion_prescription'] = '';
		}
		if (isset($this->request->post['promotion_price'])) {
			$this->data['promotion_price'] = $this->request->post['promotion_price'];
		} elseif (!empty($terminal_info) && $terminal_info['PromotionPrice']>0) {
			$this->data['promotion_price'] = $terminal_info['PromotionPrice'];
		} else {
			$this->data['promotion_price'] = '';
		}

		if (isset($this->request->post['day'])) {
			$this->data['day'] = $this->request->post['day'];
		} elseif (!empty($terminal_info)) {
			$this->data['day'] = $terminal_info['day'];
			$Days = explode(',', $this->data['day']);
			$this->data['day'] = $Days;
		} else {
			$this->data['day'] = '';
		}
			
		if (isset($this->request->post['PromotionSkuInfo'])) {
			$this->data['PromotionSkuInfo'] = $this->request->post['PromotionSkuInfo'];
		} elseif (!empty($terminal_info)) {
			$this->data['PromotionSkuInfo'] = $terminal_info['PromotionSkuInfo'];
		} else {
			$this->data['PromotionSkuInfo'] = '';
		}
		if (isset($this->request->post['location'])) {
			$this->data['location_code'] = $this->request->post['location'];
		} elseif (!empty($terminal_info)) {
			$this->data['location_code'] = $this->model_setting_promotions->getpromationLocations($terminal_info['PromotionCode']);
			$this->data['location_code'] = array_column($this->data['location_code'], 'location_code');
		} else {
			$this->data['location_code'] = '';
		}
		$this->load->model('setting/location');
		$this->data['locations'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
		$this->data['user_location_code']  = $this->session->data['location_code'];
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		// Category
		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();


		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/promotions';
		if($this->data['page_action'] == 'edit'){
			$this->template = 'setting/promotions_edit_form.tpl';
		}else{
			$this->template = 'setting/promotions_form.tpl';
		}
		

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	
	public function ajaxGetPromotionProductsList(){

		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');

		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];		
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];		
		}

		$filter_location = $this->session->data['location_code'];
		$data = array(
			'filter_department'	  => $filter_department, 
			'filter_category'	  => $filter_category,
			'filter_location_code'	  => $filter_location,
			'filter_name'            =>$filter_name		
		);
		//$_REQUEST['promotion_prescription'] = 5;
		$promation_code = $_REQUEST['promotion_code'];
		$this->data['data'] = $data;
		$results = $this->model_inventory_inventory->getProductsReport($data);
        //print_r($results);// exit;
		$Existpromotion_code = $this->model_inventory_inventory->getpromationSKU($promation_code);

		$Existpromotion_codes = array_column($Existpromotion_code,'sku');
		foreach ($results as $res) {
			if($_REQUEST['promotion_price']>0){
				if($res['sku_price']<=$_REQUEST['promotion_price']){
					unset($res);
				}else{
					$res['promotion_price'] = number_format($_REQUEST['promotion_price'],2);
				}
			}else if($_REQUEST['promotion_prescription']>0){
				 $validatePrice = $res['sku_price']-($_REQUEST['promotion_prescription']/100);
				 $res['promotion_price'] = number_format($validatePrice,2);
			}

			if(in_array($res['sku'], $Existpromotion_codes)){
					unset($res);
			}

			$newres[] = $res;
		}
		if(count($newres)>=1){
			$newres = array_values(array_filter($newres));
		}
		
		$this->response->setOutput(json_encode($newres));
	}
	public function array_flatten($array) { 
	  if (!is_array($array)) { 
	    return FALSE; 
	  } 
	  $result = array(); 
	  foreach ($array as $key => $value) { 
	    if (is_array($value)) { 
	      $result = array_merge($result,$this->array_flatten($value)); 
	    } 
	    else { 
	      $result[$key] = $value; 
	    } 
	  } 
	  return $result; 
	} 
	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/promotions')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['description'])) {
			$this->error['description'] = $this->language->get('Enter Name');
		}	
		if (empty($this->request->post['promotion_code'])) {
			$this->error['promotion_code'] = $this->language->get('Enter Code');
		}else{
			// promotion code duplication check
			$check =$this->model_setting_promotions->getPromotionDetailsById($this->request->post['promotion_code']);
			
			if(count($check)>=1 &&($this->request->post['page_action'] !='update')){
				$this->error['warning'] = 'PromotionCode '.$this->request->post['promotion_code'].' already exists';
			}
		}

		if (empty($this->request->post['from_date'])) {
			$this->error['from_date'] = $this->language->get('Enter Date');
		}

		if (empty($this->request->post['to_date'])) {
			$this->error['to_date'] = $this->language->get('Enter Date');
		}

		if (empty($this->request->post['from_time'])) {
			$this->error['from_time'] = $this->language->get('Enter Time');
		}

		if (empty($this->request->post['to_time'])) {
			$this->error['to_time'] = $this->language->get('Enter Time');
		}

		if(empty($this->request->post['selected_product_cnt']) && $this->request->post['page_action'] !='update'){
			$this->error['warning'] = 'select Products for add promotions';
		}
		if($this->request->post['promotion_prescription']=='.00'){
			$this->request->post['promotion_prescription'] = '';
		}
		if (!empty($this->request->post['promotion_prescription']) && !empty($this->request->post['promotion_price'])) {
			$this->error['warning'] = 'Enter Promotion price or percentage value only';
		}


		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}


	public function view(){
		$this->language->load('setting/promotions');
		$this->load->model('setting/promotions');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Promotions View'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		
		$this->data['back_button'] 		= $this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['promotionInfo'] 	= $this->model_setting_promotions->getPromotionDetailsById($this->request->get['promotion_code']);
		$this->data['PromotionDays'] 	= $this->model_setting_promotions->getDaysDetails($this->data['promotionInfo']['PromotionCode']);
		$this->data['skuDetails'] 	    = $this->model_setting_promotions->getSkuDetailsById($this->data['promotionInfo']['PromotionCode']);
		$this->data['skuCount'] 	    = $this->model_setting_promotions->getSkuCount($this->data['promotionInfo']['PromotionCode']);
		$this->template = 'setting/promotions_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	  }
    
	
	public function validateFormType(){
			if (!$this->user->hasPermission('modify', 'setting/promotions')) {
				$this->error['warning'] = $this->language->get('error_permission');
			}
			//$terminal_info = $this->model_setting_promotions->getTerminalGroup($this->request->post['promotion_code']);
			
			if (empty($this->request->post['promotion_code'])) {
				$this->error['promotion_code'] = $this->language->get('Enter Code');
			} elseif($this->request->post['promotion_code'] != '' && count($terminal_info) > 0){
				$this->error['warning'] = $this->language->get('GroupName already in use!.');
			}

			/*if (empty($this->request->post['terminal_code'])) {
				$this->error['terminal_code'] = $this->language->get('Enter code');
			} elseif($this->request->post['terminal_code'] != '' && count($terminal_info) > 0){
				$this->error['warning'] = $this->language->get('GroupName already in use!.');
			}*/
			
			if (!$this->error) {
				return true;
			} else {
				return false;
			}
		}


	public function promotionsDelete()
	{
		$this->load->model('setting/promotions');
		if($this->request->get['promotion_code']){
			$deleteall =  $this->model_setting_promotions->deleteAllpromotion($this->request->get['promotion_code']);
			if($deleteall){
				return true;
			}
		}
	}


}

?>