<?php
class ControllerSettingSetting extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/setting'); 

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->template = 'setting/setting.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
		$this->load->model('setting/setting');
		//printArray($this->config); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validate()*/) {
			$this->model_setting_setting->editSetting('config', $this->request->post);
			if ($this->config->get('config_currency_auto')) {
				$this->load->model('localisation/currency');
				$this->model_localisation_currency->updateCurrencies();
			}	
			$this->session->data['success'] = $this->language->get('text_success');
			//$this->redirect($this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = 'General Setting';
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['entry_admin_limit'] = $this->language->get('entry_admin_limit');
		$this->data['entry_next_sku_no'] = $this->language->get('entry_next_sku_no');
		$this->data['entry_invoice_prefix'] = $this->language->get('entry_invoice_prefix');
		$this->data['entry_admin_language'] = $this->language->get('entry_admin_language');
		$this->data['entry_language'] = $this->language->get('entry_language');
		$this->data['entry_inventory_increment'] = $this->language->get('entry_inventory_increment');
		$this->data['entry_purchase_increment'] = $this->language->get('entry_purchase_increment');
		$this->data['entry_currency'] = $this->language->get('entry_currency');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_order_status'] = $this->language->get('entry_order_status');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_others'] = $this->language->get('text_others');
		$this->data['text_apply_tax'] = $this->language->get('text_apply_tax');
		$this->data['text_method_pos'] = $this->language->get('text_method_pos');
		$this->data['text_method_credit'] = $this->language->get('text_method_credit');
		$this->data['text_method_purchase'] = $this->language->get('text_method_purchase');
		$this->data['text_language_cost'] = $this->language->get('text_language_cost');
		$this->data['text_average_cost_method'] = $this->language->get('text_average_cost_method');
		$this->data['text_manage_inventory'] = $this->language->get('text_manage_inventory');
		$this->data['text_vendor_update'] =  $this->language->get('text_vendor_update');
		$this->data['text_autosearch'] =  $this->language->get('text_autosearch');
		$this->data['text_general'] = $this->language->get('text_general');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'General Setting',
			'href'      => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL');		
		$this->data['cancel'] = $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['button_save'] = 'Save';
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->load->model('localisation/country');
		$this->data['countries'] = $this->model_localisation_country->getCountries();
		$this->load->model('localisation/currency');
		$where['status']	= 1;
		$this->data['currencies'] = $this->model_localisation_currency->getCurrencies($where);
		$this->load->model('localisation/length_class');
		$this->data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		$this->load->model('localisation/order_status');
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		$this->load->model('localisation/stock_status');
		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();
		$this->data['networks'] 	  = $this->model_setting_setting->getNetworks();
		$this->load->model('localisation/language');
		$this->data['languages'] 	  = $this->model_localisation_language->getLanguages();
		$this->data['payment_methods']= $this->model_setting_setting->getPaymentMethods();

		foreach ($languages as $language) {
			if (isset($this->request->post['config_description_' . $language['language_id']])) {
				$this->data['config_description_' . $language['language_id']] = $this->request->post['config_description_' . $language['language_id']];
			} else {
				$this->data['config_description_' . $language['language_id']] = $this->config->get('config_description_' . $language['language_id']);
			}
		}
		
		$this->data['entry_order_amount'] = $this->language->get('entry_order_amount');
		if (isset($this->request->post['config_display_amount'])) {
			$this->data['config_display_amount'] = $this->request->post['config_display_amount'];
		} else {
			$this->data['config_display_amount'] = $this->config->get('config_display_amount');
		}
		
		if (isset($this->request->post['config_apply_tax'])) {
			$this->data['config_apply_tax'] = (string)$this->request->post['config_apply_tax'];
		} else {
			$this->data['config_apply_tax'] = (string)$this->config->get('config_apply_tax');
		}
		/* Credit Sales and Purchase Code Added in 19-09-2015 */
		if (isset($this->request->post['config_apply_tax_credit_sales'])) {
			$this->data['config_apply_tax_credit_sales'] = (string)$this->request->post['config_apply_tax_credit_sales'];
		} else {
			$this->data['config_apply_tax_credit_sales'] = (string)$this->config->get('config_apply_tax_credit_sales');
		}

		if (isset($this->request->post['config_apply_tax_purchase'])) {
			$this->data['config_apply_tax_purchase'] = (string)$this->request->post['config_apply_tax_purchase'];
		} else {
			$this->data['config_apply_tax_purchase'] = (string)$this->config->get('config_apply_tax_purchase');
		}
		/* Credit Sales and Purchase Code Added in 19-09-2015 */
		if (isset($this->request->post['config_invoice_prefix'])) {
			$this->data['config_invoice_prefix'] = $this->request->post['config_invoice_prefix'];
		} elseif ($this->config->get('config_invoice_prefix')) {
			$this->data['config_invoice_prefix'] = $this->config->get('config_invoice_prefix');			
		} else {
			$this->data['config_invoice_prefix'] = 'INV-' . date('Y') . '-00';
		}
		if (isset($this->request->post['config_admin_limit'])) {
			$this->data['config_admin_limit'] = $this->request->post['config_admin_limit'];
		} else {
			$this->data['config_admin_limit'] = $this->config->get('config_admin_limit');
		}
		if (isset($this->request->post['next_sku_no'])) {
			$this->data['next_sku_no'] = $this->request->post['next_sku_no'];
		} else {
			$this->data['next_sku_no'] = $this->config->get('next_sku_no');
		}
		if (isset($this->request->post['config_inventory_increment'])) {
			$this->data['config_inventory_increment'] = $this->request->post['config_inventory_increment'];
		} else {
			$this->data['config_inventory_increment'] = $this->config->get('config_inventory_increment');
		}
		if (isset($this->request->post['config_purchase_increment'])) {
			$this->data['config_purchase_increment'] = $this->request->post['config_purchase_increment'];
		} else {
			$this->data['config_purchase_increment'] = $this->config->get('config_purchase_increment');
		}
		if (isset($this->request->post['config_admin_language'])) {
			$this->data['config_admin_language'] = $this->request->post['config_admin_language'];
		} else {
			$this->data['config_admin_language'] = $this->config->get('config_admin_language');
		}
		if (isset($this->request->post['config_language'])) {
			$this->data['config_language'] = $this->request->post['config_language'];
		} else {
			$this->data['config_language'] = $this->config->get('config_language');
		}
		if (isset($this->request->post['config_currency'])) {
			$this->data['config_currency'] = $this->request->post['config_currency'];
		} else {
			$this->data['config_currency'] = $this->config->get('config_currency');
		}
		if (isset($this->request->post['config_weight_class'])) {
			$this->data['config_weight_class'] = $this->request->post['config_weight_class'];
		} else {
			$this->data['config_weight_class'] = $this->config->get('config_weight_class');
		}
		if (isset($this->request->post['config_order_status_id'])) {
			$this->data['config_order_status_id'] = $this->request->post['config_order_status_id'];
		} else {
			$this->data['config_order_status_id'] = $this->config->get('config_order_status_id');
		}
		/* Average Cost Method Code Added in 19-09-2015 */
		if (isset($this->request->post['config_average_cost_method'])) {
			$this->data['config_average_cost_method'] = (string)$this->request->post['config_average_cost_method'];
		} else {
			$this->data['config_average_cost_method'] = (string)$this->config->get('config_average_cost_method');
		}
		if (isset($this->request->post['config_manage_inventory'])) {
			$this->data['config_manage_inventory'] = (string)$this->request->post['config_manage_inventory'];
		} else {
			$this->data['config_manage_inventory'] = (string)$this->config->get('config_manage_inventory');
		}
		if (isset($this->request->post['config_vendor_update'])) {
			$this->data['config_vendor_update'] = (string)$this->request->post['config_vendor_update'];
		} else {
			$this->data['config_vendor_update'] = (string)$this->config->get('config_vendor_update');
		}
		/* Average Cost Method Code Added in 19-09-2015 */
		if (isset($this->request->post['config_stock_status_id'])) {
			$this->data['config_stock_status_id'] = $this->request->post['config_stock_status_id'];
		} else {
			$this->data['config_stock_status_id'] = $this->config->get('config_stock_status_id');
		}
		/* Average Cost Method Code Added in 19-09-2015 */
		if (isset($this->request->post['config_product_discount'])) {
			$this->data['config_product_discount'] = $this->request->post['config_product_discount'];
		} else {
			$this->data['config_product_discount'] = $this->config->get('config_product_discount');
		}
		if (isset($this->request->post['config_autosearch_string'])) {
			$this->data['config_autosearch_string'] = $this->request->post['config_autosearch_string'];
		} else {
			$this->data['config_autosearch_string'] = $this->config->get('config_autosearch_string');
		}
		if (isset($this->request->post['config_purchase_prefix'])) {
			$this->data['config_purchase_prefix'] = $this->request->post['config_purchase_prefix'];
		} else {
			$this->data['config_purchase_prefix'] = $this->config->get('config_purchase_prefix');
		}
		if (isset($this->request->post['config_purchase_invoice_prefix'])) {
			$this->data['config_purchase_invoice_prefix'] = $this->request->post['config_purchase_invoice_prefix'];
		} else {
			$this->data['config_purchase_invoice_prefix'] = $this->config->get('config_purchase_invoice_prefix');
		}
		if (isset($this->request->post['config_purchase_return_prefix'])) {
			$this->data['config_purchase_return_prefix'] = $this->request->post['config_purchase_return_prefix'];
		} else {
			$this->data['config_purchase_return_prefix'] = $this->config->get('config_purchase_return_prefix');
		}
		if (isset($this->request->post['config_sales_quote_prefix'])) {
			$this->data['config_sales_quote_prefix'] = $this->request->post['config_sales_quote_prefix'];
		} else {
			$this->data['config_sales_quote_prefix'] = $this->config->get('config_sales_quote_prefix');
		}
		if (isset($this->request->post['config_sales_prefix'])) {
			$this->data['config_sales_prefix'] = $this->request->post['config_sales_prefix'];
		} else {
			$this->data['config_sales_prefix'] = $this->config->get('config_sales_prefix');
		}
		if (isset($this->request->post['config_sales_invoice_prefix'])) {
		   $this->data['config_sales_invoice_prefix']=$this->request->post['config_sales_invoice_prefix'];
		} else {
			$this->data['config_sales_invoice_prefix']=$this->config->get('config_sales_invoice_prefix');
		}
		if (isset($this->request->post['config_sales_return_prefix'])) {
			$this->data['config_sales_return_prefix'] =$this->request->post['config_sales_return_prefix'];
		} else {
			$this->data['config_sales_return_prefix'] = $this->config->get('config_sales_return_prefix');
		}
		if (isset($this->request->post['config_error_log'])) {
			$this->data['config_error_log'] =$this->request->post['config_error_log'];
		} else {
			$this->data['config_error_log'] = $this->config->get('config_error_log');
		}
		if (isset($this->request->post['config_sales_service_prefix'])) {
			$this->data['config_sales_service_prefix'] =$this->request->post['config_sales_service_prefix'];
		} else {
			$this->data['config_sales_service_prefix'] = $this->config->get('config_sales_service_prefix');
		}
		if (isset($this->request->post['config_sales_purchase_prefix'])) {
			$this->data['config_sales_purchase_prefix']=$this->request->post['config_sales_purchase_prefix'];
		} else {
			$this->data['config_sales_purchase_prefix']=$this->config->get('config_sales_purchase_prefix');
		}
        $this->data['allowCronJob'] = 0;
        if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
            $this->data['allowCronJob'] = 1;
        }
		$this->data['route'] = $this->request->get['route'];
		$this->template = 'setting/setting.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	protected function validate() {
	
		if (!$this->user->hasPermission('modify', 'setting/setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['config_name']) {
			$this->error['name'] = $this->language->get('error_name');
		}		
				
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
		
	public function country() 
    {
		$json = array();	
		$this->load->model('localisation/country');
    	$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		$this->response->setOutput(json_encode($json));
    }
    
    public function shopee_sync()
    {
        try {
            $fromDate= changeDate($this->request->post['from_date']);
            file_get_contents(HTTPS_SERVER."doc/mp_shopee_orderfetch.php?from_date=".$fromDate);

            $res = array('status' => true, 'msg'=> 'Imported');
            $this->session->data['success'] = 'Shopee Orders Imported';
        } catch (Exception $e) {
            $res = array('status' => false, 'msg'=> 'Something Wrong, Try Again');
        }
        $this->response->setOutput(json_encode($res));
    }
    public function shopify_sync()
    {
        try {
            $fromDate= changeDate($this->request->post['from_date']);
            file_get_contents(HTTPS_SERVER."doc/mp_shopify_orderfetch.php?from_date=".$fromDate);
            
            $res = array('status' => true, 'msg'=> 'Imported');
            $this->session->data['success'] = 'Shopify Orders Imported';
        } catch (Exception $e) {
            $res = array('status' => false, 'msg'=> 'Something Wrong, Try Again');
        }
        $this->response->setOutput(json_encode($res));
    }
}
?>