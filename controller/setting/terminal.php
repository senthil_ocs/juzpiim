<?php 
class ControllerSettingTerminal extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('setting/terminal');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/terminal');

		$this->getList();
		
	}

	public function insert() {

		$this->language->load('setting/terminal');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/terminal');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$term_id = $this->model_setting_terminal->addTerminal($this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			//if(!empty($term_id)){
			$this->redirect($this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			//} 		
			
		}
		$this->getForm();		
	}


	public function update() {

		$this->language->load('setting/terminal');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/terminal');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_setting_terminal->editTerminal($this->request->post,$this->request->get['terminal_code']);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
			
		}

		$this->getForm();
	}

	public function delete() {
    	$this->language->load('setting/terminal');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/terminal');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $terminal_code) {
				$this->model_setting_terminal->deleteTerminal($terminal_code);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}


	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_terminal_name'])) {
			$url.= '&filter_terminal_name='.$this->request->post['filter_terminal_name'];
		}
		
		if (isset($this->request->post['filter_terminal_name'])) {
			$filter_terminal_name = $this->request->post['filter_terminal_name'];
		} else {
			$filter_terminal_name ="";
		}
		
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/terminal/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/terminal/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['terminals'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_terminal_name' => $filter_terminal_name
		);
			
		 $terminal_total = $this->model_setting_terminal->getTotalTerminals($data);


		$results = $this->model_setting_terminal->getTerminals($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/terminal/update', 'token=' . $this->session->data['token'] . '&terminal_code=' . $result['terminal_code'] . $url, 'SSL')
			);

			$this->data['terminals'][] = array(
				'terminal_code'			=> $result['terminal_code'],
				'terminal_name'      	=> $result['terminal_name'],
				'system_name'			=> $result['system_name'],
				'cash_machine'			=> $result['cash_machine'],
				'cash_machine_port'		=> $result['cash_machine_port'],
				'coin_machine'			=> $result['coin_machine'],
				'coin_machine_port'		=> $result['coin_machine_port'],
				'createdon'				=> date('d/m/Y H:i:s',strtotime($result['createdon'])), 
				'last_sync_date'		=> date('d/m/Y',strtotime($result['last_sync_date'])),
				'selected'          	=> isset($this->request->post['selected']) && in_array($result['terminal_code'], $this->request->post['selected']),
				'action'            	=> $action
			);
		}
		//print_r($result);exit();
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Terminal List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		//$this->data['sort_terminal_name'] = $this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . '&sort=terminal_name' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $terminal_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/terminal_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}

	public function getForm() {

		$this->load->model('setting/terminal');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_terminal_name'] = $this->language->get('entry_terminal_name');
		$this->data['entry_system_name'] = $this->language->get('entry_system_name');
		$this->data['entry_cash_machine'] = $this->language->get('entry_cash_machine');
		$this->data['entry_cash_machine_port'] = $this->language->get('entry_cash_machine_port');
		$this->data['entry_coin_machine'] = $this->language->get('entry_coin_machine');
		$this->data['entry_coin_machine_port'] = $this->language->get('entry_coin_machine_port');
		$this->data['entry_Cust_display'] = $this->language->get('entry_Cust_display');
		$this->data['entry_Cust_Display_Port'] = $this->language->get('entry_Cust_Display_Port');
		$this->data['entry_Cust_Display_Type'] = $this->language->get('entry_Cust_Display_Type');
		$this->data['entry_Cust_Display_Msg1'] = $this->language->get('entry_Cust_Display_Msg1');
		$this->data['entry_Cust_Display_Msg2'] = $this->language->get('entry_Cust_Display_Msg2');
		$this->data['entry_speaker_enabled'] = $this->language->get('entry_speaker_enabled');
		$this->data['entry_weighingscale_enabled'] = $this->language->get('entry_weighingscale_enabled');
		$this->data['entry_weighingscale_port'] = $this->language->get('entry_weighingscale_port');
		$this->data['entry_last_sync_date'] = $this->language->get('entry_last_sync_date');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['terminal_name'])) {
			$this->data['error_terminal_name'] = $this->error['terminal_name'];
		} else {
			$this->data['error_terminal_name'] = '';
		}		


		if (isset($this->error['terminal_code'])) {
			$this->data['error_terminal_code'] = $this->error['terminal_code'];
		} else {
			$this->data['error_terminal_code'] = '';
		}

	
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Terminal List'),
			'href'		=> $this->url->link('setting/terminal', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['terminal_code'])) {
			
		   	$this->data['action'] = $this->url->link('setting/terminal/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		
			$this->data['action'] = $this->url->link('setting/terminal/update', 'token=' . $this->session->data['token'] . '&terminal_code=' . $this->request->get['terminal_code'] . $url, 'SSL');
		}	
		$this->data['cancel'] = $this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . $url, 'SSL');

		
		
		if (isset($this->request->get['terminal_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$terminal_info = $this->model_setting_terminal->getTerminal($this->request->get['terminal_code']);
		}


		if(isset($this->request->post['terminal_code'])){
			$this->data['terminal_code'] =$this->request->post['terminal_code'];
		} elseif (!empty($terminal_info)) {
			$this->data['terminal_code'] = trim($terminal_info['terminal_code']);
		} else {			
			$this->data['terminal_code'] ='';
		}	
		
		if(isset($this->request->post['terminal_name'])){
			$this->data['terminal_name'] =$this->request->post['terminal_name'];
		} elseif (!empty($terminal_info)) {
			$this->data['terminal_name'] = trim($terminal_info['terminal_name']);
		} else {			
			$this->data['terminal_name'] ='';
		}


		
		if(isset($this->request->post['system_name'])) {
			$this->data['status'] = $this->request->post['system_name'];
		} elseif(!empty($terminal_info)) {
			$this->data['system_name'] = trim($terminal_info['system_name']);
		} else {
			$this->data['system_name'] == '';
		}

		if (isset($this->request->post['cash_machine'])) {
			$this->data['cash_machine'] = $this->request->post['cash_machine'];
		} elseif (!empty($terminal_info)) {
			$this->data['cash_machine'] = $terminal_info['cash_machine'];
		} else {
			$this->data['cash_machine'] = '';
		}		
		
		if (isset($this->request->post['cash_machine_port'])) {
			$this->data['cash_machine_port'] = $this->request->post['cash_machine_port'];
		} elseif (!empty($terminal_info)) {
			$this->data['cash_machine_port'] = trim($terminal_info['cash_machine_port']);
		} else {
			$this->data['cash_machine_port'] = '';
		}

		if (isset($this->request->post['coin_machine'])) {
			$this->data['coin_machine'] = $this->request->post['coin_machine'];
		} elseif (!empty($terminal_info)) {
			$this->data['coin_machine'] = $terminal_info['coin_machine'];
		} else {
			$this->data['coin_machine'] = '';
		}

		if (isset($this->request->post['coin_machine_port'])) {
			$this->data['coin_machine_port'] = $this->request->post['coin_machine_port'];
		} elseif (!empty($terminal_info)) {
			$this->data['coin_machine_port'] = trim($terminal_info['coin_machine_port']);
		} else {
			$this->data['coin_machine_port'] = '';
		}

		if (isset($this->request->post['Cust_display'])) {
			$this->data['Cust_display'] = $this->request->post['Cust_display'];
		} elseif (!empty($terminal_info)) {
			$this->data['Cust_display'] = $terminal_info['Cust_display'];
		} else {
			$this->data['Cust_display'] = '';
		}

		if (isset($this->request->post['Cust_Display_Port'])) {
			$this->data['Cust_Display_Port'] = $this->request->post['Cust_Display_Port'];
		} elseif (!empty($terminal_info)) {
			$this->data['Cust_Display_Port'] = trim($terminal_info['Cust_Display_Port']);
		} else {
			$this->data['Cust_Display_Port'] = '';
		}

		if (isset($this->request->post['Cust_Display_Type'])) {
			$this->data['Cust_Display_Type'] = $this->request->post['Cust_Display_Type'];
		} elseif (!empty($terminal_info)) {
			$this->data['Cust_Display_Type'] = trim($terminal_info['Cust_Display_Type']);
		} else {
			$this->data['Cust_Display_Type'] = '';
		}

		if (isset($this->request->post['Cust_Display_Msg1'])) {
			$this->data['Cust_Display_Msg1'] = $this->request->post['Cust_Display_Msg1'];
		} elseif (!empty($terminal_info)) {
			$this->data['Cust_Display_Msg1'] = trim($terminal_info['Cust_Display_Msg1']);
		} else {
			$this->data['Cust_Display_Msg1'] = '';
		}

		if (isset($this->request->post['Cust_Display_Msg2'])) {
			$this->data['Cust_Display_Msg2'] = $this->request->post['Cust_Display_Msg2'];
		} elseif (!empty($terminal_info)) {
			$this->data['Cust_Display_Msg2'] = trim($terminal_info['Cust_Display_Msg2']);
		} else {
			$this->data['Cust_Display_Msg2'] = '';
		}
		
		if (isset($this->request->post['speaker_enabled'])) {
			$this->data['speaker_enabled'] = $this->request->post['speaker_enabled'];
		} elseif (!empty($terminal_info)) {
			$this->data['speaker_enabled'] = $terminal_info['speaker_enabled'];
		} else {
			$this->data['speaker_enabled'] = '';
		}

		if (isset($this->request->post['weighingscale_enabled'])) {
			$this->data['weighingscale_enabled'] = $this->request->post['weighingscale_enabled'];
		} elseif (!empty($terminal_info)) {
			$this->data['weighingscale_enabled'] = $terminal_info['weighingscale_enabled'];
		} else {
			$this->data['weighingscale_enabled'] = '';
		}

		if (isset($this->request->post['weighingscale_port'])) {
			$this->data['weighingscale_port'] = $this->request->post['weighingscale_port'];
		} elseif (!empty($terminal_info)) {
			$this->data['weighingscale_port'] = trim($terminal_info['weighingscale_port']);
		} else {
			$this->data['weighingscale_port'] = '';
		}

		

		if (isset($this->request->post['last_sync_date'])) {
			$this->data['last_sync_date'] = $this->request->post['last_sync_date'];
		} elseif (!empty($terminal_info)) {
			$this->data['last_sync_date'] = $terminal_info['last_sync_date'];
		} else {
			$this->data['last_sync_date'] = '';
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/terminal';

		$this->template = 'setting/terminal_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	

	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/terminal')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['terminal_name'])) {
			$this->error['terminal_name'] = $this->language->get('Enter Name');
		}	
		
		if (empty($this->request->post['terminal_code'])) {
			$this->error['terminal_code'] = $this->language->get('Enter Code');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
public function validateFormType()
	{
		if (!$this->user->hasPermission('modify', 'setting/terminal')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$terminal_info = $this->model_setting_terminal->getTerminalGroup($this->request->post['terminal_name'],$this->request->post['terminal_code']);
		
		if (empty($this->request->post['terminal_name'])) {
			$this->error['terminal_name'] = $this->language->get('Enter name');
		} elseif($this->request->post['terminal_name'] != '' && count($terminal_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}

		if (empty($this->request->post['terminal_code'])) {
			$this->error['terminal_code'] = $this->language->get('Enter code');
		} elseif($this->request->post['terminal_code'] != '' && count($terminal_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}

?>