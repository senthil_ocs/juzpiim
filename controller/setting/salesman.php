<?php 
class ControllerSettingSalesman extends Controller {
	private $error = array();
	public function index() {

		$this->language->load('setting/salesman');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/salesman');
		$this->getList();
	}
	public function insert() {

		$this->language->load('setting/salesman');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/salesman');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$salesman_id = $this->model_setting_salesman->addSalesman($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if(!empty($salesman_id)){	
				$this->redirect($this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} 		
		}
		$this->getForm();		
	}
	public function schedule() {
		$this->language->load('setting/salesman');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/salesman');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_date'])) {
			$filter_date = $this->request->post['filter_date'];
			$url .= '&filter_date=' . $filter_date;
		}else{
			$filter_date = date('d/m/Y');
		}
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/salesman/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');		 
		$this->data['customers'] = array();
		$salesman_id = $this->request->get['salesman_id'];

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'salesman_id' => $salesman_id,
			'filter_date' => $filter_date,
		);
		
		$this->data['filters'] = $data;
		// $salesman_total = $this->model_setting_salesman->getTotalSchedule($data);
		$results = $this->model_setting_salesman->getSchedule($data);
		$this->data['deliveryManName'] = $this->model_setting_salesman->getScheduleCustName($data);

		foreach ($results as $result) {
			$action = array();
			$count = $this->model_setting_salesman->getsalesDetailsCount($result['do_no']);
			$this->data['salesmanList'][] = array(
				'sort_id'			=> $result['sort_id'],
				'invoice_no'		=> $result['sales_transaction_no'],
				'do_no'				=> $result['do_no'],
				'salerOrderId'		=> $result['sales_invoice_id'],
				'signature'			=> $result['signature'],
				'invoice_date'	    => $result['invoice_date'],
				'name' 				=> $result['name'],
				'total_items'     	=> $count,
				'delivery_status' 	=> $result['status'],
				'net_total'			=> $result['net_total'],
				'selected'   		=> isset($this->request->post['selected']) && in_array($result['name'], $this->request->post['selected'])
			);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Driver List',
			'href'      => $this->url->link('setting/salesman', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Schedule List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['sort_name']   = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email']  = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination 		 = new Pagination();
		$pagination->total 	 = count($results);
		$pagination->page 	 = $page;
		$pagination->limit 	 = $this->config->get('config_admin_limit');
		$pagination->text 	 = $this->language->get('text_pagination');
		$pagination->url 	 = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination']  = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['token'] = $this->session->data['token'];
		$this->template 	 = 'setting/salesman_schedule_list.tpl';
		$this->children 	 = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);			
		$this->response->setOutput($this->render());
	}
	public function update() {
		$this->language->load('setting/salesman');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/salesman');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_salesman->editSalesman($this->request->post,$this->request->get['salesman_id']);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function getList(){
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}	
		$url = '';	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_phone'])) {
			$url.= '&filter_phone='.$this->request->post['filter_phone'];
		}
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_phone'])) {
			$filter_phone = $this->request->post['filter_phone'];
		} else {
			$filter_phone ="";
		}
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/salesman/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['customers'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_phone'	=>$filter_phone,
		);
		$salesman_total = $this->model_setting_salesman->getTotalSalesman($data);
		$results = $this->model_setting_salesman->getSalesman($data);
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/salesman/update', 'token=' . $this->session->data['token'] . '&salesman_id=' . $result['id'] . $url, 'SSL'),
				'textSchedule' => 'Schedule',
				'hrefSchedule' => $this->url->link('setting/salesman/schedule', 'token=' . $this->session->data['token'] . '&salesman_id=' . $result['id'] . $url, 'SSL')
			);
			$this->data['salesmanList'][] = array(
				'id'		=> $result['id'],
				'name'		=> $result['name'],
				'phone' 	=> $result['phone'],
				'email'     => $result['email'],
				'route'		=> $result['route'],
				'addedon'	=> $result['addedon'], 
				'selected'   => isset($this->request->post['selected']) && in_array($result['name'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		$this->data['heading_title']   = $this->language->get('heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert']   = $this->language->get('button_insert');
		$this->data['button_delete']   = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Driver List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination         = new Pagination();
		$pagination->total  = $salesman_total;
		$pagination->page   = $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text   = $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['route']= $this->request->get['route'];

		$this->template = 'setting/salesman_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function getForm() {

		$this->load->model('setting/salesman');		
		$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['entry_name'] 	  = $this->language->get('entry_name');
		$this->data['entry_code'] 	  = $this->language->get('entry_code');
		$this->data['entry_address']  = $this->language->get('entry_address');
		$this->data['entry_country']  = $this->language->get('entry_country');
		$this->data['entry_zipcode']  = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] 	  = $this->language->get('entry_phone');
		$this->data['entry_status']   = $this->language->get('entry_status');
		$this->data['button_save'] 	  = $this->language->get('button_save');
		$this->data['button_cancel']  = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
		if (isset($this->error['email_duplication'])) {
			$this->data['error_email_duplication'] = $this->error['email_duplication'];
		} else {
			$this->data['error_email_duplication'] = '';
		}
		if (isset($this->error['phone_duplication'])) {
			$this->data['error_phone_duplication'] = $this->error['phone_duplication'];
		} else {
			$this->data['error_phone_duplication'] = '';
		}
		if (isset($this->error['phone'])) {
			$this->data['error_phone'] = $this->error['phone'];
		} else {
			$this->data['error_phone'] = '';
		}
		if (isset($this->error['route_id'])) {
			$this->data['error_route_id'] = $this->error['route_id'];
		} else {
			$this->data['error_route_id'] = '';
		}
		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['route_list'] = $this->model_setting_salesman->routeList();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Driver List'),
			'href'		=> $this->url->link('setting/salesman', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!isset($this->request->get['salesman_id'])) {	
		   	$this->data['action'] = $this->url->link('setting/salesman/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/salesman/update', 'token=' . $this->session->data['token'] . '&salesman_id=' . $this->request->get['salesman_id'] . $url, 'SSL');
		}	
		$this->data['cancel'] = $this->url->link('setting/salesman', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['salesman_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$salesman_info = $this->model_setting_salesman->getSalesmanData($this->request->get['salesman_id']);
		}
		
		if(isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif(!empty($salesman_info)) {
			$this->data['status'] = $salesman_info['status'];
		} else {
			$this->data['status'] == '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($salesman_info)) {
			$this->data['name'] = trim($salesman_info['name']);
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($salesman_info)) {
			$this->data['email'] = trim($salesman_info['email']);
		} else {
			$this->data['email'] = '';
		}
		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} elseif (!empty($salesman_info)) {
			$this->data['password'] = trim($salesman_info['password']);
		} else {
			$this->data['password'] = '';
		}

		if (isset($this->request->post['route_id'])) {
			$this->data['route_id'] = $this->request->post['route_id'];
		} elseif (!empty($salesman_info)) {
			$this->data['route_id'] = trim($salesman_info['route_id']);
		} else {
			$this->data['route_id'] = '';
		}

		if (isset($this->request->post['phone'])) {
			$this->data['phone'] = $this->request->post['phone'];
		} elseif (!empty($salesman_info)) {
			$this->data['phone'] = trim($salesman_info['phone']);
		} else {
			$this->data['phone'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($salesman_info)) {
			$this->data['address1'] = trim($salesman_info['address1']);
		} else {
			$this->data['address1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($salesman_info)) {
			$this->data['address2'] = trim($salesman_info['address2']);
		} else {
			$this->data['address2'] = '';
		}

		if (isset($this->request->post['city'])) {
			$this->data['city'] = $this->request->post['city'];
		} elseif (!empty($salesman_info)) {
			$this->data['city'] = trim($salesman_info['city']);
		} else {
			$this->data['city'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
		} elseif (!empty($salesman_info)) {
			$this->data['state'] = trim($salesman_info['state']);
		} else {
			$this->data['state'] = '';
		}

		if (isset($this->request->post['zipcode'])) {
			$this->data['zipcode'] = $this->request->post['zipcode'];
		} elseif (!empty($salesman_info)) {
			$this->data['zipcode'] = trim($salesman_info['zip_code']);
		} else {
			$this->data['zipcode'] = '';
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/salesman';

		$this->template = 'setting/salesman_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	

	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/salesman')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Enter Name');
		}	
		if (empty($this->request->post['email'])) {
			$this->error['email'] = $this->language->get('Enter Email');
		} else {
			$emaildata = $this->model_setting_salesman->getsalesmanemail($this->request->post['email'],$this->request->get['salesman_id']);
			if($emaildata > 0){
				$this->error['email_duplication'] = $this->language->get('Email  has already been added!');
			}
		}
		if(empty($this->request->post['phone'])) {
			$this->error['phone'] = $this->language->get('Enter Phone No');
		} else {
			$phonedata = $this->model_setting_salesman->getsalesmanphone($this->request->post['phone'], $this->request->get['salesman_id']);
			if($phonedata > 0){
				$this->error['phone_duplication'] = $this->language->get('Phone No has already been added!');
			}
		}	
		if(empty($this->request->post['route_id'])) {
			$this->error['route_id'] = $this->language->get('Select Route');
		}

		if(empty($this->request->post['password'])) {
			$this->error['password'] = $this->language->get('Please enter password');
		}

		if(!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function setstatus(){
		$this->load->model('setting/salesman');	
		$statusData = $this->model_setting_salesman->setScheduleStatus($this->request->post['salerOrderId'],$this->request->post['checkStatus']);
		echo $statusData;
	}
	public function setpriority(){
		$this->load->model('setting/salesman');	
		$priorityData = $this->model_setting_salesman->setPriority($this->request->post['post_order_ids']);
		echo $priorityData;
	}
}

?>