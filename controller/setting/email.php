<?php 
class ControllerSettingEmail extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/email');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/email');
		$this->getList();
		
	}
	public function insert() {

		$this->language->load('setting/email');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/email');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$e_id = $this->model_setting_email->addEmail($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';	
			if ($this->request->get['sort'] !='') {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if ($this->request->get['order'] !='') {
				$url .= '&order=' . $this->request->get['order'];
			}
			if ($this->request->get['page'] !='') {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/email', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();		
	}
	
	public function update() {
		$this->language->load('setting/email');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/email');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_setting_email->editEmail($this->request->post,$this->request->get['report_id']);
			
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/email', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();
	}
	public function delete() {
    	$this->language->load('setting/email');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/email');
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $report_id) {
				$this->model_setting_email->deleteEmail($report_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
		
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$this->redirect($this->url->link('setting/email', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
  	}
	public function getList(){
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_report_id'])) {
			$url.= '&report_id='.$this->request->post['filter_report_id'];
		}
		if (isset($this->request->post['filter_email_id'])) {
			$url.= '&email_id='.$this->request->post['filter_email_id'];
		} 
		
		

		if (isset($this->request->post['filter_report_id'])) {
			$filter_report_id = $this->request->post['filter_report_id'];
		} else {
			$filter_report_id ="";
		}
		if (isset($this->request->post['filter_email_id'])) {
			$filter_email_id = $this->request->post['filter_email_id'];
		} else {
			$filter_email_id ="";
		}
		
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/email/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/email/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['email'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_report_id' => $filter_report_id,
			'filter_email_id' => $filter_email_id,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$email_total = $this->model_setting_email->getTotalEmail($data);
		
		$results = $this->model_setting_email->getEmail($data);
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/email/update', 'token=' . $this->session->data['token'] . '&report_id=' . $result['report_id'] . $url, 'SSL')
			);
			
			$this->data['email'][] = array(
				'report_id'			=> $result['report_id'],
				'report_name'		=> $result['report_name'],
				'email_id'			=> $result['email_id'],
				'active' 			=> $result['active'],
				'createdon'			=> $result['createdon'],
				'selected'          => isset($this->request->post['selected']) && in_array($result['report_id'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		if($result['active'] == 0){
				$active = $this->language->get('text_active');
			} else {
				$active = $this->language->get('text_inactive');
			}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Email List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['sort_report_id'] = $this->url->link('setting/email', 'token=' . $this->session->data['token'] . '&sort=sort_report_id' . $url, 'SSL');
		//$this->data['sort_email_id'] = $this->url->link('setting/email', 'token=' . $this->session->data['token'] . '&sort=sort_email_id' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $email_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/email', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['route'] = 'setting/email';

		$this->data['token'] = $this->session->data['token'];
		
		$this->template = 'setting/email_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_report_name'] = $this->language->get('entry_report_name');
		$this->data['entry_report_id'] = $this->language->get('entry_report_id');
		$this->data['entry_email_id'] = $this->language->get('entry_email_id');
		$this->data['entry_active'] = $this->language->get('entry_active');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['report_id'])) {
			$this->data['error_report_id'] = $this->error['report_id'];
		} else {
			$this->data['error_report_id'] = '';
		}	
		if (isset($this->error['report_name'])) {
			$this->data['error_report_name'] = $this->error['report_name'];
		} else {
			$this->data['error_report_name'] = '';
		}		
		if (isset($this->error['email_id'])) {
			$this->data['error_email_id'] = $this->error['email_id'];
		} else {
			$this->data['error_email_id'] = '';
		}
		
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Email List'),
			'href'		=> $this->url->link('setting/email', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		//$type = $this->request->get['type'];
		if (!isset($this->request->get['report_id'])) {
		   	$this->data['action'] = $this->url->link('setting/email/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		    	$this->data['action'] = $this->url->link('setting/email/update', 'token=' . $this->session->data['token'] . '&report_id=' . $this->request->get['report_id'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('setting/email', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['report_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$email_info = $this->model_setting_email->getEmailById($this->request->get['report_id']);
			//$this->data['editForm'] = '1';
		}		
		if (isset($this->request->post['report_id'])) {
			$this->data['report_id'] = $this->request->post['report_id'];
		} elseif (!empty($email_info)) {
			$this->data['report_id'] = trim($email_info['report_id']);
		} else {
			$this->data['report_id'] = '';
		}
		if (isset($this->request->post['report_name'])) {
			$this->data['report_name'] = $this->request->post['report_name'];
		} elseif (!empty($email_info)) {
			$this->data['report_name'] = trim($email_info['report_name']);
		} else {
			$this->data['report_name'] = '';
		}
		if (isset($this->request->post['active'])) {
			$this->data['active'] = $this->request->post['active'];
		} elseif (!empty($email_info)) {
			$this->data['active'] = $email_info['active'];
		} else {
			$this->data['active'] = '';
		}
		if (isset($this->request->post['email_id'])) {
			$this->data['email_id'] = $this->request->post['email_id'];
		} elseif (!empty($email_info)) {
			$this->data['email_id'] = trim($email_info['email_id']);
		} else {
			$this->data['email_id'] = '';
		}
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/email';
		$this->template = 'setting/email_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/email')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (empty($this->request->post['email_id'])) {
			$this->error['email_id'] = $this->language->get('Enter Id');
		}
		if (empty($this->request->post['report_name'])) {
			$this->error['report_name'] = $this->language->get('Please Enter Report Name');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function updateSortOrder(){
		$code 		= $this->request->post['itemId'];
		$sort_code  = $this->request->post['newvalue'];
		$this->load->model('setting/email');
		
		if(isset($code) && isset($sort_code)){
			$this->model_setting_email->updateSortOrder(trim($code),$sort_code);
			echo 1;
		}else{
			echo 0;
		}
	}
	
}
?>