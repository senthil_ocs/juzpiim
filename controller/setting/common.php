<?php
class ControllerSettingCommon extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/common'); 

		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validate()*/) {
			$this->model_setting_setting->editSetting('config', $this->request->post);
			if ($this->config->get('config_currency_auto')) {
				$this->load->model('localisation/currency');
				$this->model_localisation_currency->updateCurrencies();
			}	
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$userPer=  $this->user->getPermission();		

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_general'] = $this->language->get('text_general');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_sales'] = $this->language->get('text_sales');
		$this->data['text_utilities'] = $this->language->get('text_utilities');
		
        if($this->user->hasPermission('access', 'setting/company')){
			$this->data["general"][] = array( "link" => $this->url->link("setting/company", 'token=' . $this->session->data['token'], 'SSL'),
				                               "altkey"=> $this->language->get('alt_company'),
				                                "id"     => $this->language->get('company_id'),
				                               "Text" => $this->language->get('text_company_setup'),
				                               "icon" =>'fa fa-bank'
				                               );
		}
		if($this->user->hasPermission('access', 'setting/location')){
			$this->data["general"][] = array( "link" => $this->url->link("setting/location", 'token=' . $this->session->data['token'], 'SSL'),
				                               "altkey"=> $this->language->get('alt_location'),
				                                 "id"     => $this->language->get('location_id'),
				                               "Text" => $this->language->get('text_location_setup'),
				                               "icon" =>'fa fa-crosshairs'
				                               );
	    }
	   /* if($this->user->hasPermission('access', 'user/user_permission')){
			$this->data["general"][] = array( "link" => $this->url->link("user/user_permission", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => $this->language->get('text_user_group'),
				                               "icon" =>'fa fa-users'
				                               );
	    }*/
	    if($this->user->hasPermission('access', 'user/user')){
			$this->data["general"][] = array( "link" => $this->url->link("user/user", 'token=' . $this->session->data['token'], 'SSL'),
				                                "altkey"=> $this->language->get('alt_user'),
				                                  "id"     => $this->language->get('user_id'),
				                               "Text" => $this->language->get('text_user_setup'),
				                               "icon" =>'fa fa-user'
				                               );
    	}

    	if($this->user->hasPermission('access', 'user/user_right')){
			$this->data["general"][] = array( "link" => $this->url->link("user/user_right", 'token=' . $this->session->data['token'], 'SSL'),
				                                "altkey"=> $this->language->get('alt_usergroup'),
				                                  "id"     => $this->language->get('usergroup_id'),
				                               "Text" => $this->language->get('text_user_rights'),
				                               "icon" =>'fa fa-users'
				                               );
	    }
	    if($this->user->hasPermission('access', 'setting/setting')){

		$this->data["general"][] = array( "link" => $this->url->link("setting/setting", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_generalsetup'),
			                                  "id"     => $this->language->get('generalsetup_id'),
			                               "Text" => $this->language->get('text_general_setup'),
			                               "icon" =>'fa fa-cog'
			                               );
	    }
	    if($this->user->hasPermission('access', 'localisation/tax_rate')){
		$this->data["general"][] = array( "link" => $this->url->link("localisation/tax_rate", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_taxsetup'),
			                                  "id"     => $this->language->get('taxsetup_id'),
			                               "Text" => $this->language->get('text_tax_setup'),
			                               "icon" =>'fa fa-dollar'
			                               );
		}
	    if($this->user->hasPermission('access', 'setting/terms')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/terms", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_termsetup'),
			                                  "id"     => $this->language->get('termsetup_id'),
			                               "Text" => $this->language->get('text_terms_setup'),
			                               "icon" =>'fa fa-cogs'
			                               );
		}
		if($this->user->hasPermission('access', 'setting/payment_type_master')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/payment_type_master", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_termsetup'),
			                                  "id"     => $this->language->get('termsetup_id'),
			                               "Text" => 'Payment Type Master ',
			                               "icon" =>'fa fa-dollar'
			                               );
		}
	    if($this->user->hasPermission('access', 'localisation/currency')){
		$this->data["general"][] = array( "link" => $this->url->link("localisation/currency", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_currencysetup'),
			                                  "id"     => $this->language->get('currencysetup_id'),
			                               "Text" => $this->language->get('text_currency_setup'),
			                               "icon" =>'fa fa-dollar'
			                               );
		}
	    if($this->user->hasPermission('access', 'setting/country')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/country", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_countrymaster'),
			                                  "id"     => $this->language->get('countrymaster_id'),
			                               "Text" => $this->language->get('text_country_master'),
			                               "icon" =>'fa fa-globe'
			                               );
		}
	 //    if($this->user->hasPermission('access', 'localisation/stock_status')){
		// $this->data["general"][] = array( "link" => $this->url->link("localisation/stock_status", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "altkey"=> $this->language->get('alt_stockstatus'),
		// 	                                 "id"     => $this->language->get('stockstatus_id'),
		// 	                               "Text" => $this->language->get('text_stock_status'),
		// 	                               "icon" =>'fa fa-tags'
		// 	                               );
		// }

		if($this->user->hasPermission('access', 'setting/email')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/email", 'token=' . $this->session->data['token'], 'SSL'),
			                               "Text" => 'Email',
			                               "icon" =>'fa fa-envelope-o'
			                               );
		}
		if($this->user->hasPermission('access', 'setting/xero_settings')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/xero_settings", 'token=' . $this->session->data['token'], 'SSL'),
			                               "Text" => 'Xero Settings',
			                               "icon" =>'fa fa-cogs'
			                               );
		}
		if($this->user->hasPermission('access', 'setting/email_template')){
		$this->data["general"][] = array( "link" => $this->url->link("setting/email_template", 'token=' . $this->session->data['token'], 'SSL'),
			                               "Text" => 'Email Template',
			                               "icon" =>'fa fa-cogs'
			                               );
		}

		// if($this->user->hasPermission('access', 'setting/terminal')){
		// $this->data["general"][] =  array( "link" => $this->url->link("setting/terminal", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "Text" => 'Terminal',
		// 	                               "icon" =>'fa fa-building'
		// 	                               );
		// }

		// if($this->user->hasPermission('access', 'setting/promotions')){
		// $this->data["general"][] =  array( "link" => $this->url->link("setting/promotions", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "Text" => 'Promotions',
		// 	                               "icon" =>'fa fa-terminal'
		// 	                               );
		// }

		// if($this->user->hasPermission('access', 'voucher/voucher')){
		// $this->data["general"][] =  array( "link" => $this->url->link("voucher/voucher", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "Text" => 'Voucher',
		// 	                               "icon" =>'fa fa-gift'
		// 	                               );
		// }


	 //    if($this->user->hasPermission('access', 'master/payment')){
		// $this->data["sales"][] =  array( "link" => $this->url->link("master/payment", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "altkey"=> $this->language->get('alt_paymenttype'),
		// 	                                 "id"     => $this->language->get('paymenttype_id'),
		// 	                               "Text" => $this->language->get('text_payment_type'),
		// 	                               "icon" =>'fa fa-credit-card'
		// 	                               );
		// }
		
		/*if($this->user->hasPermission('access', 'setting/customers/customertype')){
		$this->data["sales"][] =  array( "link" =>$this->url->link("setting/customers/customertype", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_customergroup'),
			                                  "id"     => $this->language->get('customergroup_id'),
			                               "Text" => $this->language->get('Customer Group'),
			                               "icon" =>'fa fa-users'
			                               );
		}*/	    
	    
		

		if($this->user->hasPermission('access', 'setting/route')){

		$this->data["supplier"][] = array("link" =>$this->url->link('setting/route', '&token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_suppliermaster'),
			                                  "id"     => $this->language->get('suppliermaster_id'),
			                               "Text" => 'Route',
			                               "icon" =>'fa fa-arrows-alt');

		$this->data["supplier"][] = array("link" =>$this->url->link('setting/salesman', '&token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> $this->language->get('alt_suppliermaster'),
			                                  "id"     => $this->language->get('suppliermaster_id'),
			                               "Text" => 'Driver',
			                               "icon" =>'fa fa-truck');
		}
		// $this->data["addpayment"][] = array("link" =>"#",
		// 	                               "altkey"=> $this->language->get('alt_adjustpayment'),
		// 	                                 "id"     => $this->language->get('adjustpayment_id'),
		// 	                               "Text" => $this->language->get('text_adjust_payment'),
		// 	                               "icon" =>'fa fa-th-large');

		if($this->user->hasPermission('access', 'setting/salesperson')){
			$this->data["supplier"][] = array("link" =>$this->url->link('setting/salesperson', '&token=' . $this->session->data['token'], 'SSL'),
			                               "Text" => 'Sales Man',
			                               "icon" =>'fa fa-user');
		}

		if($this->user->hasPermission('access', 'setting/touchmenu')){
			$this->data["touchmenu"][] = array("link" =>$this->url->link('setting/touchmenu', '&token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',
			                                  "id"     => 'touchmenu',
			                               "Text" => 'Touch Menu',
			                               "icon" =>'fa fa-th-large');
		}
		if($this->user->hasPermission('access', 'setting/touchmenuitem')){
			$this->data["touchmenu"][] = array("link" =>$this->url->link('setting/touchmenuitem', '&token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',
			                                  "id"     => 'touchmenuitem',
			                               "Text" => 'Touch Menu Item Master',
			                               "icon" =>'fa fa-th-large');
		}

		if($this->user->hasPermission('access', 'setting/salesman')){
			$this->data["developeraccess"][] = array("link" =>$this->url->link('setting/developeraccess', '&token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',
			                                  "id"     => 'developeraccess',
			                               "Text" => 'Developer Access',
			                               "icon" =>'fa fa-external-link-square	');
		}

			
		$this->template = 'setting/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validate() {
	
		if (!$this->user->hasPermission('modify', 'setting/setting')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['config_name']) {
			$this->error['name'] = $this->language->get('error_name');
		}	
		
				
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
		
	public function country() {
		$json = array();
		
		$this->load->model('localisation/country');

    	$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);
		
		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']		
			);
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>