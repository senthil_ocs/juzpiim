<?php
class ControllerSettingPaymentTypeMaster extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('setting/payment_type_master');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/payment_type_master');

		$this->getList();
	}

	public function insert() {
		$this->language->load('setting/payment_type_master');
		$this->document->setTitle($this->language->get('new_title'));
		$this->load->model('setting/payment_type_master');
		//print_r($this->request); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_payment_type_master->addPaymentTypeMaster($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function update() {
		$this->language->load('setting/payment_type_master');
		$this->document->setTitle($this->language->get('edit_title'));
		$this->load->model('setting/payment_type_master');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_payment_type_master->editPaymentTypeMaster($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function delete() {
		$this->language->load('setting/payment_type_master');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/payment_type_master');
		//printArray($this->request->post);
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_setting_payment_type_master->deletePaymentTypeMaster($id);
			}
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->get['filter_name'])) {
			$url.= '&filter_name='.$this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_code'])) {
			$url.= '&filter_code='.$this->request->get['filter_code'];
		} 
		if (isset($this->request->get['filter_status'])) {
			$url.= '&filter_status='.$this->request->get['filter_status'];
		}
		if (isset($this->request->get['filter_xeroAccountCode'])) {
			$url.= '&filter_xeroAccountCode='.$this->request->get['filter_xeroAccountCode'];
		} 
		
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		} 
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		} 
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code ="";
		}
		if (isset($this->request->get['filter_xeroAccountCode'])) {
			$filter_xeroAccountCode = $this->request->get['filter_xeroAccountCode'];
		} else {
			$filter_xeroAccountCode ="";
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status ="";
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		$this->data["filterstatus"] = $filter_status;
		/*** Filter-Ends***/

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
	
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);


		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['insert'] = $this->url->link('setting/payment_type_master/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/payment_type_master/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['payment_type'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_xeroAccountCode' => $filter_xeroAccountCode,
			'filter_status' => $filter_status,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$payment_type_total = $this->model_setting_payment_type_master->getTotalPaymentTypeMaster($data);
		$results = $this->model_setting_payment_type_master->getPaymentTypeMasters($data);
		
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/payment_type_master/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);
			$this->data['paymentTypeValues'][] = array(
				'id'    => $result['id'],
				'name'   => $result['name'],
				'code'   => $result['code'],
				'xero_account_code'	   => $result['xero_account_code'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'added_date' => date($this->language->get('date_format_short'), strtotime($result['added_date'])),
				'selected'   => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_xero_account_code'] = $this->language->get('column_xero_account_code');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_added_date'] = $this->language->get('column_added_date');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . '&sort=code' . $url, 'SSL');
		$this->data['sort_xero_account_code'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . '&sort=xero_account_code' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_added_date'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . '&sort=added_date' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $payment_type_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		//print_r($this->data['pagination']);

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['payment_type'] = $this->url->link("setting/payment_type_master", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/payment_type_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_xero_account_code'] = $this->language->get('entry_xero_account_code');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('setting/payment_type_master/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/payment_type_master/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('setting/payment_type_master', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$payment_type_info = $this->model_setting_payment_type_master->getPaymentTypeMaster($this->request->get['id']);
		}
		//print_r($payment_type_info); exit;

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($payment_type_info)) {
			$this->data['name'] = $payment_type_info['name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($payment_type_info)) {
			$this->data['code'] = $payment_type_info['code'];
		} else {
			$this->data['code'] = '';
		}
		
		if (isset($this->request->post['noof_days'])) {
			$this->data['xero_account_code'] = $this->request->post['xero_account_code'];
		} elseif (!empty($payment_type_info)) {
			$this->data['xero_account_code'] = $payment_type_info['xero_account_code'];
		} else {
			$this->data['xero_account_code'] = '';
		}

		$this->load->model('user/user_group');

		$this->data['user_groups'] = $this->model_user_user_group->getUserGroups();

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($payment_type_info)) {
			$this->data['status'] = $payment_type_info['status'];
		} else {
			$this->data['status'] = '';
		}

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['payment_type'] = $this->url->link("setting/payment_type_master", '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'setting/payment_type_master';
		
		$this->template = 'setting/payment_type_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'setting/payment_type_master')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 20)) {
			$this->error['name'] = $this->language->get('error_name');
		}


		if ((utf8_strlen($this->request->post['code']) < 2) || (utf8_strlen($this->request->post['code']) > 15)) {
			$this->error['code'] = $this->language->get('error_code');
		}


		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'setting/payment_type_master')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$this->load->model('setting/payment_type_master');
		
		/*foreach ($this->request->post['selected'] as $id) {
		
			$payment_type_total = $this->model_setting_payment_type_master->getPaymentTypeMaster($id);
			if ($payment_type_total) {
				$this->error['warning'] = sprintf($this->language->get('error_account'), $payment_type_total);
			}
			
		}*/

		if (!$this->error) {
			return true;
		} else { 
			return false;
		}
	}

}
?>