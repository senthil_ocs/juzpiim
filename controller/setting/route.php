<?php 
class ControllerSettingRoute extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/route');
		$this->document->setTitle('Route');
		$this->load->model('setting/route');
		$this->getList();
		
	}
	public function insert() {

		$this->language->load('setting/route');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/route');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$e_id = $this->model_setting_route->addRoute($this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//if(!empty($e_id)){
			$this->redirect($this->url->link('setting/route', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			//}
		}
		$this->getForm();		
	}
	
	public function update() {
		$this->language->load('setting/route');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/route');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_setting_route->editRoute($this->request->post,$this->request->get['code']);
			
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/route', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();
	}
	public function getList(){
		$this->language->load('setting/route');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'addedon';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_code'])) {
			$url.= '&code='.$this->request->post['filter_code'];
		} 
		
		

		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_code'])) {
			$filter_code = $this->request->post['filter_code'];
		} else {
			$filter_code ="";
		}
		
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/route/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['routes'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$route_total = $this->model_setting_route->getTotalRoute($data);
		
		$results = $this->model_setting_route->getRoute($data);
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/route/update', 'token=' . $this->session->data['token'] . '&code=' . $result['code'] . $url, 'SSL')
			);
			
			$this->data['routes'][] = array(
				'name'			=> $result['name'],
				'code'		    => $result['code'],
				'description'	=> $result['description'],
				'status' 		=> $result['status'],
				'addedon' 		=> $result['addedon'],
				'zip_code'		=> $result['zip_code'],
				'selected'      => isset($this->request->post['selected']) && in_array($result['code'], $this->request->post['selected']),
				'action'        => $action
			);
		}
		if($result['active'] == 0){
				$active = $this->language->get('text_active');
			} else {
				$active = $this->language->get('text_inactive');
			}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
 		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Route List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['sort_route_id'] = $this->url->link('setting/route', 'token=' . $this->session->data['token'] . '&sort=sort_route_id' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $route_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/route', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['route'] = 'setting/route';

		$this->data['token'] = $this->session->data['token'];
		
		$this->template = 'setting/route_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}	
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}		
		if (isset($this->error['code_unique'])) {
			$this->data['error_code_unique'] = $this->error['code_unique'];
		} else {
			$this->data['error_code_unique'] = '';
		}
		
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Route List'),
			'href'		=> $this->url->link('setting/route', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (isset($this->request->get['code'])) {
			$this->data['getcode'] = $this->request->get['code'];
			$routedata = $this->model_setting_route->getroutedata($this->request->get['code']);
		}
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		}else if (isset($routedata['route']['name'])) {
			$this->data['name'] = $routedata['route']['name'];
		} else {
			$this->data['name'] = '';
		}
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		}else if (isset($routedata['route']['code'])) {
			$this->data['code'] = $routedata['route']['code'];
		} else {
			$this->data['code'] = '';
		}
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		}else if (isset($routedata['route']['description'])) {
			$this->data['description'] = $routedata['route']['description'];
		} else {
			$this->data['description'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		}else if (isset($routedata['route']['status'])) {
			$this->data['status'] = $routedata['route']['status'];
		} else {
			$this->data['status'] = '';
		}
		if (isset($this->request->post['zipcode'])) {
			foreach ($this->request->post['zipcode'] as $value) {
				$this->data['zipcode'][]['zip_code'] = $value;
			}
		}else if (!empty($routedata['zipcodes'])) {
			$this->data['zipcode'] = $routedata['zipcodes'];
		} else {
			$this->data['zipcode'] = '';
		}
		$this->data['action'] = '#';
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/route';
		$this->template = 'setting/route_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->data['cancel'] = $this->url->link('setting/route', 'token=' . $this->session->data['token'] . $url, 'SSL');
				
		$this->response->setOutput($this->render());
	}
	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/route')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (empty($this->request->post['code'])) {
			$this->error['code'] = $this->language->get('Enter Code');
		} else {
			$code_present='';
			if(isset($this->request->get['code'])){
				$code_present = $this->request->get['code'];
			}
			if($code_present != $this->request->post['code']){
				$codedata = $this->model_setting_route->getroutecode($this->request->post['code']);
			}

			if($codedata > 0){
				$this->error['code'] = $this->language->get('Enter Valid Code');
			}
		}
		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Please Enter Name');
		}
		if(!empty($this->request->post['zipcode'])){
			$code_present='';
			if(isset($this->request->get['code'])){
				$code_present = $this->request->get['code'];

			}
			$zipcodeData = $this->model_setting_route->getRouteZipcode($this->request->post['zipcode'],$code_present);
			if($zipcodeData > 0) {
				$this->error['warning'] = $this->language->get('Zip Code has already added!');
			}
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function checkZipcode(){
		$this->load->model('setting/route');
		$zipcodeData = $this->model_setting_route->getRouteZip($this->request->post['zipcode'],$this->request->post['getcode']);
		echo $zipcodeData;
	}
}
?>