<?php
class ControllerSettingTerms extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('setting/terms');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/terms');

		$this->getList();
	}

	public function insert() {
		$this->language->load('setting/terms');
		$this->document->setTitle($this->language->get('new_title'));
		$this->load->model('setting/terms');
		//print_r($this->request); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_terms->addTerms($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function update() {
		$this->language->load('setting/terms');
		$this->document->setTitle($this->language->get('edit_title'));
		$this->load->model('setting/terms');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_terms->editTerms($this->request->get['terms_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function delete() {
		$this->language->load('setting/terms');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/terms');
		//printArray($this->request->post);
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $terms_id) {
				$this->model_setting_terms->deleteTerms($terms_id);
			}
			exit;
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'terms_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->get['filter_name'])) {
			$url.= '&filter_name='.$this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_code'])) {
			$url.= '&filter_code='.$this->request->get['filter_code'];
		} 
		if (isset($this->request->get['filter_status'])) {
			$url.= '&filter_status='.$this->request->get['filter_status'];
		}
		if (isset($this->request->get['filter_days'])) {
			$url.= '&filter_days='.$this->request->get['filter_days'];
		} 
		
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		} 
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		} 
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->get['filter_code'])) {
			$filter_code = $this->request->get['filter_code'];
		} else {
			$filter_code ="";
		}
		if (isset($this->request->get['filter_days'])) {
			$filter_days = $this->request->get['filter_days'];
		} else {
			$filter_days ="";
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status ="";
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		$this->data["filterstatus"] = $filter_status;
		/*** Filter-Ends***/

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
	
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);


		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['insert'] = $this->url->link('setting/terms/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/terms/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['terms'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_days' => $filter_days,
			'filter_status' => $filter_status,

			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$terms_total = $this->model_setting_terms->getTotalTerms();

		$results = $this->model_setting_terms->getTerms($data);
		
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/terms/update', 'token=' . $this->session->data['token'] . '&terms_id=' . $result['terms_id'] . $url, 'SSL')
			);

			$this->data['termsValues'][] = array(
				'terms_id'    => $result['terms_id'],
				'terms_name'   => $result['terms_name'],
				'terms_code'   => $result['terms_code'],
				'noof_days'	   => $result['noof_days'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'selected'   => isset($this->request->post['selected']) && in_array($result['terms_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_terms_name'] = $this->language->get('column_termsname');
		$this->data['column_terms_code'] = $this->language->get('column_termscode');
		$this->data['column_noof_days'] = $this->language->get('column_noof_days');
		$this->data['column_status'] = $this->language->get('column_termstatus');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_terms_name'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . '&sort=terms_name' . $url, 'SSL');
		$this->data['sort_terms_code'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . '&sort=terms_code' . $url, 'SSL');
		$this->data['sort_noof_days'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . '&sort=noof_days' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $terms_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		//print_r($this->data['pagination']);

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['terms'] = $this->url->link("setting/terms", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/terms_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_days'] = $this->language->get('entry_days');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['termsname'])) {
			$this->data['error_termsname'] = $this->error['termsname'];
		} else {
			$this->data['error_termsname'] = '';
		}
		
		if (isset($this->error['termscode'])) {
			$this->data['error_termscode'] = $this->error['termscode'];
		} else {
			$this->data['error_termscode'] = '';
		}


		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		

		if (!isset($this->request->get['terms_id'])) {
			$this->data['action'] = $this->url->link('setting/terms/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/terms/update', 'token=' . $this->session->data['token'] . '&terms_id=' . $this->request->get['terms_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('setting/terms', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['terms_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$terms_info = $this->model_setting_terms->getTerm($this->request->get['terms_id']);
		}
		//print_r($terms_info); exit;

		if (isset($this->request->post['terms_name'])) {
			$this->data['terms_name'] = $this->request->post['terms_name'];
		} elseif (!empty($terms_info)) {
			$this->data['terms_name'] = $terms_info['terms_name'];
		} else {
			$this->data['terms_name'] = '';
		}
		
		if (isset($this->request->post['terms_code'])) {
			$this->data['terms_code'] = $this->request->post['terms_code'];
		} elseif (!empty($terms_info)) {
			$this->data['terms_code'] = $terms_info['terms_code'];
		} else {
			$this->data['terms_code'] = '';
		}
		
		if (isset($this->request->post['noof_days'])) {
			$this->data['noof_days'] = $this->request->post['noof_days'];
		} elseif (!empty($terms_info)) {
			$this->data['noof_days'] = $terms_info['noof_days'];
		} else {
			$this->data['noof_days'] = '';
		}

		$this->load->model('user/user_group');

		$this->data['user_groups'] = $this->model_user_user_group->getUserGroups();

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($terms_info)) {
			$this->data['status'] = $terms_info['status'];
		} else {
			$this->data['status'] = 0;
		}

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['terms'] = $this->url->link("setting/terms", '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'setting/terms';
		
		$this->template = 'setting/terms_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'setting/terms')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['terms_name']) < 3) || (utf8_strlen($this->request->post['terms_name']) > 20)) {
			$this->error['termsname'] = $this->language->get('error_termsname');
		}


		if ((utf8_strlen($this->request->post['terms_code']) < 2) || (utf8_strlen($this->request->post['terms_code']) > 15)) {
			$this->error['termscode'] = $this->language->get('error_termscode');
		}


		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'setting/terms')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$this->load->model('setting/terms');
		
		foreach ($this->request->post['selected'] as $terms_id) {
		
			$terms_total = $this->model_setting_terms->getTerm($terms_id);
			if ($terms_total) {
				$this->error['warning'] = sprintf($this->language->get('error_account'), $terms_total);
			}
			
		}

		if (!$this->error) {
			return true;
		} else { 
			return false;
		}
	}

}
?>