<?php  
class ControllerSettingCompany extends Controller { 
	private $error = array();
	
	// Company Form Fields
	public function companyFormFields() {
		$forms = array('company_id','location_code','name','logo','address1','address2','country',
						'city','state','postal_code','email','web_url','phone','fax','remarks','business_reg_no',
						'gst_reg','gst_perc','post_gst_type','purchase_gst_type','sales_gst_type','display_message1',
						'display_message2','max_discount','price_change','bill_message','print_promotion',
						'last_eod_date','config_server','config_name','config_email','location_id','currency_code','term1','term2','term3');
		return $forms;
	
	}

	public function getZones()
	{
		 $this->load->model('setting/company');
	     $country  = $this->request->post["country"];
	     $States = $this->model_setting_company->getZonesByCountry($country);
	     echo json_encode($States);
	}	
	          
	public function index() { 
		$this->load->model('tool/image');
		$this->language->load('setting/company');
		$this->load->model('setting/company');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['action'] = $this->url->link('setting/company/update', 'token=' . $this->session->data['token'], 'SSL');
		$company_id	= $this->session->data['company_id'];
		
		$formFieldNames	= $this->companyFormFields();
		if (isset($company_id)) {
      		$company_info = $this->model_setting_company->getCompanyDetails($company_id);
    	}
		//printArray($company_info);
		if(isset($company_info['logo'])) {
			$this->data['preview_logo'] = $this->model_tool_image->resize($company_info['logo'], 75, 75);
		} else {
			$this->data['preview_logo']	='';
		}
		// printArray($formFieldNames); exit;
		
		if(!empty($formFieldNames)) {
			foreach($formFieldNames as $formvalue){
				if(isset($this->request->post[$formvalue])) {
					$this->data[$formvalue]	= $this->request->post[$formvalue];
				} elseif (!empty($company_info[$formvalue])) {
					$this->data[$formvalue]	= $company_info[$formvalue];
				} else {
					$this->data[$formvalue]	='';
				}
			}
		}
		
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_companyname'] = $this->language->get('entry_companyname');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_state'] = $this->language->get('entry_state');
		$this->data['entry_postal'] = $this->language->get('entry_postal');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_weburl'] = $this->language->get('entry_weburl');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_businessregno'] = $this->language->get('entry_businessregno');
		$this->data['entry_gstreg'] = $this->language->get('entry_gstreg');
		$this->data['entry_gstperc'] = $this->language->get('entry_gstperc');
		$this->data['entry_postgsttype'] = $this->language->get('entry_postgsttype');
		$this->data['entry_purchasegsttype'] = $this->language->get('entry_purchasegsttype');
		$this->data['entry_salesgsttype'] = $this->language->get('entry_salesgsttype');
		$this->data['entry_displaymessages1'] = $this->language->get('entry_displaymessages1');
		$this->data['entry_displaymessages2'] = $this->language->get('entry_displaymessages2');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_pricechange'] = $this->language->get('entry_pricechange');
		$this->data['entry_billmessage'] = $this->language->get('entry_billmessage');
		$this->data['entry_promotions'] = $this->language->get('entry_promotions');
		$this->data['entry_lastdate'] = $this->language->get('entry_lastdate');
		$this->data['entry_configemail'] = $this->language->get('entry_configemail');
		$this->data['entry_server'] = $this->language->get('entry_server');
		$this->data['entry_displayname'] = $this->language->get('entry_displayname');
		$this->data['entry_fromemailid'] = $this->language->get('entry_fromemailid');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_cpassword'] = $this->language->get('entry_cpassword');
		$this->data['entry_company_logo'] = $this->language->get('entry_company_logo');
		
		$this->data['cancel'] = $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['button_save'] = 'Save';
		$this->data['button_update'] = $this->language->get('button_update');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/company', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);

		$this->data['token'] = $this->session->data['token'];
		$this->data['countrys'] = $this->model_setting_company->getCountry();
		$this->data['states'] = $this->model_setting_company->getZone();
		$this->data['currencys'] = $this->model_setting_company->getCurrency();
		$this->data['route'] = $this->request->get['route'];	
		$this->data['locations'] = $this->model_setting_company->getLocations();
		$this->data['location_code'] = $company_info['location_code'];
		$this->data['user_group_id'] = $this->model_setting_company->getUserGroupIdByUserId($_SESSION['user_id']);
		// printArray($this->data['currencys']); die;
		
		$this->template = 'setting/company.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	
	public function update() { 
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
		

		$company_id	= $this->session->data['company_id'];
		$this->load->model('setting/company');
		$this->language->load('setting/company');
		$formFieldNames	= $this->companyFormFields();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')  && $this->validateForm() ) {
			
			//$this->load->model('tool/image');
			//echo $testImage	= $this->model_tool_image->resize($this->request->post['CompanyLogo'], 100, 100);
			$imageName	= $this->imageUploads();
			if($imageName) {
				$this->request->post['logo']	= $imageName; 
			}
			
			$user_group_id = $this->model_setting_company->getUserGroupIdByUserId($_SESSION['user_id']);
			
			//if($user_group_id['user_group_id'] == TOP_GROUP){
				$strExplode = explode('|', $this->request->post['location_id']);
				$this->request->post['location_id'] = $strExplode[0];
				$this->request->post['location_code'] = $strExplode[1];
			//}

			$result	= $this->model_setting_company->editCompanyDetails($company_id, $this->request->post,$formFieldNames);
			
			if($result) {
				$this->session->data['success'] = $this->language->get('text_success');
				$this->data['success']	='Successfully update company details.';
			}
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
		}
		$this->redirect(HTTPS_SERVER . 'index.php?route=setting/company&token=' . $this->session->data['token'] . $url);
  	}
	
	public function getExtension($str) {
	
	 $i = strrpos($str,".");
	 if (!$i) { return ""; } 
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
	 
	}
	
	public function imageUploads() {
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$name = $_FILES['logo']['name'];
		$size = $_FILES['logo']['size'];
		if(strlen($name)){
			$ext = $this->getExtension($name);
			if(in_array($ext,$valid_formats)) {
				if($size<(1024*1024)) {
					$txt	='';
					$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['logo']['tmp_name'];
					if(move_uploaded_file($tmp, DIR_IMAGE.$actual_image_name)) {
						return $actual_image_name;
					}
				}
			} 
		}
		$this->error['warning'] = 'unable to upload image. Please contact administrator';
		return false;
	}

	public function validateForm() {
		//return true;
		if (!$this->user->hasPermission('modify', 'setting/company')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
    	//if ((utf8_strlen($this->request->post['password']) < 6)) {
      		//$this->error['password'] = $this->language->get('error_password');
    	//}
		
    	if ($this->request->post['currency_code'] =='') {
      		$this->error['warning'] = 'Please Select Currency';
    	}  
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}  
?>