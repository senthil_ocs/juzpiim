<?php
class ControllerSettingDeveloperaccess extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/developeraccess'); 

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->template = 'setting/developeraccess.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
		//$this->load->model('setting/setting');
		$this->load->model('setting/developeraccess');
		//printArray($this->config); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			if($this->request->post['submit'] == 'purchase_invoice_revert') {
				$resultant = $this->model_setting_developeraccess->purchaseInvoiceRevert($this->request->post);
			}

			if($this->request->post['submit'] == 'purchase_order_revert') {
				$resultant = $this->model_setting_developeraccess->purchaseOrderRevert($this->request->post);

			}
			if($resultant['status'] == 200) {
				$this->session->data['success'] = $resultant['message'];
			}else{
				$this->error['warning'] = $resultant['message'];
			}
			//$this->redirect($this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->data['heading_title'] = 'Developer Access';
		$this->data['text_status'] = $this->language->get('text_status');
		$this->data['text_general'] = $this->language->get('text_general');
		$this->data['text_pi_revert'] = $this->language->get('text_pi_revert');
		$this->data['text_po_revert'] = $this->language->get('text_po_revert');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Developer Access',
			'href'      => $this->url->link('setting/developeraccess', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('setting/developeraccess', 'token=' . $this->session->data['token'], 'SSL');		
		$this->data['cancel'] = $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['button_save'] = 'Save';
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		

	
		if (isset($this->request->post['purchase_invoice_no'])) {
			$this->data['purchase_invoice_no'] = ''; //(string)$this->request->post['purchase_invoice_no'];
		} else {
			$this->data['purchase_invoice_no'] = '';
		}
		
        if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
            $this->data['allowCronJob'] = 1;
        }
		$this->data['route'] = $this->request->get['route'];
		$this->template = 'setting/developeraccess.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
}
?>