<?php  
class ControllerSettingLocation extends Controller { 
	private $error = array();
	public function index() { 
		$this->language->load('setting/location');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/location');
		$this->getList();
  	}
	
	public function insert() {

		$this->language->load('setting/location');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/location');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_location->addLocation($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('setting/location', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function update() {
	
		$this->language->load('setting/location');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/location');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_location->editLocation($this->request->post,$this->request->get['location_id']);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('setting/location', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function delete() {
    	$this->language->load('setting/location');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/location');
		
		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $location_id) {
				$this->model_setting_location->deleteLocation($location_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/location', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'location_name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('setting/location/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/location/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['brands'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$location_total = $this->model_setting_location->getTotalLocation();
		
		$results = $this->model_setting_location->getLocations($data);
		
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/location/update', 'token=' . $this->session->data['token'] . '&location_id=' . $result['location_id'] . $url, 'SSL')
			);		

			$this->data['locations'][] = array(
				'location_id' 	=> $result['location_id'],
				'name'      	=> $result['location_name'],
				'code'      	=> $result['location_code'],
				'type'      	=> $result['type'], // Code changed on 10-10-2017
				'country'      	=> $result['country'],
				'phone'			=> $result['phone'],				
				'selected'   => isset($this->request->post['selected']) && in_array($result['location_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_country'] = $this->language->get('column_country');
		$this->data['column_location_code'] = $this->language->get('column_location_code');
		$this->data['column_phone'] = $this->language->get('column_phone');
		$this->data['column_action'] = $this->language->get('column_action');	

		// New Code Added 10-10-2017
		$this->data['column_type'] = $this->language->get('column_type');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/location', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['location_name'] = $this->url->link('setting/location', 'token=' . $this->session->data['token'] . '&sort=location_name' . $url, 'SSL');
		$this->data['country'] = $this->url->link('setting/location', 'token=' . $this->session->data['token'] . '&sort=country' .$url, 'SSL');		

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $location_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/location', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['setting'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['route'] = $this->request->get['route'];
		
		$this->template = 'setting/location_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_postcode'] = $this->language->get('entry_postcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');

		// New Code Added 10-10-2017
		$this->data['entry_type'] = $this->language->get('entry_type'); 
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('setting/location', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

		// New Code Added 10-10-2017
		if (isset($this->error['type'])) {
			$this->data['error_type'] = $this->error['type'];
		} else {
			$this->data['error_type'] = '';
		}
		

		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}

		if (isset($this->error['postcode'])) {
			$this->data['error_postcode'] = $this->error['postcode'];
		} else {
			$this->data['error_postcode'] = '';
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['location_id'])) { 
			$this->data['action'] = $this->url->link('setting/location/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/location/update', 'token=' . $this->session->data['token'] . '&location_id=' . $this->request->get['location_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('setting/location', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['location_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$location_info = $this->model_setting_location->getLocation($this->request->get['location_id']);
		}		

		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($location_info)) {
			$this->data['code'] = $location_info['location_code'];
		} else {
			$this->data['code'] = '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($location_info)) {
			$this->data['name'] = trim($location_info['location_name']);
		} else {
			$this->data['name'] = '';
		}

		// New Code Added 10-10-2017
		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (!empty($location_info)) {
			$this->data['type'] = $location_info['type'];
		} else {
			$this->data['type'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($location_info)) {
			$this->data['address1'] = trim($location_info['address1']);
		} else {
			$this->data['address1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($location_info)) {
			$this->data['address2'] = trim($location_info['address2']);
		} else {
			$this->data['address2'] = '';
		}

		if (isset($this->request->post['country'])) {
			$this->data['country'] = $this->request->post['country'];
		} elseif (!empty($location_info)) {
			$this->data['country'] = trim($location_info['country']);
		} else {
			$this->data['country'] = '';
		}

		if (isset($this->request->post['postcode'])) {
			$this->data['postcode'] = $this->request->post['postcode'];
		} elseif (!empty($location_info)) {
			$this->data['postcode'] = trim($location_info['postcode']);
		} else {
			$this->data['postcode'] = '';
		}

		if (isset($this->request->post['phone'])) {
			$this->data['phone'] = $this->request->post['phone'];
		} elseif (!empty($location_info)) {
			$this->data['phone'] = trim($location_info['phone']);
		} else {
			$this->data['phone'] = '';
		}
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($location_info)) {
			$this->data['email'] = trim($location_info['email']);
		} else {
			$this->data['email'] = '';
		}
		if (isset($this->request->post['business_reg_no'])) {
			$this->data['business_reg_no'] = $this->request->post['business_reg_no'];
		} elseif (!empty($location_info)) {
			$this->data['business_reg_no'] = trim($location_info['business_reg_no']);
		} else {
			$this->data['business_reg_no'] = '';
		}


		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['setting'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['location'] = $this->url->link('setting/location', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'setting/location';

		$this->template = 'setting/location_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'setting/location')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (empty($this->request->post['code'])) {
			$this->error['code'] = $this->language->get('Country Code Should not be empty!');
		}

		// New Code Added 10-10-2017
		if (empty($this->request->post['type'])) {
			$this->error['type'] = $this->language->get('Please Select Type!');
		}	
		
		if (empty($this->request->post['country'])) {
			$this->error['country'] = $this->language->get('Country Should not be empty!');
		}

		if (empty($this->request->post['postcode'])) {
			$this->error['postcode'] = $this->language->get('Postcode Should not be empty!');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
}  
?>