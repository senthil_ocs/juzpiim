<?php 
class ControllerSettingCustomers extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/customers');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/customers');
		$this->getList();
	}

	public function insert() {
		$this->language->load('setting/customers');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/customers');
		$type = 'insert';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type)) {
			
			$cciid = $this->model_setting_customers->addCustomer($this->request->post);
			$this->session->data['success'] = 'Success: You have added customer!';
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$type = $this->request->get['type'];
			if($type == 'pos'){
				$this->session->data["attachedcustomer"] = $cciid;
				$this->redirect($this->url->link('pos/pos', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} 
			else if(isset($this->request->get['from']) && $this->request->get['from'] !=''){
				$this->redirect($this->url->link('transaction/'.$this->request->get['from'].'/insert', 'token=' . $this->session->data['token'].'&newCustId='.$cciid, 'SSL'));
			}
			else {
				$this->redirect($this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
		}
		$this->getForm();		
	}

	public function update() {

		$this->language->load('setting/customers');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/customers');
		$type = 'update';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type,$this->request->get['customer_id'])) {
			$this->model_setting_customers->editCustomer($this->request->post,$this->request->get['customer_id']);	
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			/*** Filter- Starts***/
			if (isset($_REQUEST['filter_name'])) {
				$url.= '&filter_name='.$_REQUEST['filter_name'];
				$filter_name = $_REQUEST['filter_name'];
			}
			if (isset($_REQUEST['filter_email'])) {
				$url.= '&filter_email='.$_REQUEST['filter_email'];
				$filter_email = $_REQUEST['filter_email'];
			} 
			if (isset($_REQUEST['filter_Type'])) {
				$filter_Type = $_REQUEST['filter_Type'];
				$url.= '&filter_Type='.$_REQUEST['filter_Type'];
			}
			if (isset($_REQUEST['filter_date_from'])) {
				$url.= '&filter_date_from='.$_REQUEST['filter_date_from'];
				$filter_date_from = $_REQUEST['filter_date_from'];
			} 
			if (isset($_REQUEST['filter_date_to'])) {
				$filter_date_to = $_REQUEST['filter_date_to'];
				$url.= '&filter_date_to='.$_REQUEST['filter_date_to'];
			}

			if($type == 'pos'){
				$this->redirect($this->url->link('pos/pos', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
				$this->redirect($this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
		}
		$this->getForm();
	}

	public function delete() {

    	$this->language->load('setting/customers');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/customers');
		
		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $customer_id) {
				$this->model_setting_customers->deleteCustomer($customer_id);
			}

			$this->session->data['success']= $this->language->get('text_success');
			$url = '';	
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
  	}

	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}	
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		// printArray($this->request->post); die;
		$page = 1;
		if(!empty($this->request->post)) {
			$page = 1;
		}else if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}

		$url = '';
		if (isset($_REQUEST['filter_name'])) {
			$url.= '&filter_name='.$_REQUEST['filter_name'];
			$filter_name = $_REQUEST['filter_name'];
		}
		if (isset($_REQUEST['filter_email'])) {
			$url.= '&filter_email='.$_REQUEST['filter_email'];
			$filter_email = $_REQUEST['filter_email'];
		} 
		if (isset($_REQUEST['filter_Type'])) {
			$filter_Type = $_REQUEST['filter_Type'];
			$url.= '&filter_Type='.$_REQUEST['filter_Type'];
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$url.= '&filter_date_from='.$_REQUEST['filter_date_from'];
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$url.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if (isset($_REQUEST['xero_button'])) {
			$xero_button = $_REQUEST['xero_button'];
			$url.= '&xero_button='.$_REQUEST['xero_button'];
		}

		$this->data["filtertype"] = $filter_Type;
  		$this->data['insert'] = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/customers/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' 		=> $filter_name,
			'filter_email' 		=> $filter_email,
			'filter_Type' 		=> $filter_Type,
			'filter_date_from' 	=> $filter_date_from,
			'filter_date_to' 	=> $filter_date_to,
			'xero_button' 		=> $xero_button
		);
		
		$this->data['data'] = $data;
		$customer_total 	= $this->model_setting_customers->getTotalCustomers($data);
		$results 			= $this->model_setting_customers->getCustomers($data);

		foreach ($results as $result) {
			$action   = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customercode'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customercode'		=> $result['cust_code'],
				'customer_id' 		=> $result['customercode'],
				'name'      	    => $result['name'],
				'xero_id'      	    => $result['xero_id'],
				'address'			=> $result['address1'],
				'mobile'			=> $result['mobile'],
				'added_date'		=> $result['added_date'],				
				'selected'          => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
				'shipping_address'  => $this->url->link('setting/customers/shipping_address', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customercode'] . $url, 'SSL'),
				'edit_button'       => $this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customercode'] . $url, 'SSL')
			);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'   => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer List'),
			'href' => $this->url->link('setting/customers','token='.$this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['sort_name'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		

		$pagination 		= new Pagination();
		$pagination->total 	= $customer_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->template 	 = 'setting/customer_list.tpl';
		$this->children 	 = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function getForm() {

		$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['entry_name'] 	  = $this->language->get('entry_name');
		$this->data['entry_code'] 	  = $this->language->get('entry_code');
		$this->data['entry_address']  = $this->language->get('entry_address');
		$this->data['entry_country']  = $this->language->get('entry_country');
		$this->data['entry_zipcode']  = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] 	  = $this->language->get('entry_phone');
		$this->data['entry_fax'] 	  = $this->language->get('entry_fax');
		$this->data['entry_email'] 	  = $this->language->get('entry_email');
		$this->data['entry_attn'] 	  = $this->language->get('entry_attn');
		$this->data['entry_remarks']  = $this->language->get('entry_remarks');
		$this->data['entry_gst'] 	  = $this->language->get('entry_gst');
		$this->data['entry_status']   = $this->language->get('entry_status');
		$this->data['button_save'] 	  = $this->language->get('button_save');
		$this->data['button_cancel']  = $this->language->get('button_cancel');

		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
 		if (isset($this->error['email_duplication'])) {
			$this->data['error_email_duplication'] = $this->error['email_duplication'];
		} else {
			$this->data['error_email_duplication'] = '';
		}
			
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($_REQUEST['filter_name'])) {
			$url.= '&filter_name='.$_REQUEST['filter_name'];
			$filter_name = $_REQUEST['filter_name'];
		}
		if (isset($_REQUEST['filter_email'])) {
			$url.= '&filter_email='.$_REQUEST['filter_email'];
			$filter_email = $_REQUEST['filter_email'];
		} 
		if (isset($_REQUEST['filter_Type'])) {
			$filter_Type = $_REQUEST['filter_Type'];
			$url.= '&filter_Type='.$_REQUEST['filter_Type'];
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$url.= '&filter_date_from='.$_REQUEST['filter_date_from'];
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$url.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if (isset($_REQUEST['xero_button'])) {
			$xero_button = $_REQUEST['xero_button'];
			$url.= '&xero_button='.$_REQUEST['xero_button'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer List'),
			'href'		=> $this->url->link('setting/customers', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_setting_customers->getCustomer($this->request->get['customer_id']);
		}

		if (!empty($customer_info))	 {
			$this->data['added_date'] = $customer_info['added_date'];
		}
		if (isset($this->request->post['discount'])) {
			$this->data['discount'] = $this->request->post['discount'];
		} elseif (!empty($customer_info)) {
			$this->data['discount'] = $customer_info['discount'];
		} else {
			$this->data['discount'] = '';
		}

		if (isset($this->request->post['salestax'])) {
			$this->data['salestax'] = $this->request->post['salestax'];
		} elseif (!empty($customer_info)) {
			$this->data['salestax'] = $customer_info['salestax'];
		} else {
			$this->data['salestax'] = '';
		}

		if(isset($this->request->post['tax_allow'])){
			$this->data['tax_allow'] = $this->request->post['tax_allow'];
		} elseif(!empty($customer_info)) {
			$this->data['tax_allow'] = $customer_info['tax_allow'];	
		} else {
			$this->data['tax_allow'] = '1';
		}

		if(isset($this->request->post['tax_type'])){
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif(!empty($customer_info)) {
			$this->data['tax_type'] = $customer_info['tax_type'];	
		} else {
			$this->data['tax_type'] == '';
		}

		if(isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif(!empty($customer_info)) {
			$this->data['status'] = $customer_info['status'];
		} else {
			$this->data['status'] == '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($customer_info)) {
			$this->data['name'] = trim($customer_info['name']);
		} else {
			$this->data['name'] = '';
		}		
		
		if (isset($this->request->post['customer_title'])) {
			$this->data['customer_title'] = $this->request->post['customer_title'];
		} elseif (!empty($customer_info)) {
			$this->data['customer_title'] = trim($customer_info['customer_title']);
		} else {
			$this->data['customer_title'] = '';
		}

		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
		} elseif (!empty($customer_info)) {
			$this->data['company'] = trim($customer_info['company']);
		} else {
			$this->data['company'] = '';
		}
		
		if (isset($this->request->post['registration_no'])) {
			$this->data['registration_no'] = $this->request->post['registration_no'];
		} elseif (!empty($customer_info)) {
			$this->data['registration_no'] = trim($customer_info['registration_no']);
		} else {
			$this->data['registration_no'] = '';
		}
		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($customer_info)) {
			$this->data['currency_code'] = trim($customer_info['currency_code']);
		} else {
			$this->data['currency_code'] = '';
		}

		// echo $this->data['company']; die;
		if (isset($this->request->post['birth_date'])) {
			$this->data['birth_date'] = $this->request->post['birth_date'];
		} elseif (!empty($customer_info)) {
			if(date('Y',strtotime($customer_info['birth_date'])) > '1971'){
				$this->data['birth_date'] = date('d/m/Y',strtotime($customer_info['birth_date']));
			}
		} else {
			$this->data['birth_date'] = '';
		}
		// echo $this->data['birth_date']; die;

		if (isset($this->request->post['home'])) {
			$this->data['home'] = $this->request->post['home'];
		} elseif (!empty($customer_info)) {
			$this->data['home'] = trim($customer_info['home']);
		} else {
			$this->data['home'] = '';
		}

		if (isset($this->request->post['work'])) {
			$this->data['work'] = $this->request->post['work'];
		} elseif (!empty($customer_info)) {
			$this->data['work'] = trim($customer_info['work']);
		} else {
			$this->data['work'] = '';
		}

		if (isset($this->request->post['contacts'])) {

			$this->data['contacts'] = $this->request->post['contacts'];
		} elseif (!empty($customer_info)) {
			$this->data['contacts'] = trim($customer_info['mobile']);
		} else {
			$this->data['contacts'] = '';
		}
		if (isset($this->request->post['pager'])) {
			$this->data['pager'] = $this->request->post['pager'];
		} elseif (!empty($customer_info)) {
			$this->data['pager'] = trim($customer_info['pager']);
		} else {
			$this->data['pager'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (!empty($customer_info)) {
			$this->data['fax'] = trim($customer_info['fax']);
		} else {
			$this->data['fax'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($customer_info)) {
			$this->data['address1'] = trim($customer_info['address1']);
		} else {
			$this->data['address1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($customer_info)) {
			$this->data['address2'] = trim($customer_info['address2']);
		} else {
			$this->data['address2'] = '';
		}

		if (isset($this->request->post['city'])) {
			$this->data['city'] = $this->request->post['city'];
		} elseif (!empty($customer_info)) {
			$this->data['city'] = trim($customer_info['city']);
		} else {
			$this->data['city'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
		} elseif (!empty($customer_info)) {
			$this->data['state'] = trim($customer_info['state']);
		} else {
			$this->data['state'] = '';
		}

		if (isset($this->request->post['zipcode'])) {
			$this->data['zipcode'] = $this->request->post['zipcode'];
		} elseif (!empty($customer_info)) {
			$this->data['zipcode'] = trim($customer_info['zipcode']);
		} else {
			$this->data['zipcode'] = '';
		}

		if (isset($this->request->post['website'])) {
			$this->data['website'] = $this->request->post['website'];
		} elseif (!empty($customer_info)) {
			$this->data['website'] = trim($customer_info['website']);
		} else {
			$this->data['website'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($customer_info)) {
			$this->data['email'] = trim($customer_info['email']);
		} else {
			$this->data['email'] = '';
		}
		if (isset($this->request->post['notes'])) {
			$this->data['notes'] = $this->request->post['notes'];
		} elseif (!empty($customer_info)) {
			$this->data['notes'] = trim($customer_info['notes']);
		} else {
			$this->data['notes'] = '';
		}
		if (isset($this->request->post['customer_code'])) {
			$this->data['customer_code'] = $this->request->post['customer_code'];
		} elseif (!empty($customer_info)) {
			$this->data['customer_code'] = trim($customer_info['cust_code']);
		} else {
			$this->data['customer_code'] = $this->getRandomCustCode();
		}
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($customer_info)) {
			$this->data['term_id'] = $customer_info['term_id'];
		} else {
			$this->data['term_id'] = '1';
		}
		if (isset($this->request->post['country'])) {
			$this->data['country'] = $this->request->post['country'];
		} elseif (!empty($customer_info)) {
			$this->data['country'] = $customer_info['country'];
		} else {
			$this->data['country'] = '';
		}

		if(isset($this->request->get['from']) && $this->request->get['from'] !=''){
			$this->data['cancel'] = $this->url->link('transaction/'.$this->request->get['from'].'/insert', 'token=' . $this->session->data['token'], 'SSL');
		}else{
			$this->data['cancel'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['allTerms'] 		   = $this->cart->getAllTerms();
		$this->data['countryes'] 		   = $this->cart->getCountry();
		$this->data['currency_collection'] = $this->model_setting_customers->getCurrency();
		$this->data['CustomersTypes'] 	   = $this->model_setting_customers->getTypes();
		$this->data['taxrates'] 		   = $this->model_setting_customers->getTaxRates();
		$this->data['discounts'] 		   = $this->model_setting_customers->getDiscounts();
		$this->data['token']      = $this->session->data['token'];
		$this->data['route'] 	  = 'setting/customers';
		$this->template 		  = 'setting/customer_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function getTypeDetails()
	{
		$this->load->model('setting/customers');
		$customertype_id = $this->request->post["customer_type"];    	
     	$TypeDetails = $this->model_setting_customers->getTypeDetailsList($customertype_id);
     	echo json_encode($TypeDetails);     	
	}

	public function validateForm($type='',$customer_id='')
	{
		if (!$this->user->hasPermission('modify', 'setting/customers')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (empty($this->request->post['name'])) {
			$this->error['warning'] = $this->language->get('Enter Name');
		}
		if (empty($this->request->post['address1'])) {
			$this->error['warning'] = $this->language->get('Enter Address');
		}
		if (empty($this->request->post['zipcode'])) {
			$this->error['warning'] = $this->language->get('Enter Zipcode');
		}
		
		if(empty($this->request->post['customer_code'])) {
			$this->error['warning'] = 'Enter Customer Code';
		}
		/*if($type=='insert' && $this->request->post['customer_code']!=''){
			$code_info = $this->model_setting_customers->getCustomerByCust_code($this->request->post['customer_code']);
			if($code_info){
				$this->error['warning'] = 'Customer Code Already Exist';
			}
		}*/
		if($type=='insert' && $this->request->post['name']!=''){
			$code_info = $this->model_setting_customers->getCustomerByName($this->request->post['name']);
			if($code_info){
				$this->error['warning'] = 'Customer Name Already Exist';
			}
		}

		/*if (empty($this->request->post['email']) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
			$this->error['warning'] = $this->language->get('Enter Email');
		} else {
			$emaildata = $this->model_setting_customers->getcustomeremail($this->request->post['email'],$this->request->get['customer_id']);
			if($emaildata > 0){
				$this->error['email_duplication'] = $this->language->get('Email has already been added!');
			}
		}*/
		if($type!='update'){
			$user_info = $this->model_setting_customers->getCustomerByEmail($this->request->post['email']);
		}
		if(!isset($this->request->get['customercode'])){
			if (($this->request->post['email'] !='') && ($user_info['email'])){
				$this->error['warning'] = $this->language->get('Email Already Registered');
			}
		} else{
			if (($this->request->post['email'] !='') && ($user_info) && $user_info['customercode']!=$this->request->get['customercode']){
				$this->error['warning'] = $this->language->get('Email Already Registered');
			}
		}

		if (empty($this->request->post['contacts'])) {
			$this->error['warning'] = $this->language->get('Enter Phone No');
		}else{
			$user_info = $this->model_setting_customers->getCustomerByPhone($this->request->post['contacts'],$customercode);
			if($user_info && $type!='update'){
				$this->error['warning'] = 'Phone No already exist';
			}
		}

		if(empty($this->request->post['customer_code'])) {
			$this->error['warning'] = 'Customer code should not empty';
		}else{
			if($customer_id=''){
				$user_info = $this->model_setting_customers->getCustomerByCode($this->request->post['customer_code']);
				if($user_info){
					$this->error['warning'] = 'Customer code Already Exist';
				}
			}
		}

		if (($this->request->post['website']) != '' && !preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',$this->request->post['website'])) {
			$this->error['warning'] = "Please Enter Valid Web Address";
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	/*customer type list 29-feb-2016*/
	public function customertype(){
		$this->language->load('setting/customers');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/customers');

		$this->getTypeList();

	}

	public function inserttype() {

		$this->language->load('setting/customers');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/customers');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormType()) {

			$this->model_setting_customers->addCustomerType($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getFormType();		
	}
	public function updateType() {

		$this->language->load('setting/customers');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/customers');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFormType()) {

			$this->model_setting_customers->editCustomerType($this->request->post,$this->request->get['customertype_id']);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getFormType();
	}

	public function deletetype() {
    	$this->language->load('setting/customers');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/customers');
		
		if (isset($this->request->post['selected']) /*&& $this->validateDelete()*/) {
			foreach ($this->request->post['selected'] as $customertype_id) {
				$this->model_setting_customers->deleteCustomerType($customertype_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getTypeList();
  	}
  	


	public function getFormType() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_tax_category'] = $this->language->get('entry_tax_category');
		
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['discount'])) {
			$this->data['error_discount'] = $this->error['discount'];
		} else {
			$this->data['error_discount'] = '';
		}

		if (isset($this->error['taxcategory'])) {
			$this->data['error_taxcategory'] = $this->error['taxcategory'];
		} else {
			$this->data['error_taxcategory'] = '';
		}
	
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer Type List'),
			'href'      => $this->url->link('setting/customers/customertype', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['customertype_id'])) { 
			$this->data['action'] = $this->url->link('setting/customers/inserttype', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/customers/updatetype', 'token=' . $this->session->data['token'] . '&customertype_id=' . $this->request->get['customertype_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['customertype_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customertype_info = $this->model_setting_customers->getCustTypes($this->request->get['customertype_id']);
		}

		//printArray($this->request->get['customertype_id']); //die;

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($customertype_info)) {
			$this->data['name'] = trim($customertype_info['name']);
		} else {
			$this->data['name'] = '';
		}	

		if (isset($this->request->post['discount'])) {
			$this->data['discount'] = $this->request->post['discount'];
		} elseif (!empty($customertype_info)) {
			$this->data['discount'] = $customertype_info['discount'];
		} else {
			$this->data['discount'] = '';
		}

		if (isset($this->request->post['taxcategory'])) {
			$this->data['taxcategory'] = $this->request->post['taxcategory'];
		} elseif (!empty($customertype_info)) {
			$this->data['taxcategory'] = $customertype_info['taxcategory'];
		} else {
			$this->data['taxcategory'] = '';
		}

		$this->data['taxrates'] = $this->model_setting_customers->getTaxRates();
		$this->data['discounts'] = $this->model_setting_customers->getDiscounts();

		$this->data['route'] = 'setting/customers/customertype';

		$this->template = 'setting/customertype_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getTypeList(){

		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_discount'])) {
			$filter_discount = $this->request->post['filter_discount'];
		} else {
			$filter_discount ="";
		}
		if (isset($this->request->post['filter_taxcategory'])) {
			$filter_taxcategory = $this->request->post['filter_taxcategory'];
		} else {
			$filter_taxcategory ="";
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}	

		$this->data['taxrates'] = $this->model_setting_customers->getTaxRates();
		$this->data['discounts'] = $this->model_setting_customers->getDiscounts();
		
  		$this->data['insert'] = $this->url->link('setting/customers/inserttype', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/customers/deletetype', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['customerstype'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_discount' => $filter_discount,
			'filter_taxcategory' => $filter_taxcategory,
		);

		$customertype_total = $this->model_setting_customers->getTotalCustomerType($data);
		
		$results = $this->model_setting_customers->getCustomerType($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/customers/updatetype', 'token=' . $this->session->data['token'] . '&customertype_id=' . $result['customertype_id'] . $url, 'SSL')
			);

			$this->data['customerstype'][] = array(
				'customertype_id' 	=> $result['customertype_id'],
				'name'      	    => $result['name'],
				'discount'      	=> $result['discount'],
				'taxcategory'		=> $result['taxcategory'],
				'added_date'		=> $result['added_date'],				
				'selected'   => isset($this->request->post['selected']) && in_array($result['customertype_id'], $this->request->post['selected']),
				'action'     => $action
			);

			//printArray($this->data['customerstype']);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_discount'])) {
			$url.= '&filter_discount='.$this->request->post['filter_discount'];
		}
		if (isset($this->request->post['filter_taxcategory'])) {
			$url.= '&filter_taxcategory='.$this->request->post['filter_taxcategory'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer Type List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_discount'] = $this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . '&sort=discount' . $url, 'SSL');
		$this->data['sort_taxcategory'] = $this->url->link('setting/customers/customertype', 'token=' . $this->session->data['token'] . '&sort=taxcategory' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $customertype_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];	

		$this->template = 'setting/customertype_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}
	public function validateFormType()
	{
		if (!$this->user->hasPermission('modify', 'setting/customers')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$customer_info = $this->model_setting_customers->getCustomerGroup($this->request->post['name'],$this->request->get['customertype_id']);

		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Enter name');
		} elseif($this->request->post['name'] != '' && count($customer_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}
		

		if (empty($this->request->post['discount'])) {
			$this->error['discount'] = $this->language->get('Select discount');
		}		

		
		if (empty($this->request->post['taxcategory'])) {
			$this->error['taxcategory'] = $this->language->get('Select taxcategory');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function shipping_address(){

		$this->document->setTitle('Shipping Adddress');
		$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['entry_name']     = $this->language->get('entry_name');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_tax_category'] = $this->language->get('entry_tax_category');
		$this->data['button_save']    = $this->language->get('button_save');
		$this->data['button_cancel']  = $this->language->get('button_cancel');
		$this->load->model('setting/customers');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
			
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($_REQUEST['filter_name'])) {
			$url.= '&filter_name='.$_REQUEST['filter_name'];
			$filter_name = $_REQUEST['filter_name'];
		}
		if (isset($_REQUEST['filter_email'])) {
			$url.= '&filter_email='.$_REQUEST['filter_email'];
			$filter_email = $_REQUEST['filter_email'];
		} 
		if (isset($_REQUEST['filter_Type'])) {
			$filter_Type = $_REQUEST['filter_Type'];
			$url.= '&filter_Type='.$_REQUEST['filter_Type'];
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$url.= '&filter_date_from='.$_REQUEST['filter_date_from'];
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$url.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if (isset($_REQUEST['xero_button'])) {
			$xero_button = $_REQUEST['xero_button'];
			$url.= '&xero_button='.$_REQUEST['xero_button'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Customers List',
			'href'      => $this->url->link('setting/customers', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Shipping Address'),
			'href'      => '#',			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['action'] = $this->url->link('setting/customers/insertShippingAddress', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['cancel'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['customer']=$this->model_setting_customers->getCustomer($this->request->get['customer_id']);
		$this->data['shipping_address'] = $this->model_setting_customers->getShippingAddress($this->request->get['customer_id']);
		// printArray($this->data['shipping_address']); die;

		$this->data['route'] = 'setting/customers/customertype';
		$this->template = 'setting/shipping_address_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function insertShippingAddress(){
		$this->load->model('setting/customers');
		$this->model_setting_customers->addShippingAddress($this->request->post);
		
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($_REQUEST['filter_name'])) {
			$url.= '&filter_name='.$_REQUEST['filter_name'];
			$filter_name = $_REQUEST['filter_name'];
		}
		if (isset($_REQUEST['filter_email'])) {
			$url.= '&filter_email='.$_REQUEST['filter_email'];
			$filter_email = $_REQUEST['filter_email'];
		} 
		if (isset($_REQUEST['filter_Type'])) {
			$filter_Type = $_REQUEST['filter_Type'];
			$url.= '&filter_Type='.$_REQUEST['filter_Type'];
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$url.= '&filter_date_from='.$_REQUEST['filter_date_from'];
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$url.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}

		$this->redirect($this->url->link('setting/customers', '&token=' . $this->session->data['token'].'&customer_id='.$this->request->post['customer_id'] .$url, 'SSL'));
	}
	public function getRandomCustCode(){
	    $lastCustId = $this->model_setting_customers->getCustLastId();
		return 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);
	}
}
?>