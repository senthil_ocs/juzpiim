<?php 
class ControllerSettingMember extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('setting/member');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/member');

		$this->getList();
		
	}

	public function insert() {

		$this->language->load('setting/member');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/member');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$mem_id = $this->model_setting_member->addMember($this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			if(!empty($mem_id)){
				
				$this->redirect($this->url->link('setting/member', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} 		
			
		}

		$this->getForm();		
	}


	public function update() {

		$this->language->load('setting/member');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/member');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_setting_member->editMember($this->request->post,$this->request->get['member_id']);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/member', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
			
		}

		$this->getForm();
	}

	public function delete() {
    	$this->language->load('setting/member');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/member');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $member_id) {
				$this->model_setting_member->deleteMember($member_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('setting/member', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}


	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_company'])) {
			$url.= '&filter_email='.$this->request->post['filter_company'];
		} 
		if (isset($this->request->post['filter_phone'])) {
			$url.= '&filter_phone='.$this->request->post['filter_phone'];
		}
				
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 

		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_company'])) {
			$filter_company = $this->request->post['filter_company'];
		} else {
			$filter_company ="";
		}
		if (isset($this->request->post['filter_phone'])) {
			$filter_phone = $this->request->post['filter_phone'];
		} else {
			$filter_phone ="";
		}
		
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/member/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/member/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_company' => $filter_company,
			'filter_phone'	=>$filter_phone,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		
//printArray($data); exit;
		$member_total = $this->model_setting_member->getTotalMembers($data);


		$results = $this->model_setting_member->getMembers($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/member/update', 'token=' . $this->session->data['token'] . '&member_id=' . $result['member_id'] . $url, 'SSL')
			);

			$this->data['members'][] = array(
				'member_code'		=> $result['member_code'],
				'member_id' 		=> $result['member_id'],
				'name'      	    => $result['name'],
				'company'			=> $result['company'],
				'mobile'			=> $result['mobile'],
				'createdon'			=> date('d/m/Y',strtotime($result['createdon'],"d/m/Y")), 
				'selected'          => isset($this->request->post['selected']) && in_array($result['member_id'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		//print_r($result);exit();
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Members List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('setting/member', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('setting/member', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $member_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/member', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/member_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}

	public function getForm() {

		$this->load->model('setting/member');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zipcode'] = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}		

		// if (isset($this->error['primary_email'])) {
		// 	$this->data['error_primary_email'] = $this->error['primary_email'];
		// } else {
		// 	$this->data['error_primary_email'] = '';
		// }

		if (isset($this->error['mobile'])) {
			$this->data['error_mobile'] = $this->error['mobile'];
		} else {
			$this->data['error_mobile'] = '';
		}

	
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Member List'),
			'href'		=> $this->url->link('setting/member', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['member_id'])) {
			
		   	$this->data['action'] = $this->url->link('setting/member/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		
			$this->data['action'] = $this->url->link('setting/member/update', 'token=' . $this->session->data['token'] . '&member_id=' . $this->request->get['member_id'] . $url, 'SSL');
		}	
		$this->data['cancel'] = $this->url->link('setting/member', 'token=' . $this->session->data['token'] . $url, 'SSL');

		
		
		if (isset($this->request->get['member_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$member_info = $this->model_setting_member->getMember($this->request->get['member_id']);
		}

		if(isset($this->request->post['member_code'])){
			$this->data['member_code'] =$this->request->post['member_code'];
		}else if($member_info){
			$this->data['member_code'] =$member_info['member_code'];
		}else {			
			$member_code = $this->model_setting_member->getLastMemberCode();
			if(empty($member_code)){
				$this->data['member_code'] =0001;
			} 
			else {
				$this->data['member_code'] = $member_code;
			}
		}	
		
		

		if(isset($this->request->post['member_type'])){
			$this->data['member_type'] =$this->request->post['member_type'];
		} elseif (!empty($customer_info)) {
			$this->data['member_type'] = $member_info['member_type'];
		} else {			
			$this->data['member_type'] ='';
		}	
		/*if (isset($this->request->post['customer_type'])) {
			$this->data['customer_type'] = $this->request->post['customer_type'];
		} elseif (!empty($customer_info)) {
			$this->data['customer_type'] = $customer_info['customer_type'];
		} else {
			$this->data['customer_type'] = '';
		}*/



		
		if(isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif(!empty($member_info)) {
			$this->data['status'] = $member_info['status'];
		} else {
			$this->data['status'] == '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($member_info)) {
			$this->data['name'] = trim($member_info['name']);
		} else {
			$this->data['name'] = '';
		}		
		
		if (isset($this->request->post['member_title'])) {
			$this->data['member_title'] = $this->request->post['member_title'];
		} elseif (!empty($member_info)) {
			$this->data['member_title'] = trim($member_info['member_title']);
		} else {
			$this->data['member_title'] = '';
		}

		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
		} elseif (!empty($member_info)) {
			$this->data['company'] = trim($member_info['company']);
		} else {
			$this->data['company'] = '';
		}

		if (isset($this->request->post['birth_date'])) {
			$this->data['birth_date'] = $this->request->post['birth_date'];
		} elseif (!empty($member_info)) {
			$this->data['birth_date'] = trim($member_info['birth_date']);
		} else {
			$this->data['birth_date'] = '';
		}

		if (isset($this->request->post['home'])) {
			$this->data['home'] = $this->request->post['home'];
		} elseif (!empty($member_info)) {
			$this->data['home'] = trim($member_info['home']);
		} else {
			$this->data['home'] = '';
		}

		if (isset($this->request->post['work'])) {
			$this->data['work'] = $this->request->post['work'];
		} elseif (!empty($member_info)) {
			$this->data['work'] = trim($member_info['work']);
		} else {
			$this->data['work'] = '';
		}

		if (isset($this->request->post['mobile'])) {
			$this->data['mobile'] = $this->request->post['mobile'];
		} elseif (!empty($member_info)) {
			$this->data['mobile'] = trim($member_info['mobile']);
		} else {
			$this->data['mobile'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($member_info)) {
			$this->data['address1'] = trim($member_info['address1']);
		} else {
			$this->data['address1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($member_info)) {
			$this->data['address2'] = trim($member_info['address2']);
		} else {
			$this->data['address2'] = '';
		}

		if (isset($this->request->post['city'])) {
			$this->data['city'] = $this->request->post['city'];
		} elseif (!empty($member_info)) {
			$this->data['city'] = trim($member_info['city']);
		} else {
			$this->data['city'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
		} elseif (!empty($member_info)) {
			$this->data['state'] = trim($member_info['state']);
		} else {
			$this->data['state'] = '';
		}

		if (isset($this->request->post['zipcode'])) {
			$this->data['zipcode'] = $this->request->post['zipcode'];
		} elseif (!empty($member_info)) {
			$this->data['zipcode'] = trim($member_info['zipcode']);
		} else {
			$this->data['zipcode'] = '';
		}

		
		if (isset($this->request->post['notes'])) {
			$this->data['notes'] = $this->request->post['notes'];
		} elseif (!empty($member_info)) {
			$this->data['notes'] = trim($member_info['notes']);
		} else {
			$this->data['notes'] = '';
		}

		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/member';

		$this->template = 'setting/member_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	

	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'setting/member')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Enter Name');
		}	
		

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
public function validateFormType()
	{
		if (!$this->user->hasPermission('modify', 'setting/member')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$member_info = $this->model_setting_member->getMemberGroup($this->request->post['name'],$this->request->get['member_id']);
		
		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Enter name');
		} elseif($this->request->post['name'] != '' && count($member_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}

?>