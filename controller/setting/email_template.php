<?php
class ControllerSettingEmailTemplate extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('setting/xero_settings');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/email_template');
		$this->getList();
	}

	public function insert() {
		$this->language->load('setting/xero_settings');
		$this->document->setTitle($this->language->get('new_title'));
		$this->load->model('setting/email_template');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_email_template->addEmailTemplate($this->request->post);
			$this->session->data['success'] = 'Success: You have added Email Template!';;
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function update() {
		$this->language->load('setting/xero_settings');
		$this->document->setTitle($this->language->get('edit_title'));
		$this->load->model('setting/email_template');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_email_template->updateEmailTemplate($this->request->get['id'], $this->request->post);
			$this->session->data['success'] = 'Success: You have modified Email Template!';
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}

	public function delete() {

		$this->language->load('setting/xero_settings');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/email_template');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_setting_email_template->deleteXeroSettings($id);
			}
			$this->session->data['success'] = 'Success: You have deleted Email Template!';;
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
	}

	protected function getList() {
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
			$url.= '&filter_name='.$this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
			$url.= '&filter_status='.$this->request->get['filter_status'];
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Email Template List',
			'href'      => $this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['insert'] = $this->url->link('setting/email_template/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/email_template/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['xero_setting'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name'  => $filter_name,
			'filter_status'=> $filter_status,
		);
		$this->data['data'] = $data;
		$total   = $this->model_setting_email_template->getTotalEmailTemplate($data);
		$results = $this->model_setting_email_template->getAllEmailTemplate($data);

			foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/email_template/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);
			$this->data['email_templates'][] = array(
				'id'      	 => $result['id'],
				'name'    	 => $result['name'],
				'subject'  	 => $result['subject'],
				'description'=> $result['description'],
				'status'  	 => $result['status'],
				'edit_btn' 	 => $this->url->link('setting/email_template/update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);
		}

		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['column_name'] 		= $this->language->get('column_name');
		$this->data['column_available_to'] 		= $this->language->get('column_available_to');
		$this->data['column_xero_account_code'] = $this->language->get('column_xero_account_code');
		$this->data['column_status'] 	= $this->language->get('column_status');
		$this->data['column_isNetwork'] = $this->language->get('column_isNetwork');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total  = $total;
		$pagination->page   = $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text   = $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] 	= $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['xero_setting'] = $this->url->link("setting/email_template", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'setting/email_template_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	protected function getForm() {

		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_enabled'] 	= $this->language->get('text_enabled');
		$this->data['text_disabled'] 	= $this->language->get('text_disabled');
		$this->data['entry_name'] 		= $this->language->get('entry_name');
		$this->data['entry_isNetwork'] 	= $this->language->get('entry_isNetwork');
		$this->data['entry_available_to'] 		= $this->language->get('entry_available_to');
		$this->data['entry_xero_account_code'] 	= $this->language->get('entry_xero_account_code');
		$this->data['entry_status'] 	= $this->language->get('entry_status');
		$this->data['entry_captcha'] 	= $this->language->get('entry_captcha');
		$this->data['button_save'] 		= $this->language->get('button_save');
		$this->data['button_cancel'] 	= $this->language->get('button_cancel');

		$this->data['error_warning'] = '';
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home','token='.$this->session->data['token'],'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href' =>$this->url->link('setting/common','token='.$this->session->data['token'].$url,'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Email Template Form',
			'href'      => $this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		

		if (!isset($this->request->get['id'])) {
			$this->data['action'] = $this->url->link('setting/email_template/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('setting/email_template/update', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('setting/email_template', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$email_temp= $this->model_setting_email_template->getEmailTemplate($this->request->get['id']);
		}

		$this->data['name'] = '';
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($email_temp)) {
			$this->data['name'] = $email_temp['name'];
		}
		$this->data['subject'] = '';
		if (isset($this->request->post['subject'])) {
			$this->data['subject'] = $this->request->post['subject'];
		} elseif (!empty($email_temp)) {
			$this->data['subject'] = $email_temp['subject'];
		}
		$this->data['description'] = '';
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($email_temp)) {
			$this->data['description'] = $email_temp['description'];
		}
		$this->data['status'] = '';
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($email_temp)) {
			$this->data['status'] = $email_temp['status'];
		}

		$this->data['home'] 		= $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] 	= $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['xero_setting'] = $this->url->link("setting/email_template", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['route'] 		= 'setting/email_template';
		$this->template = 'setting/email_template_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if ($this->request->post['subject'] =='') {
			$this->error['warning'] = 'Please enter subject';
		}
		if ($this->request->post['name'] =='') {
			$this->error['warning'] = 'Please enter name';
		}
		if (!$this->user->hasPermission('modify', 'setting/email_template')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>