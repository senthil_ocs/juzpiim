<?php 
class ControllerSettingTouchMenuItem extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/touchmenuitem');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/touchmenuitem');
		$this->getList();
		
	}
	public function insert() {
		$this->language->load('setting/touchmenuitem');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/touchmenuitem');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='insert')) {
			//printArray($this->request->post); exit;
			$this->model_setting_touchmenuitem->addTouchMenuItem($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();		
	}
	public function update() {
		$this->language->load('setting/touchmenuitem');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/touchmenuitem');
		
		//printArray($this->request->post); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='update')) {
			$this->model_setting_touchmenuitem->editTouchMenuItem($this->request->post,$this->request->get['menu_code'],$this->request->get['sku']);
			
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();
	}
	public function delete() {
    	$this->language->load('setting/touchmenuitem');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/touchmenuitem');
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $id) {
				$this->model_setting_touchmenuitem->deleteTouchMenuItem($id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$this->redirect($this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
  	}
	public function getList(){
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->get['filter_menu_code'])) {
			$url.= '&filter_menu_code='.$this->request->get['filter_menu_code'];
		}
		if (isset($this->request->get['filter_description'])) {
			$url.= '&sku_code='.$this->request->get['filter_description'];
		} 
		
		
		if (isset($this->request->get['filter_menu_code'])) {
			$filter_menu_code = $this->request->get['filter_menu_code'];
		} else {
			$filter_menu_code ="";
		}
		if (isset($this->request->get['filter_description'])) {
			$filter_description = $this->request->get['filter_description'];
		} else {
			$filter_description ="";
		}		
		
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/touchmenuitem/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/touchmenuitem/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['touchmenuitem'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_menu_code' => $filter_menu_code,
			'filter_description' => $filter_description,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		
		$customer_total = $this->model_setting_touchmenuitem->getTotalTouchMenuItem($data);
		$results = $this->model_setting_touchmenuitem->getTouchMenuItem($data);

		foreach ($results as $result) {
			$tmDescription =$this->model_setting_touchmenuitem->getTMname($result['touch_menu_code']);
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/touchmenuitem/update', 'token=' . $this->session->data['token'] . '&menu_code='.$result['touch_menu_code'].'&sku='.base64_encode($result['sku_code']). $url, 'SSL')
			);
			if($result['status'] == 0){
				$strStatus = $this->language->get('text_disabled');
			} else {
				$strStatus = $this->language->get('text_enabled');
			}
			$this->data['touchmenuitem'][] = array(
				'id'					=> $result['id'],
				'touch_menu_code'		=> $result['touch_menu_code'],
				'touch_menu_description'=>$tmDescription,
				'sku_code'			    => $result['sku_code'],
				'description'		=> $result['sku_description'],
				'status' 		=> $strStatus,
				'sort_code' 		=> $result['sort_code'],
				'createdby'      	    => $result['createdby'],
				'createdon'				=> $result['createdon']->format('Y-m-d'),
				'selected'          => isset($this->request->post['selected']) && in_array($result['id'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 		$this->data['touchMenuCode'] = $this->model_setting_touchmenuitem->getTouchMenuCode();

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Touch Menu Item'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['touch_menu_code'] = $filter_menu_code;
		$this->data['filter_description'] = $filter_description;
		
		$this->data['sort_touch_menu_code'] = $this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . '&sort=touch_menu_code' . $url, 'SSL');
		$this->data['sort_sort_code'] = $this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . '&sort=sort_code' . $url, 'SSL');
	
		//$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['route'] = 'setting/touchmenuitem';
		
		$this->template = 'setting/touchmenuitem_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getForm() {
		//printArray($this->request->get); exit;

		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zipcode'] = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_attn'] = $this->language->get('entry_attn');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_gst'] = $this->language->get('entry_gst');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
 	
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['touch_menu_code'])) {
			$this->data['error_touch_menu_code'] = $this->error['touch_menu_code'];
		} else {
			$this->data['error_touch_menu_code'] = '';
		}		
		
		if (isset($this->error['sku_code'])) {
			$this->data['error_sku_code'] = $this->error['sku_code'];
		} else {
			$this->data['error_sku_code'] = '';
		}
		
		if (isset($this->error['sort_code'])) {
			$this->data['error_sort_code'] = $this->error['sort_code'];
		} else {
			$this->data['error_sort_code'] = '';
		}
	
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Touch Menu Item List'),
			'href'		=> $this->url->link('setting/touchmenuitem', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$type = $this->request->get['type'];
		if (!isset($this->request->get['menu_code']) || !isset($this->request->get['sku'])) {
		   	$this->data['action'] = $this->url->link('setting/touchmenuitem/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		    	$this->data['action'] = $this->url->link('setting/touchmenuitem/update', 'token=' . $this->session->data['token'] . '&menu_code=' . $this->request->get['menu_code'] .'&sku=' . $this->request->get['sku'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('setting/touchmenuitem', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['menu_code']) || isset($this->request->get['sku']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_setting_touchmenuitem->getTouchMenuItemById($this->request->get['menu_code'],$this->request->get['sku']);
			$this->data['editForm'] = 1;
		}		
		
		if (isset($this->request->post['touch_menu_code'])) {
			$this->data['touch_menu_code'] = $this->request->post['touch_menu_code'];
		} elseif (!empty($customer_info)) {
			$this->data['touch_menu_code'] = $customer_info['touch_menu_code'];
		} else {
			$this->data['touch_menu_code'] = '';
		}

		if (isset($this->request->post['filter_name'])) {
			$this->data['filter_name'] = $this->request->post['filter_name'];
		} elseif (!empty($customer_info)) {
			$this->data['filter_name'] = $customer_info['sku_description'];
		} else {
			$this->data['filter_name'] = '';
		}

		if (isset($this->request->post['touch_menu_description'])) {
			$this->data['touch_menu_description'] = $this->request->post['touch_menu_description'];
		} elseif (!empty($customer_info)) {
			$this->data['touch_menu_description'] = $customer_info['sku_description'];
		} else {
			$this->data['touch_menu_description'] = '';
		}


		if (isset($this->request->post['sku_code'])) {
			$this->data['sku_code'] = $this->request->post['sku_code'];
		} elseif (!empty($customer_info)) {
			$this->data['sku_code'] = $customer_info['sku_code'];
		} else {
			$this->data['sku_code'] = '';
		}
		if (isset($this->request->post['sku_description'])) {
			$this->data['touch_menu_description'] = $this->request->post['sku_description'];
		} elseif (!empty($customer_info)) {
			$this->data['touch_menu_description'] = $customer_info['sku_description'];
		} else {
			$this->data['touch_menu_description'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($customer_info)) {
			$this->data['status'] = $customer_info['status'];
		} else {
			$this->data['status'] = '';
		}
		if (isset($this->request->post['sort_code'])) {
			$this->data['sort_code'] = $this->request->post['sort_code'];
		} elseif (!empty($customer_info)) {
			$this->data['sort_code'] = $customer_info['sort_code'];
		} else {
			$this->data['sort_code'] = '1';
		}
         
		/*if (isset($_FILES["product_image"])) {
			$this->data['product_image'] = $_FILES["product_image"]['name'];
		} elseif (!empty($customer_info)) {
			$this->data['product_image'] = $customer_info['imgData'];
		} else {
			$this->data['product_image'] = '';
		}*/
		
		
		$this->data['touchMenuCode'] = $this->model_setting_touchmenuitem->getTouchMenuCode();
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/touchmenuitem';
		$this->template = 'setting/touchmenuitem_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	public function validateForm($type='')
	{    
		if (!$this->user->hasPermission('modify', 'setting/touchmenuitem')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$duplication = $this->model_setting_touchmenuitem->getTouchMenuItemByMenuCode($this->request->post['touch_menu_code'],$this->request->post['sku_code'],$type);
		
		if($duplication['cnt'] > 0){
			$this->error['warning'] = $this->language->get('This Product already assigned this "'.$this->request->post['touch_menu_code'].'" Touch Menu');
		}
        
		//printArray($_FILES["product_image"]);
		if (empty($this->request->post['touch_menu_code'])) {
			$this->error['touch_menu_code'] = $this->language->get('Please Choose Touch Menu Code');
		}
		if (empty($this->request->post['sku_code'])) {
			$this->error['sku_code'] = $this->language->get('Please Choose Product');
		}
		if (isset($_FILES["product_image"])) {
           $image_info = getimagesize($_FILES["product_image"]["tmp_name"]);
            $image_width = $image_info[0];
            $image_height = $image_info[1];
            $image_size = $_FILES["product_image"]["size"];
            if($image_width >=501 || $image_height >=501){
			  $this->error['warning'] = $this->language->get('Image dimension should be within 500X500');
            }
            if($image_size >= KILOBYTE){
            	$this->error['warning'] = 'uploaded image size should be less than or Equal to 500KB';
            }          
		}

		if (empty($_FILES["product_image"])) {
			$this->error['warning'] = $this->language->get('Please Choose Product Image');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getSkuDetails(){
		$this->load->model('setting/touchmenuitem');
		$productDetails = $this->model_setting_touchmenuitem->getProductDetailsBySku($this->request->post['sku']);
		$lastSortCode = $this->model_setting_touchmenuitem->getLastSortCode($this->request->post['menucode'],$this->request->post['sku']);
		echo $productDetails['description'].'||'.$lastSortCode;
	}
	public function updateSortOrder(){
		$id 		= $this->request->post['itemId'];
		$sort_code  = $this->request->post['newvalue'];
		$this->load->model('setting/touchmenuitem');
		if(isset($id) && isset($sort_code)){
			$this->model_setting_touchmenuitem->updateSortOrder($id,$sort_code);
			echo 1;
		}else{
			echo 0;
		}
	}
}
?>