<?php
class ControllerReportReport extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('setting/setting'); 

		$this->document->setTitle('Reports');
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}	
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Setting',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'General Setting',
			'href'      => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->load->model('user/user_group');
		$resportsStr = $this->model_user_user_group->userReportAccess();
		if($resportsStr['access_reports']){
			$accessAry = explode(",", $resportsStr['access_reports']);
		}




		$this->data['action'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['department'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=department", 'SSL');
		$this->data['category'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=category", 'SSL');
		$this->data['brandreport']	= $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=brand", 'SSL'); 
		

		$this->data['inventory_barcode']= $this->url->link('inventory/reports/printstock','token=' .$this->session->data['token']."&&stock_report=barcode_list", 'SSL');
		$this->data['paymode'] = $this->url->link('master/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=paymode", 'SSL');
		$this->data['vendor'] = $this->url->link('master/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=vendor", 'SSL');
		$this->data['tax'] = $this->url->link('master/reports/printreports', 'token=' . $this->session->data['token']."&&master_name=tax", 'SSL');
		$this->data['user'] = $this->url->link('master/user', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['user_group'] = $this->url->link('master/user_right', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['customer'] = $this->url->link('master/customers', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['terms'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['uomreport'] = $this->url->link('master/uomreports', '&token=' .$this->session->data['token'], 'SSL');
		$this->data['discountreport'] = $this->url->link('master/discountreports', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['qty_negative']    = $this->config->get('config_report_qty_negative');
		

		$this->data['stock_report']	= $this->url->link('inventory/reports/printstock','token=' .$this->session->data['token']."&stock_report=stock&qty_neg=".$this->data['qty_negative'], 'SSL');

		$this->data['po_request'] = $this->url->link('inventory/reports/po_request','token=' . $this->session->data['token'], 'SSL');

		$this->data['purchase_summary'] = $this->url->link('transaction/transaction_reports&type=purchase_summary','token=' . $this->session->data['token'], 'SSL');

		$this->data['purchase_details'] = $this->url->link('inventory/reports/purchase_details','token=' . $this->session->data['token'], 'SSL');

		$this->data['purchase_return_summary'] = $this->url->link('transaction/transaction_reports/purchaseReturnSummary','token=' . $this->session->data['token'], 'SSL');
		$this->data['purchase_return_details'] = $this->url->link('inventory/reports/purchase_return_details','token=' . $this->session->data['token'], 'SSL');
		
		
		$this->data['wastage_report'] = $this->url->link('transaction/transaction_reports/wastageReport','token=' . $this->session->data['token'], 'SSL');
		$this->data['wastage_details_report'] = $this->url->link('transaction/transaction_reports/wastageDetailsReport','token=' . $this->session->data['token'], 'SSL');

		$this->data['movement_report'] = $this->url->link('transaction/transaction_reports/movementReport','token=' . $this->session->data['token'], 'SSL');
		

		
		$this->data['token'] = $this->session->data['token'];
		$this->data['master_report'] = $this->url->link('inventory/reports/masterReport','token=' .$this->session->data['token']."&stock_report=master", 'SSL');
		$this->data['sales_summary'] = $this->url->link('transaction/transaction_reports/salessummary','token=' . $this->session->data['token'], 'SSL');
		$this->data['sales_detail'] = $this->url->link('transaction/transaction_reports/salesdetail','token=' . $this->session->data['token'], 'SSL');
		$this->data['sales_sku'] = $this->url->link('transaction/transaction_reports/salessummarysku','token=' . $this->session->data['token'], 'SSL');
		$this->data['settlement'] = $this->url->link('transaction/transaction_reports/settlement','token=' . $this->session->data['token'], 'SSL');

		$this->data['salestotalsummary'] = $this->url->link('transaction/transaction_reports/salestotalsummary','token=' . $this->session->data['token'], 'SSL');
		$this->data['void_sales_details'] = $this->url->link('transaction/transaction_reports/voidsalesdetails','token=' . $this->session->data['token'], 'SSL');

		$this->data['creditcollectionsummary'] = $this->url->link('transaction/transaction_reports/creditcollectionsummary','token=' . $this->session->data['token'], 'SSL');

		$this->data['sales_creditcollectionsummary'] = $this->url->link('transaction/transaction_reports/salescreditcollectionsummary','token=' . $this->session->data['token'], 'SSL');
		
		$this->data['pricetag'] = $this->url->link('transaction/transaction_reports/pricetag','token=' . $this->session->data['token'], 'SSL');
		$this->data['creditpending'] = $this->url->link('transaction/transaction_reports/creditpending','token=' . $this->session->data['token'], 'SSL');
		$this->data['creditpendingsummary'] = $this->url->link('transaction/transaction_reports/creditpendingsummary','token=' . $this->session->data['token'], 'SSL');
		$this->data['cashflow'] = $this->url->link('transaction/transaction_reports/cashflowsummary','token=' . $this->session->data['token'], 'SSL');
		$this->data['department_sales_detail'] = $this->url->link('transaction/transaction_reports/departmentsalesdetail','token=' . $this->session->data['token'], 'SSL');
		$this->data['stock_valuation_report']	= $this->url->link('inventory/reports/stockvaluationreport','token=' .$this->session->data['token'], 'SSL');
		$this->data['stock_valuation_Summary_report']	= $this->url->link('inventory/reports/stockvaluationSummaryreport','token=' .$this->session->data['token'], 'SSL');

		$this->data['profit_summary']	= $this->url->link('inventory/reports/profitSummary','token=' .$this->session->data['token'], 'SSL');
		$this->data['profit_details']   = $this->url->link('inventory/reports/profitDetails','token=' . $this->session->data['token'], 'SSL');

		$this->data['expenses'] = $this->url->link('transaction/transaction_reports/expensesreport','token=' . $this->session->data['token'], 'SSL');
		$this->data['fastsellinglowmoving'] = $this->url->link('transaction/transaction_reports/fastsellinglowmoving','token=' . $this->session->data['token'], 'SSL');


		$reportsAry = array();
		//$accessAry
		
		
		$mReportData = $this->model_user_user_group->getAccessReportByType('reports','Management Report');
		foreach($mReportData as $reportsData){
			if(in_array($reportsData['id'], $accessAry))
				$reportsAry['management_report'][] = array("name"=>$reportsData['name'],
													   "link"=>$this->url->link($reportsData['link'],'token=' .$this->session->data['token'], 'SSL'),
													   "description"=>"Your".$reportsData['name']." Reports");
		}
		
		$CreditReportData = $this->model_user_user_group->getAccessReportByType('reports','Credit Report');
		foreach($CreditReportData as $reportsData){
			if(in_array($reportsData['id'], $accessAry))
				$reportsAry['credit_report'][] = array("name"=>$reportsData['name'],
													   "link"=>$this->url->link($reportsData['link'],'token=' .$this->session->data['token'], 'SSL'),
													   "description"=>"Your basic total ".$reportsData['name']." Reports");
		}
	
		$SalesReportData = $this->model_user_user_group->getAccessReportByType('reports','Sales Report');
		foreach($SalesReportData as $reportsData){
			if(in_array($reportsData['id'], $accessAry))
				$reportsAry['sales_report'][] = array("name"=>$reportsData['name'],
													   "link"=>$this->url->link($reportsData['link'],'token=' .$this->session->data['token'], 'SSL'),
													   "description"=>"Your basic ".$reportsData['name']);
		}

		$PurchaseReportData = $this->model_user_user_group->getAccessReportByType('reports','Purchase Reports');
		foreach($PurchaseReportData as $reportsData){
			if(in_array($reportsData['id'], $accessAry))
				$reportsAry['purchase_report'][] = array("name"=>$reportsData['name'],
													   "link"=>$this->url->link($reportsData['link'],'token=' .$this->session->data['token'], 'SSL'),
													   "description"=>"Your basic ".$reportsData['name']);
		}

		$SkuReportData = $this->model_user_user_group->getAccessReportByType('reports','SKU Reports');
		foreach($SkuReportData as $reportsData){
			if(in_array($reportsData['id'], $accessAry))
				$reportsAry['sku_report'][] = array("name"=>$reportsData['name'],
													   "link"=>$this->url->link($reportsData['link'],'token=' .$this->session->data['token']."&stock_report=stock&qty_neg=".$this->data['qty_negative'], 'SSL'),
													   "description"=>"Your basic ".$reportsData['name']);
		}
		
		$this->data['reports'] = $reportsAry;
		$this->template = 'report/reports.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function updateSettings(){
		$this->load->model('inventory/inventory');
		$value = $this->model_inventory_inventory->updateConfigSetting($this->request->post['value']);
		echo $value;
	}
}
?>