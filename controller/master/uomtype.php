<?php 
class ControllerMasterUomType extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/uomtype');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uomtype');
		
		$this->getList();
	}

	public function insert() {
		$this->language->load('master/uomtype');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uomtype');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->request->post['type_value'] = $this->request->post['value1'].'*'.$this->request->post['value2'];
			$this->model_master_uomtype->addUomType($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function getTypeDetails(){
		$this->load->model('master/uomtype');
		$res['Details'] = $this->model_master_uomtype->getTypeListbyUOMCode($this->request->post['uom_code']); 
		$this->response->setOutput(json_encode($res));

	}
	public function update() {
		$this->language->load('master/uomtype');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uomtype');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_master_uomtype->editUomType($this->request->get['uom_uom_type_id'], $this->request->post);
			$this->request->post['type_value'] = $this->request->post['value1'].'*'.$this->request->post['value2'];
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		//$this->data['edit_form']	= true;
		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('master/uomtype');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uomtype');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $uom_uom_type_id) {
				$this->model_master_uomtype->deleteuomtype($uom_uom_type_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Uom Type List',
			'href'      => $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

  		$this->data['insert'] = $this->url->link('master/uomtype/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/uomtype/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['uomtypes'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$uomtype_total = $this->model_master_uomtype->getTotaluomtypes();
		
		$results = $this->model_master_uomtype->getuomtypes($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/uomtype/update', 'token=' . $this->session->data['token'] . '&uom_uom_type_id=' . $result['uom_type_id'] . $url, 'SSL')
			);
			
			$this->data['uomtypes'][] = array(
				'uom_code' => $result['uom_code'],
				'uom_type_id' => $result['uom_type_id'],
				'name'       => $result['uom_type_name'],
				'value'       => $result['uom_type_value'],
				'description'       => $result['description'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['uom_type_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_value'] = $this->language->get('column_value');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . '&sort=uom_type_name' . $url, 'SSL');
		$this->data['sort_value'] = $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . '&sort=uom_type_value' . $url, 'SSL');
		$this->data['sort_uom'] = $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . '&sort=uom_code' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $uomtype_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'master/uomtype_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_value'] = $this->language->get('entry_value');
		$this->data['entry_description'] = $this->language->get('entry_description');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['value'])) {
			$this->data['error_value'] = $this->error['value'];
		} else {
			$this->data['error_value'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['uom_code'])) {
			$this->data['error_uom_code'] = $this->error['uom_code'];
		} else {
			$this->data['error_uom_code'] = '';
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Uom Type List',
			'href'      => $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

  		if (!isset($this->request->get['uom_uom_type_id'])) { 
			$this->data['action'] = $this->url->link('master/uomtype/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/uomtype/update', 'token=' . $this->session->data['token'] . '&uom_uom_type_id=' . $this->request->get['uom_uom_type_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/uomtype', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['uom_uom_type_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$uomtype_info = $this->model_master_uomtype->getuomtype($this->request->get['uom_uom_type_id']);
			$this->data['edit_form'] = '1';
		}
		
		if(isset($uomtype_info['uom_type_value']) && $uomtype_info['uom_type_value'] !=''){
			$uom_type_value = explode('*', $uomtype_info['uom_type_value']);
		}

		if (isset($this->request->post['value1'])) {
			$this->data['value1'] = $this->request->post['value1'];
		} elseif (!empty($uomtype_info)) {
			$this->data['value1'] = $uom_type_value[0];
		} else {
			$this->data['value1'] = '';
		}

		if (isset($this->request->post['value2'])) {
			$this->data['value2'] = $this->request->post['value2'];
		} elseif (!empty($uomtype_info)) {
			$this->data['value2'] = $uom_type_value[1];
		} else {
			$this->data['value2'] = '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($uomtype_info)) {
			$this->data['name'] = $uomtype_info['uom_type_name'];
		} else {
			$this->data['name'] = '';
		}
		if (isset($this->request->post['uom_code'])) {
			$this->data['uom_code'] = $this->request->post['uom_code'];
		} elseif (!empty($uomtype_info)) {
			$this->data['uom_code'] = $uomtype_info['uom_code'];
		} else {
			$this->data['uom_code'] = '';
		}

		$this->data['UomDetails'] = $this->model_master_uomtype->getuoms();

		$this->data['route'] = 'master/uomtype';

		$this->template = 'master/uomtype_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'master/uomtype')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$strCountDuplication = $this->model_master_uomtype->checkDuplicationUomType($this->request->post['value1'],$this->request->post['value2'],$this->request->get['uom_uom_type_id']);

		if ($strCountDuplication > 0) {
			$this->error['warning'] = "UOM Type Value Already Exists!";
		}
		if ($this->request->post['name'] == "") {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if ($this->request->post['value1'] =='' && $this->request->post['value2']=='') {
			$this->error['value'] = $this->language->get('error_code');
		}

		if ($this->request->post['uom_code'] =='') {
			$this->error['uom_code'] = $this->language->get('error_uom_code');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'master/uomtype')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
	  	  
		foreach ($this->request->post['selected'] as $user_id) {
			if ($this->user->getId() == $user_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}
		 
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
}
?>