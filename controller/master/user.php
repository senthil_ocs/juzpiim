<?php 
class ControllerMasterUser extends Controller {
		private $error = array();

	public function index() {
		$this->language->load('user/user');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user');

		$this->getList();
	}
	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'username';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->post['filter_username'])) {
			$url.= '&filter_username='.$this->request->post['filter_username'];
		}
		if (isset($this->request->post['filter_email'])) {
			$url.= '&filter_email='.$this->request->post['filter_email'];
		} 
		
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 
		
		if (isset($this->request->post['filter_username'])) {
			$filter_username = $this->request->post['filter_username'];
		} else {
			$filter_username ="";
		}
		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
		} else {
			$filter_email ="";
		}
		if (isset($this->request->post['filter_status'])) {
			$filter_status = $this->request->post['filter_status'];
		} else {
			$filter_status ="";
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
		} else {
			$filter_date_to ="";
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'User Report',
			'href'      => $this->url->link('master/user', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data["filterstatus"] = $filter_status;

		$this->data['users'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_username' => $filter_username,
			'filter_email' => $filter_email,
			'filter_status' => $filter_status,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$user_total = $this->model_user_user->getTotalUsers();

		$results = $this->model_user_user->getUsers($data);

		foreach ($results as $result) {
			
			$this->data['users'][] = array(
				'user_id'    => $result['user_id'],
				'username'   => $result['username'],
				'email'		 => $result['email'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['column_username'] = $this->language->get('column_username');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		$this->data['sort_username'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=username' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('user/user', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		//$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/user', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_status'] = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'master/user_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
}
?>