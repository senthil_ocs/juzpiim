<?php 
class ControllerMasterVendor extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/vendor');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/vendor');
		
		$this->getList();
	}

	public function insert() {
		$this->language->load('master/vendor');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/vendor');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_vendor->addVendor($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/vendor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		
		$this->language->load('master/vendor');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('master/vendor');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
		if($this->request->post['gst']!='1'){
			$this->request->post['tax_method'] = 0;
		}
		$this->model_master_vendor->editVendor($this->request->get['vendor_id'], $this->request->post);
		$this->session->data['success'] = $this->language->get('text_success');

		$url ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier']; 
			$url.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = '';
		}
		if (isset($_REQUEST['filter_phone'])) {
			$filter_phone = $_REQUEST['filter_phone'];
			$url.='&filter_phone='.$filter_phone; 
		} else {
			$filter_phone = '';
		}	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
					
		$this->redirect($this->url->link('master/vendor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('master/vendor');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/vendor');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $vendor_id) {
				$this->model_master_vendor->deleteVendor($vendor_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/vendor', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$page = $_REQUEST['page'];
		/*if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}*/
		$url ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier']; 
			$url.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = '';
		}
		if (isset($_REQUEST['filter_phone'])) {
			$filter_phone = $_REQUEST['filter_phone'];
			$url.='&filter_phone='.$filter_phone; 
		} else {
			$filter_phone = '';
		}	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('master/vendor/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/vendor/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['vendors'] = array();
		
		//echo $page; exit;
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_supplier'=> $filter_supplier,
			'filter_phone'=> $filter_phone
		);
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_phone='.$filter_phone;
		
		$vendor_total = $this->model_master_vendor->getTotalVendors($data);
		// $data = array(
		// 		'filter_supplier'=>$this->request->post['filter_supplier'],
		// 		'filter_phone'=>$this->request->post['filter_phone']
		// 	);
		
		$results = $this->model_master_vendor->getVendors($data);
		
		$country_collection = $this->model_master_vendor->getCountry();
		
		// printArray($results); exit;
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/vendor/update', 'token=' . $this->session->data['token'] . '&vendor_id=' . $result['vendor_id'] . $url, 'SSL')
			);
			
			if(!empty($result['country'])){
				$country_name = $this->model_master_vendor->getCountryName($result['country']);
				$country_name = $country_name['name'];
			} else {
				$country_name	= "";
			}
// printArray($result); die;
			// $this->data['suppliers'] = $this->model_master_vendor->getSuppliers();
			$this->data['vendors'][] = array(
				'vendor_id' => $result['vendor_id'],
				'code' 		 => $result['vendor_code'],
				'name'       => $result['vendor_name'],
				'country'    => $country_name,
				'zipcode'    => $result['postal_code'],
				'phone'      => $result['phone'],
				'email'      => $result['email'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['vendor_id'], $this->request->post['selected']),
				'action'     => $action,
				'xero_id'    => $result['xero_vendor_id'],
				'xero_status'=> $xeroStatus,
				'location_code'     => $this->session->data['location_code']
			);
		}
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_address1'] = $this->language->get('column_address1');
		$this->data['column_address2'] = $this->language->get('column_address2');
		$this->data['column_country'] = $this->language->get('column_country');
		$this->data['column_zipcode'] = $this->language->get('column_zipcode');
		$this->data['column_phone'] = $this->language->get('column_phone');
		$this->data['column_fax'] = $this->language->get('column_fax');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_attn'] = $this->language->get('column_attn');
		$this->data['column_remarks'] = $this->language->get('column_remarks');
		$this->data['column_gst'] = $this->language->get('column_gst');
		$this->data['column_status'] = $this->language->get('column_status');
		
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=vendor_code' . $url, 'SSL');
		$this->data['sort_country'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=country' . $url, 'SSL');
		$this->data['sort_zipcode'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=zipcode' . $url, 'SSL');
		$this->data['sort_phone'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=phone' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');

		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$url.= $pageUrl;
		$pagination = new Pagination();
		$pagination->total = $vendor_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination']= $pagination->render();
		$this->data['sort'] 	 = $sort;
		$this->data['order'] 	 = $order;
		$this->data['home']      = $this->url->link('common/home','&token='.$this->session->data['token'],'SSL');
		$this->data['purchase']  = $this->url->link('transaction/common_purchase', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['vendorsLink']   = $this->url->link('master/vendor','&token='.$this->session->data['token'], 'SSL');
		$this->data['supplier']  = $this->url->link('master/vendor', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['route'] 	 = $this->request->get['route'];
		$this->template 		 = 'master/vendor_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zipcode'] = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_attn'] = $this->language->get('entry_attn');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_gst'] = $this->language->get('entry_gst');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

		if (isset($this->error['email_duplication'])) {
			$this->data['error_email_duplication'] = $this->error['email_duplication'];
		} else {
			$this->data['error_email_duplication'] = '';
		}
		
		if (isset($this->error['address'])) {
			$this->data['error_address'] = $this->error['address'];
		} else {
			$this->data['error_address'] = '';
		}
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}

		if (isset($this->error['email_duplication'])) {
			$this->data['error_email_duplication'] = $this->error['email_duplication'];
		} else {
			$this->data['error_email_duplication'] = '';
		}
		
		if (isset($this->error['country'])) {
			$this->data['error_country'] = $this->error['country'];
		} else {
			$this->data['error_country'] = '';
		}
				
		$url ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier']; 
			$url.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = '';
		}
		if (isset($_REQUEST['filter_phone'])) {
			$filter_phone = $_REQUEST['filter_phone'];
			$url.='&filter_phone='.$filter_phone; 
		} else {
			$filter_phone = '';
		}	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['vendor_id'])) { 
			$this->data['action'] = $this->url->link('master/vendor/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/vendor/update', 'token=' . $this->session->data['token'] . '&vendor_id=' . $this->request->get['vendor_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/vendor', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['vendor_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$vendor_info = $this->model_master_vendor->getVendor($this->request->get['vendor_id']);
		}
		
		$this->data['country_collection'] = $this->model_master_vendor->getCountry();
		$this->data['currency_collection'] = $this->model_master_vendor->getCurrency();

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($vendor_info)) {
			$this->data['name'] = $vendor_info['vendor_name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($vendor_info['currency_code'])) {
			$this->data['currency_code'] = $vendor_info['currency_code'];
		} else {
			$this->data['currency_code'] = '';
		}
		
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($vendor_info)) {
			$this->data['code'] = $vendor_info['vendor_code'];
		} else {
			$this->data['code'] = $this->getVendorCode();
		}
		
		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($vendor_info)) {
			$this->data['address1'] = $vendor_info['address1'];
		} else {
			$this->data['address1'] = '';
		}
		
		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($vendor_info)) {
			$this->data['address2'] = $vendor_info['address2'];
		} else {
			$this->data['address2'] = '';
		}
		
		if (isset($this->request->post['country'])) {
			$this->data['country'] = $this->request->post['country'];
		} elseif (!empty($vendor_info)) {
			$this->data['country'] = $vendor_info['country'];
		} else {
			$this->data['country'] = '';
		}
		
		if (isset($this->request->post['zipcode'])) {
			$this->data['zipcode'] = $this->request->post['zipcode'];
		} elseif (!empty($vendor_info)) {
			$this->data['zipcode'] = $vendor_info['postal_code'];
		} else {
			$this->data['zipcode'] = '';
		}
		
		if (isset($this->request->post['phone'])) {
			$this->data['phone'] = $this->request->post['phone'];
		} elseif (!empty($vendor_info)) {
			$this->data['phone'] = $vendor_info['phone'];
		} else {
			$this->data['phone'] = '';
		}
		
		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (!empty($vendor_info)) {
			$this->data['fax'] = $vendor_info['fax'];
		} else {
			$this->data['fax'] = '';
		}
		
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($vendor_info)) {
			$this->data['email'] = $vendor_info['email'];
		} else {
			$this->data['email'] = '';
		}
		
		if (isset($this->request->post['attn'])) {
			$this->data['attn'] = $this->request->post['attn'];
		} elseif (!empty($vendor_info)) {
			$this->data['attn'] = $vendor_info['attn'];
		} else {
			$this->data['attn'] = '';
		}
		
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($vendor_info)) {
			$this->data['remarks'] = $vendor_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}

		if(isset($this->request->post['gst'])){
			$this->data['gst'] = $this->request->post['gst'];
		} elseif(!empty($vendor_info)) {
			$this->data['gst'] = $vendor_info['gst'];
		} else {
			$this->data['gst'] = '';
		}

		$this->data['tax_method'] = '';
		if(isset($this->request->post['tax_method'])){
			$this->data['tax_method'] = $this->request->post['tax_method'];
		} elseif(!empty($vendor_info)) {
			$this->data['tax_method'] = $vendor_info['tax_method'];
		}

		$this->data['status'] = '';
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($vendor_info)) {
			$this->data['status'] = $vendor_info['status'];
		}
		$this->data['term_id'] = '';
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($vendor_info)) {
			$this->data['term_id'] = $vendor_info['term_id'];
		}
		$this->data['tax_no'] = '';
		if (isset($this->request->post['tax_no'])) {
			$this->data['tax_no'] = $this->request->post['tax_no'];
		} elseif (!empty($vendor_info)) {
			$this->data['tax_no'] = $vendor_info['tax_no'];
		}

		$this->data['allTerms'] 		   = $this->cart->getAllTerms();
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['purchase'] = $this->url->link('transaction/common_purchase', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['supplier'] = $this->url->link('master/vendor', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'master/vendor';
		$this->template = 'master/vendor_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);		
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		//if (!$this->user->hasPermission('modify', 'master/vendor')) {
			//$this->error['warning'] = $this->language->get('error_permission');
		//}

		if ((utf8_strlen($this->request->post['name']) < 1)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if ((utf8_strlen($this->request->post['code']) < 1)) {
			$this->error['code'] = $this->language->get('error_code');
		}
		
		if ((utf8_strlen($this->request->post['email']) < 1)) {
			$this->error['email'] = $this->language->get('error_code');
		}
		
		if ((utf8_strlen($this->request->post['email']) > 1)) {
			$emaildata = $this->model_master_vendor->getvendoremail($this->request->post['email'],$this->request->get['vendor_id']);
			if($emaildata > 0){
				$this->error['email_duplication'] = $this->language->get('Email  has already been added!');
			}
		}
		
		// if ((utf8_strlen($this->request->post['address1']) < 1)) {
		// 	$this->error['address'] = $this->language->get('error_address');
		// }
		
		if (!isset($this->request->post['country'])) {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getVendorCode(){
		$this->load->model('master/vendor');
		$lastNum = $this->model_master_vendor->getVendorCode();
		return 'V'.str_pad($lastNum, 3, '0', STR_PAD_LEFT);
	}

}
?>