<?php  
class ControllerMasterTerminal extends Controller { 
	private $error = array();
	
	// Company Form Fields
	public function companyFormFields() {
		$forms = array();
		return $forms;
	
	}	
	          
	public function index() { 
		
		$this->language->load('master/terminal');
		$this->load->model('master/terminal');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['action'] = $this->url->link('master/terminal/update', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'master/terminal.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	public function update() { 
		
  	}

}  
?>