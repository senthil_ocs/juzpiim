<?php  
class ControllerMasterCashier extends Controller { 

	private $error = array();
	          
	public function index() { 
	
		$this->load->language('master/cashier');
		$this->load->model('tool/image');
		
		$this->document->setTitle('Cashier');
    	$this->data['heading_title'] = 'Cashier';

		$this->load->model('master/cashier');
		
		/*if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}*/
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['warning'])) {
    		$this->error['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		} else {
			$this->data['error_warning'] = '';
		}

		$this->getList();
		
  	}
	
	public function insert() {
		
		$this->language->load('master/cashier');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashier');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_cashier->addCashier($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		
		if($this->rules->getAccessConditionByRule()) {
			$this->getForm();
		} else {
			//$this->error['warning']	= 'Your Domain have not cahiers';
			$this->session->data['warning']	= 'Your Domain have not cahiers'; 
			$this->redirect($this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
	}
	
	public function update() {
	
		$this->language->load('master/cashier');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashier');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_cashier->editCashier($this->request->get['cashier_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function delete() {
    	$this->language->load('master/cashier');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashier');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $cashier_id) {
				$this->model_master_cashier->deleteCashier($cashier_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {
	
		$messages	= $this->rules->getAccessRuleMessage();
		if(!empty($messages)) {
			$this->data['error_alert']	= $messages;
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('master/cashier/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/cashier/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['cashiers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$cashier_total = $this->model_master_cashier->getTotalCashiers();
		
		$results = $this->model_master_cashier->getCashiers($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/cashier/update', 'token=' . $this->session->data['token'] . '&cashier_id=' . $result['cashier_id'] . $url, 'SSL')
			);
			$this->data['cashiers'][] = array(
				'cashier_id' => $result['cashier_id'],
				'name'       => $result['cashier_name'],
				'group'       => $result['cashier_group_name'],
				'email'       => $result['email'],
				'status'       => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['cashier_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_group'] = $this->language->get('column_group');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . '&sort=CA.cashier_name' . $url, 'SSL');
		$this->data['sort_group'] = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . '&sort=CG.cashier_group_name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . '&sort=CA.email' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . '&sort=CA.status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $cashier_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'master/cashier_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_group'] = $this->language->get('entry_group');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_cpassword'] = $this->language->get('entry_cpassword');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['group'])) {
			$this->data['error_group'] = $this->error['group'];
		} else {
			$this->data['error_group'] = '';
		}
		
		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = '';
		}
		
		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['cashier_id'])) { 
			$this->data['action'] = $this->url->link('master/cashier/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/cashier/update', 'token=' . $this->session->data['token'] . '&cashier_id=' . $this->request->get['cashier_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/cashier', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['cashier_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$cashier_info = $this->model_master_cashier->getCashier($this->request->get['cashier_id']);
		}
		$this->load->model('master/cashiergroup');
		$this->data['cashier_collection'] = $this->model_master_cashiergroup->getCollection();
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($cashier_info)) {
			$this->data['name'] = $cashier_info['cashier_name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['group'])) {
			$this->data['group'] = $this->request->post['group'];
		} elseif (!empty($cashier_info)) {
			$this->data['group'] = $cashier_info['cashier_group_id'];
		} else {
			$this->data['group'] = '';
		}
		
		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} elseif (!empty($cashier_info)) {
			$this->data['password'] = '';
		} else {
			$this->data['password'] = '';
		}
		
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($cashier_info)) {
			$this->data['email'] = $cashier_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($cashier_info)) {
			$this->data['status'] = $cashier_info['status'];
		} else {
			$this->data['status'] = 0;
		}

		$this->template = 'master/cashier_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
	
		if (!$this->user->hasPermission('modify', 'master/cashier')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 30)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if (!isset($this->request->post['group'])) {
			$this->error['group'] = $this->language->get('error_group');
		}
		
		if (!isset($this->request->post['password'])) {
			$this->error['password'] = $this->language->get('error_password');
		}
		
		$this->load->model('master/cashier');
		
		if (isset($this->request->post['email']) && !isset($this->request->get['cashier_id'])) {
			$cashier_info = $this->model_master_cashier->isCheckAlreadyUser($this->request->post['email']);
			if(empty($cashier_info)) {
				$this->error['email'] = 'The email address already used another store.';
			}
		}
		
		if (isset($this->request->post['email']) && isset($this->request->get['cashier_id'])) {
			$cashier_info = $this->model_master_cashier->isCheckAlreadyUser($this->request->post['email'],$this->request->get['cashier_id']);
			if(empty($cashier_info)) {
				$this->error['email'] = 'The email address already used another store.';
			}
		}
		
		if (!isset($this->request->post['email'])) {
			$this->error['email'] = $this->language->get('error_email');
		}
		
		if (!isset($this->request->post['cpassword'])) {
			$this->error['password'] = $this->language->get('error_password');
		}
		if ($this->request->post['cpassword'] != $this->request->post['cpassword']) {
			$this->error['password'] = $this->language->get('error_password');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() { 
	
    	if (!$this->user->hasPermission('modify', 'master/cashier')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
	  	  
		foreach ($this->request->post['selected'] as $user_id) {
			if ($this->user->getId() == $user_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}
		 
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}

}  
?>