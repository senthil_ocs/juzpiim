<?php  
class ControllerMasterCashiergroup extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->load->language('master/cashiergroup');
		
		$this->document->setTitle('cashiergroup');
    	$this->data['heading_title'] = 'cashiergroup';

		$this->load->model('master/cashiergroup');
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->getList();
		
  	}
	
	public function insert() {

		$this->language->load('master/cashiergroup');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashiergroup');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_cashiergroup->addCashierGroup($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function update() {
	
		$this->language->load('master/cashiergroup');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashiergroup');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_cashiergroup->editCashierGroup($this->request->get['cashier_group_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function delete() {
    	$this->language->load('master/cashiergroup');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/cashiergroup');
		
		printArray($this->request->post);
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $cashier_group_id) {
				$this->model_master_cashiergroup->deleteCashierGroup($cashier_group_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {
	
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('master/cashiergroup/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/cashiergroup/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['cashiers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$cashiergroup_total = $this->model_master_cashiergroup->getTotalCashierGroups();
		
		$results = $this->model_master_cashiergroup->getCashierGroups($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/cashiergroup/update', 'token=' . $this->session->data['token'] . '&cashier_group_id=' . $result['cashier_group_id'] . $url, 'SSL')
			);
			$this->data['cashiergroups'][] = array(
				'cashier_group_id' => $result['cashier_group_id'],
				'name'       => $result['cashier_group_name'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['cashier_group_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_group'] = $this->language->get('column_group');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . '&sort=cashiergroup_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $cashiergroup_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'master/cashiergroup_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['cashier_group_id'])) { 
			$this->data['action'] = $this->url->link('master/cashiergroup/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/cashiergroup/update', 'token=' . $this->session->data['token'] . '&cashier_group_id=' . $this->request->get['cashier_group_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/cashiergroup', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['cashier_group_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$cashiergroup_info = $this->model_master_cashiergroup->getCashierGroupCollection($this->request->get['cashier_group_id']);
		}
		
		$this->data['cashiergroup_collection'] = $this->model_master_cashiergroup->getCashierGroupCollection();
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($cashiergroup_info)) {
			$this->data['name'] = $cashiergroup_info['cashier_group_name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($cashiergroup_info)) {
			$this->data['remarks'] = $cashiergroup_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		
		$this->data['permissions'] = $this->model_master_cashiergroup->getPermissionFunctionKey();
		$_accessPermission	= unserialize($cashiergroup_info['permission']);
		if (isset($this->request->post['permission']['access'])) {
			$this->data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($_accessPermission['access'])) {
			$this->data['access'] = $_accessPermission['access'];
		} else { 
			$this->data['access'] = array();
		}
		
		$this->template = 'master/cashiergroup_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
	
		if (!$this->user->hasPermission('modify', 'master/cashiergroup')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 30)) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() { 
	
    	if (!$this->user->hasPermission('modify', 'master/cashiergroup')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
	  	  
		foreach ($this->request->post['selected'] as $user_id) {
			if ($this->user->getId() == $user_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}
		 
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}

}  
?>