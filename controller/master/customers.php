<?php 
class ControllerMasterCustomers extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('setting/customers');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/customers');

		$this->getList();
	}

	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'firstname';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_email'])) {
			$url.= '&filter_email='.$this->request->post['filter_email'];
		} 
		if (isset($this->request->post['filter_Type'])) {
			$url.= '&filter_Type='.$this->request->post['filter_Type'];
		} 
		
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
		} else {
			$filter_email ="";
		}
		if (isset($this->request->post['filter_Type'])) {
			$filter_Type = $this->request->post['filter_Type'];
		} else {
			$filter_Type ="";
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/

		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_email' => $filter_email,
			'filter_Type' => $filter_Type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$customer_total = $this->model_setting_customers->getTotalCustomers();
		
		$results = $this->model_setting_customers->getCustomers($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customer_id' 		=> $result['customer_id'],
				'firstname'      	=> $result['firstname'],
				'lastname'      	=> $result['lastname'],
				'email'				=> $result['primary_email'],
				'mobile'			=> $result['mobile'],
				'added_date'		=> $result['added_date'],				
				'selected'   => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Customers',
			'href'      => $this->url->link('master/customers', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('master/customers', 'token=' . $this->session->data['token'] . '&sort=firstname' . $url, 'SSL');
		$this->data['sort_lastname'] = $this->url->link('master/customers', 'token=' . $this->session->data['token'] . '&sort=lastname' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('master/customers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('master/customers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');


		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'master/customer_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}
}
?>