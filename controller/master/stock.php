<?php 
class ControllerMasterStock extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/stock');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/stock');
		
		$this->getList();
	}

	public function insert() {
		$this->language->load('master/stock');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/stock');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_stock->addStock($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/stock', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('master/stock');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/stock');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_stock->editStock($this->request->get['stock_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/stock', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('master/stock');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/stock');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $stock_id) {
				$this->model_master_stock->deletestock($stock_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/stock', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('master/stock/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/stock/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['stocks'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$stock_total = $this->model_master_stock->getTotalStocks();
		
		$results = $this->model_master_stock->getStocks($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/stock/update', 'token=' . $this->session->data['token'] . '&stock_id=' . $result['StockID'] . $url, 'SSL')
			);

			$this->data['stocks'][] = array(
				'stock_id' => $result['StockID'],
				'name'       => $result['StockName'],
				'code'       => $result['StockCode'],
				'method'       => $result['Method'],
				'position'       => $result['Position'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['stock_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_method'] = $this->language->get('column_method');
		$this->data['column_position'] = $this->language->get('column_position');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . '&sort=code' . $url, 'SSL');
		$this->data['sort_method'] = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . '&sort=method' . $url, 'SSL');
		$this->data['sort_position'] = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . '&sort=position' . $url, 'SSL');
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $stock_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'master/stock_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_method'] = $this->language->get('entry_method');
		$this->data['entry_position'] = $this->language->get('entry_position');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['method'])) {
			$this->data['error_method'] = $this->error['method'];
		} else {
			$this->data['error_method'] = '';
		}
		
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['stock_id'])) { 
			$this->data['action'] = $this->url->link('master/stock/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/stock/update', 'token=' . $this->session->data['token'] . '&stock_id=' . $this->request->get['stock_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/stock', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['stock_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$stock_info = $this->model_master_stock->getstock($this->request->get['stock_id']);
		}
		
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['stock_code'];
		} elseif (!empty($stock_info)) {
			$this->data['code'] = $stock_info['StockCode'];
		} else {
			$this->data['code'] = '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($stock_info)) {
			$this->data['name'] = $stock_info['StockName'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['method'])) {
			$this->data['method'] = $this->request->post['method'];
		} elseif (!empty($stock_info)) {
			$this->data['method'] = $stock_info['Method'];
		} else {
			$this->data['method'] = '';
		}
		
		if (isset($this->request->post['position'])) {
			$this->data['position'] = $this->request->post['position'];
		} elseif (!empty($stock_info)) {
			$this->data['position'] = $stock_info['Position'];
		} else {
			$this->data['position'] = '';
		}

		$this->template = 'master/stock_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		//if (!$this->user->hasPermission('modify', 'master/stock')) {
			//$this->error['warning'] = $this->language->get('error_permission');
		//}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 128)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if ((utf8_strlen($this->request->post['code']) < 2)) {
			$this->error['code'] = $this->language->get('error_code');
		}
		
		/*if ((utf8_strlen($this->request->post['description']) < 3) || (utf8_strlen($this->request->post['description']) > 128)) {
			$this->error['description'] = $this->language->get('error_description');
		}*/

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
	}
}
?>