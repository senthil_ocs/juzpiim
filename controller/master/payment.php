<?php 
class ControllerMasterPayment extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/payment');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/payment');
		
		$this->getList();
	}

	public function insert() {
		$this->language->load('master/payment');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/payment');
		
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		// printArray($this->request->request);exit;
			$this->model_master_payment->addPayment($this->request->request);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('master/payment');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/payment');
		
		$payment_info	= $this->model_master_payment->getPayment(trim($this->request->get['payment_id']));
		if(empty($payment_info) || !empty($payment_info['default_field'])) {
			$this->redirect($this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_payment->editPayment($this->request->get['payment_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('master/payment');
 		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('master/payment');
		if (isset($this->request->post['selected'])) {
			$exitType ='';
			foreach ($this->request->post['selected'] as $payment_id) {
				$payment_name = $this->model_master_payment->getPaymentName($payment_id);
				if($payment_name){
					$count = $this->model_master_payment->paymentTypeCheck($payment_name);
					if($count>=1){
						$exitType.= $payment_name.',';
					}else{
						$this->model_master_payment->deletePayment($payment_id);
					}
				}
			}
			
			if($exitType){
				$this->session->data['success'] = $exitType.' not deleted because it contain some transaction';
			}else{
				$this->session->data['success'] = 'Deleted successfully';
			}	
			$url = '';
		
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('master/payment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/payment/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['payments'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$payment_total = $this->model_master_payment->getTotalPayments();
		
		$results = $this->model_master_payment->getPayments($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/payment/update', 'token=' . $this->session->data['token'] . '&payment_id=' . $result['payment_id'] . $url, 'SSL')
			);

			$this->data['payments'][] = array(
				'payment_id' => $result['payment_id'],
				'name'       => $result['payment_name'],
				'position'       => $result['position'],
				'default_field' => $result['default_field'],
				'status'       => $result['status'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['payment_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_position'] = $this->language->get('column_position');
		$this->data['column_id'] = $this->language->get('column_id');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			//unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/payment', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('master/payment', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_position'] = $this->url->link('master/payment', 'token=' . $this->session->data['token'] . '&sort=position' . $url, 'SSL');
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $payment_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['settings'] = $this->url->link("setting/common", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'master/payment_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] 			= $this->language->get('entry_name');
		$this->data['entry_description']	= $this->language->get('entry_description');
		$this->data['entry_status'] 		= $this->language->get('entry_status');
		$this->data['entry_position'] 		= $this->language->get('entry_position');
		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['payment_id'])) { 
			$this->data['action'] = $this->url->link('master/payment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/payment/update', 'token=' . $this->session->data['token'] . '&payment_id=' . $this->request->get['payment_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/payment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['payment_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$payment_info = $this->model_master_payment->getPayment($this->request->get['payment_id']);
		}
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($payment_info)) {
			$this->data['name'] = $payment_info['payment_name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($payment_info)) {
			$this->data['description'] = $payment_info['description'];
		} else {
			$this->data['description'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($payment_info)) {
			$this->data['status'] = $payment_info['status'];
		} else {
			$this->data['status'] = '';
		}
		
		if (isset($this->request->post['position'])) {
			$this->data['position'] = $this->request->post['position'];
		} elseif (!empty($payment_info)) {
			$this->data['position'] = $payment_info['position'];
		} else {
			$this->data['position'] = '';
		}

		$this->data['settings'] = $this->url->link("setting/common", '&token=' . $this->session->data['token'], 'SSL');
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['payment'] = $this->url->link('master/payment', '&token=' . $this->session->data['token'], 'SSL');
		
		$this->data['route'] = 'master/payment';

		$this->template = 'master/payment_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		//if (!$this->user->hasPermission('modify', 'master/payment')) {
			//$this->error['warning'] = $this->language->get('error_permission');
		//}

		if ((utf8_strlen($this->request->post['name']) < 3)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
	/*	if (isset($this->request->post['name']) && strtolower($this->request->post['name'])=='cash') {
			$this->error['name'] = 'Please enter unique Payment Name';
		}
		
		if ((utf8_strlen($this->request->post['description']) < 3)) {
			$this->error['description'] = $this->language->get('error_description');
		}*/
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		
	}
}
?>