<?php 
class ControllerMasterDiscountreports extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('inventory/discount');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/discount');
		
		$this->getList();
	}
	
	public function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		

  		$this->data['discounts'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$discount_total = $this->model_inventory_discount->getTotalDiscounts();
		
		$results = $this->model_inventory_discount->getDiscounts($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/discount/update', 'token=' . $this->session->data['token'] . '&discount_id=' . $result['discount_id'] . $url, 'SSL')
			);

			$this->data['discounts'][] = array(
				'discount_id' 	 => $result['discount_id'],
				'discount_name'  => $result['discount_name'],
				'discount_type'  => $this->model_inventory_discount->getTypeName($result['discount_type']),
				'added_date'     => $result['added_date'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['discount_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_discount_type'] = $this->language->get('column_discount_type');
		$this->data['column_amount'] = $this->language->get('column_amount');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_discount_name'] = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . '&sort=discount_name' . $url, 'SSL');
		$this->data['sort_discount_type'] = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . '&sort=discount_type' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $discount_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/discountreports', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['back'] = $this->url->link("report/report", 'token='.$this->session->data['token'], 'SSL');

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'master/discount_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
}
?>