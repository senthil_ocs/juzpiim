<?php
class ControllerMasterTerms extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('setting/terms');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/terms');

		$this->getList();
	}


	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'terms_name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_code'])) {
			$url.= '&filter_code='.$this->request->post['filter_code'];
		} 
		if (isset($this->request->post['filter_status'])) {
			$url.= '&filter_status='.$this->request->post['filter_status'];
		}
		if (isset($this->request->post['filter_days'])) {
			$url.= '&filter_days='.$this->request->post['filter_days'];
		} 
		
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_code'])) {
			$filter_code = $this->request->post['filter_code'];
		} else {
			$filter_code ="";
		}
		if (isset($this->request->post['filter_days'])) {
			$filter_days = $this->request->post['filter_days'];
		} else {
			$filter_days ="";
		}
		if (isset($this->request->post['filter_status'])) {
			$filter_status = $this->request->post['filter_status'];
		} else {
			$filter_status ="";
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		$this->data["filterstatus"] = $filter_status;
		/*** Filter-Ends***/

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Terms',
			'href'      => $this->url->link('master/terms', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		
		$this->data['terms'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_code' => $filter_code,
			'filter_days' => $filter_days,
			'filter_status' => $filter_status,

			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$terms_total = $this->model_setting_terms->getTotalTerms();

		$results = $this->model_setting_terms->getTerms($data);
		
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/terms/update', 'token=' . $this->session->data['token'] . '&terms_id=' . $result['terms_id'] . $url, 'SSL')
			);

			$this->data['termsValues'][] = array(
				'terms_id'    => $result['terms_id'],
				'terms_name'   => $result['terms_name'],
				'terms_code'   => $result['terms_code'],
				'noof_days'	   => $result['noof_days'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
				'selected'   => isset($this->request->post['selected']) && in_array($result['terms_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_terms_name'] = $this->language->get('column_termsname');
		$this->data['column_terms_code'] = $this->language->get('column_termscode');
		$this->data['column_noof_days'] = $this->language->get('column_noof_days');
		$this->data['column_status'] = $this->language->get('column_termstatus');
		$this->data['column_date_added'] = $this->language->get('column_date_added');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_terms_name'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . '&sort=terms_name' . $url, 'SSL');
		$this->data['sort_terms_code'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . '&sort=terms_code' . $url, 'SSL');
		$this->data['sort_noof_days'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . '&sort=noof_days' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_date_added'] = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . '&sort=date_added' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $terms_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/terms', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		//print_r($this->data['pagination']);

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['back'] = $this->url->link('report/report', 'token='. $this->session->data['token'] , 'SSL');
		$this->template = 'master/terms_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

}
?>