<?php 
class ControllerMasterReports extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/reports');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/reports');
		
		$this->getList();
	}
	
	public function printReports() {
		$this->language->load('master/reports');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->load->model('master/reports');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') || isset($this->request->get['master_name'])) {	
			$company_id	= $this->session->data['company_id'];
			$master_name = $this->request->get['master_name'];
			$company_details = $this->model_master_reports->getCompanyDetails($company_id);
			$companyInfo	= $this->getCompanyAddress($company_details);
			if($companyInfo) {
				$this->data['companyInfo']	= $companyInfo;
			} else {
				$this->data['companyInfo']	='';
			}
			//print_r($master_name); die();
			if (strtolower($master_name) == 'paymode') {
				$this->data['paymodeList'] = $this->model_master_reports->getPaymodeList();
			} elseif (strtolower($master_name) == 'salesman') {
				$this->data['salesmanList'] = $this->model_master_reports->getSalesmanList();
			} elseif (strtolower($master_name) == 'vendor') {
				$vendorInfo = $this->model_master_reports->getVendorList();
				
				foreach ($vendorInfo as $result) {					
					if(!empty($result['country'])){
						$country_name = $this->model_master_reports->getCountryName($result['country']);
						$country_name = $country_name['name'];
					} else {
						$country_name	= "";
					}		
					$this->data['vendorList'][] = array(
						'vendor_id'   => $result['vendor_id'],
						'code' 		  => $result['code'],
						'vendor_name' => $result['vendor_name'],
						'address1' 	  => $result['address1'],
						'address2' 	  => $result['address2'],
						'country'     => $country_name,
						'postal_code' => $result['postal_code'],
						'phone'       => $result['phone'],
						'email'       => $result['email']
					);
				}
				
			} elseif (strtolower($master_name) == 'cashier') {
				$cashierList = $this->model_master_reports->getCashierList();
				
				foreach ($cashierList as $result) {
					$action = array();
					$this->data['cashierList'][] = array(
						'cashier_id'   => $result['cashier_id'],
						'cashier_name' => $result['cashier_name'],
						'group'        => $result['cashier_group_name'],
						'email'        => $result['email'],
						'status'       => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
					);
				}				
			} elseif (strtolower($master_name) == 'tax') {
				$this->data['taxList'] = $this->model_master_reports->getTaxList();
			}
			
			$this->data['master_name'] = $master_name;
			$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		} else {
		    $this->redirect($this->url->link('master/reports', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->template = 'master/print.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}

	protected function getList() {
					
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$url = '';

  		$this->data['print'] = $this->url->link('master/reports/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['bins'] = array();

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['button_print'] = $this->language->get('button_print');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';
		
		$this->data['sort_name'] = $this->url->link('master/reports', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_description'] = $this->url->link('master/reports', 'token=' . $this->session->data['token'] . '&sort=description' . $url, 'SSL');

		$this->template = 'master/reports_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']." ".$company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
		}
		return $company_address;
	}

}
?>