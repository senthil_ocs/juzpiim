<?php 
class ControllerMasterUom extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('master/uom');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uom');
		
		$this->getList();
	}

	public function insert() {
		$this->language->load('master/uom');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uom');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_uom->addUom($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('master/uom');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uom');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_master_uom->editUom($this->request->get['uom_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('master/uom');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('master/uom');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $uom_id) {
				$this->model_master_uom->deleteuom($uom_id);
			}
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Uom List',
			'href'      => $this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

  		$this->data['insert'] = $this->url->link('master/uom/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('master/uom/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['uoms'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$uom_total = $this->model_master_uom->getTotaluoms();
		
		$results = $this->model_master_uom->getuoms($data);
		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('master/uom/update', 'token=' . $this->session->data['token'] . '&uom_id=' . $result['uom_id'] . $url, 'SSL')
			);

			$this->data['uoms'][] = array(
				'uom_id' => $result['uom_id'],
				'name'       => $result['uom_name'],
				'code'       => $result['code'],
				'description'       => $result['description'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['uom_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_description'] = $this->language->get('column_description');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('master/uom', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_description'] = $this->url->link('master/uom', 'token=' . $this->session->data['token'] . '&sort=description' . $url, 'SSL');
		$this->data['sort_code'] = $this->url->link('master/uom', 'token=' . $this->session->data['token'] . '&sort=code' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $uom_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		//$this->data['route'] = $this->request->get['route'];
		$this->data['route'] = 'master/uom';

		$this->template = 'master/uom_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_description'] = $this->language->get('entry_description');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

				$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Uom List',
			'href'      => $this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

  		if (!isset($this->request->get['uom_id'])) { 
			$this->data['action'] = $this->url->link('master/uom/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('master/uom/update', 'token=' . $this->session->data['token'] . '&uom_id=' . $this->request->get['uom_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('master/uom', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['uom_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$uom_info = $this->model_master_uom->getuom($this->request->get['uom_id']);
			$this->data['edit_form'] = '1';
		}
		
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($uom_info)) {
			$this->data['code'] = $uom_info['code'];
		} else {
			$this->data['code'] = '';
		}

		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($uom_info)) {
			$this->data['name'] = $uom_info['uom_name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($uom_info)) {
			$this->data['description'] = $uom_info['description'];
		} else {
			$this->data['description'] = '';
		}

		$this->data['route'] = 'master/uom';

		$this->template = 'master/uom_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'master/uom')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ((utf8_strlen($this->request->post['name']) < 2) || (utf8_strlen($this->request->post['name']) > 128)) {
			$this->error['name'] = $this->language->get('error_name');
		}
		
		if ((utf8_strlen($this->request->post['code']) < 2)) {
			$this->error['code'] = $this->language->get('error_code');
		}

		$exist = $this->model_master_uom->getUOMByCode($this->request->post['code'],$this->request->get['uom_id']);
		if($exist>=1){
			$this->error['warning'] = 'Given UOM code already Exist';
		}	
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'master/uom')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
    	foreach ($this->request->post['selected'] as $uom_code) {
			$product_total = $this->model_master_uom->getTotalProductsByUOM($uom_code);
			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
		}
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
}
?>