<?php  
class ControllerMasterCompany extends Controller { 
	private $error = array();
	
	// Company Form Fields
	public function companyFormFields() {
		$forms = array('company_id','location_code','name','logo','address1','address2','country',
						'city','state','postal_code','email','web_url','phone','fax','remarks','business_reg_no',
						'gst_reg','gst_perc','post_gst_type','purchase_gst_type','sales_gst_type','display_message1',
						'display_message2','max_discount','price_change','bill_message','print_promotion',
						'last_eod_date','config_server','config_name','config_email');
		return $forms;
	
	}	
	          
	public function index() { 
		$this->load->model('tool/image');
		$this->language->load('master/company');
		$this->load->model('master/company');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['action'] = $this->url->link('master/company/update', 'token=' . $this->session->data['token'], 'SSL');
		$company_id	= $this->session->data['company_id'];
		
		$formFieldNames	= $this->companyFormFields();
		if (isset($company_id)) {
      		$company_info = $this->model_master_company->getCompanyDetails($company_id);
    	}
		if(isset($company_info['logo'])) {
			$this->data['preview_logo'] = $this->model_tool_image->resize($company_info['logo'], 75, 75);
		} else {
			$this->data['preview_logo']	='';
		}
		
		if(!empty($formFieldNames)) {
			foreach($formFieldNames as $formvalue){
				if(isset($this->request->post[$formvalue])) {
					$this->data[$formvalue]	= $this->request->post[$formvalue];
				} elseif (!empty($company_info[$formvalue])) {
					$this->data[$formvalue]	= $company_info[$formvalue];
				} else {
					$this->data[$formvalue]	='';
				}
			}
		}
		
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_companyname'] = $this->language->get('entry_companyname');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_city'] = $this->language->get('entry_city');
		$this->data['entry_state'] = $this->language->get('entry_state');
		$this->data['entry_postal'] = $this->language->get('entry_postal');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_weburl'] = $this->language->get('entry_weburl');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_businessregno'] = $this->language->get('entry_businessregno');
		$this->data['entry_gstreg'] = $this->language->get('entry_gstreg');
		$this->data['entry_gstperc'] = $this->language->get('entry_gstperc');
		$this->data['entry_postgsttype'] = $this->language->get('entry_postgsttype');
		$this->data['entry_purchasegsttype'] = $this->language->get('entry_purchasegsttype');
		$this->data['entry_salesgsttype'] = $this->language->get('entry_salesgsttype');
		$this->data['entry_displaymessages1'] = $this->language->get('entry_displaymessages1');
		$this->data['entry_displaymessages2'] = $this->language->get('entry_displaymessages2');
		$this->data['entry_discount'] = $this->language->get('entry_discount');
		$this->data['entry_pricechange'] = $this->language->get('entry_pricechange');
		$this->data['entry_billmessage'] = $this->language->get('entry_billmessage');
		$this->data['entry_promotions'] = $this->language->get('entry_promotions');
		$this->data['entry_lastdate'] = $this->language->get('entry_lastdate');
		$this->data['entry_configemail'] = $this->language->get('entry_configemail');
		$this->data['entry_server'] = $this->language->get('entry_server');
		$this->data['entry_displayname'] = $this->language->get('entry_displayname');
		$this->data['entry_fromemailid'] = $this->language->get('entry_fromemailid');
		$this->data['entry_password'] = $this->language->get('entry_password');
		$this->data['entry_cpassword'] = $this->language->get('entry_cpassword');
		
		$this->data['button_update'] = $this->language->get('button_update');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->template = 'master/company.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	public function update() { 
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['password'])) {
			$this->data['error_password'] = $this->error['password'];
		} else {
			$this->data['error_password'] = '';
		}
		$company_id	= $this->session->data['company_id'];
		$this->load->model('master/company');
		$this->language->load('master/company');
		$formFieldNames	= $this->companyFormFields();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')  && $this->validateForm() ) {
			
			//$this->load->model('tool/image');
			//echo $testImage	= $this->model_tool_image->resize($this->request->post['CompanyLogo'], 100, 100);
			$imageName	= $this->imageUploads();
			if($imageName) {
				$this->request->post['logo']	= $imageName; 
			}
			$result	= $this->model_master_company->editCompanyDetails($company_id, $this->request->post,$formFieldNames);
			if($result) {
				$this->session->data['success'] = $this->language->get('text_success');
				$this->data['success']	='Successfully update company details.';
			}
			if (isset($this->error['warning'])) {
				$this->data['error_warning'] = $this->error['warning'];
			} else {
				$this->data['error_warning'] = '';
			}
		}
		$this->redirect(HTTPS_SERVER . 'index.php?route=master/company&token=' . $this->session->data['token'] . $url);
  	}
	
	public function getExtension($str) {
	
	 $i = strrpos($str,".");
	 if (!$i) { return ""; } 
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
	 
	}
	
	public function imageUploads() {
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$name = $_FILES['logo']['name'];
		$size = $_FILES['logo']['size'];
		if(strlen($name)){
			$ext = $this->getExtension($name);
			if(in_array($ext,$valid_formats)) {
				if($size<(1024*1024)) {
					$txt	='';
					$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $_FILES['logo']['tmp_name'];
					if(move_uploaded_file($tmp, DIR_IMAGE.$actual_image_name)) {
						return $actual_image_name;
					}
				}
			} 
		}
		$this->error['warning'] = 'unable to upload image. Please contact administrator';
		return false;
	}
	
	

	public function validateForm() {
		//return true;
		if (!$this->user->hasPermission('modify', 'master/company')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
    	//if ((utf8_strlen($this->request->post['password']) < 6)) {
      		//$this->error['password'] = $this->language->get('error_password');
    	//}
		
    	//if ($this->request->post['c_password'] != $this->request->post['password']) {
      		//$this->error['password'] = $this->language->get('error_password');
    	//}  
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}  
?>