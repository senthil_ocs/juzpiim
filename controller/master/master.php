<?php  
class ControllerPagesMaster extends Controller { 
	private $error = array();
	
	// Company Form Fields
	public function companyFormFields() {
		$forms = array('CompanyID','LocationCode','CompanyName','Address1','Address2','Country',
						'City','State','PostalCode','Email','Url','Phone','Fax','Remarks','BRNNo',
						'GSTRegNo','GstPerc','GstType','PurchaseGstType','CreditSalesGstType','DisplayMessage1',
						'DisplayMessage2','MaxDiscountAllowed','MaxPriceChangeAllowed','BillPrint','promotion_receipt',
						'EODDate','Server','DisplayName','FromEmailId');
		return $forms;
	
	}	
	          
	public function index() { 
	
		$this->language->load('pages/master');
		$this->load->model('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->template = 'pages/master/main.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	public function company() { 
		//echo "HERE"; exit;
		$this->language->load('pages/master');
		$this->load->model('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['action'] = $this->url->link('pages/master/company', 'token=' . $this->session->data['token'], 'SSL');
		
		$company_id	= 1;
		$formFieldNames	= $this->companyFormFields();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			//printArray($this->request->post); exit;
			$this->model_pages_master->editCompanyDetails($company_id, $this->request->post,$formFieldNames);
			$this->session->data['success'] = 'The Company Details updated successfully.';
		}
		if (isset($company_id)) {
      		$company_info = $this->model_pages_master->getCompanyDetails($company_id);
    	}
		if(!empty($formFieldNames)) {
			foreach($formFieldNames as $formvalue){
				if(isset($this->request->post[$formvalue])) {
					$this->data[$formvalue]	= $this->request->post[$formvalue];
				} elseif (!empty($company_info[$formvalue])) {
					$this->data[$formvalue]	= $company_info[$formvalue];
				} else {
					$this->data[$formvalue]	='';
				}
			}
		}
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		
    	$this->template = 'pages/master/company.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	
	public function location() { 
	
		$this->language->load('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	$this->template = 'pages/master/location.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	
	public function paymode() { 
		$this->language->load('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	$this->template = 'pages/master/paymode.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	
	public function tax() { 
		$this->language->load('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title');
    	$this->template = 'pages/master/tax.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	
	public function vendor() {
		$this->language->load('pages/master');
		$this->document->setTitle($this->language->get('heading_title'));
    	$this->data['heading_title'] = $this->language->get('heading_title'); 
    	$this->template = 'pages/master/vendor.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
  	}
	

	public function validateCompanyForm() {
	
    	if ((utf8_strlen($this->request->post['password']) < 5) || (utf8_strlen($this->request->post['password']) > 20)) {
      		$this->error['password'] = $this->language->get('error_password');
    	}

    	if ($this->request->post['c_password'] != $this->request->post['password']) {
      		$this->error['c_password'] = $this->language->get('error_confirm');
    	}  
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}  
?>