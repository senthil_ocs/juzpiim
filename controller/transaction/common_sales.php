<?php
class ControllerTransactionCommonSales extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/common'); 
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->data['location_code'] = $this->session->data['location_code'];		
		$this->data['heading_title'] = $this->language->get('sales_heading_title');

		$this->data['text_supplier'] ='Customer';
		$this->data['text_general']  ='Sales';

		if($this->user->hasPermission('access', 'transaction/quotation') || $this->user->hasPermission('modify', 'transaction/quotation')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/quotation", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Sales Quotation',
				                               "icon"  =>'fa fa-quote-left'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/sales", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"     => $this->language->get('sales_id'),
				                               "Text" => 'Sales Order',
				                               "icon" =>'fa fa-share'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
			$this->data["general"][] = array( "link"   => $this->url->link("transaction/sales_invoice", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Sales Invoice',
				                               "icon"  =>'fa fa-address-book-o'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/delivery') || $this->user->hasPermission('modify', 'transaction/delivery')){
			$this->data["general"][] = array( "link"   => $this->url->link("transaction/delivery", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Delivery',
				                               "icon"  =>'fa fa-truck'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/payment') || $this->user->hasPermission('modify', 'transaction/payment')){
			$this->data["general"][] = array( "link"   => $this->url->link("transaction/payment", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Payment',
				                               "icon"  =>'fa fa-credit-card'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales_return') || $this->user->hasPermission('modify', 'transaction/sales_return')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/sales_return", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"   => $this->language->get('salesreturn_id'),
				                               "Text" => $this->language->get('text_sales_return'),
				                               "icon" =>'fa fa-reply'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/service_sales') || $this->user->hasPermission('modify', 'transaction/service_sales')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/service_sales", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"   => $this->language->get('sales_id'),
				                               "Text" => 'Service Note',
				                               "icon" => 'fa fa-address-book-o'
				                               );
		} 

		if($this->user->hasPermission('access', 'customers/customers') || $this->user->hasPermission('modify', 'customers/customers')){
			$this->data["sales"][] = array( "link" => $this->url->link("setting/customers", 'token=' . $this->session->data['token'], 'SSL'),
						                       "altkey"=> '',
						                       "id"     => $this->language->get('purchase_id'),
				                               "Text" => 'Customers',
				                               "icon" =>'fa fa-user'
				                               );
		}		
		if($this->user->hasPermission('access', 'customers/customerspricing') || $this->user->hasPermission('modify', 'customers/customerspricing')){
			$this->data["sales"][] = array( "link" => $this->url->link("customers/customerspricing", 'token=' . $this->session->data['token'], 'SSL'),
				                               "altkey"=> '',
				                               "id"     => $this->language->get('purchasereturn_id'),
				                               "Text" => 'Customer Pricing',
				                               "icon" =>'fa fa-money'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/lazada_order_import') || $this->user->hasPermission('modify', 'transaction/lazada_order_import')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/lazada_order_import", 'token=' . $this->session->data['token'],  'SSL'),
				                               "altkey"=> '',
				                               "id"    => $this->language->get('purchasereturn_id'),
				                               "Text"  => 'Lazada Import',
				                               "icon"  => 'fa fa-cloud-download'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/qsm_order_import') || $this->user->hasPermission('modify', 'transaction/qsm_order_import')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/qsm_order_import", 'token=' . $this->session->data['token'],  'SSL'),
				                               "altkey"=> '',
				                               "id"    => $this->language->get('purchasereturn_id'),
				                               "Text"  => 'QSM Import',
				                               "icon"  => 'fa fa-cloud-download'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/lazada_order_import') || $this->user->hasPermission('modify', 'transaction/lazada_order_import')){
			
			$this->data["general"][] = array( "link" => $this->url->link("transaction/shopee_order_import", 'token=' . $this->session->data['token'],  'SSL'),
	                               "altkey"=> '',
	                               "id"    => $this->language->get('purchasereturn_id'),
	                               "Text"  => 'Shopee Import',
	                               "icon"  => 'fa fa-cloud-download'
	                               );
		}

		$this->data['token'] = $this->session->data['token'];	
		$this->template = 'transaction/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
 }
?>