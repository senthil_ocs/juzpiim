<?php
class ControllerTransactionSales extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Sales Orders');
		$this->load->model('transaction/sales');
		$this->clearTempFolder();
		$this->getList();
	}
	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/sales');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			}
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			$this->response->setOutput(json_encode($res));
		}
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/sales');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}else if($bill_discount_percentage =='' || $bill_discount_percentage=='0' || $bill_discount_price=='0' || $bill_discount_price==''){
				if ($this->session->data['bill_discount']) {
					unset($this->session->data['bill_discount']);
					unset($this->session->data['bill_discount_mode']);
				}
				unset($this->session->data['bill_discount_percentage']);
				unset($this->session->data['bill_discount_price']);
			}
		}
		if(isset($this->request->post['handling_fee']) && $this->request->post['handling_fee'] > 0){
			$this->session->data['handling_fee'] = $this->request->post['handling_fee'];
			$res['handling_fee']    = $this->currency->format($this->session->data['handling_fee']);
		}else{
			unset($this->session->data['handling_fee']);
			$res['handling_fee']    = $this->currency->format('0');
		}
		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			// printArray($apply_tax_type); die;
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					$res['discount_price']  	 = $bill_discount_price;
					$res['discount_percentage']  = $bill_discount_percentage;

					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					$res['discountRow'] = '';
					$colspan = $this->request->post['form'] == 'SalRetn' ? 7 : 10;

					if($res['discount_value'] !=''){
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="'.$colspan.'" align="right" class="purchase_total_left">'.$lable.'</td>
							<td class="purchase_total_right insertproduct">
							<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="textbox row_total_box text-right" readonly="readonly" /></td></tr>';
					}
				}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				$res['bill_discount']     = $this->session->data['bill_discount'];
				$res['bill_discount_mode']= $this->session->data['bill_discount_mode'];
			}
			
			$this->response->setOutput(json_encode($res));
	}
	
	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(!empty($this->session->data['cart_purchase'])){		   	
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
 		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}

		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = $purchase['total'] - $purchase['tax'];
					} else {
					    $totalValue = $purchase['tax'] + $purchase['total'];
					}
				$purchase['price'] = str_replace("$","",$purchase['price']);
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center"><textarea class="textbox description" name="cust_description['.$purchase['product_id'].']" rows = "2" cols ="60" id="desc_'.$purchase['product_id'].'" data-product_id="'.$purchase['product_id'].'" data-code="'.$purchase['code'].'" onblur="update_qty_price('.$purchase['product_id'].');">'.strip_tags(nl2br($purchase['description'])).'</textarea></td>

	                   <td class="center order-nopadding"><input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
	                   <td class="text-right order-nopadding">
	                   	<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace(",","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>';
					$str.='<td class="text-right" id="subTot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>
					   
					   <td align="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type == '2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format($res['sub_total_value']);
					}
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					$tax_text = 'GST';
					$tax_text = $apply_tax_type =='2' ? $tax_text."(7% Incl)" : $tax_text."(7% Excl)";
					$colspan = $this->request->post['form'] == 'SaleService' ? 9 : 10;
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan='.$colspan.'>'.$tax_text.'</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/sales');
			$this->data['filter_name']      = $this->request->post['sku'];
			$this->data['location_code']    = $this->request->post['location_code'];
			$this->data['transaction_date'] = $this->request->post['transactiondate'];
			$this->data['customer_code']    = $this->request->post['customercode'];
			$_productDetails = $this->model_transaction_sales->getProductByNamenew($this->data);

			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
                    $name_display = $_productDetails[$i]['name'];
                    
					$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($name_display).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
                    $name_display = $_productDetails[$i]['name'];
                     
					$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($name_display).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}
	public function getProductDetailsbyCustomer($data='', $type = '') {
			$this->load->model('transaction/sales');
			$this->data['sku'] = $this->request->post['sku'];
			$this->data['location_code']    = $this->request->post['location_code'];
			$this->data['transaction_date'] = changedates($this->request->post['transactiondate']);
			$this->data['customer_code']    = $this->request->post['customercode'];
			$CustomerDetails    = $this->model_transaction_sales->getcustomerPrice($this->data);
			for($i=0;$i<count($CustomerDetails);$i++){
			$values =  $CustomerDetails[$i]['sku'].'||'.$CustomerDetails[$i]['customer_price'];
		    }
		    echo $values;
	}
	public function insert() {
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');
		// echo "string"; die;
         $type='insert';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['reference_to_date'])) {
				$data['reference_to_date']	=  $this->request->post['reference_to_date'];
			} else {
				$data['reference_to_date']	= '';
			}
			if(isset($this->request->post['sales_quotation_trans_no'])) {
				$data['sales_quotation_trans_no']	=  $this->request->post['sales_quotation_trans_no'];
			} else {
				$data['sales_quotation_trans_no']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
				$data['tax_type']   = $data['tax_type'] =='1' ? '0' : '1';
			} else {
				$data['tax_type'] = '';
			}
			
			if (isset($this->request->post['currency_code'])) {
				$data['currency_code'] = $this->request->post['currency_code'];
			} else {
				$data['currency_code'] = '';
			}
			if (isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate'] = $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate'] = '';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '';
			}
			if (isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			} else {
				$data['shipping_id'] = '';
			}
			if (isset($this->request->post['sales_man'])) {
				$data['sales_man'] = $this->request->post['sales_man'];
			} else {
				$data['sales_man'] = '';
			}
			if (isset($this->request->post['delivery_remarks'])) {
				$data['delivery_remarks'] = $this->request->post['delivery_remarks'];
			} else {
				$data['delivery_remarks'] = '';
			}
			if (isset($this->request->post['discount_remarks'])) {
				$data['discount_remarks'] = $this->request->post['discount_remarks'];
			} else {
				$data['discount_remarks'] = '';
			}
			if (isset($this->request->post['network_id'])) {
				$data['network_id'] = $this->request->post['network_id'];
			} else {
				$data['network_id'] = '';
			}
			if (!empty($this->request->post['cust_description'])) {
				$data['cust_description'] = $this->request->post['cust_description'];
			} else {
				$data['cust_description'] = '';
			}

			$product_data = array();
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'price'                     => $product['price'],
					'quantity'                  => $product['quantity'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'sku_price'                 => $product['sku_price'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'description'               => $product['description']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = '';
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total']  = $total;
			$costMethod     = $this->config->get('config_average_cost_method');
			// printArray($data); die;
			$data['transaction_no']	= $this->getTransNo();			
			$this->model_transaction_sales->addSalesnew($data,$costMethod);
			if($_FILES["attachment"]['name'] != '') {
				$response = $this->uploadAttachment($this->request->post['transaction_no']);
				if($response != ''){
					$this->model_transaction_sales->addAttachment($response,$this->request->post['transaction_no']);
				}
			}
			$this->session->data['success'] = 'Success: You have added sales order!';
			$this->clearPurchaseData();
			$url = '';
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}
	public function update() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		$this->load->model('transaction/purchase');
		
		$type = 'update';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->get['purchase_id'])) {
				$data['purchase_id']	=  $this->request->get['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= '';
			}
            
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['reference_to_date'])) {
				$data['reference_to_date']	=  $this->request->post['reference_to_date'];
			} else {
				$data['reference_to_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			} else {
				$data['currency_code']	= '';
			}
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate']	= '';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '';
			}
			if (isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			} else {
				$data['shipping_id'] = '';
			}
			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
				$data['tax_type']   = $data['tax_type'] =='1' ? '0' : '1';
			} else {
				$data['tax_type'] = '';
			}
			if (isset($this->request->post['sales_man'])) {
				$data['sales_man'] = $this->request->post['sales_man'];
			} else {
				$data['sales_man'] = '';
			}
			if (isset($this->request->post['delivery_remarks'])) {
				$data['delivery_remarks'] = $this->request->post['delivery_remarks'];
			} else {
				$data['delivery_remarks'] = '';
			}
			if (isset($this->request->post['discount_remarks'])) {
				$data['discount_remarks'] = $this->request->post['discount_remarks'];
			} else {
				$data['discount_remarks'] = '';
			}
			if (isset($this->request->post['network_id'])) {
				$data['network_id'] = $this->request->post['network_id'];
			} else {
				$data['network_id'] = '';
			}
			if (!empty($this->request->post['cust_description'])) {
				$data['cust_description'] = $this->request->post['cust_description'];
			} else {
				$data['cust_description'] = '';
			}
			$product_data = array();
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'sku_price'                 => $product['sku_price'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'description'               => $product['description']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total']  = $total;
			$this->model_transaction_sales->editSalesnew($data['purchase_id'], $data);
			if($_FILES["attachment"]['name'] != '') {
				$response = $this->uploadAttachment($this->request->post['transaction_no']);
				if($response != ''){
					$this->model_transaction_sales->addAttachment($response,$this->request->post['transaction_no']);
				}
			}
			$this->session->data['success'] = 'Success: You have modified sales order';
			$this->clearPurchaseData();
			
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}

		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}

		$filter_network = array();   //For sales Channel
		if (!empty($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
		}else{
			$filter_network = array($filter_location);
		}
		if(!empty($filter_network)){
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
        if(!$page){
			$page = 1;
		}
			
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['column_action']   	= $this->language->get('column_action');
		$this->data['heading_title']   	= $this->language->get('Sales');
		$this->data['text_tran_no']    	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['error_bulk_convert_invoice'])) {
			$this->data['error_warning'] = $this->session->data['error_bulk_convert_invoice'];
			unset($this->session->data['error_bulk_convert_invoice']);
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		$filter_isinvoiced = null;
		if (isset($_REQUEST['filter_isinvoiced'])) {
			$filter_isinvoiced = $_REQUEST['filter_isinvoiced'];
			$url .= '&filter_isinvoiced=' . $filter_isinvoiced;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		$url .= '&filter_location=' . $filter_location;
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		} else{
			$networks = $this->cart->getChannelsList();
			foreach ($networks as $network) {
				$filter_network[] = $network['id'];
				$url .= '&filter_network[]='.$network['id'];
			}
		}

		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales List',
			'href'      => $this->url->link('transaction/sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['insert']      = $this->url->link('transaction/sales/insert', 'token='.$this->session->data['token'], 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/sales', 'token='.$this->session->data['token'] . $url, 'SSL');

		$this->data['purchases']    = array();
		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_order_number' 	=> $filter_order_number,
			'filter_sales_person'   => $filter_sales_person,			
			'filter_network'  	 	=> $filter_network,			
			'filter_location'	 	=> $filter_location,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_payment_status'	=> $filter_payment_status,
			'filter_isinvoiced'		=> $filter_isinvoiced,
			'filter_product'		=> $filter_product,
			'filter_product_id'		=> $filter_product_id,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit')
		);
		$this->data['filters']   	= $data;
		$sales_total            	= $this->model_transaction_sales->getTotalSalesnew($data);
		$results                 	= $this->model_transaction_sales->getSalesListnew($data);
		$this->data['networks']  	= $this->cart->getChannelsList();
		$this->data['customers'] 	= $this->model_transaction_sales->getB2BCustomers();
		
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$vendorDetail = array();
		$userDetail   = array();
		
		foreach ($results as $result) {
			$vendorDetail  = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
			$userDetail    = $this->model_user_user->getUser($result['createdby']);
			$network       = $this->model_transaction_sales->getNetworkName($result['network_id']);
			if($result['reference_to_date'] =='1970-01-01' || $result['reference_to_date'] ==''){
				$result['reference_to_date'] = '';
			}else{
				$result['reference_to_date'] = date('d/m/Y',strtotime($result['reference_to_date']));
			}
            $purchase_id = '';
            if($result['istagged']){
			     $purchase_id = $this->model_transaction_sales->getTaggedPurchaseId($result['invoice_no']);
            }

			$this->data['purchases'][] = array(
				'pagecnt'           => ($page-1) * $this->config->get('config_admin_limit'),
				'invoice_no'        => $result['invoice_no'],
				'transaction_no'    => $result['invoice_no'],
				'xero_sales_id' 	=> $result['xero_sales_id'],
				'network' 	        => $network,
				'transaction_date'  => date('d/m/Y',strtotime($result['invoice_date'])),
				'transaction_type'  => $result['discount_type'],
				'delivery_status'   => $result['delivery_status'],
				'delivery_from_date'=> $result['header_remarks'] !='' ? date('d/m/Y',strtotime($result['header_remarks'])): '',
				'delivery_to_date'  => $result['reference_to_date'],
				'payment_status'    => $result['payment_status'],
				'status'            => $result['status'],
				'vendor_code'       => $vendorDetail['customercode'],
				'location'          => $result['location_code'],
				'location_name'     => $result['location_name'],
				'vendor_name'       => $vendorDetail['name'],
				'total'             => $result['net_total'],
				'isinvoice'         => $result['isinvoice'],
				'istagged'     		=> $result['istagged'],
				'network_order_id'  => $result['network_order_id'] !='' ?$result['network_order_id']: '-',
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'modify_button'     => $this->url->link('transaction/sales/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'view_button'       => $this->url->link('transaction/sales/view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'delete_button'     => $this->url->link('transaction/sales/bulk_delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
			    'create_button'     => $this->url->link('transaction/sales/updateInvoiceNumber','token=' . $this->session->data['token'].'&purchase_id='.$result['invoice_no'].$url,'SSL'),
				'download_button'   => $this->url->link('transaction/sales/download_pdf', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'purchase_order_id' => $this->url->link('transaction/purchase/view', 'token=' . $this->session->data['token'] . '&purchase_id='.$purchase_id, 'SSL')
			);
		}
		$this->data['users'] = $this->model_user_user->getUserDetails();
		$this->data['salesmanlist']	= $this->model_transaction_sales->getSalesMansList();
		$this->data['Tolocations']  = $this->cart->getLocation($this->session->data['location_code']);

		$pagination 		= new Pagination();
		$pagination->total 	= $sales_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['route']= $this->request->get['route'];
		$this->template 	= 'transaction/sales_list.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {
 		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/customers');
		$this->load->model('transaction/sales');
		$this->load->model('setting/location');
		$this->load->model('setting/location');

		$this->data['heading_title'] 	 = $this->language->get('Sales');
		$this->data['entry_tran_no'] 	 = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] 	 = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type']   = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_customer_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_customer_name');
		$this->data['entry_refno']    	 = $this->language->get('entry_refno');
		$this->data['entry_refdate']     = $this->language->get('entry_refdate');
		$this->data['text_inventory_code']= $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty']    		 = $this->language->get('text_qty');
		$this->data['text_price']    	 = $this->language->get('text_price');
		$this->data['text_raw_cost']     = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc']    = $this->language->get('text_disc_perc');
		$this->data['text_disc_price']   = $this->language->get('text_disc_price');
		$this->data['text_net_price']    = $this->language->get('text_net_price');
		$this->data['text_total']        = $this->language->get('text_total');
		$this->data['text_tax']          = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class']    = $this->language->get('text_tax_class');
		$this->data['text_no_data']      = $this->language->get('text_no_data');
		$this->data['entry_vendor']      = $this->language->get('entry_vendor');
        $this->data['entry_location']    = $this->language->get('entry_location');
		$this->data['text_remove'] 		 = $this->language->get('text_remove');
		$this->data['text_select_vendor']= $this->language->get('text_select_vendor');
		$this->data['text_hold'] 		 = $this->language->get('text_hold');
		$this->data['button_hold'] 		 = $this->language->get('button_hold');
		$this->data['button_save'] 		 = $this->language->get('button_save');
		$this->data['button_cancel'] 	 = $this->language->get('button_cancel');
		$this->data['button_add'] 		 = $this->language->get('button_add');
		$this->data['button_clear'] 	 = $this->language->get('button_clear');
		$this->data['column_action'] 	 = $this->language->get('column_action');
         $apply_tax_type = 0;
         $this->clearPurchaseData();

		if (isset($this->request->get['purchase_id'])) {
			if(isset($this->request->get['type']) && $this->request->get['type']=='quote'){
				$purchaseInfo = $this->model_transaction_sales->getQuotation($this->request->get['purchase_id']);
				$purchaseInfo['customer_code'] = $purchaseInfo['vendor_id'];
				$purchaseInfo['invoice_date']  = $purchaseInfo['transaction_date'];
				$purchaseInfo['header_remarks']= $purchaseInfo['reference_date'];
			}else{
				$purchaseInfo = $this->model_transaction_sales->getSalesnew($this->request->get['purchase_id']);
			}
			// printArray($purchaseInfo); die;
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				if($purchaseInfo['handling_fee']){
					$this->session->data['handling_fee']	= $purchaseInfo['fc_handling_fee'];
				}
				if(isset($this->request->get['type']) && $this->request->get['type']=='quote'){
					$purchaseProducts = $this->model_transaction_sales->getQuotationProducts($purchaseInfo['purchase_id']);
				}else{
					$purchaseProducts = $this->model_transaction_sales->getSalesProductnew($purchaseInfo['invoice_no']);
				}
				foreach ($purchaseProducts as $products) {
					if(!isset($this->request->get['type'])){
						$products['price']      = $products['sku_price'];
						$products['net_price']  = $products['net_total'];
						$products['tax_price']  = $products['gst'];
						$products['description']= $products['cust_description'];
					}else{
						$products['qty']  = $products['quantity'];
					}
					// printArray($products); die;
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(!empty($this->session->data['cart_purchase'])){
						if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['qty'] > 0)){
							$this->cart->update($products['product_id'], $products['qty'], $cart_type, $products);
						} elseif ($products['qty'] > 0) {
							$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
						}
					} elseif ($products['qty'] > 0) {
						$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId,$productQty,'','','',$cart_type,$this->request->post);
			   }
		   }
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  $apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($vendor_id);

		}
            
		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();   //For sales Channel
		if (!empty($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
		}
		if(!empty($filter_network)){
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales List',
			'href'      => $this->url->link('transaction/sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
       
       	if(isset($this->request->get['purchase_id']) && isset($this->request->get['type'])){
			$this->data['action'] = $this->url->link('transaction/sales/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}
       	else if(isset($this->request->get['purchase_id'])){
			$this->data['action'] = $this->url->link('transaction/sales/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}else{
			$this->data['action'] = $this->url->link('transaction/sales/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}

		$this->data['cancel'] = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['invoice_no'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();
		}
		if($this->request->get['type']=='quote'){
			$this->data['transaction_no'] = $this->getTransNo();
		}

		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = $this->session->data['location_code'];
		}
        
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['invoice_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		if(isset($this->request->get['type']) && $this->request->get['type']=='quote'){
			$this->data['transaction_date'] = date('d/m/Y');
		}
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor']  = $purchaseInfo['customer_code'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor']  = $this->request->post['vendor'];
			$vendorData = $this->model_transaction_sales->getB2BCustomersbyCode($this->request->post['vendor']);
			$this->data['vendor_name']      = $vendorData['name'];
		} elseif (isset($this->request->get['newCustId'])) {
			$vendorData = $this->model_transaction_sales->getCustomerDetails($this->request->get['newCustId']);
			$this->data['vendor'] 	        = $vendorData['customercode'];
			$this->data['vendor_name'] 	    = $vendorData['name'];
			$this->data['vendorTaxAndType'] = $vendorData['tax_allow'].'||'.$vendorData['tax_type'];
			$this->session->data['vendor']  = $this->data['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $this->data['vendor'] = $purchaseInfo['customer_code'];
			$vendorData = $this->model_transaction_sales->getB2BCustomersbyCode($purchaseInfo['customer_code']);
			$this->data['vendor_name'] = $vendorData['name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}

		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['network_order_id'];
		} else {
			$this->data['reference_no'] = '';
		}
		$this->data['sales_quotation_trans_no'] = '';
		if(isset($this->request->get['type'])){
			$this->data['sales_quotation_trans_no'] = $purchaseInfo['transaction_no'];
		}

		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			if($purchaseInfo['header_remarks'] !=''){
				$this->data['reference_date'] = date("d/m/Y", strtotime($purchaseInfo['header_remarks']));
			}
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		// echo $this->data['reference_date']; die;
		
		if (isset($this->request->post['reference_to_date'])) {
			$this->data['reference_to_date'] = $this->request->post['reference_to_date'];
		} elseif (!empty($purchaseInfo)) {
			if($purchaseInfo['reference_to_date'] !=''){
				$this->data['reference_to_date'] = date("d/m/Y", strtotime($purchaseInfo['reference_to_date']));
			}else{
				$this->data['reference_to_date'] = '';
			}
		} else {
			$this->data['reference_to_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}		
		if ($this->session->data['bill_discount_mode']=='1' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_percentage'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}

		if ($this->session->data['bill_discount_mode']=='2' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_price'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		if($this->session->data['handling_fee'] > 0) {
			$this->data['handling_fee'] = $this->session->data['handling_fee'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['handling_fee'] = $purchaseInfo['handling_fee'];
		} else {
			$this->data['handling_fee'] = '';
		}

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}
		$this->data['tax_type'] = $this->data['tax_type'] =='0' ? '1': '2';
		// echo $this->data['tax_type']; die; 
		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = '';
		}

		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}
		if (isset($this->request->post['attachment'])) {
			$this->data['attachment'] = '';
		} elseif (!empty($purchaseInfo)) {
			$this->data['attachment'] = $purchaseInfo['attachment'];
		} else {
			$this->data['attachment'] = '';
		}
		if (isset($this->request->post['shipping_id'])) {
			$this->data['shipping_id'] = $this->request->post['shipping_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['shipping_id'] = $purchaseInfo['shipping_id'];
		} else {
			$this->data['shipping_id'] = '';
		}
		if (isset($this->request->post['sales_man'])) {
			$this->data['sales_man'] = $this->request->post['sales_man'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['sales_man'] = $purchaseInfo['sales_man'];
		} else {
			$this->data['sales_man'] = '';
		}
		if (isset($this->request->post['delivery_remarks'])) {
			$this->data['delivery_remarks'] = $this->request->post['delivery_remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['delivery_remarks'] = $purchaseInfo['delivery_remarks'];
		} else {
			$this->data['delivery_remarks'] = '';
		}
		if (isset($this->request->post['discount_remarks'])) {
			$this->data['discount_remarks'] = $this->request->post['discount_remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['discount_remarks'] = $purchaseInfo['discount_remarks'];
		} else {
			$this->data['discount_remarks'] = '';
		}
		if (isset($this->request->post['network_id'])) {
			$this->data['network_id'] = $this->request->post['network_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['network_id'] = $purchaseInfo['network_id'];
		} else {
			$this->data['network_id'] = '';
		}

		
		//product collection
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		if (isset($this->request->get['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->get['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		if (isset($this->request->get['type'])) {
			$this->data['purchase_type'] = $this->request->get['type'];
		} else {
			$this->data['purchase_type'] = '';
		}

		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		$this->load->model('ajax/cart');
		$this->load->model('localisation/weight_class');
		$this->load->model('localisation/tax_class');
        $apply_tax_type					 	= $this->data['tax_class_id'] =='1' ? 0 : $this->data['tax_type'];
		$this->data['Tolocations'] 			= $this->cart->getLocation();
		$this->data['salesmanlist']			= $this->model_transaction_sales->getSalesMansList();
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);

		$this->data['apply_tax_type']       = $apply_tax_type;
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		$this->data['allTerms'] 			= $this->cart->getAllTerms();
		$this->data['channels_list'] = $this->cart->getChannelsList();
		$this->data['tax_classes'] 			= $this->model_localisation_tax_class->getTaxClasses();
		$this->data['weight_classes'] 		= $this->model_localisation_weight_class->getWeightClasses();
		$this->data['convert_invoice'] 		= $this->url->link('transaction/sales_invoice/insert', 'token='.$this->session->data['token'] . '&purchase_id='.$this->request->get['purchase_id'].'&type=sales_convert'. $url, 'SSL');
		$this->data['type']      = $this->request->get['type'];//for diferenciate quote convertion
		$this->data['customer_collection'] = $this->model_transaction_sales->getB2BCustomers();
		$this->data['currencys'] = $this->model_transaction_purchase->getCurrency();
		$this->data['addCust']   = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'].'&from=sales', 'SSL');
		$this->data['token']     = $this->session->data['token'];
		$this->data['cancel']    = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['route']     = 'transaction/sales';
		$this->template 		 = 'transaction/sales_form.tpl';
		$this->children 		 = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='',$customer_id='') {
		
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one customer code.";
		}
		if (empty($this->request->post['reference_date'])) {
			// $this->error['reference_date'] = "Please enter reference date.";
		}
		if (empty($this->request->post['reference_no'])) {
			$this->error['reference_no'] = "Please enter reference no.";
		}
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}else{
			if($type !='update'){
				$purchaseInfo = $this->model_transaction_sales->getSalesByinvNonew($this->request->post['transaction_no']);
				if($purchaseInfo){
					$this->request->post['transaction_no'] = $this->getTransNo();
					$purchaseInfo = $this->model_transaction_sales->getSalesByinvNonew($this->request->post['transaction_no']);
					if($purchaseInfo){
						$this->error['warning'] = 'Please enter unique transaction number.';
					}
			 	}
			}
		}
		// printArray($this->error); exit;
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='') {
		$product_data = array();
		if($apply_tax_type=='0'){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		//echo $apply_tax_type=0;
		// printArray($this->cart->getProducts($cart_type,$apply_tax_type)); exit;
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' 	=> $product['key'],
				'code' 			=> $product['sku'],
				'name'       	=> $product['name'],
				'quantity'   	=> $product['quantity'],
				'tax_class_id'	=> $product['tax_class_id'],
				'subtract'   	=> $product['subtract'],
				'price'      	=> $this->currency->format($product['price']),
				'raw_cost'   	=> $this->currency->format($product['sku_cost']),
				'sub_total' 	=> $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      	=> $product['total'],
				'tax'        	=> $product['purchase_tax'],
				'reward'     	=> $this->currency->format($product['reward']),
				'discount'   	=> $product['purchase_discount_value'],
				'purchase_discount' => $product['purchase_discount'],
				'discount_mode' => $product['purchase_discount_mode'],
				'description'   => $product['description']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			unset($this->session->data['handling_fee']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data, $type = '') {
			$this->load->model('transaction/sales');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_sales->getProductByNamenew($this->data);
			$str ='<ul id="country-list">';
			for($i=0;$i<count($_productDetails);$i++){
				$var =  $_productDetails[$i]['product_id'].'||'.$_productDetails[$i]['sku'].'||'.$_productDetails[$i]['name'].'||'.$_productDetails[$i]['price'];
				$str.= "<li onClick=\"selectedProduct('".$var."');\">".$_productDetails[$i]['sku'].' ('.$_productDetails[$i]['name'].")</li>";
			}
			$str.='</ul>';
			echo $str;
	}

	public function export_csv()
    {
		$this->load->model('transaction/sales');
		if(!empty($this->request->post['selected_orders'])){

			ob_end_clean();
	        header('Content-Type: text/csv');
	        header('Content-Disposition: attachment;filename=sales_order.csv');
	        print "Order No,Order Date,Order Id,Customer,Delivery Date,Description,Quantity,Unit Amount,Discount,TaxType,Delivery Status,Payment Status,Sales Channel, Shipping Name,Shipping Contact, Shipping Address1, Shipping Addres2, Shipping Zip, Shipping Country \r\n";

			foreach ($this->request->post['selected_orders'] as $invoice_no) {
				$results  = $this->model_transaction_sales->getSalesOrderDetails_csv($invoice_no);

				if(count($results)){
					$j = $total = 0;

					foreach ($results as $result) {	
						$j++;
						$taxType 	 	 = '0%';
						if($result['sh_tax_class']=='2'){
							$taxType 	 = $result['tax_type'] == '0' ? '7% Excl' : '7% Incl';
						}
                        $shipping_name = $shipping_add1 = $shipping_add2 = $shipping_contact = '';
                        $network  = $this->model_transaction_sales->getNetworkName($result['network_id']);
                        if($result['shipping_id']){
                            $shipping         = $this->model_transaction_sales->getShippingAddress($result['shipping_id']);
                            $shipping_name    = $shipping['name'];
                            $shipping_add1    = $shipping['address1'];
                            $shipping_add2    = $shipping['address2'];
                            $shipping_contact = $shipping['contact_no'];
                            $shipping_zip    = $shipping['zip'];
                            $shipping_country = $shipping['country'];
                        }
						$network  = $network == 'Show Room' ? $result['location_name'] : $network;
						$reference_to_date = '';
						if($result['reference_to_date'] !='' ){
							$reference_to_date = date('d/m/Y',strtotime($result['reference_to_date']));
						}
                        if($result['header_remarks'] !='' ){
                            $header_remarks = date('d/m/Y',strtotime($result['header_remarks']));
                        }
						$cust_name        = $result['cust_name'];
						$transaction_no   = $result['invoice_no'];
						$reference   	  = $result['reference_no'];
						$network_order_id = $result['network_order_id'];
						$transaction_date = date('d/m/Y',strtotime($result['invoice_date']));
						$due_date 		  = $header_remarks.' - '.$reference_to_date;
						$description      = $result['description'];
						$quantity         = $result['qty'];
						$unit_amount      = $result['sku_price'];
						$discount  	      = $result['discount'];
						$taxType      	  = $taxType;
						$delivery         = showText($result['delivery_status']);
                        $payment          = $result['payment_status'];

						print "$transaction_no,\"$transaction_date\",\"$network_order_id\",\"$cust_name\",\"$due_date\",\"$description\",\"$quantity\",\"$unit_amount \",\"$discount\",\"$taxType\",\"$delivery\",\"$payment\",\"$network\",\"$shipping_name\",\"$shipping_contact\",\"$shipping_add1\",\"$shipping_add2\",\"$shipping_zip\",\"$shipping_country\"\r\n";
					}
				}
		}
        exit;
		$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}

	public function bulk_export_csv($orders,$url)
    {		
		foreach ($variable as $value) {
    		$results     = $this->model_transaction_sales->getSalesnew($value);
    		if(count($results)){
    	    	ob_end_clean();
    	        header( 'Content-Type: text/csv' );
    	        header( 'Content-Disposition: attachment;filename=sales_order.csv');
    	        print "S.No,Tran No,Tran Date,Location,Customer,Net total,Delivery,Payment,Network\r\n";
    			$j=0; 
    			$total=0;
    			foreach ($results as $result) {	
    				$j++;
    				$network   = $this->model_transaction_sales->getNetworkName($result['network_id']);
    				$vendorDetail = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
    				$netTotal 	  	  = number_format($result['net_total'],2);
    				$transaction_no   = $result['invoice_no'];
    				$transaction_date = date('d/m/Y',strtotime($result['invoice_date']));
    				$location 		  = $result['location_code'];
    				$delivery         = $result['delivery_status'];
    				$payment          = $result['payment_status'];
    				$vendor_name      = $vendorDetail['name'];

    				print "$j,\"$transaction_no\",\"$transaction_date\",\"$location\",\"$vendor_name\",\"$netTotal\",\"$delivery\",\"$payment \",\"$network\"\r\n";
    			}
    		}
		}
		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}

	public function getTransNo()
    { 
	    $transNumberWithSuffix = $this->model_transaction_sales->getSalesAutoIdnew();
		if($transNumberWithSuffix){
			 $transaction_no = $transNumberWithSuffix + 1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_sales_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	
	public function getSalesInvoiceTransNo()
    { 
	    $transNumberWithSuffix = $this->model_transaction_sales->getSalesInvoiceAutoId();
		if($transNumberWithSuffix){
			 $transaction_no = $transNumberWithSuffix + 1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;
			$this->data['transaction_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}

	public function view()
    {
		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle($this->language->get('Sales View'));
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		$filter_network = array();   //For sales Channel
		if (!empty($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
		}
		if(!empty($filter_network)){
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		$filter_isinvoiced = null;
		if (isset($_REQUEST['filter_isinvoiced'])) {
			$filter_isinvoiced = $_REQUEST['filter_isinvoiced'];
			$url .= '&filter_isinvoiced=' . $filter_isinvoiced;
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales List',
			'href'      => $this->url->link('transaction/sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales View',
			'href'      => $this->url->link('transaction/sales/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['back_button'] 		= $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['download_button'] 	= $this->url->link('transaction/sales/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_sales->getSalesnew($this->request->get['purchase_id']);
		// printArray($this->data['purchaseInfo']); die;
		$this->data['shippingAddress']  = $this->model_transaction_sales->getShippingAddressById($this->data['purchaseInfo']['shipping_id']);
		$this->data['vendorDetail'] 	= $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);
		$productDetail = $this->model_transaction_sales->getSalesProductnew($this->data['purchaseInfo']['invoice_no']);

		foreach ($productDetail as $value) {
			$value['childItems'] = $this->model_transaction_sales->getProductChildItems($value['product_id']);
			$this->data['productDetails'][] = $value;
		}
		$this->data['modify_button']    = $this->url->link('transaction/sales/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');
		$this->data['delete_button']    = $this->url->link('transaction/sales/delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');
		$this->data['convert_invoice']  = $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'] . '&purchase_id='.$this->request->get['purchase_id'].'&type=sales_convert'. $url, 'SSL');

		$this->template = 'transaction/sales_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function updateInvoiceNumber(){

		$this->load->model('transaction/sales');	
		if(isset($this->request->get['purchase_id'])){
	    	$invNo = $this->model_transaction_sales->updateInvoiceNumber($this->request->get['purchase_id']);
		}
		// $url='&pdf_purchase_id='.$this->request->get['purchase_id'];
		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	public function delete(){

		$this->load->model('transaction/sales');	
		if(isset($this->request->get['purchase_id'])){
			$this->model_transaction_sales->updateStatus($this->request->get['purchase_id']);
		}
		$this->session->data['success'] = 'Success: Sales Details Updated!';
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function export_pdf(){
		
		$this->clearTempFolder();
		$this->request->get['purchase_id'] = $this->request->post['orderId'];
		if(!empty($this->request->post['selected_orders'])){
			
			include(DIR_SERVER.'/MPDF/mpdf.php');
			ob_end_clean();
			$mpdf = new mPDF('c','A4');
			$mpdf->mirrorMargins = 1;
			$mpdf->SetMargins(0, 0, 5);
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->setFooter($footer);
			$mpdf->SetFont('DejaVuSans');
			$stylesheet = file_get_contents(HTTP_SERVER.'view/stylesheet/pdf.css');

			foreach ($this->request->post['selected_orders'] as $value) {
				$this->request->get['purchase_id'] = $value;
				$str = $this->download_pdf('1');
				$mpdf->WriteHTML($stylesheet,1);
				$mpdf->AddPage();
				$mpdf->WriteHTML($str);
			}
			$filename = 'download/sales_orders_list.pdf';
			$mpdf->Output($filename,'F');
			ob_end_flush();
		}
	}
	public function clearTempFolder(){   // for clear temp files in download folder
		$files = glob(DIR_SERVER.'download/*');
			foreach($files as $file){
				if(is_file($file))
				unlink($file);
		}
	}
	public function download_pdf($returnStr=''){

		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');

		$purchase_id 	 = $this->request->get['purchase_id'];
		$company_id		 = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$company_details['invoice_title'] = "Sales Order"; //Set invoice title here
		$salesInfo       = $this->model_transaction_sales->getSalesnew($purchase_id);
		$taggedOrder     = $this->model_transaction_sales->getTagSalesOrder($purchase_id);
		$vendorDetail    = $this->model_transaction_sales->getB2BCustomersbyCode($salesInfo['customer_code']);
		$shippingDtls     = $this->model_transaction_sales->getShippingAddressWithCust($salesInfo['shipping_id']);
		$company_details['location'] = $this->model_inventory_reports->getLocationDetails($salesInfo['location_code']);
		$productDetails  = $this->model_transaction_sales->getSalesProductnew($salesInfo['invoice_no']);
		$invoice_no      = $salesInfo['invoice_no'];
		$transaction_date= date('d/m/Y',strtotime($salesInfo['invoice_date']));
		$delivery_date  = date('d/m/Y',strtotime($salesInfo['header_remarks']));
		$delivery_date  = $delivery_date == '01/01/1970' ? '' : $delivery_date;
		$reference_no    = $salesInfo['network_order_id'] != '' ? $salesInfo['network_order_id'] : $salesInfo['reference_no'];
        $tax_type 		 = $salesInfo['tax_class_id']=='2' ? '7%' :'0%';

		$str  = '';
		$str  = $this->url->getCompanyAddressHeaderString2($company_details);
		$str .='<table class="table" style="width:100%;">
			<tr>
				<td class="left">
					<table class="listings"  style="width:100%;">
						<tr>
							<th align="left" style="width:20%;">Name</th>
							<td>'.$vendorDetail['name'].'</td>
						</tr>
						<tr>
							<th align="left">Address</th>
							<td>'.$vendorDetail['address1'].' '.$vendorDetail['address2'].' '.$vendorDetail['city'].'  '.$vendorDetail['zipcode'].'</td>
						</tr>
						<tr>
							<th align="left">Contact</th>
							<td>Phone : '.$vendorDetail['mobile'].' Email : '.$vendorDetail['email'].'</td>
						</tr>
					</table>
				</td>
				<td class="right">
					<table style="width:100%;">
						<tr>
							<th align="left">Invoice No</th>
							<td>'.$invoice_no.'</td>
						</tr>
						<tr>
							<th align="left">Invoice Date</th>
							<td>'.$transaction_date.'</td>
						</tr>
						<tr>
							<th align="left">Reference</th>
							<td>'.$reference_no.'</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="left">
					<table class="listings"  style="width:100%;">
						<tr>
							<th align="left" colspan="2">Shipping Details</th>
						</tr>'; 
					if($shippingDtls['name'] !=''){
					$str .='<tr>
								<th align="left" style="width:20%;">Name</th>
								<td>'.$shippingDtls['name'].'</td>
							</tr>';
					}
					$str.='<tr>
							<th align="left">Address</th>
							<td>'.$shippingDtls['address1'].' '.$shippingDtls['address2'].' '.$shippingDtls['city'].' '.$shippingDtls['country'].' '.$shippingDtls['zip'].'</td>
						</tr>
						<tr>
							<th align="left">Contact</th>
							<td>'.$shippingDtls['contact_no'].'</td>
						</tr>
					</table>
				</td>
				<td class="right">
					<table style="width:100%;">
						<tr>
							<th align="left">PO Number</th>
							<td>'.$taggedOrder['transaction_no'].'</td>
						</tr>
						<tr>
							<th align="left">Vendor Code</th>
							<td>'.$taggedOrder['vendor_name'].'</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>';
			$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
			$str.="<table class='listing' style='width:100%;'><thead>
					<tr>
						<th align='left'>SKU</th>
						<th align='left'>Description</th>
						<th align='left'>Quantity</th>
						<th align='left'>Unit Price</th>
						<th align='left'>Amount</th>
					</tr></thead><tbody>";
				$i= 1;
				foreach ($productDetails as $value) {
				$value['cust_description'] = $value['cust_description'] !='' ? '('.$value['cust_description'].')' : '';

					$str.="<tr style='border-bottom: 2px solid black;'>
							<td>".$value['sku']."</td>
		   					<td>".$value['description']."<br><p style='font-size: 12px;'>".nl2br($value['cust_description'])."</P>";
		   					$childItems = $this->model_transaction_sales->getProductChildItems($value['product_id']);
		   					if(!empty($childItems)){
		   						foreach ($childItems as $childItem) {
		   							$str .= '<span>'.$childItem['childsku'].' - '.$childItem['name'].'</span><br>';
		   						}
		   					}
					$str .="</td><td>".$value['qty']."</td>
							<td>".$value['sku_price']."</td>
							<td class='right'>".$value['sub_total']."</td>
						   </tr>";	
				$i++; }
				$str.="<tr><th colspan='3'></th><td align='right'>Subtotal</td>
						 <td class='right'>".number_format($salesInfo['sub_total'],2)."</td>
					   </tr>";
				$str.="<tr><th colspan='3'></th><td align='right'>Discount</td>
						 <td class='right'>".number_format($salesInfo['discount'],2)."</td>
					   </tr>";
				$str.="<tr><th colspan='3'></th><td align='right'>Delivery Fees</td>
						 <td class='right'>".$salesInfo['handling_fee']."</td>
					   </tr>";
				$str.="<tr>
						<th colspan='3'></th><td align='right'>GST@ ".$tax_type."</td>
						 <td class='right'>".number_format($salesInfo['gst'],2)."</td>
					   </tr>";
				$str.='<tr class="last">
						 <td align="left" colspan="3">
						 	<table class="listings"  style="width:100%;">
								<tr>
									<td align="left" style="border-right:1px solid black;padding:20px;position: relative;bottom: 20px;right: 20px;">Delivery Date: '.$delivery_date.'<br></td>
									<td align="left" style="padding:20px;bottom: 20px;right: 20px;"> Remarks: '.$salesInfo['delivery_remarks'].' <br></td>
								</tr>
							</table>
						 </td>
						 <td align="right">Total</td>
						 <td class="right">'.number_format($salesInfo['net_total'],2).'</td>
					   </tr>';

				$str .= "</tbody></table>";
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";

				$str .= "<p style='font-size:12px;text-align:justify;'>".$company_details['term1']."</p>";
				$str .= "<p style='font-size:12px;text-align:justify;'>".$company_details['term2']."</p>";
				$str .= "<p style='font-size:14px;font-weight: bold;text-align: justify;'><bold>".$company_details['term3']."</bold></p>";
		
		if($returnStr){
			return $str;
		}
		if($str){
			$filename = 'sales_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function bulk_convert_invoice(){
		$this->load->model('transaction/sales');
		
		$filters = $this->session->data['filters'];
		unset($this->session->data['filters']);
		$url = '';
		if($filters['filter_from_date'] !=''){
			$url = '&filter_from_date='.$filters['filter_from_date'];
		}
		if($filters['filter_to_date'] !=''){
			$url.= '&filter_to_date='.$filters['filter_to_date'];
		}
		if($filters['filter_location'] !=''){
			$url.= '&filter_location='.$filters['filter_location'];
		}
		if($filters['filter_customer'] !=''){
			$url.= '&filter_customer='.$filters['filter_customer'];
		}
		if($filters['filter_transaction'] !=''){
			$url.= '&filter_transaction='.$filters['filter_transaction'];
		}
		if($filters['filter_agent'] !=''){
			$url.= '&filter_agent='.$filters['filter_agent'];
		}
		if($filters['page']){
			$url.= '&page='.$filters['page'];
		}
		if(!empty($filters['filter_network'])){
			foreach($filters['filter_network'] as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		if(!empty($filters['filter_delivery_status'])){
			foreach($filters['filter_delivery_status'] as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		if(!empty($filters['filter_payment_status'])){
			foreach($filters['filter_payment_status'] as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if(!empty($this->request->post['selected_orders'])){
			foreach ($this->request->post['selected_orders'] as $invoice_no) {

				$order_header 			    = $this->model_transaction_sales->getSalesnew($invoice_no);
				if($order_header['isinvoice']=='0' && $order_header['delivery_status'] !='Canceled'){
					$data 				    = $order_header;
					$data['sales_trans_no'] = $order_header['invoice_no']; 
					$data['invoice_no']     = $this->getSalesInvoiceTransNo(); 
					$data['do_no']     		= $this->getDoNo(); 
					$data['products'] 		= $this->model_transaction_sales->getSalesProductnew($invoice_no);
					$response 				= $this->model_transaction_sales->addBulkSalesInvoice($data);
					$converted_orders 	   .= $response['order_no'].', ';
					$links 	.= "<a href='".SITE_URL."index.php?route=transaction/sales_invoice/view&token=".$this->session->data['token']."&purchase_id=".$response['invoice_no']."&from=so' target='_blank'>".$response['order_no']."</a>, ";
				}
			}
			if($converted_orders){
				$this->session->data['success'] = 'Success: Convereted orders : '.$links;
			}else{
				$this->session->data['error_bulk_convert_invoice'] = 'No order available for convert invoice';
			}
		}
		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'].$url, 'SSL')); 
	}
	public function ajaxUpdateqtyAndPrice(){

		$this->load->model('ajax/cart');
		$apply_tax_type = 0;
		$sku 	   		= $this->request->post['sku']; 
		$productQty		= $this->request->post['quantity'];
		$price     		= $this->request->post['price'];		
		$tax_class_id	= $this->request->post['tax_class_id'];

		$netPrice  		  = $productQty * $price;
		$data['netPrice'] = $netPrice;
		$data['price']    = $price;
		$data['total']    = $total;

		$netPrice  = $this->currency->format($netPrice);
		$total     = $this->currency->format($total);

		$cart_type = 'cart_purchase';
 		$productId = $this->request->post['product_id'];
 		if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 )) {
			$this->cart->update($productId, $productQty, $cart_type, $this->request->post);
		}

 		$apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
 		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->request->post['location_code']);
 		$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
 			foreach($total_data as $totals) {
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				if($tax_class_id=='1'){
					$res['tax'] 	  ='';
					$res['tax_value'] ='';
				}

				if($apply_tax_type == '2'){
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
			}
			
			switch ($this->request->post['form']) {
				case "SalRetn":
					$colspan = 7;
				break;
				case "service":
					$colspan = 9;
				break;
				default:
					$colspan = 10;
    		}
    		
			$data = array('qty'		=> $productQty, 
				'price'	   => $price, 
				'netPrice' => $netPrice, 
				'total'	   => $total, 
				'discnt'   => $discnt,
				'sku'		  => $productId,
				'orderTax'	   => $res['tax'],
				'orderSubTotal'	 => $res['sub_total'],
				'ordertax_value' => $res['tax_value'],
				'orderDiscount'	 => $res['discount'],
				'orderTotal'	 => $res['total'],
				'tax_type'		 => $apply_tax_type,
				'tax_str' 		 => '<tr id="TRtax">
	                <td align="right" class="purchase_total_left" id="tax_title" colspan="'.$colspan.'"></td>
	                <td class="purchase_total_right insertproduct">
	                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$res['tax'].'" id="tax" name="tax" style="text-align:right;">
	                </td>
	            </tr>');
		$this->response->setOutput(json_encode($data));	
	}
	public function bulk_delete(){
		$this->load->model('transaction/sales');
		$this->model_transaction_sales->deletePurchase($this->request->get['purchase_id']);
		$this->session->data['success'] = 'Success: Sales Details Deleted';
		
			$url ='';
			if (isset($_REQUEST['filter_from_date'])) {
				$filter_from_date = $_REQUEST['filter_from_date'];
				$url .= '&filter_from_date=' . $filter_from_date;
			}else{
				$filter_from_date = date('d/m/Y');
			}
			$filter_to_date = date('d/m/Y');
			if (isset($_REQUEST['filter_to_date'])) {
				$filter_to_date = $_REQUEST['filter_to_date'];
				$url .= '&filter_to_date=' . $filter_to_date;
			}
			$filter_location = $this->session->data['location_code'];
			if (isset($_REQUEST['filter_location'])) {
				$filter_location = $_REQUEST['filter_location'];
				$url .= '&filter_location=' . $filter_location;
			}
			$filter_transaction = null;
			if (isset($_REQUEST['filter_transaction'])) {
				$filter_transaction = $_REQUEST['filter_transaction'];
				$url .= '&filter_transaction=' . $filter_transaction;
			}
			$filter_customer = null;
			if (isset($_REQUEST['filter_customer'])) {
				$filter_customer = $_REQUEST['filter_customer'];
				$url .= '&filter_customer=' . $filter_customer;
			}
			$filter_network = array();
			if (!empty($_REQUEST['filter_network'])) {
				$filter_network = $_REQUEST['filter_network'];
			}
			if(!empty($filter_network)){
				foreach($filter_network as $value) {
					$url .= '&filter_network[]=' . $value;
				}
			}
			$filter_agent = '';
			if (isset($_REQUEST['filter_agent'])) {
				$filter_agent = $_REQUEST['filter_agent'];
				$url .= '&filter_agent=' . $filter_agent;
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
				$page = $this->request->get['page'];
			}

		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}
	public function getOneCartDetail(){	
 		$pro = $this->cart->getProducts('cart_purchase');
 		foreach ($pro as $value) {
	 		$data[] = array('id'=>$value['product_id'],'code'=>$value['sku']);
 		}
		$this->response->setOutput(json_encode($data));
	}
	public function checkCompanyCurrency(){
		$this->load->model('transaction/sales');
		
		$data['currency'] = $this->request->post['currency'];
		$details = $this->model_transaction_sales->checkCompanyCurrency($data);
		if(!empty($details)){
			$response = 1;
		}else{
			$response = 0;
		}
		$this->response->setOutput(json_encode($response));
	}

	public function getCustomerCurrency(){
		$this->load->model('transaction/sales');
		$currency_code	= $this->model_transaction_sales->getVendorCurrency($this->request->post['vendor']);
		$currency_code = $currency_code !='' ? $currency_code : '';
		$this->response->setOutput($currency_code);
	}

	public function cancelOrder(){ 
		$this->load->model('transaction/sales');
		$cancel = '';
		if(isset($this->request->post['invoice_no'])){
			$cancel = $this->model_transaction_sales->cancelSalesOrder($this->request->post['invoice_no'],$this->request->post['reason']);
		}
		$this->response->setOutput($cancel);
	}

	public function bulk_cancel(){
		$this->load->model('transaction/sales');

		if(!empty($this->request->post['selected_orders'])){
			foreach ($this->request->post['selected_orders'] as $invoice_no) {
				$orderHeader = $this->model_transaction_sales->getSalesByinvNonew($invoice_no);
				if($orderHeader['status'] !='Canceled' && $orderHeader['isinvoice'] =='0'){
					$this->model_transaction_sales->cancelSalesOrder($orderHeader['invoice_no'],'Bulk cancel by '.$this->session->data['username']);
				}
			}
		}
		echo "canceled"; die;
	}

	public function changeCustomerPriceAllItems(){
		$this->load->model('transaction/sales');

		foreach ($this->cart->getProducts('cart_purchase') as $cartProduct) {
			$product = $this->model_transaction_sales->getproductAlldetails($cartProduct['key'],$this->request->post['cust']);
			$this->request->post['sku'] = $product['sku'];
			$custPrice  = $this->model_transaction_sales->getcustomerPriceByProduct($this->request->post);
			$product['quantity']    = $cartProduct['quantity'];
			$product['price']       = $custPrice > 0 ? $custPrice : $product['price'];

			if($cartProduct['quantity'] > 0 ) {
				$this->cart->update($cartProduct['key'], $cartProduct['quantity'], 'cart_purchase', $product);
			}
		}

 		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}
		$cartPurchaseDetails	= $this->getPurchaseCartDetails('cart_purchase',$apply_tax_type);

		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
				
				$pid = $purchase['product_id'];
				$purchase['price'] = str_replace("$","",$purchase['price']);
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					   <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>';
                        if(!isset($this->request->post['from'])){
                            $str .='<td class="center">'.$purchase['description'].'</td>';
                        }
	                   $str .='<td class="center order-nopadding"><input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
	                   <td class="text-right order-nopadding">
	                   	<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace(",","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>';
                       $str.='<td class="text-right" id="subTot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>
					   <td class="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type == '2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format($res['sub_total_value']);
					}
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					$tax_text = 'GST';
					$tax_text = $apply_tax_type =='2' ? $tax_text."(7% Incl)" : $tax_text."(7% Excl)";
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="8">'.$tax_text.'</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
		$this->response->setOutput(json_encode($res));
        }
	}
	public function uploadAttachment($invoice_no){
	    
	    $allowed_extension = array("png", "jpg", "jpeg", "pdf", "doc", "csv", "xls", "xlsx", "docx");
	    // Get image file extension
	    $file_extension = pathinfo($_FILES["attachment"]["name"], PATHINFO_EXTENSION);
	    
	   		 // Validate file input to check if is not empty
	    if (! file_exists($_FILES["attachment"]["tmp_name"])) {
	            $response = '';
	    }    // Validate file input to check if is with valid extension
	    else if (! in_array($file_extension, $allowed_extension)) {
	            $response = '';
	    }    // Validate attachment file size
	    else if (($_FILES["attachment"]["size"] > 2000000)) {
	            $response = '';
	    } 
	    else {
			$temp = explode(".", $_FILES["attachment"]["name"]);
			$newfilename = $invoice_no . '_' . round(microtime(true)) . '.' . end($temp);
	        if (move_uploaded_file($_FILES["attachment"]["tmp_name"], DIR_SERVER.'uploads/SalesAttachment/'.$newfilename)) {
	            $response =  $newfilename;
	        } else {
	            $response = '';
	        }
	    }
		return $response;
	}
	public function removeAttachment(){
		$this->load->model('transaction/sales');
		$attachmentFileName = $this->request->post['fileName'];
		if($attachmentFileName != ''){
			$response = $this->model_transaction_sales->removeattachment($this->request->post['invoice_no']);
			$oldFile = DIR_SERVER.'uploads/SalesAttachment/'.$attachmentFileName;
			/*chmod($oldFile, 0644);*/
    		unlink($oldFile);
		} else {
	        $response = '';
		}
		echo $response;
	}
	public function copy_orders(){
		$this->load->model('transaction/sales');
		$this->load->model('ajax/cart');
		$this->load->model('inventory/inventory');
		$this->clearPurchaseData();
		
		foreach ($this->request->post['selected_orders'] as $invoice_no) {
			$customer	 	= $this->model_transaction_sales->getSalesByinvNonew($invoice_no)['customer_code'];
			$customers[] 	= $customer;
		}
		$random_order 		= $this->model_transaction_sales->getSalesnew($invoice_no);
		$customerDetails    = $this->model_transaction_sales->getCustomerDetails($customer);
		$apply_tax_type     = 0;
		$tax_class_id	    = 0;
		if(!$customerDetails['tax_allow']){
			$apply_tax_type = $customerDetails['tax_type'] == '1' ? '1' : '2';
			$tax_class_id	= '2';
		}

		$cart_type = 'cart_purchase';
		if (count(array_unique($customers)) === 1 && end($customers) === $customer) {
			foreach ($this->request->post['selected_orders'] as $invoice_no) {

				$salesDetails = $this->model_transaction_sales->getSalesProductnew($invoice_no);
				foreach ($salesDetails as $details) {
					$details['price']        = $details['sku_price'];
					$details['description']  = $details['cust_description'];
					$details['tax_class_id'] = $tax_class_id;
					if(!empty($this->session->data['cart_purchase'])){
						if(array_key_exists($details['product_id'], $this->session->data['cart_purchase']) && ($details['qty'] > 0)) {
							$details['qty'] = $details['qty'] + $this->session->data['cart_purchase'][$details['product_id']];
						   $this->cart->update($details['product_id'], $details['qty'], $cart_type, $details);
					   	} elseif ($details['qty'] > 0 || $productFocQty > 0) {
					       $this->cart->add($details['product_id'], $details['qty'], '', '', '', $cart_type, $details);
					   	}
					} elseif ($details['qty'] > 0 || $productFocQty > 0) {
				       $this->cart->add($details['product_id'], $details['qty'], '', '', '', $cart_type, $details);
				   	}
				}
			}
			$data['tax_class_id'] = $apply_tax_type;
			$data['invoice_no']	  = $this->getTransNo();
			$data['customer'] 	  = $customer;
			$data['term_id'] 	  = $customerDetails['term_id'];
			$data['network_id']	  = $random_order['network_id'];
			$data['products']  	  = $this->cart->getProducts($cart_type,$apply_tax_type);
			$data['totals']    	  = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			$m = $this->model_transaction_sales->AddCopyOrders($data);

			if($m){
				$this->session->data['success'] = 'Success: You have added sales order';
			}else{
				$this->session->data['warning'] = 'Error !!!';
			}
			$this->clearPurchaseData();
		}else{
			$this->session->data['warning'] = 'Multiple customers cannot be copy';
		}
		
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url .= '&filter_transactionno=' . $this->request->get['filter_transactionno'];
		}
		$this->redirect($this->url->link('transaction/sales', 'token='.$this->session->data['token'].$url, 'SSL'));
	}
	
	public function customerSearch(){
		$this->load->model('transaction/sales');
		$response = [];
		if($this->request->post['search'] && strlen($this->request->post['search']) >=  2 ){
			$customers = $this->model_transaction_sales->customerSearch($this->request->post['search']);
			foreach ($customers as $value) {
	            $response[] = array("value" => $value['name' ],"label" => $value['name'],"data_ref" => $value['customercode' ]);
			}
		}
		$this->response->setOutput(json_encode($response));
	}
	public function getCustomerDetails(){
		$this->load->model('transaction/sales');
		$customer = $this->model_transaction_sales->getCustomerDetails($this->request->post['cust']);
		$this->response->setOutput(json_encode($customer));
	}
	public function getDoNo(){

		$this->load->model('transaction/sales');	
	    $do_no = $this->model_transaction_sales->getSalesDoAutoId();
		if($do_no){
			 $transaction_no = $do_no+1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_do_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;
			$this->data['transaction_no'] = $this->config->get('config_sales_do_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	} 
	public function validateRefNo(){
		$this->load->model('transaction/sales');

		if($this->request->post['ref'] !=''){
	    	$count    = $this->model_transaction_sales->validateRefNo($this->request->post,'SO');
	    	if($count<= 0){
	    		$resp = true;
	    	}else{
	    		$resp = false;
	    	}
		}
		echo $resp; die;
	}
}
?>