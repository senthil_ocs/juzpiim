<?php
class ControllerTransactionStockRequest extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/stock_request');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_request');
		$this->clearPurchaseData();
		$this->getList();
	}	
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function insert() {

        $cart_type = 'cart_purchase';

		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm()) {
			/*	printArray($this->request->post);die;*/
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			
			if (isset($this->request->post['from_location_id'])) {
				$data['from_location_id'] = $this->request->post['from_location_id'];
			}  else {
				$data['from_location_id'] = '';
			}
			if (isset($this->request->post['to_location_id'])) {
				$data['to_location_id'] = $this->request->post['to_location_id'];
			}else if(isset($this->request->get['lc'])){
				$data['to_location_id'] = $this->request->get['lc'];
			}else {
				$data['to_location_id'] = '';
			}

			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			
			if(isset($this->request->post['location_fill'])) {
				$data['location_fill']	=  $this->request->post['location_fill'];
			} else {
				$data['location_fill']	= '';
			}
			$product_data = array();


			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;

			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    //echo 'here'; exit;
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			$this->load->model('transaction/stock_request');
			if( $data['hold']=='1'){
					$this->model_transaction_stock_request->addHoldPurchase($data,$costMethod);	
			}else{
			    $pid = $this->model_transaction_stock_request->addPurchase($data,$costMethod); 			   
		    }
		   

			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		/*if($this->rules->getAccessConditionByRule('product')) {
			$this->getForm();
		} else {
			$this->error['warning']	= 'Your domain have not product(s).';
			$this->redirect($this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}*/
		$this->getForm();
	}
	public function getPurchaseCartDetails($cart_type) {
		$product_data = array();
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']
			);
		}
		return $product_data;
	}
	
	protected function getList() {
		
		$this->data['heading_title'] = 'Stock Request';
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		
		$page = $_REQUEST['page'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->load->model('setting/company');
		$this->load->model('setting/location');

		$this->data['location_details'] = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->session->data['location_code']);



		

		if($show_hold=='1'){
			$this->data['heading_title'] = 'Stock Request Items List';
		}

		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Request',
			'href'      => $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);




		$pageUrl ='';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.='&filter_date_to='.$filter_date_to; 
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_from_location'])) {
			$filter_from_location = $_REQUEST['filter_from_location'];
			$pageUrl.= '&filter_from_location='.$filter_from_location;
		} else {
			$filter_from_location = null;
		}
		if (isset($_REQUEST['filter_to_location'])) {
			$filter_to_location = $_REQUEST['filter_to_location'];
			$pageUrl.= '&filter_to_location='.$filter_to_location;
		} else {
			$filter_to_location = null;
		}
		if (isset($_REQUEST['filter_accepted_status'])) {
			$filter_accepted_status = $_REQUEST['filter_accepted_status'];
			$pageUrl.= '&filter_accepted_status='.$filter_accepted_status;
		} else {
			$filter_accepted_status = 'No';
			$_REQUEST['filter_accepted_status']='No';
		}

		$strUrl = '&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to.'&filter_from_location='.$filter_from_location.'&filter_accepted_status='.$filter_accepted_status;
		$hold_button = '&show_hold=1';
		
		$this->data['insert'] = $this->url->link('transaction/stock_request/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['show_hold_button'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] .$url, 'SSL');

		$this->data['stocktransferin'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token']. $url.'&type=transferin', 'SSL');
		$this->data['stocktransferout'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token']. $url, 'SSL');

		
		 //$filter_date_to= date('Y-m-d', strtotime($filter_date_to. '+ 1 days')); 

 		$this->data['purchases'] = array();
		$data = array(
			'hold_status'		=> $show_hold,
			'location_id' 		=> $this->data['location_details']['location_id'],
			'location_code' 	=> $this->data['location_details']['location_code'],
			'list_type'   		=> $listtype,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_from_location'	=> $filter_from_location,
			'filter_to_location'	=> $filter_to_location,
			'filter_accepted_status'=> $filter_accepted_status,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		
		$purchase_total = $this->model_transaction_stock_request->getTotalPurchase($data);
		$results = $this->model_transaction_stock_request->getPurchaseList($data);
		
		$this->data['Tolocations']= $this->model_setting_location->getLocationsCode();
        if(!$page){
			$page = 1;
		}

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		foreach ($results as $result) {
			//printArray($result);
			$reference_date ='';
			if($result['reference_date']!=''){
				$reference_date = $result['reference_date'];
			}
			if($result['accepted_status']=='1'){
				$accepted_status = 'Yes';
			}else{
				$accepted_status = 'No';
			}

			if($result['revoke_status']=='1'){
				$revoke_status = 'Yes';
			}else{
				$revoke_status = 'No';
			}
			if($result['hold_status']=='1'){
				$hold_status = 'Yes';
			}else{
				$hold_status = 'No';
			}
			if($result['posted']=='1'){
				$posted_status = 'Yes';
			}else{
				$posted_status = 'No';
			}

			$from_locationAry = $this->model_transaction_stock_request->getLocationsDetailsByCode($result['transfer_from_location']);
			$result['transfer_from_location']=$from_locationAry['location_name'];
			$to_locationAry = $this->model_transaction_stock_request->getLocationsDetailsByCode($result['transfer_to_location']);
			$result['transfer_to_location'] = $to_locationAry['location_name'];


			$this->data['purchases'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'),
				'transaction_no'    => $result['transfer_no'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transfer_date'])),
				'total'             => $result['transfer_value'],
				'created_by'        => $result['createdby'],
				'reference_no'  	=> $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($reference_date)),
				'created_date'      => $result['createdon'],
				'modify_button' => $this->url->link('transaction/stock_request/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'view_button' => $this->url->link('transaction/stock_request/view','token=' . $this->session->data['token'] .'&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'posted_button' => $this->url->link('transaction/stock_request/posted','token=' . $this->session->data['token'] .'&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'post_action' => $this->url->link('transaction/stock_request/print_posted','token=' . $this->session->data['token'] .'&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				
				'from_location' => $result['transfer_from_location'],
				'to_location'   => $result['transfer_to_location'],
				'accepted_status'=>$accepted_status,
				'revoke_status'=>$revoke_status,
				'hold_status'=>$hold_status,
				'posted'     =>$posted_status
			);
		}

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] .$strUrl. $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['hold'] = $show_hold;

		$this->template = 'transaction/stock_request_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		//if (isset($this->request->get['successPid']) && $this->request->get['successPid']!='') {
		//	$this->generatePdfFile($this->request->get['successPid']);
		//}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	protected function getForm($form_from='') {
	
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/company');
		$this->load->model('setting/location');
		$this->load->model('transaction/stock_request');

		$this->data['heading_title'] = 'Insert form';
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_from_location'] = $this->language->get('entry_from_location');
		$this->data['entry_to_location'] = $this->language->get('entry_to_location');
		$this->data['entry_from_address'] = $this->language->get('entry_from_address');
		$this->data['entry_to_address'] = $this->language->get('entry_to_address');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_raw_cost'] = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		
		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_stock_request->getHoldHeaderbyId($this->request->get['purchase_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			   $purchaseProducts = $this->model_transaction_stock_request->getHoldItemdetailsByTransNo($purchaseInfo['transaction_no']);
			   
			    foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0)) {
						$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
					} elseif ($products['quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Transfer Request List',
			'href'      => $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Request Form',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!empty($purchaseInfo)) {
			$this->data['action'] = $this->url->link('transaction/stock_request/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['transfer_no'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/stock_request/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
												//echo 'here'; exit;

		$this->data['cancel'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
		$nextSRNO = $this->model_transaction_stock_request->getNextSRNofromSettings();

		

		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = str_pad($this->request->post['transaction_no'],6,"0",STR_PAD_LEFT);
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = str_pad($purchaseInfo['transaction_no'],6,"0",STR_PAD_LEFT);
		} else {			
			$this->data['transaction_no'] = str_pad($nextSRNO,6,"0",STR_PAD_LEFT);
		}

		
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		
		$this->data['transaction_date'] = date('d/m/Y');
		$this->data['location_fill'] = 0;

		if (isset($this->request->post['from_location_id'])) {
			$this->data['from_location_id'] = $this->request->post['from_location_id'];
		} elseif (!empty($purchaseInfo)) {
			//$this->data['from_location_id'] = $purchaseInfo['from_outlet_id'];
			$this->data['from_location_id'] = $purchaseInfo['transfer_from_location'];
		} else {
			$this->data['from_location_id'] = '';
		}

		if (isset($this->request->post['to_location_id'])) {
			$this->data['to_location_id'] = $this->request->post['to_location_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['to_location_id'] = $purchaseInfo['transfer_to_location'];
		}else if(isset($this->request->get['lc'])){
		    $this->data['to_location_id'] = $this->request->get['lc'];
		    $this->data['location_fill'] = 1;			
		} else {
			$this->data['to_location_id'] = '';
		}
		

		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}

		
		
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}

		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date('d/m/Y');
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}

		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		


		//product collection
		$this->data['products'] = array();
		
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		//if ($this->session->data['vendor']) {
		    $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		//}
		
		


		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		$this->data['filter_ref_details'] = array();
		
		
		//printArray($this->data);
		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax_purchase');
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();

		
		$this->data['Tolocations'] = $this->model_setting_location->getLocationsCode();

		
		$this->data['location_details'] = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->session->data['location_code']);

		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['route'] = 'transaction/stock_request';
		
		if($form_from=='posted'){
			$this->data['action'] = $this->url->link('transaction/stock_request/posted', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['transfer_no'] . $url, 'SSL');
			$this->template = 'transaction/stock_requestpost_form.tpl';

		}else{
			$this->template = 'transaction/stock_request_form.tpl';
		}

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}		
		if (empty($this->request->post['to_location_id'])) {
			$this->error['warning'] = "Please Select request location.";
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/stock_request');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['filter_location_code'] = $this->session->data['location_code'];
			$_productDetails           = $this->model_transaction_stock_request->getProductByName($this->data);
			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost'] == '0.000'){
						$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
                    
					$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost']=='0.000'){
						$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
                    $name_value = replaceSpecials($_productDetails[$i]['name']);
					$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.trim($name_value).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>'; 
			}
			echo $str;
	}
	public function view(){
		$this->document->setTitle('Stock Request Transaction Details');
		$this->language->load('transaction/stock_transfer');
		$this->load->model('transaction/stock_request');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['token'] 			= $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if (isset($this->request->get['purchase_id'])) {
			$purchase_id = $this->request->get['purchase_id'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Request List',
			'href'      => $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Request Details',
			'href'      => $this->url->link('transaction/stock_request/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 	  = $this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo']   = $this->model_transaction_stock_request->getPurchaseDetailsById($purchase_id);
		$this->data['productDetails'] = $this->model_transaction_stock_request->getProductDetailsById($purchase_id);
		
		if($this->data['purchaseInfo']){
			$this->data['purchaseInfo']['transfer_date'] = $this->data['purchaseInfo']['transfer_date'];
			$from_locationAry = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->data['purchaseInfo']['transfer_from_location']);
			$this->data['purchaseInfo']['transfer_from_location']=$from_locationAry['location_name'];
			$to_locationAry   = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->data['purchaseInfo']['transfer_to_location']);
			$this->data['purchaseInfo']['transfer_to_location'] = $to_locationAry['location_name'];
			$this->data['purchaseInfo']['posted_status'] = 'No';
			if($this->data['purchaseInfo']['posted']==1){
			$this->data['purchaseInfo']['posted_status'] = 'Yes';
			}
		}

		$this->data['purchase_bill']=$this->generatepdf($purchase_id);
		$this->template = 'transaction/stoct_request_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function update() {

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle('Stock Request Modify');
		$this->load->model('transaction/stock_request');

		if (($this->request->server['REQUEST_METHOD'] =='POST') && $this->validateForm($type='update')) {
			
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if (isset($this->request->post['from_location_id'])) {
				$data['from_location_id'] = $this->request->post['from_location_id'];
			}  else {
				$data['from_location_id'] = '';
			}
			if (isset($this->request->post['to_location_id'])) {
				$data['to_location_id'] = $this->request->post['to_location_id'];
			} else {
				$data['to_location_id'] = '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			
			$this->model_transaction_stock_request->editPurchase($data,$costMethod);
			
			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			exit;
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}

	public function posted() {

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle('Stock Request Modify');
		$this->load->model('transaction/stock_request');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {

			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}

			if(isset($this->request->post['to_location_code'])) {
				$data['to_location_code']	=  $this->request->post['to_location_code'];
			} else {
				$data['to_location_code']	= '';
			}

			if(isset($this->request->post['from_location_code'])) {
				$data['from_location_code']	=  $this->request->post['from_location_code'];
			} else {
				$data['from_location_code']	= '';
			}

			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['sku'])) {
				$data['sku']	=  $this->request->post['sku'];
			} else {
				$data['sku']	= '';
			}
			if(isset($this->request->post['received_qty'])) {
				$data['received_qty']	=  $this->request->post['received_qty'];
			} else {
				$data['received_qty']	= '';
			}
		
			$this->model_transaction_stock_request->updatePosted($data);
			
			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->redirect($this->url->link('transaction/stock_request', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			exit;
		}
		$this->data['posted_form']	= true;
		$this->getForm('posted');
	}
	public function generatepdf($purchaseId){
	
		$this->load->model('setting/company');
		$this->load->model('transaction/stock_request');
		$this->data['company_details'] = $this->model_setting_company->getCompanyDetails($this->session->data['company_id']);
		
		$companyInfo	= $this->getCompanyAddress($this->data['company_details']);

		if (isset($this->request->get['purchase_id'])) {
			$purchase_id = $this->request->get['purchase_id'];
		}


		$this->data['purchaseInfo'] 	= $this->model_transaction_stock_request->getPurchaseDetailsById($purchase_id);
		$this->data['productDetails'] 	= $this->model_transaction_stock_request->getProductDetailsById($purchase_id);
		
		if($this->data['purchaseInfo']){
			$this->data['purchaseInfo']['transfer_date'] = $this->data['purchaseInfo']['transfer_date'];
			$from_locationAry = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->data['purchaseInfo']['transfer_from_location']);
			$this->data['purchaseInfo']['transfer_from_location']=$from_locationAry['location_name'];
			$to_locationAry = $this->model_transaction_stock_request->getLocationsDetailsByCode($this->data['purchaseInfo']['transfer_to_location']);
			$this->data['purchaseInfo']['transfer_to_location'] = $to_locationAry['location_name'];
			$this->data['purchaseInfo']['posted_status'] = 'No';
			if($this->data['purchaseInfo']['posted']==1){
			$this->data['purchaseInfo']['posted_status'] = 'Yes';
			}
		}

		$html = '<html>
            	 <body>
                   <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:14px;">
                		<tr>
                            <td colspan="4">
                                <h4><strong><center>'.$companyInfo['name'].'</center></strong></h4>
                                <p>'.$companyInfo['address1'].', '.$companyInfo['address2'].','.$companyInfo['city'].'-('.$companyInfo['postal_code'].')</p>
                                <p>Ph:'.$companyInfo['phone'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax:'.$companyInfo['fax'].'</p>
                                <p>Email:'.$companyInfo['email'].'</p><p></p>
                                <p>Web:'.$companyInfo['web_url'].'</p><p></p>
                                <p>GST REG NO:'.$companyInfo['business_reg_no'].'</p>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>Transaction No :</td>
                        	<td>'.$purchase_id.'</td>
                        	<td>Date :</td>
                        	<td>'.date('d/m/Y').'</td>
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>From Location :</td>
                        	<td>'.$this->data['purchaseInfo']['transfer_from_location'].'</td> 
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>To Location :</td>
                        	<td>'.$this->data['purchaseInfo']['transfer_to_location'].'</td>                        	
                        </tr>
                        <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>User :</td>
                        	<td>'.$this->data['purchaseInfo']['createdby'].'</td>                        	
                        </tr>
                        <tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                        <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>S.No</td>
                        	<td>SKU</td>
                        	<td>Item Description</td>
                        	<td>Qty</td>                        	
                        </tr>';

                    for($i=0;$i<count($this->data['productDetails']);$i++){

                    $html.='<tr><td colspan="4" style="margin-top:10px;float:left;"> </td></tr>
                    		<tr>
	                    		<td>'.($i+1).'</td>
	                        	<td>'.$this->data['productDetails'][$i]['sku'].'</td>
	                        	<td>'.$this->data['productDetails'][$i]['name'].'</td> 
	                        	<td>'.$this->data['productDetails'][$i]['quantity'].'</td>                        	
                        	</tr>';  	
                      }
                   $html.='<tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                        <tr><td colspan="4" style="margin-top:30px;float:left;"></td></tr>
                        <tr>
                        	<td colspan="2" align="left">Signature</td>
                        	<td colspan="2" align="right">'.date('d/m/Y h:i:s').'</td>
                        </tr>
              	   </table>
            	 </body>
        </html>';

        return $html;

    }

    public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']);
			$company_address['state']	 = strtoupper($company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
			$company_address['postal_code'] = $company_details['postal_code'];
			$company_address['email'] = $company_details['email'];
			$company_address['web_url'] = $company_details['web_url'];
			$company_address['business_reg_no'] = $company_details['business_reg_no'];
			
		}
		return $company_address;
	}

}
?>