<?php
class ControllerTransactionCommonStock extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/common'); 
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->data['location_code'] = $this->session->data['location_code'];
		
		
		$this->data['heading_title'] = $this->language->get('stock_heading_title');
		$this->data['text_stock'] = 'Stock';		

		if(STOCK_TRANSFER=='1'){
			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_transfer_out", 'token=' . $this->session->data['token'], 'SSL'),
												   "id"     => '',
					                               "Text" => 'Stock Transfer out',
					                               "icon" =>'fa fa-sign-out'
					                               );

			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_transfer_in", 'token=' . $this->session->data['token'], 'SSL'),
												   "id"     => '',
					                               "Text" => 'Stock Transfer In',
					                               "icon" =>'fa fa-sign-in'
					                               );
		}
		
		if($this->user->hasPermission('access', 'transaction/stock_request') || $this->user->hasPermission('modify', 'transaction/stock_request')){
			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_request", 'token=' . $this->session->data['token'], 'SSL'),
													   "id"     => '',
						                               "Text" => 'Stock Request',
						                               "icon" =>'fa fa-list'
						                               );
		}

		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_adjustment/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stockadjust_id'),
				                               "Text" => $this->language->get('text_stock_adjust'),
				                               "icon" =>'fa fa-random'
				                               );

		/*$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stocktake_id'),
				                               "Text" => $this->language->get('text_stock_take'),
				                               "icon" =>'fa fa-list-alt'
				                               );
		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/updatelist", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stockupdate_id'),
				                               "Text" => $this->language->get('text_stock_updation'),
				                               "icon" =>'fa fa-undo'
				                               );
		*/
		$this->data['token'] = $this->session->data['token'];	
		$this->template = 'transaction/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

 }
?>