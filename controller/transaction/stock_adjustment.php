<?php
class ControllerTransactionStockAdjustment extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/stock_adjustment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_adjustment');
		$this->clearPurchaseData();
		$this->getList();
	}
	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_adjustment');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			    //$this->clearPurchaseData($remove = 'discount');
			}
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			$this->response->setOutput(json_encode($res));
		}
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_adjustment');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					if($this->request->post['discount']=='')
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="9" align="right" class="purchase_total_left">'.$lable.'</td><td class="purchase_total_right">
							          	<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="input-text row_total_box"
							          	readonly="readonly" /></td></tr>';
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
			}
			$this->response->setOutput(json_encode($res));
	}
	public function ajaxaddproducts(){
		$this->load->model('transaction/stock_adjustment');
		$settings = $this->model_transaction_stock_adjustment->getGeneralSettings();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   /*if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {*/
		   if (isset($this->request->post['product_id'])) {
		   	   $productId	 = $this->request->post['product_id'];
			    if($this->request->post['quantity'] > 0){
			   		$productQty	 = $this->request->post['quantity'];
				} else {
					$productQty	 = 1;
					$this->request->post['neg_quantity'] = $this->request->post['quantity'];
				}
			   $productPrice = $this->request->post['price'];
			   //printArray($this->request->post); exit;
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(!empty($this->session->data['cart_purchase'])){
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		$cart_type = 'cart_purchase';
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type  = $this->config->get('config_apply_tax_purchase');
		}

		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		// printArray($cartPurchaseDetails); die;
		//$apply_tax_type       	= $this->config->get('config_apply_tax');
		//$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					// $lotDetailsStr = $this->model_transaction_stock_adjustment->getlotNames($purchase['lot_no']);
					$pid = $purchase['product_id'];
				   
				   /* if($apply_tax_type=='2') {
					    $totalValue = round($purchase['total'],2);
					} else {
					    $totalValue = round($purchase['tax'] + $purchase['total'],2);
					}*/
					$totalValue = number_format($purchase['total'],3);
					
					if($purchase['neg_quantity']){
						$quantity = $purchase['neg_quantity'];
					} else {
						$quantity = $purchase['quantity'];
					}
					
					$purchase['expiry_date'] = str_replace('/', '-', $purchase['expiry_date']);
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					   <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center">'.$quantity.'</td>
	                   <td class="center">'.$purchase['sku_qty'].'</td>
	                   <td class="center">'.$purchase['price'].'</td>
		               ';	              

	             
				$str.='<td class="center">'.$purchase['sub_total'].'</td>		
					   <td class="center">'.$this->currency->format($totalValue).'</td>';
				
				/*<td class="center">'.$purchase['raw_cost'].'</td>
				<td class="text-right">'.$this->currency->format($purchase['tax']).'</td>
				$str.='<td class="center">'.$purchase['reasons'].'</td>';
				$str.='<td class="center">'.$purchase['remark'].'</td>';*/
				$str.='<td class="center"> [ <a onclick="removeProductData('.$pid.');"> Remove </a> ] </td>
                                                </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection();
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$cnt = count($data['totals'])-1;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						/*$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="11">GST (7%) (Excl)</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax">
	                                                </td>
                                                </tr>';*/
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$res['available_stock'] = '';
				if($this->request->post['transtype']=='sale'){
					$this->load->model('transaction/stock_adjustment');
					$stock = $this->model_transaction_stock_adjustment->checkstock($this->request->post['product_id'],$this->request->post['location']);
					$res['available_stock'] = $stock;
				 }

				$this->response->setOutput(json_encode($res));
         }
	}
	public function insert() {
		
		// printArray($this->request->post); exit;
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_adjustment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_adjustment');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm()) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if (isset($this->request->post['transaction_no'])) {
				$data['transaction_no'] = $this->request->post['transaction_no'];
			} else {
				$data['transaction_no'] = $this->getTransNo();
			}	
			if (isset($this->request->post['transaction_date'])) {
				$data['transaction_date'] = $this->request->post['transaction_date'];
			} else {
				$data['transaction_date'] = date('d/m/Y');
			}

			if(isset($this->request->post['location'])) {
				$data['location']	=  $this->request->post['location'];
			} else {
				$data['location']	= '';
			}
			if(isset($this->request->post['terminal'])) {
				$data['terminal']	=  $this->request->post['terminal'];
			} else {
				$data['terminal']	= '';
			}
			if(isset($this->request->post['add_or_deduct'])) {
				$data['add_or_deduct']	=  $this->request->post['add_or_deduct'];
			} else {
				$data['add_or_deduct']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'sku'						=> $product['sku'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'location'                  => $product['location'],
					'reasons'                   => $product['reasons'],
					'remark'                    => $product['remark'],
					'neg_quantity'              => $product['neg_quantity'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			// printArray($data); die;
			if($data['hold']=='1'){
				$this->model_transaction_stock_adjustment->addHoldPurchase($data,$costMethod);
			}else{
			$this->model_transaction_stock_adjustment->addPurchase($data,$costMethod);
			}
			$this->session->data['success'] = 'You have added Stock Adjustment';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$this->redirect($this->url->link('transaction/stock_adjustment&show_hole'.$data['hold'], 'token=' . $this->session->data['token'] . $url, 'SSL'));
		$this->redirect($this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		
		$this->getForm();
	}
	public function update() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_adjustment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_adjustment');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold')) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if (isset($this->request->post['transaction_no'])) {
				$data['transaction_no'] = $this->request->post['transaction_no'];
			} else {
				$data['transaction_no'] = $this->getTransNo();
			}	
			if (isset($this->request->post['transaction_date'])) {
				$data['transaction_date'] = $this->request->post['transaction_date'];
			} else {
				$data['transaction_date'] = date('d/m/Y');
			}

			if(isset($this->request->post['location'])) {
				$data['location']	=  $this->request->post['location'];
			} else {
				$data['location']	= '';
			}
			if(isset($this->request->post['terminal'])) {
				$data['terminal']	=  $this->request->post['terminal'];
			} else {
				$data['terminal']	= '';
			}
			if(isset($this->request->post['add_or_deduct'])) {
				$data['add_or_deduct']	=  $this->request->post['add_or_deduct'];
			} else {
				$data['add_or_deduct']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'sku'						=> $product['sku'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'location'                  => $product['location'],
					'reasons'                   => $product['reasons'],
					'remark'                    => $product['remark'],
					'neg_quantity'              => $product['neg_quantity'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			// printArray($data['products']); die;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			$this->model_transaction_stock_adjustment->editPurchase($data['transaction_no'], $data);
			$this->session->data['success'] = 'You have update Stock Adjustment';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];


		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
         
         //printArray($_REQUEST); exit;
         if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];			
		} else {
			$filter_date_from = date('d/m/Y');
		}
		$url .= '&filter_date_from='.$filter_date_from;

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to =  date('d/m/Y');
		}
		$url .= '&filter_date_to='.$filter_date_to;
		
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		$url .='&filter_location_code='.$filter_location_code; 

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Adjustment',
			'href'      => $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$hold_button = '&show_hold=1';
		$this->data['insert'] = $this->url->link('transaction/stock_adjustment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['show_hold_button'] = $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'filter_location_code'	   => $filter_location_code
		);
		$this->data['filter'] = $data;
		$purchase_total = $this->model_transaction_stock_adjustment->getTotalPurchase($data);
		// echo $purchase_total;
		$results = $this->model_transaction_stock_adjustment->getPurchaseList($data);
		 // printArray($results); die;
		$this->load->model('master/vendor');
		$this->load->model('user/user');

		$vendorDetail = array();
		$userDetail = array();
		foreach ($results as $result) {
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			// $userDetail = $this->model_user_user->getUser($result['created_by']);
			
			$purchaseDetail = $this->model_transaction_stock_adjustment->getPurchaseDetails($result['stkAdjustment_id']);
			//printArray($purchaseDetail); 
			//$result['transaction_date'] = str_replace('-', '/', $result['transaction_date']);
			$this->data['purchases'][] = array(
				'pagecnt'           => ($page-1) * $this->config->get('config_admin_limit'),
				'stkAdjustment_id'  => $result['stkAdjustment_id'],
				'location_code'     => $result['location_code'],
				'stkAdjustment_date'=> date('d/m/Y',strtotime($result['stkAdjustment_date'])),
				'terminal_code'  	=> $result['terminal_code'],
				'add_or_deduct'     => $result['add_or_deduct'],
				'gst'      			=> $result['gst'],
				'total_value'       => $result['total_value'],
				'created_by'        => $result['created_by'],
				'created_date'      => date('d/m/Y',strtotime($result['created_date'])),
				'modify_button' 	=> $this->url->link('transaction/stock_adjustment/update', 'token=' . $this->session->data['token'] . '&stkAdjustment_id=' . $result['stkAdjustment_id'] . $url, 'SSL'),
				'view_button' => $this->url->link('transaction/stock_adjustment/view', 'token=' . $this->session->data['token'] . '&stkAdjustment_id=' . $result['stkAdjustment_id'] . $url, 'SSL')
			);
		}
		//printArray($this->data['purchases']);
		/*$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}*/
		$this->data['page'] = $page;
		$pagination 		= new Pagination();
		$pagination->total 	= $purchase_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] 	= $sort;
		$this->data['order'] 	= $order;
		$this->data['route'] 	= $this->request->get['route'];
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		// printArray($this->data['Tolocations']); die;

		$this->template = 'transaction/stock_adjustment_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('transaction/stock_take');
		
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['entry_tran_id'] 	= $this->language->get('entry_tran_id');
		$this->data['entry_tran_dt'] 	= $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] 	= $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code']= $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name']= $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] 		= $this->language->get('entry_refno');
		$this->data['entry_refdate'] 	= $this->language->get('entry_refdate');
		$this->data['entry_location'] 	= $this->language->get('entry_location');
		$this->data['entry_terminal'] 	= $this->language->get('entry_terminal');
		$this->data['text_inventory_code'] 	= $this->language->get('text_inventory_code');
		$this->data['text_product_name']= $this->language->get('text_product_name');
		$this->data['text_qty'] 		= $this->language->get('text_qty');
		$this->data['text_price'] 		= $this->language->get('text_price');
		$this->data['text_raw_cost'] 	= $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] 	= $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] 	= $this->language->get('text_disc_price');
		$this->data['text_net_price'] 	= $this->language->get('text_net_price');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['text_tax'] 		= $this->language->get('text_tax');
		$this->data['text_weight_class']= $this->language->get('text_weight_class');
		$this->data['text_tax_class'] 	= $this->language->get('text_tax_class');
		$this->data['text_no_data'] 	= $this->language->get('text_no_data');
		$this->data['entry_vendor'] 	= $this->language->get('entry_vendor');
		$this->data['text_remove'] 		= $this->language->get('text_remove');
		$this->data['text_select_vendor']= $this->language->get('text_select_vendor');
		$this->data['text_hold'] 		= $this->language->get('text_hold');
		$this->data['button_hold'] 		= $this->language->get('button_hold');
		$this->data['button_save'] 		= $this->language->get('button_save');
		$this->data['button_cancel'] 	= $this->language->get('button_cancel');
		$this->data['button_add'] 		= $this->language->get('button_add');
		$this->data['button_clear'] 	= $this->language->get('button_clear');
		$this->data['column_action'] 	= $this->language->get('column_action');

		if (isset($this->request->get['stkAdjustment_id'])) {
			$purchaseInfo = $this->model_transaction_stock_adjustment->getPurchase($this->request->get['stkAdjustment_id']);
			
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				$purchaseProducts = $this->model_transaction_stock_adjustment->getPurchaseProduct($purchaseInfo['stkAdjustment_id']);
				foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['qty'] > 0)) {
						$this->cart->update($products['product_id'], $products['qty'], $cart_type, $products);
					} elseif ($products['qty'] > 0) {
						$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		 
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_id'])) {
			$this->data['error_transaction_id'] = $this->error['transaction_id'];
		} else {
			$this->data['error_transaction_id'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['location'])) {
			$this->data['error_location'] = $this->error['location'];
		} else {
			$this->data['error_location'] = '';
		}
		if (isset($this->error['terminal'])) {
			$this->data['error_terminal'] = $this->error['terminal'];
		} else {
			$this->data['error_terminal'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Adjustment',
			'href'      => $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Add Stock Adjustment',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!empty($purchaseInfo) || (!empty($this->request->post['purchase_id']))) {
			$this->data['action'] = $this->url->link('transaction/stock_adjustment/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['purchase_id'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/stock_adjustment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
		
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['stkAdjustment_id'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();				
		}
		//printArray($_POST);
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['stkAdjustment_date'];
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		
		if (isset($this->request->post['location'])) {
			$this->data['location'] = $this->session->data['location_code'];
		} elseif (!empty($company_info)) {
			$this->data['location'] = $this->session->data['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location'] = '';
		}
		$this->data['location'] = $this->session->data['location_code'];
		if (isset($this->request->post['terminal'])) {
			$this->data['terminal'] = $this->request->post['terminal'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['terminal'] = $purchaseInfo['terminal_code'];
		} else {
			$this->data['terminal'] = '';
		}
		if (isset($this->request->post['add_or_deduct'])) {
			$this->data['add_or_deduct'] = $this->request->post['add_or_deduct'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['add_or_deduct'] = $purchaseInfo['add_or_deduct'];
		} else {
			$this->data['add_or_deduct'] = '';
		}
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $this->session->data['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->data['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}

		if (isset($this->request->post['stock_type'])) {
			$this->data['stock_type'] = $this->request->post['stock_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['stock_type'] = $purchaseInfo['stock_type'];
		} else {
			$this->data['stock_type'] = '';
		}
		if (isset($this->request->post['stock_name'])) {
			$this->data['stock_name'] = $this->request->post['stock_name'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['stock_name'] = $purchaseInfo['stock_name'];
		} else {
			$this->data['stock_name'] = '';
		}

		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date('d/m/Y',strtotime($purchaseInfo['reference_date']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['total'])) {
			$this->data['total'] = $this->request->post['total'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['total'] = $purchaseInfo['total_value'];
		} else {
			$this->data['total'] = '';
		}
		if (isset($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		if(isset($this->data['location_code']) && $this->data['location_code'] !=''){
			$this->data['location'] = $this->data['location_code'];
		}
		//product collection
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		//if ($this->session->data['vendor']) {
		// $this->data['productss'] = $this->model_inventory_inventory->getProducts($data);
		//}
		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		// printArray($this->data['cartPurchaseDetails']); die;

		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax_purchase');
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();
		//printArray($this->data['purchase_totals']);
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		// printArray($this->data); die;

		$this->data['settings'] = $this->model_transaction_stock_adjustment->getGeneralSettings();
		
		$this->data['customerDetails'] 	=''; //$this->model_transaction_stock_adjustment->getCustomerDetails();

		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['route'] = 'transaction/stock_adjustment';
		$this->template = 'transaction/stock_adjust_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	/*public function getTransNo(){
		$transNumberWithSuffix = $this->model_transaction_stock_adjustment->getPurchaseAutoId();
		$transNumber		   = substr($transNumberWithSuffix, -8);
		if($transNumber){
			 $transaction_no = ltrim($transNumber)+1;
			 $this->data['transaction_no'] = LOCATION_CODE.PURCHASE_CODE.str_pad($transaction_no, 8, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = LOCATION_CODE.PURCHASE_CODE.str_pad($transaction_no, 8, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}*/
	public function getTransNo(){
		$this->load->model('transaction/stock_adjustment');
		$transNumber = $this->model_transaction_stock_adjustment->getStockAdjustAutoId();
	
		if($transNumber){
			$this->data['transaction_no'] = str_pad($transNumber, 4, '0', STR_PAD_LEFT);
		}else{
			$transNumber = 1;
			$this->data['transaction_no'] = str_pad($transNumber, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}
		if (empty($this->request->post['location'])) {
			$this->error['transaction_date'] = "Please enter location";
		}
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='') {
		$product_data = array();
		if($apply_tax_type==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		$pro = $this->cart->getProducts($cart_type,$apply_tax_type);
		foreach ($pro as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		 => $product['sku'],
				'lot_no'     => $product['lot_no'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'sku_qty'    => $product['sku_qty'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total'  => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;

		$stock =0;
		if($productDetails['transtype']=='sale'){
			$this->load->model('transaction/stock_adjustment');
			$stock = $this->model_transaction_stock_adjustment->checkstock($productDetails['product_id'],$productDetails['location']);
		}

		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'location'                  => $productDetails['location'], 
				'lot_no'                    => $productDetails['lot_no'],
				'expiry_date'               => $productDetails['expiry_date'], 
				'reasons'               => $productDetails['reasons'], 
				'remark'               => $productDetails['remark'], 
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total,
				'available_stock'           => $stock 
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function demoscript(){
		echo "here";
		$this->load->model('transaction/stock_adjustment');
		$this->data['filter_name'] = '56U';
		$this->data['filter_cat']  = $this->request->post['cat'];
		$_productDetails        = $this->model_transaction_stock_adjustment->getProductByName($this->data);
		printArray($_productDetails); 
	}
	public function getProductDetails($data='', $type = '') {
			$this->load->model('transaction/purchase');
			$this->data['filter_name']  = $this->request->post['sku'];
			$this->data['location_code']= $this->request->post['location_code'];
			$_productDetails            = $this->model_transaction_purchase->getProductByNameNew($this->data);
			$str = '';
			if(count($_productDetails)>=1){
				$str.='<ul id="country-list">';
				for($i=0; $i<count($_productDetails); $i++){
                    
                    $var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['unit_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}
	public function getProductDetailsByPower($data, $type = '') {
			$this->load->model('transaction/stock_adjustment');
			$this->data['filter_cat']    = $this->request->post['cat'];
			$this->data['filter_power']  = str_replace("p", "+", $this->request->post['power']); 
			$_productDetails             = $this->model_transaction_stock_adjustment->getProductByName($this->data);
			if(count($_productDetails)>=1){
				$str ='<ul id="purchase-form-autofill-invCode">';
				for($i=0;$i<count($_productDetails);$i++){
					$className = 'list';
					$_productDetails[$i]['modelName'] = $this->model_transaction_stock_adjustment->getcatName($_productDetails[$i]['category_id']);
					if($_productDetails[$i]['average_cost']=='0.0000'){
						$_productDetails[$i]['average_cost'] =' ';
					}
					$var =  $_productDetails[$i]['product_id'].'||'.$_productDetails[$i]['sku'].'||'.$_productDetails[$i]['name'].'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.$_productDetails[$i]['power'].'||'.$_productDetails[$i]['modelName'];
					$str.= "<li onClick=\"selectedProduct('".$var."');\" class=".$className.">".$_productDetails[$i]['power'].' ('.$_productDetails[$i]['name'].")</li>";
				}
				$str.='</ul>';
				echo $str;
			}else{
				$str ='<div id="purchase-form-autofill-invCode"><p>Not Found</p></div>';
				echo $str;
			}

	}

	public function getCategoriesDetails($data, $type = '') {
			$this->load->model('transaction/stock_adjustment');
			$_productDetails           = $this->model_transaction_stock_adjustment->getcategoriesDetailsforautoFill($this->request->post['cat']);
			if(count($_productDetails)>=1){
				$str ='<ul id="purchase-form-autofill-invCode">';
				for($i=0;$i<count($_productDetails);$i++){
					$var =  $_productDetails[$i]['category_id'].'||'.$_productDetails[$i]['category_name']."(".$_productDetails[$i]['department_name'].")";
					$str.= "<li onClick=\"fillCatId('".$var."');\">".$_productDetails[$i]['category_name']." (".$_productDetails[$i]['department_name'].")</li>";
				}
				$str.='</ul>';
				echo $str;
			}
	}

	public function getProductSkuDetails()
	{
		$this->load->model('transaction/stock_adjustment');
		$id = $this->request->post['id'];
		$SkuProductDetails = $this->model_transaction_stock_adjustment->getproductdetails($id);
		if($SkuProductDetails['average_cost']=='0.0000'){
					$SkuProductDetails['average_cost'] =' ';
		}
		echo json_encode($SkuProductDetails);
	}
	public function getLotNoByInventoryCode(){
		$sku = $this->request->get['sku'];
		$this->load->model('transaction/stock_adjustment');
		$SkuProductDetails = $this->model_transaction_stock_adjustment->getLotNumberByInventoryCode($sku);
		if(count($SkuProductDetails) > 0){
			$strContent = '<select name="lot_no[]" id="lot_no" class="textbox small_price_box" multiple="multiple">
							<option value="0" selected="selected"> Select Lot No </option>
							';
                               foreach($SkuProductDetails as $skuDetails){
                               		$strContent.='<option value="'.$skuDetails['id'].'|'.$skuDetails['QtyOnHand'].'">'.$skuDetails['LotNo'].' - '.date('d/m/Y',strtotime($skuDetails['ExpiryDate'])).' ('.$skuDetails['QtyOnHand'].')</option>';
                               }
                           $strContent.='</select>';
            $strValue = 0;
		} else {
			$strContent = '<select name="lot_no" id="lot_no" class="textbox small_price_box" multiple="multiple">
							<option value="0" selected="selected"> Select Lot No </option>
							</select>';
			$strValue = 1;
		}
		echo $strContent.'||'.$strValue;
	}
	public function view(){
		$this->language->load('transaction/stock_adjustment');
		$this->load->model('transaction/stock_adjustment');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('Stock Adjustment');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
        if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];			
			$url .= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
			$url .= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$url .='&filter_location_code='.$filter_location_code;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock',
			'href'      => $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Adjustment List',
			'href'      => $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Adjustment View',
			'href'      => $this->url->link('transaction/stock_adjustment/view', 'token=' . $this->session->data['token'].'&stkAdjustment_id='.$this->request->get['stkAdjustment_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/stock_adjustment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_stock_adjustment->getPurchaseDetailsById($this->request->get['stkAdjustment_id']);
		//$this->data['vendorDetail'] 	= $this->model_master_vendor->getVendor($this->data['purchaseInfo']['vendor_id']);
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['created_by']);
		$this->data['purchaseDetail'] 	= $this->model_transaction_stock_adjustment->getPurchaseDetails($this->request->get['stkAdjustment_id']);

		$this->data['productDetails'] 	= $this->model_transaction_stock_adjustment->getProductDetailsById($this->request->get['stkAdjustment_id']);
		//printArray($this->data['purchaseDetail']);
		$this->template = 'transaction/stock_adjustment_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getCustomer(){
		$this->load->model('transaction/stock_adjustment');
		$customerDetails 	= $this->model_transaction_stock_adjustment->getCustomerDetails($_POST['name']);
		/*$str = '<select name="stock_name" id="stock_name">
			<option value=""> Select Customer </option>';
			if(count($customerDetails) > 0){
				foreach($customerDetails as $customers){
					$str .='<option value="'.$customers['customercode'].'">'.$customers['name'].'</option>';
				}
			}
		$str .='</select>';
		echo $str;*/
		if(count($customerDetails)>0){
			$str ='<ul id="purchase-form-autofill-invCode">';
			foreach($customerDetails as $customers){
				$var =  $customers['customercode'].'||'.$customers['name'];
				$str.= "<li onClick=\"fillCustomer('".$var."');\">".$customers['name']."</li>";
			}
			$str.='</ul>';
			echo $str;
		}
	}
	public function getReasons(){
		$this->load->model('transaction/stock_adjustment');
		$getReasons 	= $this->model_transaction_stock_adjustment->getReasonsByType($this->request->post['type']);
		$str = '
		<option value=""> Select Reasons </option>';
		if(count($getReasons) > 0){
			foreach($getReasons as $reasons){
				$str .='<option value="'.$reasons['reasoncode'].'">'.$reasons['reasoncode'].'</option>';
			}
		}
		echo $str;
	}
}
?>