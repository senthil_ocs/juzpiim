<?php 
class ControllerTransactionSales extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/purchase');

		$this->document->setTitle($this->language->get('sales_heading_title'));
		
		$this->load->model('transaction/sales');
		
		$this->clearPurchaseData();
		
		$this->getList();
	}
	
	public function insert() {	
		$cart_type = 'cart_purchase';		
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('sales_heading_title'));
		$this->load->model('transaction/sales');		

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['sales_button'] == 'submit') && $this->validateForm()) {
			$data = array();			
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			
			$product_data = array();	
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				); 
			}
			$data['products'] = $product_data;			
			$total_data = array();
			$this->load->model('ajax/cart');		
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;			
			$this->model_transaction_sales->addPurchase($data);
			$this->session->data['success'] = $this->language->get('text_success');			
			$this->clearPurchaseData();			
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}			
		if($this->rules->getAccessConditionByRule('product')) {
			$this->getForm();
		} else {
			$this->error['warning']	= 'Your domain have not product(s).';
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		//$this->getForm();
	}
	
	public function update() {		
		$cart_type = 'cart_purchase';		
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('sales_heading_title'));
		$this->load->model('transaction/sales');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['sales_button'] == 'submit') && $this->validateForm()) {
			$data = array();			
			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			
			$product_data = array();
	
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				); 
			}
			$data['products'] = $product_data;
			
			$total_data = array();
			$this->load->model('ajax/cart');		
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			
			$this->model_transaction_sales->editPurchase($data['purchase_id'], $data);
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->clearPurchaseData();
			
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->data['edit_form']	= true;
		$this->getForm();
	}

	protected function getList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('sales_heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}

		$url = '';		

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['insert'] = $this->url->link('transaction/sales/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['purchases'] = array();

		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		$salesTotal = $this->model_transaction_sales->getTotalSales($data);
		$results = $this->model_transaction_sales->getPurchaseList($data);
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		foreach ($results as $result) {			
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$userDetail = $this->model_user_user->getUser($result['created_by']);
			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => $result['transaction_date'],
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $vendorDetail['code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'total'             => $result['total'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date("d-m-Y h:i A", strtotime($result['created_date'])),
				'modify_button' => $this->url->link('transaction/sales/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL')
			);
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $salesTotal;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'transaction/sales_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		
		$this->data['heading_title'] = $this->language->get('sales_heading_title');		
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
				
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');

		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_sales->getPurchase($this->request->get['purchase_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));	
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				$purchaseProducts = $this->model_transaction_sales->getPurchaseProduct($purchaseInfo['purchase_id']);
				foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0)) {
						$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
					} elseif ($products['quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}	
				}		   
			}
		}
		
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {		
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];	
			   $productPrice = $this->request->post['price'];

			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }		   
		}
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {		    
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);				
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}				
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {			
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage; 
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price; 
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}						
		}	
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {		    
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {		    
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
		
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
				
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (!empty($purchaseInfo) || (!empty($this->request->post['purchase_id']))) {
			$this->data['action'] = $this->url->link('transaction/sales/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['purchase_id'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/sales/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
			$this->data['transaction_no'] = '';
		}
		
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['transaction_date'];
		} else {
			$this->data['transaction_date'] = '';
		}
		
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $this->session->data['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->data['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}
		
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = $purchaseInfo['reference_date'];
		} else {
			$this->data['reference_date'] = '';
		}
		
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		
		if (isset($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		
		//product collection
		$this->load->model('inventory/inventory');
		$this->data['products'] = array();
		
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		if ($this->session->data['vendor']) {
		    $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		}
		
		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		
		$this->load->model('ajax/cart');		
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();

		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->template = 'transaction/sales_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	
	protected function validateForm() {
	
		if (!$this->request->post['transaction_no']) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}
		
		if (!$this->request->post['transaction_date']) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		
		if (!$this->request->post['vendor']) {
			$this->error['vendor'] = "Please select any one vendor.";
		}
		
		if (!$this->request->post['reference_date']) {
			$this->error['reference_date'] = "Please enter reference date.";
		}
		
		if (!$this->request->post['reference_no']) {
			$this->error['reference_no'] = "Please enter reference no.";
		}
		
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		
		$purchaseInfo = $this->model_transaction_sales->getPurchaseByTransNo($this->request->post['transaction_no']);
		if (!empty($purchaseInfo)) {
			$this->error['warning'] = "Please enter unique transaction number.";
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];								
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);				
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	
	public function getPurchaseCartDetails($cart_type) {
		$product_data = array();
		foreach ($this->cart->getProducts($cart_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']
			); 
		}
		return $product_data;
	}
	
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];								
		$taxAmount = $this->cart->getTaxeByProduct($productDetails);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;		
		$discountData = $this->getDiscountCalculation($productDetails);		
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);			
		return $rowData;
	}
	
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	
	public function getDiscountCalculation($data, $type = '') {
	    
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
		
	}	
	
}
?>