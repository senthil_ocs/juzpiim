<?php
class ControllerTransactionCommon extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/common'); 
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->data['location_code'] = $this->session->data['location_code'];
		
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_general'] = 'Purchase';//$this->language->get('text_general');
		$this->data['text_sales'] = 'Sales';//$this->language->get('text_general');
		$this->data['text_stock'] = 'Stock';//$this->language->get('text_stock');	
		

		if($this->user->hasPermission('access', 'transaction/purchase') || $this->user->hasPermission('modify', 'transaction/purchase')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase", 'token=' . $this->session->data['token'], 'SSL'),
						                       "id"     => $this->language->get('purchase_id'),
				                               "Text" => $this->language->get('text_purchase'),
				                               "icon" =>'fa fa-shopping-cart'
				                               );
		}		
		
		if($this->user->hasPermission('access', 'transaction/purchase_invoice') || $this->user->hasPermission('modify', 'transaction/purchase')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase_invoice", 'token=' . $this->session->data['token'], 'SSL'),
						                       "id"   => $this->language->get('purchase_id'),
				                               "Text" => 'Purchase Invoice',
				                               "icon" =>'fa fa-address-book-o'
				                               );
		}

		if($this->user->hasPermission('access', 'transaction/purchase_return') || $this->user->hasPermission('modify', 'transaction/purchase_return')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase_return", 'token=' . $this->session->data['token'], 'SSL'),
				                               "id"     => $this->language->get('purchasereturn_id'),
				                               "Text" => $this->language->get('text_purchase_return'),
				                               "icon" =>'fa fa-truck'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/quotation') || $this->user->hasPermission('modify', 'transaction/quotation')){
			$this->data["sales"][] = array( "link" => $this->url->link("transaction/quotation", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Sales Quotation',
				                               "icon"  =>'fa fa-quote-left'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
			$this->data["sales"][] = array( "link" => $this->url->link("transaction/sales", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"     => $this->language->get('sales_id'),
				                               "Text" => 'Sales Order',
				                               "icon" =>'fa fa-share'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales') || $this->user->hasPermission('modify', 'transaction/sales')){
			$this->data["sales"][] = array( "link"   => $this->url->link("transaction/sales_invoice", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"    => $this->language->get('sales_id'),
				                               "Text"  => 'Sales Invoice',
				                               "icon"  =>'fa fa-address-book-o'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/sales_return') || $this->user->hasPermission('modify', 'transaction/sales_return')){
			$this->data["sales"][] = array( "link" => $this->url->link("transaction/sales_return", 'token=' . $this->session->data['token'], 'SSL'),
											   "id"     => $this->language->get('salesreturn_id'),
				                               "Text" => $this->language->get('text_sales_return'),
				                               "icon" =>'fa fa-reply'
				                               );
		} 
		/*
		$this->data["general"][] = array( "link" => $this->url->link("transaction/stock_adjustment", 'token=' . $this->session->data['token'], 'SSL'),
											   "altkey"=> $this->language->get('alt_stock'),
											   "id"     => $this->language->get('stock_id'),
				                               "Text" => $this->language->get('text_stock_adjust'),
				                               "icon" =>'fa fa-random'
				                               );
		*/
		if(STOCK_TRANSFER=='1'){
			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_transfer_out", 'token=' . $this->session->data['token'], 'SSL'),
												   "id"     => '',
					                               "Text" => 'Stock Transfer out',
					                               "icon" =>'fa fa-sign-out'
					                               );

			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_transfer_in", 'token=' . $this->session->data['token'], 'SSL'),
												   "id"     => '',
					                               "Text" => 'Stock Transfer In',
					                               "icon" =>'fa fa-sign-in'
					                               );
		}
		
		if($this->user->hasPermission('access', 'transaction/stock_request') || $this->user->hasPermission('modify', 'transaction/stock_request')){
			$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_request", 'token=' . $this->session->data['token'], 'SSL'),
													   "id"     => '',
						                               "Text" => 'Stock Request',
						                               "icon" =>'fa fa-list'
						                               );
		}

		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_adjustment/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stockadjust_id'),
				                               "Text" => $this->language->get('text_stock_adjust'),
				                               "icon" =>'fa fa-random'
				                               );

		/*$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stocktake_id'),
				                               "Text" => $this->language->get('text_stock_take'),
				                               "icon" =>'fa fa-list-alt'
				                               );
		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/updatelist", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "id"     => $this->language->get('stockupdate_id'),
				                               "Text" => $this->language->get('text_stock_updation'),
				                               "icon" =>'fa fa-undo'
				                               );
		*/
		$this->data['token'] = $this->session->data['token'];	
		$this->template = 'transaction/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getProductDetailsbyScan() {
			$this->load->model('transaction/purchase');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$_productDetails           = $this->model_transaction_purchase->getProductByName($this->data);
	}
 }
?>