<?php
class ControllerTransactionStockTake extends Controller { //StockTake
	private $error = array();
	
	public function index() {
		$this->language->load('transaction/stock_take');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_take');
		$this->clearPurchaseData();
		$this->getList();
		$company_id	= $this->session->data['company_id'];
	}


	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			/*if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}*/
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_take');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			    //$this->clearPurchaseData($remove = 'discount');
			}
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity']/*,
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost']*//*,
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']*/
				);
			}
			/*$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}*/
			$this->response->setOutput(json_encode($res));
		}
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_take');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity']/*,
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']*/
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					if($this->request->post['discount']=='')
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="9" align="right" class="purchase_total_left">'.$lable.'</td><td class="purchase_total_right">
							          	<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="input-text row_total_box"
							          	readonly="readonly" /></td></tr>';
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
			}
			$this->response->setOutput(json_encode($res));
	}
	public function ajaxaddproducts(){
		$this->load->model('transaction/stock_take');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
			$vendor_id = $this->request->post['vendor'];
			//$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($vendor_id);
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			  $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		
		if($apply_tax_type=='0'){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}

		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);

		//$apply_tax_type       	= $this->config->get('config_apply_tax');
		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = round($purchase['total'],1);
					} else {
					    $totalValue = round($purchase['tax'] + $purchase['total'],1);
					}
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
		               <td class="center">'.$purchase['quantity'].'</td>
	                   <td class="center"> [ <a onclick="removeProductData('.$pid.');"> Remove </a> ] </td>
                                                </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type=='2'){
						$res['sub_total_value'] = $data['totals'][0]['value'] - $tax['value'];
						$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
					}
					$cnt = count($data['totals'])-1;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="9">GST </td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	
	public function insert() {

        $cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_take');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_take');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit') && $this->validateForm()) {
			
			if(isset($this->request->post['location'])) {
				$data['location']	=  $this->request->post['location'];
			} else {
				$data['location']	= '';
			}
			if(isset($this->request->post['terminal'])) {
				$data['terminal']	=  $this->request->post['terminal'];
			} else {
				$data['terminal']	= '';
			}
			
			$product_data = array();


			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'sku'                       => $product['sku']
				);
			}
			$data['products'] = $product_data;

			$total_data = array();
			$this->load->model('ajax/cart');

			$vendor_id = $this->request->post['vendor'];
			$apply_tax_type = $this->model_transaction_stock_take->getVendorsTaxId($vendor_id);
	
			$this->model_transaction_stock_take->addPurchase($data,$costMethod);
		   
			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		
		$this->getForm();
	}
	public function update() {
		$this->load->model('transaction/stock_take');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->model_transaction_stock_take->updatestatus();
			$this->model_transaction_stock_take->addStockTakeSummary($this->request->post);
			$this->session->data['success'] = "Stock Details updated successfully";
		}
		$this->redirect($this->url->link('transaction/stock_take/updatelist', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		
	}
	public function exportcsv(){

		$this->language->load('transaction/stock_take');
		$this->load->model('transaction/stock_take');
		$this->data['heading_title'] = 'Stock Take';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		$results2 = $this->model_transaction_stock_take->getupdateStockTakeCSV($data);
		if(count($results2>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=stocktake_update.csv');
	        print "S.No,sku Code,sku_description,scanned_qty,sku_qty\r\n";
		
			$j=0; $total=0;
		foreach ($results2 as $result) {
			$j++;
			 
				$sku  				= $result['sku'];
				$sku_description   	= trim($result['sku_description']);
				$scanned_qty       	= $result['scanned_qty'];
				$sku_qty       		= $result['sku_qty'];
		print "$j,\"$sku\",\"$sku_description\",\"$scanned_qty\",\"$sku_qty\"\r\n";
		}
		}  
	}
	public function updatelist(){
		//language assign code for transaction purchase
		$this->language->load('transaction/stock_take');
		$this->document->setTitle('Stock Take Update');
		$this->load->model('transaction/stock_take');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Stock Take Update';
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_location_code'] = $this->language->get('text_location_code');
		$this->data['text_terminal_code'] = $this->language->get('text_terminal_code');
		$this->data['text_total_items'] = $this->language->get('text_total_items');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take Update List',
			'href'      => $this->url->link('transaction/stock_take/updatelist', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
	
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		//$purchase_total = $this->model_transaction_purchase->getTotalPurchase($data);
		$results1 = $this->model_transaction_stock_take->getupdateStockTakeList($data);
		// printArray($results1); die;
		foreach ($results1 as $result) {
			$this->data['stock_take'][] = array(
				'location_code'   	=> $result['location_code'],
				'sku'  				=> $result['sku'],
				'sku_description'   => trim($result['sku_description']),
				'scanned_qty'       => $result['scanned_qty'],
				'sku_qty'       	=> $result['sku_qty'],
			);
		}
		

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['update_action'] 	= $this->url->link('transaction/stock_take/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$url.=$pageurl;
		$this->data['link_csvexport'] = $this->url->link('transaction/stock_take/exportcsv', 'token=' . $this->session->data['token'] . '&&'.$pageurl, 'SSL');
		$this->template = 'transaction/stock_take_updatelist.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		
	}
	
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_location_code'] = $this->language->get('text_location_code');
		$this->data['text_terminal_code'] = $this->language->get('text_terminal_code');
		$this->data['text_total_items'] = $this->language->get('text_total_items');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		$page = $_REQUEST['page'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		} 
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
			$pageUrl.='&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
			$pageUrl.='&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to ="";
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.='&filter_location_code='.$filter_location_code; 
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}

		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take List',
			'href'      => $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$hold_button = '&show_hold=1';
		$this->data['insert'] = $this->url->link('transaction/stock_take/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['show_hold_button'] = $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'filter_location_code'	   => $filter_location_code			
		);
		//printArray($data);
		$purchase_total = $this->model_transaction_stock_take->getStockTakeCount($data);
		$results1 = $this->model_transaction_stock_take->getStockTakeList($data);
        //printArray($results1);
        if(!$page){
			$page = 1;
		}
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		foreach ($results1 as $result) {
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			$this->data['stock_take'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'),
				'location_code'    => $result['location_code'],
				'stocktake_id'     => $result['stocktake_id'],
				'stocktake_date'   => date('d/m/Y',strtotime($result['stocktake_date'])),
				'terminal_code'    => $result['terminal_code'],
				'is_updated'       => $result['is_updated'],
				'createdby'        => $userDetail['username'],
				'createdon'        => date('d/m/Y',strtotime($result['createdon'])),
				'modify_button'    => $this->url->link('transaction/stock_take/update', 'token=' . $this->session->data['token'] . '&stocktake_id=' . $result['stocktake_id'] . $url, 'SSL'),
				'view_button' => $this->url->link('transaction/stock_take/view', 'token=' . $this->session->data['token'] . '&stocktake_id=' . $result['stocktake_id'] . $url, 'SSL')
			);
		}


		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$url.=$pageUrl;
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['purchaseInfo'] 	= $this->model_transaction_stock_take->getStockTakeDetailsById($this->request->get['stocktake_id']);
		$this->data['purchaseDetail'] 	= $this->model_transaction_stock_take->getStockTakeDetails($this->data['purchaseInfo']['purchase_id']);
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $filter_location_code;
		$this->template = 'transaction/stock_take_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_tran_id'] = $this->language->get('entry_tran_id');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_terminal'] = $this->language->get('entry_terminal');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_raw_cost'] = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		$apply_tax_type = 0;
		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_stock_take->getPurchase($this->request->get['purchase_id']);
			$apply_tax_type = $this->model_transaction_stock_take->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				$purchaseProducts = $this->model_transaction_purchase->getPurchaseProduct($purchaseInfo['purchase_id']);
				foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0)) {
						$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
					} elseif ($products['quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  //$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($vendor_id);

		}



		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_id'])) {
			$this->data['error_transaction_id'] = $this->error['transaction_id'];
		} else {
			$this->data['error_transaction_id'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['location'])) {
			$this->data['error_location'] = $this->error['location'];
		} else {
			$this->data['error_location'] = '';
		}
		if (isset($this->error['terminal'])) {
			$this->data['error_terminal'] = $this->error['terminal'];
		} else {
			$this->data['error_terminal'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		/*$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take List',
			'href'      => $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);*/
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!empty($purchaseInfo) || (!empty($this->request->post['purchase_id']))) {
			$this->data['action'] = $this->url->link('transaction/stock_take/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['purchase_id'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/stock_take/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
												//echo 'here'; exit;

		$this->data['cancel'] = $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
				// Ragu
			 	 
			$this->data['transaction_no'] = $this->getTransNo();
				/*$transNumberWithSuffix = $this->model_inventory_inventory->getPurchaseAutoId();
				if($transNumberWithSuffix){
					 $transaction_no = ltrim($transNumberWithSuffix,'HQ')+1;
					 $this->data['transaction_no'] = 'HQ'.$transaction_no;
				}else{
					$transNumber = $this->model_inventory_inventory->getInventoryAutoId();
					if (empty($purchasePrefix)) {
						$this->data['transaction_no'] = 'TRANS'.$transNumber['sku'];
					} else {
					    $this->data['transaction_no'] = trim($purchasePrefix).($transNumber['sku']+1);
					}
				} */
		} //$this->data['locations']
		 //$re= $this->model_transaction_stock_take->getLocations();
		//printArray($re);exit;
		$company_id	= $this->session->data['company_id'];
		if (isset($company_id)) {
		$company_info = $this->model_transaction_stock_take->getCompanyDetails($company_id);
    	}
    	//printArray($company_info);exit;
		if (isset($this->request->post['location'])) {
			$this->data['location'] = $this->request->post['location'];
		} elseif (empty($this->request->post['location'])) {
			$this->data['location'] = $this->session->data['location_code'];
		} else {
			$this->data['location'] = '';
		}

		//printArray($this->data['location']);exit;
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['transaction_date'];
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $this->session->data['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->data['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = $purchaseInfo['reference_date'];
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		
		if (isset($this->request->post['terminal'])) {
			$this->data['terminal'] = $this->request->post['terminal'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['terminal'] = $purchaseInfo['terminal'];
		} else {
			$this->data['terminal'] = '';
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		if (isset($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}


		//product collection
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		//if ($this->session->data['vendor']) {
		    $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		//}
		

		//purchase cart collection
		 
		    
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		$this->data['apply_tax_type']       = $apply_tax_type;
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		//printArray($this->data['purchase_totals']);
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['route'] = 'transaction/stock_take';
		$this->template = 'transaction/stock_take_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_id'])) {
			$this->error['transaction_id'] = "Please enter transaction Id.";
		}
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='') {
		$product_data = array();
		if($apply_tax_type=='0'){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' 			=> $product['product_id'],
				'product_id' 	=> $product['key'],
				'code' 			=> $product['sku'],
				'name'       	=> $product['name'],
				'quantity'   	=> $product['quantity']/*,
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']*/
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$this->load->model('transaction/stock_take');
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		
		/*$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		
		if($productDetails['vendor_allow_gst']=='1'){
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($productDetails['vendor']);
		}else{
			$apply_tax_type = 0;
		}

		if($apply_tax_type=='1'){
			$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		}else{
			$taxAmount = 0;
		}
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;*/
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice/*,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     =>number_format($total,2)*/
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data='', $type = '') {
			$this->load->model('transaction/stock_take');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_purchase->getProductByName($this->data);
			$str = '';
			if(count($_productDetails)>=1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost']=='0.000'){
						//$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'];
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}
	public function getProductSkuDetails()
	{
		$this->load->model('transaction/stock_take');
		$id = $this->request->post['id'];
		$SkuProductDetails = $this->model_transaction_stock_take->getproductdetails($id);
		if($SkuProductDetails['average_cost']=='0.0000'){
					$SkuProductDetails['average_cost'] =' ';
		}
		echo json_encode($SkuProductDetails);
	}
	public function getTransNo(){
		$this->load->model('transaction/stock_take');
		$transNumber = $this->model_transaction_stock_take->getStockTakeAutoId();
	
		if($transNumber){
			$this->data['transaction_no'] = str_pad($transNumber, 4, '0', STR_PAD_LEFT);
		}else{
			$transNumber = 1;
			$this->data['transaction_no'] = str_pad($transNumber, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	public function view(){
		$this->language->load('transaction/stock_take');
		$this->load->model('transaction/stock_take');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_location_code'] = $this->language->get('text_location_code');
		$this->data['text_terminal_code'] = $this->language->get('text_terminal_code');
		$this->data['text_total_items'] = $this->language->get('text_total_items');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take List',
			'href'      => $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Take View',
			'href'      => $this->url->link('transaction/stock_take/view', 'token=' . $this->session->data['token'].'&stocktake_id='.$this->request->get['stocktake_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/stock_take', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_stock_take->getStockTakeDetailsById($this->request->get['stocktake_id']);
		$this->data['productDetails'] 	= $this->model_transaction_stock_take->getStockstDetailsById($this->data['purchaseInfo']['stocktake_id']);
		$this->data['update_action'] 	= $this->url->link('transaction/stock_take/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['stocktake_id'] 	= $this->request->get['stocktake_id'];
		$this->data['location_code'] 	= $this->data['purchaseInfo']['location_code'];

		$this->template = 'transaction/stock_take_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
}
?>