<?php
class ControllerTransactionQuotation extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Quotation');
		$this->load->model('transaction/quotation');
		$this->clearPurchaseData();
		$this->getList();
	}
	
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/quotation');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$this->data['transaction_date'] = $this->request->post['transactiondate'];
			$this->data['customer_code'] = $this->request->post['customercode'];
			$_productDetails           = $this->model_transaction_quotation->getProductByNamenew($this->data);

			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
                    $name_display = $_productDetails[$i]['name'];
                    $_productDetails[$i]['name'] = str_replace("'", '^', $_productDetails[$i]['name']);
                    $_productDetails[$i]['name'] = str_replace('"', '!!', $_productDetails[$i]['name']);
                    
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($name_display).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
				//$str = $var;
			}
			echo $str;
	}
	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   
			   if(!empty($this->session->data['cart_purchase'])){
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
 		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}

		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = $purchase['total'] - $purchase['tax'];
					} else {
					    $totalValue = $purchase['tax'] + $purchase['total'];
					}
				$purchase['price'] = str_replace("$","",$purchase['price']);
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
						<td class="center"><textarea class="textbox description" name="description['.$purchase['product_id'].']" rows = "2" cols ="60" id="desc_'.$purchase['product_id'].'" data-product_id="'.$purchase['product_id'].'" data-code="'.$purchase['code'].'" onblur="update_qty_price('.$purchase['product_id'].');">'.strip_tags(nl2br($purchase['description'])).'</textarea></td>
	                   <td class="center order-nopadding"><input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
	                   <td class="text-right order-nopadding">
	                   	<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace(",","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>';
					$str.='<td class="text-right" id="subTot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>
					   
					   <td align="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type == '2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format($res['sub_total_value']);
					}
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					$tax_text = 'GST';
					$tax_text = $apply_tax_type =='2' ? $tax_text."(7% Incl)" : $tax_text."(7% Excl)";
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="9">'.$tax_text.'</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/sales');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}else if($bill_discount_percentage =='' || $bill_discount_percentage=='0' || $bill_discount_price=='0' || $bill_discount_price==''){
				if ($this->session->data['bill_discount']) {
					unset($this->session->data['bill_discount']);
					unset($this->session->data['bill_discount_mode']);
				}
				unset($this->session->data['bill_discount_percentage']);
				unset($this->session->data['bill_discount_price']);
			}
		}
		if(isset($this->request->post['handling_fee']) && $this->request->post['handling_fee'] > 0){
			$this->session->data['handling_fee'] = $this->request->post['handling_fee'];
			$res['handling_fee']    = $this->currency->format($this->session->data['handling_fee']);
		}else{
			unset($this->session->data['handling_fee']);
			$res['handling_fee']    = $this->currency->format('0');
		}
		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			// printArray($apply_tax_type); die;
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					$res['discount_price']  	 = $bill_discount_price;
					$res['discount_percentage']  = $bill_discount_percentage;

					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					$res['discountRow'] = '';

					if($res['discount_value'] !=''){
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="9" align="right" class="purchase_total_left">'.$lable.'</td>
							<td class="purchase_total_right insertproduct">
							<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="textbox row_total_box text-right" readonly="readonly" /></td></tr>';
					}
				}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				$res['bill_discount']     = $this->session->data['bill_discount'];
				$res['bill_discount_mode']= $this->session->data['bill_discount_mode'];
			}
			
			$this->response->setOutput(json_encode($res));
	}
	public function getProductDetailsbyCustomer($data='', $type = '') {
			$this->load->model('transaction/quotation');
			$this->data['sku'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$this->data['transaction_date'] = $this->request->post['transactiondate'];
			$this->data['customer_code'] = $this->request->post['customercode'];
			$CustomerDetails           = $this->model_transaction_quotation->getcustomerPrice($this->data);
			for($i=0;$i<count($CustomerDetails);$i++){
			$values =  $CustomerDetails[$i]['sku'].'||'.$CustomerDetails[$i]['customer_price'];
		    }
		    echo $values;
	}
	public function insert() {
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Quotation'));
		$this->load->model('transaction/quotation');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');

         $type='insert';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '';
			}
			
			if (isset($this->request->post['currency_code'])) {
				$data['currency_code'] = $this->request->post['currency_code'];
			} else {
				$data['currency_code'] = '1';
			}
			if (isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate'] = $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate'] = '1';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			if (isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			} else {
				$data['shipping_id'] = '';
			}
			if (isset($this->request->post['network_id'])) {
				$data['network_id'] = $this->request->post['network_id'];
			} else {
				$data['network_id'] = '';
			}

			$product_data = array();
 			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'price'                     => $product['price'],
					'quantity'                  => $product['quantity'],
					'sku'                 		=> $product['sku'],
					'sku_price'                 => $product['sku_price'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = '';
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			$data['transaction_no']	= $this->getTransNo();			
			// printArray($data); die;
			$this->model_transaction_quotation->addPurchase($data,$costMethod);
			$this->session->data['success'] = 'Success: Sales Quotation added successfully!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		/*if($this->rules->getAccessConditionByRule('product')) {
			$this->getForm();
		} else {
			$this->error['warning']	= 'Your domain have not product(s).';
			$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}*/
		$this->getForm();
	}
	public function update() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Quotation'));
		$this->load->model('transaction/quotation');
		$this->load->model('transaction/purchase');
		//printArray($this->request->post); exit;
		$type = 'update';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();

			if(isset($this->request->get['purchase_id'])) {
				$data['purchase_id']	=  $this->request->get['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['term_id'])) {
				$data['term_id']	=  $this->request->post['term_id'];
			} else {
				$data['term_id']	= '';
			}
			if(isset($this->request->post['shipping_id'])) {
				$data['shipping_id']	=  $this->request->post['shipping_id'];
			} else {
				$data['shipping_id']	= '';
			}
			if(isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id']	=  $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id']	= '';
			}
			if(isset($this->request->post['tax_type'])) {
				$data['tax_type']	=  $this->request->post['tax_type'];
			} else {
				$data['tax_type']	= '';
			}
			if (isset($this->request->post['network_id'])) {
				$data['network_id'] = $this->request->post['network_id'];
			} else {
				$data['network_id'] = '';
			}
			$product_data = array();
 			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'sku_price'                 => $product['sku_price'],
					'sku'                 		=> $product['sku'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = array();
			$this->load->model('ajax/cart');

		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			// printArray($total_data); die;
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$this->model_transaction_quotation->editPurchase($data['purchase_id'], $data);
			$this->session->data['success'] = 'Success: You have modified quotation!';
			$this->clearPurchaseData();
			
			$url = '';
			$filter_from_date = date('d/m/Y');
			if (isset($_REQUEST['filter_from_date'])) {
				$filter_from_date = $_REQUEST['filter_from_date'];
				$url .= '&filter_from_date=' . $filter_from_date;
			}
			$filter_to_date = date('d/m/Y');
			if (isset($_REQUEST['filter_to_date'])) {
				$filter_to_date = $_REQUEST['filter_to_date'];
				$url .= '&filter_to_date=' . $filter_to_date;
			}
			$filter_location = $this->session->data['location_code'];
			if (isset($_REQUEST['filter_location'])) {
				$filter_location = $_REQUEST['filter_location'];
				$url .= '&filter_location=' . $filter_location;
			}
			$filter_transaction = null;
			if (isset($_REQUEST['filter_transaction'])) {
				$filter_transaction = $_REQUEST['filter_transaction'];
				$url .= '&filter_transaction=' . $filter_transaction;
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('Sales');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
		}else{
			$filter_from_date = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		$url .= '&filter_from_date=' . $filter_from_date;
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
		}
		$url .= '&filter_to_date=' . $filter_to_date;
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		$url .= '&filter_location=' . $filter_location;
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home','token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'=>$this->url->link('transaction/common_sales','token='.$this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Quotation List',
			'href'=>$this->url->link('transaction/quotation','token='.$this->session->data['token'],'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['purchases'] = array();
		$data = array(
			'filter_from_date'	=> $filter_from_date,
			'filter_to_date'	=> $filter_to_date,
			'filter_location'	=> $filter_location,
			'filter_transaction'=> $filter_transaction,
			'show_hold'	  		=> $show_hold,
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit')
		);
		$this->data['filters'] = $data;
		$Sales_total 		   = $this->model_transaction_quotation->getTotalSalesnew($data);
		$results 			   = $this->model_transaction_quotation->getPurchaseList($data);
		// printArray($results); die;
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$vendorDetail = array();
		$userDetail   = array();
		foreach ($results as $result) {
			$vendor_id 		= str_pad($result['vendor_id'],4,"0",STR_PAD_LEFT);
			$vendorDetail 	= $this->model_transaction_quotation->getB2BCustomersbyCode($vendor_id);
			$userDetail 	= $this->model_user_user->getUser($result['created_by']);
			$this->data['purchases'][] = array(
				'pagecnt'           => ($page-1) * $this->config->get('config_admin_limit'),
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['discount_type'],
				'vendor_code'       => $vendorDetail['vendor_code'],
				'vendor_name'       => $vendorDetail['name'],
				'total'             => $result['total'],
				'issaled'           => $result['issaled'],
				'location_code'     => $result['location_code'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'modify_button' 	=> $this->url->link('transaction/quotation/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'view_button' 		=> $this->url->link('transaction/quotation/view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'delete_button'		=>$this->url->link('transaction/quotation/bulk_delete', 'token=' . $this->session->data['token'].'&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'download_button' 	=>$this->url->link('transaction/quotation/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$result['purchase_id'].$url,'SSL'),
				'sales_convert_button'=> $this->url->link('transaction/sales/insert', 'token='.$this->session->data['token'] . '&type=quote&purchase_id='.$result['purchase_id']. $url, 'SSL')
			);
		}
		
		$this->data['delete']= $this->url->link('transaction/quotation/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		$hold_button = '&show_hold=1';
		$this->data['insert'] = $this->url->link('transaction/quotation/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$this->data['action'] =  $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		
		$pagination 		= new Pagination();
		$pagination->total 	= $Sales_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['route']= $this->request->get['route'];

		$this->template = 'transaction/quotation_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {
 		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/customers');
		$this->load->model('transaction/quotation');
		$this->load->model('setting/location');
		$this->load->model('setting/location');

		$this->data['heading_title'] = $this->language->get('Sales');
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_customer_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_customer_name');
		$this->data['entry_refno']         = $this->language->get('entry_refno');
		$this->data['entry_refdate']       = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name']   = $this->language->get('text_product_name');
		$this->data['text_qty']            = $this->language->get('text_qty');
		$this->data['text_price']          = $this->language->get('text_price');
		$this->data['text_raw_cost']	   = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] 	   = $this->language->get('text_disc_perc');
		$this->data['text_disc_price']     = $this->language->get('text_disc_price');
		$this->data['text_net_price']      = $this->language->get('text_net_price');
		$this->data['text_total']          = $this->language->get('text_total');
		$this->data['text_tax']            = $this->language->get('text_tax');
		$this->data['text_weight_class']   = $this->language->get('text_weight_class');
		$this->data['text_tax_class']      = $this->language->get('text_tax_class');
		$this->data['text_no_data']        = $this->language->get('text_no_data');
		$this->data['entry_vendor']        = $this->language->get('entry_vendor');
        $this->data['entry_location']      = $this->language->get('entry_location');
		$this->data['text_remove']         = $this->language->get('text_remove');
		$this->data['text_select_vendor']  = $this->language->get('text_select_vendor');
		$this->data['text_hold'] 		   = $this->language->get('text_hold');
		$this->data['button_hold'] 		   = $this->language->get('button_hold');
		$this->data['button_save'] 	       = $this->language->get('button_save');
		$this->data['button_cancel'] 	   = $this->language->get('button_cancel');
		$this->data['button_add'] 		   = $this->language->get('button_add');
		$this->data['button_clear'] 	   = $this->language->get('button_clear');
		$this->data['column_action'] 	   = $this->language->get('column_action');
         $apply_tax_type = 0;
		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_quotation->getSalesnew($this->request->get['purchase_id']);
			// printArray($purchaseInfo); exit;
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				if($purchaseInfo['handling_fee']){
					$this->session->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
				}
				$purchaseProducts = $this->model_transaction_quotation->getSalesProductnew($purchaseInfo['purchase_id']);
				foreach ($purchaseProducts as $products) {

					$products = array_merge($products, $this->getDiscountCalculation($products));
					$products['qty']   = (int)$products['quantity'];
					if(!empty($this->session->data['cart_purchase'])){
						if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['qty'] > 0)) {
							$this->cart->update($products['product_id'], $products['qty'], $cart_type, $products);
						} elseif ($products['qty'] > 0) {
							$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
						}
					} elseif ($products['qty'] > 0) {
						$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		// printArray($this->session->data['purchase']); die;
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  $apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($vendor_id);

		}
            
		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		$filter_from_date = date('d/m/Y');
		if (isset($this->request->get['filter_from_date'])) {
			$url .= '&filter_from_date=' . $this->request->get['filter_from_date'];
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->get['filter_to_date'])) {
			$url .= '&filter_to_date=' . $this->request->get['filter_to_date'];
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($this->request->get['filter_location'])) {
			$url .= '&filter_location=' . $this->request->get['filter_location'];
		}
		$filter_transaction = null;
		if (isset($this->request->get['filter_transaction'])) {
			$url .= '&filter_transaction=' . $this->request->get['filter_transaction'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Quotation List',
			'href'      => $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Quotation',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
       
		if (!empty($purchaseInfo) || (!empty($this->request->post['purchase_id']))) {
			$this->data['action'] = $this->url->link('transaction/quotation/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['purchase_id'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/quotation/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
	
		if (isset($this->request->get['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();
		}
		// printArray($this->data['transaction_no']); die;
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['transaction_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		} 
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}

		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] 		   = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->data['vendor'];
			$vendorData = $this->model_transaction_quotation->getB2BCustomersbyCode($this->request->post['vendor']);
			$this->data['vendor'] = $vendorData['name'];
		} elseif (isset($this->request->get['newCustId'])) {
			$vendorData = $this->model_transaction_quotation->getCustomerDetails($this->request->get['newCustId']);
			$this->data['vendor'] 	       = $vendorData['customercode'];
			$this->data['vendor_name'] 	   = $vendorData['name'];
			$this->data['vendorTaxAndType']= $vendorData['tax_allow'].'||'.$vendorData['tax_type'];
			$this->session->data['vendor'] = $this->data['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $this->data['vendor'] = $purchaseInfo['vendor_id'];
			$vendorData = $this->model_transaction_quotation->getB2BCustomersbyCode($purchaseInfo['vendor_id']);
			$this->data['vendor_name'] = $vendorData['name'];
		} else {	
			$this->data['vendor'] 		   = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] 	   = '';
		}
		
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}

		if (isset($this->request->post['network_id'])) {
			$this->data['network_id'] = $this->request->post['network_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['network_id'] = $purchaseInfo['network_id'];
		} else {
			$this->data['network_id'] = '';
		}
		
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date("d/m/Y", strtotime($purchaseInfo['reference_date']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		
		if ($this->session->data['bill_discount_mode']=='1' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_percentage'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}

		if ($this->session->data['bill_discount_mode']=='2' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_price'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		if ($this->session->data['handling_fee']) {
			$this->data['handling_fee'] = $this->session->data['handling_fee'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
		} else {
			$this->data['handling_fee'] = '';
		}
		// printArray($this->data); die;

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}
		$this->data['tax_type'] = $this->data['tax_type'] =='0' ? '1': '2';
		// echo $this->data['tax_type']; die;

		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = '';
		}
		
		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}
		
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}
		// echo $this->data['term_id']; die;
		
		if (isset($this->request->post['shipping_id'])) {
			$this->data['shipping_id'] = $this->request->post['shipping_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['shipping_id'] = $purchaseInfo['shipping_id'];
		} else {
			$this->data['shipping_id'] = '';
		}

		if (isset($this->request->get['purchase_id'])) {
			$this->data['purchase_id'] = '1';
		} else {
			$this->data['purchase_id'] = '0';
		}
		//product collection
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		$this->data['clocation_code'] = $this->session->data['location_code'];
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		//if ($this->session->data['vendor']) {
		    // $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		//}
        //purchase cart collection
        $apply_tax_type = $this->data['tax_class_id'] =='1' ? 0 : $this->data['tax_type'];
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		$this->data['allTerms'] = $this->cart->getAllTerms();
		$this->data['apply_tax_type']       = $apply_tax_type;
		$this->load->model('ajax/cart');
		$this->data['purchase_totals'] = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		// printArray($this->data['purchase_totals']); die;
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		$this->data['convert_invoice'] = $this->url->link('transaction/sales/insert', 'token='.$this->session->data['token'] . '&type=quote&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');

		// Customer Collection
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);
		$this->data['customer_collection'] = $this->model_transaction_quotation->getB2BCustomers();
		$this->data['currencys'] = $this->model_transaction_purchase->getCurrency();
		$this->data['channels_list'] = $this->cart->getChannelsList();
		$this->data['token']  	 = $this->session->data['token'];
		$this->data['cancel'] 	 = $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['addCust']   = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'].'&from=quotation', 'SSL');
		$this->data['route'] 	 = 'transaction/sales';
		$this->template = 'transaction/quotation_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='',$customer_id='') {
		
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one customer code.";
		}
		if (empty($this->request->post['reference_date'])) {
			$this->error['reference_date'] = "Please enter reference date.";
		}
		// if (empty($this->request->post['reference_no'])) {
		// 	$this->error['reference_no'] = "Please enter reference no.";
		// }
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}else{
			if($customer_id==''){
				$purchaseInfo = $this->model_transaction_quotation->getSalesByinvNonew($this->request->post['transaction_no']);
				if($purchaseInfo){
				$this->error['warning'] = 'Please enter unique transaction number.';
			 }
			}
		}
		//printArray($this->error); exit;
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='') {
		$product_data = array();
		if($apply_tax_type=='0'){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		//echo $apply_tax_type=0;
		// printArray($this->cart->getProducts($cart_type,$apply_tax_type)); exit;
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['sku_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode'],
				'description'   => $product['description']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			unset($this->session->data['handling_fee']);
			unset($this->session->data['bill_discount_percentage']);
			unset($this->session->data['bill_discount_price']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data, $type = '') {
			$this->load->model('transaction/quotation');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_quotation->getProductByNamenew($this->data);
			$str ='<ul id="country-list">';
			for($i=0;$i<count($_productDetails);$i++){
				$var =  $_productDetails[$i]['product_id'].'||'.$_productDetails[$i]['sku'].'||'.$_productDetails[$i]['name'].'||'.$_productDetails[$i]['price'];
				$str.= "<li onClick=\"selectedProduct('".$var."');\">".$_productDetails[$i]['sku'].' ('.$_productDetails[$i]['name'].")</li>";
			}
			$str.='</ul>';
			echo $str;
	}
	public function getTransNo(){
		
	    $transNumberWithSuffix = $this->model_transaction_quotation->getSalesAutoIdnew();
		$transNumber		   = $transNumberWithSuffix;
		if($transNumber){
			 $transaction_no = ltrim($transNumber)+1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_quote_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_sales_quote_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	public function view(){
		$this->language->load('transaction/sales');
		$this->load->model('transaction/quotation');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Quotation';
		$this->document->setTitle('Quotation');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		$filter_from_date = date('d/m/Y');
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Quotation List',
			'href'      => $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Quotation View',
			'href'      => $this->url->link('transaction/quotation/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete_button'] = $this->url->link('transaction/quotation/delete', 'token=' . $this->session->data['token'] .'&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');
		$this->data['modify_button'] = $this->url->link('transaction/quotation/update', 'token=' . $this->session->data['token'] .'&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');

		$this->data['purchaseInfo']    = $this->model_transaction_quotation->getSalesnew($this->request->get['purchase_id']);
		// $this->data['purchaseInfo']['createdby'] = $this->cart->getUserDetails($this->data['purchaseInfo']['created_by'])['username'];
		$this->data['shippingAddress'] = $this->model_transaction_quotation->getShippingAddressById($this->data['purchaseInfo']['shipping_id']);
		$this->data['vendorDetail']    = $this->model_transaction_quotation->getB2BCustomersbyCode($this->data['purchaseInfo']['vendor_id']);
		$this->data['productDetails']  = $this->model_transaction_quotation->getSalesProductnew($this->data['purchaseInfo']['purchase_id']);
		$this->data['convert_invoice'] = $this->url->link('transaction/sales/insert', 'token='.$this->session->data['token'] . '&type=quote&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');
		$this->data['download_button'] = $this->url->link('transaction/quotation/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url,'SSL');

		$this->template = 'transaction/quotation_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function delete(){
		$this->load->model('transaction/quotation');
		if($this->request->get['purchase_id']!=''){
			$this->model_transaction_quotation->delete_quote($this->request->get['purchase_id']);
		}
		$url = '';
		$filter_from_date = date('d/m/Y');
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	public function create_invoice(){
		$this->load->model('transaction/quotation');
		if($this->request->get['purchase_id']!=''){
			$this->model_transaction_quotation->create_invoice($this->request->get['purchase_id']);
		}
		$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	public function download_pdf(){

		$this->load->model('transaction/quotation');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');

		if($this->request->get['purchase_id'] !=''){
			$purchase_id = $this->request->get['purchase_id'];
		}else{
			$purchase_id = $id;
		}

		$company_id	     = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$company_details['invoice_title'] = "Sales Quotation"; //Set invoice title here
		$quoteInfo       = $this->model_transaction_quotation->getSalesnew($purchase_id);
		$vendorDetail    = $this->model_transaction_quotation->getB2BCustomersbyCode($quoteInfo['vendor_id']);
		$productDetails	 = $this->model_transaction_quotation->getSalesProductnew($quoteInfo['purchase_id']);
		$company_details['location'] = $this->model_inventory_reports->getLocationDetails($quoteInfo['location_code']);
		// printArray($productDetails); die;
		$transaction_date= date('d/m/Y',strtotime($quoteInfo['transaction_date']));
		$tax_type = $quoteInfo['tax_type']=='0' ? '(7% Exclusive)':'(7% Inclusive)';
        $tax_type = $quoteInfo['tax_class_id']=='2' ? $tax_type :'(0%)';
	
		$str = $this->url->getCompanyAddressHeaderString2($company_details);
		$strs.= '</table>';
		$str .='<table class="table" style="width:100%;">
			<tr>
				<td class="left">
				<table class="listings"  style="width:100%;">
					<tr>
						<th align="left">Name</th>
						<td>'.$vendorDetail['name'].'</td>
					</tr>
					<tr>
						<th align="left">Address</th>
						<td>'.$vendorDetail['address1'].' '.$vendorDetail['address2'].', '.$vendorDetail['city'].' - '.$vendorDetail['zipcode'].'</td>
					</tr>
					<tr>
						<th align="left">Contact</th>
						<td>Phone : '.$vendorDetail['mobile'].' Email : '.$vendorDetail['email'].'</td>
					</tr>
					<tr>
						<th></th>
						<td></td>
					</tr>
				</table>
				</td>
				<td class="right">
				<table style="width:100%;">
					<tr>
						<th align="left">Invoice No</th>
						<td>'.$quoteInfo['transaction_no'].'</td>
					</tr>
					<tr>
						<th align="left">Invoice Date</th>
						<td>'.$transaction_date.'</td>
					</tr>
					<tr>
						<th align="left">Reference</th>
						<td>'.$quoteInfo['reference_no'].'</td>
					</tr>
				</table>
				</td>
			</tr>
		</table>';
			$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
			$str.="<table class='listing' style='width:100%;'><thead>
					<tr>
						<th align='left' >Description</th>
						<th align='left'>Quantity</th>
						<th align='left'>Unit Price</th>
						<th align='left'>Amount</th>
					</tr></thead><tbody>";
				$i= 1;
				foreach ($productDetails as $value) {
				$productName = $this->model_transaction_quotation->getProductName($value['product_id']);

					$str.="<tr style='border-bottom: 2px solid black;'>
		   					<td>".$productName."</td>
							<td>".$value['quantity']."</td>
							<td>".$value['price']."</td>
							<td class='right'>".$value['net_price']."</td>
						   </tr>";	
				$i++; }
				$str.="<tr><th colspan='2'></th><td align='right'>Subtotal</td>
						 <td class='right'>".number_format($quoteInfo['sub_total'],2)."</td>
					   </tr>";
				$str.="<tr><th colspan='2'></th><td align='right'>Discount</td>
						 <td class='right'>".number_format($quoteInfo['discount'],2)."</td>
					   </tr>";
				$str.="<tr><th colspan='2'></th><td align='right'>Delivery Fees</td>
						 <td class='right'>".number_format($quoteInfo['handling_fee'],2)."</td>
					   </tr>"; 
				$str.="<tr>
						<th colspan='2'></th><td align='right'>GST ".$tax_type."</td>
						 <td class='right'>".number_format($quoteInfo['gst'],2)."</td>
					   </tr>";
				$str.='<tr class="last">
						 <td align="left" colspan="2">
						 	<table class="listings"  style="width:100%;">
								<tr>
									<td align="left" style="border-right:1px solid black;padding:20px;position: relative;bottom: 20px;right: 20px;">Delivery Date:<br></td>
									<td align="left" style="padding:20px;bottom: 20px;right: 20px;">Remarks:<br></td>
								</tr>
							</table>
						 </td>
						 <td align="right">Total</td>
						 <td class="right">'.number_format($quoteInfo['total'],2).'</td>
					   </tr>';

				$str .= "</tbody></table>";
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				
				$str .= "<p style='font-size:12px;text-align: justify;'>".$company_details['term1']."</p>";
				$str .= "<p style='font-size:12px;text-align: justify;'>".$company_details['term2']."</p>";
				$str .= "<p style='font-size:14px;font-weight: bold;text-align: justify;'><bold>".$company_details['term3']."</bold></p>";
		
		// echo $str; die;
		if($str){
			$filename = 'Quotation_invoice_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function ajaxUpdateqtyAndPrice(){

		$this->load->model('ajax/cart');
		$apply_tax_type = 0;
		$sku 	   		= $this->request->post['sku']; 
		$productQty		= $this->request->post['quantity'];
		$price     		= $this->request->post['price'];		
		$tax_class_id	= $this->request->post['tax_class_id'];

		$netPrice  		  = $productQty * $price;
		$data['netPrice'] = $netPrice;
		$data['price']    = $price;
		$data['total']    = $total;

		$netPrice  = $this->currency->format($netPrice);
		$total     = $this->currency->format($total);

		$cart_type = 'cart_purchase';
 		$productId = $this->request->post['product_id'];
 		if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 )) {
			$this->cart->update($productId, $productQty, $cart_type, $this->request->post);
		}

 		$apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
 		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
 		$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
 		// printArray($total_data); die;

 			foreach($total_data as $totals) {
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				if($tax_class_id=='1'){
					$res['tax'] 	  ='';
					$res['tax_value'] ='';
				}

				if($apply_tax_type == '2'){
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
			}

			$data = array('qty'		=> $productQty, 
				'price'		=> $price, 
				'netPrice'	=> $netPrice, 
				'total'		=> $total, 
				'discnt'	=> $discnt,
				'sku'		=> $productId,
				'orderSubTotal'	=> $res['sub_total'],
				'orderTax'		=> $res['tax'],
				'ordertax_value'=> $res['tax_value'],
				'tax_type'		=> $apply_tax_type,
				'orderDiscount'	=> $res['discount'],
				'orderTotal'	=> $res['total'],
				'tax_str' 		=> '<tr id="TRtax">
	                <td align="right" class="purchase_total_left" id="tax_title" colspan="9"></td>
	                <td class="purchase_total_right insertproduct">
	                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$res['tax'].'" id="tax" name="tax" style="text-align:right;">
	                </td>
	            </tr>');
		$this->response->setOutput(json_encode($data));	
	}

	public function bulk_delete(){
		$this->load->model('transaction/quotation');
		foreach ($this->request->post['selected'] as $purchase_id) {
		}
		$this->model_transaction_quotation->deletePurchase($this->request->get['purchase_id']);

		$this->session->data['success'] = 'Success: Quotation Details Deleted';
		$url = '';
		$filter_from_date = date('d/m/Y');
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->redirect($this->url->link('transaction/quotation', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	}
	public function getOneCartDetail(){	
 		$pro = $this->cart->getProducts('cart_purchase');
 		foreach ($pro as $value) {
	 		$data[] = array('id'=>$value['product_id'],'code'=>$value['sku']);
 		}
		$this->response->setOutput(json_encode($data));
	}
	public function checkCompanyCurrency(){
		$this->load->model('transaction/quotation');
		
		$data['currency'] = $this->request->post['currency'];
		$details = $this->model_transaction_quotation->checkCompanyCurrency($data);
		if(!empty($details)){
			$response = 1;
		}else{
			$response = 0;
		}
		$this->response->setOutput(json_encode($response));
	}
	public function getCustomerCurrency(){
		$this->load->model('transaction/quotation');

		$currency_code	= $this->model_transaction_quotation->getVendorCurrency($this->request->post['vendor']);
		$this->response->setOutput($currency_code);
	}
	public function getShippingAddress(){
		$this->load->model('transaction/quotation');
		if($this->request->post['vendor']!=''){
			$shipping_code	= $this->model_transaction_quotation->getShippingAddress($this->request->post['vendor']);
		}
		if(count($shipping_code)> 0){
			foreach ($shipping_code as $value) {
				$select = '';
				if($value['id'] == $this->request->post['shipping_id']){
					$select = ' selected';
				}
				$str .="<option value='".$value['id']."' ".$select.">".$value['address1'].' '.$value['address2'].' '.$value['zip'].' '.$value['country']."</option>"; 
			}
		}
		$this->response->setOutput($str);
	}
	public function getCustomerTerm(){
		$this->load->model('transaction/quotation');
		$vendor = $this->request->post['vendor'];
		$data['term']	= $this->model_transaction_quotation->getCustomerTerm($vendor);
		$data['status'] = $data['term'] !='0' ? 1 : 0;
		$this->response->setOutput(json_encode($data));
	}
}
?>