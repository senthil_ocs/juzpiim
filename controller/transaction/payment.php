<?php
class ControllerTransactionPayment extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		// $this->clearPurchaseData();
		$this->getList();
	}
	public function insert() {	
		$this->load->model('transaction/sales');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$paymentids = $this->model_transaction_sales->updatePaymentStatus($this->request->post);

			foreach ($paymentids as $key => $value) {
				$this->sendReceiptToCustomer($value);
			}
			$this->session->data['success'] = 'Success: Payment status update successfully';
			$this->redirect($this->url->link('transaction/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->getForm();
	}
	public function updateInvoiceHeaderOnly() {	
		$this->load->model('transaction/sales');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$invoice_no = $this->model_transaction_sales->updateInvoiceHeaderOnly($this->request->post);
			$this->session->data['success'] = 'Success: Payment status update successfully';
		
		if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime('-1 day'));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
		}
		$url .= '&filter_to_date=' . $filter_to_date;
		
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_network'][0])) {
			$filter_location = $_REQUEST['filter_network'][0];
		}
		else if (isset($_REQUEST['filter_network'][1])) {
			$filter_location = $_REQUEST['filter_network'][1];
		}
		$url .= '&filter_location=' . $filter_location;
		
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_customer = null;
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$url .= '&filter_customer=' . $filter_customer;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_network = array();
		if (isset($this->request->post['filter_network'])) {
			$filter_network = $this->request->post['filter_network'];
		}else if (isset($this->request->get['filter_network'])) {
			$filter_network = $this->request->get['filter_network'];
		}else{
			$networkids = $this->cart->getChannelsList();
			$selectAll[] = $filter_location;
			foreach ($networkids as $value) {
				$selectAll[] = $value['id'];
			}
			$filter_network = $selectAll;
			array_push($filter_network, '0');
		}
		if(!empty($filter_network)){
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
        if(!$page){
			$page = 1;
		}
		$this->redirect($this->url->link('transaction/payment', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
	protected function getList() {

		$this->language->load('transaction/purchase');
		$this->document->setTitle('Sales Payment');
		$this->load->model('transaction/sales');
		//language assign code for transaction purchase
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('Sales');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->post['filter_from_date'])) {
			$filter_from_date = $this->request->post['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->post['filter_to_date'])) {
			$filter_to_date = $this->request->post['filter_to_date'];
		}else if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
		}
		$url .= '&filter_to_date=' . $filter_to_date;
		
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_customer = null;
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$url .= '&filter_customer=' . $filter_customer;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->post['filter_payment_status'])) {
			$filter_payment_status = $this->request->post['filter_payment_status'];
		}else if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}

		$filter_xero = null;
		if (isset($this->request->post['filter_xero'])) {
			$filter_xero = $this->request->post['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}else if (isset($this->request->get['filter_xero'])) {
			$filter_xero = $this->request->get['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		if (isset($this->request->get['page'])) {
			// $url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Payment List',
			'href'      => $this->url->link('transaction/payment','token='.$this->session->data['token'],'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['back_button'] = $this->url->link('transaction/sales_invoice/sales_payment', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['purchases'] = array();
		$data = array(
			'filter_from_date'	  	 => $filter_from_date,
			'filter_to_date'	  	 => $filter_to_date,
			'filter_location'	  	 => $filter_location,
			'filter_transaction'  	 => $filter_transaction,
			'filter_delivery_status' => $filter_delivery_status,
			'filter_payment_status'  => $filter_payment_status,
			'filter_network'    	 => $filter_network,
			'filter_customer'   	 => $filter_customer,
			'start'       			 => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			 => $this->config->get('config_admin_limit')
		);
		// printArray($data); die;
		$this->data['payment_insert'] = $this->url->link('transaction/payment/addPayment', 'token=' . $this->session->data['token'].$url, 'SSL');
		$this->data['filters']  = $data;
		$Sales_total            = $this->model_transaction_sales->getTotalPaymentCount($data);
		$results                = $this->model_transaction_sales->getTotalPayment($data);
		$this->data['networks'] = $this->cart->getChannelsList();
		$this->data['customers']= $this->model_transaction_sales->getB2BCustomers();

		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$vendorDetail     = array();
		$userDetail       = array();
		foreach ($results as $result) {
			$vendorDetail = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_id']);
			$salesHeader  = $this->model_transaction_sales->getSalesInvoiceByinvoice($result['order_id']);
			$xero_post    = true;
            if($salesHeader['network_id'] != '0'){
            	$xero_post = $this->cart->checkNetworksXero($salesHeader['network_id']);//check postable or not
            }
			$this->data['purchases'][] = array(
				'pagecnt'      		=> ($page-1)*$this->config->get('config_admin_limit'),
				'invoice_no'        => $result['order_id'],
				'order_id'          => $result['order_id'],
				'payment_id'        => $result['id'],
				'transaction_date'  => date('d/m/Y',strtotime($result['payment_date'])),
				'delivery_status'   => $salesHeader['delivery_status'],
				'payment_status'    => $result['payment_status'],
				'vendor_code'       => $vendorDetail['customercode'],
				'location'          => $salesHeader['location_code'],
				'vendor_name'       => $vendorDetail['name'],
				'total'             => $result['amount'],
				'xero_post'         => $xero_post,
				'xero_response'     => $result['xero_response'],
				'view_button'       => $this->url->link('transaction/payment/view', 'token=' . $this->session->data['token'] . '&purchase_id='.$result['order_id'].'&type=payment_view&payment_id='.$result['id']. $url, 'SSL'),
			);
		}
		
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $Sales_total;
		$pagination->page  = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text  = $this->language->get('text_pagination');
		$pagination->url   = $this->url->link('transaction/payment', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'transaction/payment_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}

	public function view(){
		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle($this->language->get('Sales View'));
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$url = '';
		if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_customer = null;
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$url .= '&filter_customer=' . $filter_customer;
		}
		$filter_delivery_status = array();
		if (isset($this->request->post['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->post['filter_delivery_status'];
		}else if (isset($this->request->get['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->get['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->post['filter_payment_status'])) {
			$filter_payment_status = $this->request->post['filter_payment_status'];
		}else if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Payment List',
			'href'      => $this->url->link('transaction/payment','token='.$this->session->data['token'],'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Payment View',
			'href'      => $this->url->link('transaction/payment/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 	 = $this->url->link('transaction/payment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['action'] 		 = $this->url->link('transaction/payment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo']  = $this->model_transaction_sales->getSalesInvoice($this->request->get['purchase_id']);
		// printArray($this->data['purchaseInfo']); die;
		$this->data['paymentTypes']  = $this->model_transaction_sales->getPaymentTypes();
		$this->data['vendorDetail']  = $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);
		$this->data['userDetail'] 	 = $this->model_user_user->getUser($this->data['purchaseInfo']['createdby']);
		$this->data['productDetails']= $this->model_transaction_sales->getSalesInvoiceDetails($this->data['purchaseInfo']['invoice_no']);
		$this->data['paymentDetails']= $this->model_transaction_sales->getPaymentDetailsView($this->request->get['payment_id']);
		// printArray($this->data['paymentDetails']); die;
		$this->data['download_button'] = $this->url->link('transaction/sales_invoice/download_do_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');
		$this->data['action'] = $this->url->link('transaction/payment/updateInvoiceHeaderOnly', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->template = 'transaction/payment_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function addPayment(){

		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->document->setTitle($this->language->get('Payment Form'));
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';
		if (isset($this->request->post['filter_customer'])) {
			$this->data['filter_customer'] = $this->request->post['filter_customer'];
		}
		if (isset($this->request->post['filter_invoice_date'])) {
			$this->data['filter_invoice_date']  = $this->request->post['filter_invoice_date'];
		}
		if (isset($this->request->post['filter_network'])) {
			$this->data['filter_network']  = $this->request->post['filter_network'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Payment List',
			'href'      => $this->url->link('transaction/payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'New Paymentt',
			'href'      => $this->url->link('transaction/payment/addPayment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['networks']  = $this->cart->getChannelsList();
		$this->data['locations'] = $this->cart->getLocation();
		$this->data['customers'] = $this->model_transaction_sales->getB2BCustomers();
		$this->data['payments']  = $this->model_transaction_sales->getPaymentTypes();
		$results                 = $this->model_transaction_sales->getUnpaidOrders($this->data);

		foreach ($results as $result) {
			$customer = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
			$orders[] = array(
				'invoice_no'       => $result['invoice_no'],
				'cust_name'        => $customer['name'],
				'invoice_date'     => date('d/m/Y',strtotime($result['invoice_date'])),
				'customer_code'    => $result['name'],
				'network_order_id' => $result['network_order_id'],
				'net_total'    	   => $result['net_total'] - $result['paid_amount'],
				'network_id'       => $result['network_id']
			);
		}
		$this->data['orders'] = $orders;		
		$this->data['cancel'] = $this->url->link('transaction/payment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['action'] = $this->url->link('transaction/payment/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->template = 'transaction/payment_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function testEmail(){
		if($this->request->get['id']){
			$this->sendReceiptToCustomer($this->request->get['id']);
		}
	}
	
	public function sendReceiptToCustomer($id){

		$this->load->model('transaction/sales');
		$payment     = $this->model_transaction_sales->getPaymentDetailsView($id);
		$invoice_no  = $payment['order_id'];

		$salesHeader = $this->model_transaction_sales->getSalesInvoiceByinvoice($invoice_no);
		$customers 	 = $this->model_transaction_sales->getB2BCustomersbyCode($salesHeader['customer_code']);
		$company 	 = $this->model_transaction_sales->getcompanyDetails();
		$emailTemp 	 = $this->model_transaction_sales->getEmailTemplate('2');
		$dynamicTemp = $subject = $trading_name = $contact_name = '';

		if(!empty($emailTemp)){
			$trading_name = $company['name'];
			$link         = $company['web_url'];
			$email        = $company['email'];
			$phone        = $company['phone'];
			$contact_name = $customers['name'];
			$currency_code= $salesHeader['currency_code'] !='' ? $salesHeader['currency_code'] : ''; 
			$amount       = $salesHeader['net_total']; 
			$invoice_date = $salesHeader['header_remarks'] !='' ? date('d M Y',strtotime($salesHeader['header_remarks'])) : '';
			$break        = '<br>';
			$paid_amount  = $salesHeader['paid_amount'];

			$searchArr    = array('[invoice_number]','[trading_name]','[contact_name]','[currency_code]','[amount]','[invoice_date]','[break]','[link]','[email]','[phone]');
			$replaceArr  = array($invoice_no,$trading_name,$contact_name,$currency_code,$amount,$invoice_date,$break,$link,$email,$phone);
			$subject     = str_replace($searchArr, $replaceArr, $emailTemp['subject']);
			$dynamicTemp = str_replace($searchArr, $replaceArr, $emailTemp['description']);
		}
		$str = "<h5 style='font-size:25px;margin: 0px 25%;'>".$subject."</h5><br>
					<table style='width:100%;font-size:16px;'>
					<tr>
						<td align='center'><b>AMOUNT PAID</b></td>
						<td align='center'><b>DATE PAID</b></td>
						<td align='center'><b>PAYMENT METHOD</b></td>
					</tr>
					<tr>
						<td align='center'>".$payment['amount']."</td>
						<td align='center'>".date('d/m/Y',strtotime($payment['payment_date']))."</td>
						<td align='center'>".$payment['payment_method']."</td>
					</tr>
				</table>";
		$str .= "<br><br><h4 style='margin: 0 10%;font-size: 16px;'>SUMMARY<h4><br>";
		$str .= "<table style='width:100%;font-size:16px;background-color: #f5f9fc;'>
					<tr style=''>
						<td style='padding: 10px 10%;'>Payment for invoice(s)<br>SI32433425</td>
						<td align='left'>".$currency_code." ".$amount."</td>
					</tr>
					<tr style='border-top: 1px solid #e8ecef;'>
						<td style='border-top: 1px solid #e8ecef;padding: 10px 10%;font-size:17px;'><b>Amount Charged<b></td>
						<td align='left' style='border-top: 1px solid #e8ecef;font-size:17px;'><b>".$currency_code." ".$paid_amount."<b></td>
					</tr>
				</table><br><hr><br><p style='margin: 0 10%;'>".$dynamicTemp."</p>";

    	$postArr['Content'] = $str;
    	$postArr['Subject'] = $subject;
    	$postArr['Name'] 	= $customers['name'];
    	$postArr['Email']  	= $customers['email'];
    	$postArr['Type']  	= 'payments';

    	if($customers['email']!='' && $dynamicTemp!='' && SEND_EMAIL_TO_CUSTOMER){
			$this->mail->sendEmail($postArr);
    	}
		return true;
	}
	public function cancelPayment(){
		$this->load->model('transaction/sales');

		if($this->request->post['payment_id'] !='' ){
			$this->model_transaction_sales->cancelPayment($this->request->post['payment_id']);
		}
	}
}
?>