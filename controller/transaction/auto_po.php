<?php
class ControllerTransactionAutopo extends Controller {
	private $error = array();
	
	public function index(){
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('auto_po_title'));
		$this->load->model('transaction/purchase');
		$this->data['filter_fromdate']  = $this->request->post['filter_from_date'];
		$this->data['filter_todate']    = $this->request->post['filter_to_date'];

		$this->data['filter_fromdate1'] = date('Y-d-m',strtotime($this->data['filter_fromdate']));
		$this->data['filter_todate1'] = date('Y-d-m',strtotime($this->data['filter_todate']));

		$this->data['filter_department']= $this->request->post['filter_department'];
		$this->data['filter_supplier']  = $this->request->post['filter_supplier'];

       
		$this->data['autopo_vendor'] = $this->model_transaction_purchase->getVendors();
		$this->data['po_request_header'] = $this->model_transaction_purchase->getpo_req_header($this->data);

          
         //($this->data['po_request_header']);exit;
          //print_r($this->data['filter_todate']);exit;


		//print_r($data);exit;

		$this->data['edit_url'] = $this->url->link('transaction/auto_po/view_auto_po', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = count($autopo_vendor);
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/auto_po', 'token='.$this->session->data['token'].$strUrl.$url.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $filter_location_code;

		$this->template = 'transaction/auto_po.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function view_auto_po(){

		$this->language->load('transaction/purchase');
		$this->load->model('transaction/purchase');
		$this->data['token'] = $this->session->data['token'];

		$po_req_detail = $this->model_transaction_purchase->get_po_req_detail($this->request->get['po_reqno']);
		$this->data['po_req_detail'] = $po_req_detail['details'];
		$this->data['po_req_header'] = $po_req_detail['header'][0];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Auto po List',
			'href'      => $this->url->link('transaction/auto_po', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('transaction/auto_po', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->template = 'transaction/view_auto_po.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	
	}
	public function update_auto_po(){
		$this->language->load('transaction/purchase');
		$this->load->model('transaction/purchase');
		$data['sku'] = $this->request->post['sku'];
		$data['qty'] = $this->request->post['qty'];
		$data['gst'] = $this->request->post['gst'];
		$data['skucost'] = $this->request->post['skucost'];

		if($data['sku'] !='' && $data['qty'] !=''){

			$data['subtotal'] = $data['qty'] * $data['skucost'];
			$data['nettotal'] = $data['subtotal']+$data['gst'];
			
			$this->model_transaction_purchase->update_autopo_details($data);
			$status = 1;
		}else{
			$status = 0;
		}
		return $status;
	}
}
?>