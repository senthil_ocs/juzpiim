<?php
class ControllerTransactionStockTransferOut extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_transfer_out');
		$this->clearPurchaseData();
		$this->getList();
	}
	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_transfer_out');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			    //$this->clearPurchaseData($remove = 'discount');
			}
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			$this->response->setOutput(json_encode($res));
		}
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_transfer_out');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					if($this->request->post['discount']=='')
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="9" align="right" class="purchase_total_left">'.$lable.'</td><td class="purchase_total_right">
							          	<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="input-text row_total_box"
							          	readonly="readonly" /></td></tr>';
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
			}
			$this->response->setOutput(json_encode($res));
	}

	public function loadpurchaseItems(){
		$location_codeAry = explode("|", $_POST['location_code']);
		$location_code = $location_codeAry[1];
		$ref_no='';
		if(isset($_POST['ref_no'])){
			$ref_no = $_POST['ref_no'];
		}
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/stock_transfer_out');
		//$location_code = 'SK308';
		$ProductDetails = $this->model_transaction_stock_transfer_out->getProductsbyLocation($location_code,$ref_no);
		if(count($ProductDetails)>=1){
			 $this->clearPurchaseData();
			 foreach ($ProductDetails as $product) {
			 	   $this->request->post['product_id'] = $product['product_id'];
			 	   $this->request->post['quantity']   = $product['quantity'] + $product['foc_qty']; 
			 	   $this->request->post['price']      = $product['price'];
			 	   $this->request->post['sku']        = $product['sku'];
			 	   $this->request->post['name']       = $product['name'];

			 	   $productId	 = $this->request->post['product_id'];
				   $productQty	 = $this->request->post['quantity'];
				   $productPrice = $this->request->post['price'];
				   $this->request->post['net_price'] = ($productPrice * $productQty);
				   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			 }
			$res['success'] ='true';
			$res['locationStr'] =$_POST['location_code'];
		}else{
			$this->clearPurchaseData();
			$res['success'] ='true';
			$res['locationStr'] =$_POST['location_code'];
		}
		$this->response->setOutput(json_encode($res));
	}
	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type);
		//$apply_tax_type       	= $this->config->get('config_apply_tax');
		$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');

		if (!empty($cartPurchaseDetails)) {
				$str ='';
				$i=1;
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = round($purchase['total'],2);
					} else {
					    $totalValue = round($purchase['tax'] + $purchase['total'],2);
					}
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					    <td class="center">'.$i++.'</td>	
					   <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td align="right">'.$purchase['quantity'].'</td>
	                   <td align="right">'.$purchase['price'].'</td>
	                   <td align="right">'.$purchase['raw_cost'].'</td>
	                   <td align="right">'.$this->currency->format($totalValue).'</td>';
		            
				$str.='<td align="center" style="text-align:center;"> [ <a onclick="removeProductData('.$pid.');"> Remove </a> ] </td>
                                                </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection();
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$cnt = count($data['totals'])-1;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						/*$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="9">GST </td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax">
	                                                </td>
                                                </tr>';*/
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	
	public function insert() {

        $cart_type = 'cart_purchase';

		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_transfer_out');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm()) {
			/*	printArray($this->request->post);die;*/
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			
			if (isset($this->request->post['from_location_id'])) {
				$data['from_location_id'] = $this->request->post['from_location_id'];
			}  else {
				$data['from_location_id'] = '';
			}
			if (isset($this->request->post['to_location_id'])) {
				$data['to_location_id'] = $this->request->post['to_location_id'];
			}else if(isset($this->request->get['lc'])){
				$data['to_location_id'] = $this->request->get['lc'];
			}else {
				$data['to_location_id'] = '';
			}

			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}

			if(isset($this->request->post['location_fill'])) {
				$data['location_fill']	=  $this->request->post['location_fill'];
			} else {
				$data['location_fill']	= '';
			}
			$product_data = array();


			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;

			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
		    //echo 'here'; exit;
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			
			$data['transaction_no'] = $this->getTransNo();

			if( $data['hold']=='1'){
				$this->model_transaction_stock_transfer_out->addHoldPurchase($data,$costMethod);	
			}else{
			    $pid = $this->model_transaction_stock_transfer_out->addPurchase($data,$costMethod); 			   
		    }
		   

			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
		/*if($this->rules->getAccessConditionByRule('product')) {
			$this->getForm();
		} else {
			$this->error['warning']	= 'Your domain have not product(s).';
			$this->redirect($this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}*/
		$this->getForm();
	}

	public function generatepdf($strPurchaseId){
		
		//$purchase_bill=$this->generatePdfFile($strPurchaseId);
		return $purchase_bill;
	}
	
	public function generatePdfFile($purchaseId){
		$this->load->model('setting/company');
		$this->data['company_details'] = $this->model_setting_company->getCompanyDetails($this->session->data['company_id']);
		
		$companyInfo	= $this->getCompanyAddress($this->data['company_details']);

		$data = array(
					'location_code' => $this->data['company_details']['location_code'],
					'transfer_no' =>$purchaseId,
				);
		$headerDetails = $api->getTransferHeader($data);
		$itemDetails   = $api->getTransferDetails($data);
		$html = '<html>
            	 <body>
                   <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:14px;">
                		<tr>
                            <td colspan="4">
                                <h4><strong><center>'.$companyInfo['name'].'</center></strong></h4>
                                <p>'.$companyInfo['address1'].', '.$companyInfo['address2'].','.$companyInfo['city'].'-('.$companyInfo['postal_code'].')</p>
                                <p>Ph:'.$companyInfo['phone'].'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fax:'.$companyInfo['fax'].'</p>
                                <p>Email:'.$companyInfo['email'].'</p><p></p>
                                <p>Web:'.$companyInfo['web_url'].'</p><p></p>
                                <p>GST REG NO:'.$companyInfo['business_reg_no'].'</p>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>Transaction No :</td>
                        	<td>'.$purchaseId.'</td>
                        	<td>Date :</td>
                        	<td>'.date('d/m/Y').'</td>
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>From Location :</td>
                        	<td>'.$headerDetails[0]['transfer_from_location'].'</td> 
                        </tr>
                         <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>To Location :</td>
                        	<td>'.$headerDetails[0]['transfer_to_location'].'</td>                        	
                        </tr>
                        <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>User :</td>
                        	<td>'.$headerDetails[0]['createdby'].'</td>                        	
                        </tr>
                        <tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                        <tr><td colspan="4" style="margin-top:10px;float:left;"></td></tr>
                        <tr>
                        	<td>S.No</td>
                        	<td colspan="2">Item Description</td>
                        	<td>Qty</td>                        	
                        </tr>';

                    for($i=0;$i<count($itemDetails);$i++){

                    $html.='<tr><td colspan="4" style="margin-top:10px;float:left;"> </td></tr>
                    		<tr>
	                    		<td>'.($i+1).'</td>
	                        	<td colspan="2">'.$itemDetails[$i]['sku_description'].'</td>
	                        	<td>'.$itemDetails[$i]['sku_qty'].'</td>                        	
                        	</tr>';  	
                      }
                   $html.='<tr>
                            <td colspan="4" style="border-bottom:1px solid #000;"></td>
                        </tr>
                        <tr><td colspan="4" style="margin-top:30px;float:left;"></td></tr>
                        <tr>
                        	<td colspan="2" align="left">Signature</td>
                        	<td colspan="2" align="right">'.date('d/m/Y h:i:s').'</td>
                        </tr>
              	   </table>
            	 </body>
        </html>';

        return $html;
/*        $strFile = 'receipt_'.$purchaseId.'.pdf';
        $filename = DIR_SERVER.'receipt_pdf/'.$strFile;
        include(DIR_SERVER.'MPDF/mpdf.php'); 
        $mpdf=new mPDF('c','A4');
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html,2);
        $mpdf->Output($filename,'D'); */
    }

    public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']);
			$company_address['state']	 = strtoupper($company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
			$company_address['postal_code'] = $company_details['postal_code'];
			$company_address['email'] = $company_details['email'];
			$company_address['web_url'] = $company_details['web_url'];
			$company_address['business_reg_no'] = $company_details['business_reg_no'];
			
		}
		return $company_address;
	}
	public function update() {

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/stock_transfer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/stock_transfer_out');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='update')) {

			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if (isset($this->request->post['from_location_id'])) {
				$data['from_location_id'] = $this->request->post['from_location_id'];
			}  else {
				$data['from_location_id'] = '';
			}
			if (isset($this->request->post['to_location_id'])) {
				$data['to_location_id'] = $this->request->post['to_location_id'];
			} else {
				$data['to_location_id'] = '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection();
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			
			$this->model_transaction_stock_transfer_out->editPurchase($data,$costMethod);
			
			$this->session->data['success'] = $this->language->get('purchase_text_success');
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			exit;
		}
		//printArray($this->request->post);exit;

		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['heading_title'] = 'Stock Transfer Out';
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		$page = $_REQUEST['page'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->load->model('setting/company');
		$this->data['company_details'] = $this->model_setting_company->getCompanyDetails($this->session->data['company_id']);
		$this->data['location_details'] = $this->model_transaction_stock_transfer_out->getLocationsDetailsByCode($this->session->data['location_code']);



		if($this->request->get['type'] == 'transferin') {
			$this->data['listing_title'] = 'Stock Transfer List - Transfer In';
			$listtype = 'to_location';
			$this->data['listing_type'] = $this->request->get['type'];
		} else {
			$this->data['listing_title'] = 'Stock Transfer List - Transfer Out';
			$listtype = 'from_location';
			$this->data['listing_type'] = '';
		}

		if($show_hold=='1'){
			$this->data['heading_title'] = 'Stock Transfer Out Hold Items List';
		}

		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->data['listing_title'],
			'href'      => $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);




		$pageUrl ='';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.='&filter_date_to='.$filter_date_to; 
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_from_location'])) {
			$filter_from_location = $_REQUEST['filter_from_location'];
			$pageUrl.= '&filter_from_location='.$filter_from_location;
		} else {
			$filter_from_location = null;
		}
		if (isset($_REQUEST['filter_to_location'])) {
			$filter_to_location = $_REQUEST['filter_to_location'];
			$pageUrl.= '&filter_to_location='.$filter_to_location;
		} else {
			$filter_to_location = null;
		}
		if (isset($_REQUEST['filter_accepted_status'])) {
			$filter_accepted_status = $_REQUEST['filter_accepted_status'];
			$pageUrl.= '&filter_accepted_status='.$filter_accepted_status;
		} else {
			$filter_accepted_status = 'No';
			$_REQUEST['filter_accepted_status']='No';
		}

		$strUrl = '&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to.'&filter_from_location='.$filter_from_location.'&filter_accepted_status='.$filter_accepted_status;
		$hold_button = '&show_hold=1';
		$this->data['insert'] = $this->url->link('transaction/stock_transfer_out/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['show_hold_button'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] .$url, 'SSL');

		$this->data['stocktransferin'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token']. $url.'&type=transferin', 'SSL');
		$this->data['stocktransferout'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token']. $url, 'SSL');
		$this->load->model('setting/location');
		
		 //$filter_date_to= date('Y-m-d', strtotime($filter_date_to. '+ 1 days')); 

 		$this->data['purchases'] = array();
		$data = array(
			'hold_status'		=> $show_hold,
			'location_id' 		=> $this->data['location_details']['location_id'],
			'location_code' 	=> $this->data['location_details']['location_code'],
			'list_type'   		=> $listtype,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_from_location'	=> $filter_from_location,
			'filter_to_location'	=> $filter_to_location,
			'filter_accepted_status'=> $filter_accepted_status,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);

		
		 //$filter_date_to= date('Y-m-d', strtotime($filter_date_to. '- 1 days')); 
		$purchase_total = $this->model_transaction_stock_transfer_out->getTotalPurchase($data);
		//$purchase_total = $api->getmyTotalPurchase($data);
		$results = $this->model_transaction_stock_transfer_out->getPurchaseList($data);
		//$results = $api->getmyPurchaseList($data);
		$this->load->model('setting/company');
		$this->data['Tolocations']= $this->model_setting_location->getLocationsCode();
        if(!$page){
			$page = 1;
		}
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		foreach ($results as $result) {
			//printArray($result);
			$reference_date ='';
			if($result['reference_date']!=''){
				$reference_date = $result['reference_date'];
			}
			if($result['accepted_status']=='1'){
				$accepted_status = 'Yes';
			}else{
				$accepted_status = 'No';
			}

			if($result['revoke_status']=='1'){
				$revoke_status = 'Yes';
			}else{
				$revoke_status = 'No';
			}
			if($result['hold_status']=='1'){
				$hold_status = 'Yes';
			}else{
				$hold_status = 'No';
			}
			
			$this->data['purchases'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'),
				'transaction_no'    => $result['transfer_no'],
				'transaction_date'  => $result['transfer_date'],
				'total'             => $result['transfer_value'],
				'created_by'        => $result['createdby'],
				'reference_no'  	=> $result['reference_no'],
				'reference_date'    => $reference_date,
				'created_date'      => $result['createdon'],
				'modify_button' => $this->url->link('transaction/stock_transfer_out/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'view_button' => $this->url->link('transaction/stock_transfer_out/view','token=' . $this->session->data['token'] .'&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'revoke_button' => $this->url->link('transaction/stock_transfer_out/revoke', 'token=' . $this->session->data['token'] .'&purchase_id=' . $result['transfer_no'] . $url, 'SSL'),
				'from_location' => $result['transfer_from_location'],
				'to_location'   => $result['transfer_to_location'],
				'accepted_status'=>$accepted_status,
				'revoke_status'=>$revoke_status,
				'hold_status'=>$hold_status
			);
		}

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] .$strUrl. $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->data['hold'] = $show_hold;

		$this->template = 'transaction/stock_transferout_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
		//if (isset($this->request->get['successPid']) && $this->request->get['successPid']!='') {
		//	$this->generatePdfFile($this->request->get['successPid']);
		//}
	}
	protected function getForm() {
	
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/company');
		$this->load->model('setting/location');

		$this->data['heading_title'] = 'Insert form';
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_from_location'] = $this->language->get('entry_from_location');
		$this->data['entry_to_location'] = $this->language->get('entry_to_location');
		$this->data['entry_from_address'] = $this->language->get('entry_from_address');
		$this->data['entry_to_address'] = $this->language->get('entry_to_address');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_raw_cost'] = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		
		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_stock_transfer_out->getHoldHeaderbyId($this->request->get['purchase_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			   $purchaseProducts = $this->model_transaction_stock_transfer_out->getHoldItemdetailsByTransNo($purchaseInfo['transaction_no']);

			    foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0)) {
						$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
					} elseif ($products['quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}

		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Transfer Out List',
			'href'      => $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Stock Transfer',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!empty($purchaseInfo)) {
			$this->data['action'] = $this->url->link('transaction/stock_transfer_out/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['transfer_no'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/stock_transfer_out/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
												//echo 'here'; exit;

		$this->data['cancel'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');

		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = str_pad($this->request->post['transaction_no'],6,"0",STR_PAD_LEFT);
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = str_pad($purchaseInfo['transaction_no'],6,"0",STR_PAD_LEFT);
		} else {
				// Ragu
			$this->data['transaction_no'] = str_pad($this->getTransNo(),6,"0",STR_PAD_LEFT);
				/*$transNumberWithSuffix = $this->model_inventory_inventory->getPurchaseAutoId();
				if($transNumberWithSuffix){
					 $transaction_no = ltrim($transNumberWithSuffix,'HQ')+1;
					 $this->data['transaction_no'] = 'HQ'.$transaction_no;
				}else{
					$transNumber = $this->model_inventory_inventory->getInventoryAutoId();
					if (empty($purchasePrefix)) {
						$this->data['transaction_no'] = 'TRANS'.$transNumber['sku'];
					} else {
					    $this->data['transaction_no'] = trim($purchasePrefix).($transNumber['sku']+1);
					}
				} */
		}

		//echo $this->data['transaction_no'];exit;

		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		
		/*if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['transaction_date'];
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}*/

		$this->data['transaction_date'] = date('d/m/Y');
		$this->data['location_fill'] = 0;

		if (isset($this->request->post['from_location_id'])) {
			$this->data['from_location_id'] = $this->request->post['from_location_id'];
		} elseif (!empty($purchaseInfo)) {
			//$this->data['from_location_id'] = $purchaseInfo['from_outlet_id'];
			$this->data['from_location_id'] = $purchaseInfo['transfer_from_location'];
		} else {
			$this->data['from_location_id'] = '';
		}
		if (isset($this->request->post['to_location_id'])) {
			$this->data['to_location_id'] = $this->request->post['to_location_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['to_location_id'] = $purchaseInfo['transfer_to_location'];
		}else if(isset($this->request->get['lc'])){
		    $this->data['to_location_id'] = $this->request->get['lc'];
		    $this->data['location_fill'] = 1;			
		} else {
			$this->data['to_location_id'] = '';
		}
		

		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}

		
		
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		/*if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = $purchaseInfo['reference_date'];
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}*/
		$this->data['reference_date'] = date('d/m/Y');
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		if (isset($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}


		//product collection
		$this->data['products'] = array();
		
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		//if ($this->session->data['vendor']) {
		    $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		//}
		
		


		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		$this->data['filter_ref_details'] = array();
		if(count($this->data['cartPurchaseDetails'])>=1){
			// load purchase ref no's in drop
			$location_code  = $this->request->get['lc'];
			$refDetails     = $this->model_transaction_stock_transfer_out->getRefNosbyLocation($location_code);
			if(count($location_code)>=1){
				$this->data['filter_ref_details'] = $refDetails;
			}
		}
		//printArray($this->data);
		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax_purchase');
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();

		//printArray($this->data['purchase_totals']);
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		
		//$this->data['Tolocations'] = $api->getLocationsFromHQ();
		$this->data['Tolocations'] = $this->model_setting_location->getLocationsCode();

		$this->data['company_details'] = $this->model_setting_company->getCompanyDetails($this->session->data['company_id']);
		$this->data['location_details'] = $this->model_transaction_stock_transfer_out->getLocationsDetailsByCode($this->session->data['location_code']);

		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['route'] = 'transaction/stock_transfer_out';
		
		$this->template = 'transaction/stock_transferout_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		
		/*if (empty($this->request->post['reference_date'])) {
			$this->error['reference_date'] = "Please enter reference date.";
		}
		if (empty($this->request->post['reference_no'])) {
			$this->error['reference_no'] = "Please enter reference no.";
		}*/

		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type) {
		$product_data = array();
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     =>number_format($total,2)
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data='', $type = '') {
			$this->load->model('transaction/stock_transfer_out');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_stock_transfer_out->getProductByName($this->data);
			$str = '';
			if(count($_productDetails)>=1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost']=='0.000'){
						$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'];
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/stock_transfer_out');
			$this->load->model('transaction/purchase');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['filter_location_code'] = $this->session->data['location_code'];
			$_productDetails           = $this->model_transaction_stock_transfer_out->getProductByName($this->data);
			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost']=='0.000'){
						$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['average_cost']=='0.000'){
						$_productDetails[$i]['average_cost'] =$_productDetails[$i]['price'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
				$str = $var;
			}
			echo $str;
	}
	public function getProductSkuDetails()
	{
		$this->load->model('transaction/stock_transfer_out');
		$id = $this->request->post['id'];
		$SkuProductDetails = $this->model_transaction_stock_transfer_out->getproductdetails($id);
		if($SkuProductDetails['average_cost']=='0.0000'){
					$SkuProductDetails['average_cost'] =' ';
		}
		echo json_encode($SkuProductDetails);
	}
	public function getTransNo(){
		$transNumber = $this->model_transaction_stock_transfer_out->getTransferAutoId();
		if($transNumber){
			 $data['transaction_no'] = str_pad($transNumber+1, 6, '0', STR_PAD_LEFT);
		}else{
			$transaction_no  = 1;
			$data['transaction_no'] = str_pad($transaction_no, 6, '0', STR_PAD_LEFT);
		}
		return $data['transaction_no'];
	}
	
	public function view(){
		
		$this->language->load('transaction/stock_transfer');
		$this->load->model('transaction/stock_transfer_out');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if (isset($this->request->get['purchase_id'])) {
			$purchase_id = $this->request->get['purchase_id'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transaction',
			'href'      => $this->url->link('transaction/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transfer out List',
			'href'      => $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Transfer out View Details',
			'href'      => $this->url->link('transaction/stock_transfer_out/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['revoke_button'] 	= $this->url->link('transaction/stock_transfer_out/revoke', 'token=' . $this->session->data['token'] .'&purchase_id=' . $purchase_id. $url, 'SSL');


	
		$data = array(
			'transfer_no' => $purchase_id
		);

		$this->data['purchaseInfo'] 	= $this->model_transaction_stock_transfer_out->getPurchaseDetailsById($purchase_id);
		$this->data['productDetails'] 	= $this->model_transaction_stock_transfer_out->getTransferDetails($data);

		//$headerDetails 	= $api->getTransferHeader($data);
		if($headerDetails[0]['accepted_status']=='1'){
			$headerDetails[0]['accepted_status'] = "Yes";
		}else{
			$headerDetails[0]['accepted_status'] = "No";
		}

		if($headerDetails[0]['revoke_status']=='1'){
			$headerDetails[0]['revoke_status'] = "Yes";
		}else{
			$headerDetails[0]['revoke_status'] = "No";
		}
		
		
		$this->data['purchase_bill']=$this->generatepdf($purchase_id);

		$this->template = 'transaction/stoct_transferout_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function revoke(){
		 $this->load->model('transaction/stock_transfer_out');
		 $this->load->model('setting/company');
		 $this->data['company_details'] = $this->model_setting_company->getCompanyDetails($this->session->data['company_id']);
		 $this->session->data['success'] = '';
		 
		 if(isset($this->request->get['purchase_id'])) {
				$data['purchase_id']	=  $this->request->get['purchase_id'];
				$transfer_no 			= $data['purchase_id'];
				$data = array(
					'location_code' => '',
					'transfer_no' => $data['purchase_id'],
				);
				$headerDetails = $this->model_transaction_stock_transfer_out->getPurchaseDetailsById($transfer_no);
				
				$userName	= $this->session->data['username'];
				$date       = date('Y-m-d H:i:s');
				$revoke_status = 1;

				if(count($headerDetails)>=1){
					$this->model_transaction_stock_transfer_out->updateRevokeHeader($revoke_status,$userName,$date,$transfer_no);
					$this->model_transaction_stock_transfer_out->updateItemDetails($itemDetails,$this->session->data['location_code']);
					$this->session->data['success'] = "Revoke Updated Successfull"; /*Stock Transfer accept success*/
				}else{
					$this->session->data['success'] = "Stock Transfer not accepted";
				}
		} 
		$this->redirect($this->url->link('transaction/stock_transfer_out', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	public function getAddress() {
		$this->load->model('transaction/stock_transfer_out');
		$resultAddress = $this->model_transaction_stock_transfer->getAddressForLocation($_POST);
		$address12 = implode(", ",$resultAddress);
		$address = trim($address12,',');
		echo $address;

	}
}
?>