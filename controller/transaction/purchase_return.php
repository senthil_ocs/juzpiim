<?php 
class ControllerTransactionPurchaseReturn extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('purchase_return_title'));
		$this->load->model('transaction/purchase_return');
		$this->clearPurchaseData();
		// printArray($this->session->data); die;
		$this->getList();
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/purchase_return');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {

			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);				
			}
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'tax_price'                 => $product['purchase_tax'],
					'product_id'                => $product['product_id'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'purchase_discount'         => $product['purchase_discount'],
					'weight_class_id'           => $product['weight_class_id'],
					'tax_class_id'              => $product['tax_class_id'],
					'net_price'                 => $product['sub_total'],
					'raw_cost'                  => $product['raw_cost'],
					'total'                     => $product['total'],
					'price'                     => $product['price']
				);
			}

			$data['products'] = $product_data;


			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			// printArray($res); die;
			$this->response->setOutput(json_encode($res));
		}
	}
	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		    if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];	
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(!empty($this->session->data['cart_purchase'])){
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		    }
		    $this->session->data['vendor_id'] = $this->request->post['vendor'];
		    $cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type);
		    
		    $apply_tax_type       	= $this->config->get('config_apply_tax_purchase');

		    if (!empty($cartPurchaseDetails)) {
		    	$str ='';
		    	foreach ($cartPurchaseDetails as $purchase) {
		    		$pid = $purchase['product_id'];
		    		if($apply_tax_type=='2') {
					    $totalValue = $purchase['total'];
					} else {
					    $totalValue = $purchase['tax'] + $purchase['total'];
					}
					$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center order-nopadding">
	                   		<input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   		</td>
	                   <td class="center order-nopadding">
	                   		<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace("$","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>';
	                   $str.='<td align="center" id="td_nettot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>';
					   $str.='<td align="center"> <a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
					  	</tr>';
		    	}
		    	$res['tbl_data'] = $str;
		    	$this->load->model('ajax/cart');
		    	$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                    <td align="right" class="purchase_total_left" colspan="6">GST (7%) (Excl)</td>
                                    <td class="purchase_total_right insertproduct">
                                    	<input type="text" readonly="readonly" class="textbox row_total_box text-right" value="'.$tax['text'].'" id="tax" name="tax">
                                    </td>
                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
		    }
		}	
	}
	public function insert() {
		$cart_type = 'cart_purchase';		
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Purchase Return');
		$this->load->model('transaction/purchase_return');		
		if(($this->request->server['REQUEST_METHOD']=='POST') && (isset($this->request->post['purchase_button'])) && ($this->validateForm())) {
			$data = array();			
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}

			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= '';
			}
			
			$data['vendor']	= '';
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			}
			
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			
			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '1';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			if (isset($this->request->post['currency_code'])) {
				$data['currency_code'] = $this->request->post['currency_code'];
			} else {
				$data['currency_code'] = '';
			}
			if (isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate'] = $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate'] = '';
			}
			$data['purchase_return'] = '1';
			$product_data = array();	
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'location'                  => $product['location'],
					'expiry_date'               => $product['expiry_date']
				); 
			}
			$data['products'] = $product_data;
			if(empty($data['products'])){
				$this->error['warning']	= 'Select your return Product Details.';
				$this->redirect($this->url->link('transaction/purchase_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			$total_data = array();
			$this->load->model('ajax/cart');
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] 		= $total_data;
			$data['total']  		= $total;
			$data['transaction_no']	= $this->getTransNo();
			// printArray($data); die;
			$this->model_transaction_purchase_return->addPurchase($data);
			
			$cartPurchase = array();
			$cartPurchase = $this->session->data['cart_purchase'];
			$this->session->data['success'] = 'Success: You have added purchase return!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}		
		$this->getForm();
	}
	public function update(){
		$cart_type = 'cart_purchase';		
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Purchase Return');
		$this->load->model('transaction/purchase_return');		
		if(($this->request->server['REQUEST_METHOD']=='POST') && (isset($this->request->post['purchase_button'])) && (isset($this->request->post['purchase_id'])) && ($this->validateForm())) {

			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '';
			}
			if (isset($this->request->post['remarks'])) {
				$data['remarks'] = $this->request->post['remarks'];
			} else {
				$data['remarks'] = '';
			}
			$product_data = array();
			// printArray($this->cart->getProducts($cart_type)); exit;
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'foc_quantity'              => $product['foc_quantity']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			// printArray($this->request->get); die;
			$this->model_transaction_purchase_return->editPurchase($data['purchase_id'],$data);
			$this->session->data['success'] = 'Success: You have modified purchase return!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($this->request->get['filter_date_from'])) {
				$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
			}
			if (isset($this->request->get['filter_date_to'])) {
				$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
			}
			if (isset($this->request->get['filter_supplier'])) {
				$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
			}
			if (isset($this->request->get['filter_location_code'])) {
				$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
			}
			if (isset($this->request->get['filter_transactionno'])) {
				$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
			}
			$this->redirect($this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}	
		$this->getForm();
	}
	protected function getList(){
		// echo "holding"; die;
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('purchase_return_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$pageUrl ='';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		$filter_transactionno = null;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.= '&filter_transactionno='.$filter_transactionno;
		}

		$url = '';
		if (isset($this->request->get['page'])) {
			// $url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			// $url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Purchase Return'),
			'href'      => $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['insert'] = $this->url->link('transaction/purchase_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$data = array(
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'filter_location_code' => $filter_location_code,
			'filter_supplier' 	   => $filter_supplier,
			'filter_transactionno' => $filter_transactionno,
			'sort'	      => $sort,
			'order'		  => $order,
			'hold'		  => $hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		$this->data['data'] = $data;
		$purchase_total 	= $this->model_transaction_purchase_return->getTotalPurchaseList($data);
		$results 			= $this->model_transaction_purchase_return->getPurchaseList($data);
		$this->data['suppliers'] = $this->model_transaction_purchase_return->getVendors();
		$this->data['filter_supplier'] = $filter_supplier;

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail 	= array();
		$userDetail 	= array();
		//printArray($results); exit;
		foreach ($results as $result) {			
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$userDetail = $this->model_user_user->getUser($result['created_by']);
			$purchaseDetail = $this->model_transaction_purchase_return->getPurchaseDetails($result['purchase_id']);
			
			$this->data['purchases'][] = array(
				'pagecnt'      		=> ($page-1)*$this->config->get('config_admin_limit'),
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'location_code'  	=> $result['location_code'],
				'vendor_code'       => $vendorDetail['vendor_code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'total'             => $result['total'],
				'reference_no'      => $result['reference_no'],
				'xero_purchase_id'  => $result['xero_purchase_id'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'no_item'           => $purchaseDetail['totalItem'],
				'total_qty'         => $purchaseDetail['totalQty'],
				'created_date'      => $result['created_date'],
				'modify_button' 	=> $this->url->link('transaction/purchase_return/update', 'token='.$this->session->data['token'].'&purchase_id='.$result['purchase_id'].$pageUrl, 'SSL'),
				'view_button' 		=> $this->url->link('transaction/purchase_return/view', 'token='.$this->session->data['token'].'&purchase_id='.$result['purchase_id'].$pageUrl, 'SSL')
			);
		}
		$this->data['hold_purchase'] = null;
		if($hold == null){
			$this->data['hold_purchase'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'].'&hold=1', 'SSL');
		}
		$this->data['purchase_return'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'], 'SSL');
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		
		$url.=$pageUrl;
		$pagination 		= new Pagination();
		$pagination->total 	= $purchase_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] .$strUrl. $url . '&page={page}', 'SSL');
		$this->data['pagination'] 	= $pagination->render();
		$this->data['sort'] 		= $sort;
		$this->data['order'] 		= $order;
		$this->data['route'] 		= $this->request->get['route'];
		$this->data['delete'] 		= $this->url->link('transaction/purchase_return/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		$this->data['Tolocations'] 	= $this->cart->getLocation($this->session->data['location_code']);
	    $this->data['filter_location_code'] =$filter_location_code;
	    $this->data['filter_transactionno'] =$filter_transactionno;
		
		$this->template = 'transaction/purchase_return_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		
		$this->data['heading_title'] = $this->language->get('purchase_return_title');		
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['text_return_qty'] = $this->language->get('text_return_qty');
		$this->data['button_hold'] = $this->language->get('button_hold');
				
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		if (isset($this->request->post['transaction_no'])) {
			$purchaseInfo = $this->model_transaction_purchase_return->getPurchaseByTransNo($this->request->post['transaction_no']);
			if(empty($purchaseInfo)) {
				$this->error['warnings'] = "Please enter valid transaction number.";	
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {		
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];	
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }		   
		}
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {		    
			if ($this->request->post['quantity'] <= $this->request->post['total_quantity']) {
				$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);				
				if(!empty($this->request->post['discount_percentage'])) {
					$this->data['hide_discount_percentage']	= '';
					$this->data['hide_discount_price']	= '1';
				} elseif (!empty($this->request->post['discount_price'])) {
					$this->data['hide_discount_percentage']	= '1';
					$this->data['hide_discount_price']	= '';
				}
			} else {
			    $this->error['warning'] = 'Please enter quantity below or equal '.$this->request->post['total_quantity'];
			}			
		}
		//pre fill added details 
		if(isset($this->request->get['purchase_id']) && $this->request->get['purchase_id'] !=''){
		
			$purchaseInfo   = $this->model_transaction_purchase_return->getPurchase($this->request->get['purchase_id']);
			$vendorDetails  = $this->model_transaction_purchase_return->getVendors_byId($purchaseInfo['vendor_id']);
			$apply_tax_type = $this->model_transaction_purchase_return->getVendorsTaxId($purchaseInfo['vendor_id']);
			$this->session->data['vendor_id']     = $vendorDetails['vendor_id'];
			$this->session->data['vendor_name']   = $vendorDetails['vendor_name'];
			$this->session->data['reference_num'] = $purchaseInfo['reference_no'];
			$this->data['clocation_code']  		  = $purchaseInfo['location_code'];
			$this->data['transaction_no']  		  = $purchaseInfo['transaction_no'];
			$this->data['reference_date']  		  = date('d/m/Y',strtotime($purchaseInfo['reference_date']));
			$this->data['purchase_id']   		  = $this->request->get['purchase_id'];
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				$purchaseProducts = $this->model_transaction_purchase_return->getPurchaseProduct($purchaseInfo['purchase_id']);
			 if(!empty($purchaseProducts)){
				foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					$products['foc_quantity'] = (int)$products['foc'];
					if(!empty($this->session->data['cart_purchase'])){					
						if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0 || $products['foc_quantity'] > 0)) {
							$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
						} elseif ($products['quantity'] > 0  || $products['foc_quantity'] > 0) {
							$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
						}
					} elseif ($products['quantity'] > 0  || $products['foc_quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}
				}
			 }
			}
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage; 
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price; 
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}						
		}	
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {		    
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		
		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
		
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Purchase Return'),
			'href'      => $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if(isset($this->request->get['purchase_id']) && $this->request->get['purchase_id']!='' ){
			$this->data['action'] = $this->url->link('transaction/purchase_return/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}else{
			$this->data['action'] = $this->url->link('transaction/purchase_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}

		// printArray($this->request->post); die;
		if (!empty($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();
		}
		
		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}

		if (!empty($this->request->post['ptransaction_no'])) {
			$this->data['ptransaction_no'] = $this->request->post['ptransaction_no'];
		} else {
			$this->data['ptransaction_no'] = '';
		}
		
		if (!empty($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['transaction_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		
		if (!empty($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->data['vendor_code'] = $this->request->post['vendor_code'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!empty($purchaseInfo)){
			$vendorData = $this->model_master_vendor->getVendor($purchaseInfo['vendor_id']);
			$this->data['vendor'] = $vendorData['vendor_id'];
			$this->data['vendor_code'] = $vendorData['code'];
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->data['vendor_code'] = '';
			$this->data['vendor_name'] = '';
		}
		if (!empty($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (!empty($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date('d/m/Y',strtotime($purchaseInfo['reference_date']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		
		if (!empty($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		
		if (!empty($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		
		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code']= $this->model_transaction_purchase_return->getcompanyCurrency();
		}

		if (!empty($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		
		if (!empty($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}

		if (!empty($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}
		// printArray($this->data); die;
		//product collection
		$this->load->model('inventory/inventory');
		$this->data['products'] = array();
		
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $filter_vendor = $purchaseInfo['vendor_id'];
		} else {
			$filter_vendor = null;
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {		    
		    if (!empty($this->request->post['product_id'])) {
				$productsInfo = $this->model_transaction_purchase_return->getPurchaseProduct($this->request->post['purchase_id']);
				foreach ($productsInfo as $products) {
					$products['total_quantity'] = $products['quantity'];
					$this->data['rowdata'] = array_merge($products, $this->model_transaction_purchase_return->getInventoryDetailsById($products['product_id']));	
				}
			}
		}
		
		if (!empty($purchaseInfo)) {
			if (empty($this->session->data['bill_discount'])) {
				if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
			}
			$productsInfo = $this->model_transaction_purchase_return->getPurchaseProduct($purchaseInfo['purchase_id']);
			foreach ($productsInfo as $products) {
				$this->data['products'][] = array_merge($products, $this->model_inventory_inventory->getInventoryDetailsById($products['product_id']));	
			}			
		}
		$this->load->model('setting/location');
		$this->data['clocation_code'] = $this->session->data['location_code'];
		$this->data['Tolocations'] = $this->cart->getLocation();

		//purchase cart collection
		$cart_type = 'cart_purchase';
		$this->data['product']	= $this->getPurchaseCartDetails($cart_type);


		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax');
		$this->data['currencys'] = $this->model_transaction_purchase_return->getCurrency();
		
		$this->load->model('ajax/cart');
		
		$apply_tax_type = $this->data['tax_class_id'] == '1' ? '0' : $this->data['tax_type'];
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		$this->data['allTerms'] = $this->cart->getAllTerms();
		
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['route'] = 'transaction/purchase_return';
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->template = 'transaction/purchase_return_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function getTransNo(){
		$this->load->model('inventory/inventory');
		$transNumberWithSuffix = $this->model_inventory_inventory->getPurchaseAutoId();
		$transNumber		   = $transNumberWithSuffix;
		if($transNumber){
			 $transaction_no = ltrim($transNumber)+1;
			 $this->data['transaction_no'] = $this->config->get('config_purchase_return_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_purchase_return_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}

	protected function validateForm() {
	
		if (!$this->request->post['transaction_no']) {
			$this->error['warning'] = "Please enter transaction no.";
		}
		
		if (!$this->request->post['transaction_date']) {
			$this->error['warning'] = "Please enter transaction date.";
		}
		
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one vendor.";
		}
		
		if (!$this->request->post['reference_date']) {
			$this->error['warning'] = "Please enter reference date.";
		}
		
		if (!$this->request->post['reference_no']) {
			$this->error['warning'] = "Please enter reference no.";
		}
		
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getPurchaseCartDetails($cart_type) {
		$product_data = array();
		foreach ($this->cart->getProducts($cart_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'inventory_code' 		=> $product['sku'],
				'productname'       => $product['name'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'totalValue' => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode'],
				'location'        => $product['location'],
				'expiry_date'        => $product['expiry_date']
			); 
		}
		return $product_data;
	}
	
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];								
		$taxAmount = $this->cart->getTaxeByProduct($productDetails);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;		
		$discountData = $this->getDiscountCalculation($productDetails);		
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'total_quantity'            => $productDetails['total_quantity'],
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => number_format($subTotal,2),
				'total'                     => number_format($total,2)
		);			
		return $rowData;
	}
	
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
		if(isset($this->session->data['vendor_id'])){
			unset($this->session->data['vendor_id']);
			unset($this->session->data['vendor_name']);
			unset($this->session->data['reference_num']);
		}
	}
	
	public function getDiscountCalculation($data, $type = '') {
	    
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
		
	}
	public function getProductDetailsByTransNo(){
		$this->load->model('transaction/purchase_return');
		$this->load->model('master/vendor');
        $purchaseInfo = $this->model_transaction_purchase_return->getPurchaseByTransNo($this->request->post['ptransaction_no']);
        $vendorData = $this->model_master_vendor->getVendor($purchaseInfo['vendor_id']);
		$result['purchase_id'] 		= $purchaseInfo['purchase_id'];
		$result['vendor'] 			= $vendorData['vendor_id'];
		$result['vendor_code']  	= $vendorData['vendor_code'];
		$result['vendor_name'] 		= $vendorData['vendor_name'];
        $result['transaction_date'] = $purchaseInfo['transaction_date'];
        $result['reference_no'] 	= $purchaseInfo['reference_no'];
        $result['reference_date'] 	= $purchaseInfo['reference_date'];
        $this->response->setOutput(json_encode($result));
    }
    public function getProductDetailsByProductId(){
    	$this->load->model('transaction/purchase_return');
	    if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {		    
		    if (!empty($this->request->post['product_id'])) {
				$productsInfo = $this->model_transaction_purchase_return->getPurchaseProduct($this->request->post['purchase_id'],$this->request->post['product_id']);
				foreach ($productsInfo as $products) {
					$products['total_quantity'] = $products['quantity'];
					$this->data['rowdata'] = array_merge($products, $this->model_transaction_purchase_return->getInventoryDetailsById($products['product_id']));	
				}
			}
		}
		
		$this->response->setOutput(json_encode($this->data));
	}
	public function updateRowCountData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {		    
				$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);				
				if(!empty($this->request->post['discount_percentage'])) {
					$this->data['hide_discount_percentage']	= '';
					$this->data['hide_discount_price']	= '1';
				} elseif (!empty($this->request->post['discount_price'])) {
					$this->data['hide_discount_percentage']	= '1';
					$this->data['hide_discount_price']	= '';
				}
			    $this->data['quantity']= $this->request->post['total_quantity'];
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function getAddForm() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('setting/customers');
		$this->load->model('transaction/purchase_return');
		$this->data['heading_title'] = $this->language->get('purchase_return_title');		
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['text_return_qty'] = $this->language->get('text_return_qty');
		$this->data['button_hold'] = $this->language->get('button_hold');
				
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		if (isset($this->request->post['transaction_no'])) {
			$purchaseInfo = $this->model_transaction_purchase_return->getPurchaseByTransNo($this->request->post['ptransaction_no']);
			if(empty($purchaseInfo)) {
				$this->error['warning'] = "Please enter valid transaction number.";	
			}
		}
		
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {		
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];	
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }		   
		}
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {		    
			if ($this->request->post['quantity'] <= $this->request->post['total_quantity']) {
				$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);				
				if(!empty($this->request->post['discount_percentage'])) {
					$this->data['hide_discount_percentage']	= '';
					$this->data['hide_discount_price']	= '1';
				} elseif (!empty($this->request->post['discount_price'])) {
					$this->data['hide_discount_percentage']	= '1';
					$this->data['hide_discount_price']	= '';
				}
			} else {
			    $this->error['warning'] = 'Please enter quantity below or equal '.$this->request->post['total_quantity'];
			}			
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {			
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage; 
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price; 
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}						
		}	
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {		    
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
		
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Purchase Return'),
			'href'      => $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['action'] = $this->url->link('transaction/purchase_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['cancel'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		
		if (!empty($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
			$this->data['transaction_no'] = '';
		}
		
		if (!empty($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['transaction_date'];
		} else {
			$this->data['transaction_date'] = '';
		}
		
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		
		if (!empty($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->data['vendor_code'] = $this->request->post['vendor_code'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!empty($purchaseInfo)){
			$vendorData = $this->model_master_vendor->getVendor($purchaseInfo['vendor_id']);
			$this->data['vendor'] = $vendorData['vendor_id'];
			$this->data['vendor_code'] = $vendorData['code'];
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->data['vendor_code'] = '';
			$this->data['vendor_name'] = '';
		}
		
		if (!empty($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		
		if (!empty($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = $purchaseInfo['reference_date'];
		} else {
			$this->data['reference_date'] = '';
		}
		
		if (!empty($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		
		if (!empty($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		
		if (!empty($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		
		//product collection
		$this->load->model('inventory/inventory');
		$this->data['products'] = array();
		
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $filter_vendor = $purchaseInfo['vendor_id'];
		} else {
			$filter_vendor = null;
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {		    
		    if (!empty($this->request->post['product_id'])) {
				$productsInfo = $this->model_transaction_purchase_return->getPurchaseProduct($this->request->post['purchase_id']);
				foreach ($productsInfo as $products) {
					$products['total_quantity'] = $products['quantity'];
					$this->data['rowdata'] = array_merge($products, $this->model_transaction_purchase_return->getInventoryDetailsById($products['product_id']));	
				}
			}
		}
		
		if (!empty($purchaseInfo)) {
			if (empty($this->session->data['bill_discount'])) {
				if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
			}
			$productsInfo = $this->model_transaction_purchase_return->getPurchaseProduct($purchaseInfo['purchase_id']);

			foreach ($productsInfo as $products) {
				$this->data['products'][] = array_merge($products, $this->model_inventory_inventory->getInventoryDetailsById($products['product_id']));	
			}			
		}
		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax');
		
		$this->load->model('ajax/cart');		
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		
		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['route'] = 'transaction/purchase_return';
		
		//$this->data['locations'] = $this->model_transaction_purchase_return->getCompanyLocation($_SESSION['company_id']);

		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->template = 'transaction/purchase_return_addform.tpl';
		$this->response->setOutput($this->render());
	}
	public function view(){
		$this->language->load('transaction/purchase_return');
		$this->load->model('transaction/purchase_return');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('purchase_return_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Purchase Return'),
			'href'      => $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase Return View',
			'href'      => $this->url->link('transaction/purchase_return/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_purchase_return->getPurchaseDetailsById($this->request->get['purchase_id']);
		$this->data['vendorDetail'] 	= $this->model_master_vendor->getVendor($this->data['purchaseInfo']['vendor_id']);
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['created_by']);
		$this->data['purchaseDetail'] 	= $this->model_transaction_purchase_return->getPurchaseDetails($this->data['purchaseInfo']['purchase_id']);

		$this->data['productDetails'] 	= $this->model_transaction_purchase_return->getProductDetailsById($this->data['purchaseInfo']['purchase_id']);
		//print_r($this->data['purchaseInfo']); exit;
		$this->template = 'transaction/purchase_return_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	function getPurchaseTransactions(){
		$key = $this->request->get['key'];
		$this->load->model('transaction/purchase_return');
		$res = $this->model_transaction_purchase_return->getPurchaseDetailsByKey($key);
		if(count($res)>=1){
	      foreach($res as $r){
		 	$newAry[]= $r['transaction_no'];
		  } 
		}else{
			$newAry[] = '0';
		}
		$this->response->setOutput(json_encode($newAry));
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/purchase_return');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['filter_location_code'] = $this->session->data['location_code'];
			$_productDetails = $this->model_transaction_purchase_return->getProductByName($this->data);

			$str = ''; 
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){

					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
                    $name_display = $_productDetails[$i]['sku'].' ('.$_productDetails[$i]['name'].')';
                     
					$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($name_display)."</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
                    $name_display = $_productDetails[$i]['name'];
                    $var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($name_display).")</li>";
				}
				$str.='</ul>'; 
			}
			echo $str;
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/purchase');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if($bill_discount_price=='' && $bill_discount_percentage==''){
				$bill_discount_price = 0;
			}
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}else if($bill_discount_percentage=='0' || $bill_discount_price=='0'){
				if ($this->session->data['bill_discount']) {
					unset($this->session->data['bill_discount']);
					unset($this->session->data['bill_discount_mode']);
				}
			}
		}
		foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'sku_qty'					=> $product['sku_qty'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    $res['discount_value']='';
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$showdiscountColumn = '0';
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$showdiscountColumn = '1';
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					
					$res['discount_price']  	 = $bill_discount_price;
					$res['discount_percentage']  = $bill_discount_percentage;

					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
						if($showdiscountColumn =='1'){
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="6" align="right" class="purchase_total_left">'.$lable.'</td>
							<td class="purchase_total_right insertproduct" colspan="2">
							<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="textbox row_total_box text-right" readonly="readonly" /></td></tr>';
						}
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				$res['bill_discount']     = $this->session->data['bill_discount'];
				$res['bill_discount_mode']= $this->session->data['bill_discount_mode'];
			}
			$this->response->setOutput(json_encode($res));
	}
	public function getVendorProductDetails(){
		$this->session->data['vendor_id']   = $this->request->post['id'];
		$this->session->data['vendor_name'] = $this->request->post['vendor_name'];
		$this->session->data['reference_num'] = $this->request->post['reference_num'];

		if(isset($this->request->post['unset_session']) && $this->request->post['unset_session']=='yes'){
			unset($this->session->data['vendor_id']);
			unset($this->session->data['vendor_name']);
			unset($this->session->data['reference_num']);
		}
	}
	public function ajaxUpdateqtyAndPrice(){
		$this->load->model('transaction/purchase');	
		$this->load->model('ajax/cart');
		$apply_tax_type = 0;
		
		$sku 	   		= $this->request->post['sku']; 
		$productQty 	= $this->request->post['quantity'];
		$price     		= $this->request->post['price'];		
		$tax_class_id 	= $this->request->post['tax_class_id'];
		$discount 				  = 0;
		$bill_discount_percentage = $this->request->post['bill_discount_percentage'];
		$bill_discount_price 	  = $this->request->post['bill_discount_price'];

		$netPrice  = $productQty * $price;
		if(isset($bill_discount_percentage) && $bill_discount_percentage < 100){
			$dicount = $netPrice / $bill_discount_percentage;
		}else{
			$dicount = $bill_discount_percentage;
		}

		$netPrice  = $netPrice - $discount;
		$data['netPrice'] = $netPrice;
		$data['price']    = $price;
		$data['total']    = $total;

		$netPrice  = $this->currency->format($netPrice);
		$total     = $this->currency->format($total);

		$cart_type = 'cart_purchase';
 		$productId = $this->request->post['product_id'];
 		if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 )) {
			$this->cart->update($productId, $productQty, $cart_type, $this->request->post);
		}

 		$apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
 		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->request->post['location_code']);
 		$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
 		
 			foreach($total_data as $totals) {
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				if($tax_class_id=='1'){
					$res['tax'] 	  ='';
					$res['tax_value'] ='';
				}

				if($apply_tax_type == '2'){
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
			}
		$data = array('qty'		=> $productQty, 
					'price'		=> $price, 
					'netPrice'	=> $netPrice, 
					'total'		=> $total, 
					'discnt'	=> $discnt,
					'sku'		=> $productId,
					'orderSubTotal'	=> $res['sub_total'],
					'orderTax'		=> $res['tax'],
					'ordertax_value'=> $res['tax_value'],
					'tax_type'		=> $apply_tax_type,
					'orderDiscount'	=> $res['discount'],
					'orderTotal'	=> $res['total'],
					'tax_str' 		=> '<tr id="TRtax">
		                <td align="right" class="purchase_total_left" id="tax_title" colspan="6"></td>
		                <td class="purchase_total_right insertproduct">
		                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$res['tax'].'" id="tax" name="tax" style="text-align:right;">
		                </td>
		            </tr>');
		$this->response->setOutput(json_encode($data));
	}
	public function bulk_delete(){
		$this->load->model('transaction/purchase_return');
		foreach ($this->request->post['selected'] as $purchase_id) {
			$this->model_transaction_purchase_return->deletePurchase($purchase_id);
		}

		$this->session->data['success'] = 'Purchase Details deleted Completed';
		if(isset($_REQUEST['filter_date_from'])){
			$pageUrl.= '&filter_date_from='.$_REQUEST['filter_date_from'];
		}
		if(isset($_REQUEST['filter_date_to'])){
			$pageUrl.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if(isset($_REQUEST['filter_location_code'])){
			$pageUrl.='&filter_location_code='.$_REQUEST['filter_location_code']; 
		}
		if(isset($_REQUEST['filter_supplier'])){
			$pageUrl.='&filter_supplier='.$_REQUEST['filter_supplier']; 
		}
		if(isset($_REQUEST['filter_transactionno'])){
			$pageUrl.='&filter_transactionno='.$_REQUEST['filter_transactionno']; 
		}

		$this->redirect($this->url->link('transaction/purchase_return', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	}
}
?>