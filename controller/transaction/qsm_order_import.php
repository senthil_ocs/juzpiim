<?php
class ControllerTransactionQSMorderImport extends Controller { // 15-03-2021 |^|
	private $error = array();
	public function index() {
		$this->load->model('transaction/qsm_order_import');
		$this->document->setTitle('QSM Order Import');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'QSM Order Import',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['fileUploadAction'] = $this->url->link('transaction/qsm_order_import/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['back']   = $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['clearImportedOrders'] = $this->url->link('transaction/qsm_order_import/clearImportedOrders', 'token=' . $this->session->data['token'], 'SSL');

		$orders = $this->model_transaction_qsm_order_import->getQSMOrders();
		$tempOrders = array();
		$m = 0;
		if(!empty($orders)){
			foreach ($orders as $value) {
				$customerDetails = $this->model_transaction_qsm_order_import->getCustomerDetails($value['customercode']);
				
				$product_sku = str_replace("'",'',$value['OptionCode']) !='' ? str_replace("'",'',$value['OptionCode']) : str_replace("'",'',$value['SellerCode']);

				if($product_sku!=''){
					$product  	 = $this->model_transaction_qsm_order_import->getProductBySKU($product_sku);
					if(empty($product)){
						$netwrkProductId =$this->model_transaction_qsm_order_import->getProductIdBySKU($product_sku);
					}
					$product_id  = '';
					if($product['product_id'] !=''){
						$product_id =  $product['product_id'];
					}else if($netwrkProductId){
						$product_id =  $netwrkProductId;
					}else{
						$m++;
					}
					$productDetails = $this->model_transaction_qsm_order_import->getProductById($product_id);
					$tempOrders[] 	= array(
						'orderID' 		=> $value['id'],
						'orderNumber' 	=> $value['OrderNo'],
						'sellerSKU' 	=> $product_sku,
						'itemName' 		=> $value['Item'].' '.$value['Options'],
						'customerName'	=> $customerDetails['name'],
						'orderExist' 	=> $value['orderExist'],
						'product_id' 	=> $product_id,
						'qty' 			=> $value['Qty'],
						'unitPrice' 	=> $value['SellPrice'],
						'total' 		=> number_format($value['Qty'] * $value['SellPrice'], 2,'.',''),
						'createNewBtn'  => $this->url->link('inventory/inventory/insert', 'token=' . $this->session->data['token'].'&from=qsm_order_import&qsm_id='.$value['id'], 'SSL'),
						'deleteBtn'  => $this->url->link('transaction/qsm_order_import/delete', 'token=' . $this->session->data['token'].'&qsm_id='.$value['id'], 'SSL')
					);
				}
			}
		}else{
			$m=1;
		}
		$this->data['action'] = $this->url->link('transaction/qsm_order_import/insertAsSalesOrder', 'token=' . $this->session->data['token'], 'SSL'); 
		if($m != 0 || empty($tempOrders)){
			$this->data['action'] = ''; 
		}
		$this->data['deleteAllBtn'] = $this->url->link('transaction/qsm_order_import/deleteAllData', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->data['orders'] = $tempOrders;
		$this->data['token']  = $this->session->data['token'];
		$this->data['route']  = 'transaction/delivery';
		$this->template 	  = 'transaction/qsm_order_import.tpl';
		$this->children 	  = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
    function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, '',",");
        }
        fclose($file_handle);
        return $line_of_text;
	}
    
	public function insert() {
		$this->load->model('transaction/qsm_order_import');

		if(!empty($_FILES['file']['size'])){

			$res = $this->uploadFile();
			if($res['status']){
                $orders   = $this->readCSV(DIR_SERVER.'doc/import_csv/'.$res['new_file_name']);
                // printArray($orders); die;

				if(!empty($orders)){
					$header = $orders[0];    
			        if($header[38]=='Seller code' && $header[14] == 'Qty.' && $header[16] == 'Option Code' && $header[18] == 'Recipient' && $header[20] == 'Recipient Phone number' && $header[21] == 'Recipient mobile Phone number' && $header[22] == 'Address' && $header[23] == 'Postal code' && $header[27] == 'Currency' && $header[29] == 'Sell Price' && $header[30] =='discount' && $header[33] == 'Customer' && $header[35] == 'Shipping memo' && $header[36] =='Customer phone number' && $header[37] =='Customer mobile phone number'){

				        foreach ($orders as $key => $order) {
				            if($key != 0 && !empty($order)){
				        		$customerMobile = str_replace('-', '', $order[37]) !='' ? str_replace('-', '', $order[37]) : str_replace('-', '', $order[36]);

				            	$customer  = $this->model_transaction_qsm_order_import->checkCustomerByPhone($customerMobile);

				                if(empty($customer)){
				                	$order['cust_code'] = $this->getRandomCustCode();
				                    $customer = $this->model_transaction_qsm_order_import->createNewCustomer($order);
				                    if($customer){
					                    $order['shipping_id'] = $customer['shipping_id'];
					                    $order['customer_id'] = $customer['customercode'];
				                    }
				                }else{
					                $order['customer_id'] = $customer['customercode'];
				                    $shippingDetails      = $this->model_transaction_qsm_order_import->checkShippingAddress($customer['customercode'],$order);
				                	if(empty($shippingDetails)){
				                		$shippingDetails  = $this->model_transaction_qsm_order_import->addShippingAddress($order);
				                	}
				                    $order['shipping_id'] = $shippingDetails['id'];
				                }

				                if($order['customer_id'] != ''){
									$product_sku = str_replace("'",'',$order[16]) !='' ? str_replace("'",'',$order[16]) : str_replace("'",'',$order[38]);
                                    $product = $this->model_transaction_qsm_order_import->getProductBySKU($product_sku);
                                    if(empty($product) && !$product['product_id']){
                                        $product = $this->model_transaction_qsm_order_import->getProductById($this->model_transaction_qsm_order_import->getProductIdBySKU($product_sku));
                                    }
                                    if($product['product_id']!='' || $product_sku!=''){
                                        $order['exist'] = '0';
                                        if(!empty($this->model_transaction_qsm_order_import->checkOrderIdAndSku($order[1],$product['product_id']))){
                                            $order['exist'] = '1';
                                        }
                                        $this->model_transaction_qsm_order_import->insertTempTable($order);
                                    }
				                }
				            }
					    }
			    	}else{
						$this->session->data['error']   = 'Error: Uploaded file format wrong';
				    }
				}
		    }else{
				$this->session->data['error']   = 'Error: '.$res['message'];
			}
		}else{
			$this->session->data['error']   	= 'Error: Please select file to import';
		}
		$this->redirect($this->url->link('transaction/qsm_order_import', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function uploadFile(){
        if($_FILES["file"]){
            $temp = explode(".", $_FILES["file"]["name"]);

            if(end($temp) == 'csv'){
	            $newfilename = 'QSM_'.date('Y-m-d_His') . '.' . end($temp);
	            if(move_uploaded_file($_FILES["file"]["tmp_name"], DIR_SERVER."doc/import_csv/" . $newfilename)){
	                return $res = array('status' => 1, 'new_file_name' => $newfilename);
	            }
            }else{
            	return $res = array('status' => 0, 'message' => 'Uploaded file format not supported!'); 
            }
        }
    }
    public function getProductDetails(){
    	$this->load->model('transaction/sales');

    	$productDetails = $this->model_transaction_sales->getProductByNamenew($this->request->post);
    	$str.='<ul id="country-list">';
    	foreach ($productDetails as $details) {
    		$str.= "<li onClick=\"selectedProduct('".$details['sku']."','".$details['product_id']."');\">".trim($details['sku']).' ('.trim($details['name']).")</li>";
    	}
    	$str.='</ul>';
    	echo $str;
    }
    public function updateQSMProduct(){
		$this->load->model('transaction/qsm_order_import');
    	echo $this->model_transaction_qsm_order_import->updateQSMProduct($this->request->post);
    }
    public function insertAsSalesOrder(){
    	$this->load->model('transaction/qsm_order_import');
    	$tempOrders = $this->model_transaction_qsm_order_import->getDetailsFromTempTable();
    	// printArray($tempOrders); die;

	    if(!empty($tempOrders)){
	        foreach ($tempOrders as $temOrder) {
	            if(empty($this->model_transaction_qsm_order_import->checkOrderExistOrNot($temOrder['OrderNo']))){
	                $temOrder['invoice_no'] = $this->getInvoiceNo();
	                $this->model_transaction_qsm_order_import->addSalesOrder($temOrder);
	            }
	        }
	    }
	    $this->redirect($this->url->link('transaction/qsm_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function deleteAllData(){
    	$this->load->model('transaction/qsm_order_import');
    	$this->model_transaction_qsm_order_import->deleteAllData();
    	
    	$this->redirect($this->url->link('transaction/qsm_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function delete(){
    	$this->load->model('transaction/qsm_order_import');
    	$this->model_transaction_qsm_order_import->delete($this->request->get['qsm_id']);
    	$this->redirect($this->url->link('transaction/qsm_order_import', 'token=' . $this->session->data['token'], 'SSL'));        
    }
    public function getInvoiceNo(){
		$salesLastId = $this->model_transaction_qsm_order_import->getSalesLastId();
		return $this->config->get('config_sales_prefix').date('ym').str_pad($salesLastId +1, 4, '0', STR_PAD_LEFT);
    }
    public function clearImportedOrders(){
    	$this->load->model('transaction/qsm_order_import');
    	$this->model_transaction_qsm_order_import->clearImportedOrders();
    	$this->redirect($this->url->link('transaction/qsm_order_import', 'token=' . $this->session->data['token'], 'SSL')); 	
    }
	public function getRandomCustCode(){
    	$this->load->model('setting/customers');
	    $lastCustId = $this->model_setting_customers->getCustLastId();
		return 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);
	}
}
?>