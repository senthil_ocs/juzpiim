<?php
class ControllerTransactionShopeeOrderImport extends Controller { // 30-08-2021 |^|
	private $error = array();
	public function index() 
	{
		$this->load->model('transaction/shopee_order_import');
		$this->document->setTitle('Shopee Order Import');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Shopee Order Import',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['fileUploadAction'] = $this->url->link('transaction/shopee_order_import/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['back']   = $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['clearImportedOrders'] = $this->url->link('transaction/shopee_order_import/clearImportedOrders', 'token=' . $this->session->data['token'], 'SSL');

		$orders = $this->model_transaction_shopee_order_import->getShopeeOrders();
		$tempOrders = array();
		$m = 0;
		if(!empty($orders)){
			foreach ($orders as $value) {
				$customerDetails = $this->model_transaction_shopee_order_import->getCustomerDetails($value['customercode']);
				$product_sku     = $value['SKUReferenceNo'];

				if($product_sku !=''){
					$product  	 = $this->model_transaction_shopee_order_import->getProductBySKU($product_sku);
					if(empty($product)){
						$netwrkProductId =$this->model_transaction_shopee_order_import->getProductIdBySKU($product_sku);
					}
					$product_id  = '';
					if($product['product_id'] !=''){
						$product_id =  $product['product_id'];
					}else if($netwrkProductId){
						$product_id =  $netwrkProductId;
					}else{
						$m++;
					}
					$productDetails = $this->model_transaction_shopee_order_import->getProductById($product_id);
					$tempOrders[] 	= array(
						'orderID' 		=> $value['id'],
						'orderNumber' 	=> $value['OrderID'],
						'sellerSKU' 	=> $product_sku,
						'itemName' 		=> $value['ProductName'].' '.$value['VariationName'],
						'customerName'	=> $customerDetails['name'],
						'orderExist' 	=> $value['orderExist'],
						'product_id' 	=> $product_id,
						'qty' 			=> $value['Quantity'],
						'unitPrice' 	=> $value['DealPrice'],
						'total' 		=> number_format($value['Quantity'] * $value['DealPrice'], 2,'.',''),
						'createNewBtn'  => $this->url->link('inventory/inventory/insert', 'token=' . $this->session->data['token'].'&from=shopee_order_import&id='.$value['id'], 'SSL'),
						'deleteBtn'     => $this->url->link('transaction/shopee_order_import/delete', 'token=' . $this->session->data['token'].'&id='.$value['id'], 'SSL')
					);
				}
			}
		}else{
			$m=1;
		}
		$this->data['action'] = $this->url->link('transaction/shopee_order_import/insertAsSalesOrder', 'token=' . $this->session->data['token'], 'SSL'); 
		if($m != 0 || empty($tempOrders)){
			$this->data['action'] = ''; 
		}
		$this->data['deleteAllBtn'] = $this->url->link('transaction/shopee_order_import/deleteAllData', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->data['orders'] = $tempOrders;
		$this->data['token']  = $this->session->data['token'];
		$this->data['route']  = 'transaction/delivery';
		$this->template 	  = 'transaction/shopee_order_import.tpl';
		$this->children 	  = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
    private function readCSV($csvFile)
    {
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, '',",");
        }
        fclose($file_handle);
        return $line_of_text;
	}
    private function readXLSX($xlsxFile)
    {
    	require_once DIR_SERVER.'MPDF/SimpleXLSX.php';
    	if ( $xlsx = SimpleXLSX::parse($xlsxFile)) {
    		$res = array('status'=> 1, 'data'=> $xlsx->rows());
		}else{
			$res = array('status'=> 0, 'message'=> SimpleXLSX::parseError());
		}
		return $res;
    }

	public function insert() 
	{
		$this->load->model('transaction/shopee_order_import'); 
		if(!empty($_FILES['file']['size'])){

			$res = $this->uploadFile();
			if($res['status']){
                $orders   = $this->readXLSX(DIR_SERVER.'doc/import_csv/'.$res['new_file_name']);

				if(!empty($orders['data'])){
					$header = $orders['data'][0];

			        if( $header[0] 	== 'Order ID' && $header[1] == 'Order Status' && $header[8] == 'Order Creation Date' && $header[9] == 'Order Paid Time' && $header[11] == 'Product Name' && $header[12] == 'SKU Reference No.' && $header[14] == 'Original Price' && $header[15] == 'Deal Price' && $header[16] == 'Quantity' && $header[17] == 'Product Subtotal' && $header[19] == 'Seller Discount' && $header[33] == 'Total Amount' && $header[36] == 'Reverse Shipping Fee' && $header[38] == 'Commission Fee (Incl. GST)' && $header[39] == 'Service Fee (incl. GST)' && $header[40] == 'Grand Total' && $header[44] == 'Phone Number' && $header[45] == 'Delivery Address' && $header[46] == 'Town' && $header[47] == 'District' && $header[48] == 'City' && $header[50] == 'Country' && $header[51] == 'Zip Code' && $header[52] == 'Remark from buyer' && $header[54] == 'Note'){

				        foreach ($orders['data'] as $key => $order) {
				            if($key != 0 && !empty($order)){
				            	$order[44] 		= substr($order[44], 2);
				        		$customerMobile = str_replace('-', '', $order[44]) !='' ? str_replace('-', '', $order[44]) : '';
				            	$customer  		= $this->model_transaction_shopee_order_import->checkCustomerByPhone($customerMobile);

				                if(empty($customer)){
				                	$order['cust_code'] = $this->getRandomCustCode();
				                    $customer = $this->model_transaction_shopee_order_import->createNewCustomer($order);
				                    if($customer){
					                    $order['shipping_id'] = $customer['shipping_id'];
					                    $order['customer_id'] = $customer['customercode'];
				                    }
				                }else{
					                $order['customer_id'] = $customer['customercode'];
				                    $shippingDetails      = $this->model_transaction_shopee_order_import->checkShippingAddress($customer['customercode'], $order);
				                	if(empty($shippingDetails)){
				                		$shippingDetails  = $this->model_transaction_shopee_order_import->addShippingAddress($order);
				                	}
				                    $order['shipping_id'] = $shippingDetails['id'];
				                }
				                if($order['customer_id'] != ''){
									$product_sku = $order[12];
                                    $product     = $this->model_transaction_shopee_order_import->getProductBySKU($product_sku);
                                    if(empty($product) && !$product['product_id']){
                                        $product = $this->model_transaction_shopee_order_import->getProductById($this->model_transaction_shopee_order_import->getProductIdBySKU($product_sku));
                                    }
                                    if($product['product_id']!='' || $product_sku!=''){
                                    	$order['product_id'] = $product['product_id'];
                                        $order['exist'] 	 = '0';
                                        if(!empty($this->model_transaction_shopee_order_import->checkOrderIdAndSku($order[0],$product['product_id']))){
                                            $order['exist'] = '1';
                                        }
                                        $this->model_transaction_shopee_order_import->insertTempTable($order);
                                    }
				                }
				            }
					    }
			    	}else{
						$this->session->data['error']   = 'Error: Uploaded file format wrong';
				    }
				}
		    }else{
				$this->session->data['error']   = 'Error: '.$res['message'];
			}
		}else{
			$this->session->data['error']   	= 'Error: Please select file to import';
		}
		$this->redirect($this->url->link('transaction/shopee_order_import', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function uploadFile()
	{
        if($_FILES["file"]){
            $temp = explode(".", $_FILES["file"]["name"]);
            if(end($temp) == 'xlsx'){
	            $newfilename = 'Shopee_'.date('Y-m-d_His') . '.' . end($temp);
	            if(move_uploaded_file($_FILES["file"]["tmp_name"], DIR_SERVER."doc/import_csv/" . $newfilename)){
	                return $res = array('status' => 1, 'new_file_name' => $newfilename);
	            }
            }else{
            	return $res = array('status' => 0, 'message' => 'Uploaded file format not supported!'); 
            }
        }
    }
    public function getProductDetails()
    {
    	$this->load->model('transaction/sales');

    	$productDetails = $this->model_transaction_sales->getProductByNamenew($this->request->post);
    	$str.='<ul id="country-list">';
    	foreach ($productDetails as $details) {
    		$str.= "<li onClick=\"selectedProduct('".$details['sku']."','".$details['product_id']."');\">".trim($details['sku']).' ('.trim($details['name']).")</li>";
    	}
    	$str.='</ul>';
    	echo $str;
    }
    public function updateQSMProduct()
    {
		$this->load->model('transaction/shopee_order_import');
    	echo $this->model_transaction_shopee_order_import->updateQSMProduct($this->request->post);
    }
    public function insertAsSalesOrder()
    {
    	$this->load->model('transaction/shopee_order_import');
    	$tempOrders = $this->model_transaction_shopee_order_import->getDetailsFromTempTable();
    	
	    if(!empty($tempOrders)){
	        foreach ($tempOrders as $tempOrder) {
	        	$order = $this->model_transaction_shopee_order_import->checkOrderExistOrNot($tempOrder['OrderID']);
	            if(empty($order)){
	                $tempOrder['invoice_no'] = $this->getInvoiceNo();
	                $this->model_transaction_shopee_order_import->addSalesOrder($tempOrder);
	            }else if(!empty($order) && $tempOrder['OrderStatus'] == 'Cancelled'){
	            	$this->model_transaction_shopee_order_import->editSalesOrder($tempOrder, $order);
	            }
	        }
	    }
	    $this->redirect($this->url->link('transaction/shopee_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function deleteAllData()
    {
    	$this->load->model('transaction/shopee_order_import');
    	$this->model_transaction_shopee_order_import->deleteAllData();
    	$this->redirect($this->url->link('transaction/shopee_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function delete()
    {
    	$this->load->model('transaction/shopee_order_import');
    	$this->model_transaction_shopee_order_import->delete($this->request->get['id']);
    	$this->redirect($this->url->link('transaction/shopee_order_import', 'token=' . $this->session->data['token'], 'SSL'));        
    }
    public function getInvoiceNo()
    {
		$salesLastId = $this->model_transaction_shopee_order_import->getSalesLastId();
		return $this->config->get('config_sales_prefix').date('ym').str_pad($salesLastId +1, 4, '0', STR_PAD_LEFT);
    }
    public function clearImportedOrders()
    {
    	$this->load->model('transaction/shopee_order_import');
    	$this->model_transaction_shopee_order_import->clearImportedOrders();
    	$this->redirect($this->url->link('transaction/shopee_order_import', 'token=' . $this->session->data['token'], 'SSL')); 	
    }
	public function getRandomCustCode()
	{
    	$this->load->model('setting/customers');
	    $lastCustId = $this->model_setting_customers->getCustLastId();
		return 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);
	}
}
?>