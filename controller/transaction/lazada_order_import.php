<?php
class ControllerTransactionLazadaOrderImport extends Controller { // 22-02-2021 |^|
	private $error = array();
	public function index() {
		$this->load->model('transaction/lazada_order_import');
		$this->document->setTitle('Lazada Order Import');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Lazada Order Import',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['fileUploadAction'] = $this->url->link('transaction/lazada_order_import/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['back']   = $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['clearImportedOrders'] = $this->url->link('transaction/lazada_order_import/clearImportedOrders', 'token=' . $this->session->data['token'], 'SSL');

		$orders     = $this->model_transaction_lazada_order_import->getLazadaOrders();
		
		$tempOrders = array();
		$m = 0;
		if(!empty($orders)){
			foreach ($orders as $value) {
				$customerDetails = $this->model_transaction_lazada_order_import->getCustomerDetails($value['customercode']);
				// printArray($customerDetails); die;
				$product  	     = $this->model_transaction_lazada_order_import->getProductBySKU($value['SellerSKU']);
				
				if(empty($product)){
					$product = $this->model_transaction_lazada_order_import->getProduct($value['SellerSKU']);
				}
				$product_id  = '';
				if($product['product_id'] !=''){
					$product_id =  $product['product_id'];
				}else{
					$m++;
				}

				$tempOrders[] 	= array(
					'orderID' 		=> $value['id'],
					'orderNumber' 	=> $value['OrderNumber'],
					'sellerSKU' 	=> $value['SellerSKU'],
					'product_id' 	=> $value['product_id'],
					'itemName' 		=> $value['ItemName'],
					'customerName'	=> $customerDetails['name'],
					'unitPrice' 	=> $value['UnitPrice'],
					'orderExist' 	=> $value['orderExist'],
					'product_id' 	=> $product_id,
					'createNewBtn'  => $this->url->link('inventory/inventory/insert', 'token=' . $this->session->data['token'].'&from=lazada_order_import&id='.$value['id'], 'SSL'),
					'deleteBtn'  => $this->url->link('transaction/lazada_order_import/delete', 'token=' . $this->session->data['token'].'&lazada_id='.$value['id'], 'SSL')
				);
			}
		}else{
			$m=1;
		}
		$this->data['action'] = $this->url->link('transaction/lazada_order_import/insertAsSalesInvoice', 'token=' . $this->session->data['token'], 'SSL'); 
		if($m != 0){
			$this->data['action'] = ''; 
		}
		$this->data['deleteAllBtn'] = $this->url->link('transaction/lazada_order_import/deleteAllData', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->data['orders'] = $tempOrders;
		$this->data['token']  = $this->session->data['token'];
		$this->data['route']  = 'transaction/delivery';
		$this->template 	  = 'transaction/lazada_order_import.tpl';
		$this->children 	  = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
    function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, '',";");
        }
        fclose($file_handle);
        return $line_of_text;
	}
    private function readXLSX($xlsxFile){
    	require_once DIR_SERVER.'MPDF/SimpleXLSX.php';
    	if ( $xlsx = SimpleXLSX::parse($xlsxFile)) {
    		$res = array('status'=> 1, 'data'=>$xlsx->rows());
		}else{
			$res = array('status'=> 0, 'message'=> SimpleXLSX::parseError());
		}
		return $res;
    }
	public function insert() {
		$this->load->model('transaction/lazada_order_import');

		$temp = explode(".", $_FILES["file"]["name"]);
        if(end($temp) == 'csv'){
			if(!empty($_FILES['file']['size'])){

				$res = $this->uploadFile();
				if($res['status']){
	                $orders   = $this->readCSV(DIR_SERVER.'doc/import_csv/'.$res['new_file_name']);
	                
					if(!empty($orders)){
						$order1 = $orders[0];
	                    
				        if($order1[8] == 'Order Number' && $order1[4] == 'Seller SKU' && $order1[10] == 'Customer Name' && $order1[11] == 'Customer Email' && $order1[12] == 'National Registration Number' && $order1[13] == 'Shipping Name' && $order1[14] == 'Shipping Address' && $order1[15] == 'Shipping Address2' && $order1[16] == 'Shipping Address3' && $order1[17] == 'Shipping Address4' && $order1[18] == 'Shipping Address5' && $order1[19] == 'Shipping Phone Number' && $order1[21] == 'Shipping City' && $order1[22] == 'Shipping Postcode' && $order1[23] == 'Shipping Country' && $order1[26] == 'Billing Address' && $order1[27] == 'Billing Address2' && $order1[28] == 'Billing Address3' && $order1[33] == 'Billing City' && $order1[34] == 'Billing Postcode' && $order1[38] == 'Unit Price'){

					        foreach ($orders as $key => $order) {
					            if($key != 0 && !empty($order)){
					            	$customer  = $this->model_transaction_lazada_order_import->checkCustomerByPhone($order[19]);
					                if(empty($customer)){
					                	$order['cust_code'] = $this->getRandomCustCode();
					                    $customer = $this->model_transaction_lazada_order_import->createNewCustomer($order);
					                    if($customer){
						                    $order['shipping_id'] = $customer['shipping_id'];
						                    $order['customer_id'] = $customer['customercode'];
					                    }
					                }else{
					                	
					                	$shippingDetails = $this->model_transaction_lazada_order_import->checkShippingAddress($customer['customercode'],$order);
					                    if(empty($shippingDetails)){
					                    	$shippingDetails = $this->model_transaction_lazada_order_import->createNewShippingAddressCSV($customer['customercode'],$order);
					                    }

					                    // $shippingDetails      = $this->model_transaction_lazada_order_import->getShippingDetails($customer['customercode']);
					                    $order['shipping_id'] = $shippingDetails['id'];
					                    $order['customer_id'] = $customer['customercode'];
					                }

					                if($order['customer_id'] != ''){
	                                    $product = $this->model_transaction_lazada_order_import->getProductBySKU($order[4]);
	                                    if(empty($product) && !$product['product_id']){
	                                        $product = $this->model_transaction_lazada_order_import->getProductById($this->model_transaction_lazada_order_import->getProductIdBySKU($order[4]));
	                                    }
	                                    
	                                    if($product['product_id'] !='' || $order[4] !=''){
	                                        $order['exist'] = '0';
	                                        if(!empty($this->model_transaction_lazada_order_import->checkOrderIdAndSku($order[8],$product['product_id']))){
	                                            $order['exist'] = '1';
	                                        }
	                                        $order['sales_channel'] = $this->request->post['sales_channel'];
	                                        $this->model_transaction_lazada_order_import->insertTempTable($order);
	                                    }
					                }
					            }
						    }
				    	}else{
							$this->session->data['error'] = 'Error: Uploaded file format wrong';
					    }
					}
			    }else{
					$this->session->data['error'] = 'Error: '.$res['message'];
				}
			}else{
				$this->session->data['error'] = 'Error: Please select file to import';
			}
		}else if(end($temp) == 'xlsx'){
			
			$res = $this->uploadFile();
			if($res['status']){
				$res2   = $this->readXLSX(DIR_SERVER.'doc/import_csv/'.$res['new_file_name']);

				if($res2['status']){
					$header = $res2['data'][0]; 
					// $body  = $res2['data'][1]; 
					// $sample = [];
					// foreach ($header as $key => $value) {
					// 	$sample[$key] = array('key' => $value, 'value' => $body[$key]); 
					// }
					// printArray($sample); die;

					if($header[12] == 'orderNumber' && $header[5] == 'sellerSku' && $header[16] == 'customerName' && $header[17] == 'customerEmail' && $header[18] == 'nationalRegistrationNumber' && $header[19] == 'shippingName' && $header[20] == 'shippingAddress' && $header[21] == 'shippingAddress2' && $header[22] == 'shippingAddress3' && $header[23] == 'shippingAddress4' && $header[24] == 'shippingAddress5' && $header[25] == 'shippingPhone' && $header[27] == 'shippingCity' && $header[28] == 'shippingPostCode' && $header[29] == 'shippingCountry' && $header[32] == 'billingAddr' && $header[33] == 'billingAddr2' && $header[34] == 'billingAddr3' && $header[35] == 'billingAddr4' && $header[37] == 'billingPhone' && $header[38] == 'billingPhone2' && $header[39] == 'billingCity' && $header[47] == 'unitPrice' && $header[51] == 'itemName'){
						
						foreach ($res2['data'] as $key => $order) {

				            if($key != 0 && !empty($order)){
				            	$customer  = $this->model_transaction_lazada_order_import->checkCustomerByPhone($order[37]);
				                if(empty($customer)){
									// Old keys already used for csv format, so here just customize customer details based on keys
				                	$custData[10] = $order[16]; // Name
				                	$custData[11] = $order[17]; // Email
				                	$custData[19] = $order[37] ? $order[37] : $order[25]; // Contact
				                	$custData[26] = $order[20]; // Address 1
				                	$custData[27] = $order[21].' '.$order[22].' '.$order[23].' '.$order[24]; // Address 2
				                	$custData[33] = $order[27]; // City
				                	$custData[34] = $order[28]; //Zip 

									$custData[13] = $order[19];  // Name  
									$custData[14] = $order[20].' '.$order[21].' '.$order[22]; // for shipping address 1
									$custData[15] = $order[23].' '.$order[24]; // for shipping address 2
									$custData[21] = $order[27];  // City  
									$custData[22] = $order[28];  // Zip
									$custData[23] = $order[29];  // Country  
					                $custData['cust_code'] = $this->getRandomCustCode();
				                    $customer = $this->model_transaction_lazada_order_import->createNewCustomer($custData);
				                    if($customer){
					                    $order['shipping_id'] = $customer['shipping_id'];
					                    $order['customer_id'] = $customer['customercode'];
				                    }
				                }else{
									$custData[19] = $order[37] ? $order[37] : $order[25]; // Contact No
									$custData[14] = $order[21].' '.$order[22].' '.$order[23].' '.$order[24]; // Address
									$custData[22] = $order[28]; // Zip

				                    $shippingDetails = $this->model_transaction_lazada_order_import->checkShippingAddress($customer['customercode'], $custData);
				                    if(empty($shippingDetails)){
										$custData[18] = $order[20]; // Address 1
										$custData[20] = $order[21].' '.$order[22].' '.$order[23]; // Address 2 
										$custData[26] = $order[27]; // City
										$custData[28] = $order[29]; // Country
										$custData[27] = $order[28]; // Zip
										$custData[24] = $order[25]; // Contact 
				                    	$shippingDetails = $this->model_transaction_lazada_order_import->createNewShippingAddress($customer['customercode'], $custData);
				                    }
				                    $order['shipping_id'] = $shippingDetails['id'];
				                    $order['customer_id'] = $customer['customercode'];
				                }

				                if($order['customer_id'] != ''){
                                    $product = $this->model_transaction_lazada_order_import->getProductBySKU($order[5]);
                                    if(empty($product) && !$product['product_id']){
                                        $product = $this->model_transaction_lazada_order_import->getProductById($this->model_transaction_lazada_order_import->getProductIdBySKU($order[5]));
                                    }
                                    
                                    if($product['product_id'] !='' || $order[4] !=''){
                                        $order['exist'] = '0';
                                        if(!empty($this->model_transaction_lazada_order_import->checkOrderIdAndSku($order[12],$product['product_id']))){
                                            $order['exist'] = '1';
                                        }
	                                    $order['sales_channel'] = $this->request->post['sales_channel'];
                                        $this->model_transaction_lazada_order_import->insertTempTableType2($order);
                                    }
				                }
				            }
					    }
					}else{
						$this->session->data['error'] = 'Error: Uploaded file format wrong';
				    }
				}else{
					$this->session->data['error'] = 'Error: '.$res2['message'];
				}
			}else{
				$this->session->data['error'] = 'Error: '.$res['message'];
			}
		}
		$this->redirect($this->url->link('transaction/lazada_order_import', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function uploadFile(){
        if($_FILES["file"]){
            $temp = explode(".", $_FILES["file"]["name"]);
            if(end($temp) == 'csv' || end($temp) == 'xlsx'){
	            $newfilename = 'lazada_'.date('Y-m-d_His') . '.' . end($temp);
	            if(move_uploaded_file($_FILES["file"]["tmp_name"], DIR_SERVER."doc/import_csv/" . $newfilename)){
	                return $res = array('status' => 1, 'new_file_name' => $newfilename);
	            }
            }else{
            	return $res = array('status' => 0, 'message' => 'Uploaded file format not supported!'); 
            }
        }
    }
    public function getProductDetails(){
    	$this->load->model('transaction/sales');

    	$productDetails = $this->model_transaction_sales->getProductByNamenew($this->request->post);
    	$str.='<ul id="country-list">';
    	foreach ($productDetails as $details) {
    		$str.= "<li onClick=\"selectedProduct('".$details['sku']."','".$details['product_id']."');\">".trim($details['sku']).' ('.trim($details['name']).")</li>";
    	}
    	$str.='</ul>';
    	echo $str;
    }
    public function updateLazadaProduct(){
		$this->load->model('transaction/lazada_order_import');
    	echo $this->model_transaction_lazada_order_import->updateLazadaProduct($this->request->post);
    }
    public function insertAsSalesInvoice(){
    	$this->load->model('transaction/lazada_order_import');
    	$tempOrders = $this->model_transaction_lazada_order_import->getDetailsFromTempTable();

	    if(!empty($tempOrders)){
	        foreach ($tempOrders as $temOrder) {
	            if(empty($this->model_transaction_lazada_order_import->checkOrderExistOrNot($temOrder['OrderNumber'],$temOrder['sales_channel']))){
	                $temOrder['invoice_no'] = $this->getInvoiceNo();
	                $this->model_transaction_lazada_order_import->addSalesOrder($temOrder);
	            }
	        }
	    }
	    $this->redirect($this->url->link('transaction/lazada_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function deleteAllData(){
    	$this->load->model('transaction/lazada_order_import');
    	$this->model_transaction_lazada_order_import->deleteAllData();
    	
    	$this->redirect($this->url->link('transaction/lazada_order_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function delete(){
    	$this->load->model('transaction/lazada_order_import');
    	$this->model_transaction_lazada_order_import->delete($this->request->get['lazada_id']);
    	$this->redirect($this->url->link('transaction/lazada_order_import', 'token=' . $this->session->data['token'], 'SSL'));        
    }
    public function getInvoiceNo(){
		$salesLastId = $this->model_transaction_lazada_order_import->getSalesLastId();
		return $this->config->get('config_sales_prefix').date('ym').str_pad($salesLastId +1, 4, '0', STR_PAD_LEFT);
    }
    public function clearImportedOrders(){
    	$this->load->model('transaction/lazada_order_import');
    	$this->model_transaction_lazada_order_import->clearImportedOrders();
    	$this->redirect($this->url->link('transaction/lazada_order_import', 'token=' . $this->session->data['token'], 'SSL')); 
    }
	public function getRandomCustCode(){
    	$this->load->model('setting/customers');
	    $lastCustId = $this->model_setting_customers->getCustLastId();
		return 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);
	}

    public function updateShippingAddressFromInsertedByLazda()
    {
        $this->load->model('transaction/lazada_order_import');
        // $this->model_transaction_lazada_order_import->updateShippingAddressFromInsertedByLazda();
        
    }
}
?>