<?php
class ControllerTransactionSalesReturn extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales Return'));
		$this->load->model('transaction/sales_return');
		$this->clearPurchaseData();
		$this->getList();
	}

	public function insert() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales Return'));
		$this->load->model('transaction/sales_return');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['purchase_button']) && $this->validateForm()) {

			$data = array();
			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= $this->session->data['location_code'];
			}

			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}

			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}

			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}

			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}

			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}

			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}

			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
				$data['tax_type']   = $data['tax_type'] =='1' ? '0' : '1';
			} else {
				$data['tax_type'] = '';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '';
			}
			if (isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			} else {
				$data['shipping_id'] = '';
			}
			if (isset($this->request->post['currency_code'])) {
				$data['currency_code'] = $this->request->post['currency_code'];
			} else {
				$data['currency_code'] = '';
			}
			if (isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate'] = $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate'] = '';
			}

			$data['purchase_return'] = '1';
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'price'                     => $product['price'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$total_data       = array();
			$this->load->model('ajax/cart');
   			$apply_tax_type   = $data['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		    $total_data       = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	  = $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total']  = $total;
//			 printArray($data); die;
			$data['transaction_no']	        = $this->getTransNo();
			$this->model_transaction_sales_return->addSalesnew($data);
			$this->session->data['success'] = 'Success: You have added sales return!';
			$this->clearPurchaseData();
			$this->redirect($this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}

			$this->getForm();
		}

	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
 		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type = $this->config->get('config_apply_tax_purchase');
		}
		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		
		if(!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = $purchase['total'] - $purchase['tax'];
					} else {
					    $totalValue = $purchase['tax'] + $purchase['total'];
					}
				$purchase['price'] = str_replace("$","",$purchase['price']);
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center order-nopadding"><input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
	                   <td class="text-right order-nopadding">
	                   	<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace(",","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>';
					$str.='<td class="text-right">'.$purchase['raw_cost'].'</td>
					<td class="text-right" id="subTot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>   
					   <td align="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type == '2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format($res['sub_total_value']);
					}
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					$tax_text = 'GST';
					$tax_text = $apply_tax_type =='2' ? $tax_text."(7% Incl)" : $tax_text."(7% Excl)";
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="7">'.$tax_text.'</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	protected function getList() {
		
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('Sales Return');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Sales Return'),
			'href'      => $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$url = '';
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
			$url .= '&page='.$page;
		}
        if(!$page){
			$page = 1;
		}
		$this->data['insert'] = $this->url->link('transaction/sales_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		$purchase_total = $this->model_transaction_sales_return->getTotalPurchase($data);
		$results 		= $this->model_transaction_sales_return->getPurchaseList($data);
		
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$vendorDetail 	= array();
		$userDetail 	= array();

		foreach ($results as $result) {
			$vendorDetail = $this->model_transaction_sales_return->getB2BCustomersbyCode($result['customer_code']);
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			if($result['network_id'] !=''){
				$network = $this->model_transaction_sales_return->getNetworkName($result['network_id']);
			}else{
				$network = 'Show Room'; 
			}
			$this->data['purchases'][] = array(
				'pagecnt'      		=> ($page-1)*$this->config->get('config_admin_limit'),
				'purchase_id'       => $result['invoice_no'],
				'order_id'          => $result['id'],
				'network'           => $network,
				'transaction_no'    => $result['invoice_no'],
				'invoice_nom' 	    => $result['invoice_nom'],
				'xero_sales_id' 	=> $result['xero_sales_id'],
				'location'          => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['invoice_date'])),
				'transaction_type'  => $result['discount_type'],
				'vendor_code'       => $vendorDetail['customercode'],
				'vendor_name'       => $vendorDetail['name'],
				'total'             => $result['net_total'],
				'delivery_status'   => $result['delivery_status'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'modify_button' => $this->url->link('transaction/sales_return/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'view_button' => $this->url->link('transaction/sales_return/view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
			    'create_button' => $this->url->link('transaction/sales_return/updateInvoiceNumber', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'download_button' => $this->url->link('transaction/sales_return/download_pdf', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL')
			);
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		$hold_button = '&show_hold=1';
		$this->data['show_hold_button'] = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete']      = $this->url->link('transaction/sales_return/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');

		$pagination 		 = new Pagination();
		$pagination->total 	 = $purchase_total;
		$pagination->page 	 = $page;
		$pagination->limit 	 = $this->config->get('config_admin_limit');
		$pagination->text 	 = $this->language->get('text_pagination');
		$pagination->url 	 = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];
		$this->template 	 = 'transaction/sales_return_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function updateInvoiceNumber(){

		$this->load->model('transaction/sales_return');	
		if(isset($this->request->get['purchase_id'])){
	    	$invNo = $this->model_transaction_sales_return->updateInvoiceNumber($this->request->get['purchase_id']);
		}
		$this->redirect($this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	protected function getForm() {

 		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/customers');
		$this->load->model('transaction/sales_return');
		$this->load->model('setting/location');
		$this->document->setTitle('Sales Return');

		$this->data['heading_title'] = $this->language->get('Sales Return');
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_customer_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_customer_name');

		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_raw_cost'] = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
        $this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
         $apply_tax_type = 0;
		if (isset($this->request->get['purchase_id'])) {
			$purchaseInfo = $this->model_transaction_sales_return->getSalesnew($this->request->get['purchase_id']);
			// printArray($purchaseInfo); die;
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				$purchaseProducts = $this->model_transaction_sales_return->getSalesProductnew($purchaseInfo['invoice_no']);
					// printArray($purchaseProducts); exit;
				foreach ($purchaseProducts as $products) {
					$products['price']      = $products['sku_price'];
					$products['purchase_id']= $products['invoice_no'];
					$products['net_price']  = $products['net_total'];
					$products['tax_price']  = $products['gst'];

					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['qty'] > 0)) {
						$this->cart->update($products['product_id'], $products['qty'], $cart_type, $products);
					} elseif ($products['qty'] > 0) {
						$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
					}
					// printArray($this->session->data['purchase']); die;
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  $apply_tax_type = '1';//$this->model_transaction_sales_return->getVendorsTaxId($vendor_id);

		}
            
		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Return List',
			'href'      => $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Return',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
       
		if (!empty($purchaseInfo) || (!empty($this->request->post['invoice_no']))) {
			$this->data['action'] = $this->url->link('transaction/sales_return/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['invoice_no'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/sales_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
	
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['invoice_no'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();
		}
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['invoice_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		} 
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['customer_code'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_transaction_sales_return->getB2BCustomersbyCode($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $this->session->data['vendor'];
			$vendorData = $this->model_transaction_sales_return->getB2BCustomersbyCode($this->session->data['vendor']);
			$this->data['vendor_name'] = $vendorData['name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date("d/m/Y", strtotime($purchaseInfo['header_remarks']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		if (isset($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
			$this->data['tax_type'] = $this->data['tax_type'] == '0' ? '1':'2';
		} else {
			$this->data['tax_type'] = '';
		}
		if(isset($this->data['tax_type'])){
		}
		// echo $this->data['tax_type']; die;

		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}
		if (isset($this->request->post['shipping_id'])) {
			$this->data['shipping_id'] = $this->request->post['shipping_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['shipping_id'] = $purchaseInfo['shipping_id'];
		} else {
			$this->data['shipping_id'] = '';
		}
		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = '';
		}
		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}

		//product collection
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		$this->data['clocation_code'] = $this->session->data['location_code'];
		$this->data['Tolocations'] = $this->cart->getLocation();
		//if ($this->session->data['vendor']) {
		    // $this->data['products'] = $this->model_inventory_inventory->getProducts($data);
		//}
        //purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		$this->data['apply_tax_type']       = $apply_tax_type;
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		$this->data['allTerms'] 			= $this->cart->getAllTerms();
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		// Customer Collection
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);
		$this->data['currencys']   = $this->model_transaction_sales_return->getCurrency();
        $this->data['customer_collection'] = $this->model_transaction_sales_return->getB2BCustomers();
		$this->data['purchase_id'] = $this->request->get['purchase_id'];
		// printArray($this->session->data); die;

		$this->data['token']   	   = $this->session->data['token'];
		$this->data['cancel']      = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['route']   = 'transaction/sales_return';
		$this->template = 'transaction/sales_return_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getTransNo(){
		
	    $transNumberWithSuffix = $this->model_transaction_sales_return->getSalesAutoIdnew();
		// $transNumber		   = substr($transNumberWithSuffix, -6);
		// printArray($transNumberWithSuffix); exit;
		if($transNumberWithSuffix){
			 $transaction_no = $transNumberWithSuffix + 1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_return_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_sales_return_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	public function getForms() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('setting/customers');
		$this->data['heading_title'] = $this->language->get('Sales Return');
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['text_return_qty'] = $this->language->get('text_return_qty');
		$this->data['button_hold'] = $this->language->get('button_hold');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		if (isset($this->request->post['transaction_no'])) {
			$purchaseInfo = $this->model_transaction_sales_return->getPurchaseByTransNo($this->request->post['transaction_no']);
			if(empty($purchaseInfo)) {
				$this->error['warning'] = "Please enter valid transaction number.";
			}
		}

		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));

			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			if ($this->request->post['quantity'] <= $this->request->post['total_quantity']) {
				$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
				if(!empty($this->request->post['discount_percentage'])) {
					$this->data['hide_discount_percentage']	= '';
					$this->data['hide_discount_price']	= '1';
				} elseif (!empty($this->request->post['discount_price'])) {
					$this->data['hide_discount_percentage']	= '1';
					$this->data['hide_discount_price']	= '';
				}
			} else {
			    $this->error['warning'] = 'Please enter quantity below or equal '.$this->request->post['total_quantity'];
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}

 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}

		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}

		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}

		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}

		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Sales Return'),
			'href'      => $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['action'] = $this->url->link('transaction/sales_return/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['cancel'] = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}

		if (!empty($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
			$this->data['transaction_no'] = '';
		}

		if (!empty($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = $purchaseInfo['transaction_date'];
		} else {
			$this->data['transaction_date'] = '';
		}

		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}

		if (!empty($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->data['vendor_code'] = $this->request->post['vendor_code'];
			//$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			 $vendorData = $this->model_setting_customers->getCustomer($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['name'];
		} elseif (!empty($purchaseInfo)){
			$vendorData = $this->model_setting_customers->getCustomer($purchaseInfo['vendor_id']);
			$this->data['vendor'] = $vendorData['customer_id'];
			$this->data['vendor_code'] = $vendorData['customercode'];
			$this->data['vendor_name'] = $vendorData['name'];
		} else {
			$this->data['vendor'] = '';
			$this->data['vendor_code'] = '';
			$this->data['vendor_name'] = '';
		}

		if (!empty($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}

		if (!empty($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = $purchaseInfo['reference_date'];
		} else {
			$this->data['reference_date'] = '';
		}

		if (!empty($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}

		if (!empty($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}

		if (!empty($this->request->post['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->request->post['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}

		//product collection
		$this->load->model('inventory/inventory');
		$this->data['products'] = array();

		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $filter_vendor = $purchaseInfo['vendor_id'];
		} else {
			$filter_vendor = null;
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
				$productsInfo = $this->model_transaction_sales_return->getPurchaseProduct($this->request->post['purchase_id']);
				foreach ($productsInfo as $products) {
					$products['total_quantity'] = $products['quantity'];
					$this->data['rowdata'] = array_merge($products, $this->model_transaction_sales_return->getInventoryDetailsById($products['product_id']));
				}
			}
		}

		if (!empty($purchaseInfo)) {
			if (empty($this->session->data['bill_discount'])) {
				if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
			}
			$productsInfo = $this->model_transaction_sales_return->getPurchaseProduct($purchaseInfo['purchase_id']);
			foreach ($productsInfo as $products) {
				$this->data['products'][] = array_merge($products, $this->model_inventory_inventory->getInventoryDetailsById($products['product_id']));
			}
		}

		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type);
		$this->data['apply_tax_type']       = $this->config->get('config_apply_tax');

		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection();
		//Tax Class
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		// Weight Class
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		// Vendor Collection
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['route'] = 'transaction/sales_return';
		$this->data['purchase_id'] = $this->request->get['purchase_id'];
		
		echo $this->data['purchase_id']; die;

		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->template = 'transaction/sales_return_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function view(){

		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/sales_return');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle($this->language->get('Sales Return View'));
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Return List',
			'href'      => $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_sales->getSalesnew($this->request->get['purchase_id']);
		//printArray($this->data['purchaseInfo']); exit;
		$this->data['vendorDetail'] 	= $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);
         //printArray($this->data['vendorDetail']); exit;
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['createdby']);
		$this->data['productDetails'] 	= $this->model_transaction_sales->getSalesProductnew($this->data['purchaseInfo']['invoice_no']);
		$this->data['modify_button'] = $this->url->link('transaction/sales_return/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');
		$this->data['delete_button'] = $this->url->link('transaction/sales_return/delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');

		//print_r($this->data['purchaseInfo']); exit;
		$this->template = 'transaction/sales_return_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function update(){

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		$this->load->model('transaction/sales_return');
		$this->load->model('transaction/purchase');
		//printArray($this->request->post); exit;
		$type = 'update';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' || $this->request->post['purchase_button'] == 'hold') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->get['purchase_id'])) {
				$data['purchase_id']	=  $this->request->get['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['term_id'])) {
				$data['term_id']	=  $this->request->post['term_id'];
			} else {
				$data['term_id']	= '';
			}
			if(isset($this->request->post['shipping_id'])) {
				$data['shipping_id']	=  $this->request->post['shipping_id'];
			} else {
				$data['shipping_id']	= '';
			}
			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
				$data['tax_type']   = $data['tax_type'] =='1' ? '0' : '1';
			} else {
				$data['tax_type'] = '';
			}
			
			$product_data = array();
			foreach ($this->cart->getProducts($cart_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'sku_price'                 => $product['sku_price'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			// printArray($data); exit;
			$this->model_transaction_sales_return->editSalesnew($data['purchase_id'], $data);
			$this->session->data['success'] = 'Success: You have updated sales return!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	
	}
	protected function validateForm() {

		if (!$this->request->post['transaction_no']) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}

		if (!$this->request->post['transaction_date']) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}

		if (empty($this->request->post['vendor'])) {
			$this->error['vendor'] = "Please select any one vendor.";
		}

		if (!$this->request->post['reference_date']) {
			$this->error['reference_date'] = "Please enter reference date.";
		}

		// if (!$this->request->post['reference_no']) {
		// 	$this->error['reference_no'] = "Please enter reference no.";
		// }

		if (empty($this->session->data['cart_purchase'])) {
			$this->data['error_warning'] = "Please add product and save.";
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function getPurchaseCartDetails($cart_type) {
		$product_data = array();
		foreach ($this->cart->getProducts($cart_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode']
			);
		}
		return $product_data;
	}

	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$taxAmount = $this->cart->getTaxeByProduct($productDetails);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;

		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'total_quantity'            => $productDetails['total_quantity'],
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);
		return $rowData;
	}

	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}

	public function getDiscountCalculation($data, $type = '') {

		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}

		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}

	}
	public function download_pdf(){

		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');

		if($this->request->get['purchase_id'] !=''){
			$purchase_id = $this->request->get['purchase_id'];
		}else{
			$purchase_id = $id;
		}
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		
		$salesInfo = $this->model_transaction_sales->getSalesnew($purchase_id);
		$vendorDetail=$this->model_transaction_sales->getB2BCustomersbyCode($salesInfo['customer_code']);
		$userDetail = $this->model_user_user->getUser($salesInfo['createdby']);
		$productDetails	= $this->model_transaction_sales->getSalesProductnew($salesInfo['invoice_no']);
		// printArray($productDetails); die;
		$invoice_no       = SAL_INV_PREFIX.str_pad($salesInfo['invoice_nom'], 4, '0', STR_PAD_LEFT);
		$transaction_date = $salesInfo['invoice_date'];
		$reference_no     = $salesInfo['reference_no'];
		$reference_date   = $salesInfo['invoice_date'];
		$vendor_name      = $vendorDetail['name'];

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;"><img src="view/assets/img/side-top.png" style="width: 90px;height: 50px;"></td>
					<td align="right">Invoice No: '.$invoice_no.' <br> Date: '.$transaction_date.'</td></tr></table>';
			$str .='<table class="table" style="width:100%;">
				<tr>
					<td class="left"><h3>Delivery Address</h3></td>
					<td class="right"><h3>Billing Address</h3></td>
				</tr>
				<tr>
					<td class="left">'.$vendor_name.'</td>
					<td class="right">'.$vendor_name.'</td>
				</tr>
				<tr>
					<td class="left">Sample, Sample, <br>Sample</td>
					<td class="right">Sample, Sample, <br>Sample</td>
				</tr>
			</table>';
			$str .="<table class='listing' style='width:100%;'>
					<tr>
						<thead>
							<th>Invoice Number</th>
							<th>Invoice Date</th>
							<th>Order Reference</th>
							<th>Order Date</th>
						</thead>
					</tr>
					<tr>
						<td style='border-right: 1px solid black;'>".$invoice_no."</td>
						<td style='border-right: 1px solid black;'>".$transaction_date."</td>
						<td style='border-right: 1px solid black;'>".$reference_no."</td>
						<td>".$reference_date."</td>
					</tr>
				</table>";
				$str .="<table class='listing' style='width:100%;'><tr><td></td></tr></table><br>";
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Sno</th>
							<th>Product</th>
							<th>Unit Price (Tax excl)</th>
							<th>Qty</th>
							<th class='text-right'>Total (Tax excl)</th>
						</tr></thead><tbody>";

				$j=$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				$i= 1;
				foreach ($productDetails as $value) {
				$productName = $this->model_transaction_sales->getProductName($value['product_id']);
				$prodTotal  += $value['sku_price'];
				$totalandTax+= $value['gst'];
				$totals     += $value['net_total'];

					$str.="<tr>
		   				    <td>".$i."</td>
							<td>".$value['description']."</td>
							<td>".$value['sku_price']."</td>
							<td>".$value['qty']."</td>
							<td class='right'>".$value['actual_total']."</td>
						   </tr>";	
				$i++; }
				// $actualtotal = ($actualtotal-$tot_discount)+$tot_gst;
				$str.="<tr><td colspan='4' align='right'>Total Products</td>
						 <td class='right'>".number_format($prodTotal,2)."</td>
					   </tr>";
				$str.="<tr><td colspan='4' align='right'>Total (Tax incl.)</td>
						 <td class='right'>".number_format($totalandTax,2)."</td>
					   </tr>";
				$str.="<tr><td colspan='4' align='right'>Total</td>
						 <td class='right'>".number_format($totals,2)."</td>
					   </tr>";

				$str .= "</tbody></table>";
		// echo $str; die;

		if($str){
			$filename = 'Sales_Return_invoice_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function bulk_delete(){
		$this->load->model('transaction/sales_return');
		foreach ($this->request->post['selected'] as $purchase_id) {
			$this->model_transaction_sales_return->deletePurchase($purchase_id);
		}

		$this->session->data['success'] = 'Sales Details deleted Completed';
		if(isset($_REQUEST['filter_date_from'])){
			$pageUrl.= '&filter_date_from='.$_REQUEST['filter_date_from'];
		}
		if(isset($_REQUEST['filter_date_to'])){
			$pageUrl.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if(isset($_REQUEST['filter_location_code'])){
			$pageUrl.='&filter_location_code='.$_REQUEST['filter_location_code']; 
		}
		if(isset($_REQUEST['filter_supplier'])){
			$pageUrl.='&filter_supplier='.$_REQUEST['filter_supplier']; 
		}
		if(isset($_REQUEST['filter_transactionno'])){
			$pageUrl.='&filter_transactionno='.$_REQUEST['filter_transactionno']; 
		}

		$this->redirect($this->url->link('transaction/sales_return', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	
	} 
}
?>