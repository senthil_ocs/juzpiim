<?php
class ControllerTransactionServicePurchase extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/service_purchase');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/service_purchase');
		$this->clearPurchaseData();
		$this->getList();
	}
	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/service_purchase');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			    //$this->clearPurchaseData($remove = 'discount');
			}
			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'sku_qty'					=> $product['sku_qty'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			$this->response->setOutput(json_encode($res));
		}
	}
	public function getProductDetailsbyScan($data='', $type = '') {

		$this->load->model('transaction/service_purchase');
		$this->data['filter_name']   = $this->request->post['sku'];
		$this->data['location_code'] = $this->request->post['location_code'];
		$_productDetails 	= $this->model_transaction_service_purchase->getProductByNameNew($this->data);
		try {

		$str = '';
		if(count($_productDetails)>1){
			$str.='<ul id="country-list">';
			for($i=0;$i<count($_productDetails);$i++){
				if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
					$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
				}
                $name_display = $_productDetails[$i]['sku'].' ('.$_productDetails[$i]['name'].')';
                 
				$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']-$_productDetails[$i]['reserved_qty']).'||'.$_productDetails[$i]['hasChild'];
				$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($name_display)."</li>";
			}
			$str.='</ul>';
		}

		if(count($_productDetails)==1){
			$str.='<ul id="country-list">';
			for($i=0;$i<count($_productDetails);$i++){ 
				if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
					$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
				}
                $name_display = $_productDetails[$i]['name'];
                
				$var =  $_productDetails[$i]['product_id'].'||'.replaceSpecials($_productDetails[$i]['sku']).'||'.replaceSpecials($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']-$_productDetails[$i]['reserved_qty']).'||'.$_productDetails[$i]['hasChild'];
				$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($name_display).")</li>";
			}
			$str.='</ul>'; 
		}
		echo $str;
		}catch(\Exception $e) {
			return $e->getMessage();
		}
	}
	
	public function AjaxBillDiscountUpdate(){

		$cart_type = 'cart_purchase';
		$apply_tax_type = 0;
		$this->load->model('transaction/service_purchase');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal 		= $this->request->post['sub_total_value'];
			$apply_tax_type = $this->request->post['tax_type'];
			
			if($bill_discount_price=='' && $bill_discount_percentage==''){
				$bill_discount_price = 0;
				$bill_discount_percentage = 0;
			}

			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_percentage']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_price']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}else if($bill_discount_percentage=='0' || $bill_discount_price=='0'){
				if ($this->session->data['bill_discount']) {
					unset($this->session->data['bill_discount']);
					unset($this->session->data['bill_discount_mode']);
				}
				unset($this->session->data['bill_discount_percentage']);
				unset($this->session->data['bill_discount_price']);
			}
			if(isset($this->request->post['handling_fee']) && $this->request->post['handling_fee'] > 0){
				$this->session->data['handling_fee'] = $this->request->post['handling_fee'];
				$res['handling_fee']    = $this->currency->format($this->session->data['handling_fee']);
			}else{
				unset($this->session->data['handling_fee']);
				$res['handling_fee']    = $this->currency->format('0');
			}
		}
		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'sku_qty'					=> $product['sku_qty'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');

			$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type); 
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$showdiscountColumn = '0';
			
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				if($apply_tax_type=='2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$showdiscountColumn = '1';
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					
					$res['discount_price']  	 = $bill_discount_price;
					$res['discount_percentage']  = $bill_discount_percentage;

					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					$colspan = $this->request->post['form'] == 'PurInvc' ? 9 : 7;
					if($showdiscountColumn=='1')
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="'.$colspan.'" align="right" class="purchase_total_left">'.$lable.'</td>
						<td class="purchase_total_right insertproduct" colspan="1" align="right">
							          	<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="textbox row_total_box" style="text-align:right;"
							          	readonly="readonly" /></td></tr>';
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];

				
			}
			// printArray($res); die;
			$this->response->setOutput(json_encode($res));
	}
	public function ajaxaddproducts(){
		$this->load->model('transaction/service_purchase');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
			$vendor_id = $this->request->post['vendor'];
			$apply_tax_type = $this->request->post['tax_type'];;
           
            $productQty    = $this->request->post['quantity'];
            $productFocQty = $this->request->post['foc_quantity'];
            

		   if (isset($this->request->post['product_id']) && ($productQty > 0 || $productFocQty > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   //$this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));

			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 || $productFocQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0 || $productFocQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if($apply_tax_type=='0'){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}
		
		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->request->post['location_code']);

		if (!empty($cartPurchaseDetails)) { $i=1;
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) { 

					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					   	$totalValue = round($purchase['total'],2);
					} else {
					   // $totalValue = round($purchase['tax'] + $purchase['total'],1);
						 $totalValue = round($purchase['tax'] + $purchase['total'],2);
					}

				$purchasediscount = '0';
				if (($purchase['discount_mode'] == '1') && !empty($purchase['discount'])){
					$purchasediscount = $purchase['discount'];
				}
				$purchase['price'] = str_replace("$","",$purchase['price']);
				$purchase_code = "'".$purchase['code']."'";
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					   <td class="center">'.$i++.'</td>
					   <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center order-nopadding">
	                   		<input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].','.$purchase_code.');">
	                   </td>';
	                   if($this->request->post['isConverted']){
	                   	$str .='<td class="center">'.$purchase['quantity'].'</td>';
	                   }

	                   $str .='<td class="center">'.$purchase['foc_quantity'].'</td>
	                   <td class="text-right order-nopadding">
	                   		<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace(",","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].','.$purchase_code.');">
	                   </td>
		               <td class="text-right">'.$purchase['sku_qty'].'</td>';
				if (($purchase['discount_mode'] == '1') && !empty($purchase['discount'])){
					$str.=$purchase['discount'];
				}
					$str.='</td>';
					/*$str.='<td class="text-right">';
				if (($purchase['discount_mode'] == '2') && !empty($purchase['discount'])){
					$str.=$purchase['discount'];
				}
					$str.='</td>';*/
				$str.='<td class="text-right" id="subTot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>
					   <td class="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i> </a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
				$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type']; 
				$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					if($apply_tax_type=='2'){
						$res['sub_total_value'] = $data['totals'][0]['value'];
						$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
					}
					$cnt = count($data['totals'])-2;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					$colspan = $this->request->post['form'] == 'PurInvc' ? 9 : 7;
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="'.$colspan.'">GST </td>
	                                                <td class="purchase_total_right insertproduct" colspan="1">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	public function insert() {
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/service_purchase');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/service_purchase');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit') && $this->validateForm()) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}

			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '0';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '0';
			}
			if(isset($this->request->post['handling_fee'])) {
				$data['handling_fee']	=  $this->request->post['handling_fee'];
			} else {
				$data['handling_fee']	= '0';
			}
			if(isset($this->request->post['total'])) {
				$data['bill_total']	=  $this->request->post['total'];
			} else {
				$data['bill_total']	= '0';
			}

			if(isset($this->request->post['tax'])) {
				$data['bill_tax']	=  $this->request->post['tax'];
			} else {
				$data['bill_tax']	= '0';
			}

			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			} else {
				$data['currency_code']	= '';
			}
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '1';
			}
			
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			if (isset($this->request->post['remarks'])) {
				$data['remarks'] = $this->request->post['remarks'];
			} else {
				$data['remarks'] = '1';
			}
			$product_data = array();
			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'hasChild'                  => $product['hasChild'],
					'sku'                  		=> $product['sku'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'description'               => $product['description'],
					'total'                     => $product['total'],
					'foc_quantity'              => $product['foc_quantity']
				);
			}
			$data['products'] = $product_data;

			$total_data = array();
			$this->load->model('ajax/cart');

			$vendor_id = $this->request->post['vendor'];
			// if($apply_tax_type=='0'){
			// 	$apply_tax_type = $this->config->get('config_apply_tax_purchase');
			// }
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total']  = $total;
			$costMethod 	= $this->config->get('config_average_cost_method');
		    $data['config_vendor_update'] 	= $this->config->get('config_vendor_update');
            $data['transaction_no'] 		= $this->getTransNo();
//            printArray($data); die;
		    $this->model_transaction_service_purchase->addPurchase($data,$costMethod);
			$this->session->data['success'] = 'Success: You have added service purchase order!';
			$this->clearPurchaseData();
			$this->redirect($this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}
	public function update() {

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/service_purchase');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/service_purchase');
		$this->load->model('inventory/reports');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && (($this->request->post['purchase_button'] == 'submit') || ($this->request->post['purchase_button'] == 'hold') ) && $this->validateForm($type='update')) {
			$data = array();

			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['handling_fee'])) {
				$data['handling_fee']	=  $this->request->post['handling_fee'];
			} else {
				$data['handling_fee']	= '';
			}
			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			} else {
				$data['currency_code']	= '';
			}
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '1';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			$product_data = array();
			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type']; 
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],					
					'hasChild'                  => $product['hasChild'],
					'sku'                  		=> $product['sku'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'foc_quantity'              => $product['foc_quantity'],
					'description'               => $product['description']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			// printArray($data); die;
			$this->model_transaction_service_purchase->editPurchase($data['purchase_id'], $data,$costMethod);
			$this->session->data['success'] = 'Success: You have modified purchase!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($this->request->get['filter_date_from'])) {
				$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
			}
			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}
			if (isset($this->request->get['filter_location_code'])) {
				$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
			}
			if (isset($this->request->get['filter_supplier'])) {
				$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
			}
			if (isset($this->request->get['filter_transactionno'])) {
				$url .= '&filter_transactionno=' . $this->request->get['filter_transactionno'];
			}
			$this->session->data['success'] = 'Success: You have modified service purchase order!';
			$this->redirect($this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {

		$this->load->model('setting/location');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_location'] = $this->language->get('text_location');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
			$this->data['hold'] = 0;
		}

		$pageUrl ='';

		if($this->session->data['location_code']!='HQ'){
			$_REQUEST['filter_location_code'] = $this->session->data['location_code']; 
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}
		
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.='&filter_location_code='.$filter_location_code; 
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.='&filter_supplier='.$filter_supplier; 
		}else{
			$filter_supplier = null;
		}
		$filter_transactionno = null;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			// $url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			// $url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Service Note',
			'href'      => $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$strUrl = '&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to.'&filter_location_code='.$filter_location_code;
		$hold_button = '&show_hold=1';
		$this->data['insert'] = $this->url->link('transaction/service_purchase/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['show_hold_button'] = $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url.$hold_button, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	           => $show_hold,
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'filter_supplier'	   => $filter_supplier,
			'filter_transactionno' => $filter_transactionno,
			'filter_location_code' => $filter_location_code,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		$purchase_total = $this->model_transaction_service_purchase->getTotalPurchase($data);
		$results = $this->model_transaction_service_purchase->getPurchaseList($data);
		// printArray($data); die;
		$this->data['data']      = $data;
		$this->data['suppliers'] = $this->model_transaction_service_purchase->getVendors();

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();

		foreach ($results as $result) {
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$userDetail   = $this->model_user_user->getUser($result['created_by']);
			$status = $this->model_transaction_service_purchase->checkPurchaseCovertversion($result['purchase_id']);
			
			$convert_status = 'Pending';
			if($status['total_records'] == $status['completed_records']){
				$convert_status = 'Completed';
			}
			else if($status['partial_records'] >= 1){
				$convert_status = 'Partial';
			}
			else if($status['total_records'] >= 1){
				$convert_status = 'Pending';
			}
			if($result['deleted']){
				$result['isinvoice'] = $result['deleted']; 
				$convert_status = 'Canceled';
			}
			$this->data['purchases'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'), 
				'purchase_id'       => $result['purchase_id'],
				'delivery_status'       => $result['delivery_status'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => $result['transaction_date'],
				'transaction_type'  => $result['transaction_type'],
				'reference_no'  	=> $result['reference_no'],
				'reference_date'	=> $result['reference_date'],
				'location_code' 	=> $result['location_code'],
				'vendor_code'       => $vendorDetail['vendor_code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'total'             => $result['total'],
				'fc_nettotal'       => $result['fc_nettotal'],
				'invoice_no'        => $result['isinvoice'],
				'xero_purchase_id'  => $result['xero_purchase_id'],
				'status'  			=> $convert_status,
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['created_date'],
				'modify_button'     => $this->url->link('transaction/service_purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $pageUrl.$url, 'SSL'),
				'view_button'       => $this->url->link('transaction/service_purchase/view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $pageUrl.$url, 'SSL'),
				'delete_button'     => $this->url->link('transaction/service_purchase/delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] .$pageUrl.$url, 'SSL'),
				'download_button'   => $this->url->link('transaction/service_purchase/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$result['purchase_id'] .$pageUrl.$url, 'SSL'),
				'invoice_button'   => $this->url->link('transaction/service_purchase/updateInvoiceNumber', 'token=' . $this->session->data['token'].'&purchase_id='.$result['purchase_id'] . $url, 'SSL')
			);
		}
		
		$this->data['Tolocations'] 			= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $filter_location_code;
		$this->data['filter_transactionno'] = $filter_transactionno;
		$this->data['filter_supplier'] 		= $filter_supplier;
		$this->data['delete']    			= $this->url->link('transaction/service_purchase/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');

		$pagination 		= new Pagination();
		$pagination->total 	= $purchase_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] .$pageUrl. $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['route']= $this->request->get['route'];

		$this->template 	= 'transaction/service_purchase_list.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {
		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/location');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] = $this->language->get('entry_tran_dt');
		$this->data['entry_location'] = $this->language->get('entry_location');
		$this->data['entry_tran_type'] = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name'] = $this->language->get('entry_vendor_name');
		$this->data['entry_refno'] = $this->language->get('entry_refno');
		$this->data['entry_refdate'] = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name'] = $this->language->get('text_product_name');
		$this->data['text_qty'] = $this->language->get('text_qty');
		$this->data['text_price'] = $this->language->get('text_price');
		$this->data['text_raw_cost'] = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price'] = $this->language->get('text_net_price');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] = $this->language->get('text_tax_class');
		$this->data['text_no_data'] = $this->language->get('text_no_data');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['text_remove'] = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] = $this->language->get('text_hold');
		$this->data['button_hold'] = $this->language->get('button_hold');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action'] = $this->language->get('column_action');
		$apply_tax_type = 0;
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($this->request->get['filter_date_from'])) {
				$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
			}
			if (isset($this->request->get['filter_date_to'])) {
				$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
			}
			if (isset($this->request->get['filter_location_code'])) {
				$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
			}
			if (isset($this->request->get['filter_supplier'])) {
				$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
			}
			if (isset($this->request->get['filter_transactionno'])) {
				$url .= '&filter_transactionno=' . $this->request->get['filter_transactionno'];
			}

		//Get Porequest details to convert 03-03-2020
		if(isset($this->request->get['poreq_num']) && $this->request->get['poreq_num'] !=''){
			$this->data['poreq_num'] 	  = $this->request->get['poreq_num'];
			$this->data['productDetails'] =$this->model_transaction_service_purchase->get_po_req_detail($this->data['poreq_num']);
			$vendorcode  = $this->data['productDetails']['header'][0]['Vendor_Code'];
			$this->data['vendors'] 		  = $this->model_master_vendor->getvendor_byCode($vendorcode);
			$this->data['vendor'] 		  = $this->data['vendors'][0]['vendor_id'];
			$this->data['vendor_name'] 	  = $this->data['vendors'][0]['vendor_name'];
			$this->session->data['vendor']	    	= $this->data['vendor'];
			$this->session->data['location_code'] 	= $this->data['productDetails']['header'][0]['location_code'];
			$taxclassId = $this->model_transaction_service_purchase->gettax_bycompany(); //get tax class id by company id
			//printArray($this->data['productDetails']);exit;
			foreach ($this->data['productDetails']['details'] as $products){
				$product = $this->model_transaction_service_purchase->getproductbySKU($products['Sku'],$this->session->data['location_code']);
				if(!empty($product)){
					$product_data = array(
						'product_id'    => $product['product_id'],
						'code'          => $products['Sku'],
						'name'          => $products['Description'],
						'quantity'      => (int)$products['Qty'],
						'neg_quantity'  => (int)$products['Qty'],
						'price'         => $product['average_cost'],
						'raw_cost'      => $product['price'],
						'description'       => $products['description'],
						'sku_qty'			=> $product['quantity'],
						'tax_class_id'		=> $taxclassId['tax_class_id'],
						'vendor_allow_gst'	=> $this->data['vendors'][0]['gst'],
						'vendor'			=> $this->data['vendor'],
						'foc_quantity'  	=> '0'
					);
				 	$this->request->post = $product_data;
					if(array_key_exists($product['product_id'], $this->session->data['cart_purchase'])){
					 	//update no need 
					}else{
						$this->cart->add($product['product_id'], (int)$products['Qty'], '', '', '', $cart_type, $this->request->post);
					}
				}
			}
		}

		if (isset($this->request->get['purchase_id'])) {

			$purchaseInfo = $this->model_transaction_service_purchase->getPurchase($this->request->get['purchase_id']);
			$apply_tax_type = $this->model_transaction_service_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				if($purchaseInfo['handling_fee'] > 0){
					$this->session->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
				}
				$purchaseProducts = $this->model_transaction_service_purchase->getPurchaseProduct($purchaseInfo['purchase_id']);
				// printArray($purchaseProducts); exit;
				foreach ($purchaseProducts as $products) {
					$products = array_merge($products, $this->getDiscountCalculation($products));
					$products['foc_quantity'] = (int)$products['foc'];
					if(!empty($this->session->data['cart_purchase'])){
						if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0 || $products['foc_quantity'] > 0)) {
							$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
						} elseif ($products['quantity'] > 0  || $products['foc_quantity'] > 0) {
							$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
						}
					} elseif ($products['quantity'] > 0  || $products['foc_quantity'] > 0) {
						$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
					}
				}
			}
		}
		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  $apply_tax_type = $this->model_transaction_service_purchase->getVendorsTaxId($vendor_id);

		}

		$this->load->model('setting/company');
		$this->data['Tolocations'] = $this->cart->getLocation();
		/*if(isset($this->request->post['location_code'])){			
			$this->session->data['location_code'] = $this->request->post['location_code'];
		}*/
		
		//$this->data['clocation_code'] = $this->session->data['location_code'];
		//$this->data['Tolocations'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);

		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}


		if (isset($this->error['transaction_no'])) {
			$this->data['error_transaction_no'] = $this->error['transaction_no'];
		} else {
			$this->data['error_transaction_no'] = '';
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_transaction_date'] = $this->error['transaction_date'];
		} else {
			$this->data['error_transaction_date'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_reference_date'] = $this->error['reference_date'];
		} else {
			$this->data['error_reference_date'] = '';
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_reference_no'] = $this->error['reference_no'];
		} else {
			$this->data['error_reference_no'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Service Note',
			'href'      => $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Service',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!empty($purchaseInfo) || (!empty($this->request->post['purchase_id']))) {
			$this->data['action'] = $this->url->link('transaction/service_purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $purchaseInfo['purchase_id'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('transaction/service_purchase/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
		} else {
				// Ragu
			 	 
			$this->data['transaction_no'] = $this->getTransNo();
				/*$transNumberWithSuffix = $this->model_inventory_inventory->getServicePurchaseAutoId();
				if($transNumberWithSuffix){
					 $transaction_no = ltrim($transNumberWithSuffix,'HQ')+1;
					 $this->data['transaction_no'] = 'HQ'.$transaction_no;
				}else{
					$transNumber = $this->model_inventory_inventory->getInventoryAutoId();
					if (empty($purchasePrefix)) {
						$this->data['transaction_no'] = 'TRANS'.$transNumber['sku'];
					} else {
					    $this->data['transaction_no'] = trim($purchasePrefix).($transNumber['sku']+1);
					}
				} */
		}

		if (isset($this->request->post['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->post['purchase_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['purchase_id'] = $purchaseInfo['purchase_id'];
		} else {
			$this->data['purchase_id'] = '';
		}
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['transaction_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		
		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}
		// echo $this->data['tax_type']; die;
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['vendor_id'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $this->session->data['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->data['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date('d/m/Y',strtotime($purchaseInfo['reference_date']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = $this->session->data['location_code'];
		}
		
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		if (isset($this->session->data['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->session->data['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}

		if (isset($this->session->data['handling_fee'])) {
			$this->data['handling_fee'] = $this->session->data['handling_fee'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
		} else {
			$this->data['handling_fee'] = '';
		}

		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = $this->model_transaction_service_purchase->getcompanyCurrency();
		}
		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}

		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}

	    // $this->data['products'] = $this->model_inventory_inventory->getProducts($data); //7-Aug-20
		$this->data['products'] = array();
		// printArray($this->data['products']); die;
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'location_code'   => $this->data['location_code'],
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->data['location_code']);
		$this->data['apply_tax_type'] = $apply_tax_type;
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']	= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		// printArray($this->data['purchase_totals']); die;

		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$this->data['currencys'] = $this->model_transaction_service_purchase->getCurrency();

		$this->load->model('localisation/weight_class');
		$this->data['weight_classes']    = $this->model_localisation_weight_class->getWeightClasses();
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		$this->data['allTerms'] = $this->cart->getAllTerms();
		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['convert_invoice'] = '';
		if(isset($this->request->get['purchase_id']) && !$purchaseInfo['isinvoice']){
			$this->data['convert_invoice'] = $this->url->link('transaction/service_purchase_invoice/insert', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].'&type=purConvrt', 'SSL');
		}

		$this->data['route'] = 'transaction/service_purchase';
		$this->template = 'transaction/service_purchase_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_no'])) {
			$this->error['warning'] = "Please enter transaction no.";
		}
		if (empty($this->request->post['transaction_date'])) {
			$this->error['warning'] = "Please enter transaction date.";
		}
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one vendor.";
		}
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if($type!='update'){
			$purchaseInfo = '';//$this->model_transaction_service_purchase->getPurchaseByTransNo($this->request->post['transaction_no']);
			if (!empty($purchaseInfo)) {
				$this->error['warning'] = "Please enter unique transaction number.";
			}
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='',$location_code) {
		 //printArray($this->session->data); 
		$product_data = array();
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		$pro = $this->cart->getProducts($cart_type,$apply_tax_type);
		// printArray($pro); exit;
		foreach ($pro as $product) {
           $SkuQtyDetails = $this->model_transaction_service_purchase->getQtyDetailsByLocation($product['sku'],$location_code);
           $Reservedstock = $this->model_transaction_service_purchase->getReservedStockdetails($location_code,$product['sku']); 
		    $product['sku_qty'] = $SkuQtyDetails['sku_qty']-$Reservedstock['reserved_qty']; 

			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'sku_qty'   => $product['sku_qty'],
				'price'      => $this->currency->format4Decimal($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode'],
				'foc_quantity'   => $product['foc_quantity'],
				'description'   => $product['description']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$this->load->model('transaction/service_purchase');
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$sku_qty = $productDetails['sku_qty'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		
		if($productDetails['vendor_allow_gst']=='1'){
			$apply_tax_type = $this->model_transaction_service_purchase->getVendorsTaxId($productDetails['vendor']);
		}else{
			$apply_tax_type = 0;
		}

		if($apply_tax_type=='1'){
			$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		}else{
			$taxAmount = 0;
		}
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'sku_qty'					=> $sku_qty,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			unset($this->session->data['handling_fee']);
			unset($this->session->data['bill_discount_percentage']);
			unset($this->session->data['bill_discount_price']);

			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data='', $type = '') {
			$this->load->model('transaction/service_purchase');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_service_purchase->getProductByName($this->data);
			$str = '';
			if(count($_productDetails)>=1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}

	public function getProductSkuDetails()
	{
		$this->load->model('transaction/service_purchase');
		$id = $this->request->post['id'];
		$SkuProductDetails = $this->model_transaction_service_purchase->getproductdetails($id);
		if($SkuProductDetails['average_cost']=='0.0000'){
					$SkuProductDetails['average_cost'] =' ';
		}
		echo json_encode($SkuProductDetails);
	}
	public function getTransNo(){
		
		$transNumberWithSuffix = $this->model_inventory_inventory->getServicePurchaseAutoId();
		$transNumber		   = $transNumberWithSuffix;
		if($transNumber){
			 $transaction_no = ltrim($transNumber)+1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_purchase_prefix') .date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1; // $this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_sales_purchase_prefix') .date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	
	public function view(){
		$this->language->load('transaction/service_purchase');
		$this->load->model('transaction/service_purchase');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle('Service Note Details');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url .= '&filter_transactionno=' . $this->request->get['filter_transactionno'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Service Note List',
			'href'      => $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Service Note View',
			'href'      => $this->url->link('transaction/service_purchase/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] 	= $this->model_transaction_service_purchase->getPurchaseDetailsById($this->request->get['purchase_id']);
		// printArray($this->data['purchaseInfo']); exit;
		$this->data['vendorDetail'] 	= $this->model_master_vendor->getVendor($this->data['purchaseInfo']['vendor_id']);
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['created_by']);
		$this->data['purchaseDetail'] 	= $this->model_transaction_service_purchase->getPurchaseDetails($this->data['purchaseInfo']['purchase_id']);
		$this->data['convert_invoice'] = $this->url->link('transaction/service_purchase_invoice/insert', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'].'&type=purConvrt','SSL');
		$this->data['delete_button'] = $this->url->link('transaction/service_purchase/delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');

		$this->data['productDetails'] 	= $this->model_transaction_service_purchase->getProductDetailsById($this->data['purchaseInfo']['purchase_id'],$this->data['purchaseInfo']['transaction_no']);
		$this->data['download_button']=$this->url->link('transaction/service_purchase/download_pdf','token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url,'SSL');

		$this->template = 'transaction/service_purchase_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function ajaxUpdateqtyAndPrice(){
		$this->load->model('transaction/service_purchase');	
		$this->load->model('ajax/cart');
		$apply_tax_type = 0;
		
		$sku 	   		= $this->request->post['sku']; 
		$productQty 	= $this->request->post['quantity'];
		$price     		= $this->request->post['price'];		
		$tax_class_id 	= $this->request->post['tax_class_id'];
		$discount 				  = 0;
		$bill_discount_percentage = $this->request->post['bill_discount_percentage'];
		$bill_discount_price 	  = $this->request->post['bill_discount_price'];

		$netPrice  = $productQty * $price;
		if(isset($bill_discount_percentage) && $bill_discount_percentage < 100){
			$dicount = $netPrice / $bill_discount_percentage;
		}else{
			$dicount = $bill_discount_percentage;
		}

		$netPrice  = $netPrice - $discount;
		$data['netPrice'] = $netPrice;
		$data['price']    = $price;
		$data['total']    = $total;

		$netPrice  = $this->currency->format($netPrice);
		$total     = $this->currency->format($total);

		$cart_type = 'cart_purchase';
 		$productId = $this->request->post['product_id'];
 		if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 )) {
			$this->cart->update($productId, $productQty, $cart_type, $this->request->post);
		}

 		$apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
 		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->request->post['location_code']);
 		$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
 		// printArray($total_data); die;
 		
 			foreach($total_data as $totals) {
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}

			$data['totals'] = $total_data;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				if($tax_class_id=='1'){
					$res['tax'] 	  ='';
					$res['tax_value'] ='';
				}

				if($apply_tax_type == '2'){
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-2;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
			}
		$colspan = $this->request->post['form'] == 'PurInvc' ? 9 : 7;
		$data = array('qty'		=> $productQty, 
					'price'		=> $price, 
					'netPrice'	=> $netPrice, 
					'total'		=> $total, 
					'discnt'	=> $discnt,
					'sku'		=> $productId,
					'orderSubTotal'	=> $res['sub_total'],
					'orderTax'		=> $res['tax'],
					'ordertax_value'=> $res['tax_value'],
					'tax_type'		=> $apply_tax_type,
					'orderDiscount'	=> $res['discount'],
					'orderTotal'	=> $res['total'],
					'tax_str' 		=> '<tr id="TRtax">
		                <td align="right" class="purchase_total_left" id="tax_title" colspan="'.$colspan.'"></td>
		                <td class="purchase_total_right insertproduct">
		                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$res['tax'].'" id="tax" name="tax" style="text-align:right;">
		                </td>
		            </tr>');
		$this->response->setOutput(json_encode($data));
	}
	public function checkCompanyCurrency(){
		$this->load->model('transaction/service_purchase');	
		
		$data['currency'] = $this->request->post['currency'];
		$details = $this->model_transaction_service_purchase->checkCompanyCurrency($data);
		if(!empty($details)){
			$response = 1;
		}else{
			$response = 0;
		}
		$this->response->setOutput(json_encode($response));
	}
	public function updateInvoiceNumber(){
		$this->load->model('transaction/service_purchase');	
		if(isset($this->request->get['purchase_id'])){
			$invNo = $this->model_transaction_service_purchase->updateInvoiceNumber($this->request->get['purchase_id']);
		}
		$this->redirect($this->url->link('transaction/service_purchase','token='.$this->session->data['token'] . $url, 'SSL'));
	}
	public function delete(){

		$this->load->model('transaction/service_purchase');	
		if(isset($this->request->get['purchase_id'])){
			$this->model_transaction_service_purchase->updateStatus($this->request->get['purchase_id']);
		}
		$this->session->data['success'] = 'Service Note Details Updated!';
			$this->redirect($this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function download_pdf(){

		$this->language->load('transaction/service_purchase');
		$this->load->model('transaction/service_purchase');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');
		
		$purchase_id = '';
		if($this->request->get['purchase_id'] !=''){
			$purchase_id = $this->request->get['purchase_id'];
		}

		$company_details = $this->model_inventory_reports->getCompanyDetails($this->session->data['company_id']);
		$company_details['purchase_title'] = 'Service Note';
		$headerStr	      = $this->url->purchasePdfHeaderString($company_details);
		$purchaseInfo     = $this->model_transaction_service_purchase->getPurchaseDetailsById($purchase_id);
		$vendorDetail     = $this->model_master_vendor->getVendor($purchaseInfo['vendor_id']);
		$userDetail       = $this->model_user_user->getUser($purchaseInfo['created_by']);
		$purchaseDetail   = $this->model_transaction_service_purchase->getPurchaseDetails($purchaseInfo['purchase_id']);
		$productDetails   = $this->model_transaction_service_purchase->getProductDetailsById($purchaseInfo['purchase_id'],$purchaseInfo['transaction_no']);

		$invoice_no       = $purchaseInfo['transaction_no'];
		$transaction_date = date('Y/m/d',strtotime($purchaseInfo['transaction_date']));
		$reference_no     = $purchaseInfo['reference_no'];
		$reference_date   = date('Y/m/d',strtotime($purchaseInfo['reference_date']));
		$vendor_name      = $vendorDetail['vendor_name'];		
		
		$tax_type = $purchaseInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
        $tax_type = $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)';
        $vendorDetail['address1']=$vendorDetail['address1'] !='' ? '<br>'.$vendorDetail['address1'] : '';
        $vendorDetail['country']=$vendorDetail['country'] !='' ? '<br>'.$vendorDetail['country'] : '';
        $vendorDetail['postal_code'] = $vendorDetail['postal_code'] !='' ? 'Phone: '.$vendorDetail['postal_code'] : '';
        $vendorDetail['fax'] = $vendorDetail['fax'] !='' ? ',Fax: '.$vendorDetail['fax'] : '';
        /*printArray($productDetails);die;*/
		$str = $headerStr;
		$str .='<table style="width:100%;">
				<tr>
					<td style="width:285px;">
					<table class="custmtable addrstbl">
						<tr>
							<td colspan="2" align="left" style="font-size:15px;font-weight:bold;    border-bottom: 1px solid black;">Vendor:</td>
						</tr>
						<tr>
							<td>'.$vendorDetail['vendor_name'].$vendorDetail['address1'].$vendorDetail['country'].'</td>
						</tr>
						<tr>
							<td colspan="2" align="left" style="font-size:15px;font-weight:bold;">
							Contact:</td>
						</tr>
						<tr>
							<td>'.$vendorDetail['postal_code'].$vendorDetail['fax'].'</td>
						</tr>
					</table>
					</td>

					<td style="width:285px;">
					<table class="custmtable addrstbl">
						<tr>
							<td style="font-size:12px;font-weight:bold;">Service Note No</td>
							<td align="left">'.$invoice_no.'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Date</td>
							<td align="left">'.date('d/m/Y',strtotime($transaction_date)).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Currency</td>
							<td align="left">'.$purchaseInfo['currency_code'].'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Remarks</td>
							<td align="left">'.$purchaseInfo['delivery_remarks'].'</td>
						</tr>
                        <tr>
                            <td style="font-size:12px;font-weight:bold;">Reference No</td>
                            <td align="left">'.$purchaseInfo['reference_no'].'</td>
                        </tr>';
						$str.='<tr style="height: 15px;"><td>&nbsp;</td><td>&nbsp;</td></tr>';
						if($vendorDetail['address1'] !=''){
							$str.='<tr style="height: 15px;"><td>&nbsp;</td><td>&nbsp;</td></tr>';
						}
					$str.='</table>
					</td>
				</tr>
			</table>';
			$str.="<table class='customtable'><tbody>
					<tr>
						<th style='font-weight:bold; width:5%;'>Sno</th>
						<th style='font-weight:bold; width:20%;'>SKU</th>
						<th style='font-weight:bold; width:65%;'>Description</th>
						<th style='font-weight:bold; width:20%;'>Qty</th>
					</tr>";

				$j=$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				$i= 1;
				$count = count($productDetails);
				foreach ($productDetails as $value) {    
					$str.="<tr>
		   				    <td align='center'>".$i."</td>
							<td align='left'>".$value['sku']."</td>
							<td align='left'>".nl2br($value['description'])."</td>
							<td align='right'>".$value['qty']."</td>
						   </tr>";
				    $i++;
                }

				$result = fmod($count,28);
				$counts =  24 - $result;
				for ($i=0; $i < $counts; $i++) { 
					$str .='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
				}
			$str .= "</tbody></table>";
			$str .='<table class="custmtable" style="border:none;">
				<tr>
					<td align="center" colspan="2"><p style="font-size:17px;font-weight:bold;"></p></td>
				</tr>
				<tr>
					<td>
    					<table class="custmtable">
                            <tr>
                                <td colspan="2" style="font-size:12px;font-weight:bold;">Add on customer details</td>
                            </tr>
                            <tr>
                                <td style="font-size:12px;font-weight:bold;">Name</td>
                                <td align="right">65432154</td>
                            </tr>
                            <tr>
                                <td style="font-size:12px;font-weight:bold;">Address</td>
                                <td align="right">32165487856</td>
                            </tr>
                            <tr>
                                <td style="font-size:12px;font-weight:bold;">Contact No</td>
                                <td align="right">89407496654</td>
                            </tr>
                        </table>
					</td>
					<td align="right">
    					<table class="custmtable">
    						<tr>
    							<td style="font-size:12px;font-weight:bold;">SGD</td>
    							<td align="right">'.$this->currency->format($purchaseInfo['sub_total']).'</td>
    						</tr>
    						<tr>
    							<td style="font-size:12px;font-weight:bold;">Discount</td>
    							<td align="right">'.$this->currency->format($purchaseInfo['discount']).'</td>
    						</tr>
    						<tr>
    							<td style="font-size:12px;font-weight:bold;">Delivery Fees</td>
    							<td align="right">'.$this->currency->format($purchaseInfo['handling_fee']).'</td>
    						</tr>
    						<tr>
    							<td style="font-size:12px;font-weight:bold;">GST '.$tax_type.'</td>
    							<td align="right">'.$this->currency->format($purchaseInfo['gst']).'</td>
    						</tr>
    						<tr>
    							<td style="font-size:12px;font-weight:bold;">Net Total</td>
    							<td align="right">'.$this->currency->format($purchaseInfo['total']).'</td>
    						</tr>
    					</table>
					</td>
				</tr>
			</table></body>';
		// echo $str; die;
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		if($str){
			$filename = 'ServiceNote_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function bulk_delete(){
		$this->load->model('transaction/service_purchase');
		// foreach ($this->request->post['selected'] as $purchase_id) {
			// this is for bulk delete 
		// }
		$purchase_id = $this->request->get['purchase_id'];
		$this->model_transaction_service_purchase->deletePurchase($purchase_id);

		$this->session->data['success'] = 'Service Note Details deleted Completed';
		if(isset($_REQUEST['filter_date_from'])){
			$pageUrl.= '&filter_date_from='.$_REQUEST['filter_date_from'];
		}
		if(isset($_REQUEST['filter_date_to'])){
			$pageUrl.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if(isset($_REQUEST['filter_location_code'])){
			$pageUrl.='&filter_location_code='.$_REQUEST['filter_location_code']; 
		}
		if(isset($_REQUEST['filter_supplier'])){
			$pageUrl.='&filter_supplier='.$_REQUEST['filter_supplier']; 
		}
		if(isset($_REQUEST['filter_transactionno'])){
			$pageUrl.='&filter_transactionno='.$_REQUEST['filter_transactionno']; 
		}

		$this->redirect($this->url->link('transaction/service_purchase', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	}
	public function getOneCartDetail(){
 		
 		$pro = $this->cart->getProducts('cart_purchase');
 		foreach ($pro as $value) {
	 		$data[] = array('id'=>$value['product_id'],'code'=>$value['sku']);
 		}
		$this->response->setOutput(json_encode($data));
	}
	public function getVendorCurrency(){
		$this->load->model('transaction/service_purchase');
		$vendor = $this->request->post['vendor'];
		$currency_code	= $this->model_transaction_service_purchase->getVendorCurrency($vendor);
		$this->response->setOutput($currency_code);
	}
	public function getVendorTerm(){
		$this->load->model('transaction/service_purchase');
		$vendor = $this->request->post['vendor'];
		$data['term']	= $this->model_transaction_service_purchase->getVendorTerm($vendor);
		$data['status'] = $data['term'] !='0' ? 1 : 0;
		$this->response->setOutput(json_encode($data));
	}
}
?>