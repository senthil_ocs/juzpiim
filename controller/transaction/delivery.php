<?php
class ControllerTransactionDelivery extends Controller { // 22-10-2020|^|
	private $error = array();
	public function index() {
		$this->language->load('transaction/sales');
		$this->load->model('transaction/delivery');
		$this->document->setTitle('Delivery Order List');
		$this->getList();
	}
	public function insert() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Delivery');
		$this->load->model('transaction/delivery');
		

		if (($this->request->server['REQUEST_METHOD'] == 'POST' && $this->request->post['type'] !='filter')) {
			if(!empty($this->request->post['selecteds'])){
				foreach ($this->request->post['selecteds'] as $selected => $from) {
					$data['invoice_no']    = $selected;
					$data['from'] 		   = $from;
					$data['delivery_man']  = $this->request->post['delivery_man'];
					$data['delivery_date'] = $this->request->post['delivery_date'];
					$data['do_no']         = $this->getDoNo();
					$assigneds 			  .= $this->model_transaction_delivery->addDelivery($data);
				}
			}
			
			if($this->request->post['delivery_man']){
				// $deliveryMan = $this->model_transaction_delivery->getDeliveryManName($this->request->post['delivery_man']);
				// $deliveryMan  = ' to -'.$deliveryMan;
			}
			if($assigneds !=''){
				$this->session->data['success'] = 'Response: '.$assigneds;
			}else{
				$this->session->data['error_bulk_convert'] = 'No invoice can be convert!.';
			}
			$this->redirect($this->url->link('transaction/delivery', 'token='.$this->session->data['token'], 'SSL'));
		}
		$this->getForm();
	}
	protected function getList() {
		
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$this->data['column_action']   	= $this->language->get('column_action');
		$this->data['heading_title']   	= $this->language->get('Sales');
		$this->data['text_tran_no']    	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['error_bulk_convert'])) {
			$this->data['error_warning'] = $this->session->data['error_bulk_convert'];
			unset($this->session->data['error_bulk_convert']);
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}

		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Delivery',
			'href'      => $this->url->link('transaction/delivery', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['insert']       = $this->url->link('transaction/delivery/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['back_button']  = $this->url->link('transaction/delivery', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['purchases']    = array();
		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit')
		);
		$this->data['filters']  = $data;
		$sales_total           	= $this->model_transaction_delivery->getTotalDelivery($data);
		$results                = $this->model_transaction_delivery->getDeliveryList($data);
		
		$userDetail   = array();
		foreach ($results as $result) {
			$shipping  = $this->model_transaction_delivery->getCustomerShippingDetails($result['shipping_id']);
			$delivery  = $this->model_transaction_delivery->getDeliveryManDetails($result['deliveryman_id']);
			$header    = $this->model_transaction_delivery->getinvoiceHeader($result['sales_transaction_no']);
			$result['assignedon'] 	   = date('d/m/Y',strtotime($result['assignedon']));
			if($result['assignedon']   == '01/01/1970'){
				$result['assignedon']  = '-';
			}
			$result['reason'] 		   = $result['reason'] == '' ? '-' : $result['reason'];
			if($result['froms'] == 'Purchase_Service'){
				$shipping_address      = '';
				$vendor                = $this->model_transaction_delivery->getVendor($result['customer_id']);
				$result['name']        = $vendor['vendor_name'];
				$shipping_address      = $vendor['address1'].', '.$vendor['address2'].', '.$vendor['postal_code'].', '.$vendor['phone'];
			}else{
				$shipping['address1']  = $shipping['address1'] !='' ? $shipping['address1'].',<br> ' : '';
				$shipping['address2']  = $shipping['address2'] !='' ? $shipping['address2'].',<br> ' : '';
				$shipping['country']   = $shipping['country'] !='' ? $shipping['country'].',<br> ' : '';
				$shipping_address      = $shipping['address1'].$shipping['address2'].$shipping['city'].' '.$shipping['country'].' '.$shipping['zip'].' '.$shipping['mobile'].' '.$shipping['contact_no'];
			}

			$header['balance_amount']  = $header['net_total'] - $header['paid_amount'];
			$this->data['delivery'][]  = array(
				'pagecnt'           => ($page-1) * $this->config->get('config_admin_limit'),
				'invoice_no'          => $result['sales_transaction_no'],
				'do_no' 			  => $result['do_no'],
				'from' 				  => $result['froms'],
				'vendor_name'         => $result['name'],
				'shipping_address'    => $shipping_address,
				'route_code'  		  => $delivery['route_code'],
				'assignedon'  		  => $result['assignedon'],
				'deliveredon'  		  => date('d/m/Y',strtotime($result['deliveredon'])),
				'deliveryman'    	  => $delivery['name'],
				'status'   			  => $result['status'],
				'signature'   		  => $result['signature'],
				'proofimg'   		  => $result['proofimg'],
				'payment_status'      => $header['payment_status'],
				'paidBalance'         => number_format($header['paid_amount'],2).' / '.number_format($header['balance_amount'],2),
				'reason'              => $result['reason'],
				'view_button'         => $this->url->link('transaction/delivery/view', 'token=' . $this->session->data['token'].'&purchase_id='.$result['invoice_no'].$url, 'SSL'),
				'download_button'     => $this->url->link('transaction/delivery/download_do_pdf', 'token=' . $this->session->data['token'].'&do_no='.$result['do_no'].$url, 'SSL')
			);
		}
		$this->data['salesmanlist'] = $this->model_transaction_delivery->getDeliveryMansList();
		$pagination 		= new Pagination();
		$pagination->total 	= $sales_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/delivery', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['route']= $this->request->get['route'];
		$this->template 	= 'transaction/delivery_list.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {

		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/customers');
		$this->load->model('transaction/delivery');
		$this->load->model('setting/location');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}	
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		
		$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Delivery List',
			'href'      => $this->url->link('transaction/delivery', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Delivery',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['action'] 	= $this->url->link('transaction/delivery/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['filter'] 	= $this->url->link('transaction/delivery/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel']   = $this->url->link('transaction/delivery','token='.$this->session->data['token'], 'SSL');

		$data = array(
				'filter_customer_name' => $filter_customer_name,
				'filter_contact_number'=> $filter_contact_number,
				'filter_transaction'   => $filter_transaction,
				'filter_sales_person'  => $filter_sales_person,
				'filter_from_date'     => $filter_from_date,
				'filter_to_date'   	   => $filter_to_date
		);
		$this->data['data']   		   = $data;
		$this->data['deliveryManList'] = $this->model_transaction_delivery->getDeliveryMansList();
		$this->data['salesPersonList'] = $this->model_transaction_delivery->salesPersonList();
		$salesInvoice    			   = $this->model_transaction_delivery->getSalesInvoiceList($data);
		$salesService    			   = $this->model_transaction_delivery->getSalesServiceList($data);
		$salesReturn    			   = $this->model_transaction_delivery->getSalesReturnList($data);
		$purchaseService    		   = $this->model_transaction_delivery->getPurchaseServiceList($data);

		$this->data['allDatas']  = array(
			array('from' => 'Sales_Invoice'   ,'details' => $salesInvoice),
			array('from' => 'Sales_Service'   ,'details' => $salesService),
			array('from' => 'Sales_Return'    ,'details' => $salesReturn),
			array('from' => 'Purchase_Service','details' => $purchaseService)
		);
		// printArray($salesInvoice); die;
		$this->data['token']  = $this->session->data['token'];
		$this->data['route']  = 'transaction/delivery';
		$this->template 	  = 'transaction/delivery_form.tpl';
		$this->children 	  = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm() {
		
		if (empty($this->request->post['delivery_man'])) {
			$this->error['warning'] = "Please select Delivery man.";
		}
		if (empty($this->request->post['selecteds'])) {
			$this->error['warning'] = "Please select minimum one invoice";
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function getDoNo(){
		$this->load->model('transaction/delivery');
		$do_no = $this->model_transaction_delivery->getDoNo();
		return $this->config->get('config_sales_do_prefix').date('ym').str_pad($do_no + 1, 4, '0', STR_PAD_LEFT);
	}
	public function getDeliveryManAndDate(){
		$this->load->model('transaction/delivery');
		if($this->request->post['do_no']){
			$doDetails = $this->model_transaction_delivery->getDeliveryManAndDate($this->request->post['do_no']);
			if(!empty($doDetails)){
				$assignedon = '';
				$doDetails['assignedon'] = date('d/m/Y',strtotime($doDetails['assignedon']));
				if($doDetails['assignedon'] != '01/01/1970'){
					$assignedon  =  $doDetails['assignedon'];
				}
				$return = array('status' => true,'delivery_man' =>$doDetails['deliveryman_id'],'delivery_date'=>$assignedon, 'delivery_remarks' => $doDetails['delivery_remarks'], 'delivery_status' => $doDetails['status']);
			}else{
				$return = array('status' => false);
			}
			$this->response->setOutput(json_encode($return));
		}
	}
	public function updateDelivery(){
		$this->load->model('transaction/delivery');
		if($this->request->post['do_no'] !='' && $this->request->post['delivery_man'] !=''){
			$updated = $this->model_transaction_delivery->updateDelivery($this->request->post);
			if($updated){
				$return = array('status' => true);
			}else{
				$return = array('status' => false);
			}
		}
		$this->response->setOutput(json_encode($return));
	}

	public function CancelDelivery(){
		$this->load->model('transaction/delivery');
		if($this->request->post['do_no'] !=''){
			$updated = $this->model_transaction_delivery->CancelDelivery($this->request->post['do_no']);
			if($updated){
				$return = array('status' => true);
			}else{
				$return = array('status' => false);
			}
		}
		$this->response->setOutput(json_encode($return));
	}
	public function bulk_cancel(){
		$this->load->model('transaction/delivery');
		if(!empty($this->request->post['selected_orders'])){
			foreach ($this->request->post['selected_orders'] as $do_no) {
				$this->model_transaction_delivery->CancelDelivery($do_no);
			}
		}
	}

	public function export_pdf(){
		$this->clearTempFolder();
		if(!empty($this->request->post['selected_orders'])){
			
			include(DIR_SERVER.'/MPDF/mpdf.php');
			ob_end_clean();
			$mpdf = new mPDF('c','A4');
			$mpdf->mirrorMargins = 1;
			$mpdf->SetMargins(0, 0, 5);
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->setFooter($footer);
			$mpdf->SetFont('DejaVuSans');
			$stylesheet = file_get_contents(HTTP_SERVER.'view/stylesheet/pdf.css');

			foreach ($this->request->post['selected_orders'] as $do_no) {
				$this->request->get['do_no'] = $do_no;
				$str = $this->download_do_pdf('1');
				$mpdf->WriteHTML($stylesheet,1);
				$mpdf->AddPage();
				$mpdf->WriteHTML($str);
			}
			$filename = 'download/sales_do_list.pdf';
			$mpdf->Output($filename,'F');
			ob_end_flush();
		}
	}
	public function clearTempFolder(){   // for clear temp files in *download* folder
		$files = glob(DIR_SERVER.'download/*');
			foreach($files as $file){
				if(is_file($file))
				unlink($file);
		}
	}
	public function download_do_pdf($return_str=''){

		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/delivery');

		$do_no 			 = $this->request->get['do_no'];
		$company_id	     = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$company_details['invoice_title'] = "Sales DO"; //Set invoice title here
		$doHeader 		 = $this->model_transaction_delivery->getDeliveryManAndDate($do_no);
		$salesInfo       = $this->model_transaction_sales->getSalesInvoiceForDo($doHeader['sales_transaction_no'],$doHeader['froms']);
		$vendorDetail    = $this->model_transaction_sales->getB2BCustomersbyCode($doHeader['customer_id'],$doHeader['froms']);
		$shipping_id = $doHeader['shipping_id'];
		if($doHeader['froms'] == 'Purchase_Service'){
			$shipping_id = $doHeader['customer_id'];	
		}
		$shipping 	     = $this->model_transaction_delivery->getCustomerShippingDetails($shipping_id,$doHeader['froms']);
		$company_details['location'] = $this->model_inventory_reports->getLocationDetails($salesInfo['location_code']);
		$productDetails	 = $this->model_transaction_sales->getSalesDoDetail($doHeader['id']);
		$invoice_no      = $salesInfo['invoice_no'];
		$transaction_date= date('d/m/Y',strtotime($salesInfo['invoice_date']));
		$transaction_date= $transaction_date == '01/01/1970' ? '' : $transaction_date;

		$reference_no    = $salesInfo['reference_no'];
		$reference_date  = $salesInfo['invoice_date'];
		$tax_type 		 = $salesInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
        $tax_type 		 = $salesInfo['tax_class_id']=='2' ? $tax_type :'(0%)';
		
		$str = $this->url->getCompanyAddressHeaderString2($company_details);
			$strs.= '</table>';
			$str .='<table class="table" style="width:100%;">
				<tr>
					<td class="left">
					<table class="listings"  style="width:100%;">
						<tr>
							<th align="left">Customer Details</th>
							<td></td>
						</tr>
						<tr>
							<th align="left">Name</th>
							<td>'.$vendorDetail['name'].'</td>
						</tr>
						<tr>
							<th align="left">Shipping Address</th>
							<td>'.$shipping['address1'].' '.$shipping['address2'].', '.$shipping['city'].' - '.$shipping['country'].' '.$shipping['zip'].'</td>
						</tr>
						<tr>
							<th align="left">Contact</th>
							<td>Phone : '.$vendorDetail['mobile'].' Email : '.$shipping['contact_no'].'</td>
						</tr>
					</table>
					</td>
					<td class="right">
					<table style="width:100%;">
						<tr>
							<th align="left">DoNo</th>
							<td>'.$doHeader['do_no'].'</td>
						</tr>
						<tr>
							<th align="left">DoDate</th>
							<td>'.$transaction_date.'</td>
						</tr>
						<tr>
							<th align="left"></th>
							<td></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>';
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th align='left' >SKU</th>
							<th align='left' >Description</th>
							<th align='left'>Quantity</th>
						</tr></thead><tbody>";
				$i= 1;
				foreach ($productDetails as $value) {
					$str.="<tr style='border-bottom: 2px solid black;'>
		   					<td>".$value['sku']."</td>
		   					<td>".$value['prod_name']."</td>
							<td>".$value['qty']."</td>
						   </tr>";	
				$i++; }
				$str.='</tbody></table>';

				$result = fmod(count($productDetails),38);
				$minus  = $result < 10 ? 28 : 26;
				$counts = $minus - $result;
 				for ($i=0; $i < $counts; $i++) { 
					$br .='<br>';
				}
					$str .= $br.'<tr>
							    	<td align="right">';
									$deliveryDate = '-';
									if($doHeader['status'] == 'Delivered') {
								 		$str .= '<figure>
												  <figcaption>All goods received are in good condition.</figcaption>
												  <img src="'.HTTP_SERVER.'doc/signature/'.$doHeader['signature'].'" style="width: 100px;"/>
												  <figcaption>Customer Signature</figcaption>
												</figure>';
										$deliveryDate = date('d/m/Y',strtotime($doHeader['deliveredon']));
										$deliveryDate = $deliveryDate != '01/01/1970' ? $deliveryDate : '-';
									}
								    $str .= '</td>
							    </tr>';
				$str .= '<tr>
						  <td align="center" colspan="3">
						 	<table class="listings"  style="width:100%;">
								<tr>
									<td align="left" style="border-right:1px solid black;padding:20px;position: relative;bottom: 20px;right: 20px;">Delivery Date: '.$deliveryDate.'<br></td>
									<td align="left" style="padding:20px;bottom: 20px;right: 20px;">Remarks: '.$doHeader['reason'].'<br></td>
								</tr>
							</table>
						  </td>
					    </tr>';
				

		if($return_str) return $str;
		if($str){
			$filename = $doHeader['do_no'].'_'.date('YmdHi').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function getlistsort()
    {
    	$dono = $_REQUEST['dono'];
  	    $this->load->model('transaction/delivery');
	    $updated = $this->model_transaction_delivery->sortdelivery($dono);
	    if($updated != ''){
		 	$return = array('status' => true);
	    }else{
			$return = array('status' => false);
		}	 
		$this->response->setOutput(json_encode($return));
    }
}
?>