<?php
class Controllertransactionpurchaseinvoice extends Controller {
	private $error = array();
	public function index(){
		
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Invoice');
		$this->load->model('transaction/purchase');
		$this->clearPurchaseData();
		$this->getlist();
	}
	protected function getlist(){

		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title']	= $this->language->get('heading_title');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_location'] 	= $this->language->get('text_location');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if($this->session->data['location_code']!='HQ'){
			$_REQUEST['filter_location_code'] = $this->session->data['location_code']; 
		}
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$pageUrl.= '&filter_xero='.$filter_xero;
		} else {
			$filter_xero = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}
		// echo $filter_supplier; die;
		$filter_transactionno = null;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		$pageUrl.='&filter_location_code='.$filter_location_code; 

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.='&filter_supplier='.$filter_supplier; 
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$url .= '&page='.$page;
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase Invoice',
			'href'      => $this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$data = array(
			'show_hold'	           => $show_hold,
			'filter_xero'	       => $filter_xero,
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'filter_supplier'	   => $filter_supplier,
			'filter_transactionno' => $filter_transactionno,
			'filter_location_code' => $filter_location_code,
			'order' 			   => $order,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);
		// printArray($data);
		$this->data['data'] = $data;
		$purchase_total = $this->model_transaction_purchase->getTotalPurchaseInvoice($data);
		$results 		= $this->model_transaction_purchase->getPurchaseInvoiceList($data);
		$this->data['suppliers'] = $this->model_transaction_purchase->getVendors();

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail   = array();
		
		foreach ($results as $result) {
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$userDetail   = $this->model_user_user->getUser($result['created_by']);
			$this->data['purchases'][] = array(
				'pagecnt'           => ($page-1)*$this->config->get('config_admin_limit'), 
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => $result['transaction_date'],
				'transaction_type'  => $result['transaction_type'],
				'reference_no'  	=> $result['reference_no'],
				'reference_date'	=> $result['reference_date'],
				'location_code' 	=> $result['location_code'],
				'vendor_code'       => $vendorDetail['vendor_code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'total'             => $result['total'],
				'fc_nettotal'       => $result['fc_nettotal'],
				'invoice'        	=> $result['invoice'],
				'xero_purchase_id'  => $result['xero_purchase_id'],
				'status'  			=> $result['status'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['created_date'],
				'view_button'       => $this->url->link('transaction/purchase_invoice/view', 'token=' . $this->session->data['token'].'&purchase_id='.$result['purchase_id'].$pageUrl.$url, 'SSL'),
				'download_button'   => $this->url->link('transaction/purchase_invoice/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$result['purchase_id'] . $url, 'SSL'),
				'modify_button'     => $this->url->link('transaction/purchase_invoice/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $pageUrl.$url, 'SSL'),
			);
		}
		$this->data['delete'] = $this->url->link('transaction/purchase_invoice/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		$this->data['insert'] = $this->url->link('transaction/purchase_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$this->data['Tolocations'] 			= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $filter_location_code;

		$pagination 		= new Pagination();
		$pagination->total 	= $purchase_total['totalPurchase'];
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/purchase_invoice', 'token='.$this->session->data['token'].$pageUrl.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];

		$this->template 	 = 'transaction/purchase_invoice_list.tpl';
		$this->children 	 = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function update(){

		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/purchase');
		$this->load->model('inventory/reports');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm('update')) {
			$data = array();

			if(isset($this->request->post['purchase_id'])) {
				$data['purchase_id']	=  $this->request->post['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['handling_fee'])) {
				$data['handling_fee']	=  $this->request->post['handling_fee'];
			} else {
				$data['handling_fee']	= '';
			}
			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			} else {
				$data['currency_code']	= '';
			}
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '1';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			if (isset($this->request->post['status'])) {
				$data['status'] = $this->request->post['status'];
			} else {
				$data['status'] = '';
			}
			$data['oldPurchase_id']	= '';
			if(isset($this->request->post['oldPurchase_id'])) {
				$data['oldPurchase_id']	=  $this->request->post['oldPurchase_id'];
			}
			$product_data = array();
			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type']; 
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],					
					'hasChild'                  => $product['hasChild'],
					'sku'                  		=> $product['sku'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total'],
					'foc_quantity'              => $product['foc_quantity'],
					'description'               => $product['description']
				);
			}
			$data['products'] = $product_data;
			$total_data = array();
			$this->load->model('ajax/cart');
		    $total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$costMethod = $this->config->get('config_average_cost_method');
			$this->model_transaction_purchase->editPurchaseInvoice($data['purchase_id'], $data,$costMethod);
			$this->session->data['success'] = 'Success: You have modified purchase invoice!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($this->request->get['filter_date_from'])) {
				$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
			}
			if (isset($this->request->get['filter_date_to'])) {
				$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
			}
			if (isset($this->request->get['filter_supplier'])) {
				$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
			}
			if (isset($this->request->get['filter_location_code'])) {
				$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
			}
			if (isset($this->request->get['filter_transactionno'])) {
				$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
			}
			if (isset($this->request->get['filter_xero'])) {
				$url.= '&filter_xero='.$this->request->get['filter_xero'];
			}
			$this->redirect($this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getform(){
	
		$this->load->model('setting/company');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/location');
		
		$this->language->load('transaction/purchase');
		$this->language->load('transaction/purchase');
		$cart_type = 'cart_purchase';
		$this->document->setTitle('Purchase Invoice Form');
		$this->data['entry_tran_no'] = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt']  = $this->language->get('entry_tran_dt');
		$this->data['entry_location']  = $this->language->get('entry_location');
		$this->data['entry_tran_type']  = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code'] = $this->language->get('entry_vendor_code');
		$this->data['entry_vendor_name']  = $this->language->get('entry_vendor_name');
		$this->data['entry_refno']         = $this->language->get('entry_refno');
		$this->data['entry_refdate']       = $this->language->get('entry_refdate');
		$this->data['entry_vendor']        = $this->language->get('entry_vendor');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name']   = $this->language->get('text_product_name');
		$this->data['text_qty'] 		  = $this->language->get('text_qty');
		$this->data['text_price'] 	     = $this->language->get('text_price');
		$this->data['text_raw_cost']	= $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] 	= $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] = $this->language->get('text_disc_price');
		$this->data['text_net_price']  = $this->language->get('text_net_price');
		$this->data['text_total']      = $this->language->get('text_total');
		$this->data['text_tax']         = $this->language->get('text_tax');
		$this->data['text_weight_class'] = $this->language->get('text_weight_class');
		$this->data['text_tax_class']     = $this->language->get('text_tax_class');
		$this->data['text_no_data']       = $this->language->get('text_no_data');
		$this->data['text_remove']        = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] 		 = $this->language->get('text_hold');
		$this->data['button_hold'] 		= $this->language->get('button_hold');
		$this->data['button_save']     = $this->language->get('button_save');
		$this->data['button_cancel']  = $this->language->get('button_cancel');
		$this->data['button_add']    = $this->language->get('button_add');
		$this->data['button_clear'] = $this->language->get('button_clear');
		$this->data['column_action']= $this->language->get('column_action');

		$this->data['error_warning'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		}
		if (isset($this->error['transaction_no'])) {
			$this->data['error_warning'] = $this->error['transaction_no'];
		}
 		if (isset($this->error['transaction_date'])) {
			$this->data['error_warning'] = $this->error['transaction_date'];
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_warning'] = $this->error['vendor'];
		}
		if (isset($this->error['reference_date'])) {
			$this->data['error_warning'] = $this->error['reference_date'];
		}
		if (isset($this->error['reference_no'])) {
			$this->data['error_warning'] = $this->error['reference_no'];
		}
		if (isset($this->error['price'])) {
			$this->data['error_warning'] = $this->error['price'];
		}

		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
		}
		if (isset($this->request->get['filter_xero'])) {
			$url.= '&filter_xero='.$this->request->get['filter_xero'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase Invoice List',
			'href'      => $this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (isset($this->request->get['purchase_id'])) {
			$this->clearPurchaseData();
			if(isset($this->request->get['type']) && $this->request->get['type']=='purConvrt'){
				$purchaseInfo = $this->model_transaction_purchase->getPurchase($this->request->get['purchase_id']);
			}else{
				$purchaseInfo = $this->model_transaction_purchase->getPurchaseInvoiceHeader($this->request->get['purchase_id']);
			}
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				if($purchaseInfo['handling_fee'] > 0){
					$this->session->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
				}
				if(isset($this->request->get['type']) && $this->request->get['type']=='purConvrt'){
					$purchaseProducts = $this->model_transaction_purchase->getPurchaseProduct($purchaseInfo['purchase_id']);
				}else{
					$purchaseProducts = $this->model_transaction_purchase->getPurchaseInvoiceDetails_Id($purchaseInfo['purchase_id']);
				}
				foreach ($purchaseProducts as $products) {
					if(isset($products['conversion_status']) && $products['conversion_status'] == '1'){
						$products['quantity'] = $products['quantity'] - $products['received_qty'];
						$products['foc']      = '0';
					}
					if($this->request->get['type'] ==''){
						$products['conversion_status'] = '1';
					}
					/*Below condition is filter complted purchase product (qty) */
					if(isset($products['conversion_status']) && $products['conversion_status'] !='2'){
						$products['original_quantity'] = $products['quantity'];
						$products = array_merge($products, $this->getDiscountCalculation($products));
						$products['foc_quantity'] = (int)$products['foc'];
						if(!empty($this->session->data['cart_purchase'])){
							if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['quantity'] > 0)) {
								$this->cart->update($products['product_id'], $products['quantity'], $cart_type, $products);
							} elseif ($products['quantity'] > 0) {
								$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
							}
						} elseif ($products['quantity'] > 0) {
							$this->cart->add($products['product_id'], $products['quantity'], '', '', '', $cart_type, $products);
						}
					}
				}
			}
		}

		$this->data['Tolocations'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']); 
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
       		if(isset($this->request->get['purchase_id']) && !isset($this->request->get['type'])){
				$this->data['transaction_no'] = $purchaseInfo['transaction_no'];
       		}else{
				$this->data['transaction_no'] = $this->getTransNo();
       		}
		}else{
			$this->data['transaction_no'] = $this->getTransNo();
		}
		$this->data['purchase_trans_no'] = $purchaseInfo['transaction_no'];
		if (isset($this->request->post['purchase_trans_no'])) {
			$this->data['purchase_trans_no'] = $this->request->post['purchase_trans_no'];
		}
		$this->data['isConverted'] = 0;
		if(isset($this->request->get['type']) && $this->request->get['type']=='purConvrt'){
			$this->data['isConverted'] = 1;
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_master_vendor->getVendor($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} elseif (!isset($this->request->post['vendor'])) {
		    $this->data['vendor'] = $purchaseInfo['vendor_id'];
			$vendorData = $this->model_master_vendor->getVendor($this->data['vendor']);
			$this->data['vendor_name'] = $vendorData['vendor_name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}
		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['reference_no'];
		} else {
			$this->data['reference_no'] = '';
		}
		if (isset($this->request->post['bill_discount_percentage'])) {
			$this->data['bill_discount_percentage'] = $this->request->post['bill_discount_percentage'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}
		if (isset($this->session->data['bill_discount_price'])) {
			$this->data['bill_discount_price'] = $this->session->data['bill_discount_price'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		} 
		if (isset($this->session->data['handling_fee'])) {
			$this->data['handling_fee'] = $this->session->data['handling_fee'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
		} else {
			$this->data['handling_fee'] = '';
		} 
		$this->data['remarks'] = '';
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} 
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = $this->session->data['location_code'];
		} 
		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = $this->model_transaction_purchase->getcompanyCurrency();
		}
		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		} 
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['transaction_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		}
		$this->data['transaction_date']     = date('d/m/Y');

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}
		if (isset($this->request->get['purchase_id'])) {
			$this->data['purchase_id'] = $this->request->get['purchase_id'];
		}else{
			$this->data['purchase_id']='';
		}
		if (isset($this->request->get['type'])) {
			$this->data['purchase_type'] = $this->request->get['type'];
		}else{
			$this->data['purchase_type']='';
		}
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['status'] = $purchaseInfo['status'];
		} else {
			$this->data['status'] = '';
		}
		if (isset($this->request->post['arrival_date_from'])) {
			$this->data['arrival_date_from'] = $this->request->post['arrival_date_from'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['arrival_date_from'] = date('d/m/Y',strtotime($purchaseInfo['reference_date']));
			if(date('Y-m-d',strtotime($purchaseInfo['reference_date'])) < date('Y-m-d')){
				$this->data['arrival_date_from'] = date('d/m/Y');
			}
		} else {
			$this->data['arrival_date_from'] = date('d/m/Y');
		} 
		if (isset($this->request->post['arrival_date_to'])) {
			$this->data['arrival_date_to'] = $this->request->post['arrival_date_to'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['arrival_date_to'] = date('d/m/Y',strtotime($purchaseInfo['arrival_date_to']));
			if(date('Y-m-d',strtotime($purchaseInfo['arrival_date_to'])) < date('Y-m-d')){
				$this->data['arrival_date_to'] = date('d/m/Y',strtotime('+1 day'));
			}
		} else {
			$this->data['arrival_date_to'] = date('d/m/Y',strtotime('+1 day'));
		}
		if (isset($this->request->post['attachment'])) {
			$this->data['attachment'] = '';
		} elseif (!empty($purchaseInfo)) {
			$this->data['attachment'] = $purchaseInfo['attachment'];
		} else {
			$this->data['attachment'] = '';
		}
		if (isset($this->request->post['customer_id'])) {
			$this->data['customer_id'] = '';
		} elseif (!empty($purchaseInfo)) {
			$this->data['customer_id'] = $purchaseInfo['customer_id'];
		} else {
			$this->data['customer_id'] = '';
		}
		if (isset($this->request->post['shipping_id'])) {
			$this->data['shipping_id'] = '';
		} elseif (!empty($purchaseInfo)) {
			$this->data['shipping_id'] = $purchaseInfo['shipping_id'];
		} else {
			$this->data['shipping_id'] = '';
		} 
		$this->load->model('ajax/cart');
		$this->load->model('localisation/weight_class');
		$this->load->model('localisation/tax_class');
		
		$this->data['products'] = array();
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->data['location_code']);
		$this->data['apply_tax_type'] 		= $apply_tax_type;
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		$this->data['tax_classes'] 			= $this->model_localisation_tax_class->getTaxClasses();
		$this->data['currencys'] 			= $this->model_transaction_purchase->getCurrency();
		$this->data['weight_classes'] 		= $this->model_localisation_weight_class->getWeightClasses();
		$this->data['vendor_collection'] 	= $this->model_master_vendor->getVendors();
		$this->data['token'] 				= $this->session->data['token'];
		$this->data['cancel'] 				= $this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['allTerms'] 			= $this->cart->getAllTerms();

       	if(isset($this->request->get['purchase_id']) && isset($this->request->get['type'])){
			$this->data['action'] = $this->url->link('transaction/purchase_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}
       	else if(isset($this->request->get['purchase_id'])){
			$this->data['action'] = $this->url->link('transaction/purchase_invoice/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}else{
			$this->data['action'] = $this->url->link('transaction/purchase_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	} 
       	$this->data['purConvrt']      = $this->request->get['type']; 
		$this->data['oldPurchase_id'] = $this->request->get['purchase_id'];
		$this->data['route'] 		  = 'transaction/purchase_invoice';
		$this->template = 'transaction/purchase_invoice_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getTransNo(){

		$transNumberWithSuffix = $this->model_transaction_purchase->getPurchaseAutoId();
		$transNumber		   = $transNumberWithSuffix;

		if($transNumber){
			 $transaction_no = ltrim($transNumber)+1;
			 $this->data['transaction_no'] = $this->config->get('config_purchase_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_purchase_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='',$location_code) {
		 //printArray($this->session->data); 
		$product_data = array();
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		$pro = $this->cart->getProducts($cart_type,$apply_tax_type);
		// printArray($pro); exit;
		foreach ($pro as $product) {
           $SkuQtyDetails = $this->model_transaction_purchase->getQtyDetailsByLocation($product['sku'],$location_code);
           $Reservedstock = $this->model_transaction_purchase->getReservedStockdetails($location_code,$product['sku']); 
		    $product['sku_qty'] = $SkuQtyDetails['sku_qty']-$Reservedstock['reserved_qty']; 

			$product_data[]  = array(
				'key' 		 => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		 => $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'sku_qty'    => $product['sku_qty'],
				'price'      => $this->currency->format4Decimal($product['price']),
				'raw_cost'   => $this->currency->format($product['raw_cost']),
				'sub_total'  => $this->currency->format($product['sub_total']),
				'discount_amount' 	=> $this->currency->format($product['discount_amount']),
				'total'      		=> $product['total'],
				'tax'        		=> $product['purchase_tax'],
				'reward'     		=> $this->currency->format($product['reward']),
				'discount'   		=> $product['purchase_discount_value'],
				'purchase_discount'   	=> $product['purchase_discount'],
				'discount_mode'   		=> $product['purchase_discount_mode'],
				'foc_quantity'   		=> $product['foc_quantity'],
				'original_quantity'   	=> $product['original_quantity'],
				'description'   		=> $product['description']
			);
		}
		return $product_data;
	}
	public function insert() {
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('transaction/purchase');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit') && $this->validateForm()) {
			$data = array();
			$data['transaction_date']	= '';
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			}
			$data['vendor']	= '';
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			}
			$data['reference_no']	= '';
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			}
			$data['purchase_trans_no']	= '';
			if(isset($this->request->post['purchase_trans_no'])) {
				$data['purchase_trans_no']	=  $this->request->post['purchase_trans_no'];
			}
			$data['remarks']	= '';
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}

			$data['bill_discount_percentage']	= '0';
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			}
			
			$data['bill_discount_price']	= '0';
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			}
			$data['handling_fee']	= '0';
			if(isset($this->request->post['handling_fee'])) {
				$data['handling_fee']	=  $this->request->post['handling_fee'];
			}
			$data['bill_total']	= '0';
			if(isset($this->request->post['total'])) {
				$data['bill_total']	=  $this->request->post['total'];
			}

			$data['bill_tax']	= '0';
			if(isset($this->request->post['tax'])) {
				$data['bill_tax']	=  $this->request->post['tax'];
			}

			$data['currency_code']	= '';
			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			}

			$data['conversion_rate']	= '';
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			}
			$data['oldPurchase_id']	= '';
			if(isset($this->request->post['oldPurchase_id'])) {
				$data['oldPurchase_id']	=  $this->request->post['oldPurchase_id'];
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '1';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '1';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '1';
			}
			$data['arrival_date_from']	   = '';
			if(isset($this->request->post['arrival_date_from'])) {
				$data['arrival_date_from'] = $this->request->post['arrival_date_from'];
			}
			$data['arrival_date_to']	 = '';
			if(isset($this->request->post['arrival_date_to'])) {
				$data['arrival_date_to'] = $this->request->post['arrival_date_to'];
			}
			$data['purchase_attachment']	 = '';
			if(isset($this->request->post['purchase_attachment'])) {
				$data['purchase_attachment'] = $this->request->post['purchase_attachment'];
			}
			$data['customer_id']	 = '';
			if(isset($this->request->post['customer_id'])) {
				$data['customer_id'] = $this->request->post['customer_id'];
			}
			$data['shipping_id']	 = '';
			if(isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			}

			$product_data = array();
			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type']; 
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				if($product['quantity'] > 0){
					$product_data[] = array(
						'product_id'             => $product['product_id'],
						'name'                   => $product['name'],
						'quantity'               => $product['quantity'],
						'price'                  => $product['price'],
						'raw_cost'               => $product['raw_cost'],
						'hasChild'               => $product['hasChild'],
						'sku'                 	 => $product['sku'],
						'purchase_discount'      => $product['purchase_discount'],
						'purchase_discount_mode' => $product['purchase_discount_mode'],
						'purchase_discount_value'=> $product['purchase_discount_value'],
						'tax_class_id'           => $product['tax_class_id'],
						'weight_class_id'        => $product['weight_class_id'],
						'net_price'              => $product['sub_total'],
						'tax_price'              => $product['purchase_tax'],
						'total'                  => $product['total'],
						'foc_quantity'           => $product['foc_quantity'],
						'original_quantity'      => $product['original_quantity'],
						'description' 	         => $product['description']
					);
				}
			}
			$this->load->model('ajax/cart');
			$data['products']= $product_data;
			$total_data 	 = array();
			$vendor_id 		 = $this->request->post['vendor'];
		    $total_data 	 = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	 = $totals['value'];
				}
			}
			$data['totals']  = $total_data;
			$data['total']   = $total;
			$costMethod      = $this->config->get('config_average_cost_method');
		    $data['config_vendor_update'] 	 = $this->config->get('config_vendor_update');
            
            $data['attachment'] = '';
            if($data['purchase_trans_no'] !='' && $data['purchase_attachment']!=''){
            	$oldFile   = DIR_SERVER.'uploads/PurchaseAttachment/'.$data['purchase_attachment'];
            	$path_info = pathinfo($oldFile);
            	$extention = '.'.$path_info['extension'];
            	$newFile   = DIR_SERVER.'uploads/PurchaseAttachment/Purchase_Invoice_'.$data['purchase_trans_no'].$extention;
            	if(file_exists($oldFile)){
            		copy($oldFile , $newFile);
            		$data['attachment'] = 'Purchase_Invoice_'.$data['purchase_trans_no'].$extention;
            	}
            }
			if($_FILES["attachment"]['name']!= '') {
				$response = $this->uploadAttachment($data['transaction_no']);
				if($response != ''){
					$data['attachment'] = $response; 
 				}
			}
			$data['transaction_no']	= $this->getTransNo(); 
			if(!empty($data['products'])){ 
	            $this->model_transaction_purchase->addPurchaseInvoice($data,$costMethod);
				$this->session->data['success'] = $this->language->get('purchase_text_success');
				$this->clearPurchaseData();
			} else {
				$this->session->data['error'] = 'No products with valid quantity!';
			}
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->data = $data;
		$this->getform();
	}
	protected function validateForm($type='') {
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one vendor.";
		}
		if (empty($this->request->post['reference_no'])) {
			$this->error['reference_no'] = "Please enter reference no.";
		}
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if($type!='update'){
			$purchaseInfo = '';//$this->model_transaction_purchase->getPurchaseByTransNo($this->request->post['transaction_no']);
			if (!empty($purchaseInfo)) {
				$this->error['warning'] = "Please enter unique transaction number.";
			}
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			unset($this->session->data['handling_fee']);
			unset($this->session->data['bill_discount_percentage']);
			unset($this->session->data['bill_discount_price']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount = array();
		$discountError = '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice	= $data['discount_price'];
		$data['subTotal'] = $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function view(){
		$this->language->load('transaction/purchase');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle('Purchase Details');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url.= '&filter_supplier='.$this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url.= '&filter_location_code='.$this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_transactionno'])) {
			$url.= '&filter_transactionno='.$this->request->get['filter_transactionno'];
		}
		if (isset($this->request->get['filter_xero'])) {
			$url.= '&filter_xero='.$this->request->get['filter_xero'];
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase',
			'href'      => $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase List',
			'href'      => $this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Purchase View',
			'href'      => $this->url->link('transaction/purchase_invoice/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$backUrl = $url;
		if($this->request->get['from']=='po'){
			$backUrl = '';
		}
		$this->data['back_button'] 		= $this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'].$backUrl, 'SSL');
		$this->data['purchaseInfo'] = $this->model_transaction_purchase->getPurchaseInvoiceDetailsById($this->request->get['purchase_id']);

		$this->data['vendorDetail'] 	= $this->model_master_vendor->getVendor($this->data['purchaseInfo']['vendor_id']);
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['created_by']);
		$this->data['purchaseDetail'] 	= $this->model_transaction_purchase->getPurchaseInvoiceDetails($this->data['purchaseInfo']['purchase_id']);
		$this->data['delete_button'] = $this->url->link('transaction/purchase/delete', 'token=' . $this->session->data['token'] . '&purchase_id=' . $this->request->get['purchase_id'] . $url, 'SSL');

		$productDetails	= $this->model_transaction_purchase->getInvoice_ProductDetailsById($this->data['purchaseInfo']['purchase_id'],$this->data['purchaseInfo']['transaction_no']);
		
		$this->data['productDetails'] = [];
		foreach ($productDetails as $value) {
			$value['childItems'] = $this->model_transaction_purchase->getProductChildItems($value['product_id']);
			$this->data['productDetails'][] = $value;
		}

		$this->data['print_pdf']=$this->url->link('transaction/purchase_invoice/download_pdf','token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url,'SSL');

		$this->template = 'transaction/purchase_invoice_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function download_pdf($return_str = ''){

		$this->language->load('transaction/purchase');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');
		
		$purchase_id = '';
		if($this->request->get['purchase_id'] !=''){
			$purchase_id = $this->request->get['purchase_id'];
		}

		$company_details = $this->model_inventory_reports->getCompanyDetails($this->session->data['company_id']);
		$company_details['purchase_title'] = 'Purchase Invoice';
		$headerStr	  = $this->url->purchasePdfHeaderString($company_details);
		$purchaseInfo = $this->model_transaction_purchase->getPurchaseInvoiceDetailsById($purchase_id);
		$vendorDetail = $this->model_master_vendor->getVendor($purchaseInfo['vendor_id']);
		$userDetail   = $this->model_user_user->getUser($purchaseInfo['created_by']);
		$purchaseDetail = $this->model_transaction_purchase->getPurchaseInvoiceDetails($purchaseInfo['purchase_id']);
		$productDetail = $this->model_transaction_purchase->getProductInvoiceDetailsById($purchaseInfo['purchase_id'],$purchaseInfo['transaction_no']);
		foreach ($productDetail as $value) {
			$value['childItems'] = $this->model_transaction_purchase->getProductChildItems($value['product_id']);
			$productDetails[] = $value;
		}
		$invoice_no       = $purchaseInfo['transaction_no'];
		$transaction_date = date('Y/m/d',strtotime($purchaseInfo['transaction_date']));
		$reference_no     = $purchaseInfo['reference_no'];
		$reference_date   = date('Y/m/d',strtotime($purchaseInfo['reference_date']));
		$vendor_name      = $vendorDetail['vendor_name'];		

		$tax_type = $purchaseInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
        $tax_type = $purchaseInfo['tax_class_id']=='2' ? $tax_type :'(0%)';
        
        $vendorDetail['address1']=$vendorDetail['address1'] !='' ? '<br>'.$vendorDetail['address1'] : '<br>';
        $vendorDetail['country']=$vendorDetail['country'] !='' ? '<br>'.$vendorDetail['country']: '<br>';
        $vendorDetail['postal_code'] = $vendorDetail['postal_code'] !='' ? 'Phone: '.$vendorDetail['postal_code']: '';
        $vendorDetail['fax'] = $vendorDetail['fax'] !='' ? ' Fax: : '.$vendorDetail['fax']: '';
		if($vendorDetail['postal_code']==''  && $vendorDetail['fax'] ==''){
			$vendorDetail['postal_code'] = '<br>';
		}
		$str = $headerStr;
			$str .='<table style="width:100%;">
				<tr style="height:auto;">
					<td style="width:50%; height:100%;font-family: Roboto,sans-serif;">
					<table class="custmtable addrstbl">
						<tr>
							<td colspan="2" align="left" style="font-size:15px;font-weight:bold;    border-bottom: 1px solid black;">
							Vendor:</td>
						</tr>
						<tr>
							<td>'.$vendorDetail['vendor_name'].$vendorDetail['address1'].$vendorDetail['country'].'</td>
						</tr>
						<tr>
							<td colspan="2" align="left" style="font-size:15px;font-weight:bold;">
							Contact:</td>
						</tr>
						<tr>
							<td>'.$vendorDetail['postal_code'].$vendorDetail['fax'].'</td>
						</tr>
					</table>
					</td>

					<td>
					<table class="custmtable addrstbl">
						<tr>
							<td style="font-size:12px;font-weight:bold;">Purchase Invoice No</td>
							<td align="left">'.$invoice_no.'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Date</td>
							<td align="left">'.date('d/m/Y',strtotime($transaction_date)).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Ref Date</td>
							<td align="left">'.date('d/m/Y',strtotime($reference_date)).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Currency</td>
							<td align="left">'.$purchaseInfo['currency_code'].'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Conversion Rate</td>
							<td align="left">'.$purchaseInfo['conversion_rate'].'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Remarks</td>
							<td align="left">'.$purchaseInfo['remarks'].'</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>';
			// echo $str; die;
			$str.="<table class='customtable'><tbody>
					<tr>
						<th style='font-weight:bold; width:5%;'>Sno</th>
						<th style='font-weight:bold; width:40%;'>Name</th>
						<th style='font-weight:bold; width:5%;'>Qty</th>
						<th style='font-weight:bold; width:10%;'>UOM</th>
						<th style='font-weight:bold; width:10%;'>Price</th>
						<th style='font-weight:bold; width:10%;'>Discount</th>
						<th style='font-weight:bold; width:10%;' class='text-right bold'>Total</th>
					</tr>";

				$j=$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				$i= 1;
				$count = count($productDetails);
				foreach ($productDetails as $value) {
				$productName = $this->model_transaction_purchase->getProductName($value['product_id']);

					$str.="<tr style='margin-top: 5px;'>
		   				    <td align='center'>".$i."</td>
							<td align='left'>".$productName;
							if($value['description'] !=''){
								$str .= "<br><p style='font-size: 12px;'>(".$value['description'].")</P>";
							}
							if(!empty($value['childItems'])){
                                foreach ($value['childItems'] as $childItems) {
                                  $str .= '<span>'.$childItems['childsku'].' - '.$childItems['name'].'</span><br>';
                                }
                            }
					$str .="<br><br></td><td align='center'>".$value['qty']."</td>
							<td align='center'>".$value['sku_uom']."</td>
							<td align='right'>".$this->currency->format($value['prices'])."</td>
							<td align='right'>".$this->currency->format($value['discount_price'])."</td>
							<td class='right'>".$this->currency->format($value['nettotal'])."</td>
						   </tr>";	
				$i++; }

				$result = fmod($count,18);
				$counts =  18 - $result;
				for ($i=0; $i < $counts; $i++) { 
					$str .='<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>';
				}
				$str .= "</tbody></table>";
			$str .='<table class="custmtable" style="border:none;">
				<tr>
					<td align="center" colspan="2"><p style="font-size:17px;font-weight:bold;"></p></td>
				</tr>
				<tr>
					<td>
					<table>
						<tr><td> </td></tr>
						<tr><td> </td></tr>
						<tr>
							<td colspan="2" align="left"><hr></td>
						</tr>
						<tr>
							<td>Signature & Company Stamp</td>
						</tr>
					</table>
					</td>

					<td align="right">
					<table class="custmtable">
						<tr>
							<td style="font-size:12px;font-weight:bold;">SGD</td>
							<td align="right">'.$this->currency->format($purchaseInfo['sub_total']).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Discount</td>
							<td align="right">'.$this->currency->format($purchaseInfo['discount']).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Delivery Fees</td>
							<td align="right">'.$this->currency->format($purchaseInfo['handling_fee']).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">GST '.$tax_type.'</td>
							<td align="right">'.$this->currency->format($purchaseInfo['gst']).'</td>
						</tr>
						<tr>
							<td style="font-size:12px;font-weight:bold;">Net Total</td>
							<td align="right">'.$this->currency->format($purchaseInfo['total']).'</td>
						</tr>
					</table>
					</td>
				</tr>
			</table></body>';

		// echo $str; die;
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		if($return_str){
			return $str;
		}
		if($str){
			$filename = 'Purchase_invoice_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function bulk_delete(){

		$this->load->model('transaction/purchase');
		foreach ($this->request->post['selected'] as $purchase_id) {
			$this->model_transaction_purchase->deletePurchaseInvoice($purchase_id);
		}

		$this->session->data['success'] = 'Purchase Invoice Details deleted Completed';
		if(isset($_REQUEST['filter_date_from'])){
			$pageUrl.= '&filter_date_from='.$_REQUEST['filter_date_from'];
		}
		if(isset($_REQUEST['filter_date_to'])){
			$pageUrl.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if(isset($_REQUEST['filter_location_code'])){
			$pageUrl.='&filter_location_code='.$_REQUEST['filter_location_code']; 
		}
		if(isset($_REQUEST['filter_supplier'])){
			$pageUrl.='&filter_supplier='.$_REQUEST['filter_supplier']; 
		}
		if(isset($_REQUEST['filter_transactionno'])){
			$pageUrl.='&filter_transactionno='.$_REQUEST['filter_transactionno']; 
		}

		$this->redirect($this->url->link('transaction/purchase_invoice', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	}

	public function getAllItemsInCart(){	
 		$pro = $this->cart->getProducts('cart_purchase');
 		foreach ($pro as $value) {
	 		$data[] = array('id'=>$value['product_id'],'code'=>$value['sku']);
 		}
		$this->response->setOutput(json_encode($data));
	}

	public function export_pdf(){

		$this->clearTempFolder();
		if(!empty($this->request->post['selected_orders'])){
			// printArray($this->request->post['selected_orders']); die;
			include(DIR_SERVER.'/MPDF/mpdf.php');
			foreach ($this->request->post['selected_orders'] as $value) {
				ob_end_clean();
				$mpdf = new mPDF('c','A4');
				$this->request->get['purchase_id'] = $value;
				$str = $this->download_pdf('1');
				$mpdf->mirrorMargins = 1;
				$mpdf->SetMargins(0, 0, 5);
				$mpdf->SetDisplayMode('fullpage');
				$mpdf->setFooter('{PAGENO}');
				$mpdf->SetFont('DejaVuSans');
				$stylesheet = file_get_contents(HTTP_SERVER.'view/stylesheet/pdf.css');
				$mpdf->WriteHTML($stylesheet,1);
				$mpdf->WriteHTML($str);
				$filename = 'download/purchase_invoice_'.$value.'.pdf';
				$mpdf->Output($filename,'F');
				ob_end_flush();
			}
		}
	}
	public function clearTempFolder(){   // for clear temp files in download folder
		$files = glob(DIR_SERVER.'download/*');
			foreach($files as $file){
				if(is_file($file))
				unlink($file);
		}
	}

	public function export_csv(){
		$this->load->model('transaction/purchase');

			ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=purchase_invoice.csv');
	        print "Order No,Order Date,Arraival Dates,Vendor,Net Total,FC Nettoal,Reference\r\n";
			
			foreach ($this->request->post['selectedXero'] as $purchase_id) {
				$purchaseInfo 	 = $this->model_transaction_purchase->getPurchaseInvoiceHeader($purchase_id);
				$vendor 	= $this->model_transaction_purchase->getVendor($purchaseInfo['vendor_id']);

				$invoice_no      = $purchaseInfo['transaction_no'];
				$order_date      = $purchaseInfo['transaction_date'];
				$arrival_dates   = $purchaseInfo['reference_date'].' - '.$purchaseInfo['reference_date'];
				$vandor          = $vendor['vendor_name'];
				$total           = $purchaseInfo['total'];
				$fc_subtotal     = $purchaseInfo['fc_subtotal'];
				$reference 		 = $purchaseInfo['reference_no'];

				print "$invoice_no,\"$order_date\",\"$arrival_dates\",\"$vandor\",\"$total\",\"$fc_subtotal\",\"$reference\"\r\n";
		}
	}

	public function uploadAttachment($invoice_no){
	    
	    $allowed_extension = array("png", "jpg", "jpeg", "pdf", "doc", "csv", "xls", "xlsx", "docx");
	    // Get image file extension
	    $file_extension = pathinfo($_FILES["attachment"]["name"], PATHINFO_EXTENSION);
	    
  		 // Validate file input to check if is not empty
	    if (! file_exists($_FILES["attachment"]["tmp_name"])) {
	            $response = '';
	    }
	    // Validate file input to check if is with valid extension
	    else if (! in_array($file_extension, $allowed_extension)) {
	            $response = '';
	    }
	    // Validate attachment file size
	    else if (($_FILES["attachment"]["size"] > 2000000)) {
	            $response = '';
	    }else {
			$temp = explode(".", $_FILES["attachment"]["name"]);
			$newfilename = 'Purchase_Invoice_'.$invoice_no.'.'.end($temp);
	        if (move_uploaded_file($_FILES["attachment"]["tmp_name"], DIR_SERVER.'uploads/PurchaseAttachment/'.$newfilename)) {
	            $response =  $newfilename;
	        } else {
	            $response = '';
	        }
	    }
		return $response;
	}
	public function removeAttachment(){
		$this->load->model('transaction/purchase');
		$attachmentFileName     = $this->request->post['fileName'];
		if($attachmentFileName != ''){
			$response = $this->model_transaction_purchase->removeInvoiceAttachment($this->request->post['purchase_id']);
			$oldFile = DIR_SERVER.'uploads/PurchaseAttachment/'.$attachmentFileName;
    		unlink($oldFile);
		} else {
	        $response = '';
		}
		echo $response;
	}	
}
?>