<?php
class ControllerTransactionCommonPurchase extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('transaction/common'); 
		$this->document->setTitle('Purchase');	
		$this->data['location_code'] = $this->session->data['location_code'];
		
		
		$this->data['heading_title'] = $this->language->get('purchase_heading_title');
		$this->data['text_general']  = 'Purchase';	
		$this->data['text_supplier'] = 'Supplier';	
		

		if($this->user->hasPermission('access', 'transaction/purchase') || $this->user->hasPermission('modify', 'transaction/purchase')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase", 'token=' . $this->session->data['token'], 'SSL'),
						                       "id"   => $this->language->get('purchase_id'),
				                               "Text" => 'Purchase Order',
				                               "icon" =>'fa fa-shopping-cart'
				                               );
		}		
		
		if($this->user->hasPermission('access', 'transaction/purchase_invoice') || $this->user->hasPermission('modify', 'transaction/purchase')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase_invoice", 'token=' . $this->session->data['token'], 'SSL'),
						                       "id"   => $this->language->get('purchase_id'),
				                               "Text" => 'Purchase Invoice',
				                               "icon" =>'fa fa-address-book-o'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/purchase_return') || $this->user->hasPermission('modify', 'transaction/purchase_return')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/purchase_return", 'token=' . $this->session->data['token'], 'SSL'),
				                               "id"     => $this->language->get('purchasereturn_id'),
				                               "Text" => $this->language->get('text_purchase_return'),
				                               "icon" =>'fa fa-truck'
				                               );
		}
		if($this->user->hasPermission('access', 'transaction/service_purchase') || $this->user->hasPermission('modify', 'transaction/service_purchase')){
			$this->data["general"][] = array( "link" => $this->url->link("transaction/service_purchase", 'token=' . $this->session->data['token'], 'SSL'),
				                               "id"     => $this->language->get('purchase_id'),
				                               "Text" => 'Service Note',
				                               "icon" =>'fa fa-address-book-o'
				                               );
		}

		if($this->user->hasPermission('access', 'master/vendor')){
		$this->data["sales"][] =  array( "link" =>$this->url->link("master/vendor", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',//$this->language->get('alt_suppliermaster'),
			                                  "id"     => $this->language->get('suppliermaster_id'),
			                                   "Text" => 'Supplier Master',
			                               	   "icon" =>'fa fa-th-large'
			                               );
		}

		$this->data['token'] = $this->session->data['token'];	
		$this->template = 'transaction/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
}
?>