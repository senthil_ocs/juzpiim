<?php
class ControllerTransactionTransactionReports extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/purchase');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($type=='purchase_summary'){
			if($print == 'printsummary'){
				$this->document->setTitle('Purchase Summary Print');
				$this->printsummary();
			} else {
				$this->document->setTitle('Purchase Summary');
				$this->getTRsummaryList();
			}
		}else if($type=='purchase_invoice'){
			if($print == 'printinvoice'){
				$this->document->setTitle('Purchase Invoice Summary Print');
				$this->printinvoice();
			} else {
				$this->document->setTitle('Purchase Invoice Summary');
				$this->getPIsummaryList();
			}
		}else{
			if($print == 'printdetails'){
				$this->document->setTitle('Purchase Summary Print');
				$this->printdetails();
			} else {
				$this->document->setTitle('Purchase Details');
				$this->getTRdetailsList();	
			}
		}
	}
	
	public function purchaseReturnSummary(){
		$this->language->load('transaction/reports');
		$this->load->model('transaction/purchase_return');
		$this->document->setTitle('Purchase Return Summary List');
		$this->getTRReturnsummaryList();

	}
	protected function getTRdetailsList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Purchase Details';
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$hold_button = '&show_hold=1';
		$page = $_REQUEST['page'];
		$this->data['purchases'] = array();
		$this->data['exportpurchasedetails'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_details&print=printdetails'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_details&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$purchase_total = $this->model_transaction_purchase->getTotalPurchase($data);
		$results = $this->model_transaction_purchase->getPurchaseProductList($data);
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		
		foreach ($results as $result) {	
			$inventoryCodeAry = $this->model_transaction_purchase->getproductdetails($result['product_id']);		
			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'inventory_code'    => $inventoryCodeAry['sku'],
				'qty'  				=> $result['quantity'],
				'price'     		=> $result['price'],
				'net_price'         => $result['net_price'],
				'tax_price'         => $result['tax_price'],
				'total' 	        => number_format($result['net_price']+$result['tax_price'],4),
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date("d-m-Y h:i A", strtotime($result['created_date'])),
				'modify_button' => $this->url->link('transaction/purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL')
			);
		}
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_supplier'=>$this->request->get['filter_supplier'],
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_purchase->getPurchaseProductList($data);
			foreach ($results as $result) {	
				$inventoryCodeAry = $this->model_transaction_purchase->getproductdetails($result['product_id']);		
				$this->data['exportpurchasedetails'][] = array(
					'purchase_id'       => $result['purchase_id'],
					'transaction_no'    => $result['transaction_no'],
					'inventory_code'    => $inventoryCodeAry['sku'],
					'qty'  				=> $result['quantity'],
					'price'     		=> $result['price'],
					'net_price'         => $result['net_price'],
					'tax_price'         => $result['tax_price'],
					'total' 	        => number_format($result['net_price']+$result['tax_price'],4),
					'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname']
				);
			}
			$this->model_transaction_purchase->export_purchase_details_to_csv($this->data['exportpurchasedetails']); exit;
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports&type=purchase_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->template = 'transaction/purchase_details_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getTRReturnsummaryList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}

	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);
		$this->data['data'] = $data;
		$purchase_grand_totals = $this->model_transaction_purchase_return->getTotalPurchaseListNew($data);
		// printArray($purchase_grand_totals); die;
		$purchase_total = $purchase_grand_totals['total'];
		$this->data['purchase_return_grand_total'] = $purchase_grand_totals['grandtotal'];

		$this->data['vendors'] = $this->model_transaction_purchase_return->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/purchaseReturnSummary&print=printsummary'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/purchaseReturnSummary_csv'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$results = $this->model_transaction_purchase_return->getPurchaseReturnList($data);
		// printArray($results); die;

		foreach ($results as $result) {	
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$userDetail   = $this->model_user_user->getUser($result['created_by']);
			
			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'location_code'     => $result['location_code'],
				'vendor_code'       => $vendorDetail['vendor_code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'net_total'         => $result['total'],
				'total'             => $result['total'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date('d/m/Y',strtotime($result['created_date'])),
				'modify_button' => $this->url->link('transaction/purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'purchasedetails'   => $this->url->link('inventory/reports/purchase_return_details', 'token=' . $this->session->data['token'] . '&filter_transactionno=' . $result['transaction_no'] .'&filter_location_code='.$filter_location_code.' '.$url, 'SSL')
			);
		}
		
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_supplier'      => $this->request->get['filter_supplier'],
				'filter_transactionno' => $this->request->get['filter_transactionno'],
				'filter_date_from'     => $this->request->get['filter_date_from'],
				'filter_date_to'       => $this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_purchase->getPurchaseList($data);
			foreach ($results as $result) {		
				$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
				$userDetail   = $this->model_user_user->getUser($result['created_by']);

				$this->data['exportpurchases'][] = array(
					'purchase_id'       => $result['purchase_id'],
					'transaction_no'    => $result['transaction_no'],
					'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
					'transaction_type'  => $result['transaction_type'],
					'vendor_code'       => $vendorDetail['code'],
					'vendor_name'       => $vendorDetail['vendor_name'],
					'sub_total'         => $subTotal,
					'gst'				=> $gst,
					'net_total'         => $netTotal,
					'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname']
				);
			}
			$this->model_transaction_purchase->export_purchase_summary_to_csv($this->data['exportpurchases']); exit;
		}
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/purchaseReturnSummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_purchaseReturnSummary_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_return_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	
	public function purchaseReturnSummary_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase_return');
		$this->load->model('user/user');
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);
		$data['filter_stock'] =$this->config->get('config_report_qty_negative');

		$results = $this->model_transaction_purchase_return->getPurchaseList($data);
		$tot_quantity = 0;
		$tot_value = 0;
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=purchase_return.csv');
	        print "S.No,Tran No,Tran Date,Location,Vendor Name,Reference No,Reference Date,Sub total,GST,Net Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
				$j++;
				$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
				$userDetail   = $this->model_user_user->getUser($result['created_by']);
				$subTotal 	  = number_format($result['sub_total'],2);
				$gst      	  = number_format($result['gst'],2);
				$netTotal 	  = number_format($result['total'],2);
				$total 		  +=$netTotal; 
				$total        = number_format($total,2);

				$transaction_no   = $result['transaction_no'];
				$refno  		  = $result['reference_no'];
				$refdate   		  = $result['reference_date'];
				$transaction_date = $result['transaction_date'];
				$location 		  = $result['location_code'];
				$vendor_code      = $vendorDetail['vendor_code'];
				$vendor_name      = $vendorDetail['vendor_name'];

				print "$j,\"$transaction_no\",\"$transaction_date\",\"$location\",\"$vendor_name\",\"$refno\",\"$refdate\",\"$subTotal \",\"$gst\",\"$netTotal\"\r\n";
			}
				print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\r\n";
		}
	}

	public function export_purchaseReturnSummary_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('transaction/purchase_return');
		$this->load->model('master/vendor');

		$filterStr = '';

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}

		if(isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$filterStr.=" Supplier: ".$filter_supplier.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location : ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'=> $filter_location_code
		);

		$results = $this->model_transaction_purchase_return->getPurchaseList($data);
		// printArray($results);die;
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Return Summary Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing'><thead>
			<tr>
				<th>S No</th>
				<th>Tran No</th>
				<th>Tran Date</th>
				<th>Location</th>
				<th>Vendor Name</th>
				<th>Referennce No</th>
				<th>Reference Date</th>
				<th>Sub Total</th>
				<th>GST</th>
				<th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$netTotal = $this->model_transaction_purchase_return->getTotalPurchaseHistory($result['purchase_id'],'total');

			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$subTotal = number_format($result['sub_total'],2);
			$gst      = number_format($result['gst'],2);

			$transaction_no   = $result['transaction_no'];
			$location_code    = $result['location_code'];
			$transaction_date = date('d/m/Y',strtotime($result['transaction_date']));
			$transaction_type = $result['transaction_type'];
			$reference_no 	  = $result['reference_no'];
			$reference_date   = date('d/m/Y',strtotime($result['reference_date']));
			$vendor_code      = $vendorDetail['vendor_code'];
			$vendor_name      = $vendorDetail['vendor_name'];
			$net_total        = $netTotal;
			$total            = number_format($result['total'],2);

			$total_Stock_Value+=$total;
			$str.="<tr>
				   <td>".$j."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td>".$location_code."</td>
				   <td>".$vendor_name."</td>
				   <td>".$reference_no."</td>
				   <td>".$reference_date."</td>
				   <td class='right'>".$subTotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$total."</td>
			</tr>";	
		}
		$str.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td class='right'>Total</td>
		<td class='right'>".number_format($total_Stock_Value,2)."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'Purchase_ReturnSummary_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	protected function getPIsummaryList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.= '&filter_reference_date='.$filter_location_code;
		} else {
			$filter_reference_date = $this->session->data['filter_reference_date'];
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];

		$data = array(
			'show_hold'	  		=> $show_hold,
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit'),
			'filter_supplier'		=>$filter_supplier,
			'filter_transactionno'	=>$filter_transactionno,
			'filter_date_from'		=>$filter_date_from,
			'filter_date_to'		=>$filter_date_to,
			'filter_reference_date' =>$filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);

		$this->data['data'] = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPurchaseInvoice($data);
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['oveallTotal']= number_format($purchase_grand_total['grandtotal'],2);
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');

		foreach ($results as $result) {	

			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_code'],
				'vendor_name'       => $result['vendor_name'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'modify_button'     => $this->url->link('transaction/purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'purchasedetails'   => $this->url->link('inventory/reports/purchase_invoice_details', 'token=' . $this->session->data['token'] . '&filter_transactionno=' . $result['transaction_no'] .'&filter_location_code='.$filter_location_code.' '.$url, 'SSL')
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;

		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports&type=purchase_invoice', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_purchase_invoice_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_purchase_invoice_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_invoice_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getTRsummaryList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.= '&filter_reference_date='.$filter_location_code;
		} else {
			$filter_reference_date = $this->session->data['filter_reference_date'];
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  			=> $show_hold,
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			=> $this->config->get('config_admin_limit'),
			'filter_supplier' 		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from' 		=> $filter_date_from,
			'filter_date_to' 		=> $filter_date_to,
			'filter_reference_date' => $filter_reference_date,
			'filter_location_code'	=> $filter_location_code

		);

		$this->data['data'] = $data;
		$results = $this->model_transaction_purchase->getPurchaseList($data);
		$this->data['totals']  = $this->model_transaction_purchase->getTotalPurchaseNew($data);
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		// printArray($purchase_grand_total); die;

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail   = array();
	
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_summary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_summary&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_purchase_summary_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_purchase_summary_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		foreach ($results as $result) {
			// printArray($result); die;

			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_id'],
				'vendor_name'       => $result['vendor_name'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'modify_button'     => $this->url->link('transaction/purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL'),
				'purchasedetails'   => $this->url->link('inventory/reports/purchase_details', 'token=' . $this->session->data['token'] . '&filter_transactionno=' . $result['transaction_no'] .'&filter_location_code='.$filter_location_code.' '.$url, 'SSL')
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;

		$pagination = new Pagination();
		$pagination->total = $this->data['totals']['totalPurchase'];
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports&type=purchase_summary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_purchase_invoice_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}

		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);


		$data['filter_stock'] =$this->config->get('config_report_qty_negative');

		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);

		$tot_quantity = 0;
		$tot_value = 0;

		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=PurchaseInvoiceSummaryReport.csv');
	        print "Tran No,Tran Date,Location,Supplier,Reference No,Due Date,Total,GST,NetTotal\r\n";
		
			$j=0; 
			foreach ($results as $result) {	
				$subTotal = number_format($result['sub_total'],2);
				$gst 	  = number_format($result['gst'],2);
				$netTotal = number_format($result['total'],2);
				$grandtotal+=$netTotal; 

				
				$purchase_id 	=  $result['purchase_id'];
				$transaction_no =  $result['transaction_no'];
				$transaction_date= date('d/m/Y',strtotime($result['transaction_date']));
				$location_code  = $result['location_code'];
				$supplier 		= $result['vendor_name'];
				$refno 			= $result['reference_no'];
				$refdate 		= date('d/m/Y',strtotime($result['reference_date']));


				print "\"$transaction_no\",\"$transaction_date\",\"$location_code\",\"$supplier\",\"$refno\",\"$refdate\",\"$subTotal\",\"$gst\",\"$netTotal\"\r\n";
			}
				$grandtotal = number_format($grandtotal,2);
				print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$grandtotal\"\r\n";
			
		}  
	}

	public function export_purchase_invoice_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}

		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		// $results= $this->model_inventory_inventory->getProductstock($data);
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);
		// printArray($results);die;

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Invoice Summary Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';

		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing'><thead>
			<tr>
             <th>S No</th>
             <th>Tran No</th>
             <th>Tran Date</th>
             <th>Location</th>
             <th>Supplier</th>
             <th>Reference No</th>
             <th>Due Date</th>
             <th>Subtotal</th>
             <th>GST</th>
             <th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		$total = 0;
		foreach ($results as $result) {
			$j++;
			$subTotal = number_format($result['sub_total'],2);
			$gst 	  = number_format($result['gst'],2);
			$netTotal = number_format($result['total'],2);		
			$total 	  +=$result['total']; 

			$transaction_no   = $result['transaction_no'];
			$transaction_date = date('d/m/Y',strtotime($result['transaction_date']));
			$transaction_type = $result['transaction_type'];
			$location_code    = $result['location_code'];
			$vendor_name      = $result['vendor_name'];
			$refno      	  = $result['reference_no'];
			$refdate      	  =	date('d/m/Y',strtotime($result['reference_date']));

			$str.="<tr>
					<td>".$j."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td>".$location_code."</td>
				   <td>".$vendor_name."</td>
				   <td>".$refno."</td>
				   <td>".$refdate."</td>
				   <td class='right'>".$subTotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$netTotal."</td>
			</tr>";	
		}
		$total = number_format($total,2);
		$str.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td>
		<td class='right'>".$total."</td></tr>";
		$str.="</tbody></table>";
		if($str){
			$filename = 'Purchase_Invoice_Summary_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}


	public function export_purchase_summary_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}

		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);


		$data['filter_stock'] =$this->config->get('config_report_qty_negative');

		$results = $this->model_transaction_purchase->getPurchaseList($data);

		$tot_quantity = 0;
		$tot_value = 0;

		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=PurchaseSummaryReport.csv');
	        print "Tran No,Tran Date,Location,Supplier,Total,GST,NetTotal\r\n";
		
			$j=0; 
			foreach ($results as $result) {	
				$subTotal = number_format($result['sub_total'],2);
				$gst 	  = number_format($result['gst'],2);
				$netTotal = number_format($result['total'],2);
				$grandtotal+= $result['total']; 
				
				$purchase_id 	=  $result['purchase_id'];
				$transaction_no =  $result['transaction_no'];
				$transaction_date= date('d/m/Y',strtotime($result['transaction_date']));
				$location_code  = $result['location_code'];
				$supplier 		= $result['vendor_name'];
				$refno 			= $result['reference_no'];
				$refdate 		= date('d/m/Y',strtotime($result['reference_date']));

				print "\"$transaction_no\",\"$transaction_date\",\"$location_code\",\"$supplier\",\"$subTotal\",\"$gst\",\"$netTotal\"\r\n";
			}
				$grandtotal = number_format($grandtotal,2);
				print "\"\",\"\",\"\",\"\",\"\",\"Total\",\"$grandtotal\"\r\n";
			
		}  
	}

	public function export_purchase_summary_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}

		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		// $results= $this->model_inventory_inventory->getProductstock($data);
		$results = $this->model_transaction_purchase->getPurchaseList($data);
		// printArray($results);die;

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Order Summary Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';

		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing' style='width:100%;'><thead>
			<tr>
             <th>S No</th>
             <th>Tran No</th>
             <th>Tran Date</th>
             <th>Location</th>
             <th>Supplier</th>
             <th>Subtotal</th>
             <th>GST</th>
             <th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		$total = 0;
		foreach ($results as $result) {
			$j++;
			$subTotal = number_format($result['sub_total'],2);
			$gst 	  = number_format($result['gst'],2);
			$netTotal = number_format($result['total'],2);		
			$total +=$result['total']; 

			$transaction_no   = $result['transaction_no'];
			$transaction_date = date('d/m/Y',strtotime($result['transaction_date']));
			$transaction_type = $result['transaction_type'];
			$location_code    = $result['location_code'];
			$vendor_name      = $result['vendor_name'];
			$refno      	  = $result['reference_no'];
			$refdate      	  =	date('d/m/Y',strtotime($result['reference_date']));

			$str.="<tr>
					<td>".$j."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td>".$location_code."</td>
				   <td>".$vendor_name."</td>
				   <td class='right'>".$subTotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$netTotal."</td>
			</tr>";	
		}
		$total = number_format($total,2);
		$str.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td>
		<td class='right'>".$total."</td></tr>";
		$str.="</tbody></table>";

		if($str){
			$filename = 'Purchase_Summary_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	protected function printinvoice() {	
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}	
		$hold_button = '&show_hold=1';
		$this->data['back'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->load->model('merchadising/promotions_report');
		$this->load->model('inventory/inventory');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_merchadising_promotions_report->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$purchase_total = $this->model_transaction_purchase->getTotalPurchaseInvoice($data);
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$vendorDetail = array();
		$gst = 0;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice/printinvoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		foreach ($results as $result) {			
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$subTotal = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'sub_total');
			$gst      = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'tax');
			$netTotal = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'total');
						
			$this->data['purchases'][] = array(
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => $result['transaction_date'],
				'vendor_code'       => $vendorDetail['code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'sub_total'         => $subTotal,
				'gst'				=> $gst,
				'net_total'         => $netTotal
			);
		}
		$this->template = 'transaction/print.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function printsummary() {	
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}	
		$hold_button = '&show_hold=1';
		$this->data['back'] = $this->url->link('transaction/transaction_reports&type=purchase_summary', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->load->model('merchadising/promotions_report');
		$this->load->model('inventory/inventory');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_merchadising_promotions_report->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$purchase_total = $this->model_transaction_purchase->getTotalPurchase($data);
		$results = $this->model_transaction_purchase->getPurchaseList($data);
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$vendorDetail = array();
		$gst = 0;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_summary/printsummary', 'token=' . $this->session->data['token'] . $url, 'SSL');
		foreach ($results as $result) {			
			$vendorDetail = $this->model_master_vendor->getVendor($result['vendor_id']);
			$subTotal = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'sub_total');
			$gst      = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'tax');
			$netTotal = $this->model_transaction_purchase->getTotalPurchaseHistory($result['purchase_id'],'total');
						
			$this->data['purchases'][] = array(
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => $result['transaction_date'],
				'vendor_code'       => $vendorDetail['code'],
				'vendor_name'       => $vendorDetail['vendor_name'],
				'sub_total'         => $subTotal,
				'gst'				=> $gst,
				'net_total'         => $netTotal
			);
		}
		$this->template = 'transaction/print.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']);
			$company_address['state']	 = strtoupper($company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
			$company_address['postal_code'] = $company_details['postal_code'];
			$company_address['email'] = $company_details['email'];
			$company_address['web_url'] = $company_details['web_url'];
			$company_address['business_reg_no'] = $company_details['business_reg_no'];
			
		}
		return $company_address;
	}
	protected function printdetails() {	
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		$this->data['back'] = $this->url->link('transaction/transaction_reports&type=purchase_details', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->load->model('merchadising/promotions_report');
		$this->load->model('inventory/inventory');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_merchadising_promotions_report->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$hold_button = '&show_hold=1';
		$page = $_REQUEST['page'];
		$this->data['purchases'] = array();
		$data = array(
			'show_hold'	  => $show_hold,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_details&print=printdetails'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$purchase_total = $this->model_transaction_purchase->getTotalPurchase($data);
		$results = $this->model_transaction_purchase->getPurchaseProductList($data);
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		
		foreach ($results as $result) {	
			$inventoryCodeAry = $this->model_transaction_purchase->getproductdetails($result['product_id']);		
			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'inventory_code'    => $inventoryCodeAry['sku'],
				'qty'  				=> $result['quantity'],
				'price'     		=> $result['price'],
				'net_price'         => $result['net_price'],
				'tax_price'         => $result['tax_price'],
				'total' 	        => number_format($result['net_price']+$result['tax_price'],4),
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date("d-m-Y h:i A", strtotime($result['created_date'])),
				'modify_button' => $this->url->link('transaction/purchase/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['purchase_id'] . $url, 'SSL')
			);
		}
		$this->template = 'transaction/printdetails.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function salesquotation() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printquotation'){
			$this->document->setTitle('Sales Summary Print');
			$this->printquotation();
		} else {
			$this->document->setTitle('Sales Summary');
			$this->getsalesquotationList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesquotation&print=printquotation'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesquotation&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesquotationList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id			= $this->session->data['company_id'];
		$stock_report 		= $this->request->get['stock_report'];
		$company_details 	= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo		= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';

		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl .= '&filter_channel='.$filter_channel; 
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Summary Reports',
			'href'      => $this->url->link('transaction/transaction_reports/salessummary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'            	=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=> $filter_supplier,
			'filter_transaction'=> $filter_transactionno,
			'filter_from_date'	=> $filter_date_from,
			'filter_to_date'	=> $filter_date_to,
			'filter_channel'	=> $filter_channel,
			'filter_location'	=> $filter_location,
			'location_code' 	=> trim($filter_location)
		);
		$this->data['data'] = $data;
		$this->load->model('user/user');
		
		$sale_totalAry = $this->model_transaction_sales->getTotalSalesQuotation($data);
		$this->data['sales_net_total'] = $sale_totalAry['net_total'];
		$sale_total    = $sale_totalAry['totalsales'];
		$results = $this->model_transaction_sales->getSalesQuotationList($data);
		
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');

		foreach ($results as $result) {
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			
			$this->data['sales'][] = array(
				'location_code'     => $result['location_code'],
				'transaction_no'    => $result['transaction_no'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'sub_total'  		=> $result['sub_total'],
				'discount'  		=> $result['discount'],
				'gst'       		=> $result['gst'],
				'actual_total'      => $result['actual_total'],
				'round_off'         => $result['round_off'],
				'net_total'			=> $result['total'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['createdon'],
				'salesDetail'       => $this->url->link('transaction/transaction_reports/salesquotationdetail', 'token=' . $this->session->data['token'] . '&filter_transactionno='.$result['transaction_no'])
				
			);
		}
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_from_date'=>$this->request->get['filter_date_from'],
				'filter_to_date'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesQuotationList($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'location_code'       => $result['location_code'],
				'transaction_no'         => $result['transaction_no'],
				'transaction_date'        => date('d/m/Y',strtotime($result['transaction_date'])),
				'sub_total'           => $result['sub_total'],
				'discount'            => $result['discount'],
				'gst'                 => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				
			);

			}
			$this->model_transaction_sales->export_sales_quotation_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salessummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;

		$this->data['channels'] 		= $this->cart->getChannelsList();
		$this->data['Tolocations'] 	 	=  $this->cart->getLocation();
		$this->data['link_pdfexport'] 	= $this->url->link('transaction/transaction_reports/export_salesquotation_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] 	= $this->url->link('transaction/transaction_reports/export_salesquotation_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['back'] 			= $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/sales_quotation_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_salesquotation_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier' 		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_channel' 		=> $filter_channel,
			'filter_date_from' 		=> $filter_date_from,
			'filter_date_to' 		=> $filter_date_to,
			'location_code' 		=> trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesQuotationListPdf($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=salesquotation.csv');
	        print "S.No,Location Code,Transaction No,Transaction Date,Sub Total,Discount,GST,Actual Total,Round Off,Net Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$j++;
						$location_code  = $result['location_code'];
						$transaction_no     = $result['transaction_no'];
						$transaction_date   = date('d/m/Y',strtotime($result['transaction_date']));
						$sub_total  	= $result['sub_total'];
						$discount  		= $result['discount'];
						$gst       		= $result['gst'];
						$actual_total   = $result['actual_total'];
						$round_off      = $result['round_off'];
						$net_total		= $result['total'];
						$total+=$net_total;
		
				print "$j,\"$location_code\",\"$transaction_no\",\"$transaction_date\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\",\"$round_off\",\"$net_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}  	 
		
	}
	public function export_salesquotation_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_channel'=>$filter_channel,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesQuotationListPdf($data);

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		$filter_channel_name = $this->model_transaction_sales->getNetworkName($filter_channel);

		if($filter_channel) {
			$filterStr.=" Sales Channel : ".$filter_channel_name.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction no : ".$filter_transactionno.',';
		}
		if($filter_location) {
			$filterStr.=" Location : ".$filter_location.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Quotation Summary Reports</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<th>Location Code</th>
			<th>Invoice No</th>
			<th>Invoice Date</th>
			<th>Sub Total</th>
			<th>Discount</th>
			<th>GST</th>
			<th>Actual Total</th>
			<th>Round Off</th>
			<th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$location_code  = $result['location_code'];
			$transaction_no     = $result['transaction_no'];
			$transaction_date   = date('d/m/Y',strtotime($result['transaction_date']));
			$sub_total  	= $result['sub_total'];
			$discount  		= $result['discount'];
			$gst       		= $result['gst'];
			$actual_total   = $result['actual_total'];
			$round_off      = $result['round_off'];
			$net_total		= $result['total'];
			
			$total_Stock_Value+=$net_total;

			$str.="<tr><td>".$j."</td>
				   <td>".$location_code."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td class='right'>".$sub_total."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$actual_total."</td>
				   <td class='right'>".$round_off."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
		}
		$str.="<tr><td colspan='9' align='right'>Total</td><td align='right'>".$total_Stock_Value."</td>
		</tr></tbody></table>";
		if($str){
			$filename = 'SalesQuotationReport_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function salesinvoice() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Sales Invoice Print');
			$this->printinvoice();
		} else {
			$this->document->setTitle('Sales Invoice Summary Repoert');
			$this->getsalesinvoiceList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesinvoiceList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
		} else {
			$filter_from_date = date('d/m/Y',strtotime('-1 day'));
		}
		$pageUrl.= '&filter_from_date='.$filter_from_date;
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
		} else {
			$filter_to_date = date('d/m/Y');	
		}		
		$pageUrl.= '&filter_to_date='.$filter_to_date;
		// echo $pageUrl; die;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$filter_customer = '';
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$pageUrl.='&filter_customer='.$filter_customer; 
		}
		$filter_delivery_status = '';
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
			$pageUrl.='&filter_delivery_status='.$filter_delivery_status; 
		}
		$filter_payment_status = '';
		if (isset($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
			$pageUrl.='&filter_payment_status='.$filter_payment_status; 
		}
		$filter_xero = '';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$pageUrl.='&filter_xero='.$filter_xero; 
		}
		$filter_network = '';
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
			$pageUrl.='&filter_network='.$filter_network; 
		}
		$filter_agent = '';
		if (isset($_REQUEST['filter_agent'])) {
			$filter_agent = $_REQUEST['filter_agent'];
			$pageUrl.='&filter_agent='.$filter_agent; 
		}
		$url = '';	
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice Summary Reports',
			'href'      => $this->url->link('transaction/transaction_reports/salesinvoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$data = array(
			'filter_from_date'	  	=> $filter_from_date,
			'filter_to_date'	  	=> $filter_to_date,
			'filter_location'	  	=> $filter_location,
			'filter_transaction'  	=> $filter_transaction,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_customer' 		=> $filter_customer,
			'filter_payment_status' => $filter_payment_status,
			'filter_network'        => $filter_network,
			'filter_agent'          => $filter_agent,
			'filter_xero'           => $filter_xero,
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'            		=> $this->config->get('config_admin_limit')
		);
		// printArray($data); die;
		$this->data['data'] = $data;
		$this->load->model('user/user');
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_from_date='.$filter_from_date.'&filter_to_date='.$filter_to_date;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&print=printinvoice'.$strUrl,'token='.$this->session->data['token'].$url,'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesinvoice&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$sale_totalAry 		 	= $this->model_transaction_sales->getTotalSalesInvoiceForReports($data);
		$this->data['networks'] 	   = $this->cart->getChannelsList();
		$this->data['sales_net_total'] = $sale_totalAry['net_total'];
		$sale_total    		 		   = $sale_totalAry['totalsales'];
		$results                = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);
		$this->data['sales']    = array();

		foreach ($results as $result) {	
			$customer 				  = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
			$result['payment_status'] = $result['status'] == 'Canceled' ? 'Canceled' : $result['payment_status']; 
			$this->data['sales'][]    = array(
				'location_code'   => $result['location_code'],
				'invoice_no'      => $result['invoice_no'],
				'invoice_date'    => date('d/m/Y',strtotime($result['invoice_date'])),
				'sub_total' 	  => $result['sub_total'],
				'discount' 		  => $result['discount'],
				'delivery_status' => $result['delivery_status'],
				'payment_status'  => $result['payment_status'],
				'customer' 		  => $customer['name'],
				'gst'      		  => $result['gst'],
				'actual_total'    => $result['actual_total'],
				'round_off'       => $result['round_off'],
				'net_total'		  => $result['net_total'],
				'salesDetail'     => $this->url->link('transaction/transaction_reports/salesinvoicedetail', 'token=' . $this->session->data['token'] . '&filter_transactionno='.$result['invoice_no'])
			);
		}

		$url.= $pageUrl;
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salesinvoice', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['Tolocations'] 	  =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['users'] 		  = $this->model_user_user->getUserDetails();
		$this->data['salesmanlist']	  = $this->model_transaction_sales->getSalesMansList();
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_salesinvoice_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_salesinvoice_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template 	= 'transaction/sales_invoice_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_salesinvoice_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('user/user');
		$pageUrl ='';

		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
		} else {
			$filter_from_date = date('d/m/Y',strtotime('-1 day'));
		}
		$pageUrl.= '&filter_from_date='.$filter_from_date;
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
		} else {
			$filter_to_date = date('d/m/Y');	
		}		
		$pageUrl.= '&filter_to_date='.$filter_to_date;
		// echo $pageUrl; die;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$filter_customer = '';
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$pageUrl.='&filter_customer='.$filter_customer; 
		}
		$filter_delivery_status = '';
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
			$pageUrl.='&filter_delivery_status='.$filter_delivery_status; 
		}
		$filter_payment_status = '';
		if (isset($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
			$pageUrl.='&filter_payment_status='.$filter_payment_status; 
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$pageUrl.='&filter_xero='.$filter_xero; 
		}
		$filter_network = '';
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
			$pageUrl.='&filter_network='.$filter_network; 
		}
		$filter_agent = '0';
		if (isset($_REQUEST['filter_agent'])) {
			$filter_agent = $_REQUEST['filter_agent'];
			$pageUrl.='&filter_agent='.$filter_agent; 
		}
		$data = array(
			'filter_from_date'	  	=> $filter_from_date,
			'filter_to_date'	  	=> $filter_to_date,
			'filter_location'	  	=> $filter_location,
			'filter_transaction'  	=> $filter_transaction,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_customer' 		=> $filter_customer,
			'filter_payment_status' => $filter_payment_status,
			'filter_network'        => $filter_network,
			'filter_agent'          => $filter_agent,
			'filter_xero'           => $filter_xero
		);

		$results = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=salesinvoice.csv');
	        print "S.No,Location Code,Invoice No,Invoice Date,Customer,Order No,Sales Agent,Delivery,Payment,Sub Total,Discount,GST,Actual Total,Round Off,Net Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
				$customer = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code'])['name'];
				$user  = $this->model_user_user->getUser($result['createdby']);

						$j++;
						$location_code  = $result['location_code'];
						$invoice_no     = $result['invoice_no'];
						$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
						$customer 	      = $customer;
						$sales_agent 	  = $user['firstname'].' '.$user['lastname'];
						$delivery_status  = $result['delivery_status'];
						$payment_status   = $result['payment_status'];
						$network_order_id = $result['network_order_id'];
						$sub_total  	  = $result['sub_total'];
						$discount  		  = $result['discount'];
						$gst       		= $result['gst'];
						$actual_total   = $result['actual_total'];
						$round_off      = $result['round_off'];
						$net_total		= $result['net_total'];
						$total+=$net_total;
		
				print "$j,\"$location_code\",\"$invoice_no\",\"$invoice_date\",\"$customer\",\"$network_order_id\",\"$sales_agent\",\"$delivery_status\",\"$payment_status\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\",\"$round_off\",\"$net_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}  	 
		
	}


	public function export_salesinvoice_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('user/user');
		$pageUrl ='';

		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
		} else {
			$filter_from_date = date('d/m/Y',strtotime('-1 day'));
		}
		$pageUrl.= '&filter_from_date='.$filter_from_date;
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
		} else {
			$filter_to_date = date('d/m/Y');	
		}		
		$pageUrl.= '&filter_to_date='.$filter_to_date;
		// echo $pageUrl; die;
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$filter_customer = '';
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
			$pageUrl.='&filter_customer='.$filter_customer; 
		}
		$filter_delivery_status = '';
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
			$pageUrl.='&filter_delivery_status='.$filter_delivery_status; 
		}
		$filter_payment_status = '';
		if (isset($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
			$pageUrl.='&filter_payment_status='.$filter_payment_status; 
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$pageUrl.='&filter_xero='.$filter_xero; 
		}
		$filter_network = '';
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
			$pageUrl.='&filter_network='.$filter_network; 
		}
		$filter_agent = '';
		if (isset($_REQUEST['filter_agent'])) {
			$filter_agent = $_REQUEST['filter_agent'];
			$pageUrl.='&filter_agent='.$filter_agent; 
		}
		$data = array(
			'filter_from_date'	  	=> $filter_from_date,
			'filter_to_date'	  	=> $filter_to_date,
			'filter_location'	  	=> $filter_location,
			'filter_transaction'  	=> $filter_transaction,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_customer' 		=> $filter_customer,
			'filter_payment_status' => $filter_payment_status,
			'filter_network'        => $filter_network,
			'filter_agent'          => $filter_agent,
			'filter_xero'           => $filter_xero
		);

		$results = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);
		// printArray($results); die;

		if($filter_from_date){
			$filterStr.=" From Date: ".$filter_from_date.',';
		}
		if($filter_to_date){
			$filterStr.=" To Date: ".$filter_to_date.',';
		}
		if($filter_location) {
			$filterStr.=" Location : ".$filter_location.',';
		}
		if($filter_transaction) {
			$filterStr.=" Invoice no : ".$filter_transaction.',';
		}
		if($filter_customer) {
			$customer = $this->model_transaction_sales->getB2BCustomersbyCode($filter_customer)['name'];
			$filterStr.=" Customer : ".$customer.',';
		}
		if($filter_delivery_status) {
			$filterStr.=" Delivery : ".$filter_delivery_status.',';
		}
		if($filter_payment_status) {
			$filterStr.=" Payment : ".$filter_payment_status.',';
		}
		if($filter_network) {
            $filter_network =$this->model_transaction_sales->getNetworkName($filter_network);
			$filterStr.=" Network : ".$filter_network.',';
		}
		if($filter_xero) {
			$filter_xero = $filter_xero =='1' ? 'Posted' : 'Not Posted';
			$filterStr.=" Xero : ".$filter_xero.',';
		}
		if($filter_agent) {
            $agentName = $this->model_transaction_sales->getSalesMansDetails($filter_agent);
			$filterStr.= " Agent : ".$agentName['name'];
		}
		$company_id	     = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	     = $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Invoice Summary Reports</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<th>Location Code</th>
			<th>Invoice No</th>
			<th>Invoice Date</th>
			<th>Customer</th>
			<th>Order No</th>
			<th>Sales Agent</th>
			<th>Delivery</th>
			<th>Payment</th>
			<th>Sub Total</th>
			<th>Discount</th>
			<th>GST</th>
			<th>Actual Total</th>
			<th>Round Off</th>
			<th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$customer 		= $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
			$user  			= $this->model_user_user->getUser($result['createdby']);
			$location_code  = $result['location_code'];
			$invoice_no     = $result['invoice_no'];
			$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
			$customer  		= $customer['name'];
			$sales_agent 	= $user['firstname'].' '.$user['lastname'];
			$delivery_status= $result['delivery_status'];
			$payment_status = $result['payment_status'];
			$sub_total  	= $result['sub_total'];
			$discount  		= $result['discount'];
			$gst       		= $result['gst'];
			$actual_total   = $result['actual_total'];
			$round_off      = $result['round_off'];
			$net_total		  = $result['net_total'];
			$network_order_id = $result['network_order_id'];		
			$total_Stock_Value+=$net_total;

			$str.="<tr><td>".$j."</td>
				   <td>".$location_code."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td class='right'>".$customer."</td>
				   <td class='right'>".$network_order_id."</td>
				   <td class='right'>".$sales_agent."</td>
				   <td class='right'>".$delivery_status."</td>
				   <td class='right'>".$payment_status."</td>
				   <td class='right'>".$sub_total."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$actual_total."</td>
				   <td class='right'>".$round_off."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
		}
		$str.="<tr><td colspan='14' align='right'>Total</td><td align='right'>".$total_Stock_Value."</td>
		</tr></tbody></table>";
		if($str){
			$filename = 'SalesInvoiceReport_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function salessummary() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Sales Summary Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Sales Summary');
			$this->getsalessummaryList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummary&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalessummaryList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Summary Reports',
			'href'      => $this->url->link('transaction/transaction_reports/salessummary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'            	=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=> $filter_supplier,
			'filter_transactionno'=> $filter_transactionno,
			'filter_channel' 	=> $filter_channel,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location' 	=> $filter_location
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSales($data);
		$this->data['sales_net_total'] = $sale_totalAry['net_total'];
		$sale_total    = $sale_totalAry['totalSale'];
		$results = $this->model_transaction_sales->getSalesList($data);

		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummary&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		foreach ($results as $result) {	
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			
			$this->data['sales'][] = array(
				'location_code'       => $result['location_code'],
				'invoice_no'          => $result['invoice_no'],
				'invoice_date'        => date('d/m/Y',strtotime($result['invoice_date'])),
				'sub_total'  		  => $result['sub_total'],
				'discount' 			  => $result['discount'],
				'gst'       		  => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['net_total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'        => $result['createdon'],
				'salesDetail'         => $this->url->link('transaction/transaction_reports/salesdetail', 'token=' . $this->session->data['token'] . '&filter_transactionno='.$result['invoice_no'])
				
			);
		}
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno' =>$this->request->get['filter_transactionno'],
				'filter_date_from'     =>$this->request->get['filter_date_from'],
				'filter_date_to'       =>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesList($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'location_code'       => $result['location_code'],
				'invoice_no'          => $result['invoice_no'],
				'invoice_date'        => date('d/m/Y',strtotime($result['invoice_date'])),
				'sub_total'           => $result['sub_total'],
				'discount'            => $result['discount'],
				'gst'                 => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['net_total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				
			);

			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);
		$this->data['Tolocations'] 	 	   = $this->cart->getLocation();

		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/salessummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] 	= $pagination->render();
		$this->data['sort'] 		= $sort;
		$this->data['order'] 		= $order;
		$this->data['channels'] 		= $this->cart->getChannelsList();
		$this->data['Tolocations'] 		= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['link_pdfexport'] 	= $this->url->link('transaction/transaction_reports/export_salessummary_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] 	= $this->url->link('transaction/transaction_reports/export_salessummary_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/sales_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_salessummary_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_channel'=>$filter_channel,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesListPdf($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=salessummary.csv');
	        print "S.No,Location Code,Invoice No,Invoice Date,Sub Total,Discount,GST,Net Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$j++;
						$location_code  = $result['location_code'];
						$invoice_no     = $result['invoice_no'];
						$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
						$sub_total  	= $result['sub_total'];
						$discount  		= $result['discount'];
						$gst       		= $result['gst'];
						$actual_total   = $result['actual_total'];
						$round_off      = $result['round_off'];
						$net_total		= $result['net_total'];
						$total+=$net_total;
		
				print "$j,\"$location_code\",\"$invoice_no\",\"$invoice_date\",\"$sub_total\",\"$discount\",\"$gst\",\"$net_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}  	 
		
	}


	public function export_salessummary_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_channel'=>$filter_channel,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesListPdf($data);

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);		
		if($filter_channel) {
			$filterStr.=" Sales Channel : ".$this->data['filter_channel_name'].',';
		}
		if($filter_location) {
			$filterStr.=" Location : ".$filter_location.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction no : ".$filter_transactionno.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Summary Reports</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<th>Location Code</th>
			<th>Invoice No</th>
			<th>Invoice Date</th>
			<th>Sub Total</th>
			<th>Discount</th>
			<th>GST</th>
			<th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$location_code  = $result['location_code'];
			$invoice_no     = $result['invoice_no'];
			$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
			$sub_total  	= $result['sub_total'];
			$discount  		= $result['discount'];
			$gst       		= $result['gst'];
			$actual_total   = $result['actual_total'];
			$round_off      = $result['round_off'];
			$net_total		= $result['net_total'];
			
			$total_Stock_Value+=$net_total;

			$str.="<tr><td>".$j."</td>
				   <td>".$location_code."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td class='right'>".$sub_total."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
		}
		$str.="<tr><td colspan='7' align='right'>Total</td><td align='right'>".$total_Stock_Value."</td>
		</tr></tbody></table>";
		if($str){
			$filename = 'SkuReport_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}


	public function salesquotationdetail() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printinvoice'){
			$this->document->setTitle('Sales Quotation Detail Print');
			$this->printquotation();
		} else {
			$this->document->setTitle('Sales Quotation Detail');
			$this->getsalesquotationdetailList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesquotationdetail&print=printquotation'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesquotationdetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesquotationdetailList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		
		$pageUrl ='';
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}

		if ($_REQUEST['filter_location']) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if ($_REQUEST['filter_channel']) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'      		   => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'      		   => $this->config->get('config_admin_limit'),
			'filter_supplier'	   => $filter_supplier,
			'filter_transaction'   => $filter_transactionno,
			'filter_channel'   	   => $filter_channel,
			'filter_from_date'	   => $filter_date_from,
			'filter_to_date'	   => $filter_date_to,
			'filter_location'	   => $filter_location,
			'filterVal' 		   => $filterVal
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSalesQuotation($data);
		$sale_total    = $sale_totalAry['totalsales'];
		$results = $this->model_transaction_sales->getSalesQuotationList($data);
		$this->load->model('user/user');

		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_salesquotationdetails_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_salesquotationdetails_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$salesAry = array();
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_sales->getsalesquotationDetailsbyInvoice($result['purchase_id']);
			$this->data['salesHeader'][] = array(
				"location_code"		=>$result['location_code'],
				"transaction_no"	=>$result['transaction_no'],
				"transaction_date"	=>date('d/m/Y',strtotime($result['transaction_date'])),
				"customer_code"		=>$result['customer_code'],
				"sub_total"			=>$result['sub_total'],
				"discount"			=>$result['discount'],
				"gst"				=>$result['gst'],
				"actual_total"		=>$result['actual_total'],
				"round_off"			=>$result['round_off'],
				"total"  			=>$result['total'],
				"cashier"			=>$result['cashier'],
				"terminal_code"		=>$result['terminal_code'],
				"header_remarks"	=>$result['header_remarks'],
				'salesDetails'		=>$itemDetails
			);			
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['filter_channel_name'] 	= $this->model_transaction_sales->getNetworkName($filter_channel);
		$this->data['Tolocations'] 	 		= $this->cart->getLocation();
		$this->data['channels'] 			= $this->cart->getChannelsList();
		$this->data['back']  				= $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salesquotationdetail', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->template = 'transaction/sales_quotation_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_salesquotationdetails_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from =  '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
		}
		$data = array(
			'filter_transactionno'  => $filter_transactionno,
			'filter_channel'  		=> $filter_channel,
			'filter_date_from'      => $filter_date_from,
			'filter_date_to'        => $filter_date_to,
			'filter_location'       => $filter_location,
			'filterVal'             => $filterVal
		);

		$salesHeader = $this->model_transaction_sales->getSalesQuotationListPdf($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$purchase_id = $value['purchase_id'];
				$invoice_no  = $value['transaction_no'];
				$results= $this->model_transaction_sales->getsalesquotationDetailsbyInvoice($value['purchase_id']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=SalesquotationDetailReport.csv');
		        print "\"Transaction No\",\"$invoice_no\"\r\n";
		        print "S.No,sku,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				foreach ($results as $result) {	
					$j++;
					$sku       		= $result['sku_name'];
					$description    = $result['name'];
					$qty    	    = $result['quantity'];
					$sku_price    	= $result['price'];
					$sub_total      = $result['raw_cost'];
					$discount    	= $result['discount_price'];
					$gst    		= $result['tax_price'];
					$actual_total   = $result['net_price'];
					$tot_subtotal  	= $value['sub_total'];
					$tot_gst  		= $value['gst'];
					$tot_discount  	= $value['discount'];
					$actualtotal  	= $value['total'];

					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
					print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}  
	}

	public function export_salesquotationdetails_pdf()
	{
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
		}
		$data = array(
			'filter_channel' 	  => $filter_channel,
			'filter_transactionno'=> $filter_transactionno,
			'filter_date_from'    => $filter_date_from,
			'filter_date_to'      => $filter_date_to,
			'filter_location'     => $filter_location,
			'filterVal'           => $filterVal
		);
		$salesHeader = $this->model_transaction_sales->getSalesQuotationListPdf($data);

	    if(count($salesHeader>=1)){
	    	if($filter_channel){
				$filter_channel_name = $this->model_transaction_sales->getNetworkName($filter_channel);
				$filterStr.=" Sales Channel : ".$filter_channel_name.',';
			}
			if($filter_location){
				$filterStr.=" Location : ".$filter_location.',';
			}
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			if($filter_transactionno){
				$filterStr.=" Transaction No: ".$filter_transactionno.',';
			}
			$company_id		 = $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
		
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Quotation Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($salesHeader as $key => $value) {
				$purchase_id = $value['purchase_id'];
				$transaction_no = $value['transaction_no'];
				$transaction_date = date('d/m/Y',strtotime($value['transaction_date']));
				$createdTime  = date('H:i',strtotime($result['createdon']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='5' class='left' style='border-right:none;'>Transaction No:".$transaction_no."	</th>
						<th colspan='4' align='right' style='border-left:none;'>Transaction Date:".$transaction_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_sales->getsalesquotationDetailsbyInvoice($purchase_id);
		    	
				$j=0; 
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['raw_cost'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['net_price'];
					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($value['sub_total'],2)."</td>
							<td class='right'>".number_format($value['discount'],2)."</td>
							<td class='right'>".number_format($value['gst'],2)."</td>
							<td class='right'>".number_format($value['total'],2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'sales_quotation_details_Report'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
		}  
	}

	public function salesinvoicedetail() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printinvoice'){
			$this->document->setTitle('Sales Invoice Detail Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Sales Invoice Details Report');
			$this->getsalesinvoicedetailList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesinoicedetail&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesinvoicedetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesinvoicedetailList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		
		$pageUrl ='';
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}

		if ($_REQUEST['filter_location']) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$filter_network = '';
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
			$pageUrl .= '&filter_network='.$filter_network; 
		}

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'      		=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=> $filter_supplier,
			'filter_transaction'=> $filter_transactionno,
			'filter_from_date'	=> $filter_date_from,
			'filter_to_date'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_network'	=> $filter_network,
			'filterVal' 		=> $filterVal
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSalesInvoiceForReports($data);
		$sale_total    = $sale_totalAry['totalsales'];
		$results = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);
		$this->load->model('user/user');
		$userDetail = array();
		$this->data['channels'] = $this->cart->getChannelsList();
		$gst = 0;

		$this->data['Tolocations'] 	  =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_salesinvoicedetails_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_salesinvoicedetails_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$salesAry = array();
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_sales->getsalesinvoiceDetailsbyInvoice($result['invoice_no']);
			$this->data['salesHeader'][] = array(
				"location_code"		=> $result['location_code'],
				"invoice_no"		=> $result['invoice_no'],
				"invoice_date"		=> date('d/m/Y',strtotime($result['invoice_date'])),
				"createdTime"		=>  date('H:i',strtotime($result['createdon'])),
				"customer_code"		=> $result['customer_code'],
				"sub_total"			=> $result['sub_total'],
				"discount"			=> $result['discount'],
				"gst"				=> $result['gst'],
				"actual_total"		=> $result['actual_total'],
				"round_off"			=> $result['round_off'],
				"net_total"			=> $result['net_total'],
				"cashier"			=> $result['cashier'],
				"terminal_code"		=> $result['terminal_code'],
				"header_remarks"	=> $result['header_remarks'],
				'salesDetails'		=> $itemDetails
			);			
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filterVal!='1'){
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salesinvoicedetail', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template 	= 'transaction/sales_invoice_detail_report.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_salesinvoicedetails_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
		}

		$data = array(
			'filter_transaction' 	=> $filter_transactionno,
			'filter_from_date' 		=> $filter_date_from,
			'filter_to_date'		=> $filter_date_to,
			'filter_location'		=> $filter_location,
			'filter_network'		=> $filter_network,
			'filterVal' 			=> $filterVal
		);
		$salesHeader = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$results= $this->model_transaction_sales->getsalesinvoiceDetailsbyInvoice($value['invoice_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=SalesInvioceDetailReport.csv');
		        print "\"$invoice_no\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku'];
					$description        = $result['description'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];

					  $actualtotal+=$result['net_total'];
	                  $tot_subtotal+=$result['sub_total'];
	                  $tot_gst+=$result['gst'];
	                  $tot_discount+=$result['discount'];

			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}  
	}

	public function export_salesinvoicedetails_pdf()
	{
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');

		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
		}

		$data = array(
			'filter_transaction' 	=> $filter_transactionno,
			'filter_from_date' 		=> $filter_date_from,
			'filter_to_date'		=> $filter_date_to,
			'filter_location'		=> $filter_location,
			'filter_network'		=> $filter_network,
			'filterVal' 			=> $filterVal
		);
		// printArray($data);
		$salesHeader = $this->model_transaction_sales->getSalesInvoiceHeaderForReports($data);
		// printArray($salesHeader); die;

	    if(count($salesHeader>=1)){
	    	if($filter_location){
				$filterStr.=" Location: ".$filter_location.',';
			}
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			if($filter_transactionno){
				$filterStr.=" Transaction No: ".$filter_transactionno.',';
			}
			if($filter_network){
                $filter_network_name = $this->model_transaction_sales->getNetworkName($data['filter_network']);
				$filterStr.=" Sales Channel : ".$filter_network_name;
			}

			$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
		
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Invoice Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$invoice_date = date('d/m/Y',strtotime($value['invoice_date']));
				$createdTime  = date('H:i',strtotime($result['createdon']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='5' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='4' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_sales->getsalesinvoiceDetailsbyInvoice($invoice_no);
		    	
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku'];
					$description        = $result['description'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];
					$tot_subtotal 		= $value['sub_total'];
					$tot_gst 			= $value['gst'];
					$tot_discount 		= $value['discount'];
					$actualtotal 		= $value['net_total'];
					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($actualtotal,2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'sales_invoice_details_Report'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
		}  
	}

	public function salesdetail() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Sales Detail Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Sales Order Details Report');
			$this->getsalesdetailList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesdetail&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesdetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesdetailList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		
		$pageUrl ='';
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}

		if ($_REQUEST['filter_location']) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if ($_REQUEST['filter_channel']) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filter_channel'=>$filter_channel,
			'filterVal' =>$filterVal
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSales($data);
		$sale_total    = $sale_totalAry['totalSale'];
		$results = $this->model_transaction_sales->getSalesList($data);
		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;

		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_salesdetails_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_salesdetails_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$salesAry = array();
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_sales->getsalesDetailsbyInvoice($result['invoice_no']);
			$this->data['salesHeader'][] = array(
				"location_code"		=>$result['location_code'],
				"invoice_no"		=>$result['invoice_no'],
				"invoice_date"		=>date('d/m/Y',strtotime($result['invoice_date'])),
				"createdTime"		=> date('H:i',strtotime($result['createdon'])),
				"customer_code"		=>$result['customer_code'],
				"sub_total"			=>$result['sub_total'],
				"discount"			=>$result['discount'],
				"gst"				=>$result['gst'],
				"actual_total"		=>$result['actual_total'],
				"round_off"			=>$result['round_off'],
				"total"				=>$result['net_total'],
				"cashier"			=>$result['cashier'],
				"terminal_code"		=>$result['terminal_code'],
				"header_remarks"	=>$result['header_remarks'],
				'salesDetails'		=>$itemDetails
			);			
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$this->data['filter_channel_name'] 	= $this->model_transaction_sales->getNetworkName($filter_channel);
		$this->data['Tolocations'] 			= $this->cart->getLocation($this->session->data['location_code']);

		$url.= $pageUrl;
		if($filterVal!='1'){
			$pagination 		= new Pagination();
			$pagination->total 	= $sale_total;
			$pagination->page 	= $page;
			$pagination->limit 	= $this->config->get('config_admin_limit');
			$pagination->text 	= $this->language->get('text_pagination');
			$pagination->url 	= $this->url->link('transaction/transaction_reports/salesdetail', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort']     = $sort;
		$this->data['order']    = $order;
		$this->data['channels'] = $this->cart->getChannelsList();
		$this->data['back']     = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/sales_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_salesdetails_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
		}

		$data = array(
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filter_channel'=>$filter_channel,
			'filterVal' =>$filterVal
		);

		$salesHeader = $this->model_transaction_sales->getSalesListPdf($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$results= $this->model_transaction_sales->getsalesDetailsbyInvoice($value['invoice_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=SalesDetailReport.csv');
		        print "\"$invoice_no\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Delivery Fee,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];
					$handling_fee	= 0.00;

					//   $actualtotal+=$result['net_total'];
	                //   $tot_subtotal+=$result['sub_total'];
	                //   $tot_gst+=$result['gst'];
	                //   $tot_discount+=$result['discount'];

					$actualtotal  = $value['net_total'];
					$tot_subtotal = $value['sub_total'];
					$tot_gst 	  = $value['gst'];
					$tot_discount = $value['discount'];
					$delivery_fee  = $value['handling_fee'];

					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$handling_fee\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$delivery_fee\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}  
	}

	public function export_salesdetails_pdf()
	{
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');

		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}

		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
		}
		$data = array(
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filter_channel'=>$filter_channel,
			'filterVal' =>$filterVal
		);

		$salesHeader = $this->model_transaction_sales->getSalesListPdf($data);
	    if(count($salesHeader>=1)){
			$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);
	    	if($filter_channel){
				$filterStr.=" Sales Channel : ".$this->data['filter_channel_name'].',';
			}
	    	if($filter_location){
				$filterStr.=" Location : ".$filter_location.',';
			}
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			if($filter_transactionno){
				$filterStr.=" Transaction No: ".$filter_transactionno.',';
			}
			$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
		
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$invoice_date = date('d/m/Y',strtotime($value['invoice_date']));
				$createdTime  = date('H:i',strtotime($result['createdon']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='5' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='4' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Delivery Fee</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_sales->getsalesDetailsbyInvoice($invoice_no);
		    	
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];

					//   $actualtotal+=$result['net_total'];
	                //   $tot_subtotal+=$result['sub_total'];
	                //   $tot_gst+=$result['gst'];
	                //   $tot_discount+=$result['discount'];

					$actualtotal   = $value['net_total'];
					$tot_subtotal  = $value['sub_total'];
					$tot_gst	   = $value['gst'];
					$tot_discount  = $value['discount'];
					$delivery_fee  = $value['handling_fee'];

					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>0.00</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($delivery_fee,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($actualtotal,2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'sales_details_Report'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
		}  
	}
	public function expensesreport() { 
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$this->document->setTitle('Expenses Report');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
	    $company_id	= $this->session->data['company_id']; 
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		//print_r($companyInfo);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		}

			
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/expensesexportcsv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_sku'=>$filter_sku,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
				
		);
		$totalAmount =0;
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getExpensesListDetailTotal($data);
		$sale_total    = $sale_totalAry['totalrecords'];
		$totalAmount   = $sale_totalAry['totalamount'];
		
		$results = $this->model_transaction_sales->getExpensesListDetail($data);
		
		$this->load->model('user/user');
		$userDetail = array();
	
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/expensesreport&print=printsummary'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/expensesreport&export=yes'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/expenses_pdfexport'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL');
		foreach ($results as $result) {	
			//$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
			//$productDetails = $this->model_transaction_sales->getDepartmentDetailsbySKU($result['sku']);
			$this->data['sales'][] = array(
				'payout_refno'           => $result['payout_refno'],
				'payout_location_code'   => $result['payout_location_code'],
				'payout_terminal_code'   => $result['payout_terminal_code'],
				'payout_date'            => $result['payout_date'],
				'payout_remarks'         => $result['payout_remarks'],
				'payout_amount'          => $result['payout_amount'],
				'payout_shift'		     => $result['payout_shift']
			);
		}	
		$this->data['totalAmount']	= number_format($totalAmount,2);
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/expensesreport', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/expenses_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());		
	}

	public function salessummarysku() { 
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
			
		if($print == 'printsummary'){
			$this->document->setTitle('Itemwise Sales Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Itemwise Sales');
			$this->getsalessummaryskuList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	public function export_credit_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		
		$this->data['heading_title'] = 'Credit Pending';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
		} 
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} 

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
	
		$results= $this->model_transaction_sales->getCreditPendingListAll($data);
		//printArray($results); exit;
	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=creditPending.csv');
	        print "S.No,Location Code,Customer,Invoice No,Invoice Date,Payment Type,Payment Amount,Remarks\r\n";
		
			$j=0; $total=0;
		foreach ($results as $result) {	

				$j++;  $total+=$result['payment_amount'];

				$location_code       = $result['location_code'];
				$customer_code       = $result['name'];
				$invoice_no    		 = $result['invoice_no'];
				$invoicedate         = $result['createdon'];
				$Payment_type     	 = $result['payment_type'];
				$payment_amount      = $result['payment_amount'];
				$createdby       	 = $result['createdby'];
				$remarks             = $result['Remarks'];
		
				print "$j,\"$location_code\",\"$customer_code\",\"$invoice_no\",\"$invoicedate\",\"$Payment_type\",\"$payment_amount\",\"$remarks\"\r\n";
		
		}
			print "\"\",\"\",\"\",\"\",\"\",Total,\"$total\",\"\",\"\"\r\n";
			
		}  

	}
	public function export_credit_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		$this->load->model('inventory/reports');
		

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
		} 
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} 

		


		$page = $_REQUEST['page'];
		$data = array(
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
	
		$results= $this->model_transaction_sales->getCreditPendingListAll($data);
		
	    if(count($results>=1)){
	    	if($filter_location){
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_customer){
			$filter_customer = $this->model_transaction_sales->getCustomernameByCode($filter_customer);
			$filterStr.=" Customer: ".$filter_customer.',';
		}
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

	   	    $company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Credit Pending Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
	             if($filterStr){
	        		$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
	        	 }
	        $str.="<table class='listing'><thead>
           		<tr><th>No</th>
	           		<th>Location</th>
	           		<th>Customer</th>
	           		<th>Invoice</th>
	           		<th>Invoice Date</th>
	           		<th>Payment_type</th>
	           		<th>Amount</th>
	           		<th>Remarks</th>
           		</tr></thead> <tbody>";  
		
			$j=0; $total=0;
			foreach ($results as $result) {	

				$j++;  $total+=$result['payment_amount'];

				$location_code       = $result['location_code'];
				$customer_code       = $result['name'];
				$invoice_no    		 = $result['invoice_no'];
				$invoicedate         = $result['createdon'];
				$Payment_type     	 = $result['payment_type'];
				$payment_amount      = $result['payment_amount'];
				$createdby       	 = $result['createdby'];
				$remarks             = $result['Remarks'];
		
				$str.="<tr><td>".$j."</td>
							<td>".$location_code."</td>
					   		<td>".$customer_code."</td>
					   		<td>".$invoice_no."</td>
					   		<td>".$invoicedate."</td>
					   		<td>".$Payment_type."</td>
					   	    <td align='right'>".$payment_amount."</td>
					   	    <td>".$remarks."</td>
				</tr>";	
			}
			$total = number_format($total,2);
			$str.="<tr><td colspan='6' align='right'>Total</td><td align='right'>".$total."</td></tr>
				</tbody></table>";
			if($str){
				$filename = 'creditPending'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
			
		}  

	}

	public function export_credit_summary_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		
		$this->data['heading_title'] = 'Credit Pending Summary';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');			
		}

		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if ($_REQUEST['filter_location']) {
			 $filter_location = $_REQUEST['filter_location']; 
		}else{
			 $filter_location = $this->session->data['location_code']; 
		} 
		if (isset($_REQUEST['filter_customer'])) {
			 $filter_customer = $_REQUEST['filter_customer']; 
		}
		
		
			$data = array(
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from' =>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
	
		$results= $this->model_transaction_sales->getcreditpendingsummaryListAll($data);
		//printArray($results); exit;
		$total = 0;
	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=creditPendingsummary.csv');
	        print "S.No,Location,Customer,Total Invoice No,Total Amount\r\n";
		
			$j=0; 
			foreach ($results as $result) {	
				$result['customer_code'] = $this->model_transaction_sales->getCustomernameByCode($result['customer_code']);
				$j++;  

				$location_code       = $result['location_code'];
				$customer_code       = $result['customer_code'];
				$total_invoice_no    = $result['total_invoice_count'];
				$payment_amount      = $result['payment_amount'];
				$total+=$payment_amount;
		
				print "$j,\"$location_code\",\"$customer_code\",\"$total_invoice_no\",\"$payment_amount\"\r\n";
		
			}
			  print "\"\",\"\",\"\",\"Total\",\"$total\"\r\n";

		}  

	}
	public function export_credit_summary_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		$this->load->model('inventory/reports');
		
		$this->data['heading_title'] = 'Credit Pending Summary';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');			
		}

		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if ($_REQUEST['filter_location']) {
			 $filter_location = $_REQUEST['filter_location']; 
		}else{
			 $filter_location = $this->session->data['location_code']; 
		} 
		if (isset($_REQUEST['filter_customer'])) {
			 $filter_customer = $_REQUEST['filter_customer']; 
		}
		
		
			$data = array(
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from' =>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		
		if($filter_location){
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_customer){
			$filter_customer = $this->model_transaction_sales->getCustomernameByCode($filter_customer);
			$filterStr.=" Customer: ".$filter_customer.',';
		}

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}

		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$results= $this->model_transaction_sales->getcreditpendingsummaryListAll($data);
		//printArray($results); exit;
		$total = 0;
		$j=0;
	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Credit Pending Summary Details</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
	             if($filterStr){
	        		$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
	        	 }
	        $str.="<table class='listing' style='width:100%;'><thead>
           			<tr>
	           			<th>No</th>
		           		<th>Location</th>
		           		<th>Customer</th>
		           		<th>Total Invoice</th>
		           		<th>Payment Amount</th>
           			</tr></thead> <tbody>";  
			foreach ($results as $result) {	
				$result['customer_code'] = $this->model_transaction_sales->getCustomernameByCode($result['customer_code']);
				$j++;  

				$location_code       = $result['location_code'];
				$customer_code       = $result['customer_code'];
				$total_invoice_no    = $result['total_invoice_count'];
				$payment_amount      = $result['payment_amount'];
				$total+=$payment_amount;
		
				$str.="<tr><td>".$j."</td>
							<td>".$location_code."</td>
					   		<td>".$customer_code."</td>
					   		<td>".$total_invoice_no."</td>
					   		<td align='right'>".$payment_amount."</td>  							   	    
						</tr>";	
		
			}
			  $str.="<tr><td align='right' colspan='4'>Total</td><td align='right'>".$total."</td></tr>
				</tbody></table>";
			if($str){
				$filename = 'creditPendingSummary'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}

		}  

	}
	public function export_cashflow_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
		}		
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}

		if (isset($_REQUEST['filter_teminal'])) {
			$filter_teminal = $_REQUEST['filter_teminal'];
		}
		if (isset($_REQUEST['short_by'])) {
			$short_by = $_REQUEST['short_by'];
		}
		if (isset($_REQUEST['short_by_order'])) {
			$short_by_order = $_REQUEST['short_by_order'];
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location'=>$filter_location,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_teminal'=>$filter_teminal,
			'short_by'=>$short_by,
			'short_by_order'=>$short_by_order
		);
		
		if($filter_location){
			$filterStr.=" Location: ".$filter_location.',';
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}

		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}

		if($filter_teminal){
			$filterStr.=" Terminal Code: ".$filter_teminal.',';
		}
		
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$results= $this->model_transaction_sales->getCashflowListAll($data);
		if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Cash Flow Summary Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
	             if($filterStr){
	        		$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
	        	 }

	       
	         $str.="<table class='listing'><thead>
           			<tr>
	           			<th>No</th>
		           		<th>Location</th>
		           		<th>Date</th>
		           		<th>Terminal</th>
		           		<th>Opening Cash</th>
		           		<th>Cash Sales</th>
		           		<th>Credit Collection</th>
		           		<th>Payin</th>
		           		<th>Payout</th>
		           		<th>Closing Cash</th>
           			</tr></thead> <tbody>"; 
			$j=0; //$total=0;
			foreach ($results as $result) {	

				$j++;  //$total+=$result['payment_amount'];
				$shift_location_code 		= $result['shift_location_code'];
				$collection_date    		= date('d/m/Y',strtotime($result['collection_date']));
				$opening_cash       		= $result['opening_cash'];
				$cash_collection    		= $result['cash_collection'];
				$credit_cash_collection     = $result['credit_cash_collection'];
				$cash_out_machine      		= $result['cash_out_machine'];
				$closing_cash       	 	= $result['closing_cash'];
				$credit_pending             = $result['credit_pending'];
				$terminal_code      		= $result['terminal_code'];
				$shift_refno       	 		= $result['shift_refno'];
				$cash_in_machine            = $result['cash_in_machine'];
			

				$str.="<tr><td>".$j."</td>
							<td>".$shift_location_code."</td>
					   		<td>".$collection_date."</td>
					   		<td>".$terminal_code."</td>
					   		<td align='right'>".$opening_cash."</td> 
					   		<td align='right'>".$cash_collection."</td>
					   		<td align='right'>".$credit_cash_collection."</td>
					   		<td align='right'>".$cash_in_machine."</td> 
					   		<td align='right'>".number_format($cash_out_machine,2)."</td> 
					   		<td align='right'>".number_format($closing_cash,2)."</td> 								   	    
						</tr>";
			}
			// $str.="<tr><td align='right' colspan='9'>Total</td><td align='right'></td></tr>";
			 $str.="</tbody></table>";
			if($str){
				$filename = 'cashflow'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
			
		}  
	}
	public function export_cashflow_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		
		$this->data['heading_title'] = 'CashFlow Summary';
		
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
		}		
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_teminal'])) {
			$filter_teminal = $_REQUEST['filter_teminal'];
		}
		if (isset($_REQUEST['short_by'])) {
			$short_by = $_REQUEST['short_by'];
		}
		if (isset($_REQUEST['short_by_order'])) {
			$short_by_order = $_REQUEST['short_by_order'];
		}
		

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location'=>$filter_location,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_teminal'=>$filter_teminal,
			'short_by'=>$short_by,
			'short_by_order'=>$short_by_order
		);
		$results= $this->model_transaction_sales->getCashflowListAll($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=cashflow_summary.csv');
	        print "S.No,Location,Date,Terminal,Opening Cash,Cash Sales,Credit Collection,Payin,Payout,Closing Cash\r\n";
		
			$j=0; //$total=0;
			foreach ($results as $result) {	

				$j++;  //$total+=$result['payment_amount'];
				$shift_location_code 		= $result['shift_location_code'];
				$collection_date    		= date('d/m/Y',strtotime($result['collection_date']));
				$opening_cash       		= $result['opening_cash'];
				$cash_collection    		= $result['cash_collection'];
				$credit_cash_collection     = $result['credit_cash_collection'];
				$cash_out_machine      		= $result['cash_out_machine'];
				$closing_cash       	 	= $result['closing_cash'];
				$credit_pending             = $result['credit_pending'];
				$terminal_code      		= $result['terminal_code'];
				$shift_refno       	 		= $result['shift_refno'];
				$cash_in_machine            = $result['cash_in_machine'];
		
				print "$j,\"$shift_location_code\",\"$collection_date\",\"$terminal_code\",\"$opening_cash\",\"$cash_collection\",\"$credit_cash_collection\",\"$cash_in_machine\",\"$cash_out_machine\",\"$closing_cash\"\r\n";
		
			} 	
		}  
	}

	public function exportcsv(){

		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$results =$this->getsalessummaryskuListcsv();

	    $i=0;
	    $PaidAmountTot = 0;
	    $BalanceAmountTot = 0;

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=SalesReport.csv');
	        print "S.No,LocationCode,Department,Category,Brand,Vendor,Sku,Name,Quantity,SkuPrice,Discount,NetTotal\r\n";
	        $this->load->model('user/user');
			
			$userDetail = array();
			$gst 	= 0;
			$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
			$j=0;
			$total = 0;
		foreach ($results as $result) {	
				$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
				$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);

				$sku_shortdescription = substr(preg_replace('/[^A-Za-z0-9\-]/','',$productDetails['sku_shortdescription']),20);
				$j++;
				$location_code      = $result['location_code'];
				$sku    		    = $result['sku'];
				$invoice_date       = '';
				$description      	= $productDetails['name'];
				$qty                = $result['qty'];
				$department         = $result['department_name'];
				$category           = $result['category_name'];
				$brand				= $result['sku_brand_code'];
				$vendor				= $vendor['vendor_name'];
				$sku_price          = $result['sku_price'];
				$sub_total          = $result['sub_total'];
				$discount           = $result['discount'];
				$gst                = $result['gst'];
				$actual_total       = $result['actual_total'];
				$round_off          = $result['round_off'];
				$net_total			= $result['net_total'];
				$total+=$net_total;
				print "$j,\"$location_code\",\"$department\",\"$category\",\"$brand\",\"$vendor\",\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$discount\",\"$net_total\"\r\n";		
		}
		print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\r\n"; 
		}  
	}
	public function fastlowqtyListcsv() {
	    $this->language->load('transaction/reports');
		$this->load->model('transaction/sales');

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		
		if (isset($_REQUEST['filter_quantity_start'])) {
			$filter_quantity_start = $_REQUEST['filter_quantity_start'];
			$pageUrl.= '&filter_quantity_start='.$filter_quantity_start;
		} else {
			$filter_quantity_start = 100;
		}
		if (isset($_REQUEST['filter_quantity_end'])) {
			$filter_quantity_end = $_REQUEST['filter_quantity_end'];
			$pageUrl.= '&filter_quantity_end='.$filter_quantity_end;
		} else {
			$filter_quantity_end = 150;
		}
		if (isset($_REQUEST['filter_fastlow'])) {
			$filter_fastlow = $_REQUEST['filter_fastlow'];
			$pageUrl.= '&filter_fastlow='.$filter_fastlow;
		} else {
			$filter_fastlow = '';
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.= '&filter_channel='.$filter_channel;
		} else {
			$filter_channel = '';
		}

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,			
			'filter_channel'=>$filter_channel,			
			'filter_quantity_start' => $filter_quantity_start,		
			'filter_quantity_end' => $filter_quantity_end,			
			'filter_fastlow' => $filter_fastlow			
		);
		$results = $this->model_transaction_sales->getFastMovingDetail($data);
	    $i=0;
	    $PaidAmountTot = 0;
	    $BalanceAmountTot = 0;

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=QuqntityReport.csv');
	        print "S.No,Location Code,Department,Sku,Description,Quantity\r\n";
	        $this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/fastsellinglowmoving&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/fastsellinglowmoving&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
			$j=0;
			$total = 0;
		foreach ($results as $result) {	
				$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
			//$userDetail = $this->model_user_user->getUser($result['createdby']);
			//$invoicedate = $this->model_transaction_sales->getInvoiceDateByNo($result['invoice_no']);
			//$productDetails = $this->model_transaction_sales->getDepartmentDetailsbySKU($result['sku']);
				
				$j++;
				$location_code      = $result['location_code'];
				$sku    		    = $result['sku'];
				$description      	= $result['sku_description'];
				$qty                = $result['sale_qty'];
				$department         =$result['department_name'];
				$category           =$result['category_name'];
				$brand				=$result['brand_name'];
				$sku_price          = $result['sku_price'];
				
				//print "$j,\"$location_code\",\"$department\",\"$category\",\"$brand\",\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$discount\",\"$net_total\"\r\n";		
				print "$j,\"$location_code\",\"$department\",\"$sku\",\"$description\",\"$qty\"\r\n";		
		}
		//print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\r\n";			
		} 			
	}
	public function expensesexportcsv(){

		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales'); 
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		
		
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location			
		);
		$results = $this->model_transaction_sales->getExpensesListDetail($data);
	    $i=0;
	    $PaidAmountTot = 0;
	    $BalanceAmountTot = 0;

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=ExpensesReport.csv');
	        print "S.No,Ref No,Location,Terminal,Date,Remarks,Shift,Amount\r\n";
	    		$j=0;
				$total = 0;
		foreach ($results as $result) {	
				
				$j++;
				$refno           = $result['payout_refno'];
				$location_code   = $result['payout_location_code'];
				$terminal_code   = $result['payout_terminal_code'];
				$date            = $result['payout_date'];
				$remarks         =$result['payout_remarks'];
				$shift			 =$result['payout_shift'];
				$amount          =$result['payout_amount'];
				$total+=$amount;
				
				print "$j,\"$refno\",\"$location_code\",\"$terminal_code\",\"$date\",\"$remarks\",\"$shift\",\"$amount\"\r\n";		
		}
		print "\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\r\n";
			
			
		}  
	}

	protected function getsalessummaryskuListcsv() {		
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$pageUrl.='&filter_product_id='.$filter_product_id; 
		} else {
			$filter_product_id = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}
		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_category='.$filter_brand;
		} else {
			$filter_brand = '';
		}
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_category='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.= '&filter_channel ='.$filter_channel;
		} else {
			$filter_channel = '';
		}
		
		$data = array(
			'filter_supplier'	=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_product_id'	=> $filter_product_id,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_category'	=> $filter_category,
			'filter_department'	=> $filter_department,
			'filter_location'	=> $filter_location,			
			'filter_channel'	=> $filter_channel,			
			'filter_brand'		=> $filter_brand,			
			'filter_vendor'		=> $filter_vendor			
		);
        // printArray($data); exit;
		//$results = $this->model_transaction_sales->getSalesListDetailReCSV($data);
		$results = $this->model_transaction_sales->getSalesListDetailRe($data);
		return $results['results'];		
	}

	protected function getsalessummaryskuList() {	
		//language assign code for transaction purchase
		$this->document->setTitle('Sales Summary Report By Sku');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning']= $this->error['warning'];
		} else {
			$this->data['error_warning']= '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id		= $this->session->data['company_id'];
		$stock_report 	= $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		} 
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} 
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		} 
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		} 
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		} 
		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		} 
        if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$pageUrl.= '&filter_product_id='.$filter_product_id;
		} else {
			$filter_product_id = '';
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.= '&filter_channel='.$filter_channel;
		} else {
			$filter_channel = '';
		}

		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/exportcsv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit'),
			'filter_supplier'	   => $filter_supplier,
			'filter_transactionno' => $filter_transactionno,
			'filter_product_id'	   => $filter_product_id,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_channel'	=> $filter_channel,
			'filter_department' => $filter_department,			
			'filter_category' 	=> $filter_category,			
			'filter_brand' 		=> $filter_brand,		
			'filter_vendor' 	=> $filter_vendor,			
			'filter_name' 		=> $filter_name			
		);
        // printArray($data);

		$this->data['data'] = $data;
		$data['group_by']   = 1;
		$totalCount 	= $this->model_transaction_sales->getSalesSummaryReportBySkuTotalCount($data);
		$response 		= $this->model_transaction_sales->getSalesListDetailRe($data);
        
        $results        = $response['results'];
        $sale_total     = count($response['total_rows']);
        $net_total      = 0;
        foreach($response['total_rows'] as $total_rows){
        	$net_total 	+= $total_rows['net_total'];
        }

		$this->load->model('user/user');
		$userDetail = array();
		$gst 		= 0;
		$strUrl 	= '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&print=printsummary'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummarysku&export=yes'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL'); 
		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/salessummarysku_pdfexport'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL'); 
        // $sale_total    	= $totalCoun['rows_total']; 

		foreach ($results as $result) {	
			$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
			$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);
			// $net_total += $result['net_total'];
			$this->data['sales'][] = array(
				'location_code'   => $result['location_code'],
				'sku'       	  => $result['sku'],
				'invoice_date'    => '',
				'description'     => $productDetails['sku_description'],
				'name'     		  => $productDetails['name'],
				'qty'             => $result['qty'],
				'department'      => $result['department_name'],
				'category'        => $result['category_name'],
				'brand'			  => $result['sku_brand_code'],
				'vendor'		  => $vendor['vendor_name'],
				'sku_price'       => $result['sku_price'],
				'sub_total'       => $result['sub_total'],
				'discount'        => $result['discount'],
				'gst'             => $result['gst'],
				'actual_total'    => $result['actual_total'],
				'round_off'       => $result['round_off'],
				'net_total'		  => $result['net_total']				
			);

		}
		$this->data['nettotal'] = number_format($net_total,2);
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'	=> $this->request->get['filter_transactionno'],
				'filter_sku'			=> $this->request->get['filter_sku'],
				'filter_date_from'		=> $this->request->get['filter_date_from'],
				'filter_date_to'		=> $this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesListDetail($data);

			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'location_code'      => $result['location_code'],
				'sku'       		 => $result['sku'],
				'invoice_date'       => $invoicedate,
				'description'        => $result['description'],
				'qty'                => $result['qty'],
				'sku_price'          => $result['sku_price'],
				'sub_total'          => $result['sub_total'],
				'discount'           => $result['discount'],
				'gst'                => $result['gst'],
				'actual_total'       => $result['actual_total'],
				'round_off'          => $result['round_off'],
				'net_total'			 => $result['net_total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				);
			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salessummarysku', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		// Departmnet
		$this->data['channels'] = $this->cart->getChannelsList();         
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		// Category
		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();

		$this->data['filter_department'] = $filter_department;
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->template = 'transaction/sales_sku_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function salessummarysku_pdfexport(){

		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department'); 
		$this->load->model('inventory/category');
		$this->load->model('master/vendor'); 

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];			
		}
		if (isset($this->request->get['filter_product_id'])) {
			$filter_product_id = $this->request->get['filter_product_id'];			
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];			
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '';
		}
		if (isset($this->request->get['filter_channel'])) {
			$filter_channel = $this->request->get['filter_channel'];
		} else {
			$filter_channel = '';
		}
		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} 
        if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		}
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_department){
		    $departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
		    $filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_category){
		    $categoryResult = $this->model_inventory_category->getCategoryNameByCode($filter_category);
		    $filterStr.=" Category: ".$categoryResult['category_name'].',';
		}
		if($filter_brand) {
			$filterStr.=" Brand: ".$filter_brand.',';
		}
		if($filter_vendor) {
			$vendorResult = $this->model_master_vendor->getSuppliersDetailsbyCode($filter_vendor);
			$filterStr.=" Vendor: ".$vendorResult['vendor_name'].',';
		}
		if($filter_name) {
			$filterStr.=" SKU: ".$filter_name.',';
		}
		/*if($filter_sku) {
			$filterStr.=" SKU: ".$filter_sku.',';
		}*/
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_channel) {
            $filter_network_name = $this->model_transaction_sales->getNetworkName($filter_channel);
			$filterStr.=" Sales Channel: ".$filter_network_name.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		
		$filterVal = 1;
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filter_channel'=>$filter_channel,
			'filterVal' =>$filterVal,
			'filter_product_id'=>$filter_product_id,
			'filter_category'=>$filter_category,
			'filter_department'=>$filter_department,
			'filter_brand'=>$filter_brand,
			'filter_vendor'=>$filter_vendor
		);
		$data['group_by'] = 1;
		$results = $this->model_transaction_sales->getSalesListDetailRe($data);
		$results = $results['results'];

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Summary Report By Sku</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location</th>
							<th>Department</th>
							<th>Category</th>
							<th>Brand</th>
							<th>Vendor</th>
							<th>Sku</th>
							<th>Name</th>
							<th>Quantity</th>
							<th>Sku Price</th>
							<th>Discount</th>
							<th>Net Total</th>
						</tr></thead> <tbody>"; 

	       
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
					    $productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
						$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);

						$location_code      = $result['location_code'];
						$sku    		    = $result['sku'];
						$invoice_date       = '';
						$description      	= $productDetails['name'];
						$qty                = $result['qty'];
						$department         = $result['department_name'];
						$category           = $result['category_name'];
						$brand				= $result['sku_brand_code'];
						$vendor				= $vendor['vendor_name'];
						$sku_price          = $result['sku_price'];
						$sub_total          = $result['sub_total'];
						$discount           = $result['discount'];
						$gst                = $result['gst'];
						$actual_total       = $result['actual_total'];
						$round_off          = $result['round_off'];
						$net_total			= $result['net_total'];
						$amtTotal+=$net_total;
				$str.="<tr>
				   <td>".$location_code."</td>
				   <td>".$department."</td>
				   <td>".$category."</td>
				   <td>".$brand."</td>
				   <td>".$vendor."</td>
				   <td>".$sku."</td>
				   <td>".$description."</td>
				   <td>".$qty."</td>
				   <td>".$sku_price."</td>
				   <td>".$discount."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
			}
			$str.="<tr><td colspan='10' align='right'>Total</td><td class='right'>".number_format($amtTotal,2)."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'itemwiseSales_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}
	public function fastlowqty_pdfexport(){

		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department'); 
		$this->load->model('inventory/category'); 
       
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];			
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
		} else {
			$filter_department = '';
		}
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
		} else {
			$filter_category = '';
		}
		if (isset($this->request->get['filter_quantity_start'])) {
			$filter_quantity_start = $this->request->get['filter_quantity_start'];	
		} else {
			$filter_quantity_start = 100;
		}
		if (isset($this->request->get['filter_quantity_end'])) {
			$filter_quantity_end = $this->request->get['filter_quantity_end'];	
		} else {
			$filter_quantity_end = 150;
		}
		if (isset($this->request->get['filter_fastlow'])) {
			$filter_fastlow = $this->request->get['filter_fastlow'];	
		} else {
			$filter_fastlow = '';
		}
		if (isset($this->request->get['filter_channel'])) {
			$filter_channel = $this->request->get['filter_channel'];	
		} else {
			$filter_channel = '';
		}
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		$filter_quantity=$filter_quantity_start.'-'.$filter_quantity_end;
		if($filter_sku) {
			$filterStr.=" SKU: ".$filter_sku.',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_channel) {
            $filter_network_name = $this->model_transaction_sales->getNetworkName($filter_channel);
			$filterStr.=" Sales Channel : ".$filter_network_name.',';
		}
		if($filter_quantity ){
			$filterStr.=" Filter Quantity: ".$filter_quantity.',';
		}
		if($filter_fastlow){
			$filterStr.=" Filter Movings : ".ucfirst($filter_fastlow).',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
	
		$filterVal = 1;
		$data = array(
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_channel'	=> $filter_channel,
			'filterVal'			=> $filterVal,
			'filter_quantity_start' => $filter_quantity_start,		
			'filter_quantity_end' 	=> $filter_quantity_end,			
			'filter_fastlow' 		=> $filter_fastlow	
		);
		$data['group_by'] = 1;
		$results = $this->model_transaction_sales->getFastMovingDetail($data);
	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Fast Selling and Low Moving Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location</th>
							<th>Department</th>
							<th>Sku</th>
							<th>Description</th>
							<th>Quantity</th>
						</tr></thead> <tbody>"; 
			foreach ($results as $result) {	
					   $productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);

						$location_code      = $result['location_code'];
						$sku    		    = $result['sku'];
						$description      	= $productDetails['name'];
						$qty                = $result['sale_qty'];
						$department         = $result['department_name'];
						$category           = $result['category_name'];
						$brand				= $result['brand_name'];
						$sku_price          = $result['sku_price'];
				$str.="<tr>
				   <td>".$location_code."</td>
				   <td>".$department."</td>
				   <td>".$sku."</td>
				   <td>".$description."</td>
				   <td>".$qty."</td>
			       </tr>";	
			}
			//$str.="<tr><td colspan='9' align='right'>Total</td><td class='right'>".number_format($amtTotal,2)."</td></tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'Quantity_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		} 
	}

	public function expenses_pdfexport(){

		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department'); 
		$this->load->model('inventory/category'); 


		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		
		$filterVal = 1;
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);
	
		$results = $this->model_transaction_sales->getExpensesListDetail($data);
	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Expenses Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Ref No</th>
							<th>Location</th>
							<th>Terminal</th>
							<th>Date</th>
							<th>Remarks</th>
							<th>Shift</th>
							<th>Amount</th>
						</tr></thead> <tbody>"; 

	       
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
					    //$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
						$refno           = $result['payout_refno'];
						$location_code   = $result['payout_location_code'];
						$terminal_code   = $result['payout_terminal_code'];
						$date            = $result['payout_date'];
						$remarks         =$result['payout_remarks'];
						$shift			 =$result['payout_shift'];
						$amount          =$result['payout_amount'];
						$amtTotal+=$amount;
				$str.="<tr>
				   <td>".$refno."</td>
				   <td>".$location_code."</td>
				   <td>".$terminal_code."</td>
				   <td>".$date."</td>
				   <td>".$remarks."</td>
				   <td>".$shift."</td>
				   <td align='right'>".$amount."</td>
				  
			</tr>";	
			}
			$str.="<tr><td colspan='6' align='right'>Total</td><td class='right'>".number_format($amtTotal,2)."</td></tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'Expenses_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}
	
	public function fastsellinglowmoving() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];

		$this->document->setTitle('Fast Selling and Low Moving');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}				

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		}

		if (isset($_REQUEST['filter_quantity_start'])) {
			$filter_quantity_start = $_REQUEST['filter_quantity_start'];
			$pageUrl.= '&filter_quantity_start='.$filter_quantity_start;
		} else {
			$filter_quantity_start = 100;
		}

		if (isset($_REQUEST['filter_quantity_end'])) {
			$filter_quantity_end = $_REQUEST['filter_quantity_end'];
			$pageUrl.= '&filter_quantity_end='.$filter_quantity_end;
		} else {
			$filter_quantity_end = 150;
		}

		if (isset($_REQUEST['filter_fastlow'])) {
			$filter_fastlow = $_REQUEST['filter_fastlow'];
			$pageUrl.= '&filter_fastlow='.$filter_fastlow;
		} else {
			$filter_fastlow = '';
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.= '&filter_channel='.$filter_channel;
		} else {
			$filter_channel = '';
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Fast Selling and Low Moving Report',
			'href'      => $this->url->link('transaction/transaction_reports/fastsellinglowmoving', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/fastlowqtyListcsv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit'),
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_channel'		=> $filter_channel,
			'filter_quantity_start' => $filter_quantity_start,		
			'filter_quantity_end' 	=> $filter_quantity_end,			
			'filter_fastlow' 		=> $filter_fastlow			
		);
		$this->data['data'] = $data;
		$data['group_by'] 	= 1;
	    $sale_total = $this->model_transaction_sales->getTotalcntFastMovingReport($data);		
		$results 	= $this->model_transaction_sales->getFastMovingDetail($data); //getSalesListDetailRe
		
		$this->load->model('user/user');
		$userDetail = array();
		$gst 	= 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/fastsellinglowmoving&print=printsummary'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/fastsellinglowmoving&export=yes'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL');
		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/fastlowqty_pdfexport'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL');

		foreach ($results as $result) {	
        	$this->data['sales'][] = array(
						'location_code'   => $result['location_code'],
						'sku'       	  => $result['sku'],
						'description'     => $result['name'],
						'qty'             => $result['sale_qty'],
						'department'      => $result['department_name'],
						'category'        => $result['category_name'],
						'brand'			  => $result['brand_name'],
						'sku_price'       => $result['sku_price']
					);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['filter_fastlow'])) {
			$url .= '&filter_fastlow=' . $this->request->get['filter_fastlow'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl; 
		$this->data['channels'] 			 = $this->cart->getChannelsList();         
		
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/fastsellinglowmoving', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		$this->load->model('inventory/category');			
		$this->data['category_collection'] 	 = $this->model_inventory_category->getCategorys();
		$this->data['filter_fastlow'] 		 = $filter_quantity;
		$this->data['filter_fastlow'] 		 = $filter_fastlow;
		$this->data['Tolocations'] 			 = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] 				 = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'transaction/fast_selling_low_moving_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());	
	}

	public function settlement() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Settlement Report Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Settlement Report');
			$this->getsalessettlementList();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/settlement&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/settlement&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}
	public function getsalessettlementList() {
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = '';
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		} else {
			$filter_location = '';
		}


		$this->data['settlementdetails'] = array();
		$allowResult = '';
		
		$locationDetails = $this->model_transaction_sales->getLocations();
		if(count($locationDetails)>=1)
			$this->data['location'] = $locationDetails;	
		
		if (isset($_REQUEST['filter_teminal']) && $_REQUEST['filter_teminal']!='0') {
			$filter_teminal = $_REQUEST['filter_teminal'];
			$allowResult = 1;
			$terminals = $this->model_transaction_sales->getTerminals($filter_date_from,$filter_location);
			if(count($terminals)>=1){
				$this->data['terminals'] = $terminals;
			}

			$shift_cashier = $this->model_transaction_sales->getCashierByTerminalLocation($filter_date_from,$filter_location,$_REQUEST['filter_teminal']);
			if(count($shift_cashier)>=1){
				$this->data['cashier'] = $shift_cashier;	
			}

			if(isset($_REQUEST['filter_shift'])){
				$filter_shift = $_REQUEST['filter_shift'];
				$shift = $this->model_transaction_sales->getShiftByTerminalLocation($filter_date_from,$filter_location,$filter_teminal);
				if(count($shift)>=1){
					$this->data['shift'] = $shift;
				}

			}
		}


		/*if($filter_date_from!='' && $filter_location!=''){
			$terminals = $this->model_transaction_sales->getTerminals($filter_date_from,$filter_location);
			if(count($terminals)>=1){
				$allowResult = 0;
				$this->data['terminalDropdown'] = 1;
				$this->data['terminals'] = $terminals;
			}else{
				$allowResult = 1;
				$this->data['terminalDropdown'] = 0;
			}
		}
		$filter_teminal = '';
		if (isset($_REQUEST['filter_teminal'])) {
			$filter_teminal = $_REQUEST['filter_teminal'];
			$allowResult = 1;
		}*/

		
		

		if($filter_date_from !='' && $filter_location!='' && $filter_teminal!='' && $filter_shift!=''){

			$settlementshiftsums   = $this->model_transaction_sales->settlementshiftsums($filter_date_from,$filter_teminal,$filter_location,$filter_shift);
			$settlementshiftdetails= $this->model_transaction_sales->settlementshiftdetails($filter_date_from,$filter_teminal,$filter_shift);
			$collectiondetails 	   = $this->model_transaction_sales->settlementcollectiondetails($filter_date_from,$settlementshiftdetails['shift_refno'],$filter_teminal);
			$paymentCreditdetails  = $this->model_transaction_sales->paymentCreditdetails($settlementshiftdetails['shift_refno']);
			$newPaymentDetails 	   = $this->model_transaction_sales->paymentdetailsSettlement($settlementshiftdetails['shift_refno']);

			if(count($newPaymentDetails)>=1){
				foreach ($newPaymentDetails as $amdetails) {
					if($amdetails['shift_paymode']=='CASH'){
							$paymentCashDetails = $this->splitamountDetails($amdetails);
					}
					if($amdetails['shift_paymode']=='NETS'){
							$paymentNetsDetails = $this->splitamountDetails($amdetails);
					}
					if($amdetails['shift_paymode']=='VISA'){
							$paymentVisaDetails = $this->splitamountDetails($amdetails);
					}
					if($amdetails['shift_paymode']=='CREDIT'){
							$paymentCreditDetails = $this->splitamountDetails($amdetails);
					}

					if($amdetails['shift_paymode']=='REDMART'){
							$paymentredMartDetails = $this->splitamountDetails($amdetails);
					}
					if($amdetails['shift_paymode']=='MASTER'){
							$paymentMasterDetails = $this->splitamountDetails($amdetails);
					}

					if($amdetails['shift_paymode']=='BANKTR'){
							$paymentBanktrDetails = $this->splitamountDetails($amdetails);
					}

					if($amdetails['shift_paymode']=='CHEQUE'){
							$paymentChequeDetails = $this->splitamountDetails($amdetails);
					}
				}
			}



			$banktr_amount = 0;
			$cashtr_amount = 0;
			$netstr_amount = 0;
			$ccardtr_amount = 0;
			$chequetr_amount = 0;
			$visatr_amount = 0;
			$ccTotal = 0;
			

			if(count($paymentCreditdetails)>=1){
				foreach ($paymentCreditdetails as $details) {
						if($details['shift_paymode']=='BANKTR'){
							$banktr_amount = $details['paymentamount'];
							$ccTotal+=$banktr_amount;
						}
						if($details['shift_paymode']=='CASH'){
							$cashtr_amount = $details['paymentamount'];
							$ccTotal+=$cashtr_amount;
						}
						if($details['shift_paymode']=='NETS'){
							$netstr_amount = $details['paymentamount'];
							$ccTotal+=$netstr_amount;
						}
						if($details['shift_paymode']=='CCARD'){
							$ccardtr_amount = $details['paymentamount'];
							$ccTotal+=$ccardtr_amount;
						}
						if($details['shift_paymode']=='CHEQUE'){
							$chequetr_amount = $details['paymentamount'];
							$ccTotal+=$chequetr_amount;
						}
						if($details['shift_paymode']=='VISA'){
							$visatr_amount = $details['paymentamount'];
							$ccTotal+=$visatr_amount;
						}
						
				}

			}

		


			if($settlementshiftsums['totalsales'] !='' || $settlementshiftsums['discount'] !='' || $settlementshiftsums['gst'] !=''){
				$round_off = $settlementshiftsums['totalsales'] -($settlementshiftsums['subtotalsales']- $settlementshiftsums['discount']);
				$this->data['settlementdetails'] = array(
					"shift_regno" => $settlementshiftdetails['shift_refno'],
					"shift_date" => $settlementshiftdetails['shift_date'],
					"shift_cashier" => $settlementshiftdetails['shift_cashier'],
					"shift_teminal" => $settlementshiftdetails['shift_teminal'],
				
					"subtotalsales" => number_format($settlementshiftsums['subtotalsales'],2),
					"discount"   => number_format($settlementshiftsums['discount'],2),
					"nettotalsales"   => number_format($settlementshiftsums['totalsales'],2),
					"gst"   => number_format($settlementshiftsums['gst'],2),
					"round_off"   => number_format($round_off,2),
					"netsales"   => $this->data['grosssales'] - $this->data['gst'],
					"plusgst"   => $this->data['netsales'] + $this->data['gst'],
					
					"cashpayment" => $paymentCashDetails['sys'],
					"cashpayment_mnl" => $paymentCashDetails['mnl'],
					"cashpayment_diff" => $paymentCashDetails['diff'],

					"netspayment" 	   => $paymentNetsDetails['sys'],
					"netspayment_mnl"  => $paymentNetsDetails['mnl'],
					"netspayment_diff" => $paymentNetsDetails['diff'],
					
					"visapayment" 	   => $paymentVisaDetails['sys'],
					"visapayment_mnl"  => $paymentVisaDetails['mnl'],
					"visapayment_diff" => $paymentVisaDetails['diff'],

					"creditpayment" 	 => $paymentCreditDetails['sys'],
					"creditpayment_mnl"  => $paymentCreditDetails['mnl'],
					"creditpayment_diff" => $paymentCreditDetails['diff'],


					"redMartpayment" 	  => $paymentredMartDetails['sys'],
					"redMartpayment_mnl"  => $paymentredMartDetails['mnl'],
					"redMartpayment_diff" => $paymentredMartDetails['diff'],

					"masterpayment"  	 => $paymentMasterDetails['sys'],
					"masterpayment_mnl"  => $paymentMasterDetails['mnl'],
					"masterpayment_diff" => $paymentMasterDetails['diff'],

					"bankpayment"  => $paymentBanktrDetails['sys'],
					"bankpayment_mnl"  => $paymentBanktrDetails['mnl'],
					"bankpayment_diff" => $paymentBanktrDetails['diff'],

					"chequepayment"  => $paymentChequeDetails['sys'],
					"chequepayment_mnl"  => $paymentChequeDetails['mnl'],
					"chequepayment_diff"  => $paymentChequeDetails['diff'],


					"banktr_amount" => $banktr_amount,
					"cashtr_amount" => $cashtr_amount,
					"netstr_amount" => $netstr_amount,
					"ccardtr_amount" => $ccardtr_amount,
					"chequetr_amount" => $chequetr_amount,
					"visatr_amount" => $visatr_amount,
					"creditsales_total" =>number_format($ccTotal,2),

					
					"opening_cash" => number_format($collectiondetails['opening_cash'],2),
					"cash_collection" => number_format($collectiondetails['cash_collection'],2),
					"credit_cash_collection" => number_format($collectiondetails['credit_cash_collection'],2),
					"cash_out_machine" => number_format($collectiondetails['cash_out_machine'],2),
					"closing_cash" => number_format($collectiondetails['closing_cash'],2),
					"credit_pending" => number_format($collectiondetails['credit_pending'],2),
					"cash_in_machine" => number_format($collectiondetails['cash_in_machine'],2)
				);
			}
		}

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->template = 'transaction/settlement_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function splitamountDetails($details){
		$res = array();
		$res['sys'] = $details['sys_paymentamount'];
		$res['mnl'] = $details['mnl_paymentamount'];
		$res['diff'] = $details['diff'];
		return $res;

	}
	public function AjaxloadTerminals(){
		$this->load->model('transaction/sales');
		$res = '';
		$location_code = $this->request->get['locationCode'];
		if($location_code){
			$terminals = $this->model_transaction_sales->getTerminalsByLocation($filter_date_from,$location_code);
			if(count($terminals)>=1){
				$res = $terminals;
			}
		}
		$this->response->setOutput(json_encode($res));
	}
    
    public function AjaxloadTerminalsCashier(){
		$this->load->model('transaction/sales');
		$res = '';
		$location_code = $this->request->get['locationCode'];
		$terminal_code = $this->request->get['terminal'];
		$filter_date_from   = $this->request->get['filter_date'];
		if($location_code){
			$terminals = $this->model_transaction_sales->getCashierByTerminalLocation($filter_date_from,$location_code,$terminal_code);
			if(count($terminals)>=1){
				$res = $terminals;
			}
		}
		$this->response->setOutput(json_encode($res));
	}

    public function AjaxloadTerminalsShift(){
		$this->load->model('transaction/sales');
		$res = '';
		$location_code = $this->request->get['locationCode'];
		$terminal_code = $this->request->get['terminal'];
		$filter_date_from   = $this->request->get['filter_date'];
		if($location_code){
			$terminals = $this->model_transaction_sales->getShiftByTerminalLocation($filter_date_from,$location_code,$terminal_code);
			if(count($terminals)>=1){
				$res = $terminals;
			}
		}
		$this->response->setOutput(json_encode($res));
	}


	public function salestotalsummary() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Total Sales Summary Report By Outlets Print');
			$this->printsummary();
		} else {
			$locationDetails = $this->model_transaction_sales->getLocationCode($this->session->data['company_id']);
			
			if($locationDetails[0]['type'] == 'Outlet'){
				$this->document->setTitle('Total Sales Summary Report By Outlets');
				$this->gettotalsalessummarybyoutletList($locationDetails[0]['location_code']);
			} else {
				$this->document->setTitle('Total Sales Summary Report By Outlets');
				$this->gettotalsalessummarybyhqList();
			}
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	public function creditcollectionsummary() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Total Sales Summary Report By Outlets Print');
			$this->printsummary();
		} else {
			$locationDetails = $this->model_transaction_sales->getLocationCode($this->session->data['company_id']);
			
			if($locationDetails[0]['type'] == 'Outlet'){
				$this->document->setTitle('Total Sales Summary Report By Outlets');
				$this->gettotalsalessummarybyoutletList($locationDetails[0]['location_code']);
			} else {
				$this->document->setTitle(' Total Credit collection Summary Report By Outlets');
				$this->gettotalcreditcollectionsummarybyhqList();
			}
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function gettotalsalessummarybyoutletList($location_code) {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => $location_code
		);

		//$sale_total = $this->model_transaction_sales->getTotalSalesListByDate($data);
		$sale_total = $this->model_transaction_sales->getTotalSalesListByDate($data);
		$results = $this->model_transaction_sales->getSalesListByDate($data);

		foreach ($results as $result) {	
			$strResults = $this->model_transaction_sales->getSalesListByCodeDate($result['location_code'],$result['invoice_date']);
			$strInvoiceNo = $this->model_transaction_sales->getSalesInvoiceByCodeDate($result['location_code'],$result['invoice_date']);
			$paymentDetail = $this->model_transaction_sales->getPaymentDetails($result['location_code'],$result['invoice_date']);
			$cash   = '';
			$nets   = '';
			$visa   = '';
			$master = '';
			$credit = '';
			foreach($paymentDetail as $payment){
				if($payment['payment_type'] == 'CASH'){
					$cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'VISA'){
					$visa.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'MASTER'){
					$master.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CREDIT'){
					$credit.= $payment['payment_amount'];
				}
			}

			$this->data['sales'][] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $strResults['actual_total'],
				'gst'  				=> $strResults['gst'],
				'total_sales'       => $strResults['net_total'],
				'cash'       		=> $cash,
				'nets'         		=> $nets,
				'visa'				=> $visa,
				'master'			=> $master,
				'credit'			=> $credit,
				'discrepencies'		=> '',
			);
		}
		$strUrl = '&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesList($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $result['actual_total'],
				'gst'  				=> $result['gst'],
				'total_sales'       => $result['net_total'],
				'cash'       		=> '',
				'nets'         		=> '',
				'visa'				=> '',
				'master'			=> '',
				'credit'			=> '',
				'discrepencies'		=> '',
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['createdon']
			);

			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filter_date_from =='' || $filter_date_to ==''){
			$pagination = new Pagination();
			$pagination->total = count($sale_total);
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_admin_limit');
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('transaction/transaction_reports/salestotalsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/sales_total_summary_report_by_outlet.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	protected function gettotalsalessummarybyhqList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		$filterVal = '';
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filter_date_from = null;
		}


		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno']) && $_REQUEST['filter_transactionno']!='') {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
			$filterVal = 1;
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location']) && $_REQUEST['filter_location']!='') {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
			$filterVal = 1;
		} else {
			$filter_location = null;
		}
		
	
		$url = '';
		if($_POST){
			$url = '';
		}else if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => $filter_location,
			'filterVal'     => $filterVal
		);
		$sale_total = $this->model_transaction_sales->getTotalSalesListByDate($data);


		$results = $this->model_transaction_sales->getSalesListByDate($data);

		if(count($results)==0){
			if($_REQUEST['filter_date_from']!='' && $_REQUEST['filter_date_to']!=''){
				if(strtotime($_REQUEST['filter_date_from'])>strtotime($_REQUEST['filter_date_to'])){
					$this->data['error_warning'] = "To date should be greater from date";
				}
			}
		}

		foreach ($results as $result) {	
			$strResults = $this->model_transaction_sales->getSalesListByCodeDate($result['location_code'],$result['invoice_date']);
			$strInvoiceNo = $this->model_transaction_sales->getSalesInvoiceByCodeDate($result['location_code'],$result['invoice_date']);
			$paymentDetail = $this->model_transaction_sales->getPaymentDetails($result['location_code'],$result['invoice_date']);
			$cash   = '';
			$nets   = '';
			$visa   = '';
			$master = '';
			$credit = '';
			$cheque = '';
			$banktr = '';
			$redmart = '';
			$voucher = '';


			foreach($paymentDetail as $payment){
				if($payment['payment_type'] == 'CASH'){
					$cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'VISA'){
					$visa.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'MASTER'){
					$master.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$cheque.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CREDIT'){
					$credit.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDMART'){
					$redmart.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VOUCHER'){
					$voucher.= $payment['payment_amount'];
				}
			}

			$this->data['sales'][] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $strResults['actual_total'],
				'gst'  				=> $strResults['gst'],
				'total_sales'       => $strResults['net_total'],
				'discount'          => $strResults['discount'],
				'round_off'         => $strResults['round_off'],
				'cash'       		=> $cash,
				'nets'         		=> $nets,
				'visa'				=> $visa,
				'master'			=> $master,
				'credit'			=> $credit,
				'cheque'			=> $cheque,
				'banktr'			=> $banktr,
				'redmart'			=> $redmart,
				'voucher'			=> $voucher,
				'discount'          => $strResults['discount'],
				'discrepencies'		=> '',
			);
		}
		
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesList($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $result['actual_total'],
				'gst'  				=> $result['gst'],
				'total_sales'       => $result['net_total'],
				'cash'       		=> '',
				'nets'         		=> '',
				'visa'				=> '',
				'master'			=> '',
				'credit'			=> '',
				'discrepencies'		=> '',
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['createdon']
			);

			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filterVal !='1'){
			$pagination = new Pagination();
			$pagination->total = count($sale_total);
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_admin_limit');
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('transaction/transaction_reports/salestotalsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['location'] = $this->model_transaction_sales->getLocation();

		$this->template = 'transaction/sales_total_summary_report_by_hq.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	protected function gettotalcreditcollectionsummarybyhqList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		$filterVal = '';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filter_date_from = null;
		}


		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
			$filterVal = 1;
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
			$filterVal = 1;
		} else {
			$filter_location = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => $filter_location,
			'filterVal' => $filterVal
		);
		$sale_total = $this->model_transaction_sales->getTotalCreditSalesListByDate($data);


		$results = $this->model_transaction_sales->getCreditSalesListByDate($data);
		
		if(count($results)==0){
			if($_REQUEST['filter_date_from']!='' && $_REQUEST['filter_date_to']!=''){
				if(strtotime($_REQUEST['filter_date_from'])>strtotime($_REQUEST['filter_date_to'])){
					$this->data['error_warning'] = "To date should be greater from date";
				}
			}
		}

		foreach ($results as $result) {	
			$strResults = $this->model_transaction_sales->getCCListByCodeDate($result['location_code'],$result['collection_date'],
				$result['terminal_code']);
			$strInvoiceNo = $this->model_transaction_sales->getCCInvoiceByCodeDate($result['location_code'],$result['collection_date'],
			   $result['terminal_code']);
			$paymentDetail = $this->model_transaction_sales->getCollectionPaymentDetails($result['location_code'],$result['collection_date'],
				$result['terminal_code']);
			$cash   = '';
			$nets   = '';
			$visa   = '';
			$master = '';
			$credit = '';
			$cheque = '';
			$banktr = '';
			$redmart = '';
			$voucher = '';
			foreach($paymentDetail as $payment){
				if($payment['payment_type'] == 'CASH'){
					$cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'VISA'){
					$visa.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'MASTER'){
					$master.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$cheque.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CREDIT'){
					$credit.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDMART'){
					$redmart.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VOUCHER'){
					$voucher.= $payment['payment_amount'];
				}
			}

			$this->data['sales'][] = array(
				'date'       		=> $result['collection_date'],
				'outlet_name'       => $result['location_code'],
				'terminal_code'       => $result['terminal_code'],
				'payment_amount'       => $strResults['payment_amount'],
				'cash'       		=> $cash,
				'nets'         		=> $nets,
				'visa'				=> $visa,
				'master'			=> $master,
				'credit'			=> $credit,
				'cheque'			=> $cheque,
				'banktr'			=> $banktr,
				'redmart'			=> $redmart,
				'voucher'			=> $voucher,
				'discrepencies'		=> '',
			);
		}
		
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesList($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $result['actual_total'],
				'gst'  				=> $result['gst'],
				'total_sales'       => $result['net_total'],
				'cash'       		=> '',
				'nets'         		=> '',
				'visa'				=> '',
				'master'			=> '',
				'credit'			=> '',
				'discrepencies'		=> '',
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => $result['createdon']
			);

			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filterVal != '1'){
			$pagination = new Pagination();
			$pagination->total = count($sale_total);
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_admin_limit');
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('transaction/transaction_reports/creditcollectionsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['location'] = $this->model_transaction_sales->getLocation();

		$this->template = 'transaction/sales_creditcollection_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function voidsalesdetails() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		$action = $this->request->get['action'];
		if($print == 'printsummary'){
			$this->document->setTitle('Void Sales Print');
			$this->printsummary();
		} else if($action == 'view'){
			$this->document->setTitle('Void Sales');
			$this->voidviewsalesdetails();
		} else {
			$this->document->setTitle('Void Sales');
			$this->getvoidsalesdetailList();
		}		
	}
	public function clearcartreport() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		$action = $this->request->get['action'];
		if($print == 'printsummary'){
			$this->document->setTitle('Clear Cart Detail Print');
			$this->printsummary();
		} else if($action == 'view'){
			$this->document->setTitle('Clear Cart Detail');
			$this->voidviewsalesdetails();
		} else {
			$this->document->setTitle('Clear Cart Detail');
			$this->getclearcartdetailList();
		}		
	}
	protected function getvoidsalesdetailList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		
		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);

       
		$this->data['data'] = $data;
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);

		$sale_totalAry = $this->model_transaction_sales->getTotalVoidSalesDetail($data);

		$sale_total = $sale_totalAry['totalSale']; 
		$this->data['overall_total'] = $sale_totalAry['net_total'];

		$results = $this->model_transaction_sales->getVoidSalesListDetail($data);
		//printArray($results); exit;
		$this->load->model('user/user');
		
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_voidsalesdetails_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_voidsalesdetails_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		

		foreach ($results as $result) {	
			
			$this->data['sales'][] = array(
				'location_code'   => $result['location_code'],
				'sku'         	  => $result['sku'],
				'invoice_no'      => $result['invoice_no'],
				'invoice_date'    => date('d/m/Y',strtotime($result['invoice_date'])),
				'cancel_date'     => $result['createdon'],
				'sub_total'       => $result['sub_total'],
				'discount'        => $result['discount'],
				'gst'             => $result['gst'],
				'actual_total'    => $result['actual_total'],
				'round_off'       => $result['round_off'],
				'net_total'		  => $result['net_total'],
				'modify_user'	  => $result['modifiedby'], 
				'cancel_time'	  => date('H:i:s',strtotime($result['createdon'])),
				'modify_date'	  => $result['modifiedon'],
				'action'            => $this->url->link('transaction/transaction_reports/voidsalesdetails&action=view&inv='.$result['invoice_no'], 'token=' . $this->session->data['token'] . $url, 'SSL')
				
			);
		}
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesListDetail($data);
			foreach ($results as $result) {			
				$userDetail = $this->model_user_user->getUser($result['created_by']);		
				$this->data['exportsales'][] = array(
				'location_code'       => $result['location_code'],
				'sku'         		  => $result['sku'],
				'invoice_no'          => $result['invoice_no'],
				'invoice_date'        => $invoicedate,
				'sub_total'           => $result['sub_total'],
				'discount'            => $result['discount'],
				'gst'                 => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['net_total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				
			);

			}
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/voidsalesdetails', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'transaction/voidsalesdetails_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	// public function expense_detailsreport() {

	// }
	protected function getclearcartdetailList() {

		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		if (isset($_REQUEST['filter_clear_type'])) {
			$filter_clear_type = $_REQUEST['filter_clear_type'];
			$pageUrl.='&filter_clear_type='.$filter_clear_type; 
		} else {
			$filter_clear_type = null;
		}
		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_clear_type'=>$filter_clear_type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);
		$this->data['data'] = $data;
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['clear_type'] =  $this->model_transaction_sales->clear_type();

		$this->data['clear_type'] = array_merge([array('void_type'=>'All')] , $this->data['clear_type']);
		$sale_totalAry = $this->model_transaction_sales->get_totalclearcartdetails($data);
		$recordsCnt    = $this->model_transaction_sales->get_clearcartdetailsTotal($data);
		$sale_total = $sale_totalAry['totalSale']; 

		$this->data['overall_total'] = $sale_total;

		$results = $this->model_transaction_sales->get_clearcartdetails($data);
		$this->load->model('user/user');

		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_clearecartdetails_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_clearcartdetails_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		foreach ($results as $result){	
			$this->data['sales'][] = array(
				'location_code'   => $result['location_code'],
				'invoice_date'    => date('d/m/Y',strtotime($result['invoice_date'])),
				'sku'         	  => $result['sku'],
				'description'     => $result['description'],
				'qty'    		  => $result['qty'],
				'sku_price'       => $result['sku_price'],
				'net_total'       => $result['net_total'],
				'void_type'       => $result['void_type'],
				'terminal_code'   => $result['terminal_code'],
				'createdby'       => $result['createdby'],
				'modify_date'	  => $result['modifiedon'],
				'remarks'    	  => $result['remarks']
				
			);
		}
		
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = $recordsCnt;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/voidsalesdetails', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/clearcartreport.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}

	protected function voidviewsalesdetails() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Viod Sales Details Report',
			'href'      => $this->url->link('transaction/transaction_reports/voidsalesdetails', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);	
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/transaction_reports/voidsalesdetails', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'invoice_no'=>$this->request->get['inv'],
		);
		
		$sale_total = $this->model_transaction_sales->getTotalVoidSalesListDetailByInvNo($data);
		$results = $this->model_transaction_sales->getVoidSalesListDetailByInvNo($data);
		
		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/voidsalesdetails&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/voidsalesdetails&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		foreach ($results as $result) {	
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			$invoicedate = $this->model_transaction_sales->getInvoiceVoidDateByNo($result['invoice_no']);
			
			$this->data['sales'][] = array(
				'sku'       			=> $result['sku'],
				'location_code'         => $result['location_code'],
				'invoice_no'       		=> $result['invoice_no'],
				'invoice_date'       	=> $invoicedate,
				'qty'  					=> $result['qty'],
				'sub_total'  			=> number_format($result['sub_total'],3),
				'discount'       		=> number_format($result['discount'],3),
				'gst'       			=> number_format($result['gst'],3),
				'actual_total'         	=> number_format($result['actual_total'],3),
				'round_off'				=> number_format($result['round_off'],3),
				'net_total'				=> number_format($result['net_total'],3),
				'created_by'        	=> $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      	=> $result['createdon']
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/voidsalesdetails&action=view&inv='.$this->request->get['inv'], 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('transaction/transaction_reports/voidsalesdetails', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/view_voidsalesdetails_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	// New report Total Sales and Credit collection Summary Report By Outlet
	public function salesSummaryOutletReport() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Total Sales and Credit collection Summary Report By Outlet');
			$this->printsummary();
		} else {
			$locationDetails =  $this->cart->getLocation($this->session->data['location_code']);
			if($locationDetails[0]['type'] == 'Outlet'){
				$this->document->setTitle('Total Sales and Credit collection Summary Report By Outlet');
				//$this->gettotalsalessummarybyoutletList($locationDetails[0]['location_code']);
				$this->gettotalSalesAndCreditcollectionsummarybyhqList();
			} else {
				$this->document->setTitle(' Total Sales and Credit collection Summary Report By Outlet');
				$this->gettotalSalesAndCreditcollectionsummarybyhqList();
			}
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salestotalsummary&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}
	protected function gettotalSalesAndCreditcollectionsummarybyhqList() {
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
	
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
		$this->data['companyInfo']	= $companyInfo;
		} else {
		$this->data['companyInfo']	='';
		}

		$pageUrl ='';
		$filterVal = '';

		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filterVal = 1;
		}


		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filterVal = 1;
		}		
		
		if (isset($_REQUEST['filter_transactionno']) && $_REQUEST['filter_transactionno']!='') {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
			$filterVal = 1;
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location']) && $_REQUEST['filter_location']!='') {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
			$filterVal = 1;
		} else {
			$filter_location = '';
		}
		
	
		$url = '';
		if($_POST){
			$url = '';
		}else if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => $filter_location,
			'filterVal'     => $filterVal
		);
		$sale_total = $this->model_transaction_sales->getTotalSalesListByDate($data);
		$results = $this->model_transaction_sales->getSalesListByDate($data);

		if(count($results)==0){
			if($_REQUEST['filter_date_from']!='' && $_REQUEST['filter_date_to']!=''){
				if(strtotime($_REQUEST['filter_date_from'])>strtotime($_REQUEST['filter_date_to'])){
					$this->data['error_warning'] = "To date should be greater from date";
				}
			}
		}

		foreach ($results as $result) {	
			$strResultsCC = array();
			$paymentDetailCredit = array();
			$strResults = $this->model_transaction_sales->getSalesListByCodeDate($result['location_code'],$result['invoice_date']);
			$strResultsCC = $this->model_transaction_sales->getCCListByCodeDateNew($result['location_code'],$result['invoice_date'],
			  	$result['terminal_code']);
			$strResultsCCTot = $this->model_transaction_sales->getCCListByCodeDateNewTotal($result['location_code'],$result['invoice_date'],
				  $result['terminal_code']);
			$strInvoiceNo = $this->model_transaction_sales->getSalesInvoiceByCodeDate($result['location_code'],$result['invoice_date']);
			$paymentDetail = $this->model_transaction_sales->getPaymentDetails($result['location_code'],$result['invoice_date']);
			
			$cash   = '';
			$nets   = '';
			$visa   = '';
			$credit = '';
			$cheque = '';
			$banktr = '';
			$redmart = '';
			$voucher = '';
			$foodpanda = '';
			$redeem = '';
			$master = '';
			$stripe = '';
			foreach($paymentDetail as $payment){
				if($payment['payment_type'] == 'CASH'){
					$cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'VISA'){
					$visa.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$cheque.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CREDIT'){
					$credit.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDMART'){
					$redmart.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VOUCHER'){
					$voucher.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'FOODPANDA'){
					$foodpanda.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDEEM'){
					$redeem.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'MASTER'){
					$master.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'STRIPE'){
					$stripe.= $payment['payment_amount'];
				}
			}

			$CC_cash   = '';
			$CC_nets   = '';
			$CC_cheque = '';
			$CC_banktr = '';
			$CC_visatr = '';
			$CC_cashb = '';

			foreach($strResultsCC as $payment){
				if($payment['payment_type'] == 'CASH'){
					$CC_cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$CC_nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$CC_cheque.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$CC_banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VISA'){
					$CC_visatr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'CASHB'){
					$CC_cashb.= $payment['payment_amount'];
				}
			}
			$this->data['sales'][] = array(
				'date'       		=> date('d/m/Y',strtotime($result['invoice_date'])),
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $strResults['actual_total'],
				'gst'  				=> $strResults['gst'],
				'total_sales'       => $strResults['net_total'],
				'discount'          => $strResults['discount'],
				'round_off'         => $strResults['round_off'],
				'cash'       		=> $cash,
				'nets'         		=> $nets,
				'visa'				=> $visa,
				'credit'			=> $credit,
				'cheque'			=> $cheque,
				'banktr'			=> $banktr,
				'redmart'			=> $redmart,
				'voucher'			=> $voucher,
				'foodpanda'			=> $foodpanda,
				'redeem'			=> $redeem,
				'master'			=> $master,
				'stripe'			=> $stripe,

				'CC_payment_amount'	=> $strResultsCCTot['payment_amount'],
				'CC_cash'			=> $CC_cash,
				'CC_nets'			=> $CC_nets,
				'CC_cheque'			=> $CC_cheque,
				'CC_banktr'			=> $CC_banktr,
				'CC_visatr'         => $CC_visatr,
				'CC_cashb'          => $CC_cashb
			);
		}
		
		if($this->request->get['export'] == 'yes'){
			$data = array(
				'filter_transactionno'=>$this->request->get['filter_transactionno'],
				'filter_date_from'=>$this->request->get['filter_date_from'],
				'filter_date_to'=>$this->request->get['filter_date_to']
			);
			$results = $this->model_transaction_sales->getSalesList($data);
				foreach ($results as $result) {			
					$userDetail = $this->model_user_user->getUser($result['created_by']);		
					$this->data['exportsales'][] = array(
					'date'       		=> date('d/m/Y',strtotime($result['invoice_date'])),
					'outlet_name'       => $result['location_code'],
					'before_gst'  		=> $result['actual_total'],
					'gst'  				=> $result['gst'],
					'total_sales'       => $result['net_total'],
					'cash'       		=> '',
					'nets'         		=> '',
					'visa'				=> '',
					'master'			=> '',
					'credit'			=> '',
					'discrepencies'		=> '',
					'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
					'created_date'      => $result['createdon']
				);
			}
			
			$this->model_transaction_sales->export_sales_summary_to_csv($this->data['exportsales']); exit;
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filterVal !='1'){
			$pagination = new Pagination();
			$pagination->total = count($sale_total);
			$pagination->page = $page;
			$pagination->limit = $this->config->get('config_admin_limit');
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('transaction/transaction_reports/salestotalsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
			$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_date_from'] = $filter_date_from;
		$this->data['filter_date_to'] 	= $filter_date_to;
		$this->data['filter_location'] 	= $filter_location;
		$this->data['token'] = $this->session->data['token'];
		$this->data['link_cspdfexport'] = $this->url->link('transaction/transaction_reports/salescreditcollectionsummary_export_pdf', 'token=' . $this->session->data['token'] . ''.$url, 'SSL');

		$this->template = 'transaction/sales_and_creditcollection_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function salescreditcollectionsummary_export_pdf(){
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');

		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
			$filterVal = 1;
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
			$filterVal = 1;
		}
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];	
			$filterVal = 1;		
		}
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$filterVal = 1;
		} 
		if (isset($_REQUEST['filter_location']) && $_REQUEST['filter_location']!='') {
			$filter_location = $_REQUEST['filter_location'];
			$filterVal = 1;
		}
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => $filter_location,
			'filterVal'     => $filterVal
		);
		$results = $this->model_transaction_sales->getSalesListByDate($data);
		
		foreach ($results as $result) {	
			$strResultsCC = array();
			$paymentDetailCredit= array();
			$strResults 		= $this->model_transaction_sales->getSalesListByCodeDate($result['location_code'],$result['invoice_date']);
			$strResultsCC 		= $this->model_transaction_sales->getCCListByCodeDateNew($result['location_code'],$result['invoice_date'],
			  	$result['terminal_code']);
			$strResultsCCTot 	= $this->model_transaction_sales->getCCListByCodeDateNewTotal($result['location_code'],$result['invoice_date'],$result['terminal_code']);
			$strInvoiceNo 		= $this->model_transaction_sales->getSalesInvoiceByCodeDate($result['location_code'],$result['invoice_date']);
			$paymentDetail 		= $this->model_transaction_sales->getPaymentDetails($result['location_code'],$result['invoice_date']);

			$cash   = '';
			$nets   = '';
			$visa   = '';
			$credit = '';
			$cheque = '';
			$banktr = '';
			$redmart = '';
			$voucher = '';
			$foodpanda = '';
			$redeem = '';
			$master = '';
			$stripe = '';
			foreach($paymentDetail as $payment){
				if($payment['payment_type'] == 'CASH'){
					$cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'VISA'){
					$visa.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$cheque.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CREDIT'){
					$credit.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDMART'){
					$redmart.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VOUCHER'){
					$voucher.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'FOODPANDA'){
					$foodpanda.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'REDEEM'){
					$redeem.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'MASTER'){
					$master.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'STRIPE'){
					$stripe.= $payment['payment_amount'];
				}
			}

			$CC_cash   = '';
			$CC_nets   = '';
			$CC_cheque = '';
			$CC_banktr = '';
			$CC_visatr = '';
			$CC_cashb = '';

			foreach($strResultsCC as $payment){
				if($payment['payment_type'] == 'CASH'){
					$CC_cash.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'NETS'){
					$CC_nets.= $payment['payment_amount'];
				} else if($payment['payment_type'] == 'CHEQUE'){
					$CC_cheque.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'BANKTR'){
					$CC_banktr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'VISA'){
					$CC_visatr.= $payment['payment_amount'];
				}else if($payment['payment_type'] == 'CASHB'){
					$CC_cashb.= $payment['payment_amount'];
				}
			}
			$Fsales[] = array(
				'date'       		=> $result['invoice_date'],
				'outlet_name'       => $result['location_code'],
				'before_gst'  		=> $strResults['actual_total'],
				'gst'  				=> $strResults['gst'],
				'total_sales'       => $strResults['net_total'],
				'discount'          => $strResults['discount'],
				'round_off'         => $strResults['round_off'],
				'cash'       		=> $cash,
				'nets'         		=> $nets,
				'visa'				=> $visa,
				'credit'			=> $credit,
				'cheque'			=> $cheque,
				'banktr'			=> $banktr,
				'redmart'			=> $redmart,
				'voucher'			=> $voucher,
				'foodpanda'			=> $foodpanda,
				'redeem'			=> $redeem,
				'master'			=> $master,
				'stripe'			=> $stripe,

				'CC_payment_amount'	=> $strResultsCCTot['payment_amount'],
				'CC_cash'			=> $CC_cash,
				'CC_nets'			=> $CC_nets,
				'CC_cheque'			=> $CC_cheque,
				'CC_banktr'			=> $CC_banktr,
				'CC_visatr'         => $CC_visatr,
				'CC_cashb'          => $CC_cashb
			);
		}

		if(count($Fsales>=1)){
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			
			if($filter_location) {
				$filterStr.=" Location : ".$filter_location.',';
			}

			if($filterStr){
				$filterStr = rtrim($filterStr,",");
			}

	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Total Sales Summary Report By Outlets
					</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
		    $str.="<table class='listing'><thead>
                     <tr>
                          <th>No</th>
                          <th>Date</th>
                          <th>Outlet Name</th>
                          <th>TSB-GST</th>
                          <th>GST</th>
                          <th>Discount</th>
                          <th>Round off</th>
                          <th>Total Sales</th>
                          <th>CASH</th>
                          <th>NETS</th>

                          <th>VISA</th>
                          <th>CHEQUE</th>
                          <th>CREDIT</th>
                          <th>BANKTR</th>
                          <th>REDMART</th>
                          <th>FOODPANDA</th>
                          <th>REDEEM</th>
                          <th>MASTER</th>
                          <th>STRIPE</th>
                      </tr>
                    </thead><tbody>";
            $j=0;
            $tot_before_gst=$tot_gst=$tot_total_sales=$tot_discount=$tot_round_off=$tot_cash=$tot_nets=$tot_visa=$tot_cheque=$tot_credit=$tot_banktr=$tot_redmart=$tot_voucher=0;
            $tot_CC_payment_amount=$tot_CC_cash=$tot_CC_nets=$tot_CC_cheque=$tot_CC_banktr=$tot_CC_visatr=$tot_CC_cashb=0;
            foreach ($Fsales as $result) {	
            			$j++;
						$date 			=  date('d/m/Y',strtotime($result['date']));
						$outlet_name    =  $result['outlet_name'];
						$before_gst		=  $result['before_gst'];
						$gst			=  $result['gst'];
						$total_sales	=  $result['total_sales'];
						$discount		=  $result['discount'];
						$round_off		=  $result['round_off'];
						$cash			=  $result['cash'];
						$nets			=  $result['nets'];

						$visa			=  $result['visa'];
						$master			=  $result['master'];
						$credit			=  $result['credit'];
						$cheque			=  $result['cheque'];
						$banktr			=  $result['banktr'];
						$redmart		=  $result['redmart'];
						$voucher		=  $result['voucher'];
						$foodpanda		=  $result['foodpanda'];
						$redeem		=  $result['redeem'];
						$master		=  $result['master'];
						$stripe		=  $result['stripe'];


						$CC_payment_amount	=  $result['CC_payment_amount'];
						$CC_cash			=  $result['CC_cash'];
						$CC_nets			=  $result['CC_nets'];
						$CC_cheque			=  $result['CC_cheque'];
						$CC_banktr			=  $result['CC_banktr'];
						$CC_visatr			=  $result['CC_visatr'];
						$CC_cashb			=  $result['CC_cashb'];

						$tot_before_gst+=$before_gst; 
						$tot_gst+=$gst;
						$tot_total_sales+=$total_sales;
						$tot_discount+=$discount;
						$tot_round_off+=$round_off;
						$tot_cash+=$cash;
						$tot_nets+=$nets;
						$tot_visa+=$visa;
						$tot_cheque+=$cheque;
						$tot_credit+=$credit;

						$tot_banktr+=$banktr; 
						$tot_redmart+=$redmart;
						$tot_voucher+=$voucher;
						$tot_foodpanda+=$foodpanda;
						$tot_redeem+=$redeem;
						$tot_master+=$master;
						$tot_stripe+=$stripe;
						$tot_CC_payment_amount+=$CC_payment_amount;
						$tot_CC_cash+=$CC_cash;
						$tot_CC_nets+=$CC_nets;
						$tot_CC_cheque+=$CC_cheque;
						$tot_CC_banktr+=$CC_banktr;
						$tot_CC_visatr+=$CC_visatr;
						$tot_CC_cashb+=$CC_cashb;

				$str.="<tr>
				   <td>".$j."</td>
				   <td>".$date."</td>
				   <td>".$outlet_name."</td>
				   <td class='right'>".$before_gst."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$round_off."</td>
				   <td class='right'>".$total_sales."</td>
				   <td class='right'>".$cash."</td>
				   <td class='right'>".$nets."</td>

				   <td class='right'>".$visa."</td>
				   <td class='right'>".$cheque."</td>
				   <td class='right'>".$credit."</td>
				   <td class='right'>".$banktr."</td>
				   <td class='right'>".$redmart."</td>
				   <td class='right'>".$foodpanda."</td>
				   <td class='right'>".$redeem."</td>
				   <td class='right'>".$master."</td>
				   <td class='right'>".$stripe."</td>

				</tr>";	
			}
			$str.="<tr><td colspan='3' align='right'>Total</td>
						<td class='right'>".number_format($tot_before_gst,2)."</td>
						<td class='right'>".number_format($tot_gst,2)."</td>
						<td class='right'>".number_format($tot_discount,2)."</td>
						<td class='right'>".number_format($tot_round_off,2)."</td>
						<td class='right'>".number_format($tot_total_sales,2)."</td>
						<td class='right'>".number_format($tot_cash,2)."</td>
						<td class='right'>".number_format($tot_nets,2)."</td>
						<td class='right'>".number_format($tot_visa,2)."</td>
						<td class='right'>".number_format($tot_cheque,2)."</td>
						<td class='right'>".number_format($tot_credit,2)."</td>

						<td class='right'>".number_format($tot_banktr,2)."</td>
						<td class='right'>".number_format($tot_redmart,2)."</td>
						<td class='right'>".number_format($tot_foodpanda,2)."</td>
						<td class='right'>".number_format($tot_redeem,2)."</td>
						<td class='right'>".number_format($tot_master,2)."</td>
						<td class='right'>".number_format($tot_stripe,2)."</td>

			</tr>";

			$str.="</tbody></table>";
			if($str){
					$filename = 'salescreditcollectionsummary_'.date('dmyHis').'.pdf';
					$landscape =1;
					include(DIR_SERVER.'pdf_print.php');
			}
		}	

	}
	public function pricetag() {
		
		$this->data['heading_title'] = 'Price Tag';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$filter = 0;
		
		
		$this->data['settlementdetails'] = array();
		$allowResult = '';

		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		// Category
		$this->load->model('inventory/category');
		$this->data['category_collection'] = $this->model_inventory_category->getCategorys();
		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$filter = 1;
		} 

		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$filter = 1;
		}

		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$filter = 1;
		}

		$this->data['filter_name'] = $filter_name;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_category'] = $filter_category;

		if($filter=='1'){
			$data = array(
						'filter_department'	  => $filter_department,
						'filter_category'	  => $filter_category,
						'filter_name'	  	  => $filter_name	
			);
			$idstring = 1;
			$idsList = $this->model_inventory_inventory->getProductsforreport($data,'',$idstring); 
			if(!isset($_SESSION['smtIdStr'])){
				$_SESSION['smtIdStr'] = $idsList;
			}else{
				$_SESSION['smtIdStr'].= $idsList;
			}
			$searchIds = $this->removeDuplication($_SESSION['smtIdStr']);
		}else{
			$_SESSION['smtIdStr'] ='';
			unset($_SESSION['smtIdStr']);
		}
		
		$idstring = 0;
		//echo $searchIds;
		$results = $this->model_inventory_inventory->getProductsforreportResult($searchIds); 
		  //$results = $this->model_inventory_inventory->getProductsforreport($data,'',$idstring);
		$this->data['results'] = $results;

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->template = 'transaction/pricetag_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	// credit pending
	public function creditpending() {

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		$this->load->model('inventory/reports');
		$this->document->setTitle('Credit Pending');
		$this->data['heading_title'] = 'Credit Pending';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Credit Pending Details',
			'href'      => $this->url->link('transaction/transaction_reports/creditpending', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		


		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}  else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} 

		if ($_REQUEST['filter_location']) {
			 $filter_location = $_REQUEST['filter_location']; 
			 $pageUrl.='&filter_location='.$filter_location;
		}else{
			 $filter_location = $this->session->data['location_code']; 
			 $pageUrl.='&filter_location='.$filter_location;	
		} 
		if (isset($_REQUEST['filter_customer'])) {
			 $filter_customer = $_REQUEST['filter_customer']; 
			 $pageUrl.='&filter_customer='.$filter_customer;
		}
		
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['creditpending'] = array();
		$page = $_REQUEST['page'];
		if(isset($_REQUEST['sort'])){
			$order=$_REQUEST['sort'];
		}else{
			$order="createdon";
		}

		if(isset($_REQUEST['order'])){
			$order=$_REQUEST['order'];

		}else{
			$order='DESC';
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;


		if($_POST){
			$page=1;
		}
		$data = array(
			'sort'	      =>$sort,
			'order'		  =>$order,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from' =>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);



		$this->data['location_list'] = $this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['customer'] = $this->model_transaction_sales->getCustomer();


	    $credit_totalAry = $this->model_transaction_sales->getCreditPendingCount($data);
	    $credit_total = $credit_totalAry['total_records'];
   		$this->data['creditpending_tot'] =  $credit_totalAry['payment_amount'];

		$this->data['creditpending'] = $this->model_transaction_sales->getCreditPendingList($data);
		$creditpending_tot = 0;
		
		$this->data['filter_location'] = $filter_location;
		$this->data['filter_customer'] = $filter_customer;
		$this->data['filter_date_from'] = $filter_date_from;
		$this->data['filter_date_to'] = $filter_date_to;
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_credit_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_credit_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

 		
		$pagination = new Pagination();
		$pagination->total = $credit_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/creditpending', 'token=' . $this->session->data['token'] . $pageUrl . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->template = 'transaction/creditpending_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	// Cash flow summary report
	public function cashflowsummary() {

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
	    
	    $this->document->setTitle('Cash Flow Summary Report');	

		$this->data['heading_title'] = 'Cash Flow Summary';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Cash Flow Summary',
			'href'      => $this->url->link('transaction/transaction_reports/cashflowsummary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		//printArray($_REQUEST); exit;
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl .= '&filter_date_from='.$filter_date_from; 
		}else{
			$filter_date_from = date('d/m/Y');
			$pageUrl .= '&filter_date_from='.$filter_date_from; 
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl .= '&filter_date_to='.$filter_date_to; 
		}else{
			$filter_date_to =date('d/m/Y');
			$pageUrl .= '&filter_date_to='.$filter_date_to; 
		}
		if (isset($_REQUEST['filter_location'])) {
			 $filter_location = $_REQUEST['filter_location']; 
			 $pageUrl.='&filter_location='.$filter_location;
		}else{
			 $filter_location = $this->session->data['location_code']; 
			 $pageUrl.='&filter_location='.$filter_location;	
		}
		/*if (isset($_REQUEST['filter_teminal']) && $_REQUEST['filter_teminal']!='0') {
			$filter_teminal = $_REQUEST['filter_teminal'];
		
		}*/ 
		if (isset($_REQUEST['filter_teminal'])) {
			 $filter_teminal = $_REQUEST['filter_teminal']; 
			 $pageUrl.='&filter_teminal='.$filter_teminal;
		}else{
			 $filter_teminal = $this->session->data['filter_teminal']; 
			 $pageUrl.='&filter_teminal='.$filter_teminal;	
		} 
		if (isset($_REQUEST['short_by'])) {
			 $short_by = $_REQUEST['short_by']; 
			 $pageUrl.='&short_by='.$short_by;
		}else{
			 $short_by = $this->session->data['short_by']; 
			 $pageUrl.='&short_by='.$short_by;	
		}
		if (isset($_REQUEST['short_by_order'])) {
			 $short_by_order = $_REQUEST['short_by_order']; 
			 $pageUrl.='&short_by_order='.$short_by_order;
		}else{
			 $short_by_order = $this->session->data['short_by_order']; 
			 $pageUrl.='&short_by_order='.$short_by_order;	
		} 
		
		
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['cashflow'] = array();
	
		$page = $_REQUEST['page'];
		if(isset($_REQUEST['sort'])){
			$order=$_REQUEST['sort'];
		}else{
			$order="createdon";
		}

		if(isset($_REQUEST['order'])){
			$order=$_REQUEST['order'];

		}else{
			$order='DESC';
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;


		if($_POST){
			$page=1;
		}
		$data = array(
			'sort'	      =>$sort,
			'order'		  =>$order,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_location'=>$filter_location,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_teminal'=>$filter_teminal,
			'short_by'=>$short_by,
			'short_by_order'=>$short_by_order
		);
        
		$this->data['data'] = $data;
		$this->data['location_list'] = $this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);

	
		$cashflow_totalAry = $this->model_transaction_sales->getCashflowCount($data);
		$cashflow_total = $cashflow_totalAry['total_records'];

		$this->data['cashflow'] = $this->model_transaction_sales->getCashflowList($data);
		//printArray($this->data['cashflow']);
		$this->data['filter_location'] = $filter_location;
		$this->data['filter_teminal'] = $filter_teminal;
		$this->data['filter_date_from'] = $filter_date_from;
		$this->data['filter_date_to'] = $filter_date_to;
		$this->data['short_by'] = $short_by;
		$this->data['short_by_order'] = $short_by_order;
			$terminals = $this->model_transaction_sales->getTerminals($filter_date_from,$filter_location);
			if(count($terminals)>=1){
				$this->data['terminals'] = $terminals;
			}
        //printArray($this->data['terminals']); exit;
		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location='.$filter_location; 
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			 $url .= '&filter_date_from='.$filter_date_from; 
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			 $url .= '&filter_date_to='.$filter_date_to; 
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			 $url .= '&filter_transactionno='.$filter_transactionno; 
		}
		
		$url.=$pageurl;

		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_cashflow_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_cashflow_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

 		
		$pagination = new Pagination();
		$pagination->total = $cashflow_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/cashflowsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$this->template = 'transaction/cashflow_summary.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function removeDuplication($sessionIds){
		if($sessionIds){
			$sessionIdsAry = explode(",", rtrim($sessionIds,','));
			$sessionIdsary = array_reverse(array_unique(array_values($sessionIdsAry)));
			$productsIdsStr = implode(",", $sessionIdsary);
			return $productsIdsStr;
		}
	}

	// DEPARTMENT SALES DETAILS REPORT
	public function departmentsalesdetail() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Sales Detail Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Department Sales Detail');
			$this->getsalesdetailList_department();
		}
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salesdetail&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesdetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}

	protected function getsalesdetailList_department() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Department Sales Detail';
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';

		if ($_REQUEST['filter_date_from']=='' && $_REQUEST['filter_date_to']=='') {
			$_REQUEST['filter_date_from'] = date('d/m/Y');
			$_REQUEST['filter_date_to'] = date('d/m/Y');
		}
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filter_date_to = null;
		}


		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location;			 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location;	
		}
          $this->load->model('inventory/department');
		if (isset($_REQUEST['filter_department'])) {
            $filter_department = $_REQUEST['filter_department'];
			$pageUrl.='&filter_department='. $filter_department; 	
			$filterVal = 1;		
		} else {
			$filter_department = null;
		}

		if (isset($_REQUEST['filter_category'])) {
            $filter_category = $_REQUEST['filter_category'];
            $pageUrl.='&filter_category='.$filter_category;     
            $filterVal = 1;        
        } else {
            $filter_category = null;
        }
        $filter_channel = '';
		if (isset($_REQUEST['filter_channel'])) {
            $filter_channel = $_REQUEST['filter_channel'];
            $pageUrl.='&filter_channel='.$filter_channel;
        }

       // printArray($filter_department['department_name']);
		//echo  $filter_department; exit;
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];

		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit'),
			'filter_supplier' 	=> $filter_supplier,
			'filter_transactionno'=> $filter_transactionno,
			'filter_date_from' 	=> $filter_date_from,
			'filter_date_to' 	=> $filter_date_to,
			'filter_location' 	=> $filter_location,
			'filter_channel' 	=> $filter_channel,
			'filterVal' 		=> $filterVal,
			'filter_department' => $filter_department,
			'filter_category'	=> $filter_category
		);
		$this->data['data'] = $data;

		$results = $this->model_transaction_sales->getTotalSalesListDetailDepartment($data);
		//printArray($results); exit;
		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_departmentsalesdetail_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_departmentsalesdetail_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

		foreach ($results as $result) {
            $this->data['sales'][] = array(
                'qty'                 => $result['total_qty'],
                'department'          => $result['department_name'],
                'department_code'     => $result['sku_department_code'],
                'category_name'       => $result['Category_Name'],
                'total_qty'           => $result['total_qty'],
                'net_total'           => $result['actual_total']            
            );
        }
		
		$this->load->model('inventory/department');
		$this->load->model('inventory/category');   
		$this->data['channels'] 			 = $this->cart->getChannelsList();         
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();	
		$this->data['Tolocations'] 			 = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] 				 = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['category_collection']   = $this->model_inventory_category->getCategorys();
		$this->template = 'transaction/sales_detail_report_by_department.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_voidsalesdetails_csv()
	{
		$this->load->model('transaction/sales');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];			
		}
		
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department
		);
          //printArray($data); exit;
		$results = $this->model_transaction_sales->getVoidSalesListDetail($data);
	
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=voidsalesdetail.csv');
	        print "Location, Invoice no, Invoice Date,Cancel Date,Cancel Time,Cashier,Sub Total,Discount,GST,Actual Total,Round Off,Net Total\r\n";
			$j=0; 
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
						$location_code   = $result['location_code'];
						$invoice_no      = $result['invoice_no'];
						$invoice_date    = date('d/m/Y',strtotime($result['invoice_date']));
						$cancel_date     = $result['createdon'];
						$sub_total  	 = $result['sub_total'];
						$discount  		 = $result['discount'];
						$gst       		 = $result['gst'];
						$actual_total      = $result['actual_total'];
						$round_off         = $result['round_off'];
						$net_total		   = $result['net_total'];
						$modify_user	  = $result['modifiedby'];
				        $cancel_time	  = date('H:i:s',strtotime($result['createdon']));
				        $modify_date	  = $result['modifiedon'];
						$amtTotal+=$net_total;
		
				print "\"$location_code\",\"$invoice_no\",\"$invoice_date\",\"$cancel_date\",\"$cancel_time\",\"$modify_user\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\",\"$round_off\",\"$net_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$amtTotal\"\r\n";
		}  
	}
	public function export_clearcartdetails_csv()
	{

		$this->load->model('transaction/sales');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_clear_type'])) {
			$filter_clear_type = $_REQUEST['filter_clear_type'];			
		}
		
		$data = array(
			'filter_clear_type'=>$filter_clear_type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);
          //printArray($data); exit;
		$results = $this->model_transaction_sales->get_clearcartdetails($data);
	
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=clearcartreport.csv');
	        print "Location,Clear Date,Clear Time,SKU,Description,Qty,SKU Price,Net Total,Clear type,Terminal,Cashier,Remarks\r\n";
			$j=0; 
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
						$location_code   = $result['location_code'];
						$invoice_date    = date('d/m/Y',strtotime($result['invoice_date']));
						$invoice_time    = date('H:i:s',strtotime($result['invoice_date']));
						$sku  			 = $result['sku'];
						$description  	 = $result['description'];
						$qty  	         = $result['qty'];
						$sku_price  	 = $result['sku_price'];
						$net_total  	 = $result['net_total'];
						$void_type  	 = $result['void_type'];
						$terminal_code   = $result['terminal_code'];
						$createdby  	 = $result['createdby'];
						$remarks  	     = $result['remarks'];

						$net_total		 = $result['net_total'];
						$amtTotal+=$net_total;
		
				print "\"$location_code\",\"$invoice_date\",\"$invoice_time \",\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$net_total\",\"$void_type\",\"$terminal_code\",\"$createdby\",\"$remarks\" \r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$amtTotal\"\r\n";
		}  
		
	}
	public function export_voidsalesdetails_pdf()
	{
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];			
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}

		if($filter_transactionno) {
			$filterStr.=" Invoice: ".$filter_transactionno.',';
		}

		
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department
		);

		$results = $this->model_transaction_sales->getVoidSalesListDetail($data);
	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Void Sales Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location Code</th>
							<th>Invoice No</th>
							<th>Invoice Date</th>
							<th>Cancel Date</th>
							<th>Cancel Time</th>
							<th>Cashier</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Actual Total</th>	
							<th>Round Off</th>	
							<th>Net Total</th>						
						</tr>
					</thead> <tbody>"; 
       
	       
			$amtTotal=0;
			foreach ($results as $result) {	
						$location_code   = $result['location_code'];
						$invoice_no      = $result['invoice_no'];
						$invoice_date    = date('d/m/Y',strtotime($result['invoice_date']));
						$cancel_date     = $result['createdon'];
						$sub_total  	 = $result['sub_total'];
						$discount  		 = $result['discount'];
						$gst       		 = $result['gst'];
						$actual_total      = $result['actual_total'];
						$round_off         = $result['round_off'];
						$net_total		   = $result['net_total'];
						$modify_user	  = $result['modifiedby'];
				        $cancel_time	  = date('H:i:s',strtotime($result['createdon']));
				        $modify_date	  = $result['modifiedon'];
						$amtTotal+=$net_total;
		
				$str.="<tr>
				   <td>".$location_code."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td>".$cancel_date."</td>
				   <td>".$cancel_time."</td>
				   <td>".$modify_user."</td>
				   <td class='right'>".$sub_total."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$actual_total."</td>
				   <td class='right'>".$round_off."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
			}
			$str.="<tr><td align='right' colspan='12'>Total</td><td class='right'>".$amtTotal."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'voidSales_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}
	public function export_clearecartdetails_pdf()
	{
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_clear_type'])) {
			$filter_clear_type = $_REQUEST['filter_clear_type'];			
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}

		if($filter_clear_type) {
			$filterStr.=" Invoice: ".$filter_clear_type.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_clear_type'=>$filter_clear_type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);

		$results = $this->model_transaction_sales->get_clearcartdetails($data);
					// foreach ($results as $result) {
					// 	$sdffds[] = $result['invoice_date']['date'];
					// }

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Clear Cart Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location Code</th>
							<th>Clear Date</th>
							<th>Clear Time</th>
							<th>SKU</th>
							<th>Description</th>
							<th>Qty</th>
							<th>SKU Price</th>
							<th>Net Total</th>
							<th>Clear type</th>
							<th>Terminal</th>	
							<th>Cashier</th>	
							<th>Remarks</th>	
						</tr>
					</thead> <tbody>"; 
			$amtTotal=0;
			foreach ($results as $result) {	
					$amtTotal += $result['net_total'];
				$str.="<tr>
				   <td>".$result['location_code']."</td>
				   <td>".date('d/m/Y',strtotime($result['invoice_date']))."</td>
				   <td>".date('H:i:s',strtotime($result['invoice_date']))."</td>
				   <td>".$result['sku']."</td>
				   <td>".$result['description']."</td>
				   <td>".$result['qty']."</td>
				   <td class='right'>".$result['sku_price']."</td>
				   <td class='right'>".$result['net_total']."</td>
				   <td>".$result['void_type']."</td>
				   <td>".$result['terminal_code']."</td>
				   <td>".$result['createdby']."</td>
				   <td>".$result['remarks']."</td>
			</tr>";	
			}
			$str.="<tr><td align='right' colspan='7'>Total</td><td class='right'>".number_format($amtTotal,3)."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'voidsaesDetails_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	
	}

	public function export_departmentsalesdetail_csv()
	{
		$this->load->model('transaction/sales');
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department =$this->request->get['filter_department'];			
		}
		if (isset($this->request->get['filter_category'])) {
            $filter_category = $this->request->get['filter_category'];    
        }
		if (isset($this->request->get['filter_channel'])) {
            $filter_channel = $this->request->get['filter_channel'];    
        }

		$data = array(
			'filter_supplier' 		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_location'		=> $filter_location,
			'filter_channel'		=> $filter_channel,
			'filterVal' 			=> $filterVal,
			'filter_department'		=> $filter_department,
			'filter_category'		=> $filter_category
		);

		$results = $this->model_transaction_sales->getTotalSalesListDetailDepartment($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=departmentsalesdetail.csv');
	        print "Department, Category, SUM OF QTY, Total\r\n";
			$j=0; 
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
						$department_name       	= $result['department_name'];
						$total_qty      		= $result['total_qty'];
						$actual_total       	= $result['actual_total'];
						$category_name          = $result['Category_Name'];
						$Qtytotal+=$total_qty;
						$amtTotal+=$actual_total;
		
				print "\"$department_name\",\"$category_name\",\"$total_qty\",\"$actual_total\"\r\n";
			}
			print "\"\",\"Total\",\"$Qtytotal\",\"$amtTotal\"\r\n";
		}  
	}

	public function export_departmentsalesdetail_pdf()
	{
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location_code = $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];			
		}
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];			
		}
		if (isset($this->request->get['filter_channel'])) {
			$filter_channel = $this->request->get['filter_channel'];			
		}

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($filter_department) {
			$departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
			$filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_channel) {
            $filter_network_name = $this->model_transaction_sales->getNetworkName($filter_channel);
			$filterStr.=" Sales Channel: ".$filter_network_name;
		}

		if($filter_category) {
			$filterStr.=" Category: ".$filter_category.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_supplier'	=> $filter_supplier,
			'filter_transactionno'=> $filter_transactionno,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_channel'	=> $filter_channel,
			'filterVal' 		=> $filterVal,
			'filter_department'	=> $filter_department,
			'filter_category'	=> $filter_category
		);

		$results = $this->model_transaction_sales->getTotalSalesListDetailDepartment($data);
	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Department Sales Summary Report Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Department</th>
							<th>Category</th>
							<th>SUM OF QTY</th>
							<th>Total</th>							
						</tr></thead> <tbody>"; 

	       
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
						$department_name       	= $result['department_name'];
						$total_qty      		= $result['total_qty'];
						$actual_total       	= $result['actual_total'];
						$category_name          = $result['Category_Name'];
						$Qtytotal+=$total_qty;
						$amtTotal+=$actual_total;
		
				$str.="<tr>
				   <td>".$department_name."</td>
				   <td>".$category_name."</td>
				   <td>".$total_qty."</td>
				   <td class='right'>".$actual_total."</td>
			</tr>";	
			}
			$str.="<tr><td align='right' colspan='2'>Total</td><td>".$Qtytotal."</td><td class='right'>".number_format($amtTotal,2)."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'departmentsalesSummary_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}

	// credit pending
	public function creditpendingsummary() {

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/stock_transfer_out');
		$this->load->model('inventory/reports');

		$this->document->setTitle('Credit Pending');
		
		$this->data['heading_title'] = 'Credit Pending';
		$this->document->setTitle('Credit Pending summary');
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Credit Pending Summary Report',
			'href'      => $this->url->link('transaction/transaction_reports/creditpendingsummary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		


		$company_id	= $this->session->data['company_id'];
	
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if ($_REQUEST['filter_location']) {
			 $filter_location = $_REQUEST['filter_location']; 
			 $pageUrl.='&filter_location='.$filter_location;
		}else{
			 $filter_location = $this->session->data['location_code']; 
			 $pageUrl.='&filter_location='.$filter_location;	
		} 
		if (isset($_REQUEST['filter_customer'])) {
			 $filter_customer = $_REQUEST['filter_customer']; 
			 $pageUrl.='&filter_customer='.$filter_customer;
		}

		
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['creditpendingsummary'] = array();
		$page = $_REQUEST['page'];
		if(isset($_REQUEST['sort'])){
			$order=$_REQUEST['sort'];
		}else{
			$order="createdon";
		}

		if(isset($_REQUEST['order'])){
			$order=$_REQUEST['order'];

		}else{
			$order='DESC';
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;


		if($_POST){
			$page=1;
		}
		$data = array(
			'sort'	      =>$sort,
			'order'		  =>$order,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_location'=>$filter_location,
			'filter_customer'=>$filter_customer,
			'filter_date_from' =>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);
		$this->data['data'] = $data;

		$this->data['location_list'] = $this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['customer'] = $this->model_transaction_sales->getCustomer();
		$credit_total = $this->model_transaction_sales->getcreditpendingsummaryCount($data);
		$results = $this->model_transaction_sales->getcreditpendingsummaryList($data);

		foreach ($results as $result) {
			$result['customer_code'] = $this->model_transaction_sales->getCustomernameByCode($result['customer_code']);
			$this->data['creditpendingsummary'][] = $result;
		}
		$creditpending_tot = 0;
		foreach ($this->data['creditpendingsummary'] as $result) {
			$creditpending_tot+=$result['payment_amount'];
		}
		$this->data['filter_location'] = $filter_location;
		$this->data['creditpending_tot'] = $credit_total['payment_amount'];
		$total_invoice_count 		     = $credit_total['total_invoice_count'];

		$url = '';
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			 $url .= '&filter_location='.$filter_location; 
		}
		
		$url.=$pageurl;

		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_credit_summary_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_credit_summary_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

 		
		$pagination = new Pagination();
		$pagination->total = $total_invoice_count;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/creditpendingsummary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] = $this->session->data['token'];
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}


		$this->template = 'transaction/creditpendingsummary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function wastageReport() {

		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		$this->document->setTitle('Wastage Report');

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if ($_REQUEST['filter_location_code']) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}


		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code,
		);
		$this->data['data'] = $data;
		//printArray($data);die;
		$this->load->model('transaction/purchase');
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$Wastageheader_totalAry  = $this->model_transaction_purchase->getWastageheaderListTotal($data);
		$Wastageheader_total = $Wastageheader_totalAry['total'];
		$this->data['Wastageheader_total_amount'] = $Wastageheader_totalAry['total_amount'];
		
		$results = $this->model_transaction_purchase->getWastageheaderList($data);

		foreach ($results as $key => $result) {
			$location_name = $this->model_transaction_purchase->getLocation($result['location_code']);
			$this->data['wastages'][] = array(
				'wastage_no'       	=> $result['wastage_no'],
				'wastage_date'      => date('d/m/Y',strtotime($result['wastage_date'])),
				'location_code'     => $location_name['location_name'],
				'terminal_code'     => $result['terminal_code'],
				'subtotal'       	=> $result['subtotal'],
				'gst'    			=> $result['gst'],
				'nettotal'    		=> $result['nettotal'],
			);
		}

		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		$pagination = new Pagination();
		$pagination->total = $Wastageheader_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports&type=purchase_summary', 'token=' . $this->session->data['token'] . $pageUrl . '&page={page}', 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/export_wastage_report_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/export_wastage_report_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/wastage_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_wastage_report_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Wastage Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code
		);

		$results= $this->model_transaction_purchase->getWastageheaderList($data);

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=WastageReport.csv');
	        print "S.No, Wastage No, Wastage Date,Location Code,Terminal Code,Subtotal,GST,Nettotal\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$location_name = $this->model_transaction_purchase->getLocation($result['location_code']);
						$j++;
						$wastage_no       	= $result['wastage_no'];
						$wastage_date       = date('d/m/Y',strtotime($result['wastage_date']));
						$location_code      = $location_name['location_name'];
						$terminal_code      = $result['terminal_code'];
						$subtotal    		= $result['subtotal'];
						$gst			    = $result['gst'];
						$nettotal    		= $result['nettotal'];
						$total+=$nettotal;
		
				print "$j,\"$wastage_no\",\"$wastage_date\",\"$location_code\",\"$terminal_code\",\"$subtotal\",\"$gst\",\"$nettotal\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}  
	}

	public function export_wastage_report_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Wastage Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		
		if($filter_location_code) {
			$filterStr.=" Location : ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}


		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code
		);

		$results= $this->model_transaction_purchase->getWastageheaderList($data);

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Wastage Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing'><thead>
						<tr>
							<th>S No</th>
							<th>Wastage No</th>
							<th>Wastage Date</th>
							<th>Location</th>
							<th>Terminal</th>
							<th>Sub Total</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead> <tbody>"; 

	       
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$location_name = $this->model_transaction_purchase->getLocation($result['location_code']);
						$j++;
						$wastage_no       	= $result['wastage_no'];
						$wastage_date       = date('d/m/Y',strtotime($result['wastage_date']));
						$location_code      = $location_name['location_name'];
						$terminal_code      = $result['terminal_code'];
						$subtotal    		= $result['subtotal'];
						$gst			    = $result['gst'];
						$nettotal    		= $result['nettotal'];
						$total+=$nettotal;
		
				$str.="<tr>
				   <td>".$j."</td>
				   <td>".$wastage_no."</td>
				   <td>".$wastage_date."</td>
				   <td>".$location_code."</td>
				   <td>".$terminal_code."</td>
				   <td class='right'>".$subtotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$nettotal."</td>
			</tr>";	
			}
			$str.="<tr><td colspan='7' align='right'>Total</td><td class='right'>".$total."</td>
		</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'wastage_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}

	public function wastageDetailsReport(){
		$this->document->setTitle('Wastage Details Report');	
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];

		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}



		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code
		);

		$this->data['data'] = $data;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$WastageheaderList = $this->model_transaction_purchase->getWastageheaderList($data);
		$list_total = count($WastageheaderList);

		foreach ($WastageheaderList as $key => $value) {

			$this->data['wastages'][] = array(
				'wastage_no'   => $value['wastage_no'],
				'wastage_date' => date('d/m/Y',strtotime($value['wastage_date'])),
			);
              
			$wastage_no = $value['wastage_no']; 
			

			$results = $this->model_transaction_purchase->getWastagedetailsList($data,$wastage_no);
			// printArray($results);

			foreach ($results as $keys => $result) {
				$this->data['wastages'][$key]['wastages_details'][] = array(
					'wastage_no'   => $result['wastage_no'], 
					'sku_description'   => $result['sku_description'], 
					'current_stock'    	=> $result['current_stock'],
					'new_stock'    		=> $result['new_stock'],
					'stock_difference'  => $result['stock_difference'],
					'sku_cost'    		=> $result['sku_cost'],
					'subtotal'       	=> $result['subtotal'],
					'gst'    			=> $result['gst'],
					'nettotal'    		=> $result['nettotal'],
					'actualtotal'       => $result['actualtotal'],
				);
			}
		}
		// printArray($this->data);
		// die;

		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}

		$pagination = new Pagination();
		$pagination->total = $list_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/wastageDetailsReport', 'token=' . $this->session->data['token'] . $pageUrl . '&page={page}', 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/export_wastage_details_report_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/export_wastage_details_report_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		

		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/wastage_details_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_wastage_details_report_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Wastage Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		$page = $_REQUEST['page'];
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code
		);


		$WastageheaderList = $this->model_transaction_purchase->getWastageheaderList($data);

	    if(count($WastageheaderList>=1)){
			foreach ($WastageheaderList as $key => $value) {
				$wastage_no = $value['wastage_no'];
				$results= $this->model_transaction_purchase->getWastagedetailsList($data,$wastage_no);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=WastageDetailReport.csv');
		        print "\"$wastage_no\"\r\n";
		        print "S.No,Item,current stock,New stock,Stock Difference,Sku Cost,Sub Total,GST,Net Total,Actual Total\r\n";
				$j=0; 
				$total=0;
				foreach ($results as $result) {	
					$location_name = $this->model_transaction_purchase->getLocation($result['location_code']);
					$j++;
					$total+=$result['actualtotal'];

					$wastage_no       	= $result['wastage_no'];
					$sku_name           = $result['sku_description'];
					$current_stock    	= $result['current_stock'];
					$new_stock    		= $result['new_stock'];
					$stock_difference   = $result['stock_difference'];
					$sku_cost    		= $result['sku_cost'];
					$subtotal       	= $result['subtotal'];
					$gst    			= $result['gst'];
					$nettotal    		= $result['nettotal'];
					$actualtotal        = $result['actualtotal'];
			
					print "$j,\"$sku_name\",\"$current_stock\",\"$new_stock\",\"$stock_difference\",\"$sku_cost\",\"$subtotal\",\"$gst\",\"$nettotal\",\"$actualtotal\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\r\n";
			}
		}  
	}

	public function export_wastage_details_report_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('transaction/purchase');
		$this->load->model('inventory/reports');

		
		$this->data['heading_title'] = 'Wastage Report';
		$url = '';		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		$page = $_REQUEST['page'];
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location_code
		);


		$WastageheaderList = $this->model_transaction_purchase->getWastageheaderList($data);	



	    if(count($WastageheaderList>=1)){
	    	if($filter_location_code){
				$filterStr.=" Location: ".$filter_location_code.',';
			}

			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
		
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Wastage Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($WastageheaderList as $key => $value) {
				$wastage_no = $value['wastage_no'];
				$wastage_date = date('d/m/Y',strtotime($value['wastage_date']));
				$str.="<table class='listing'><thead>
						<tr><th colspan='5' class='left'>Wastage No:".$wastage_no."	</th><th colspan='4' align='right'>Wastage Date:".$wastage_date."</th></tr>
						<tr>
							<th>Item</th>
							<th>Current Stock</th>
							<th>New Stock</th>
							<th>Stock Differenc</th>
							<th>Sku Cost</th>
							<th>Sub Total</th>
							<th>GST</th>
							<th>Net Total</th>
							<th>Actual Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_purchase->getWastagedetailsList($data,$wastage_no);
		    	
				$j=0; 
				$total=0;
				foreach ($results as $result) {	
					$location_name = $this->model_transaction_purchase->getLocation($result['location_code']);
					$j++;
					$total+=$result['actualtotal'];

					$wastage_no       	= $result['wastage_no'];
					$sku_name           = $result['sku_description'];
					$current_stock    	= $result['current_stock'];
					$new_stock    		= $result['new_stock'];
					$stock_difference   = $result['stock_difference'];
					$sku_cost    		= $result['sku_cost'];
					$subtotal       	= $result['subtotal'];
					$gst    			= $result['gst'];
					$nettotal    		= $result['nettotal'];
					$actualtotal        = $result['actualtotal'];
					
					$str.="<tr>
							   <td>".$sku_name."</td>
							   <td>".$current_stock."</td>
							   <td>".$new_stock."</td>
							   <td>".$stock_difference."</td>
							   <td class='right'>".$sku_cost."</td>
							   <td class='right'>".$subtotal."</td>
							   <td class='right'>".$gst."</td>
							   <td class='right'>".$nettotal."</td>
							   <td class='right'>".$actualtotal."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='8' align='right'>Total</td><td class='right'>".$total."</td>
				</tr><tr><td colspan='9' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'wastage_details_Report'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
		}  
	} 
	// Movement Report start
	public function movementReport() {
		$this->document->setTitle('SKU Movement Report'); 
		$this->load->model('inventory/reports');
		
        $this->data['column_action']  = $this->language->get('column_action');
		$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt']   = $this->language->get('text_tran_dt');
		$this->data['text_supplier']  = $this->language->get('text_supplier');
		$this->data['text_total']     = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back']    = $this->language->get('button_back');
		$this->data['token']          = $this->session->data['token'];

		$company_id	     = $this->session->data['company_id'];
		$stock_report    = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	 = $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$url='&';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];	
		} else {
			$filter_date_from = date('d/m/Y');
		}
		$url.= '&filter_date_from=' . $filter_date_from;		
		
        if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}
		$url.= '&filter_date_to='.$filter_date_to;

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location =  $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url.= '&filter_product='.urlencode($filter_product);
		} else {
			$filter_product = '';
		}
        if (isset($_REQUEST['filter_product_id'])) {
            $filter_product_id = $_REQUEST['filter_product_id'];
            $url.= '&filter_product_id='.urlencode($filter_product_id);
        } else {
            $filter_product_id = '';
        }
        $token = $this->session->data['token'];
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		); 
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
        $this->data['breadcrumbs'][] = array(
            'text'      => 'Inventory Movement Report',
            'href'      => $this->url->link('transaction/transaction_reports/movementReport', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' <i class="fa fa-angle-right"></i> '
        ); 
		$data = array(
			'filter_date_from' => $filter_date_from,
			'filter_date_to'   => $filter_date_to,
			'filter_location'  => $filter_location,
            'filter_product'   => $filter_product,
			'filter_product_id'=> $filter_product_id
		);

        $this->data['filters'] = $data;
        $sales          = $this->model_inventory_reports->getInventoryMovementForSales($data);
        $salesChild     = $this->model_inventory_reports->getInventoryMovementForSalesChild($data);
        $purchase       = $this->model_inventory_reports->getInventoryMovementForPurchase($data);
        $purchaseChild  = $this->model_inventory_reports->getInventoryMovementForPurchaseChild($data);
        $stock          = $this->model_inventory_reports->getInventoryMovementForStock($data);
        $this->data['allDatas'] = array_merge($purchase, $stock, $sales, $salesChild, $purchaseChild);

		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back']        = $this->url->link('report/report', 'token=' . $token, 'SSL');
		$this->data['action']      = $this->url->link('transaction/transaction_reports/movementReport', 'token='.$token,'SSL');
		$this->data['export_csv']  = $this->url->link('transaction/transaction_reports/movementReport_csv','token='.$token.$url,'SSL');
		$this->data['export_pdf']  = $this->url->link('transaction/transaction_reports/movementReport_pdf','token='.$token.$url,'SSL');

		$this->template = 'transaction/movement_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function movementReport_csv()
	{
		$this->load->model('inventory/reports'); 	
        if (isset($_REQUEST['filter_date_from'])) {
            $filter_date_from = $_REQUEST['filter_date_from'];  
        } else {
            $filter_date_from = date('d/m/Y');
        }
        
        if (isset($_REQUEST['filter_date_to'])) {
            $filter_date_to = $_REQUEST['filter_date_to'];
        } else {
            $filter_date_to = date('d/m/Y');
        }

        if (isset($_REQUEST['filter_location'])) {
            $filter_location = $_REQUEST['filter_location'];
        } else {
            $filter_location =  $this->session->data['location_code'];
        }
        if (isset($_REQUEST['filter_product'])) {
            $filter_product = $_REQUEST['filter_product'];
        } else {
            $filter_product = '';
        }
        if (isset($_REQUEST['filter_product_id'])) {
            $filter_product_id = $_REQUEST['filter_product_id'];
        } else {
            $filter_product_id = '';
        }

        $data = array(
            'filter_date_from' => $filter_date_from,
            'filter_date_to'   => $filter_date_to,
            'filter_location'  => $filter_location,
            'filter_product'   => $filter_product,
            'filter_product_id'=> $filter_product_id
        );

		$sales          = $this->model_inventory_reports->getInventoryMovementForSales($data);
        $salesChild     = $this->model_inventory_reports->getInventoryMovementForSalesChild($data);
        $purchase       = $this->model_inventory_reports->getInventoryMovementForPurchase($data);
        $purchaseChild  = $this->model_inventory_reports->getInventoryMovementForPurchaseChild($data);
        $stock          = $this->model_inventory_reports->getInventoryMovementForStock($data);
        $results        = array_merge($purchase, $stock, $sales, $salesChild, $purchaseChild);

		if(!empty($results)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=movementReport.csv');
	        print "Transaction Date, Transaction No,Location Code,Type,In,Out\r\n";
            $total_in = $total_out = 0;
			foreach ($results as $result) {
                
                $in   = $out = '';
                if(!$result['method']){
                   $total_in += $in = (int)$result['qty']; 
                } else {
                   $total_out += $out = (int)$result['qty'];
                }
                $date       = showDate($result['rdate']);
                $invoice_no = $result['invoice_no'];
                $location   = $result['location'];
                $type       = $result['type'];
				print "$date\",\"$invoice_no\",\"$location\",\"$type\",\"$in\",\"$out\"\r\n";
			}
			print "\"\",\"\",\"\",\"Total\",\"$total_in\",\"$total_out\"\r\n";
		}  	    
	}

	public function movementReport_pdf()
	{	
        $this->load->model('inventory/reports');    
        if (isset($_REQUEST['filter_date_from'])) {
            $filter_date_from = $_REQUEST['filter_date_from'];  
        } else {
            $filter_date_from = date('d/m/Y');
        }
        
        if (isset($_REQUEST['filter_date_to'])) {
            $filter_date_to = $_REQUEST['filter_date_to'];
        } else {
            $filter_date_to = date('d/m/Y');
        }

        if (isset($_REQUEST['filter_location'])) {
            $filter_location = $_REQUEST['filter_location'];
        } else {
            $filter_location =  $this->session->data['location_code'];
        }
        if (isset($_REQUEST['filter_product'])) {
            $filter_product = $_REQUEST['filter_product'];
        } else {
            $filter_product = '';
        }
        if (isset($_REQUEST['filter_product_id'])) {
            $filter_product_id = $_REQUEST['filter_product_id'];
        } else {
            $filter_product_id = '';
        }

        $data = array(
            'filter_date_from' => $filter_date_from,
            'filter_date_to'   => $filter_date_to,
            'filter_location'  => $filter_location,
            'filter_product'   => $filter_product,
            'filter_product_id'=> $filter_product_id
        );

        $sales          = $this->model_inventory_reports->getInventoryMovementForSales($data);
        $salesChild     = $this->model_inventory_reports->getInventoryMovementForSalesChild($data);
        $purchase       = $this->model_inventory_reports->getInventoryMovementForPurchase($data);
        $purchaseChild  = $this->model_inventory_reports->getInventoryMovementForPurchaseChild($data);
        $stock          = $this->model_inventory_reports->getInventoryMovementForStock($data);
        $results        = array_merge($purchase, $stock, $sales, $salesChild, $purchaseChild);

		if(!empty($results)){
	    	if($filter_location){
				$filterStr.=" Location: ".$filter_location.',';
			}
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			if($filter_product){
				$filterStr.=" Product : ".$filter_product.',';
			}
			if($filterStr){
				$filterStr = rtrim($filterStr,",");
			}

			$company_details = $this->model_inventory_reports->getCompanyDetails($this->session->data['company_id']);
			$headerStr	     = $this->url->getCompanyAddressHeaderString($company_details);
			
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">SKU Movement Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			        if($filterStr){
			        	$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			        }
			
			$str .="<table class='listing' style='width:100%;'><thead>
	           		<tr>
		           		<th>Trans.Date</th>
		           		<th>Trans.No</th>
		           		<th>Location</th> 
		           		<th>Type</th>
		           		<th>In</th>
		           		<th>Out</th>	           		
	           		</tr></thead> <tbody>";

            $total_in = $total_out = 0;
			foreach ($results as $result) {	
                
                $in   = $out = '';
                if(!$result['method']){
                   $total_in += $in = (int)$result['qty']; 
                } else {
                   $total_out += $out = (int)$result['qty'];
                }
                $date       = showDate($result['rdate']);
                $invoice_no = $result['invoice_no'];
                $location   = $result['location'];
                $type       = $result['type'];

				$str.="<tr>
                            <td>".$date."</td>
    					    <td>".$invoice_no."</td>
    					    <td>".$location."</td>
    					    <td>".$type."</td>
    					    <td align='right'>".$in."</td>
    					    <td align='right'>".$out."</td>
				        </tr>";
			}
                $str .= "<tr>
                        <td colspan='4'>Total</td>
                        <td align='right'>".$total_in."</td>
                        <td align='right'>".$total_out."</td>
                </tr>";
			$str.="</tbody></table>";

			if($str){
				$filename = 'SkuMovementReport_'.date('ymd').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
		}  	    
	}
	public function suppliersales(){

		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$this->document->setTitle('Supplier Wise Sales Report');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
 		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.='&filter_supplier='.$filter_supplier; 
		} else {
			$filter_supplier = 'All';
			$pageUrl.='&filter_supplier='.$filter_supplier; 
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		
		$data = array(
			'start'     	   => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'      	   => $this->config->get('config_admin_limit'),
			'filter_date_from' => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_from))),
			'filter_date_to'   => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_to))),
			'filter_location'  => $filter_location,
			'filter_channel'   => $filter_channel,
			'filter_supplier'  => $filter_supplier
		); 
		$this->data['data'] 	= $data;
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['vendors'] 	=  $this->model_transaction_sales->getVendors();

		$sale_totalAry = $this->model_transaction_sales->get_totalsupplierssku($data);
		// printArray($sale_totalAry); die;
		$this->data['overall_total'] = $sale_total;

		$results 	= $this->model_transaction_sales->get_suppliersalesNew($data);
		$sale_total = count($results['rows']);
		$results 	= $results['results'];
		$this->load->model('user/user');

		$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_supplierssales_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_supplierssales_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		foreach ($results as $result){	
			$this->data['sales'][] = array(
				'sku'         	  => $result['sku'],
				'name'            => $result['name'],
				'sku_price'       => $result['sku_price'],
				'sku_vendor_code' => $result['vendor_name'],
				'vendor_name'     => $result['vendor_name'],
				'saleQty'    	  => $result['saleQty']
			);
		}

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$this->data['channels'] 		= $this->cart->getChannelsList();

		$url.= $pageUrl;
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/suppliersales', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/suppliersales_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	public function export_supplierssales_pdf(){

		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];			
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];			
		}		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_channel) {
            $filter_network_name = $this->model_transaction_sales->getNetworkName($filter_channel);
			$filterStr.=" Sales Channel: ".$filter_network_name.',';
		}
		if($filter_supplier) {
			$filterStr.=" Supplier: ".$filter_supplier.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_date_from'  => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_from))),
			'filter_date_to'    => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_to))),
			'filter_location'   => $filter_location,
			'filter_channel'   => $filter_channel,
			'filter_supplier'   => $filter_supplier
		);
		$results = $this->model_transaction_sales->get_suppliersalesNew($data);
		$results = $results['results'];

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Supplier Wise sales report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>SKU</th>	
							<th>Description</th>
							<th>SKU Price</th>
							<th>Supplier</th>
							<th>Sale Qty</th>
						</tr>
					</thead> <tbody>"; 
			$amtTotal=0;
			foreach ($results as $result) {	
					$amtTotal += $result['sku_price'];
				$str.="<tr>
						   <td>".$result['sku']."</td>
						   <td>".$result['name']."</td>
						   <td>".$result['sku_price']."</td>
						   <td>".$result['vendor_name']."</td>
						   <td>".$result['saleQty']."</td> 
				      </tr>";	
			}
			// $str.="<tr><td align='right' colspan='2'>Total</td><td class='right'>".number_format($amtTotal,3)."</td>";
		    $str.="</tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'SuppliersWiseSalesReport'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}
	public function export_supplierssales_csv(){
		$this->load->model('transaction/sales');
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];			
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];			
		}

		$data = array(
			'filter_date_from'  => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_from))),
			'filter_date_to'    => date('Y-m-d', strtotime(str_replace('/', '-', $filter_date_to))),
			'filter_location'   => $filter_location,
			'filter_channel'    => $filter_channel,
			'filter_supplier'   => $filter_supplier
		);
		$results = $this->model_transaction_sales->get_suppliersalesNew($data);
          // printArray($results); exit;
		$results = $results['results']; 
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=suppliersWiseSalesReport.csv');
	        print "SKU,Description,SKU Price,Supplier,Sale Qty\r\n";
			$j=0; 
			$Qtytotal=0;
			$amtTotal=0;
			foreach ($results as $result) {	
						$sku           = $result['sku'];
						$name          = $result['name'];
						$sku_price     = $result['sku_price'];
						$sku_vendor_code= $result['vendor_name'];
						$saleQty       = $result['saleQty'];
						$amtTotal      += $result['sku_price'];
				    	print "\"$sku\",\"$name\",\"$sku_price \",\"$sku_vendor_code\",\"$saleQty\" \r\n";
			}
			// print "\"\",\"Total\",\"$amtTotal\"\r\n";
		}  
	}

	public function voidsalesdetailsreport() {
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
        $this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		$this->document->setTitle('Void Sales Details Report');
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	    = $this->session->data['company_id'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		if($_REQUEST['filter_date_from']==''){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']==''){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}

		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}		
		
		if (isset($_REQUEST['filter_invoiceno'])) {
			$filter_invoiceno = $_REQUEST['filter_invoiceno'];
			$pageUrl.='&filter_invoiceno='.$filter_invoiceno; 
		} else {
			$filter_invoiceno = null;
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),			
			'filter_invoiceno'=>$filter_invoiceno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location
		);
		$this->data['data'] = $data;
		$results = $this->model_transaction_sales->getVoidSalesDetailreport($data);
        foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_sales->getVoidSalesDetail($result['invoice_no']);
			$this->data['purchaseHeader'][] = array(
				'invoice_no'    => $result['invoice_no'],
				'invoice_date'  => date('d/m/Y',strtotime($result['invoice_date'])),
				'salesDetails'  =>$itemDetails
			);
		}

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination = new Pagination();
		$pagination->total = count($results);
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');	
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/voidsalesdetailsreport', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_void_detailsreport_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
     	$this->data['link_pdfexport'] = $this->url->link('transaction/transaction_reports/export_void_detailsreport_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');

     	$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/voidsalesdetailsreport.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_void_detailsreport_csv()
	{
		
		$this->load->model('transaction/sales');
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		}
		
		if (isset($_REQUEST['filter_invoiceno'])) {
			$filter_invoiceno = $_REQUEST['filter_invoiceno'];
		} else {
			$filter_invoiceno = null;
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location= $_REQUEST['filter_location'];
		} 
		$data = array(
			
			'filter_invoiceno'=>$filter_invoiceno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'	  => $filter_location
		);

		$tot_quantity = 0;
		$tot_value = 0;
		$salesHeader =  $this->model_transaction_sales->getVoidSalesDetailreport($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$results= $this->model_transaction_sales->getVoidSalesDetail($value['invoice_no']);

		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=voidSalesDetailReport.csv');
		        print "\"$invoice_no\"\r\n";
		        print "S.No,sku,Location Code,Description,Invoice No,Inoice Date,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$tot_qty=$tot_skuprice=$tot_subtotal=$tot_discount=$tot_gst=$tot_nettotal=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku'];
					$location       	= $result['location_code'];
					$description        = $result['description'];
					$invoice_no         = $value['invoice_no'];
					$invoice_date       = $value['invoice_date'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$net_total       =   $result['net_total'];
					
					  $tot_qty+=$result['qty'];
                      $tot_skuprice+=$result['sku_price'];
					  $tot_subtotal+=$result['sub_total'];
	                  $tot_discount+=$result['discount'];
	                  $tot_gst+=$result['gst'];
	                  $tot_nettotal+=$result['net_total'];

					print "$j,\"$sku\",\"$location\",\"$description\",\"$invoice_no\",\"$invoice_date\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$net_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"\",\"\",\"$tot_qty\",\"$tot_skuprice\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$tot_nettotal\"\r\n";
			}
		}   
	}

	public function export_void_detailsreport_pdf()
	{
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		
		if (isset($_REQUEST['filter_invoiceno'])) {
			$filter_invoiceno = $_REQUEST['filter_invoiceno'];
			$pageUrl.='&filter_invoiceno='.$filter_invoiceno; 
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.= '&filter_location='.$filter_location;
		} else {
			$filter_location = null;
		}

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		
		if($filter_transactionno) {
			$filterStr.=" Invoice No: ".$filter_invoiceno.',';
		}

		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			
			'filter_invoiceno'=>$filter_invoiceno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'	  => $filter_location
		);
		$results = $this->model_transaction_sales->getVoidSalesDetailreport($data);

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Void Sales Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$invoice_date = $value['invoice_date'];
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='6' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='5' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Location Code</th>
							<th>Description</th>
							<th>Invoice No</th>
							<th>Invoice Date</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_sales->getVoidSalesDetail($invoice_no);
		    	
				$j=0; 
				$tot_qty=$tot_skuprice=$tot_subtotal=$tot_discount=$tot_gst=$tot_nettotal=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku'];
					$location       	= $result['location_code'];
					$description        = $result['description'];
					$invoice_no       =   $value['invoice_no'];
					$invoice_date       = $value['invoice_date'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$net_total       = $result['net_total'];
					
                      $tot_qty+=$result['qty'];
					  $tot_skuprice+=$result['sku_price'];
					  $tot_subtotal+=$result['sub_total'];
	                  $tot_discount+=$result['discount'];
	                  $tot_gst+=$result['gst'];
	                  $tot_nettotal+=$result['net_total'];

					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$location."</td>
							   <td>".$description."</td>
							   <td>".$invoice_no."</td>
							   <td>".$invoice_date."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$net_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='5' align='right'>Total</td>
				             <td class='right'>".number_format($tot_qty,2)."</td>
							<td class='right'>".number_format($tot_skuprice,2)."</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($tot_nettotal,2)."</td>

				</tr><tr><td colspan='10' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'void_Salesdetailsreport_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
	}


	public function purchasecomparisonsummary() { 
		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
			
		if($print == 'printsummary'){
			$this->document->setTitle('Itemwise Purchase Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Itemwise Purchase');
			$this->getpurchasecomparisonsummaryList();
		}


		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
	}
	protected function getpurchasecomparisonsummaryList() {
		//language assign code for transaction purchase
		$this->document->setTitle('Purchase Comparision Summary Report');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning']= $this->error['warning'];
		} else {
			$this->data['error_warning']= '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');
		$company_id		= $this->session->data['company_id'];
		$stock_report 	= $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		} 
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} 
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		} 
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		} 
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		} 
		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		} 
        if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$pageUrl.= '&filter_product_id='.$filter_product_id;
		} else {
			$filter_product_id = '';
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}
			
		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/exportpurscompcsv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		); 
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=> $filter_supplier,
			'filter_transactionno'	=>$filter_transactionno,
			'filter_sku'		=> $filter_sku,
			'filter_product_id'	=> $filter_product_id,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_department' => $filter_department,			
			'filter_category' 	=> $filter_category,			
			'filter_brand' 		=> $filter_brand,		
			'filter_vendor' 	=> $filter_vendor,			
			'filter_name' 		=> $filter_name			
		);

		$this->data['data'] = $data;
		// printArray($data);
		
		$data['group_by'] 	= 1;
		$purchase_totalAry 	= $this->model_transaction_purchase->getTotalPursCompSummary($data);
		$results 			= $this->model_transaction_purchase->getPurchaseCompSummaryList($data);
		
		$this->load->model('user/user');
		$userDetail = array();
		$gst 	= 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&print=printsummary'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&export=yes'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['exportPDFAction'] = $this->url->link('transaction/transaction_reports/purchasecompsummary_pdfexport'.$pageUrl, 'token=' . $this->session->data['token'], 'SSL'); 

		foreach ($results as $result) {	
			$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']); 
			$this->data['sales'][] = array(
				'location_code'      => $result['location_code'],
				'sku'       		 => $result['sku'],
				'invoice_date'       => '',
				'description'        => $productDetails['sku_shortdescription'],
				'qty'                => $result['qty'],
				'received_qty'       => $result['received_qty'],
				'department'         => $result['department_name'],
				'category'           => $result['category_name'],
				'brand'				 => $result['sku_brand_code'],
				'vendor'			 => $result['sku_vendor_code'],			
			);

		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		
		$pagination 		= new Pagination();
		$pagination->total 	= count($results);
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salessummarysku', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
 
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments(); 
		$this->load->model('inventory/category');			
		$this->data['category_collection'] 	 = $this->model_inventory_category->getCategorys(); 
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] 	 = $this->model_master_vendor->getVendors(); 
		$this->load->model('inventory/brand');
		$this->data['brand_collection']  	 = $this->model_inventory_brand->getBrands(); 
		$this->data['filter_department'] 	 = $filter_department;
		$this->data['Tolocations'] 	= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] 		= $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->template = 'transaction/purchase_comparison_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function exportpurscompcsv(){

		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/purchase');
		$results =$this->getpurscompsummaryListcsv();
	    $i=0;
	    $PaidAmountTot = 0;
	    $BalanceAmountTot = 0;
			// printArray($results); die;

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=PurchaseComparisonSummary.csv');
	        print "S.No,LocationCode,Department,Category,Brand,Vendor,Sku,Description,Quantity,ReceivedQuantity,BalanceQuantity\r\n";
	        $this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/purchasecomparisonsummary&export=yes'.$strUrl,'token='.$this->session->data['token'].$url, 'SSL');

			$j = $total = 0;
			foreach ($results as $result) {	
					$j++;
					$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
					$sku_shortdescription = substr(preg_replace('/[^A-Za-z0-9\-]/','',$productDetails['sku_shortdescription']),20);

					$sku    		= $result['sku'];
					$qty            = $result['qty'];
					$brand 			= $result['sku_brand_code'];
					$description    = $sku_shortdescription;
					$invoice_date   = '';
					$location_code  = $result['location_code'];
					$received_qty   = $result['received_qty'];
					$balance_qty    = $result['qty'] - $result['received_qty'];
					$department     = $result['department_name'];
					$category       = $result['category_name'];
					$vendor 		= $result['sku_vendor_code'];
					$total+=$net_total;
					print "$j,\"$location_code\",\"$department\",\"$category\",\"$brand\",\"$vendor\",\"$sku\",\"$description\",\"$qty\",\"$received_qty\",\"$balance_qty\"\r\n";		
			} 
		}  
	}
	protected function getpurscompsummaryListcsv() {		
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$pageUrl.='&filter_product_id='.$filter_product_id; 
		} else {
			$filter_product_id = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		} 
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		} 
		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_category='.$filter_brand;
		} else {
			$filter_brand = '';
		}
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_category='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		
		$data = array(
			'filter_transactionno' 	=> $filter_transactionno,
			'filter_supplier' 	=> $filter_supplier,
			'filter_date_from' 	=> $filter_date_from,
			'filter_product_id'	=> $filter_product_id,
			'filter_date_to' 	=> $filter_date_to,
			'filter_category' 	=> $filter_category,
			'filter_department' => $filter_department,
			'filter_location' 	=> $filter_location,			
			'filter_brand' 		=> $filter_brand,			
			'filter_vendor' 	=> $filter_vendor			
		);
		$results = $this->model_transaction_purchase->getPurchaseCompSummaryList($data);
		// printArray($results); die;
		return $results;		
	}
	public function purchasecompsummary_pdfexport(){

		$this->load->model('transaction/sales');
		$this->load->model('transaction/purchase');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department'); 
		$this->load->model('inventory/category');
		$this->load->model('master/vendor'); 

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_product_id'])) {
			$filter_product_id = $this->request->get['filter_product_id'];			
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];			
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '';
		}
		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} 

        if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} 

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_department){
		    $departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
		    $filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_category){
		    $categoryResult = $this->model_inventory_category->getCategoryNameByCode($filter_category);
		    $filterStr.=" Category: ".$categoryResult['category_name'].',';
		}
		if($filter_brand) {
			$filterStr.=" Brand: ".$filter_brand.',';
		}
		if($filter_vendor) {
			$vendorResult = $this->model_master_vendor->getSuppliersDetailsbyCode($filter_vendor);
			$filterStr.=" Vendor: ".$vendorResult['vendor_name'].',';
		}
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		
		$filterVal = 1;
		$data = array(
			'filter_date_from' 	=> $filter_date_from,
			'filter_date_to' 	=> $filter_date_to,
			'filter_location' 	=> $filter_location,
			'filterVal'  		=> $filterVal,
			'filter_product_id'	=> $filter_product_id,
			'filter_product_id' => $filter_product_id,
			'filter_category' 	=> $filter_category,
			'filter_department' => $filter_department,
			'filter_brand' 		=> $filter_brand,
			'filter_vendor' 	=> $filter_vendor
		);
		$data['group_by'] = 1;
		$results = $this->model_transaction_purchase->getPurchaseCompSummaryList($data);

	    if(count($results >=1 )){
	    	$company_id		= $this->session->data['company_id'];
			$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr		= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Comparison Summary Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location</th>
							<th>Department</th>
							<th>Category</th>
							<th>Brand</th>
							<th>Vendor</th>
							<th>Sku</th>
							<th>Description</th>
							<th>Quantity</th>
							<th>Received Quantity</th>
							<th>Balance Quantity</th>
						</tr></thead> <tbody>";
	       
			$Qtytotal = $amtTotal = 0;
			foreach ($results as $result) {	
					    $productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
						$location_code      = $result['location_code'];
						$sku    		    = $result['sku'];
						$invoice_date       = '';
						$description      	= $productDetails['sku_description'];
						$qty                = $result['qty'];
						$received_qty       = $result['received_qty'];
						$balance_qty        = $result['qty'] - $result['received_qty'];
						$department         = $result['department_name'];
						$category           = $result['category_name'];
						$brand				= $result['sku_brand_code'];
						$vendor				= $result['sku_vendor_code'];
				$str.="<tr>
				   <td>".$location_code."</td>
				   <td>".$department."</td>
				   <td>".$category."</td>
				   <td>".$brand."</td>
				   <td>".$vendor."</td>
				   <td>".$sku."</td>
				   <td>".$description."</td>
				   <td>".$qty."</td>
				   <td>".$received_qty."</td>
				   <td>".$balance_qty."</td>
			</tr>";	
			}

		$str.="</tbody></table>";
		if($str){
			$filename = 'PurchaseComparisonSummary'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}

	public function purchase_servicenote_summary(){

		$this->load->model('transaction/purchase');
		$this->document->setTitle('Purchase Service Note Summary Report');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id		= $this->session->data['company_id'];
		$stock_report 	= $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.= '&filter_reference_date='.$filter_location_code;
		} else {
			$filter_reference_date = $this->session->data['filter_reference_date'];
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home','token='.$this->session->data['token'],'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'    => 'Reports',
			'href'    => $this->url->link('report/report','token='.$this->session->data['token'],'SSL'),
			'separator'=> ' <i class="fa fa-angle-right"></i> '
		);		
		
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  			=> $show_hold,
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			=> $this->config->get('config_admin_limit'),
			'filter_supplier' 		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from' 		=> $filter_date_from,
			'filter_date_to' 		=> $filter_date_to,
			'filter_reference_date' => $filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);

		$this->data['data'] 		= $data;
		$results 					= $this->model_transaction_purchase->getPurchaseServiceList($data);
		$this->data['totals']  		= $this->model_transaction_purchase->getPurchaseServiceTotal($data);
		$this->data['vendors'] 		= $this->model_transaction_purchase->getVendors();
		$this->data['Tolocations'] 	= $this->cart->getLocation($this->session->data['location_code']);

		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['export_csv'] = $this->url->link('transaction/transaction_reports/export_purchase_service_summary_csv', 'token='.$this->data['token'].'&'.$pageUrl, 'SSL');
		$this->data['export_pdf'] = $this->url->link('transaction/transaction_reports/export_purchase_service_summary_pdf', 'token='.$this->data['token'].'&'.$pageUrl, 'SSL');
		$this->data['back'] 	  = $this->url->link('report/report','token='.$this->data['token'],'SSL');

		foreach ($results as $result) {

			$this->data['purchases'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_id'],
				'vendor_name'       => $result['vendor_name'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'purchasedetails'   => $this->url->link('transaction/transaction_reports/purchase_servicenote_details', 'token=' . $this->session->data['token'] . '&filter_transactionno=' . $result['transaction_no'] .'&filter_location_code='.$filter_location_code.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to, 'SSL')
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;

		$pagination 		= new Pagination();
		$pagination->total 	= $this->data['totals']['totalPurchase'];
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports&type=purchase_summary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		$this->template = 'transaction/purchase_servicenote_summary.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_purchase_service_summary_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');		
		
		$url = '';		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_supplier'		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_reference_date'	=> $filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);
		$data['filter_stock'] =$this->config->get('config_report_qty_negative');
		$results = $this->model_transaction_purchase->getPurchaseServiceList($data);

		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=PurchaseServiceNoteReport.csv');
	        print "Tran No,Tran Date,Location,Supplier,Total,GST,NetTotal\r\n";
		
			$j=0; 
			foreach ($results as $result) {	
				$subTotal = number_format($result['sub_total'],2);
				$gst 	  = number_format($result['gst'],2);
				$netTotal = number_format($result['total'],2);
				$grandtotal+= $result['total']; 
				
				$purchase_id 	=  $result['purchase_id'];
				$transaction_no =  $result['transaction_no'];
				$transaction_date= date('d/m/Y',strtotime($result['transaction_date']));
				$location_code  = $result['location_code'];
				$supplier 		= $result['vendor_name'];
				$refno 			= $result['reference_no'];
				$refdate 		= date('d/m/Y',strtotime($result['reference_date']));

				print "\"$transaction_no\",\"$transaction_date\",\"$location_code\",\"$supplier\",\"$subTotal\",\"$gst\",\"$netTotal\"\r\n";
			}
				$grandtotal = number_format($grandtotal,2);
				print "\"\",\"\",\"\",\"\",\"\",\"Total\",\"$grandtotal\"\r\n";	
		}
	}

	public function export_purchase_service_summary_pdf(){
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		} else {
			$filter_date_to = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} else{
			$filter_location_code = $this->session->data['location_code'];
		}

		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
		} else{
			$filter_reference_date ='';
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_reference_date'	=> $filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);
		$results 		= $this->model_transaction_purchase->getPurchaseServiceList($data);
		$company_id		= $this->session->data['company_id'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr		= $this->url->getCompanyAddressHeaderString($company_details);
		$str 			= $headerStr;

		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Service Note Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';

		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing' style='width:100%;'><thead>
			<tr>
             <th>S No</th>
             <th>Tran No</th>
             <th>Tran Date</th>
             <th>Location</th>
             <th>Supplier</th>
             <th>Subtotal</th>
             <th>GST</th>
             <th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		$total = 0;
		foreach ($results as $result) {
			$j++;
			$subTotal = number_format($result['sub_total'],2);
			$gst 	  = number_format($result['gst'],2);
			$netTotal = number_format($result['total'],2);		
			$total +=$result['total']; 

			$transaction_no   = $result['transaction_no'];
			$transaction_date = date('d/m/Y',strtotime($result['transaction_date']));
			$transaction_type = $result['transaction_type'];
			$location_code    = $result['location_code'];
			$vendor_name      = $result['vendor_name'];
			$refno      	  = $result['reference_no'];
			$refdate      	  =	date('d/m/Y',strtotime($result['reference_date']));

			$str.="<tr>
					<td>".$j."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td>".$location_code."</td>
				   <td>".$vendor_name."</td>
				   <td class='right'>".$subTotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$netTotal."</td>
			</tr>";	
		}
		$total = number_format($total,2);
		$str.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td>
		<td class='right'>".$total."</td></tr>";
		$str.="</tbody></table>";

		if($str){
			$filename = 'Purchase_Service_Note_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function purchase_servicenote_details(){

		$this->load->model('transaction/purchase');
		$this->load->model('inventory/reports');
		$this->document->setTitle('Purchase Service Note Details Report');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$company_id			= $this->session->data['company_id'];
		$stock_report 		= $this->request->get['stock_report'];
		$company_details 	= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo		= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			//$filter_date_from = '';
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_location_code']) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			//$filter_date_to = '';
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno;
		
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}
		if (isset($_GET['filter_transactionno']) && !$_POST) {
			$filter_date_from='';
			$filter_date_to='';
			$pageUrl.= '&filter_date_from=';
            $pageUrl.= '&filter_date_to='; 
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home','token='.$this->session->data['token'],'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'    => 'Reports',
			'href'    => $this->url->link('report/report','token='.$this->session->data['token'],'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			=> $this->config->get('config_admin_limit'),
			'filter_supplier'		=>$filter_supplier,
			'filter_transactionno'	=>$filter_transactionno,
			'filter_date_from'		=>$filter_date_from,
			'filter_date_to'		=>$filter_date_to,
			'filter_reference_date'	=>$filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);
		$this->data['data']   = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPurchaseServiceNote($data);
		$purchase_total 	  = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = $purchase_grand_total['grandtotal'];
		$results 		      = $this->model_transaction_purchase->getPurchaseServiceList($data);
		
		$this->data['vendors']    = $this->model_transaction_purchase->getVendors();
		$this->data['export_csv'] = $this->url->link('transaction/transaction_reports/export_purchase_servicenote_details_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['export_pdf'] = $this->url->link('transaction/transaction_reports/export_purchase_servicenote_details_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['back']   = $this->url->link('report/report', 'token='.$this->data['token'], 'SSL');
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		// $this->load->model('master/vendor');

		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
	
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_purchase->getPurchaseServiceNoteDetails($result['transaction_no']);

			$this->data['purchaseHeader'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_code'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'salesDetails'      => $itemDetails
			);
		}

		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;

		$pagination 		= new Pagination();
		$pagination->total 	= $purchase_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/purchase_servicenote_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] 	= $pagination->render();
		$this->data['sort'] 		= $sort;
		$this->data['order'] 		= $order;
		$this->template = 'transaction/purchase_servicenote_details.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_purchase_servicenote_details_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		// $this->data['heading_title'] = 'Stock Report';
		
		$url = '';		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}
		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'      	=> $filter_supplier,
			'filter_transactionno' 	=> $filter_transactionno,
			'filter_date_from'	   	=> $filter_date_from,
			'filter_date_to' 		=> $filter_date_to,
			'filter_reference_date'	=> $filter_reference_date,
			'filter_location_code'	=> $filter_location_code
		);

		$salesHeader =  $this->model_transaction_purchase->getPurchaseServiceList($data);
	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$refno 		= $value['reference_no'];
				$refdate 	= date('d/m/Y',strtotime($value['transaction_date']));
				$results= $this->model_transaction_purchase->getPurchaseServiceNoteDetails($value['transaction_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=PurchaseServiceNoteDetailReport.csv');
		        print "\"Invoice No : $invoice_no\",\"\",\"Ref No : $refno\",\"\",\"Tran Date: $refdate\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];

					  $actualtotal+=$result['total'];
	                  $tot_subtotal+=$result['net_price'];
	                  $tot_gst+=$result['tax_price'];
	                  $tot_discount+=$result['discount_price'];
			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}   
	}
	public function export_purchase_servicenote_details_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);

		$results 	= $this->model_transaction_purchase->getPurchaseServiceList($data);
		$company_id	= $this->session->data['company_id'];
		$company_details  =  $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str 		= $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Invoice Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no   = $value['transaction_no'];
				$invoice_date = date('d/m/Y',strtotime($value['transaction_date']));
				$refno        = $value['reference_no'];
				$refdate      = date('d/m/Y',strtotime($value['reference_date']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='2' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference No:".$refno."</th>
						<th class='left' style='border-right:none;'></th>
						<th colspan='3' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";

				$results= $this->model_transaction_purchase->getPurchaseServiceNoteDetails($invoice_no);
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];

				  $actualtotal+=$result['total'];
                  $tot_subtotal+=$result['net_price'];
                  $tot_gst+=$result['tax_price'];
                  $tot_discount+=$result['discount_price'];
					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($actualtotal,2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
		if($str){
			$filename = 'purchase_service_note_details_Report_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function sales_servicenote_summary(){

		$this->document->setTitle('Sales Service Note Summary Report');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']  = $this->language->get('text_tran_no');
		$this->data['text_tran_dt']  = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] 	 = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back']   = $this->language->get('button_back');
		$this->data['token'] 		 = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$company_id	      = $this->session->data['company_id'];
		$stock_report 	  = $this->request->get['stock_report'];
		$company_details  = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	  = $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Summary Reports',
			'href'      => $this->url->link('transaction/transaction_reports/salessummary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'            	=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'	=>$filter_date_from,
			'filter_date_to'	=>$filter_date_to,
			'location_code' 	=> trim($filter_location)
		);
		$this->data['data'] 	= $data;
		$sale_totalAry 			= $this->model_transaction_sales->getTotalSalesService($data);
		$this->data['sales_net_total'] = $sale_totalAry['net_total'];
		$sale_total    			= $sale_totalAry['totalSale'];
		$results 				= $this->model_transaction_sales->getSalesServiceList($data);

		$this->load->model('user/user');
		$userDetail = array();
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['export_csv'] = $this->url->link('transaction/transaction_reports/sales_servicenote_summary_csv'.$strUrl, 'token='.$this->data['token'].$url,'SSL');
		$this->data['export_pdf'] = $this->url->link('transaction/transaction_reports/sales_servicenote_summary_pdf'.$pageUrl, 'token='.$this->data['token'].$url,'SSL');
		$this->data['back'] 	  = $this->url->link('transaction/transaction_reports/sales_servicenote_summary'.$pageUrl, 'token=' . $this->data['token'], 'SSL');
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);

		foreach ($results as $result) {	
			$userDetail = $this->model_user_user->getUser($result['createdby']);
			
			$this->data['sales'][] = array(
				'location_code'       => $result['location_code'],
				'invoice_no'          => $result['invoice_no'],
				'invoice_date'        => date('d/m/Y',strtotime($result['invoice_date'])),
				'sub_total'  		  => $result['sub_total'],
				'discount' 			  => $result['discount'],
				'gst'       		  => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['net_total'],
				'created_by'          => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'        => $result['createdon'],
				'salesDetail'         => $this->url->link('transaction/transaction_reports/sales_servicenote_details','token='.$this->data['token'].'&filter_transactionno='.$result['invoice_no'])
			);
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}

		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/sales_servicenote_summary', 'token=' . $this->session->data['token'].$pageUrl.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;

		$this->template = 'transaction/sales_service_summary.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function sales_servicenote_summary_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location_code' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesServiceList($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=SalesServiceNoteSummary.csv');
	        print "S.No,Location Code,Invoice No,Invoice Date,Sub Total,Discount,GST,Actual Total,Round Off,Net Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$j++;
						$location_code  = $result['location_code'];
						$invoice_no     = $result['invoice_no'];
						$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
						$sub_total  	= $result['sub_total'];
						$discount  		= $result['discount'];
						$gst       		= $result['gst'];
						$actual_total   = $result['actual_total'];
						$round_off      = $result['round_off'];
						$net_total		= $result['net_total'];
						$total+=$net_total;
		
				print "$j,\"$location_code\",\"$invoice_no\",\"$invoice_date\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\",\"$round_off\",\"$net_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}
	}
	public function sales_servicenote_summary_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
 
		$filterStr = '';
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		$data = array(
			'filter_supplier'	   => $filter_supplier,
			'filter_transactionno' => $filter_transactionno,
			'filter_date_from'	   => $filter_date_from,
			'filter_date_to'	   => $filter_date_to,
			'location_code' 	   => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesServiceList($data);
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location) {
			$filterStr.=" Location : ".$filter_location.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction no : ".$filter_transactionno.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$company_id	     = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	     = $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Service Note Summary Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<th>Location Code</th>
			<th>Invoice No</th>
			<th>Invoice Date</th>
			<th>Sub Total</th>
			<th>Discount</th>
			<th>GST</th>
			<th>Actual Total</th>
			<th>Round Off</th>
			<th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$location_code  = $result['location_code'];
			$invoice_no     = $result['invoice_no'];
			$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
			$sub_total  	= $result['sub_total'];
			$discount  		= $result['discount'];
			$gst       		= $result['gst'];
			$actual_total   = $result['actual_total'];
			$round_off      = $result['round_off'];
			$net_total		= $result['net_total'];
			
			$total_Stock_Value+=$net_total;

			$str.="<tr><td>".$j."</td>
				   <td>".$location_code."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td class='right'>".$sub_total."</td>
				   <td class='right'>".$discount."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$actual_total."</td>
				   <td class='right'>".$round_off."</td>
				   <td class='right'>".$net_total."</td>
			</tr>";	
		}
		$str.="<tr><td colspan='9' align='right'>Total</td><td align='right'>".$total_Stock_Value."</td>
		</tr></tbody></table>";
		if($str){
			$filename = 'SalesServiceNoteSummaryReport_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function sales_servicenote_details(){
	
		$this->document->setTitle('Sales Service Note Details Report');
		$this->load->model('transaction/sales');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id			= $this->session->data['company_id'];
		$stock_report 		= $this->request->get['stock_report'];
		$company_details 	= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo		= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if ($_REQUEST['filter_location']) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSalesService($data);
		$sale_total    = $sale_totalAry['totalSale'];
		$results 	   = $this->model_transaction_sales->getSalesServiceList($data);

		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;

		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['export_csv']  = $this->url->link('transaction/transaction_reports/sales_servicenote_details_csv', 'token=' . $this->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['export_pdf']  = $this->url->link('transaction/transaction_reports/sales_servicenote_details_pdf', 'token='.$this->data['token'].'&&'.$pageUrl,'SSL');

		$salesAry = array();
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_sales->getSalesServiceDetails($result['invoice_no']);
			$this->data['salesHeader'][] = array(
				"location_code"		=>$result['location_code'],
				"invoice_no"		=>$result['invoice_no'],
				"invoice_date"		=>date('d/m/Y',strtotime($result['invoice_date'])),
				"createdTime"		=> date('H:i',strtotime($result['createdon'])),
				"customer_code"		=>$result['customer_code'],
				"sub_total"			=>$result['sub_total'],
				"discount"			=>$result['discount'],
				"gst"				=>$result['gst'],
				"actual_total"		=>$result['actual_total'],
				"round_off"			=>$result['round_off'],
				"net_total"			=>$result['net_total'],
				"cashier"			=>$result['cashier'],
				"terminal_code"		=>$result['terminal_code'],
				"header_remarks"	=>$result['header_remarks'],
				'salesDetails'		=>$itemDetails
			);			
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($filterVal!='1'){
		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/salesdetail', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		}
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/sales_servicenote_details.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function sales_servicenote_details_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}

		$data = array(
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal
		);

		$salesHeader = $this->model_transaction_sales->getSalesServiceList($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$results= $this->model_transaction_sales->getSalesServiceDetails($value['invoice_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=SalesServiceNoteDetailReport.csv');
		        print "\"$invoice_no\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];

					  $actualtotal+=$result['net_total'];
	                  $tot_subtotal+=$result['sub_total'];
	                  $tot_gst+=$result['gst'];
	                  $tot_discount+=$result['discount'];

			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}
	}
	public function sales_servicenote_details_pdf(){

		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y', strtotime($date .' -1 day'));	
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} else {
			$filter_date_from = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		} else {
			$filter_date_to = '';
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		$data = array(
			'filter_transactionno'	=> $filter_transactionno,
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_location'		=> $filter_location,
			'filterVal' 			=> $filterVal
		);

		$salesHeader = $this->model_transaction_sales->getSalesServiceList($data);
	    if(count($salesHeader>=1)){
	    	if($filter_location){
				$filterStr.=" Location: ".$filter_location.',';
			}
			if($filter_date_from){
				$filterStr.=" From Date: ".$filter_date_from.',';
			}
			if($filter_date_to){
				$filterStr.=" To Date: ".$filter_date_to.',';
			}
			if($filter_transactionno){
				$filterStr.=" Transaction No: ".$filter_transactionno.',';
			}
			$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
		
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Service Note Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['invoice_no'];
				$invoice_date = date('d/m/Y',strtotime($value['invoice_date']));
				$createdTime  = date('H:i',strtotime($result['createdon']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='5' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='4' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_sales->getSalesServiceDetails($invoice_no);
		    	
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['qty'];
					$sku_price    		= $result['sku_price'];
					$sub_total          = $result['sub_total'];
					$discount    		= $result['discount'];
					$gst    			= $result['gst'];
					$actual_total       = $result['net_total'];
					$actualtotal+=$result['net_total'];
					$tot_subtotal+=$result['sub_total'];
					$tot_gst+=$result['gst'];
					$tot_discount+=$result['discount'];
					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($actualtotal,2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'SalesServiceNoteDetailsReport_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
		}
	}
	public function delivery_report(){

		$this->document->setTitle('Delivery Summary Report');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']  = $this->language->get('text_tran_no');
		$this->data['text_tran_dt']  = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] 	 = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back']   = $this->language->get('button_back');
		$this->data['token'] 		 = $this->session->data['token'];
		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');
		
		$company_id		 = $this->session->data['company_id'];
		$stock_report 	 = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	 = $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Delivery Report',
			'href'      => $this->url->link('transaction/transaction_reports/delivery_report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit')
		);
		$this->data['filters'] 	= $data;
		$delivery_total			= $this->model_transaction_delivery->getTotalDelivery($data);
		$results 				= $this->model_transaction_delivery->getDeliveryList($data);

		$this->data['export_csv']  = $this->url->link('transaction/transaction_reports/delivery_report_csv'.$strUrl, 'token='.$this->data['token'].$url,'SSL');
		$this->data['export_pdf']  = $this->url->link('transaction/transaction_reports/delivery_report_pdf'.$pageUrl, 'token='.$this->data['token'].$url,'SSL');
		$this->data['back'] 	   = $this->url->link('report/report'.$pageUrl, 'token=' . $this->data['token'], 'SSL');
		$this->data['salesmanlist']= $this->model_transaction_delivery->getDeliveryMansList();

		foreach ($results as $result) {
			$deliveryman 		   = $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);
			$result['assignedon']  = $result['assignedon'] !='' ? date('d/m/Y',strtotime($result['assignedon'])) : '-';
			$this->data['delivery'][] = array(
				'invoice_no'     => $result['sales_transaction_no'],
				'do_no'   		 => $result['do_no'],
				'invoice_date'   => $result['assignedon'],
				'name'           => $result['name'],
				'deliveryman_id' => $deliveryman,
				'status'         => $result['status'],
				'details_link'	 => $this->url->link('transaction/transaction_reports/delivery_report_details&filter_do_number='.$result['do_no'].'&filter_from_date='.$filter_from_date.'&filter_to_date='.$filter_to_date.'&from=summary', 'token=' . $this->data['token'], 'SSL')
			);
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}

		$pagination 		= new Pagination();
		$pagination->total 	= $delivery_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/delivery_report', 'token=' . $this->session->data['token'].$url.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;

		$this->template = 'transaction/delivery_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function delivery_report_csv(){

		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status
		);
		$this->data['filters'] 	= $data;
		$results 				= $this->model_transaction_delivery->getDeliveryList($data);
	    if(count($results >=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=DeliveryReport.csv');
	        print "\"$invoice_no\"\r\n";
	        print "S.No,Do No,Invoice No,Customer,Date,Delivery Man,Status\r\n";
				
				$j=0; 
				foreach ($results as $result) {	
					$j++;
					$do_no 		= $result['do_no'];
					$invoice_no = $result['sales_transaction_no'];
					$name 		= $result['name'];
					$status 	= showText($result['status']);
					$deliveryman= $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);
					$assignedon = $result['assignedon'] !='' ? date('d/m/Y',strtotime($result['assignedon'])) : '-';

					print "$j,\"$do_no\",\"$invoice_no\",\"$name\",\"$assignedon\",\"$deliveryman\",\"$status\"\r\n";
				}
		}
	}
	public function delivery_report_pdf(){

		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status
		);

    	if($filter_customer_name){
			$filterStr.=" Customer Name: ".$filter_customer_name.',';
		}
    	if($filter_contact_number){
			$filterStr.=" Contact: ".$filter_contact_number.',';
		}
    	if($filter_transaction){
			$filterStr.=" Invoice No: ".$filter_transaction.',';
		}
    	if($filter_do_number){
			$filterStr.=" Do No: ".$filter_do_number.',';
		}
    	if($filter_delivery_man){
			$deliveryman = $this->model_transaction_delivery->getDeliveryManName($filter_delivery_man);
			$filterStr.=" Delivery Man: ".$deliveryman.',';
		}
    	if(!empty($filter_delivery_status)){
    		$filter_delivery_status = implode(',',$filter_delivery_status);
			$filterStr.=" Delivery Status: ".$filter_delivery_status.',';
		}
		if($filter_from_date){
			$filterStr.=" From Date: ".$filter_from_date.',';
		}
		if($filter_to_date){
			$filterStr.=" To Date: ".$filter_to_date.',';
		}

		$results  		 = $this->model_transaction_delivery->getDeliveryList($data);
		$company_id		 = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
		$str 			 = $headerStr;
	
		$str.= '<table style="width:100%;">
					<tr>
						<td align="left" style="font-size:20px;">Delivery Report</td>
						<td align="right">Time: '.date('d-m-Y H:i').'</td>
					</tr>
				</table>';
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing' style='width:100%;'><thead>
					<tr>
						<th>Sno</th>
						<th>Do No</th>
						<th>Invoice No</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Delivery Man</th>
						<th>Status</th>
					</tr>
				   </thead>
				<tbody>";		    	
				$M = 0; 
				foreach ($results as $result) {	
					$M ++;
					$date = $result['assignedon'] !='' ? date('d/m/Y',strtotime($result['assignedon'])) : '-';
					$deliveryman= $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);
					$str.="<tr>
							   <td>".$M."</td>
							   <td>".$result['do_no']."</td>
							   <td>".$result['sales_transaction_no']."</td>
							   <td>".$result['name']."</td>
							   <td>".$date."</td>
							   <td>".$deliveryman."</td>
							   <td>".showText($result['status'])."</td>
						</tr>";	
				
				}
				$str.="</tbody></table>";

			if($str){
				$filename = 'DeliveryReport_'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
	}
	public function delivery_report_details(){
	
		$this->document->setTitle('Delivery Details Report');
		$this->load->model('transaction/sales');
		$this->load->model('transaction/delivery');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id			= $this->session->data['company_id'];
		$stock_report 		= $this->request->get['stock_report'];
		$company_details 	= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo		= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Delivery Details Report',
			'href'      => $this->url->link('transaction/transaction_reports/delivery_report_details', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit')
		);
		$this->data['filters'] 	= $data;
		$delivery_total			= $this->model_transaction_delivery->getTotalDelivery($data);
		$results 				= $this->model_transaction_delivery->getDeliveryList($data);

		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['export_csv']  = $this->url->link('transaction/transaction_reports/delivery_report_details_csv', 'token=' . $this->data['token'] . '&&'.$url, 'SSL');
		$this->data['export_pdf']  = $this->url->link('transaction/transaction_reports/delivery_report_details_pdf', 'token='.$this->data['token'].'&&'.$url,'SSL');

		$salesAry = array();
		// printArray($results); 
		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_delivery->getDeliveryDetails($result['do_no']);
			$deliveryman = $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);

			$dates = strtotime($result['deliveredon']); 
			$delivery_date  = $dates > 0 ? date('d/m/Y',strtotime($result['deliveredon'])) : '';
			$this->data['deliveryHeader'][] = array(
				"invoice_no"	  => $result['sales_transaction_no'],
				"do_no"			  => $result['do_no'],
				"name"			  => $result['name'],
				"deliveryman"	  => $deliveryman,
				"invoice_date"	  => date('d/m/Y',strtotime($result['invoice_date'])),
				"delivery_date"	  => $delivery_date,
				'deliveryDetails' => $itemDetails,
			);			
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		if (isset($this->request->get['from'])) {
			$this->data['back'] = $this->url->link('transaction/transaction_reports/delivery_report', 'token=' . $this->session->data['token'], 'SSL');
		}
		$this->data['salesmanlist'] = $this->model_transaction_delivery->getDeliveryMansList();
		$pagination 		= new Pagination();
		$pagination->total 	= $delivery_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/delivery_report_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->template = 'transaction/delivery_details_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function delivery_report_details_csv(){

		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status
		);
		$this->data['filters'] 	= $data;
		$results 				= $this->model_transaction_delivery->getDeliveryList($data);
	    if(count($results >=1)){
	    	//printArray($results);
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=DeliveryReport.csv');
	        /*print "\"$do_no\"\r\n";*/
	        print "S.No,Do No,Invoice No,Sku,Name,Customer,Delivery Man,Status,Delivery Date\r\n";
			$j=0; 
			foreach ($results as $result) {	
				$dates = strtotime($result['deliveredon']);
				$do_no = $result['do_no'];
				$itemDetails = $this->model_transaction_delivery->getDeliveryDetails($result['do_no']);
				foreach ($itemDetails as $items) {	
					$j++;
					$do_no 		    = $items['do_no'];
					$invoice_no     = $result['sales_transaction_no'];
					$customer       = $result['name'];
					$name 		    = $items['name'];
					$sku 		    = $items['sku'];
					$status 	    = showText($result['status']);
					$delivery_date 	= $dates > 0 ? date('d/m/Y',strtotime($result['deliveredon'])) : '';
					$deliveryman= $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);

					print "$j,\"$do_no\",\"$invoice_no\",\"$sku\",\"$name\",\"$customer\",\"$deliveryman\",\"$status\",\"$delivery_date\"\r\n";
				}
			}	
		}
	}
	public function delivery_report_details_pdf(){

		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_do_number' 		=> $filter_do_number,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_payment_status'	=> $filter_payment_status
		);

    	if($filter_customer_name){
			$filterStr.=" Customer Name: ".$filter_customer_name.',';
		}
    	if($filter_contact_number){
			$filterStr.=" Contact: ".$filter_contact_number.',';
		}
    	if($filter_transaction){
			$filterStr.=" Invoice No: ".$filter_transaction.',';
		}
    	if($filter_do_number){
			$filterStr.=" Do No: ".$filter_do_number.',';
		}
    	if($filter_delivery_man){
			$deliveryman = $this->model_transaction_delivery->getDeliveryManName($filter_delivery_man);
			$filterStr.=" Delivery Man: ".$deliveryman.',';
		}
    	if(!empty($filter_delivery_status)){
    		$filter_delivery_status = implode(',',$filter_delivery_status);
			$filterStr.=" Delivery Status: ".$filter_delivery_status.',';
		}
		if($filter_from_date){
			$filterStr.=" From Date: ".$filter_from_date.',';
		}
		if($filter_to_date){
			$filterStr.=" To Date: ".$filter_to_date.',';
		}

		$results  		 = $this->model_transaction_delivery->getDeliveryList($data);
		$company_id		 = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
		$str 			 = $headerStr;
	
		$str.= '<table style="width:100%;">
					<tr>
						<td align="left" style="font-size:20px;">Delivery Report</td>
						<td align="right">Time: '.date('d-m-Y H:i').'</td>
					</tr>
				</table>';
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$M = 0;
		foreach ($results as $result) {
		$M ++;
		$date = $result['assignedon'] !='' ? date('d/m/Y',strtotime($result['assignedon'])) : '-';
		$str.="<table class='listing' style='width:100%;'>
				   <thead>
					<tr>
						<th>Do No :".$result['do_no']."</th>
						<th colspan='2'>Invoice No :".$result['sales_transaction_no']."</th>
						<th></th>
						<th></th>
						<th colspan='2'>Date :".$date."</th>
					</tr>
					<tr>
						<th>Sno</th>
						<th>Sku</th>
						<th>Name</th>
						<th>Customer</th>
						<th>Qty</th>
						<th>Status</th>
						<th>Delivery Date</th>
					</tr>
				   </thead>
				<tbody>";

			$itemDetails = $this->model_transaction_delivery->getDeliveryDetails($result['do_no']);
			foreach ($itemDetails as $items) {
			$dates = strtotime($result['deliveredon']);
			$cort_date = $dates > 0 ? date('d/m/Y',strtotime($result['deliveredon'])) : '';
				$deliveryman= $this->model_transaction_delivery->getDeliveryManName($result['deliveryman_id']);
				$str.="<tr>
						   <td>".$M."</td>
						   <td>".$items['sku']."</td>
						   <td>".$items['name']."</td>
						   <td>".$result['name']."</td>
						   <td>".$items['qty']."</td>
						   <td>".showText($result['status'])."</td>
						   <td>".$cort_date."</td>
					</tr>";	
			}
			$str.="</tbody></table>";
		}

		if($str){
			$filename = 'DeliveryDetailsReports_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function pick_list_report(){

		$this->document->setTitle('Delivery Summary Report');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']  = $this->language->get('text_tran_no');
		$this->data['text_tran_dt']  = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] 	 = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back']   = $this->language->get('button_back');
		$this->data['token'] 		 = $this->session->data['token'];
		$this->load->model('transaction/delivery');
		$this->load->model('inventory/reports');
		
		$company_id		 = $this->session->data['company_id'];
		$stock_report 	 = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	 = $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_do_number = null;
		if (isset($_REQUEST['filter_do_number'])) {
			$filter_do_number = $_REQUEST['filter_do_number'];
			$url .= '&filter_do_number=' . $filter_do_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_man = null;
		if (isset($_REQUEST['filter_delivery_man'])) {
			$filter_delivery_man = $_REQUEST['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$url .= '&page=1';
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Pick List Report',
			'href'      => $this->url->link('transaction/transaction_reports/pick_list_report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		if($_REQUEST['export'] == 'csv' || $_REQUEST['export'] == 'pdf'){
			$data = array(
				'filter_from_date'	 	=> $filter_from_date,
				'filter_to_date'	 	=> $filter_to_date,
				'filter_delivery_man'   => $filter_delivery_man,			
				'filter_delivery_status'=> $filter_delivery_status,
				'from'					=> 'Picklist',
			);
		} else {
			$data = array(
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_delivery_man'   => $filter_delivery_man,			
			'filter_delivery_status'=> $filter_delivery_status,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit'),
			'from'					=> 'Picklist',
			);
		}
		$this->data['filters'] 	= $data;
		$delivery_total			= $this->model_transaction_delivery->getTotalDeliveryDetails($data);
		$results 				= $this->model_transaction_delivery->getDeliveryDetailsList($data);
		//printArray($results);die;
		if(!empty($results)){
			$csv= '&export=csv';
			$pdf = '&export=pdf';
		} else {
			$csv= '';
			$pdf = '';
		}
		$this->data['export_csv']  = $this->url->link('transaction/transaction_reports/pick_list_report'.$strUrl, 'token='.$this->data['token'].$url.$csv,'SSL');
		$this->data['export_pdf']  = $this->url->link('transaction/transaction_reports/pick_list_report'.$pageUrl, 'token='.$this->data['token'].$url.$pdf,'SSL');
		$this->data['back'] 	   = $this->url->link('report/report'.$pageUrl, 'token=' . $this->data['token'], 'SSL');
		$this->data['salesmanlist']= $this->model_transaction_delivery->getDeliveryMansList();

		foreach ($results as $result) {
			// printArray($result);
			$delivery  = $this->model_transaction_delivery->getDeliveryManDetails($result['deliveryman_id']);
			$cus  = $this->model_transaction_delivery->getCustomerDetails($result['customer_id']);
			$product  = $this->model_transaction_delivery->getproductDetails($result['sku']);

			$result['assignedon'] 	  = date('d/m/Y',strtotime($result['assignedon']));
			if($result['assignedon']  == '01/01/1970'){
				$result['assignedon'] = '-';
			}
			$this->data['delivery'][] = array(
				'invoice_no'          => $result['invoice_no'],
				'do_no' 			  => $result['do_no'],
				'vendor_name' 		  => $cus['name'],
				'from' 				  => $result['froms'],
				'qty' 				  => $result['qty'],
				'sku_name' 			  => $product['name'],
				'sku'				  => $result['sku'],
				'route_code'  		  => $delivery['route_code'],
				'assignedon'  		  => $result['assignedon'],
				'deliveredon'  		  => date('d/m/Y',strtotime($result['deliveredon'])),
				'deliveryman'    	  => $delivery['name'],
				'status'   			  => $result['status'],
			);
		}
		// printArray($this->data['delivery']);die;
		if($_REQUEST['export'] == 'csv' || $_REQUEST['export'] == 'pdf'){
			$products = $this->data['delivery'];
			if($_REQUEST['export'] == 'csv'){
				if(!empty($products)){
			    	ob_end_clean();
			        header( 'Content-Type: text/csv' );
			        header( 'Content-Disposition: attachment;filename=pick_list_report.csv');
			        print "SNo,InvoiceNo,DoNo,From,Customer,SKU,Name,Qty,Route,Date,DeliveryMan\r\n";
					$j=0; 
					$total=0;
					foreach ($products as $delivery) {
						$j++;
						$invoice_no 	= $delivery['invoice_no']; 
						$do_no 			= $delivery['do_no']; 
						$from 			= showText($delivery['from']); 
						$vendor_name 	= $delivery['vendor_name']; 
						$sku 			= $delivery['sku']; 
						$name 			= $delivery['sku_name']; 
						$qty 			= $delivery['qty']; 
						$route_code 	= $delivery['route_code']; 
						$date 			= $delivery['assignedon']; 
						$deliveryman 	= $delivery['deliveryman']; 
						$status  		= $delivery['status'];
						print "$j,\"$invoice_no\",\"$do_no\",\"$from\",\"$vendor_name\",\"$sku\",\"$qty\",\"$qty\",\"$route_code\",\"$date\",\"$deliveryman\"\r\n";
					}
				}die;
			}
			if($_REQUEST['export'] == 'pdf'){
				if(!empty($products)){
			    	$company_id		 = $this->session->data['company_id'];
					$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
					$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
					$str 			 = $headerStr;
					$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Pick List Report</td>
							<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
					/*if($filterStr){
						$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
					}*/
			        $str.="<table class='listing' style='width:100%;line-height: 26px;overflow: wrap;' autosize='1'><thead>
			           		<tr style='font-size:25px;'>
								<td width='5%'>SNo</td>
								<td width='10%'>InvoiceNo</td>
								<td width='7%'>DoNo</td>
								<td width='10%'>From</td>
								<td width='10%'>Customer</td>
								<td width='10%'>SKU</td>
								<td width='13%'>Name</td>
								<td width='5%'>Qty</td>
								<td width='10%'>Route</td>
								<td width='10%'>Date</td>
								<td width='10%'>Delivery</td>								
			           		</tr>
		           		  </thead><tbody>";
					$j=0; 
					foreach ($products as $delivery) {
						$j++;
						$invoice_no 	= $delivery['invoice_no']; 
						$do_no 			= $delivery['do_no']; 
						$from 			= $delivery['from']; 
						$vendor_name 	= $delivery['vendor_name']; 
						$sku 			= $delivery['sku'];
						$name 			= $delivery['sku_name'];  
						$qty 			= $delivery['qty'];  
						$route_code 	= $delivery['route_code']; 
						$date 			= $delivery['assignedon']; 
						$deliveryman 	= $delivery['deliveryman']; 
						$status  		= $delivery['status'];
						$str.="<tr>
								<td>".$j."</td>
						   		<td>".$invoice_no."</td>
						   		<td>".$do_no."</td>
						   		<td>".$from."</td>
						   		<td>".$vendor_name."</td>
						   		<td>".$sku."</td>
						   		<td>".$name."</td>
						   		<td>".$qty."</td>
						   		<td>".$route_code."</td>
						   		<td>".$date."</td>
						   		<td>".$deliveryman."</td>						   	
							</tr>";	
					}
					$str.="</tbody></table>";
					//echo $str; die;
					if($str){
						$filename = 'pick_list_report-'.date('m-Y').'.pdf';
						include(DIR_SERVER.'pdf_print.php');
					}
				}die;
			}
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}

		$pagination 		= new Pagination();
		$pagination->total 	= $delivery_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/pick_list_report', 'token=' . $this->session->data['token'].$url.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order']= $order;

		$this->template = 'transaction/pick_list_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function salesSummaryReportBySkuAndQty(){

		$this->load->model('transaction/sales');
		//language assign code for transaction purchase
		$this->document->setTitle('Sales Summary Report By Sku And Qty');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning']= $this->error['warning'];
		} else {
			$this->data['error_warning']= '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id		= $this->session->data['company_id'];
		$stock_report 	= $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		} 
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} 
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		} 
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		} 
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		} 
		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		} 
        if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$pageUrl.= '&filter_product_id='.$filter_product_id;
		} else {
			$filter_product_id = '';
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.= '&filter_channel='.$filter_channel;
		} else {
			$filter_channel = '';
		}

		$this->data['export_csv'] = $this->url->link('transaction/transaction_reports/salesSummaryReportBySkuAndQty_CSV', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['export_pdf'] = $this->url->link('transaction/transaction_reports/salesSummaryReportBySkuAndQty_PDF', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$data = array(
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			=> $this->config->get('config_admin_limit'),
			'filter_supplier'		=> $filter_supplier,
			'filter_transactionno'	=> $filter_transactionno,
			'filter_product_id'		=> $filter_product_id,
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_location'		=> $filter_location,
			'filter_channel'		=> $filter_channel,
			'filter_department' 	=> $filter_department,			
			'filter_category' 		=> $filter_category,			
			'filter_brand' 			=> $filter_brand,		
			'filter_vendor' 		=> $filter_vendor,			
			'filter_name' 			=> $filter_name			
		);
        // printArray($data);

		$this->data['data'] = $data;
		$data['group_by']   = 1;
		$response 			= $this->model_transaction_sales->getSalesListDetailRe($data);
        $results        	= $response['results'];
        $sale_total         = count($response['total_rows']);

		$this->load->model('user/user');
		$userDetail = array();
		$gst 		= 0;
		$strUrl 	= '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		foreach ($results as $result) {	
			$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
			$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);
			// $net_total += $result['net_total'];
			$this->data['sales'][] = array(
				'location_code'   => $result['location_code'],
				'sku'       	  => $result['sku'],
				'invoice_date'    => '',
				'description'     => $productDetails['sku_description'],
				'name'     		  => $productDetails['name'],
				'qty'             => $result['qty'],
				'department'      => $result['department_name'],
				'category'        => $result['category_name'],
				'brand'			  => $result['sku_brand_code'],
				'vendor'		  => $vendor['vendor_name']				
			);

		}
		$this->data['nettotal'] = number_format($net_total,2);		
		$url.= $pageUrl;

		// Departmnet
		$this->data['channels'] = $this->cart->getChannelsList();         
		$this->load->model('inventory/department');
		$this->data['department_collection']= $this->model_inventory_department->getDepartments();
		// Category
		$this->load->model('inventory/category');			
		$this->data['category_collection'] 	= $this->model_inventory_category->getCategorys();
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] 	= $this->model_master_vendor->getVendors();
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] 	= $this->model_inventory_brand->getBrands();

		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['sort'] = $sort;
		$this->data['order']= $order;
		
		$pagination 		= new Pagination();
		$pagination->total 	= $sale_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/salesSummaryReportBySkuAndQty', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
	
		$this->template = 'transaction/salesSummaryReportBySkuAndQty.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function salesSummaryReportBySkuAndQty_PDF(){

		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department'); 
		$this->load->model('inventory/category');
		$this->load->model('master/vendor'); 

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];			
		}
		if (isset($this->request->get['filter_product_id'])) {
			$filter_product_id = $this->request->get['filter_product_id'];			
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];			
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = '';
		}
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = '';
		}
		if (isset($this->request->get['filter_channel'])) {
			$filter_channel = $this->request->get['filter_channel'];
		} else {
			$filter_channel = '';
		}
		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} 
        if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		}
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_department){
		    $departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
		    $filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_category){
		    $categoryResult = $this->model_inventory_category->getCategoryNameByCode($filter_category);
		    $filterStr.=" Category: ".$categoryResult['category_name'].',';
		}
		if($filter_brand) {
			$filterStr.=" Brand: ".$filter_brand.',';
		}
		if($filter_vendor) {
			$vendorResult = $this->model_master_vendor->getSuppliersDetailsbyCode($filter_vendor);
			$filterStr.=" Vendor: ".$vendorResult['vendor_name'].',';
		}
		if($filter_name) {
			$filterStr.=" SKU: ".$filter_name.',';
		}
		/*if($filter_sku) {
			$filterStr.=" SKU: ".$filter_sku.',';
		}*/
		if($filter_location) {
			$filterStr.=" Location: ".$filter_location.',';
		}
		if($filter_channel) {
            $filter_network_name = $this->model_transaction_sales->getNetworkName($filter_channel);
			$filterStr.=" Sales Channel: ".$filter_network_name.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		
		$filterVal = 1;
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filter_channel'=>$filter_channel,
			'filterVal' =>$filterVal,
			'filter_product_id'=>$filter_product_id,
			'filter_category'=>$filter_category,
			'filter_department'=>$filter_department,
			'filter_brand'=>$filter_brand,
			'filter_vendor'=>$filter_vendor
		);
		$data['group_by'] = 1;
		$results = $this->model_transaction_sales->getSalesListDetailRe($data);
		$results = $results['results'];

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Summary Report By Sku And Qty</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Location</th>
							<th>Department</th>
							<th>Category</th>
							<th>Brand</th>
							<th>Vendor</th>
							<th>Sku</th>
							<th>Name</th>
							<th>Quantity</th>
						</tr></thead> <tbody>"; 

			foreach ($results as $result) {	
					    $productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
						$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);
						$location_code      = $result['location_code'];
						$sku    		    = $result['sku'];
						$invoice_date       = '';
						$description      	= $productDetails['name'];
						$qty                = $result['qty'];
						$department         = $result['department_name'];
						$category           = $result['category_name'];
						$brand				= $result['sku_brand_code'];
						$vendor				= $vendor['vendor_name'];
				$str.="<tr>
				   <td>".$location_code."</td>
				   <td>".$department."</td>
				   <td>".$category."</td>
				   <td>".$brand."</td>
				   <td>".$vendor."</td>
				   <td>".$sku."</td>
				   <td>".$description."</td>
				   <td>".$qty."</td>
			</tr>";	
			}

		$str.="</tbody></table>";
		if($str){
			$filename = 'SalesSummaryReportBySkuAndQty_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}
	public function salesSummaryReportBySkuAndQty_CSV(){

		$this->language->load('transaction/reports');
		$this->load->model('transaction/sales');
		$results =$this->getsalessummaryskuListcsv();

	    $i=0;
	    $PaidAmountTot = 0;
	    $BalanceAmountTot = 0;

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=salesSummaryReportBySkuAndQty.csv');
	        print "S.No,LocationCode,Department,Category,Brand,Vendor,Sku,Name,Quantity\r\n";
	        $this->load->model('user/user');
			
			$j=0;
		foreach ($results as $result) {	
				$productDetails = $this->model_transaction_sales->getproductdetailsBySKU($result['sku']);
				$vendor 		= $this->model_transaction_sales->getVendor($result['sku_vendor_code']);

				$j++;
				$location_code      = $result['location_code'];
				$sku    		    = $result['sku'];
				$invoice_date       = '';
				$description      	= $productDetails['name'];
				$qty                = $result['qty'];
				$department         = $result['department_name'];
				$category           = $result['category_name'];
				$brand				= $result['sku_brand_code'];
				$vendor				= $vendor['vendor_name'];
				print "$j,\"$location_code\",\"$department\",\"$category\",\"$brand\",\"$vendor\",\"$sku\",\"$description\",\"$qty\"\r\n";		
		}
		}  
	}
	public function aging_report(){

		$this->load->model('transaction/sales');
		//language assign code for transaction purchase
		$this->document->setTitle('Aging Report');
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no']   	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_supplier'] 	= $this->language->get('text_supplier');
		$this->data['text_total'] 		= $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning']= $this->error['warning'];
		} else {
			$this->data['error_warning']= '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id		= $this->session->data['company_id'];
		$stock_report 	= $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		} 
		$pageUrl ='';
		
		if ($_REQUEST['filter_date_from']) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if ($_REQUEST['filter_date_to']) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} 
		if (isset($_REQUEST['filter_invoice_no'])) {
			$filter_invoice_no = $_REQUEST['filter_invoice_no'];
			$pageUrl.='&filter_invoice_no='.$filter_invoice_no; 
		} else {
			$filter_invoice_no = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
		}
		
		$this->data['export_csv'] = $this->url->link('transaction/transaction_reports/aging_report_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['export_pdf'] = $this->url->link('transaction/transaction_reports/aging_report_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$url = '';		
		if (isset($this->request->get['page'])) {
			$page     = $this->request->get['page'];
		}
		if (isset($_POST)) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		
		$data = array(
			'start'       			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       			=> $this->config->get('config_admin_limit'),
			'filter_date_from'		=> $filter_date_from,
			'filter_date_to'		=> $filter_date_to,
			'filter_invoice_no'		=> $filter_invoice_no,
			'filter_location'		=> $filter_location
		);

		$this->data['data'] = $data;
		$data['group_by']   = 1;
		$response 			= $this->model_transaction_sales->getAgingReport($data);
        $aging_total        = count($response['total_rows']);
        foreach($response['results'] as $result){
        	$result = $result;
        	$result['pagecnt'] = ($page-1)*$this->config->get('config_admin_limit');
			$this->data['results'][] = $result; 
        }
		$this->load->model('user/user');
		$userDetail = array();
		$gst 		= 0;
		$strUrl 	= '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$url.= $pageUrl;
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$pagination 		= new Pagination();
		$pagination->total 	= $aging_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('transaction/transaction_reports/aging_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
	
		$this->template = 'transaction/aging_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function aging_report_csv(){
		
		$this->load->model('transaction/sales');
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_invoice_no'])) {
			$filter_invoice_no = $_REQUEST['filter_invoice_no'];
		} 
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		}
		$data = array(
			'filter_location'   => $filter_location,
			'filter_invoice_no' => $filter_invoice_no,
			'filter_date_from'  => $filter_date_from,
			'filter_date_to'    => $filter_date_to
		);

		$results= $this->model_transaction_sales->getAgingReportForExport($data);
	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=AgingReport.csv');
	        print "S.No,Invoice No,Invoice Date,Location Code,Customer,Amount, Paid, Balance\r\n";
			$j=0;

			foreach ($results as $result) {	
				$j++;
				$invoice_no       = $result['invoice_no'];
				$invoice_date     = date('d/m/Y',strtotime($result['invoice_date']));
				$location_code    = $result['location_code'];
				$cust_name        = $result['cust_name'];
				$balance 	      = number_format($result['net_total'] - $result['paid_amount'],2);
				$net_total        = number_format($result['net_total'],2);
				$paid_amount      = number_format($result['paid_amount'],2);
				print "$j,\"$invoice_no\",\"$invoice_date\",\"$location_code\",\"$cust_name\",\"$net_total\",\"$paid_amount\",\"$balance\"\r\n";
			}
		}
	}
	public function aging_report_pdf(){
		
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_invoice_no'])) {
			$filter_invoice_no = $_REQUEST['filter_invoice_no'];
		} 
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
		}
		$data = array(
			'filter_location'   => $filter_location,
			'filter_invoice_no' => $filter_invoice_no,
			'filter_date_from'  => $filter_date_from,
			'filter_date_to'    => $filter_date_to
		);

		$results= $this->model_transaction_sales->getAgingReportForExport($data);
	    if(!empty($results)){
	    	$company_id		= $this->session->data['company_id'];
			$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr		= $this->url->getCompanyAddressHeaderString($company_details);
			$str 			= $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Aging Report</td>
					<td align="right">Time: '.date('d/m/Y H:i').'</td></tr></table>';
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>SNo</th>
							<th>Invoice No</th>
							<th>Invoice Date</th>
							<th>Location</th>
							<th>Customer</th>
							<th>Amount</th>
							<th>Paid</th>
							<th>Balance</th>
						</tr></thead> <tbody>"; 

			foreach ($results as $result) {
				$j++;
				$invoice_no       = $result['invoice_no'];
				$invoice_date     = date('d/m/Y',strtotime($result['invoice_date']));
				$location_code    = $result['location_code'];
				$cust_name        = $result['cust_name'];
				$balance 	      = number_format($result['net_total'] - $result['paid_amount'],2);
				$net_total        = number_format($result['net_total'],2);
				$paid_amount      = number_format($result['paid_amount'],2);

				$str.="<tr>
				   <td>".$j."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td>".$location_code."</td>
				   <td>".$cust_name."</td>
				   <td  align='right'>".$net_total."</td>
				   <td  align='right'>".$paid_amount."</td>
				   <td  align='right'>".$balance."</td>
			</tr>";	
			}
		$str.="</tbody></table>";
			if($str){
				$filename = 'AgingReport.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
		}
	}

	public function sales_return_summary() {		
		//language assign code for transaction purchase
		$this->load->model('transaction/sales');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		if($_REQUEST['filter_date_from']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_from'] = date('d/m/Y');
		}
		if($_REQUEST['filter_date_to']=='' && !isset($_REQUEST['filter_transactionno'])){
			$_REQUEST['filter_date_to'] = date('d/m/Y');	
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location; 
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}

		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Return Summary Reports',
			'href'      => $this->url->link('transaction/transaction_reports/sales_return_summary', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page = 1;	
		}
		$data = array(
			'start'       		=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'            	=> $this->config->get('config_admin_limit'),
			'filter_supplier'	=> $filter_supplier,
			'filter_transactionno'=> $filter_transactionno,
			'filter_channel' 	=> $filter_channel,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location' 	=> $filter_location
		);
		$this->data['data'] = $data;
		$sale_totalAry = $this->model_transaction_sales->getTotalSalesReturn($data);
		$this->data['sales_net_total'] = $sale_totalAry['net_total'];
		$sale_total    = $sale_totalAry['totalSale'];
		$results = $this->model_transaction_sales->getSalesReturnList($data);
		// printArray($$userDetail);die;
		$this->load->model('setting/customers');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['printAction'] = $this->url->link('transaction/transaction_reports/salessummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salessummary&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		foreach ($results as $result) {	
			$userDetail = $this->model_setting_customers->getCustomer($result['customer_code']);			
			// printArray($userDetail);die;
			$this->data['sales'][] = array(
				'location_code'       => $result['location_code'],
				'invoice_no'          => $result['invoice_no'],
				'invoice_date'        => date('d/m/Y',strtotime($result['invoice_date'])),
				'sub_total'  		  => $result['sub_total'],
				'sku'  		  		  => $result['sku'],
				'discount' 			  => $result['discount'],
				'gst'       		  => $result['gst'],
				'actual_total'        => $result['actual_total'],
				'round_off'           => $result['round_off'],
				'net_total'			  => $result['net_total'],
				'customer_name' 	  => $userDetail['name'],
				'created_date'        => $result['createdon'],
				'qty'            	  => $result['qty'],
				'sku_price'           => $result['sku_price'],
				'sub_total'           => $result['sub_total'],
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		if($_POST){
			$_REQUEST['page'] = 1;
			$page = 1;
		}
		$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);
		$this->data['Tolocations'] 	 	   = $this->cart->getLocation();

		$pagination = new Pagination();
		$pagination->total = $sale_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('transaction/transaction_reports/sales_return_summary', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] 	= $pagination->render();
		$this->data['sort'] 		= $sort;
		$this->data['order'] 		= $order;
		$this->data['channels'] 		= $this->cart->getChannelsList();
		$this->data['Tolocations'] 		= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['link_pdfexport'] 	= $this->url->link('transaction/transaction_reports/export_sales_return_summary_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_csvexport'] 	= $this->url->link('transaction/transaction_reports/export_sales_return_summary_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'transaction/sales_return_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_sales_return_summary_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_channel'=>$filter_channel,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesReturnListPdf($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=sales_return_summary.csv');
	        print "S.No,Transaction No,Transaction Date,SKU,Customer,Quality,Price,Total\r\n";
			$j=0; 
			$total=0;
			foreach ($results as $result) {	
						$userDetail = $this->model_setting_customers->getCustomer($result['customer_code']);	
						$j++;
						$location_code  = $result['location_code'];
						$invoice_no     = $result['invoice_no'];
						$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
						$sub_total  	= $result['sub_total'];
						$discount  		= $result['discount'];
						$gst       		= $result['gst'];
						$actual_total   = $result['actual_total'];
						$qty   = $result['qty'];
						$sku   = $result['sku'];
						$sku_price      = $result['sku_price'];
						$sub_total		= $result['sub_total'];
						$customer_name  = $userDetail['name'];
						$total+=$sub_total;
		
				print "$j,\"$invoice_no\",\"$invoice_date\",\"$sku\",\"$customer_name\",\"$qty\",\"$sku_price\",\"$sub_total\"\r\n";
			}
			print "\"\",\"\",\"\",\"\",\"\",\"\",Total,\"$total\"\r\n";
		}  	 
		
	}


	public function export_sales_return_summary_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/sales');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('setting/customers');
		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location; 
		} else {
			$filter_location = null;
		}
		if (isset($_REQUEST['filter_channel'])) {
			$filter_channel = $_REQUEST['filter_channel'];
			$pageUrl.='&filter_channel='.$filter_channel; 
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_channel'=>$filter_channel,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location' => trim($filter_location)
		);
		$results = $this->model_transaction_sales->getSalesReturnListPdf($data);

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		$this->data['filter_channel_name'] = $this->model_transaction_sales->getNetworkName($filter_channel);		
		if($filter_channel) {
			$filterStr.=" Sales Channel : ".$this->data['filter_channel_name'].',';
		}
		if($filter_location) {
			$filterStr.=" Location : ".$filter_location.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction no : ".$filter_transactionno.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Return Summary Reports</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<td>Transaction No</td>
			<td>Transaction Date</td>
			<td>SKU</td>
			<td>Customer</td>
			<td>Quality</td>
			<td>Price</td>
			<td>Total</td>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$userDetail = $this->model_setting_customers->getCustomer($result['customer_code']);
			$j++;
			$location_code  = $result['location_code'];
			$invoice_no     = $result['invoice_no'];
			$invoice_date   = date('d/m/Y',strtotime($result['invoice_date']));
			$sub_total  	= $result['sub_total'];
			$discount  		= $result['discount'];
			$gst       		= $result['gst'];
			$actual_total   = $result['actual_total'];
			$round_off      = $result['round_off'];
			$net_total		= $result['net_total'];
			$qty   = $result['qty'];
			$sku   = $result['sku'];
			$sku_price      = $result['sku_price'];
			$sub_total		= $result['sub_total'];
			$customer_name  = $userDetail['name'];
			
			$total_Stock_Value+=$sub_total;

			$str.="<tr><td>".$j."</td>
				   <td>".$invoice_no."</td>
				   <td>".$invoice_date."</td>
				   <td>".$sku."</td>
				   <td>".$customer_name."</td>
				   <td class='right'>".$qty."</td>
				   <td class='right'>".$sku_price."</td>
				   <td class='right'>".$sub_total."</td>
			</tr>";	
		}
		$str.="<tr><td colspan='7' align='right'>Total</td><td align='right'>".$total_Stock_Value."</td>
		</tr></tbody></table>";
		if($str){
			$filename = 'sales_return_summary_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
}
?>