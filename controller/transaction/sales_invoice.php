<?php
class ControllerTransactionSalesInvoice extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('transaction/purchase');
		$this->document->setTitle('Sales Invoice List');
		$this->load->model('transaction/sales');
		$this->clearPurchaseData();
		$this->clearTempFolder();
		$this->getList();
	}
	public function AjaxupdateRowData(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		$this->response->setOutput(json_encode($this->data));
	}
	public function AjaxRemoveProduct(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/sales');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
			}
   			$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			if(count($data['products'])==0){
				$this->clearPurchaseData($remove = 'discount');
			}
			$this->load->model('ajax/cart');
			$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
				if($tax['text']){
					$res['tax'] =$tax['text'];
					$res['tax_value'] =$tax['value'];
				}else{
					$res['tax'] = 0;
					$res['tax_value'] = 0;
				}
			}
			$this->response->setOutput(json_encode($res));
		}
	}
	public function AjaxBillDiscountUpdate(){
		$cart_type = 'cart_purchase';
		$this->load->model('transaction/sales');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price		= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}else if($bill_discount_percentage =='' || $bill_discount_percentage=='0' || $bill_discount_price=='0' || $bill_discount_price==''){
				if ($this->session->data['bill_discount']) {
					unset($this->session->data['bill_discount']);
					unset($this->session->data['bill_discount_mode']);
				}
			}
		}
		$apply_tax_type = $this->request->post['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
			}
			$data['products'] = $product_data;
			$this->load->model('ajax/cart');
			$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		    foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
					// $res['discount_price']  	 = $bill_discount_price;
					// $res['discount_percentage']  = $bill_discount_percentage;

					$lable = 'Discount($)';
					if($bill_discount_percentage){
						$lable = 'Discount(%)';
					}
					if($this->request->post['discount']=='')
							$res['discountRow'] = '<tr id ="TRdiscount"><td colspan="7" align="right" class="purchase_total_left">'.$lable.'</td>
							<td class="purchase_total_right insertproduct">
							<input type="text" name="discount" id="discount" value="'.$res['discount'].'" class="textbox row_total_box text-right" readonly="readonly" /></td></tr>';
					}
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				$res['bill_discount']     = $this->session->data['bill_discount'];
				$res['bill_discount_mode']= $this->session->data['bill_discount_mode'];
			}
			$this->response->setOutput(json_encode($res));
	}
	
	public function ajaxaddproducts(){
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
			$cart_type = 'cart_purchase';
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(!empty($this->session->data['cart_purchase'])){
				   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
					   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
				   } elseif ($productQty > 0) {
				       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
				   }
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		if($apply_tax_type=='0' || $apply_tax_type==''){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}
		$cartPurchaseDetails	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		if (!empty($cartPurchaseDetails)) {
				$str ='';
				foreach ($cartPurchaseDetails as $purchase) {
					$pid = $purchase['product_id'];
				    if($apply_tax_type=='2') {
					    $totalValue = $purchase['total'];
					} else {
					    $totalValue = $purchase['tax'] + $purchase['total'];
					}
				$str.='<tr id="purchase_'.$purchase['product_id'].'">
					  <td class="center">'.$purchase['code'].'</td>
	                   <td class="center">'.$purchase['name'].'</td>
	                   <td class="center order-nopadding"><input type="text" id="qty_'.$purchase['product_id'].'" value="'.$purchase['quantity'].'" class="textbox small_box update_qty" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
	                   <td class="text-right order-nopadding">
	                   	<input type="text" id="price_'.$purchase['product_id'].'"  value="'.str_replace("$","",$purchase['price']).'" class="textbox small_box update_price" data-sku="'.$purchase['product_id'].'" onblur="update_qty_price('.$purchase['product_id'].');">
	                   </td>
		               <td class="text-right">'.$purchase['raw_cost'].'</td>';
					$str.='<td class="text-right" id="td_nettot_'.$purchase['product_id'].'">'.$purchase['sub_total'].'</td>
					   
					   <td class="center"><a onclick="removeProductData('.$pid.');"> <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="Delete" style="font-size: 16px;"></i></a></td>
                    </tr>';
                }
                $res['tbl_data'] = $str;
				$this->load->model('ajax/cart');
   			    $apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
			    $total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			    foreach($total_data as $totals) {
					if($totals['code']=='total') {
						$total	= $totals['value'];
					}
					if($totals['code']=='tax'){
						$tax = $totals;
					}
				}
				$data['totals'] = $total_data;
				$data['total'] = $total;
				if(count($data['totals'])>=1){
					$res['sub_total'] 		= $data['totals'][0]['text'];
					$res['sub_total_value'] = $data['totals'][0]['value'];
					$cnt = count($data['totals'])-1;
					$res['total'] 			= $data['totals'][$cnt]['text'];
					$res['total_value'] 	= $data['totals'][$cnt]['value'];
					if($data['totals'][1]['code']=='discount'){
						$res['discount'] 		= $data['totals'][1]['text'];
						$res['discount_value']  = $data['totals'][1]['value'];
					}else{
						$res['discount'] 		= 0;
					}
					if($tax['text']){
						$res['tax'] =$tax['text'];
						$res['tax_value'] =$tax['value'];
						$res['tax_str'] = '<tr id="TRtax">
                                                    <td align="right" class="purchase_total_left" colspan="7">GST (7%) (Excl)</td>
	                                                <td class="purchase_total_right insertproduct">
	                                                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$tax['text'].'" id="tax" name="tax" style="text-align:right;">
	                                                </td>
                                                </tr>';
					}else{
						$res['tax'] = 0;
						$res['tax_value'] = 0;
					}
				}
				$this->response->setOutput(json_encode($res));
         }
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('transaction/sales');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$this->data['transaction_date'] = $this->request->post['transactiondate'];
			$this->data['customer_code'] = $this->request->post['customercode'];
			$_productDetails           = $this->model_transaction_sales->getProductByNamenew($this->data);

			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
				//$str = $var;
			}
			echo $str;
	}
	public function getProductDetailsbyCustomer($data='', $type = '') {
			$this->load->model('transaction/sales');
			$this->data['sku'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$this->data['transaction_date'] = $this->request->post['transactiondate'];
			$this->data['customer_code'] = $this->request->post['customercode'];
			$CustomerDetails           = $this->model_transaction_sales->getcustomerPrice($this->data);
			for($i=0;$i<count($CustomerDetails);$i++){
			$values =  $CustomerDetails[$i]['sku'].'||'.$CustomerDetails[$i]['customer_price'];
		    }
		    echo $values;
	}
	public function insert() {
        $cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('transaction/purchase');
         $type='insert';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit' ) && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}

			$data['sales_trans_no']	= '';
			if(isset($this->request->post['sales_trans_no'])) {
				$data['sales_trans_no']	=  $this->request->post['sales_trans_no'];
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(!empty($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$company_id	= $this->session->data['company_id'];
				$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
				$data['location_code']	= $company_details['location_code'];
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}

			if (isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id'] = $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id'] = '';
			}
			if (isset($this->request->post['tax_type'])) {
				$data['tax_type'] = $this->request->post['tax_type'];
			} else {
				$data['tax_type'] = '';
			}
			if (isset($this->request->post['currency_code'])) {
				$data['currency_code'] = $this->request->post['currency_code'];
			} else {
				$data['currency_code'] = '';
			}
			if (isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate'] = $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate'] = '';
			}
			if (isset($this->request->post['term_id'])) {
				$data['term_id'] = $this->request->post['term_id'];
			} else {
				$data['term_id'] = '';
			}
			if (isset($this->request->post['shipping_id'])) {
				$data['shipping_id'] = $this->request->post['shipping_id'];
			} else {
				$data['shipping_id'] = '';
			}
			if (isset($this->request->post['network_id'])) {
				$data['network_id'] = $this->request->post['network_id'];
			} else {
				$data['network_id'] = '';
			}
			if (isset($this->request->post['network_order_id'])) {
				$data['network_order_id'] = $this->request->post['network_order_id'];
			} else {
				$data['network_order_id'] = '';
			}
			if (isset($this->request->post['delivery_remarks'])) {
				$data['delivery_remarks'] = $this->request->post['delivery_remarks'];
			} else {
				$data['delivery_remarks'] = '';
			}
			if (isset($this->request->post['discount_remarks'])) {
				$data['discount_remarks'] = $this->request->post['discount_remarks'];
			} else {
				$data['discount_remarks'] = '';
			}
			if (isset($this->request->post['delivery_to_date'])) {
				$data['delivery_to_date'] = $this->request->post['delivery_to_date'];
			} else {
				$data['delivery_to_date'] = '';
			}
			if (isset($this->request->post['sales_man'])) {
				$data['sales_man'] = $this->request->post['sales_man'];
			} else {
				$data['sales_man'] = '';
			}
			$data['istagged'] = 0;
			if (isset($this->request->post['istagged'])) {
				$data['istagged'] = $this->request->post['istagged'];
			}
			
			$product_data = array();
   			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $data['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'             => $product['product_id'],
					'name'                   => $product['name'],
					'price'                  => $product['price'],
					'quantity'               => $product['quantity'],
					'sku'                    => $product['sku'],
					'hasChild'               => $product['hasChild'],
					'sku_price'              => $product['sku_price'],
					'sku_cost'               => $product['sku_cost'],
					'sku_avg'                => $product['sku_avg'],
					'raw_cost'               => $product['raw_cost'],
					'purchase_discount'      => $product['purchase_discount'],
					'purchase_discount_mode' => $product['purchase_discount_mode'],
					'purchase_discount_value'=> $product['purchase_discount_value'],
					'tax_class_id'           => $product['tax_class_id'],
					'weight_class_id'        => $product['weight_class_id'],
					'net_price'              => $product['sub_total'],
					'tax_price'              => $product['purchase_tax'],
					'description'            => $product['description'],
					'total'                  => $product['total']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = '';
			$this->load->model('ajax/cart');
			$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total']  = $total;
			$costMethod     = $this->config->get('config_average_cost_method');
			// printArray($data); die;
			$data['transaction_no']	= $this->getTransNo();
			$data['do_no']          = $this->getDoNo(); //for auto assign to driver
			$invoice_no = $this->model_transaction_sales->addSalesInvoice($data,$costMethod);
			$this->sendPdf_toCustomer($invoice_no);
			$this->session->data['success'] = 'Success: You have added sales invoice!';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->clearPurchaseData();
		$this->getForm();
	}
	public function update() {
		$cart_type = 'cart_purchase';
		$this->language->load('transaction/purchase');
		$this->document->setTitle($this->language->get('Sales'));
		$this->load->model('transaction/sales');
		$this->load->model('transaction/purchase');
		$type = 'update';

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit') && $this->validateForm($type,$this->request->get['purchase_id'])) {
			$data = array();
			if ($this->request->post['purchase_button'] == 'hold') {
			    $data['hold'] = 1;
			} else {
			    $data['hold'] = 0;
			}
			if(isset($this->request->get['purchase_id'])) {
				$data['purchase_id']	=  $this->request->get['purchase_id'];
			} else {
				$data['purchase_id']	= '';
			}
			if(isset($this->request->post['location_code'])) {
				$data['location_code']	=  $this->request->post['location_code'];
			} else {
				$data['location_code']	= '';
			}
			if(isset($this->request->post['transaction_no'])) {
				$data['transaction_no']	=  $this->request->post['transaction_no'];
			} else {
				$data['transaction_no']	= '';
			}
			if(isset($this->request->post['transaction_date'])) {
				$data['transaction_date']	=  $this->request->post['transaction_date'];
			} else {
				$data['transaction_date']	= '';
			}
			if(isset($this->request->post['vendor'])) {
				$data['vendor']	=  $this->request->post['vendor'];
			} else {
				$data['vendor']	= '';
			}
			if(isset($this->request->post['reference_no'])) {
				$data['reference_no']	=  $this->request->post['reference_no'];
			} else {
				$data['reference_no']	= '';
			}
			if(isset($this->request->post['reference_date'])) {
				$data['reference_date']	=  $this->request->post['reference_date'];
			} else {
				$data['reference_date']	= '';
			}
			if(isset($this->request->post['remarks'])) {
				$data['remarks']	=  $this->request->post['remarks'];
			} else {
				$data['remarks']	= '';
			}
			if(isset($this->request->post['bill_discount_percentage'])) {
				$data['bill_discount_percentage']	=  $this->request->post['bill_discount_percentage'];
			} else {
				$data['bill_discount_percentage']	= '';
			}
			if(isset($this->request->post['bill_discount_price'])) {
				$data['bill_discount_price']	=  $this->request->post['bill_discount_price'];
			} else {
				$data['bill_discount_price']	= '';
			}
			if(isset($this->request->post['currency_code'])) {
				$data['currency_code']	=  $this->request->post['currency_code'];
			} else {
				$data['currency_code']	= '';
			}
			if(isset($this->request->post['conversion_rate'])) {
				$data['conversion_rate']	=  $this->request->post['conversion_rate'];
			} else {
				$data['conversion_rate']	= '';
			}
			if(isset($this->request->post['term_id'])) {
				$data['term_id']	=  $this->request->post['term_id'];
			} else {
				$data['term_id']	= '';
			}
			if(isset($this->request->post['shipping_id'])) {
				$data['shipping_id']	=  $this->request->post['shipping_id'];
			} else {
				$data['shipping_id']	= '';
			}
			if(isset($this->request->post['status'])) {
				$data['status']	=  $this->request->post['status'];
			} else {
				$data['status']	= '';
			}		
			if(isset($this->request->post['tax_class_id'])) {
				$data['tax_class_id']	=  $this->request->post['tax_class_id'];
			} else {
				$data['tax_class_id']	= '';
			}
			if(isset($this->request->post['tax_type'])) {
				$data['tax_type']	= $this->request->post['tax_type'];
				$data['tax_type']   = $data['tax_type'] =='1' ? '0' : '1';
			} else {
				$data['tax_type']	= '';
			}
			if (isset($this->request->post['delivery_remarks'])) {
				$data['delivery_remarks'] = $this->request->post['delivery_remarks'];
			} else {
				$data['delivery_remarks'] = '';
			}
			if (isset($this->request->post['discount_remarks'])) {
				$data['discount_remarks'] = $this->request->post['discount_remarks'];
			} else {
				$data['discount_remarks'] = '';
			}
			if (isset($this->request->post['delivery_to_date'])) {
				$data['delivery_to_date'] = $this->request->post['delivery_to_date'];
			} else {
				$data['delivery_to_date'] = '';
			}

			$product_data = array();
   			$apply_tax_type = $data['tax_class_id'] == '1' ? '0' : $this->request->post['tax_type'];
			foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
				$product_data[] = array(
					'product_id'                => $product['product_id'],
					'name'                      => $product['name'],
					'quantity'                  => $product['quantity'],
					'price'                     => $product['price'],
					'sku'                       => $product['sku'],
					'hasChild'                  => $product['hasChild'],
					'sku_price'                 => $product['sku_price'],
					'sku_cost'                  => $product['sku_cost'],
					'sku_avg'                   => $product['sku_avg'],
					'raw_cost'                  => $product['raw_cost'],
					'purchase_discount'         => $product['purchase_discount'],
					'purchase_discount_mode'    => $product['purchase_discount_mode'],
					'purchase_discount_value'   => $product['purchase_discount_value'],
					'tax_class_id'              => $product['tax_class_id'],
					'weight_class_id'           => $product['weight_class_id'],
					'net_price'                 => $product['sub_total'],
					'tax_price'                 => $product['purchase_tax'],
					'total'                     => $product['total']
				);
				$totaltax +=$product['purchase_discount'];
			}
			$data['products'] = $product_data;
			$data['total_tax_value'] = $totaltax;
			$total_data = array();
			$this->load->model('ajax/cart');
			$total_data		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					$total	= $totals['value'];
				}
			}
			$data['totals'] = $total_data;
			$data['total'] = $total;

			$this->model_transaction_sales->editSalesInvoice($data['purchase_id'], $data);
			$this->session->data['success'] = 'Success: You have modified sales invoice';
			$this->clearPurchaseData();
			$url = '';
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if (isset($_REQUEST['filter_from_date'])) {
				$filter_from_date = $_REQUEST['filter_from_date'];
				$url .= '&filter_from_date=' . $filter_from_date;
			}else{
				$filter_from_date = date('d/m/Y');
			}
			$filter_to_date = date('d/m/Y');
			if (isset($_REQUEST['filter_to_date'])) {
				$filter_to_date = $_REQUEST['filter_to_date'];
				$url .= '&filter_to_date=' . $filter_to_date;
			}
			$filter_location = $this->session->data['location_code'];
			if (isset($_REQUEST['filter_location'])) {
				$filter_location = $_REQUEST['filter_location'];
				$url .= '&filter_location=' . $filter_location;
			}
			$filter_transaction = null;
			if (isset($_REQUEST['filter_transaction'])) {
				$filter_transaction = $_REQUEST['filter_transaction'];
				$url .= '&filter_transaction=' . $filter_transaction;
			}
			$filter_delivery_status = array();
			if (!empty($this->request->get['filter_delivery_status'])) {
				$filter_delivery_status = $this->request->get['filter_delivery_status'];
			}
			if(!empty($filter_delivery_status)){
				foreach($filter_delivery_status as $value) {
					$url .= '&filter_delivery_status[]=' . $value;
				}
			}
			$filter_network = array();
			if (!empty($this->request->get['filter_network'])) {
				$filter_network = $this->request->get['filter_network'];
			}
			if(!empty($filter_network)){
				foreach($filter_network as $value) {
					$url .= '&filter_network[]=' . $value;
				}
			}
			$filter_payment_status = array();
			if (!empty($this->request->get['filter_payment_status'])) {
				$filter_payment_status = $this->request->get['filter_payment_status'];
			}
			if(!empty($filter_payment_status)){
				foreach($filter_payment_status as $value) {
					$url .= '&filter_payment_status[]=' . $value;
				}
			}
			$filter_xero = '0';
			if (isset($_REQUEST['filter_xero'])) {
				$filter_xero = $_REQUEST['filter_xero'];
				$url .= '&filter_xero=' . $filter_xero;
			}
			$filter_agent = '';
			if (isset($_REQUEST['filter_agent'])) {
				$filter_agent = $_REQUEST['filter_agent'];
				$url .= '&filter_agent=' . $filter_agent;
			}
			$filter_customer = null;
			if (isset($_REQUEST['filter_customer'])) {
				$filter_customer = $_REQUEST['filter_customer'];
				$url .= '&filter_customer=' . $filter_customer;
			}		
			$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	protected function getList() {
		//language assign code for transaction purchase
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('Sales');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$filter_customer_name = null;
		if (isset($this->request->post['filter_customer_name'])) {
			$filter_customer_name = $this->request->post['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}else if(isset($this->request->get['filter_customer_name'])){
			$filter_customer_name = $this->request->get['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($this->request->post['filter_contact_number'])) {
			$filter_contact_number = $this->request->post['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}else if(isset($this->request->get['filter_contact_number'])){
			$filter_contact_number = $this->request->get['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($this->request->post['filter_order_number'])) {
			$filter_order_number = $this->request->post['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}else if(isset($this->request->get['filter_order_number'])){
			$filter_order_number = $this->request->get['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($this->request->post['filter_transaction'])) {
			$filter_transaction = $this->request->post['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}else if(isset($this->request->get['filter_transaction'])){
			$filter_transaction = $this->request->get['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($this->request->post['filter_sales_person'])) {
			$filter_sales_person = $this->request->post['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}else if(isset($this->request->get['filter_sales_person'])){
			$filter_sales_person = $this->request->get['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		$filter_is_delivery = null;
		if (isset($this->request->post['filter_is_delivery'])) {
			$filter_is_delivery = $this->request->post['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}else if (isset($this->request->get['filter_is_delivery'])) {
			$filter_is_delivery = $this->request->get['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		// echo $url; die;
		if (isset($this->request->post['filter_from_date'])) {
			$filter_from_date = $this->request->post['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime(DEFAULT_FROM_DATE));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->post['filter_to_date'])) {
			$filter_to_date = $this->request->post['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}else if(isset($this->request->get['filter_to_date'])){
			$filter_to_date = $this->request->get['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($this->request->post['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->post['filter_delivery_status'];
		}else if (isset($this->request->get['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->get['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->post['filter_payment_status'])) {
			$filter_payment_status = $this->request->post['filter_payment_status'];
		}else if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($this->request->post['filter_xero'])) {
			$filter_xero = $this->request->post['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}else if (isset($this->request->get['filter_xero'])) {
			$filter_xero = $this->request->get['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		$url .= '&filter_location=' . $filter_location;
		
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}
		else{
			$networks = $this->cart->getChannelsList();
			foreach ($networks as $network) {
				$filter_network[] = $network['id'];
				$url .= '&filter_network[]='.$network['id'];
			}
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		}
		if (isset($this->request->post['page'])) {
			$page = '1';
		}
        if(!$page){
			$page = 1;
		}
		$url .='&page='.$page; 

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice List',
			'href'      => $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['insert'] 	= $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['back_button'] = $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['url'] 		= $url;
		$this->data['export_pdf'] = $this->url->link('transaction/sales_invoice/exportPdf_SalesInvoiceList', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['purchases'] = array();
		$data = array(
			'filter_customer_name'	=> $filter_customer_name,
			'filter_contact_number'	=> $filter_contact_number,
			'filter_transaction' 	=> $filter_transaction,
			'filter_order_number' 	=> $filter_order_number,
			'filter_sales_person'   => $filter_sales_person,			
			'filter_network'  	 	=> $filter_network,			
			'filter_location'	 	=> $filter_location,
			'filter_is_delivery'	=> $filter_is_delivery,
			'filter_from_date'	 	=> $filter_from_date,
			'filter_to_date'	 	=> $filter_to_date,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_payment_status'	=> $filter_payment_status,
			'filter_xero'           => $filter_xero,
			'filter_product'		=> $filter_product,
			'filter_product_id'		=> $filter_product_id,
			'page'	  		     	=> $page,
			'start'       		 	=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       		 	=> $this->config->get('config_admin_limit')
		);
		// printArray($data);
		$this->data['filters']   = $data;
		$sales_total 			 = $this->model_transaction_sales->getTotalSalesInvoice($data);
		$results     			 = $this->model_transaction_sales->getSalesInvoiceList($data);
		$this->data['networks']  = $this->cart->getChannelsList();
		$this->data['customers'] = $this->model_transaction_sales->getB2BCustomers();

		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$userDetail = array();

		foreach ($results as $result) {
			
			$userDetail   = $this->model_user_user->getUser($result['createdby']);
			$signature    = '';
			$network      = $this->model_transaction_sales->getNetworkName($result['network_id']);
			$result['balance_amount'] = $result['net_total'] - $result['paid_amount'];
            $purchase_id  = '';
            if($result['istagged']){
			     $purchase_id = $this->model_transaction_sales->getTaggedPurchaseId($result['sales_trans_no']);
            }
            $xero_post = true;
            if($result['network_id'] != '0'){
            	$xero_post = $this->cart->checkNetworksXero($result['network_id']);  //check postable or not
            }
			$this->data['purchases'][] = array(
				'pagecnt'           => ($page-1) * $this->config->get('config_admin_limit'),
				'invoice_no'        => $result['invoice_no'],
				'order_id'          => $result['id'],
				'transaction_no'    => $result['invoice_no'],
				'xero_sales_id' 	=> $result['xero_sales_id'],
				'transaction_date'  => date('d/m/Y',strtotime($result['invoice_date'])),
				'transaction_type'  => $result['discount_type'],
				'delivery_status'   => $result['delivery_status'],
				'payment_status'    => $result['payment_status'],
				'paid_amount'       => $result['paid_amount'].' / '. $result['balance_amount'],
				'status'            => $result['status'],
				'network_order_id'  => $result['network_order_id']!=''?$result['network_order_id']: '-',
				'network'  	        => $network,
				'vendor_code'       => $result['customer_code'],
				'location'          => $result['location_code'],
				'location_name'     => $result['location_name'],
				'vendor_name'       => $result['cust_name'],
				'xero_cust_id'      => $result['xero_cust_id'],
				'istagged'     		=> $result['istagged'],
				'xero_post'   		=> $xero_post,
				'total'             => $result['net_total'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date('d-m-Y g:i A', $result['createdon']),
				'view_button' 		=> $this->url->link('transaction/sales_invoice/view', 'token=' . $this->session->data['token'].'&purchase_id='.$result['invoice_no'] . $url, 'SSL'),
				'multiple_do' 		=> $this->url->link('transaction/sales_invoice/multiple_do', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'payment_view_button'=> $this->url->link('transaction/sales_invoice/payment_view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'modify_button' 	=> $this->url->link('transaction/sales_invoice/update', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'download_button'	=> $this->url->link('transaction/sales_invoice/download_pdf', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'payment_url'		=> $this->url->link('transaction/sales_invoice/payment_link', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'purchase_order_id' => $this->url->link('transaction/purchase/view', 'token=' . $this->session->data['token'] . '&purchase_id='.$purchase_id, 'SSL')
			);
		}

		$this->data['delete']= $this->url->link('transaction/sales_invoice/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		$this->data['insert']= $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		$this->data['users'] = $this->model_user_user->getUserDetails();
		$this->data['sales_persons'] = $this->model_transaction_sales->getSalesMansList();
		$this->data['Tolocations']   = $this->cart->getLocation($this->session->data['location_code']);
		$pagination          = new Pagination();
		$pagination->total   = $sales_total;
		$pagination->page    = $page;
		$pagination->limit   = $this->config->get('config_admin_limit');
		$pagination->text    = $this->language->get('text_pagination');
		$pagination->url     = $this->url->link('transaction/sales_invoice','token='.$this->session->data['token'].$url.'&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'transaction/sales_invoice_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {

 		$cart_type = 'cart_purchase';
		$this->load->model('master/vendor');
		$this->load->model('inventory/inventory');
		$this->load->model('setting/customers');
		$this->load->model('transaction/sales');
		$this->load->model('setting/location');
		$this->load->model('setting/location');

		$this->data['heading_title'] 	   = $this->language->get('Sales');
		$this->data['entry_tran_no'] 	   = $this->language->get('entry_tran_no');
		$this->data['entry_tran_dt'] 	   = $this->language->get('entry_tran_dt');
		$this->data['entry_tran_type'] 	   = $this->language->get('entry_tran_type');
		$this->data['entry_vendor_code']   = $this->language->get('entry_customer_code');
		$this->data['entry_vendor_name']   = $this->language->get('entry_customer_name');
		$this->data['entry_refno'] 		   = $this->language->get('entry_refno');
		$this->data['entry_refdate'] 	   = $this->language->get('entry_refdate');
		$this->data['text_inventory_code'] = $this->language->get('text_inventory_code');
		$this->data['text_product_name']   = $this->language->get('text_product_name');
		$this->data['text_qty'] 		   = $this->language->get('text_qty');
		$this->data['text_price'] 		   = $this->language->get('text_price');
		$this->data['text_raw_cost'] 	  = $this->language->get('text_raw_cost');
		$this->data['text_disc_perc'] 	  = $this->language->get('text_disc_perc');
		$this->data['text_disc_price'] 	  = $this->language->get('text_disc_price');
		$this->data['text_net_price'] 	  = $this->language->get('text_net_price');
		$this->data['text_total'] 		  = $this->language->get('text_total');
		$this->data['text_tax'] 		  = $this->language->get('text_tax');
		$this->data['text_weight_class']  = $this->language->get('text_weight_class');
		$this->data['text_tax_class'] 	  = $this->language->get('text_tax_class');
		$this->data['text_no_data'] 	  = $this->language->get('text_no_data');
		$this->data['entry_vendor'] 	  = $this->language->get('entry_vendor');
        $this->data['entry_location'] 	  = $this->language->get('entry_location');
		$this->data['text_remove'] 		  = $this->language->get('text_remove');
		$this->data['text_select_vendor'] = $this->language->get('text_select_vendor');
		$this->data['text_hold'] 		  = $this->language->get('text_hold');
		$this->data['button_hold'] 		  = $this->language->get('button_hold');
		$this->data['button_save'] 		  = $this->language->get('button_save');
		$this->data['button_cancel'] 	  = $this->language->get('button_cancel');
		$this->data['button_add'] 		  = $this->language->get('button_add');
		$this->data['button_clear'] 	  = $this->language->get('button_clear');
		$this->data['column_action'] 	  = $this->language->get('column_action');
        $apply_tax_type = 0;

		if (isset($this->request->get['purchase_id'])) {
			if(isset($this->request->get['type']) && $this->request->get['type']=='sales_convert'){
				$purchaseInfo = $this->model_transaction_sales->getSalesnew($this->request->get['purchase_id']);
				$purchaseInfo['remarks'] = $purchaseInfo['payment_status']=='Paid' ? $purchaseInfo['payment_status'] : $purchaseInfo['remarks'];
				$purchaseInfo['reference_no'] = $purchaseInfo['network_order_id'];
			}else{
				$purchaseInfo=$this->model_transaction_sales->getSalesInvoice($this->request->get['purchase_id']);
			}
			$apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($purchaseInfo['vendor_id']);
			if(empty($purchaseInfo)) {
				$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
			if (!empty($purchaseInfo)) {
			    if (!empty($purchaseInfo['bill_discount_percentage']) && ($purchaseInfo['bill_discount_percentage'] > 0)) {
				    $this->session->data['bill_discount']	= $purchaseInfo['bill_discount_percentage'];
					$this->session->data['bill_discount_mode']	= 1;
				} elseif(!empty($purchaseInfo['bill_discount_price']) && ($purchaseInfo['bill_discount_price'] > 0)) {
					$this->session->data['bill_discount']	= $purchaseInfo['bill_discount_price'];
					$this->session->data['bill_discount_mode'] = 2;
				}
				if($purchaseInfo['handling_fee'] > 0){
					$this->session->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
				}
				if(isset($this->request->get['type']) && $this->request->get['type']=='sales_convert'){
					$purchaseProducts = $this->model_transaction_sales->getSalesProductnew($purchaseInfo['invoice_no']);
				}else{
					$purchaseProducts = $this->model_transaction_sales->getSalesInvoiceDetails($purchaseInfo['invoice_no']);
				}

				foreach ($purchaseProducts as $products) {
					if(!isset($this->request->get['type'])){
						$products['price']      = $products['sku_price'];
						$products['purchase_id']= $products['invoice_no'];
						$products['net_price']  = $products['net_total'];
						$products['tax_price']  = $products['gst'];
					}else{
						$products['price']      = $products['sku_price'];
					}
					$products['description']= $products['cust_description'];
					if($products['location_code']==''){
						$products['location_code'] = $this->session->data['location_code'];
					}
					// printArray($products); die;
					$products['product_id'] = $this->model_transaction_sales->getProductIdby_sku($products['sku']);
					$products = array_merge($products, $this->getDiscountCalculation($products));
					if(!empty($this->session->data['cart_purchase'])){
						if(array_key_exists($products['product_id'], $this->session->data['cart_purchase']) && ($products['qty'] > 0)) {
							$this->cart->update($products['product_id'], $products['qty'], $cart_type, $products);
						} elseif ($products['qty'] > 0) {
							$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
						}
					} elseif ($products['qty'] > 0) {
						$this->cart->add($products['product_id'], $products['qty'], '', '', '', $cart_type, $products);
					}
				}
			}
		}

		$this->data['rowdata'] = array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'product')) {
		   if (isset($this->request->post['product_id']) && ($this->request->post['quantity'] > 0)) {
			   $productId	= $this->request->post['product_id'];
			   $productQty	= $this->request->post['quantity'];
			   $productPrice = $this->request->post['price'];
			   $this->request->post['net_price'] = ($productPrice * $productQty);
			   $this->request->post = array_merge($this->request->post, $this->getDiscountCalculation($this->request->post));
			   if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0)) {
				   $this->cart->update($productId, $productQty, $cart_type, $this->request->post);
			   } elseif ($productQty > 0) {
			       $this->cart->add($productId, $productQty, '', '', '', $cart_type, $this->request->post);
			   }
		   }
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'rowcount')) {
			$this->data['rowdata'] = $this->getProductRowCalculation($this->request->post);
			if(!empty($this->request->post['discount_percentage'])) {
				$this->data['hide_discount_percentage']	= '';
				$this->data['hide_discount_price']	= '1';
			} elseif (!empty($this->request->post['discount_price'])) {
				$this->data['hide_discount_percentage']	= '1';
				$this->data['hide_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'billDiscount')) {
			$bill_discount_percentage	= $this->request->post['bill_discount_percentage'];
			$bill_discount_price	= $this->request->post['bill_discount_price'];
			$subTotal = $this->request->post['sub_total_value'];
			if (!empty($bill_discount_percentage) && ($bill_discount_percentage > 0)) {
				if($bill_discount_percentage > 100) {
					$this->error['warning'] = 'Please enter discount percentage below 100';
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_percentage;
				    $this->session->data['bill_discount_mode']	= 1;
				}
				$this->data['bill_discount_percentage'] = $bill_discount_percentage;
				$this->data['hide_bill_discount_percentage']	= '';
				$this->data['hide_bill_discount_price']	= '1';
			} elseif (!empty($bill_discount_price) && ($bill_discount_price > 0)) {
				if($bill_discount_price > $subTotal) {
					$this->error['warning'] = 'Please enter voucher discount below '.$subTotal;
				} else {
				    $this->session->data['bill_discount']	= $bill_discount_price;
				    $this->session->data['bill_discount_mode'] = 2;
				}
				$this->data['bill_discount_price'] = $bill_discount_price;
				$this->data['hide_bill_discount_percentage']	= '1';
				$this->data['hide_bill_discount_price']	= '';
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'inventory')) {
		    if (!empty($this->request->post['product_id'])) {
			    $this->data['rowdata'] = $this->getProductInfo($this->request->post['product_id']);
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get['updateType'] == 'removeProduct')) {
		    if (!empty($this->request->get['removeProductId'])) {
			    $this->cart->remove($this->request->get['removeProductId'], $cart_type);
				$this->clearPurchaseData($remove = 'discount');
			}
		}
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->post['purchase_button'] == 'submit')) {
		  $vendor_id = $this->request->post['vendor'];
		  $apply_tax_type = $this->model_transaction_purchase->getVendorsTaxId($vendor_id);

		}
            
		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$url = '';
		$filter_from_date = date('d/m/Y');
		if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_transaction = null;
		if (isset($this->request->get['filter_transaction'])) {
			$filter_transaction = $this->request->get['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_status = array();
		if (!empty($this->request->get['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->get['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_network = array();
		if (!empty($this->request->get['filter_network'])) {
			$filter_network = $this->request->get['filter_network'];
		}
		if(!empty($filter_network)){
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($this->request->get['filter_xero'])) {
			$filter_xero = $this->request->get['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_agent = '';
		if (isset($_REQUEST['filter_agent'])) {
			$filter_agent = $_REQUEST['filter_agent'];
			$url .= '&filter_agent=' . $filter_agent;
		}
		$filter_customer = null;
		if (isset($this->request->get['filter_customer'])) {
			$filter_customer = $this->request->get['filter_customer'];
			$url .= '&filter_customer=' . $filter_customer;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice List',
			'href'      => $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
       	if(isset($this->request->get['purchase_id']) && isset($this->request->get['type'])){
			$this->data['action'] = $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}
       	else if(isset($this->request->get['purchase_id'])){
			$this->data['action'] = $this->url->link('transaction/sales_invoice/update', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}else{
			$this->data['action'] = $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
       	}
		$this->data['cancel'] = $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$purchasePrefix	= $this->config->get('config_purchase_increment');
	
		if (isset($this->request->post['transaction_no'])) {
			$this->data['transaction_no'] = $this->request->post['transaction_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_no'] = $purchaseInfo['invoice_no'];
		} else {
			$this->data['transaction_no'] = $this->getTransNo();
		}
		if($this->request->get['type']=='sales_convert'){
			$this->data['transaction_no'] = $this->getTransNo();
		}

		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		
		if (isset($this->request->post['transaction_date'])) {
			$this->data['transaction_date'] = $this->request->post['transaction_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['transaction_date'] = date('d/m/Y',strtotime($purchaseInfo['invoice_date']));
		} else {
			$this->data['transaction_date'] = date('d/m/Y');
		} 
		$this->data['transaction_date'] = date('d/m/Y');
		
		if (!empty($purchaseInfo)) {
		    $this->session->data['vendor'] = $purchaseInfo['customer_code'];
		}
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
			$this->session->data['vendor'] = $this->request->post['vendor'];
			$vendorData = $this->model_transaction_sales->getB2BCustomersbyCode($this->request->post['vendor']);
			$this->data['vendor_name'] = $vendorData['name'];
		} elseif (isset($this->request->get['newCustId'])) {
			$vendorData = $this->model_transaction_sales->getCustomerDetails($this->request->get['newCustId']);
			$this->data['vendor'] 	       = $vendorData['customercode'];
			$this->data['vendor_name'] 	   = $vendorData['name'];
			$this->data['vendorTaxAndType']= $vendorData['tax_allow'].'||'.$vendorData['tax_type'];
			$this->session->data['vendor'] = $this->data['vendor'];
		} elseif (!empty($purchaseInfo)) {
		    $this->data['vendor'] = $purchaseInfo['customer_code'];
			$vendorData = $this->model_transaction_sales->getB2BCustomersbyCode($this->session->data['vendor']);
			$this->data['vendor_name'] = $vendorData['name'];
		} else {
			$this->data['vendor'] = '';
			$this->session->data['vendor'] = '';
			$this->data['vendor_name'] = '';
		}

		if (isset($this->request->post['reference_no'])) {
			$this->data['reference_no'] = $this->request->post['reference_no'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_no'] = $purchaseInfo['network_order_id'];
		} else {
			$this->data['reference_no'] = '';
		}
		$this->data['sales_trans_no'] = '';
		if(isset($this->request->get['purchase_id'])){
			$this->data['sales_trans_no'] = $purchaseInfo['invoice_no'];
		}
		
		if (isset($this->request->post['reference_date'])) {
			$this->data['reference_date'] = $this->request->post['reference_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['reference_date'] = date("d/m/Y", strtotime($purchaseInfo['header_remarks']));
		} else {
			$this->data['reference_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['delivery_to_date'])) {
			$this->data['delivery_to_date'] = $this->request->post['delivery_to_date'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['delivery_to_date'] = date("d/m/Y", strtotime($purchaseInfo['delivery_to_date']));
		} else {
			$this->data['delivery_to_date'] = date('d/m/Y');
		}
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['remarks'] = $purchaseInfo['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['location_code'])) {
			$this->data['location_code'] = $this->request->post['location_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['location_code'] = $purchaseInfo['location_code'];
		} else {
			$this->data['location_code'] = '';
		}
		
		if ($this->session->data['bill_discount_mode']=='1' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_percentage'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_percentage'] = $purchaseInfo['bill_discount_percentage'];
		} else {
			$this->data['bill_discount_percentage'] = '';
		}

		if ($this->session->data['bill_discount_mode']=='2' && $this->session->data['bill_discount']!='') {
			$this->data['bill_discount_price'] = $this->session->data['bill_discount'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['bill_discount_price'] = $purchaseInfo['bill_discount_price'];
		} else {
			$this->data['bill_discount_price'] = '';
		}
		if($this->session->data['handling_fee']!='') {
			$this->data['handling_fee'] = $this->session->data['handling_fee'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['handling_fee'] = $purchaseInfo['fc_handling_fee'];
		} else {
			$this->data['handling_fee'] = '';
		}

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_class_id'] = $purchaseInfo['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = '';
		}
		
		if (isset($this->request->post['tax_type'])) {
			$this->data['tax_type'] = $this->request->post['tax_type'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['tax_type'] = $purchaseInfo['tax_type'];
		} else {
			$this->data['tax_type'] = '';
		}
		$this->data['tax_type'] = $this->data['tax_type'] =='0' ? '1': '2';

		if (isset($this->request->post['currency_code'])) {
			$this->data['currency_code'] = $this->request->post['currency_code'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['currency_code'] = $purchaseInfo['currency_code'];
		} else {
			$this->data['currency_code'] = '';
		}
		if (isset($this->request->post['conversion_rate'])) {
			$this->data['conversion_rate'] = $this->request->post['conversion_rate'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['conversion_rate'] = $purchaseInfo['conversion_rate'];
		} else {
			$this->data['conversion_rate'] = '';
		}
		if (isset($this->request->post['term_id'])) {
			$this->data['term_id'] = $this->request->post['term_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['term_id'] = $purchaseInfo['term_id'];
		} else {
			$this->data['term_id'] = '';
		}
		if (isset($this->request->post['shipping_id'])) {
			$this->data['shipping_id'] = $this->request->post['shipping_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['shipping_id'] = $purchaseInfo['shipping_id'];
		} else {
			$this->data['shipping_id'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['status'] = $purchaseInfo['status'];
		} else {
			$this->data['status'] = '';
		}
		if (isset($this->request->post['network_id'])) {
			$this->data['network_id'] = $this->request->post['network_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['network_id'] = $purchaseInfo['network_id'];
		} else {
			$this->data['network_id'] = '';
		}
		if (isset($this->request->post['network_order_id'])) {
			$this->data['network_order_id'] = $this->request->post['network_order_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['network_order_id'] = $purchaseInfo['network_order_id'];
		} else {
			$this->data['network_order_id'] = '';
		}
		if (isset($this->request->post['delivery_remarks'])) {
			$this->data['delivery_remarks'] = $this->request->post['delivery_remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['delivery_remarks'] = $purchaseInfo['delivery_remarks'];
		} else {
			$this->data['delivery_remarks'] = '';
		}
		if (isset($this->request->post['discount_remarks'])) {
			$this->data['discount_remarks'] = $this->request->post['discount_remarks'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['discount_remarks'] = $purchaseInfo['discount_remarks'];
		} else {
			$this->data['discount_remarks'] = '';
		}
		if($this->request->get['purchase_id'] !=''){
			$this->data['purchase_id'] = $this->request->get['purchase_id'];
		}
		if($this->request->get['type'] !=''){
			$this->data['purchase_type'] = $this->request->get['type'];
		} else {
			$this->data['purchase_type'] = '';
		}
		if($this->request->get['sales_man'] !=''){
			$this->data['sales_man'] = $this->request->get['sales_man'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['sales_man'] = $purchaseInfo['sales_man'];
		}else {
			$this->data['sales_man'] = '';
		}

		if (isset($this->request->post['network_id'])) {
			$this->data['network_id'] = $this->request->post['network_id'];
		} elseif (!empty($purchaseInfo)) {
			$this->data['network_id'] = $purchaseInfo['network_id'];
		} else {
			$this->data['network_id'] = '';
		}


		$this->data['istagged'] = 0;
		if (!empty($purchaseInfo)) {
			$this->data['istagged'] = $purchaseInfo['istagged'];
		}
		$this->data['products'] = array();
		if (!empty($this->request->post['vendor'])) {
			$filter_vendor = $this->request->post['vendor'];
		} elseif (isset($this->session->data['vendor'])) {
		    $filter_vendor = $this->session->data['vendor'];
		} else {
			$filter_vendor = null;
		}
		$filter_vendor =''; // Ragu Assigned for
		$data = array(
			'filter_vendor'	  => $filter_vendor,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);
		$this->data['clocation_code'] = $this->session->data['location_code'];
		$this->data['Tolocations'] = $this->cart->getLocation();
		//purchase cart collection
		$this->data['cartPurchaseDetails']	= $this->getPurchaseCartDetails($cart_type,$apply_tax_type);
		// printArray($this->data['cartPurchaseDetails']); die;

		$this->data['allTerms'] 			= $this->cart->getAllTerms();
		$this->data['channels_list'] 		= $this->cart->getChannelsList();
		$this->data['apply_tax_type']       = $apply_tax_type;
		$this->load->model('ajax/cart');
		$this->data['purchase_totals']		= $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$this->load->model('localisation/weight_class');
		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);
		$this->data['salesmanlist']		   = $this->model_transaction_sales->getSalesMansList();
		$this->data['type'] 			   = $this->request->get['type'];
        $this->data['customer_collection'] = $this->model_transaction_sales->getB2BCustomers();
		$this->data['token'] 			   = $this->session->data['token'];
		$this->data['currencys'] 		   = $this->model_transaction_purchase->getCurrency();
		$this->data['addCust']   		   = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'].'&from=sales_invoice', 'SSL');
		if(isset($this->request->get['type']) && $this->request->get['type']=='sales_convert'){
			$this->data['cancel'] = $this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}else{
			$this->data['cancel'] = $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['route'] = 'transaction/sales';
		$this->template = 'transaction/sales_invoice_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='',$customer_id='') {
		
		if (empty($this->request->post['transaction_date'])) {
			$this->error['transaction_date'] = "Please enter transaction date.";
		}
		if (empty($this->request->post['vendor'])) {
			$this->error['warning'] = "Please select any one customer code.";
		}
		if (empty($this->request->post['reference_date'])) {
			// $this->error['reference_date'] = "Please enter reference date.";
		}
		if (empty($this->request->post['reference_no'])) {
			$this->error['reference_no'] = "Please enter reference no.";
		}
		if (empty($this->session->data['cart_purchase'])) {
			$this->error['warning'] = "Please add product and save.";
		}
		if (empty($this->request->post['transaction_no'])) {
			$this->error['transaction_no'] = "Please enter transaction no.";
		}else{
			if($type !='update'){
				$purchaseInfo = $this->model_transaction_sales->getSalesInvoiceByinvoice($this->request->post['transaction_no']);
				if($purchaseInfo){
				$this->error['warning'] = 'Please enter unique transaction number.';
			 }
			}
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	public function export_csv(){
		$this->load->model('transaction/sales');
		
		if(!empty($this->request->post['selected_invoice'])){

			ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=sales_invoice.csv');
	        print "Tran No,TranDate,Order Id,Customer,Delivery Date,Description,Quantity,Unit Amount,Discount,TaxType,Delivery Status,Payment Status,Sales Channel\r\n";

			foreach ($this->request->post['selected_invoice'] as $invoice_no) {
				$results  = $this->model_transaction_sales->getSalesInvoiceDetails_csv($invoice_no);
				if(count($results)){
					$j=0; 
					$total=0; 
					foreach ($results as $result) {	
						$j++;
						
						$taxType 	 	 = '0%';
						if($result['sh_tax_class']=='2'){
							$taxType 	 = $result['tax_type'] == '0' ? '7% Excl' : '7% Incl';
						}
						$network = $this->model_transaction_sales->getNetworkName($result['network_order_id']);
						$network = $network == 'Show Room' ? $result['location_name'] : $network;
						$delivery_to_date = '';
						if($result['delivery_to_date'] !='' ){
							$delivery_to_date =' - '.date('d/m/Y',strtotime($result['delivery_to_date']));
						}
						$cust_name        = $result['cust_name'];
						$transaction_no   = $result['invoice_no'];
						$reference   	  = $result['reference_no'];
						$network_order_id = $result['network_order_id'];
						$transaction_date = date('d/m/Y',strtotime($result['invoice_date']));
						$due_date 		  = date('d/m/Y',strtotime($result['header_remarks'])).$delivery_to_date;
						$description      = $result['description'];
						$quantity         = $result['qty'];
						$unit_amount      = $result['sku_price'];
						$discount  	      = $result['discount'];
						$taxType      	  = $taxType;
						$delivery         = showText($result['delivery_status']);
						$payment          = $result['payment_status'];

						print "$transaction_no,\"$transaction_date\",\"$network_order_id\",\"$cust_name\",\"$due_date\",\"$description\",\"$quantity\",\"$unit_amount \",\"$discount\",\"$taxType\",\"$delivery\",\"$payment\",\"$network\"\r\n";
					}
				}
		}
		$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'].$url, 'SSL'));
		}
	}
	public function exportPdf_SalesInvoiceList(){
		$this->load->model('transaction/sales');
		$this->load->model('inventory/reports');
		$this->load->model('user/user');
		
		if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filterStr = 'From Date: '.$filter_from_date;
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filterStr .= ' To Date: '.$filter_to_date;
		$filter_location = $this->session->data['location_code'];
		if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filterStr .= ', Location : '.$filter_location;
		$filter_customer = null;
		if ($this->request->get['filter_customer'] !='') {
			$filter_customer = $this->request->get['filter_customer'];
			$url .= '&filter_customer=' . $filter_customer;
			$filterStr .= ', Customer : '.$this->model_transaction_sales->getB2BCustomersbyCode($filter_customer)['name'];
		}
		$filter_transaction = null;
		if ($this->request->get['filter_transaction'] !='') {
			$url .= '&filter_transaction=' . $this->request->get['filter_transaction'];
			$filterStr .= ', Transaction : '.$this->request->get['filter_transaction'];
		}
		$filter_xero = '0';
		if ($this->request->get['filter_xero'] !='') {
			$filter_xero = $this->request->get['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
			if($filter_xero=='1'){
				$filterStr .= ', Xero : Posted';
			}else if($filter_xero=='2'){
				$filterStr .= ', Xero : UnPosted';
			}
		}
		$filter_delivery_status = array();
		if (isset($this->request->get['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->get['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			$filterStr .= ', Delivery : ';
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
				$filterStr .= $value.' ';
			}
		}
		$filter_payment_status = array();
		if (!empty($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			$filterStr .= ', Payment : ';
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
				$filterStr .= $value.' ';
			}
		}
		$filter_network = array();
		if (isset($this->request->get['filter_network'])) {
			$filter_network = $this->request->get['filter_network'];
		}
		if(!empty($filter_network)){
			$filterStr .= ', Network : ';
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
				$filterStr .= $this->model_transaction_sales->getNetworkName($value).' ';
			}
		}
		$filter_agent = '';
		if (isset($this->request->get['filter_agent'])) {
			$filter_agent = $this->request->get['filter_agent'];
			$user = $this->model_user_user->getUser($filter_agent);
			$filterStr .= ', Agent : '.$user['firstname'].' '.$user['lastname'];
			$url .= '&filter_agent=' . $filter_agent;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$data = array(
			'filter_from_date'	  => $filter_from_date,
			'filter_to_date'	  => $filter_to_date,
			'filter_location'	  => $filter_location,
			'filter_transaction'  => $filter_transaction,
			'filter_delivery_status'=> $filter_delivery_status,
			'filter_customer' 		=> $filter_customer,
			'filter_payment_status' => $filter_payment_status,
			'filter_network'        => $filter_network,
			'filter_agent'          => $filter_agent,
			'filter_xero'           => $filter_xero
		);
		$results    	 = $this->model_transaction_sales->getSalesInvoiceList($data);   
		$company_id		 = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$str			 = $this->url->getCompanyAddressHeaderString($company_details);
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Sales Invoice</td><td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}
		$str.="<table class='listing'><thead>
			<tr>
				<th align='left'>S No</th>
				<th align='center'>Tran No</th>
				<th align='center'>Tran Date</th>
				<th align='left'>Location</th>
				<th align='left'>Customer</th>
				<th align='center'>Net Total</th>
				<th align='center'>Delivery</th>
				<th align='center'>Payment</th>
				<th align='center'>Network</th>
			</tr></thead> <tbody>"; 
		$j=0;
		foreach ($results as $result) {
			$j++;
			$result['payment_status'] = $result['status'] == 'Canceled' ? 'Canceled' : $result['payment_status']; 
			$network         = $this->model_transaction_sales->getNetworkName($result['network_id']);
			$customer        = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code'])['name'];
			$invoice_no      = $result['invoice_no'];
			$invoice_date    = date('d/m/Y',strtotime($result['invoice_date']));
			$location_code   = $result['location_code'];
			$net_total       = $result['net_total'];
			$delivery_status = $result['delivery_status'];
			$payment_status  = $result['payment_status'];

			$str.="<tr>
				   <td align='left'>".$j."</td>
				   <td align='center'>".$invoice_no."</td>
				   <td align='center'>".$invoice_date."</td>
				   <td align='left'>".$location_code."</td>
				   <td align='left'>".$customer."</td>
				   <td align='center'>".$net_total."</td>
				   <td align='center'>".$delivery_status."</td>
				   <td align='center'>".$payment_status."</td>
				   <td align='center'>".$network."</td>
			</tr>";	
		}
		$str.="</tbody></table>";
		if($str){
			$filename = 'Sales_Invoice.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		die;
		$this->redirect($this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'].$url, 'SSL'));
	}
	public function getProductInfo($productId) {
		$this->data['product'] = array();
		$this->load->model('inventory/inventory');
		$_productDetails = $this->model_inventory_inventory->getProduct($productId);
		if(!empty($_productDetails)) {
			$quantity	= $_productDetails['quantity'];
			$originalPrice	= $_productDetails['price'];
			$discount_percentage = '';
			$discount_price	= '';
			$taxAmount = $this->cart->getTaxeByProduct($_productDetails);
			$this->data['product'] = array(
					'product_id'                => $_productDetails['product_id'],
					'sku'                       => $_productDetails['sku'],
					'name'                      => $_productDetails['name'],
					'price'                     => $_productDetails['price']
				);
		}
		return $this->data['product'];
	}
	public function getPurchaseCartDetails($cart_type,$apply_tax_type='') {
		$product_data = array();
		if($apply_tax_type=='0'){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		//echo $apply_tax_type=0;
		//printArray($this->cart->getProducts($cart_type,$apply_tax_type)); exit;
		foreach ($this->cart->getProducts($cart_type,$apply_tax_type) as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'tax_class_id'=> $product['tax_class_id'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'raw_cost'   => $this->currency->format($product['sku_cost']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $product['total'],
				'tax'        => $product['purchase_tax'],
				'reward'     => $this->currency->format($product['reward']),
				'discount'   => $product['purchase_discount_value'],
				'purchase_discount'   => $product['purchase_discount'],
				'discount_mode'   => $product['purchase_discount_mode'],
				'description'   => $product['description']
			);
		}
		return $product_data;
	}
	public function getProductRowCalculation($productDetails) {
		$rowData = array();
		$discount_error = '';
		$discountData = array();
		$quantity	= $productDetails['quantity'];
		$originalPrice	= $productDetails['price'];
		$rawCost	= $productDetails['raw_cost'];
		$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		$taxAmount = $this->cart->getTaxeByProduct($productDetails,$apply_tax_type);
		$subTotal = ($originalPrice * $quantity);
		$productDetails['net_price'] = $subTotal;
		$discountData = $this->getDiscountCalculation($productDetails);
		$total = ($subTotal - $discountData['purchase_discount']) + $taxAmount;
		$rowData = array(
				'product_id'                => $productDetails['product_id'],
				'sku'                		=> $productDetails['sku'],
				'name'                      => $productDetails['name'],
				'quantity'                  => $quantity,
				'raw_cost'                  => $rawCost,
				'price'                     => $originalPrice,
				'tax_class_id'              => $productDetails['tax_class_id'],
				'weight_class_id'           => $productDetails['weight_class_id'],
				'discount_percentage'       => $productDetails['discount_percentage'],
				'discount_price'            => $productDetails['discount_price'],
				'sub_total'                 => $subTotal,
				'total'                     => $total
		);
		return $rowData;
	}
	public function clearPurchaseData($remove = '') {
	    if ($remove == 'purchase') {
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
		} else {
		    unset($this->session->data['cart_purchase']);
			unset($this->session->data['purchase']);
			unset($this->session->data['purchase_totals']);
			unset($this->session->data['handling_fee']);
			unset($this->session->data['totals']);
			if ($this->session->data['bill_discount']) {
				unset($this->session->data['bill_discount']);
				unset($this->session->data['bill_discount_mode']);
			}
			if ($this->session->data['vendor']) { unset($this->session->data['vendor']); }
		}
	}
	public function getDiscountCalculation($data, $type = '') {
		$discount 		= array();
		$discountError 	= '';
		$discountPercentage	= $data['discount_percentage'];
		$discountPrice		= $data['discount_price'];
		$data['subTotal'] 	= $data['net_price'];
		if (!empty($discountPercentage) && ($discountPercentage > 0)) {
			if($discountPercentage >= 100) {
			    $discountError = 'Please enter discount percentage below 100';
			} else {
			    $purchaseDiscountValue = $discountPercentage;
			    $purchaseDiscountMode	= 1;
			}
		} elseif (!empty($discountPrice) && ($discountPrice > 0)) {
			if($discountPrice >= $data['subTotal']) {
				$discountError = 'Please enter voucher discount below '.$data['subTotal'];
			} else {
			    $purchaseDiscountValue = $discountPrice;
			    $purchaseDiscountMode	= 2;
			}
		}
		if (!empty($purchaseDiscountValue)) {
			$discountAmount = $this->cart->getDiscountByProduct($data);
			if (!empty($discountAmount)) {
				$discount['purchase_discount'] = $discountAmount;
				$discount['purchase_discount_mode'] = $purchaseDiscountMode;
				$discount['purchase_discount_value'] = $purchaseDiscountValue;
			}
		}
		if (!empty($discountError)) {
		    $this->error['warning'] = $discountError;
			return false;
		} else {
		    return $discount;
		}
	}
	public function getProductDetails($data, $type = '') {
			$this->load->model('transaction/sales');
			$this->data['filter_name'] = $this->request->post['sku'];
			$_productDetails           = $this->model_transaction_sales->getProductByNamenew($this->data);
			$str ='<ul id="country-list">';
			for($i=0;$i<count($_productDetails);$i++){
				$var =  $_productDetails[$i]['product_id'].'||'.$_productDetails[$i]['sku'].'||'.$_productDetails[$i]['name'].'||'.$_productDetails[$i]['price'];
				$str.= "<li onClick=\"selectedProduct('".$var."');\">".$_productDetails[$i]['sku'].' ('.$_productDetails[$i]['name'].")</li>";
			}
			$str.='</ul>';
			echo $str;
	}
	public function getTransNo(){
		
	    $transNumberWithSuffix = $this->model_transaction_sales->getSalesInvoiceAutoId();
		$transNumber		   = $transNumberWithSuffix;
		if($transNumber){
			 $transaction_no = $transNumber+1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;//$this->model_inventory_inventory->getInventoryAutoId();
			$this->data['transaction_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}
	public function payment_view(){
		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle($this->language->get('Sales View'));
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}

		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice List',
			'href'      => $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice View',
			'href'      => $this->url->link('transaction/sales_invoice/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] 		= $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchaseInfo'] = $this->model_transaction_sales->getSalesInvoice($this->request->get['purchase_id']);
		$this->data['paymentInfo'] = $this->model_transaction_sales->getOrderPaymentStatus($this->data['purchaseInfo']['invoice_no']);
		$this->data['vendorDetail']= $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);

		$this->data['userDetail']= $this->model_user_user->getUser($this->data['purchaseInfo']['createdby']);
		$this->data['productDetails']= $this->model_transaction_sales->getSalesInvoiceDetails($this->data['purchaseInfo']['invoice_no']);

		// $this->data['download_button'] = $this->url->link('transaction/sales_invoice/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');

		$this->template = 'transaction/sales_invoice_payment_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function view(){
		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');

		$this->document->setTitle($this->language->get('Sales View'));
		$this->data['column_action']   = $this->language->get('column_action');
		$this->data['heading_title']   = $this->language->get('heading_title');
		$this->data['text_tran_no']    = $this->language->get('text_tran_no');
		$this->data['text_tran_dt']    = $this->language->get('text_tran_dt');
		$this->data['text_tran_type']  = $this->language->get('text_tran_type');
		$this->data['text_vendor_code']= $this->language->get('text_vendor_code');
		$this->data['text_vendor_name']= $this->language->get('text_vendor_name');
		$this->data['text_net_total']  = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert']   = $this->language->get('button_insert');
		$this->data['button_delete']   = $this->language->get('button_delete');
		$this->data['button_filter']   = $this->language->get('button_filter');
		$this->data['text_print'] 	   = $this->language->get('text_print');
		$this->data['text_modify']     = $this->language->get('text_modify');
		$this->data['button_show_hold']= $this->language->get('button_show_hold');
		$this->data['button_back']     = $this->language->get('button_back');
		$this->data['token'] 		   = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}

		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice List',
			'href'      => $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice View',
			'href'      => $this->url->link('transaction/sales_invoice/view', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$backUrl     = $url;
		if($this->request->get['from']=='so'){
			$backUrl = '';
		}
		$this->data['back_button'] 		= $this->url->link('transaction/sales_invoice', 'token='.$this->session->data['token'].$backUrl,'SSL');

		if(isset($this->request->get['type']) && $this->request->get['type']=='payment_view'){
			$this->data['back_button'] 	= $this->url->link('transaction/sales_invoice/sales_payment', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['purchaseInfo']     = $this->model_transaction_sales->getSalesInvoice($this->request->get['purchase_id']);
		$this->data['vendorDetail'] 	= $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);
		$this->data['shippingAddress']  = $this->model_transaction_sales->getShippingAddressById($this->data['purchaseInfo']['shipping_id']);
		$this->data['userDetail'] 		= $this->model_user_user->getUser($this->data['purchaseInfo']['createdby']);
		$productDetail 	= $this->model_transaction_sales->getSalesInvoiceDetails($this->data['purchaseInfo']['invoice_no']);
		foreach ($productDetail as $value) {
			$value['childItems'] = $this->model_transaction_sales->getProductChildItems($value['product_id']);
			$this->data['productDetails'][] = $value;
		}
		$this->data['download_button'] 	= $this->url->link('transaction/sales_invoice/download_pdf', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id']. $url, 'SSL');
		$this->data['modify_button'] 	= $this->url->link('transaction/sales_invoice/update', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'].$url, 'SSL');

		$this->template = 'transaction/sales_invoice_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function update_do(){
		
		$product_details['do_no']            	= $this->getDoNo();
		$product_details['shipping_id']      	= $this->request->post['shipping_id'];
		$product_details['customer_id']      	= $this->request->post['customer_id'];
		$product_details['sales_invoice_id'] 	= $this->request->post['sales_invoice_id'];
		$product_details['sales_transaction_no']= $this->request->post['sales_transaction_no'];
		$product_details['total_invoice_items'] = $this->request->post['total_invoice_items'];
		$product_details['delivery_date']    	= $this->request->post['delivery_date'];
		$product_details['deliveryman_id']   	= $this->request->post['deliveryman_id'];
		$product_details['quantity'] 		 	= $this->request->post['quantity'];
		$product_details['real_quantity'] 	 	= $this->request->post['real_quantity'];
		$product_details['do_quantity'] 	 	= $this->request->post['do_quantity'];
		$products 							 	= $this->request->post['selected'];

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}

		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();   //For sales Channel
		if (!empty($_REQUEST['filter_network'])) {
			$filter_network = $_REQUEST['filter_network'];
			foreach($filter_network as $value) {
				$url .= '&filter_network[]=' . $value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		$err = $this->model_transaction_sales->add_do($product_details,$products);
		if($err !=''){
			$this->session->data['warning'] = $err;
		}
		$this->redirect($this->url->link('transaction/sales_invoice/multiple_do','token='.$this->session->data['token'].'&purchase_id='.$product_details['sales_transaction_no'].$url,'SSL'));

	}
	public function multiple_do(){

		$this->language->load('transaction/sales');
		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		//language assign code for transaction purchase
		$this->document->setTitle($this->language->get('Sales View'));
		$this->data['column_action'] 	= $this->language->get('column_action');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_tran_no'] 	= $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] 	= $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] 	= $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] 	= $this->language->get('text_net_total');
		$this->data['text_created_by'] 	= $this->language->get('text_created_by');
		$this->data['text_created_on'] 	= $this->language->get('text_created_on');
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');
		$this->data['button_insert'] 	= $this->language->get('button_insert');
		$this->data['button_delete'] 	= $this->language->get('button_delete');
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['text_print'] 		= $this->language->get('text_print');
		$this->data['text_modify'] 		= $this->language->get('text_modify');
		$this->data['button_show_hold']	= $this->language->get('button_show_hold');
		$this->data['button_back'] 		= $this->language->get('button_back');
		$this->data['token'] 			= $this->session->data['token'];

		$this->data['warning'] = '';
		if (isset($this->session->data['warning'])) {
			$this->data['warning'] = $this->session->data['warning'];
			unset($this->session->data['warning']);
		}

		$url ='';
		$filter_customer_name = null;
		if (isset($_REQUEST['filter_customer_name'])) {
			$filter_customer_name = $_REQUEST['filter_customer_name'];
			$url .= '&filter_customer_name=' . $filter_customer_name;
		}
		$filter_contact_number = null;
		if (isset($_REQUEST['filter_contact_number'])) {
			$filter_contact_number = $_REQUEST['filter_contact_number'];
			$url .= '&filter_contact_number=' . $filter_contact_number;
		}
		$filter_order_number = null;
		if (isset($_REQUEST['filter_order_number'])) {
			$filter_order_number = $_REQUEST['filter_order_number'];
			$url .= '&filter_order_number=' . $filter_order_number;
		}
		$filter_transaction = null;
		if (isset($_REQUEST['filter_transaction'])) {
			$filter_transaction = $_REQUEST['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_sales_person = null;
		if (isset($_REQUEST['filter_sales_person'])) {
			$filter_sales_person = $_REQUEST['filter_sales_person'];
			$url .= '&filter_sales_person=' . $filter_sales_person;
		}
		$filter_location = null;
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_network = array();
		if(!empty($_REQUEST['filter_network'])){
			$filter_network = $_REQUEST['filter_network'];
			foreach ($_REQUEST['filter_network'] as $value) {
				$url .= '&filter_network[]='.$value;
			}
		}
		$filter_is_delivery = null;
		if (isset($_REQUEST['filter_is_delivery'])) {
			$filter_is_delivery = $_REQUEST['filter_is_delivery'];
			$url .= '&filter_is_delivery=' . $filter_is_delivery;
		}
		if (isset($_REQUEST['filter_from_date'])) {
			$filter_from_date = $_REQUEST['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y',strtotime("- 1 day"));
		}
		$filter_to_date = date('d/m/Y');
		if (isset($_REQUEST['filter_to_date'])) {
			$filter_to_date = $_REQUEST['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_delivery_status = array();
		if (isset($_REQUEST['filter_delivery_status'])) {
			$filter_delivery_status = $_REQUEST['filter_delivery_status'];
		}
		if(!empty($filter_delivery_status)){
			foreach($filter_delivery_status as $value) {
				$url .= '&filter_delivery_status[]=' . $value;
			}
		}
		$filter_payment_status = array();
		if (!empty($_REQUEST['filter_payment_status'])) {
			$filter_payment_status = $_REQUEST['filter_payment_status'];
		}
		if(!empty($filter_payment_status)){
			foreach($filter_payment_status as $value) {
				$url .= '&filter_payment_status[]=' . $value;
			}
		}
		$filter_xero = '0';
		if (isset($_REQUEST['filter_xero'])) {
			$filter_xero = $_REQUEST['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_product = null;
		if (isset($_REQUEST['filter_product'])) {
			$filter_product = $_REQUEST['filter_product'];
			$url .= '&filter_product=' . $filter_product;
		}
		$filter_product_id = null;
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url .= '&filter_product_id=' . $filter_product_id;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
			$page = $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Invoice List',
			'href'      => $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Multiple Invoice DO',
			'href'      => $this->url->link('transaction/sales_invoice/multiple_do', 'token=' . $this->session->data['token'].'&purchase_id='.$this->request->get['purchase_id'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['action']          = $this->url->link('transaction/sales_invoice/update_do', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['back_button']     = $this->url->link('transaction/sales_invoice','token='.$this->session->data['token'].$url,'SSL');
		$this->data['download_button'] = $this->url->link('transaction/delivery/download_do_pdf', 'token='.$this->session->data['token']);
		$this->data['purchaseInfo']    = $this->model_transaction_sales->getSalesInvoice($this->request->get['purchase_id']);
		$this->data['shippingAddress'] = $this->model_transaction_sales->getShippingAddress($this->data['purchaseInfo']['shipping_id']);
		$this->data['salesman'] 	   = $this->model_transaction_sales->getSalesmanList();
		$this->data['vendorDetail']	   = $this->model_transaction_sales->getB2BCustomersbyCode($this->data['purchaseInfo']['customer_code']);

		$this->data['userDetail'] 	   = $this->model_user_user->getUser($this->data['purchaseInfo']['createdby']);
		$this->data['productDetails']  = array();
		
		if($this->data['purchaseInfo']['status'] != 'Canceled'){
			$this->data['productDetails']  = $this->model_transaction_sales->getSalesInvoiceDetailsforDO($this->data['purchaseInfo']['invoice_no']);
		}
		$this->data['doHeaders'] 	   = $this->model_transaction_sales->getSalesDoHeader($this->data['purchaseInfo']['invoice_no']);
			$this->data['doDetails']   = $this->model_transaction_sales->getSalesDoDetails($this->data['purchaseInfo']['invoice_no']);
		$invoiceDetails 	 		   = $this->model_transaction_sales->getSalesInvoiceDetails($this->data['purchaseInfo']['invoice_no']);
		$this->data['total_invoice_items'] = count($this->data['productDetails']);

		$do_sku = array();
		foreach ($this->data['doDetails'] as  $value) {
			array_push($do_sku,$value['sku']);
		}
		$this->data['do_sku'] = $do_sku;
		$this->template = 'transaction/sales_invoice_do.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function updateInvoiceNumber(){

		$this->load->model('transaction/sales');	
		if(isset($this->request->get['purchase_id'])){
	    	$invNo = $this->model_transaction_sales->updateInvoiceNumber($this->request->get['purchase_id']);
		}
		// $url='&pdf_purchase_id='.$this->request->get['purchase_id'];
		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $url, 'SSL'));
	}
	public function delete(){

		$this->load->model('transaction/sales');	
		if(isset($this->request->get['purchase_id'])){
			$this->model_transaction_sales->updateStatus($this->request->get['purchase_id']);
		}
		$this->session->data['success'] = 'Success: Sales Details Updated!';
			$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function export_pdf(){
		
		$this->clearTempFolder();
		$this->request->get['purchase_id'] = $this->request->post['orderId'];
		if(!empty($this->request->post['selected_invoice'])){
			
			include(DIR_SERVER.'/MPDF/mpdf.php');
			ob_end_clean();
			$mpdf = new mPDF('c','A4');
			$this->load->model('transaction/sales');
			$mpdf->mirrorMargins = 1;
			$mpdf->SetMargins(0, 0, 5);
			if(PDF_WM_LOGO=='1'){
				$mpdf->showWatermarkImage = true;
				$mpdf->watermarkImageAlpha = 0.1;
			}
			$mpdf->SetDisplayMode('fullpage');
			$mpdf->SetFont('DejaVuSans');
				$stylesheet = file_get_contents(HTTP_SERVER.'view/stylesheet/pdf.css');

			foreach ($this->request->post['selected_invoice'] as $value) {
				$this->request->get['purchase_id'] = $this->model_transaction_sales->getInvoiceNoById($value);
				$str = $this->download_pdf('1');
				if($str !=''){
					$mpdf->WriteHTML($stylesheet,1);
					$mpdf->AddPage();
					$mpdf->WriteHTML($str);
				}
			}
			$mpdf->setFooter($footer);
			$filename   = 'download/sales_invoice_list.pdf';
			$mpdf->Output($filename,'F');
			ob_end_flush();
		}
	}
	public function clearTempFolder(){     // for clear temp files in download folder
 		$files = glob(DIR_SERVER.'download/*');
			foreach($files as $file){
				if(is_file($file))
				unlink($file);
		}
	}
	public function download_pdf($returnStr=''){

		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');

		$purchase_id 	  = $this->request->get['purchase_id'];
		$company_id	      = $this->session->data['company_id'];
		$company_details  = $this->model_inventory_reports->getCompanyDetails($company_id);
		$company_details['invoice_title'] = "TAX Invoice"; //Set invoice title here
		$salesInfo 		  = $this->model_transaction_sales->getSalesInvoice($purchase_id);
		$company_details['location']      = $this->model_inventory_reports->getLocationDetails($salesInfo['location_code']);
		$vendorDetail	  = $this->model_transaction_sales->getB2BCustomersbyCode($salesInfo['customer_code']);
		$shippingDtls     = $this->model_transaction_sales->getShippingAddressWithCust($salesInfo['shipping_id']);
		$term	          = $this->model_transaction_sales->getTermById($salesInfo['term_id']);
		$paid_total	      = $this->model_transaction_sales->getTotalPaidAmount($salesInfo['invoice_no']);
		$userDetail 	  = $this->model_user_user->getUser($salesInfo['createdby']);
		$productDetails	  = $this->model_transaction_sales->getSalesInvoiceDetails($salesInfo['invoice_no']);
		$invoice_no       		= $salesInfo['invoice_no'];
		$transaction_date 		= date('d/m/Y',strtotime($salesInfo['invoice_date']));
		$reference_no     		= $salesInfo['network_order_id'];
		$reference_date         = '';
		if($salesInfo['header_remarks'] !='' && $salesInfo['header_remarks'] !='1970-01-01'){
			$reference_date   	= date('d/m/Y',strtotime($salesInfo['header_remarks']));
		}
        $tax_type 		  		= $salesInfo['tax_class_id']=='2' ? '7%' :'0%';
		$vendorDetail['mobile'] = $vendorDetail['mobile'] !='' ? 'Phone : '.$vendorDetail['mobile'] : '';
		$vendorDetail['email']  = $vendorDetail['email'] !='' ? ' Email : '.$vendorDetail['email'] : '';
			
			$str  = $this->url->getCompanyAddressHeaderString2($company_details);
			$str .='<table class="table" style="width:100%;">
				<tr>
					<td class="left">
						<table class="listings" style="width:100%;">
							<tr>
								<th align="left" style="width:20%;">Name</th>
								<td>'.$vendorDetail['name'].'</td>
							</tr>
							<tr>
								<th align="left">Address</th>
								<td>'.$vendorDetail['address1'].' '.$vendorDetail['address2'].', '.$vendorDetail['city'].' - '.$vendorDetail['zipcode'].'</td>
							</tr>
							<tr>
								<th align="left">Contact</th>
								<td>'.$vendorDetail['mobile'].$vendorDetail['email'].'</td>
							</tr>
						</table>
					</td>
					<td class="right">
						<table style="width:100%;">
							<tr>
								<th align="left">Invoice No</th>
								<td>'.$invoice_no.'</td>
							</tr>
							<tr>
								<th align="left">Invoice Date</th>
								<td>'.$transaction_date.'</td>
							</tr>
							<tr>
								<th align="left">Reference No</th>
								<td>'.$reference_no.'</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td class="left">
						<table class="listings" style="width:100%;">
							<tr>
								<th align="left" colspan="2">Shipping Details</th>
							</tr>'; 
					if($shippingDtls['name'] !=''){
					$str .='<tr>
								<th align="left">Name</th>
								<td>'.$shippingDtls['name'].'</td>
							</tr>';
					}
					$str .='<tr>
								<th align="left" style="width:20%;">Address</th>
								<td>'.$shippingDtls['address1'].' '.$shippingDtls['address2'].' '.$shippingDtls['city'].' '.$shippingDtls['country'].' '.$shippingDtls['zip'].'</td>
							</tr>
							<tr>
								<th align="left">Contact</th>
								<td> Phone : '.$shippingDtls['contact_no'].'</td>
							</tr>
						</table>
					</td>
					<td class="right">
						<table style="width:100%;">
							<tr>
								<th align="left">Order No</th>
								<td>'.$salesInfo['sales_trans_no'].'</td>
							</tr>
							<tr>
								<th align="left">Terms</th>
								<td>'.$term['terms_name'].'</td>
							</tr>
							<tr>
								<th align="left">Due Date</th>
								<td>'.$reference_date.'</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>';
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th align='left' >Description</th>
							<th align='left'>Quantity</th>
							<th align='left'>Unit Price</th>
							<th align='left'>Amount</th>
						</tr></thead><tbody>";
				$i= 1;
				$div_class = '';
				foreach ($productDetails as $value) {
				$value['cust_description'] = $value['cust_description'] !='' ? '('.$value['cust_description'].')' : '';

					$str.="<tr style='border-bottom: 2px solid black;'>
		   					<td>".$value['description']."<br><p style='font-size: 12px;'>".$value['cust_description']."</P>";
		   					$childItems = $this->model_transaction_sales->getProductChildItems($value['product_id']);
		   					if(!empty($childItems)){
		   						foreach ($childItems as $childItem) {
		   							$str .= '<span>'.$childItem['childsku'].' - '.$childItem['name'].'</span><br>';
		   						}
		   					}
					$str .="</td><td>".$value['qty']."</td>
							<td>".$value['sku_price']."</td>
							<td class='right'>".$value['sub_total']."</td>
						   </tr>";	
				$i++; }
				$str.="<tr>
						<td align='left' rowspan='6'>
							<img src='".BARCODE_IMAGE."' style='width:133px;'> 
						</td>
						<th></th><td align='right'>Subtotal</td>
						 <td class='right'>".number_format($salesInfo['sub_total'],2)."</td>
					   </tr>";
				$str.="<tr>
						<th></th><td align='right'>Discount</td>
						 <td class='right'>".number_format($salesInfo['discount'],2)."</td>
					   </tr>";
				$str.="<tr>
						<th></th><td align='right'>Delivery Fees</td>
						 <td class='right'>".$salesInfo['handling_fee']."</td>
					   </tr>";
				$str.="<tr>
						<th></th><td align='right'>GST@ ".$tax_type."</td>
						 <td class='right'>".number_format($salesInfo['gst'],2)."</td>
					   </tr>";
				$str.="<tr><th></th><td align='right'>Total</td>
						 <td class='right'>".number_format($salesInfo['net_total'],2)."</td>
					   </tr>";
				$str.="<tr><th></th><td align='right'>Total Payments</td>
						 <td class='right'>".number_format($paid_total,2)."</td>
					   </tr>";   
				$str.='<tr class="last">
						 <td align="left">
						 	Mode Of Payment<br>
						 	<input type="checkbox">&nbsp;Cash &nbsp;&nbsp;&nbsp;
							<input type="checkbox">&nbsp;Cheque &nbsp;&nbsp;&nbsp;
							<input type="checkbox">&nbsp;Nets &nbsp;&nbsp;&nbsp;
							<input type="checkbox">&nbsp;Visa/Master

						 	<table class="listings"  style="width:100%;">
								<tr>
									<td align="left" style="border-right:1px solid black;position: relative;right: 20px;">Delivery Date: '.$reference_date.'</td>
									<td align="left" style="right: 20px;">Remarks: '.$salesInfo['delivery_remarks'].'</td>
								</tr>
							</table>
						 </td>
						 <td align="right">Amount Due</td>
						 <td class="right">'.number_format($salesInfo['net_total'] - $paid_total,2).'</td>
					   </tr>';

				$str .= "</tbody></table>";
				$str .= "<table style='width:100%;'><tr><td></td></tr></table>";
				$str .= "<table style='width:100%;'><tr><td></td></tr></table>";
				$str .= "<p style='font-size:13px;text-align:justify;'>".$company_details['term1']."</p>";
				$str .= "<p style='font-size:13px;text-align:justify;'>".$company_details['term2']."</p>";
				$str .= "<p style='font-size:15px;font-weight: bold;text-align: justify;'><bold>".$company_details['term3']."</bold></p>";

			$str .='<table class="table" style="width:100%;">
				<tr>
					<td class="left">
					<table style="width:100%;">
						<tr>
							<th align="center">Banking Information</th>
							<td></td>
						</tr>
						<tr>
							<th align="left">Bank</th>
							<td>'.BANK_NAME.'</td>
						</tr>
						<tr>
							<th align="left">A/C No</th>
							<td>'.ACCOUNT_NO.'</td>
						</tr>
					</table>
					</td>
					<td class="right">
					<table style="width:100%;">
						<tr>
							<th align="center">Pay Now Option</th>
							<td></td>
						</tr>
						<tr>
							<th align="left">UEN</th>
							<td>'.$company_details['business_reg_no'].'</td>
						</tr>
					</table>
					</td>
				</tr>
				</table>';

		// echo $str; die;
		if($returnStr){
			return $str;
		}
		if($str){
			$filename = 'sales_invoice_'.$invoice_no.'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function download_do_pdf(){  // not used from wherever

		$this->load->model('transaction/sales');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->load->model('inventory/reports');

		$do_id 	 		 = $this->request->get['do_id'];
		$company_id	     = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$doHeader 	 = $this->model_transaction_sales->getSalesDoHeaderById($do_id);
		$salesInfo   = $this->model_transaction_sales->getSalesInvoice($doHeader['sales_transaction_no']);
		$vendorDetail= $this->model_transaction_sales->getB2BCustomersbyCode($salesInfo['customer_code']);
		$productDetails	  = $this->model_transaction_sales->getSalesDoDetail($doHeader['id']);
		$invoice_no       = $salesInfo['invoice_no'];
		$transaction_date = date('d/m/Y',strtotime($salesInfo['invoice_date']));
		$reference_no     = $salesInfo['reference_no'];
		$reference_date   = $salesInfo['invoice_date'];
		$company_details['invoice_title'] = "Sales DO"; //Set invoice title here
		$tax_type = $salesInfo['tax_type']=='1' ? '(7% Exclusive)':'(7% Inclusive)';
        $tax_type = $salesInfo['tax_class_id']=='2' ? $tax_type :'(0%)';
		
			$str = $this->url->getCompanyAddressHeaderString2($company_details);
			$strs.= '</table>';
			$str .='<table class="table" style="width:100%;">
				<tr>
					<td class="left">
					<table class="listings"  style="width:100%;">
						<tr>
							<th align="left">Customer Details</th>
							<td></td>
						</tr>
						<tr>
							<th align="left">Name</th>
							<td>'.$vendorDetail['name'].'</td>
						</tr>
						<tr>
							<th align="left">Shipping Address</th>
							<td>'.$vendorDetail['address1'].' '.$vendorDetail['address2'].', '.$vendorDetail['city'].' - '.$vendorDetail['zipcode'].'</td>
						</tr>
						<tr>
							<th align="left">Contact</th>
							<td>Phone : '.$vendorDetail['mobile'].' Email : '.$vendorDetail['email'].'</td>
						</tr>
					</table>
					</td>
					<td class="right">
					<table style="width:100%;">
						<tr>
							<th align="left">DoNo</th>
							<td>'.$doHeader['do_no'].'</td>
						</tr>
						<tr>
							<th align="left">DoDate</th>
							<td>'.$transaction_date.'</td>
						</tr>
						<tr>
							<th align="left"></th>
							<td></td>
						</tr>
					</table>
					</td>
				</tr>
			</table>';
				$str .="<table style='width:100%;'><tr><td></td></tr></table><br>";
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th align='left' >SKU</th>
							<th align='left' >Description</th>
							<th align='left'>Quantity</th>
						</tr></thead><tbody>";
				$i= 1;
				foreach ($productDetails as $value) {
					$str.="<tr style='border-bottom: 2px solid black;'>
		   					<td>".$value['sku']."</td>
		   					<td>".$value['prod_name']."</td>
							<td>".$value['qty']."</td>
						   </tr>";	
				$i++; }
				$str.='</tbody></table>';

				$result = fmod(count($productDetails),38);
				$minus  = $result < 10 ? 34 : 32;
				$counts = $minus - $result;
 				// echo $result;
 				// echo '-'.$counts; die;
 				for ($i=0; $i < $counts; $i++) { 
					$br .='<br>';
				}
				// <tr><td colspan="3"></td>'.$br.'</tr>
				$str .=$br.'<tr>
						 <td align="center" colspan="3">
						 	<table class="listings"  style="width:100%;">
								<tr>
									<td align="left" style="border-right:1px solid black;padding:20px;position: relative;bottom: 20px;right: 20px;">Delivery Date:<br></td>
									<td align="left" style="padding:20px;bottom: 20px;right: 20px;">Remarks:<br></td>
								</tr>
							</table>
						 </td>
					   </tr>';
		// echo $str; die;			
		if($str){
			$filename = $doHeader['do_no'].'_'.date('YmdHi').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}

	}
	public function ajaxUpdateqtyAndPrice(){

		$this->load->model('ajax/cart');
		$apply_tax_type = 0;
		$sku 	   		= $this->request->post['sku']; 
		$productQty		= $this->request->post['quantity'];
		$price     		= $this->request->post['price'];		
		$tax_class_id	= $this->request->post['tax_class_id'];

		$netPrice  		  = $productQty * $price;
		$data['netPrice'] = $netPrice;
		$data['price']    = $price;
		$data['total']    = $total;

		$netPrice  = $this->currency->format($netPrice);
		$total     = $this->currency->format($total);

		$cart_type = 'cart_purchase';
 		$productId = $this->request->post['product_id'];
 		if(array_key_exists($productId, $this->session->data['cart_purchase']) && ($productQty > 0 )) {
			$this->cart->update($productId, $productQty, $cart_type, $this->request->post);
		}

 		$apply_tax_type = $tax_class_id == '1' ? '0' : $this->request->post['tax_type'];
 		$cartPurchaseDetails = $this->getPurchaseCartDetails($cart_type,$apply_tax_type,$this->request->post['location_code']);
 		$total_data = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
 			foreach($total_data as $totals) {
				if($totals['code']=='tax'){
					$tax = $totals;
				}
			}
			$data['totals'] = $total_data;
			if(count($data['totals'])>=1){
				$res['sub_total'] 		= $data['totals'][0]['text'];
				$res['sub_total_value'] = $data['totals'][0]['value'];
				
				$res['tax'] =$tax['text'];
				$res['tax_value'] =$tax['value'];
				if($tax_class_id=='1'){
					$res['tax'] 	  ='';
					$res['tax_value'] ='';
				}

				if($apply_tax_type == '2'){
					$res['sub_total_value'] = $data['totals'][0]['value'] - $tax['value'];
					$res['sub_total'] 		= $this->currency->format(max(0, $res['sub_total_value']));
				}
				$cnt = count($data['totals'])-1;
				$res['total'] 			= $data['totals'][$cnt]['text'];
				$res['total_value'] 	= $data['totals'][$cnt]['value'];
				if($data['totals'][1]['code']=='discount'){
					$res['discount'] 		= $data['totals'][1]['text'];
					$res['discount_value']  = $data['totals'][1]['value'];
				}else{
					$res['discount'] 		= 0;
				}
			}

			$data = array('qty'		=> $productQty, 
				'price'		=> $price, 
				'netPrice'	=> $netPrice, 
				'total'		=> $total, 
				'discnt'	=> $discnt,
				'sku'		=> $productId,
				'orderSubTotal'	=> $res['sub_total'],
				'orderTax'		=> $res['tax'],
				'ordertax_value'=> $res['tax_value'],
				'tax_type'		=> $apply_tax_type,
				'orderDiscount'	=> $res['discount'],
				'orderTotal'	=> $res['total'],
				'tax_str' 		=> '<tr id="TRtax">
	                <td align="right" class="purchase_total_left" id="tax_title" colspan="7"></td>
	                <td class="purchase_total_right insertproduct">
	                	<input type="text" readonly="readonly" class="textbox row_total_box" value="'.$res['tax'].'" id="tax" name="tax" style="text-align:right;">
	                </td>
	            </tr>');
		$this->response->setOutput(json_encode($data));	
	}
	public function bulk_delete(){
		$this->load->model('transaction/sales');
		foreach ($this->request->post['selected'] as $purchase_id) {
			$this->model_transaction_sales->deletePurchase($purchase_id);
		}
		$this->session->data['success'] = 'Sales Details deleted Completed';
		if(isset($_REQUEST['filter_date_from'])){
			$pageUrl.= '&filter_date_from='.$_REQUEST['filter_date_from'];
		}
		if(isset($_REQUEST['filter_date_to'])){
			$pageUrl.= '&filter_date_to='.$_REQUEST['filter_date_to'];
		}
		if(isset($_REQUEST['filter_location_code'])){
			$pageUrl.='&filter_location_code='.$_REQUEST['filter_location_code']; 
		}
		if(isset($_REQUEST['filter_supplier'])){
			$pageUrl.='&filter_supplier='.$_REQUEST['filter_supplier']; 
		}
		if(isset($_REQUEST['filter_transactionno'])){
			$pageUrl.='&filter_transactionno='.$_REQUEST['filter_transactionno']; 
		}

		$this->redirect($this->url->link('transaction/sales', 'token=' . $this->session->data['token'] . $pageUrl.$url, 'SSL'));
	}
	public function payment_link(){ 
		$this->load->model('transaction/sales');
		$this->load->model('user/user');

		if(isset($this->request->get['purchase_id'])){
			$salesHeader = $this->model_transaction_sales->getSalesInvoiceByinvoice($this->request->get['purchase_id']);
			$customers = $this->model_transaction_sales->getB2BCustomersbyCode($salesHeader['customer_code']);
			$company = $this->model_transaction_sales->getcompanyDetails();

			$strTemplate  = '<p>Hi '.$customers['name'].',<br><br></p> <p>Attached is a receipt for your payment of '.$salesHeader['currency_code'].' '.$salesHeader['net_total'].' Payment Link:<a href="'.HTTPS_SERVER.'payment/invoice.php?id='.base64_encode($this->request->get['purchase_id']).'">click here</a><br><br>If you have any questions, Please let us know.<br><br>Thanks,<br>'.$company['name'];
			$subject = 'Receipt for '.$customers['name'].' for '.$salesHeader['currency_code'].' '.$salesHeader['net_total'].' From '.$company['name'];
			
        	$postArr['Content'] = $strTemplate;
        	$postArr['Subject'] = $subject;
        	$postArr['Name'] 	= $customers['name'];
        	$postArr['Email']  	= $customers['email'];
			$this->mail->sendEmail($postArr, 'paymentMail');
			$postArr['link'] 	= HTTPS_SERVER.'payment/invoice.php?id='.base64_encode($this->request->get['purchase_id']);
 			$this->response->setOutput(json_encode($postArr));
		}
	}	
	
	public function cancelOrder(){ 
		$this->load->model('transaction/sales');
		$cancel = '';
		if(isset($this->request->post['invoice_no'])){
			$invoice = $this->model_transaction_sales->getSalesInvoice($this->request->post['invoice_no']);
			   $cancel = $this->model_transaction_sales->cancelOrder($this->request->post['invoice_no'],$this->request->post['reason']);
			   $this->sendMailForCancelledOrder($this->request->post['invoice_no'],$this->request->post['reason']);
		}
		$this->response->setOutput($cancel);
	}

	public function bulk_cancel(){
		$this->load->model('transaction/sales');

		if(!empty($this->request->post['selected_invoice'])){
			foreach ($this->request->post['selected_invoice'] as $invoice_id) {
				$invoiceHeader = $this->model_transaction_sales->checkInvoiceCanceled($invoice_id);

				if($invoiceHeader['delivery_status'] == 'Pending' && $invoiceHeader['payment_status'] != 'Paid' && $invoiceHeader['payment_status'] != 'Partial' && $invoiceHeader['xero_sales_id'] == ''){
					$this->model_transaction_sales->cancelOrder($invoiceHeader['invoice_no'],'Bulk cancel by '.$this->session->data['username']);
					$this->sendMailForCancelledOrder($invoiceHeader['invoice_no'],'Bulk cancel by '.$this->session->data['username']);
				}
			}
		}
		echo "done"; die;
	}

	public function getDoNo(){

		$this->load->model('transaction/sales');	
	    $transNumberWithSuffix = $this->model_transaction_sales->getSalesDoAutoId();
		$transNumber		   = $transNumberWithSuffix;
		if($transNumber){
			 $transaction_no = $transNumber+1;
			 $this->data['transaction_no'] = $this->config->get('config_sales_do_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);
		}else{
			$transaction_no = 1;
			$this->data['transaction_no'] = $this->config->get('config_sales_do_prefix').date('ym').str_pad($transaction_no, 4, '0', STR_PAD_LEFT);					
		}
		return $this->data['transaction_no'];
	}

	public function doStatusUpdate(){

		$this->load->model('transaction/sales');
		$status = '';
		if(isset($this->request->post['product_id'])){
			$status = $this->model_transaction_sales->doStatusUpdate($this->request->post['product_id'],$this->request->post['status']);
			$this->cart->updateSalesInvoiceDeliveryStatus($this->request->post['invoice_no']);
		}
		$this->response->setOutput($status);
	}
	public function sendMailForCancelledOrder($invoice_no,$reason){ 
		$this->load->model('transaction/sales');
		$this->load->model('setting/email');
		$this->load->model('user/user');

		$cc 		= $this->model_setting_email->getEmailDetails();
		$company 	= $this->model_transaction_sales->getcompanyDetails();
		$strTemplate= '<p>Dear Admin ,<br><br></p> <p>Sales Order #'.$invoice_no.' cancelled by '.$this->session->data['username'].' on '.date('d/m/Y').', due to '.$reason;
		$subject 	= 'Sales invoice No: '.$invoice_no.' Cancelled';

    	$postArr['Content']   = $strTemplate;
    	$postArr['Subject']   = $subject;
    	$postArr['Name'] 	  = $this->session->data['username'];
    	$postArr['Email']  	  = $company['email'];
		$postArr['Cc']  	  = $cc;
		$this->mail->sendEmail($postArr);
		return true;
	}
	public function testEmail(){
		if($this->request->get['invoice_no']){
			$this->sendPdf_toCustomer($this->request->get['invoice_no']);
		}
	}
	public function sendPdf_toCustomer($invoice_no){
		$this->load->model('transaction/sales');

		$salesHeader = $this->model_transaction_sales->getSalesInvoiceByinvoice($invoice_no);
		$customers 	 = $this->model_transaction_sales->getB2BCustomersbyCode($salesHeader['customer_code']);
		$company 	 = $this->model_transaction_sales->getcompanyDetails();
		$emailTemp 	 = $this->model_transaction_sales->getEmailTemplate('1');
		$strTemplate = $subject = $trading_name = $contact_name = '';

		if(!empty($emailTemp)){
			$trading_name = $company['name'];
			$link         = $company['web_url'];
			$email        = $company['email'];
			$phone        = $company['phone'];
			$contact_name = $customers['name'];
			$currency_code= $salesHeader['currency_code'] !='' ? $salesHeader['currency_code'] : ''; 
			$amount       = $salesHeader['net_total']; 
			$invoice_date = $salesHeader['header_remarks'] !='' ? date('d M Y',strtotime($salesHeader['header_remarks'])) : '';
			$break        = '<br>';

			$searchArr    = array('[invoice_number]','[trading_name]','[contact_name]','[currency_code]','[amount]','[invoice_date]','[break]','[link]','[email]','[phone]');
			$replaceArr  = array($invoice_no,$trading_name,$contact_name,$currency_code,$amount,$invoice_date,$break,$link,$email,$phone);
			
			$subject     = str_replace($searchArr, $replaceArr, $emailTemp['subject']);
			$strTemplate = str_replace($searchArr, $replaceArr, $emailTemp['description']);
		}

		$this->createSalesInvoicePDF($invoice_no);
		$postArr['file']    = DIR_SERVER.'download/salesinvoice.pdf';
    	$postArr['Content'] = $strTemplate;
    	$postArr['Subject'] = $subject;
    	$postArr['Name'] 	= $customers['name'];
    	$postArr['Email']  	= $customers['email'];
    	// printArray($strTemplate); die;

    	if($customers['email']!='' && $strTemplate!='' && SEND_EMAIL_TO_CUSTOMER){
			$this->mail->sendEmail($postArr);
    	}
		unlink(DIR_SERVER.'download/salesinvoice.pdf');
		return true;
	}
	public function createSalesInvoicePDF($invoice_no){
		
		//Delete old file before create invoice
		if(file_exists(DIR_SERVER.'download/salesinvoice.pdf')){
			unlink(DIR_SERVER.'download/salesinvoice.pdf');
		}
		$this->request->get['purchase_id'] = $invoice_no;
		$str = $this->download_pdf('1');
		if($str){
			$type = 'F';
			$filename = DIR_SERVER.'download/salesinvoice.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function sales_payment(){

		$this->language->load('transaction/purchase');
		$this->document->setTitle('Sales Payment');
		$this->load->model('transaction/sales');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('Sales');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->request->post['filter_from_date'])) {
			$filter_from_date = $this->request->post['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else if (isset($this->request->get['filter_from_date'])) {
			$filter_from_date = $this->request->get['filter_from_date'];
			$url .= '&filter_from_date=' . $filter_from_date;
		}else{
			$filter_from_date = date('d/m/Y');
		}
		$filter_to_date = date('d/m/Y');
		if (isset($this->request->post['filter_to_date'])) {
			$filter_to_date = $this->request->post['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}else if (isset($this->request->get['filter_to_date'])) {
			$filter_to_date = $this->request->get['filter_to_date'];
			$url .= '&filter_to_date=' . $filter_to_date;
		}
		$filter_location = $this->session->data['location_code'];
		if (isset($this->request->post['filter_location'])) {
			$filter_location = $this->request->post['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}else if (isset($this->request->get['filter_location'])) {
			$filter_location = $this->request->get['filter_location'];
			$url .= '&filter_location=' . $filter_location;
		}
		$filter_transaction = null;
		if (isset($this->request->post['filter_transaction'])) {
			$filter_transaction = $this->request->post['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}else if (isset($this->request->get['filter_transaction'])) {
			$filter_transaction = $this->request->get['filter_transaction'];
			$url .= '&filter_transaction=' . $filter_transaction;
		}
		$filter_delivery_status = null;
		if (isset($this->request->post['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->post['filter_delivery_status'];
			$url .= '&filter_delivery_status=' . $filter_delivery_status;
		}else if (isset($this->request->get['filter_delivery_status'])) {
			$filter_delivery_status = $this->request->get['filter_delivery_status'];
			$url .= '&filter_delivery_status=' . $filter_delivery_status;
		}
		$filter_payment_status = null;
		if (isset($this->request->post['filter_payment_status'])) {
			$filter_payment_status = $this->request->post['filter_payment_status'];
			$url .= '&filter_payment_status=' . $filter_payment_status;
		}else if (isset($this->request->get['filter_payment_status'])) {
			$filter_payment_status = $this->request->get['filter_payment_status'];
			$url .= '&filter_payment_status=' . $filter_payment_status;
		}
		$filter_xero = '0';
		if (isset($this->request->post['filter_xero'])) {
			$filter_xero = $this->request->post['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}else if (isset($this->request->get['filter_xero'])) {
			$filter_xero = $this->request->get['filter_xero'];
			$url .= '&filter_xero=' . $filter_xero;
		}
		$filter_network = null;
		if (isset($this->request->post['filter_network'])) {
			$filter_network = $this->request->post['filter_network'];
			$url .= '&filter_network=' . $filter_network;
		}else if (isset($this->request->get['filter_network'])) {
			$filter_network = $this->request->get['filter_network'];
			$url .= '&filter_network=' . $filter_network;
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales Payment List',
			'href'      => $this->url->link('transaction/sales_invoice/sales_payment', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['back_button'] = $this->url->link('transaction/sales_invoice/sales_payment', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['purchases'] = array();
		$data = array(
			'filter_from_date'	  => $filter_from_date,
			'filter_to_date'	  => $filter_to_date,
			'filter_location'	  => $filter_location,
			'filter_transaction'  => $filter_transaction,
			'filter_delivery_status'  => $filter_delivery_status,
			'filter_payment_status'  => $filter_payment_status,
			'filter_network'  => $filter_network,
			'filter_xero'  => '1',
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit')
		);

		$this->data['filters'] = $data;
		$Sales_total = $this->model_transaction_sales->getTotalSalesInvoice($data);
		$results     = $this->model_transaction_sales->getSalesInvoiceList($data);
		$this->data['networks']     = $this->cart->getChannelsList();
		// printArray($url); die;

		$this->load->model('user/user');
		$this->load->model('setting/customers');
		$vendorDetail = array();
		$userDetail = array();
		foreach ($results as $result) {
			$vendorDetail = $this->model_transaction_sales->getB2BCustomersbyCode($result['customer_code']);
			$userDetail   = $this->model_user_user->getUser($result['createdby']);
			$signature    = '';//$this->model_transaction_sales->getOrderSignature($result['id']);

			$this->data['purchases'][] = array(
				'invoice_no'       => $result['invoice_no'],
				'order_id'          => $result['id'],
				'transaction_no'    => $result['invoice_no'],
				'xero_sales_id' 	=> $result['xero_sales_id'],
				'transaction_date'  => date('d/m/Y',strtotime($result['invoice_date'])),
				'transaction_type'  => $result['discount_type'],
				'delivery_status'   => $result['delivery_status'],
				'payment_status'    => $result['payment_status'],
				'status'            => $result['status'],
				'signature'  	    => $signature,
				'vendor_code'       => $vendorDetail['customercode'],
				'location'          => $result['location_code'],
				'vendor_name'       => $vendorDetail['name'],
				'total'             => $result['net_total'],
				'created_by'        => $userDetail['firstname'].' '.$userDetail['lastname'],
				'created_date'      => date_format($result['createdon'], 'd-m-Y g:i A'),
				'view_button'       => $this->url->link('transaction/sales_invoice/view', 'token=' . $this->session->data['token'] . '&purchase_id='.$result['invoice_no'].'&type=payment_view'. $url, 'SSL'),
				'multiple_do'       => $this->url->link('transaction/sales_invoice/multiple_do', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'payment_view_button'=> $this->url->link('transaction/sales_invoice/payment_view', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'modify_button'     => '',
				'download_button'   => $this->url->link('transaction/sales_invoice/download_pdf', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL'),
				'payment_url'	    => $this->url->link('transaction/sales_invoice/payment_link', 'token=' . $this->session->data['token'] . '&purchase_id=' . $result['invoice_no'] . $url, 'SSL')
			);
		}
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		// $this->data['delete']= $this->url->link('transaction/sales_invoice/bulk_delete', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		// $this->data['insert']= $this->url->link('transaction/sales_invoice/insert', 'token=' . $this->session->data['token'].$pageUrl.$url, 'SSL');
		
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total   = $Sales_total;
		$pagination->page    = $page;
		$pagination->limit   = $this->config->get('config_admin_limit');
		$pagination->text    = $this->language->get('text_pagination');
		$pagination->url     = $this->url->link('transaction/sales_invoice', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = $this->request->get['route'];

		$this->template = 'transaction/sales_payment_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	} 
	public function validateRefNo(){
		$this->load->model('transaction/sales');
		if($this->request->post['ref'] !=''){
	    	$count    = $this->model_transaction_sales->validateRefNo($this->request->post,'SI');
	    	if($count<= 0){
	    		$resp = true;
	    	}else{
	    		$resp = false;
	    	}
		}
		echo $resp; die;
	}
	public function cancelPartiallInvoice(){
		$this->load->model('transaction/sales');
		$this->load->model('ajax/cart');

		try {
			$this->clearPurchaseData();
			unset($this->session->data['']);
		    $this->model_transaction_sales->cancelPartiallInvoice($this->request->post);
		    $data['invoice_no'] = $this->request->post['invoice_no'];
		    $data['order_no'] 	= $this->request->post['order_no'];
		    $cart_type 			= 'cart_purchase';
			// $this->request->post['invoice_no'] = 'SI21082323';         
			$invoiceHeader 		= $this->model_transaction_sales->getSalesInvoice($data['invoice_no']);
			$customerDetails    = $this->model_transaction_sales->getCustomerDetails($invoiceHeader['customer_code']);
			$apply_tax_type     = 0;
			$tax_class_id	    = 0;
			if(!$customerDetails['tax_allow']){
				$apply_tax_type = $customerDetails['tax_type'] == '1' ? '1' : '2';
				$tax_class_id	= '2';
			}

			$salesDetails = $this->model_transaction_sales->getSalesInvoiceDetails($this->request->post['invoice_no']);
			if(!empty($salesDetails)){
				foreach ($salesDetails as $details) {
					if($details['cancel'] !='1'){
						$details['price']        = $details['sku_price'];
						$details['tax_class_id'] = $tax_class_id;

						if(!empty($this->session->data['cart_purchase'])){
							if(array_key_exists($details['product_id'], $this->session->data['cart_purchase']) && ($details['qty'] > 0)) {
								$this->cart->update($details['product_id'], $details['qty'], $cart_type, $details);
						   	} elseif ($details['qty'] > 0 ) {
						       $this->cart->add($details['product_id'], $details['qty'], '', '', '', $cart_type, $details);
						   	}
						} elseif ($details['qty'] > 0 ) {
					        $this->cart->add($details['product_id'], $details['qty'], '', '', '', $cart_type, $details);
					   	}
					}
				}
			}
			$data['products'] = $this->cart->getProducts($cart_type,$apply_tax_type);
			$data['totals']   = $this->model_ajax_cart->getTotalsCollection($apply_tax_type);
			$data['conversion_rate'] = $invoiceHeader['conversion_rate'];
			$this->model_transaction_sales->UpdateSalesInvoice($data);
		    $resArr = array('status'=> true);
		
		} catch (Exception $e) {
			$errMsg  = SHOW_EXCEPTION ? $e->getMessage() : WRONG_MESSAGE;
		    $resArr  = array('status'=> false , 'err'=> $errMsg);
		}
		$this->response->setOutput(json_encode($resArr));
	}

	public function untagProduct(){
		$this->load->model('transaction/sales');

		try {
			$this->model_transaction_sales->untagProduct($this->request->post);
		    $data['invoice_no'] = $this->request->post['invoice_no'];
		    $data['order_no'] 	= $this->request->post['order_no'];

		    $resArr = array('status'=> true);
		} catch (Exception $e) {
			$errMsg = SHOW_EXCEPTION ? $e->getMessage() : WRONG_MESSAGE;
		    $resArr = array('status'=> false, 'err'=> $errMsg);
		}
		$this->response->setOutput(json_encode($resArr));
	}
}
?>