<?php 
class ControllerCustomerscustomersb2b extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('customers/customer');

		$this->document->setTitle($this->language->get('customer_pricing_title'));

		$this->load->model('customers/customersb2b');

		$this->getList();
		
	}

	public function insert() {

		$this->language->load('customers/customer');

		$this->document->setTitle($this->language->get('customer_pricing_title'));

		$this->load->model('customers/customersb2b');

		$type = 'insert';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type,$this->request->get['customer_id'])) {

			$insert = $this->model_customers_customersb2b->addB2BCustomer($this->request->post);


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

		    $this->redirect($this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . $url, 'SSL'));
							
		}
		$this->getForm();		
	}


	public function update() {

		$this->language->load('customers/customer');

		$this->document->setTitle($this->language->get('customer_pricing_title'));
		
		$this->load->model('customers/customersb2b');
		$type = 'update';
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type,$this->request->get['customer_id'])) {

			$this->model_customers_customersb2b->editB2BCustomer($this->request->post,$this->request->get['customer_id']);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . $url, 'SSL'));									
		}

		$this->getForm();
	}

	public function delete() {
    	$this->language->load('customers/customer');
 
		$this->document->setTitle($this->language->get('customer_pricing_title'));
		
		$this->load->model('customers/customersb2b');
		//print_r($this->request->get); exit;
		if (isset($this->request->get['customer_id'])) {
		
				$this->model_customers_customersb2b->deleteB2BCustomer($this->request->get['customer_id']);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}


	public function getList(){

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/	
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
			$url.= '&filter_name='.$this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
			$url.= '&filter_email='.$this->request->post['filter_email'];
		} else {
			$filter_email ="";
		}
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('customers/customersb2b/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('customers/customersb2b/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_email' => $filter_email,
		);
		$customer_total = $this->model_customers_customersb2b->getTotalB2BCustomers($data);
		$results = $this->model_customers_customersb2b->getB2BCustomers($data);

		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('customers/customersb2b/view', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL'),
				'delete' => $this->url->link('customers/customersb2b/delete', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL'),
				'edit' => $this->url->link('customers/customersb2b/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customercode'		=> $result['customercode'],
				'customer_id' 		=> $result['customer_id'],
				'name'      	    => $result['name'],
				'email'				=> $result['primary_email'],
				'mobile'			=> $result['mobile'],
				'added_date'		=> $result['added_date']->format('d-m-Y'),				
				'selected'          => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
				'action'            => $action
			);
		}
		
		$this->data['customer_pricing_title'] = $this->language->get('customer_pricing_title');
		$this->data['text_no_results']	= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Customers',
			'href'      => $this->url->link('customers/customers', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('B2B Customer List'),	
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'customers/customerb2b_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}

	public function getForm() {

		$this->data['customer_pricing_title'] = $this->language->get('customer_pricing_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_address'] = $this->language->get('entry_address');
		$this->data['entry_country'] = $this->language->get('entry_country');
		$this->data['entry_zipcode'] = $this->language->get('entry_zipcode');
		$this->data['entry_phone'] = $this->language->get('entry_phone');
		$this->data['entry_fax'] = $this->language->get('entry_fax');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_attn'] = $this->language->get('entry_attn');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_gst'] = $this->language->get('entry_gst');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}		

		if (isset($this->error['primary_email'])) {
			$this->data['error_primary_email'] = $this->error['primary_email'];
		} else {
			$this->data['error_primary_email'] = '';
		}

		if (isset($this->error['mobile'])) {
			$this->data['error_mobile'] = $this->error['mobile'];
		} else {
			$this->data['error_mobile'] = '';
		}

		if (isset($this->error['zipcode'])) {
			$this->data['error_zipcode'] = $this->error['zipcode'];
		} else {
			$this->data['error_zipcode'] = '';
		}	


		if (isset($this->error['website'])) {
			$this->data['error_website'] = $this->error['website'];
		} else {
			$this->data['error_website'] = '';
		}
		// printArray($this->data);die;
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Customers',
			'href'      => $this->url->link('customers/customers', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('B2B Customer List'),
			'href'		=> $this->url->link('customers/customersb2b', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['cancel'] = $this->url->link('customers/customersb2b', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['customer_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$customer_info = $this->model_customers_customersb2b->getB2BCustomer($this->request->get['customer_id']);
		}				

		if (!empty($customer_info))	 {
			$this->data['added_date'] = $customer_info['added_date'];
		}

		if (isset($this->request->post['discount'])) {
			$this->data['discount'] = $this->request->post['discount'];
		} elseif (!empty($customer_info)) {
			$this->data['discount'] = $customer_info['discount'];
		} else {
			$this->data['discount'] = '';
		}

		if (isset($this->request->post['salestax'])) {
			$this->data['salestax'] = $this->request->post['salestax'];
		} elseif (!empty($customer_info)) {
			$this->data['salestax'] = $customer_info['salestax'];
		} else {
			$this->data['salestax'] = '';
		}

		if(isset($this->request->post['tax_allow'])){
			$this->data['tax_allow'] = $this->request->post['tax_allow'];
		} elseif(!empty($customer_info)) {
			$this->data['tax_allow'] = $customer_info['tax_allow'];	
		} else {
			$this->data['tax_allow'] == '';
		}

		if(isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif(!empty($customer_info)) {
			$this->data['status'] = $customer_info['status'];
		} else {
			$this->data['status'] == '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($customer_info)) {
			$this->data['name'] = trim($customer_info['name']);
		} else {
			$this->data['name'] = '';
		}		
		
		if (isset($this->request->post['customer_title'])) {
			$this->data['customer_title'] = $this->request->post['customer_title'];
		} elseif (!empty($customer_info)) {
			$this->data['customer_title'] = trim($customer_info['customer_title']);
		} else {
			$this->data['customer_title'] = '';
		}

		if (isset($this->request->post['company'])) {
			$this->data['company'] = $this->request->post['company'];
		} elseif (!empty($customer_info)) {
			$this->data['company'] = trim($customer_info['company']);
		} else {
			$this->data['company'] = '';
		}

		if (isset($this->request->post['birth_date'])) {
			$this->data['birth_date'] = $this->request->post['birth_date'];
		} elseif (!empty($customer_info)) {
			$this->data['birth_date'] = $customer_info['birth_date']->format('d-m-Y');
		} else {
			$this->data['birth_date'] = '';
		}

		if (isset($this->request->post['home'])) {
			$this->data['home'] = $this->request->post['home'];
		} elseif (!empty($customer_info)) {
			$this->data['home'] = trim($customer_info['home']);
		} else {
			$this->data['home'] = '';
		}

		if (isset($this->request->post['work'])) {
			$this->data['work'] = $this->request->post['work'];
		} elseif (!empty($customer_info)) {
			$this->data['work'] = trim($customer_info['work']);
		} else {
			$this->data['work'] = '';
		}

		if (isset($this->request->post['mobile'])) {

			$this->data['mobile'] = $this->request->post['mobile'];
		} elseif (!empty($customer_info)) {
			$this->data['mobile'] = trim($customer_info['mobile']);
		} else {
			$this->data['mobile'] = '';
		}
		if (isset($this->request->post['pager'])) {
			$this->data['pager'] = $this->request->post['pager'];
		} elseif (!empty($customer_info)) {
			$this->data['pager'] = trim($customer_info['pager']);
		} else {
			$this->data['pager'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$this->data['fax'] = $this->request->post['fax'];
		} elseif (!empty($customer_info)) {
			$this->data['fax'] = trim($customer_info['fax']);
		} else {
			$this->data['fax'] = '';
		}

		if (isset($this->request->post['address1'])) {
			$this->data['address1'] = $this->request->post['address1'];
		} elseif (!empty($customer_info)) {
			$this->data['address1'] = trim($customer_info['address1']);
		} else {
			$this->data['address1'] = '';
		}

		if (isset($this->request->post['address2'])) {
			$this->data['address2'] = $this->request->post['address2'];
		} elseif (!empty($customer_info)) {
			$this->data['address2'] = trim($customer_info['address2']);
		} else {
			$this->data['address2'] = '';
		}

		if (isset($this->request->post['city'])) {
			$this->data['city'] = $this->request->post['city'];
		} elseif (!empty($customer_info)) {
			$this->data['city'] = trim($customer_info['city']);
		} else {
			$this->data['city'] = '';
		}

		if (isset($this->request->post['state'])) {
			$this->data['state'] = $this->request->post['state'];
		} elseif (!empty($customer_info)) {
			$this->data['state'] = trim($customer_info['state']);
		} else {
			$this->data['state'] = '';
		}

		if (isset($this->request->post['zipcode'])) {
			$this->data['zipcode'] = $this->request->post['zipcode'];
		} elseif (!empty($customer_zipcode)) {
			$this->data['zipcode'] = trim($customer_zipcode['zipcode']);
		} else {
			$this->data['zipcode'] = '';
		}

		if (isset($this->request->post['website'])) {
			$this->data['website'] = $this->request->post['website'];
		} elseif (!empty($customer_info)) {
			$this->data['website'] = trim($customer_info['website']);
		} else {
			$this->data['website'] = '';
		}

		if (isset($this->request->post['primary_email'])) {
			$this->data['primary_email'] = $this->request->post['primary_email'];
		} elseif (!empty($customer_info)) {
			$this->data['primary_email'] = trim($customer_info['primary_email']);
		} else {
			$this->data['primary_email'] = '';
		}

		if (isset($this->request->post['secondary_email'])) {
			$this->data['secondary_email'] = $this->request->post['secondary_email'];
		} elseif (!empty($customer_info)) {
			$this->data['secondary_email'] = trim($customer_info['secondary_email']);
		} else {
			$this->data['secondary_email'] = '';
		}

		if (isset($this->request->post['custom'])) {
			$this->data['custom'] = $this->request->post['custom'];
		} elseif (!empty($customer_info)) {
			$this->data['custom'] = trim($customer_info['custom']);
		} else {
			$this->data['custom'] = '';
		}

		if (isset($this->request->post['tag'])) {
			$this->data['tag'] = $this->request->post['tag'];
		} elseif (!empty($customer_info)) {
			$this->data['tag'] = trim($customer_info['tag']);
		} else {
			$this->data['tag'] = '';
		}

		if (isset($this->request->post['do_not'])) {
			$this->data['do_not'] = $this->request->post['do_not'];
		} elseif (!empty($customer_info)) {
			$this->data['do_not'] = unserialize($customer_info['do_not']);
		} else {
			$this->data['do_not'] = '';
		}
		
		if (isset($this->request->post['notes'])) {
			$this->data['notes'] = $this->request->post['notes'];
		} elseif (!empty($customer_info)) {
			$this->data['notes'] = trim($customer_info['notes']);
		} else {
			$this->data['notes'] = '';
		}

		if (isset($this->request->post['customer_code'])) {
			$this->data['customer_code'] = $this->request->post['customer_code'];
		} elseif (!empty($customer_info)) {
			$this->data['customer_code'] = trim($customer_info['customercode']);
		} else {
			$this->data['customer_code'] = $this->config->get('customer_prefix').str_pad($this->config->get('next_member_no'), 4,0,STR_PAD_LEFT);
		}
		$this->data['taxrates'] = $this->model_customers_customersb2b->getTaxRates();
		$this->data['discounts'] = $this->model_customers_customersb2b->getDiscounts();

		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'customers/customersb2b';

		$this->template = 'customers/customerb2b_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getTypeDetails()
	{
		$this->load->model('customers/customersb2b');
		$customertype_id = $this->request->post["customer_type"];    	
     	$TypeDetails = $this->model_customers_customersb2b->getTypeDetailsList($customertype_id);
     	echo json_encode($TypeDetails);     	
	}

	public function validateForm($type='',$customer_id='')
	{
		if (!$this->user->hasPermission('modify', 'customers/customersb2b')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
        
		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('Enter Name');
		}	
		
		if (empty($this->request->post['primary_email']) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['primary_email'])) {
			$this->error['primary_email'] = $this->language->get('Enter Primary Email');
		}

		$user_info = $this->model_customers_customersb2b->getCustomerb2bByEmail($this->request->post['primary_email']);
		if(!isset($this->request->get['customer_id'])){
			if (($this->request->post['primary_email'] !='') && ($user_info)){
				 $this->error['warning'] = $this->language->get('Email Already Registered');
			}
		} else{
			if (($this->request->post['primary_email'] !='') && ($user_info) && $user_info['customer_id']!=$this->request->get['customer_id']){
				$this->error['warning'] = $this->language->get('Email Already Registered');
			}
		}

		if (empty($this->request->post['mobile'])) {
			$this->error['mobile'] = $this->language->get('Enter mobile');
		}else{
			$user_info = $this->model_customers_customersb2b->getCustomerb2bByPhone($this->request->post['mobile'],$customer_id);
			if($user_info){
				$this->error['warning'] = 'Mobile no already exist';
			}
		}

		if(empty($this->request->post['customer_code'])) {
			$this->error['warning'] = 'Customer code should not empty';
		}else{
			if($customer_id==''){
			 $user_info = $this->model_customers_customersb2b->getCustomerb2bByCode($this->request->post['customer_code']);
			 if($user_info){
				$this->error['warning'] = 'Customer code Already Exist';
			 }
			}
		}
        
		if (empty($this->request->post['mobile'])) {
			$this->error['zipcode'] = $this->language->get('Enter Zipcode');
		}

		if (($this->request->post['secondary_email'] != '') && !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['secondary_email'])) {
			$this->error['secondary_email'] = $this->language->get('Enter Secondary Email');
		}

		if (($this->request->post['website']) != '' && !preg_match('/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i',$this->request->post['website'])) {
			$this->error['website'] = "Please Enter Valid Web Address";
		}
			
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	
	
  	


	
	
	
	
	
}
?>