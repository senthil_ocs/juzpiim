<?php 
class ControllerCustomersCustomerspricing extends Controller {
	private $error = array();
	public function index() {
		$this->language->load('customers/customer');
		$this->document->setTitle($this->language->get('customer_pricing_title'));
		$this->load->model('customers/customerspricing');
		$this->getList();
	}

	public function insert() {

		$this->language->load('customers/customer');
		$this->document->setTitle($this->language->get('customer_pricing_title'));
		$this->load->model('customers/customerspricing');
     
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data 			 = $this->request->post;
			$customerpriceid = $this->model_customers_customerspricing->getcustomerDetailsBypriceid();
            $customerpriceid = $customerpriceid =='' ? rand(1000,9999) : $customerpriceid +1;
			$term_id =$this->model_customers_customerspricing->addCustomerpricing($data,$customerpriceid);
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			if($term_id == flase){
				$this->session->data['success'] = 'Customer name exists';
				$this->redirect($this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
				$this->session->data['success'] = 'Customer price Added success';
				$this->redirect($this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}		
		}
		$this->getForm();		
	}
	public function update() {

		$this->load->model('customers/customerspricing');
		$this->document->setTitle($this->language->get('customer_pricing_title'));	
		$this->load->model('customers/customerspricing');
		$this->data['page_action'] = 'edit';
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$customerpriceid = $this->model_customers_customerspricing->getcustomerDetailsBypriceid();
            $customerpriceid = $customerpriceid == '' ? rand(1000,9999) : $customerpriceid +1;
			$this->model_customers_customerspricing->editCustomerpricing($this->request->post,$customerpriceid);
			$this->session->data['success'] ='Customer price Update success';
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			$this->redirect($this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}else{
			$this->data['page_action'] ='edit';
		}
		$this->getForm();
	}

	public function delete() {

    	$this->language->load('customers/customer');
		$this->document->setTitle($this->language->get('customer_pricing_title'));
		$this->load->model('customers/customerspricing');
		if (isset($this->request->get['customers_price_id'])) {
			$this->model_customers_customerspricing->deleteCustomerpricing($this->request->get['customers_price_id']);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$this->redirect($this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
  	}
	public function getList(){
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->post['filter_customer'])) {
			$filter_customer = $this->request->post['filter_customer'];
			$this->data['filter_customer'] = $filter_customer;
		} else {
			$filter_customer = null;
			$this->data['filter_customer'] = '';
		}
		if (isset($this->request->post['filter_location'])) {
			$filter_location = $this->request->post['filter_location'];
		} else {
			$filter_location = $this->session->data['location_code'];
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
			$this->data['filter_date_from'] = $filter_date_from;
		} else {
			$filter_date_from = null;
			$this->data['filter_date_from'] = '';
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
			$this->data['filter_date_to'] = $filter_date_to;
		} else {
			$filter_date_to = null;
			$this->data['filter_date_to'] = '';
		}
		if (isset($this->request->post['filter_sku'])) {
			$filter_sku = $this->request->post['filter_sku'];
		} else {
			$filter_sku = '';
		}
		$this->data['filter_sku'] = $filter_sku;
		$this->data['filter_location'] = $filter_location;
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_terminal_name'])) {
			$filter_terminal_name = $this->request->post['filter_terminal_name'];
			$url.= '&filter_terminal_name='.$this->request->post['filter_terminal_name'];
		} else {
			$filter_terminal_name ="";
		}

		$this->data['insert'] 		= $this->url->link('customers/customerspricing/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] 		= $this->url->link('customers/customerspricing/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['Tolocations'] 	= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['terminals'] 	= array();

		$data = array(
			'sort'  			=> $sort,
			'order' 			=> $order,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_sku'		=> $filter_sku,
			'start' 			=> ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' 			=> $this->config->get('config_admin_limit'),
			'filter_terminal_name' => $filter_terminal_name
		);

		$custprice_total = $this->model_customers_customerspricing->getTotalCustomerpricing($data);
		$results = $this->model_customers_customerspricing->getCustomerpricing($data);

		foreach ($results as $result) {

		    $date='&Fromdate='.$result['FromDate'].'&Todate='.$result['ToDate'];
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('customers/customerspricing/view', 'token=' . $this->session->data['token'] . '&customers_code=' . $result['customers_code'] . $url, 'SSL'),
				'delete' => $this->url->link('customers/customerspricing/delete', 'token=' . $this->session->data['token'] . '&customers_price_id=' . $result['customers_price_id'] . $url, 'SSL'),
				'edit' => $this->url->link('customers/customerspricing/update', 'token=' . $this->session->data['token'] . '&customers_code=' . $result['customers_code'] .$date. $url, 'SSL')
			);
			$this->load->model('setting/location');
			$this->data['location_list'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
			$this->data['terminals'][] = array(
				'custmercode'	=> $result['customers_code'],
				'name'		    => $result['name'],
				'location'		=> $result['location_code'],
				'FromDate'		=> $result['FromDate'],
				'ToDate'		=> $result['ToDate'],
				'sku'		    => $result['sku'],				
				'customerprice'	=> $result['customer_price'],				
				'CreatedBy'		=> $result['added_by'],				
				'action'       	=> $action
			);
		}
		
		$this->data['heading_title'] = $this->language->get('customer_pricing_title');
		$this->data['text_no_results']= $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customers pricing List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		//$this->data['sort_terminal_name'] = $this->url->link('setting/terminal', 'token=' . $this->session->data['token'] . '&sort=terminal_name' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $custprice_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] 	  = $sort;
		$this->data['order'] 	  = $order;
		$this->data['route'] 	  = $this->request->get['route'];

		$this->template = 'customers/customersprice_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function deletepricedetails()
	{
		$this->load->model('customers/customerspricing');
        //echo $_POST['sku']; exit;
			
		if(isset($_POST['sku'])){
			$deleteall = $this->model_customers_customerspricing->deletepricedetailsbysku($_POST['sku']);
			if($deleteall){
				return true;
			}
			
		}
	}
	public function CustomerspriceDelete()
	{
		$this->load->model('customers/customerspricing');
		if($this->request->get['customers_id']){
			$deleteall = $this->model_customers_customerspricing->deleteCustomerpricing($this->request->get['customers_id']);
			//$deleteall =  $this->model_customers_customerspricing->deleteAllpromotion($this->request->get['promotion_code']);
			if($deleteall){
				return true;
			}
			/*if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
		    }*/ 
		}
	}
	public function getForm() {
		
		$this->load->model('customers/customerspricing');

		$this->data['heading_title'] = $this->language->get('customer_pricing_title');
		$this->data['entry_terminal_name'] = $this->language->get('entry_terminal_name');
		$this->data['entry_system_name'] = $this->language->get('entry_system_name');
		$this->data['entry_cash_machine'] = $this->language->get('entry_cash_machine');
		$this->data['entry_cash_machine_port'] = $this->language->get('entry_cash_machine_port');
		$this->data['entry_coin_machine'] = $this->language->get('entry_coin_machine');
		$this->data['entry_coin_machine_port'] = $this->language->get('entry_coin_machine_port');
		$this->data['entry_Cust_display'] = $this->language->get('entry_Cust_display');
		$this->data['entry_Cust_Display_Port'] = $this->language->get('entry_Cust_Display_Port');
		$this->data['entry_Cust_Display_Type'] = $this->language->get('entry_Cust_Display_Type');
		$this->data['entry_Cust_Display_Msg1'] = $this->language->get('entry_Cust_Display_Msg1');
		$this->data['entry_Cust_Display_Msg2'] = $this->language->get('entry_Cust_Display_Msg2');
		$this->data['entry_speaker_enabled'] = $this->language->get('entry_speaker_enabled');
		$this->data['entry_weighingscale_enabled'] = $this->language->get('entry_weighingscale_enabled');
		$this->data['entry_weighingscale_port'] = $this->language->get('entry_weighingscale_port');
		$this->data['entry_last_sync_date'] = $this->language->get('entry_last_sync_date');
		$this->data['button_add_barcode'] = $this->language->get('button_add_barcode');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_add_image'] = $this->language->get('button_add_image');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['from_date'])) {
			$this->data['error_from_date'] = $this->error['from_date'];
		} else {
			$this->data['error_from_date'] = '';
		}
		if (isset($this->error['to_date'])) {
			$this->data['error_to_date'] = $this->error['to_date'];
		} else {
			$this->data['error_to_date'] = '';
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer pricing List'),
			'href'		=> $this->url->link('customers/customerspricing', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		if (!isset($this->request->get['customers_code'])) {
			if(	$this->data['page_action'] == 'edit'){
				$this->data['action'] = $this->url->link('customers/customerspricing/edit', 'token=' . $this->session->data['token'] . $url, 'SSL');
			}else{
				$this->data['action'] = $this->url->link('customers/customerspricing/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
			}
		} else {
			$this->data['action'] = $this->url->link('customers/customerspricing/update', 'token=' . $this->session->data['token'] . '&customers_code=' . $this->request->get['customers_code'] . $url, 'SSL');

		}
		$this->data['cancel'] = $this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'] . $url, 'SSL');
		if (isset($this->request->get['customers_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$terminal_info = $this->model_customers_customerspricing->getCustomersTerminal($this->request->get);
		}
		if($this->data['page_action'] == 'edit'){
			$this->data['CusomersInfo'] = $this->model_customers_customerspricing->viewcustomerdetailsbycode($this->request->get,$this->request->get['customers_code']);		
		}
		if(isset($this->request->post['filter_customer'])){
			$this->data['filter_customer']  = $this->request->post['filter_customer'];
		} elseif (!empty($terminal_info)) {
			$this->data['filter_customer']  = $terminal_info['customers_code'];
		} else {			
			$this->data['filter_customer']  ='';
		}
		if(isset($this->request->post['filter_date_from'])) {
			$this->data['filter_date_from'] = $this->request->post['filter_date_from'];
		} elseif(!empty($this->request->get['Fromdate'])) {
			$this->data['filter_date_from'] = date("d-m-Y", strtotime($this->request->get['Fromdate']));
		} else {
			$this->data['filter_date_from'] == '';
		}
		if (isset($this->request->post['filter_date_to'])) {
			$this->data['filter_date_to'] = $this->request->post['filter_date_to'];
		} elseif (!empty($this->request->get['Todate'])) {
			$this->data['filter_date_to'] = date("d-m-Y", strtotime($this->request->get['Todate']));
		} else {
			$this->data['filter_date_to'] = '';
		}
		if (isset($this->request->post['filter_location'])) {
			$this->data['location_code']  = $this->request->post['filter_location'];
		} elseif (!empty($terminal_info)) {
			$this->data['location_code']  = $terminal_info['location_code'];
		} else {
			$this->data['location_code']  = '';
		}

		$this->load->model('setting/location');
		$this->data['locations'] 			= $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
		$this->data['user_location_code']  	= $this->session->data['location_code'];
		// Customers
		$this->data['customers_collection'] = $this->model_customers_customerspricing->getTotalCustomersname();
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection']= $this->model_inventory_department->getDepartments();
		// Category
		$this->load->model('inventory/category');			
		$this->data['category_collection'] = $this->model_inventory_category->getUsedCategorys();

		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'setting/promotions';
		
		if($this->data['page_action'] == 'edit'){
			$this->template  = 'customers/customersprice_edit_form.tpl';
		}else{
			$this->template  = 'customers/customersprice_form.tpl';
		}
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function ajaxGetCustomerpricingList(){

		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('customers/customerspricing');
        
        if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
		} 
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
		} 
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];		
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];		
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];		
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		}
		
		$filter_location = $this->session->data['location_code'];
		$data = array(
			'filter_date_from'	  => $filter_date_from, 
			'filter_date_to'	  => $filter_date_to, 
			'filter_customer'	  => $filter_customer, 
			'filter_department'	  => $filter_department, 
			'filter_category'	  => $filter_category,
			'filter_location_code'=> $filter_location,
			'filter_sku'          =>$filter_sku		
		);
		$this->data['data'] = $data;
		$skuDetails = array();
		$skuDetails = $this->model_customers_customerspricing->getSkuDetailsById($data);
		if(!empty($skuDetails)){
			foreach ($skuDetails as $res) {
				$existsku[] = $res['sku'];
			}
		}
		$results = $this->model_customers_customerspricing->getProductsReport($data);     
         foreach( $results as $key => $record )
		  {
		    if( !empty($existsku) && in_array( $results[$key]['sku'], $existsku ) )
		    {
		        unset( $results[ $key ] );  
		    }
		 }
     
		foreach ($results as $res) {
			$newres[] = $res;
		}
		if(count($newres)>=1){
			$newres = array_values(array_filter($newres));
		}
		
		$this->response->setOutput(json_encode($newres));
	}
	public function ajaxGetCategory(){
		$this->load->model('inventory/category');
		if($this->request->post['department_code']!=''){
			$category = $this->model_inventory_category->ajaxGetCategory($this->request->post['department_code']);	
		}
		$category_slect = '';
		if($this->request->post['category']!=''){
			$category_slect = $this->request->post['category'];
		}
		$str = "<option value=''>Select Category</option>";
		if(count($category)> 0){
			foreach ($category as $value) {
				$select  = '';
				if($value['category_code']==$category_slect){
					$select = 'Selected';
				}
				$str .="<option value='".$value['category_code']."' ".$select.">".$value['category_name']."</option>"; 
			}
		}
		echo $str;
	}
	public function ajaxGetExistingdataList()
	{
		$this->load->model('customers/customerspricing');
        
		if (isset($_REQUEST['filter_customer'])) {
			$filter_customer = $_REQUEST['filter_customer'];
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
		} 
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];		
		}
		

		//$filter_location = $this->session->data['location_code'];
		$data = array(
			'filter_customer'	  => $filter_customer, 
			'filter_date_from'	  => $filter_date_from,
			'filter_date_to'	  => $filter_date_to,
			'filter_name'         =>$filter_name		
		);
		$this->data['data'] = $data;
		 //print_r($this->data['data']); exit;
		$results = $this->model_customers_customerspricing->getExistingReport($data);
		foreach ($results as $res) {
			$newres[] = $res;
		}
		if(count($newres)>=1){
			$newres = array_values(array_filter($newres));
		}
		$this->response->setOutput(json_encode($newres));
	}
	public function array_flatten($array) { 
	  if (!is_array($array)) { 
	    return FALSE; 
	  } 
	  $result = array(); 
	  foreach ($array as $key => $value) { 
	    if (is_array($value)) { 
	      $result = array_merge($result,$this->array_flatten($value)); 
	    } 
	    else { 
	      $result[$key] = $value; 
	    } 
	  } 
	  return $result; 
	} 
	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'customers/customerspricing')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (empty($this->request->post['filter_customer'])) {
			$this->error['warning'] = $this->language->get('Select Customer (do not edit name after select!)');
		}
		if (empty($this->request->post['filter_date_from'])) {
			$this->error['warning'] = $this->language->get('Enter From Date');
		}
		if (empty($this->request->post['filter_date_to'])) {
			$this->error['warning'] = $this->language->get('Enter To Date');
		}
		if (empty($this->request->post['filter_location'])) {
			$this->error['warning'] = $this->language->get('Select loctions');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}


	public function view(){
		$this->language->load('customers/customer');
		$this->load->model('customers/customerspricing');
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('customer_pricing_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['show_hold'])) {
			$show_hold = $this->request->get['show_hold'];
			$this->data['hold'] = $this->request->get['show_hold'];
		} else {
			$show_hold = 0;
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Customer pricing',
			'href'      => $this->url->link('customers/customerspricing', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer pricing details View'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		
		$this->data['back_button'] 		= $this->url->link('setting/promotions', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['promotionInfo'] 	= $this->model_customers_customerspricing->getPromotionDetailsById($this->request->get['customers_code']);
		$this->data['PromotionDays'] 	= $this->model_customers_customerspricing->getDaysDetails($this->data['promotionInfo']['PromotionCode']);
		$this->data['skuDetails'] 	    = $this->model_customers_customerspricing->getSkuDetailsById($this->data['promotionInfo']['PromotionCode']);
		$this->data['skuCount'] 	    = $this->model_customers_customerspricing->getSkuCount($this->data['promotionInfo']['PromotionCode']);
		$this->template = 'customers/customersprice_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	  }

}

?>