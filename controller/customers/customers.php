<?php
class Controllercustomerscustomers extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('customers/common'); 
		$this->document->setTitle($this->language->get('heading_title'));	
		$this->data['location_code'] = $this->session->data['location_code'];
		
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_general'] = $this->language->get('text_general');	
		$this->data['text_sales']   = 'Supplier';

		if($this->user->hasPermission('access', 'customers/customers') || $this->user->hasPermission('modify', 'customers/customers')){
			$this->data["general"][] = array( "link" => $this->url->link("setting/customers", 'token=' . $this->session->data['token'], 'SSL'),
						                       "altkey"=> $this->language->get('alt_customers'),
						                       "id"     => $this->language->get('purchase_id'),
				                               "Text" => $this->language->get('text_customer'),
				                               "icon" =>'fa fa-user'
				                               );
		}		
		if($this->user->hasPermission('access', 'customers/customerspricing') || $this->user->hasPermission('modify', 'customers/customerspricing')){
			$this->data["general"][] = array( "link" => $this->url->link("customers/customerspricing", 'token=' . $this->session->data['token'], 'SSL'),
				                               "altkey"=> $this->language->get('alt_customerspricing'),
				                               "id"     => $this->language->get('purchasereturn_id'),
				                               "Text" => $this->language->get('text_customer_pricing'),
				                               "icon" =>'fa fa-money'
				                               );
		}
		
		/*
		$this->data["general"][] = array( "link" => $this->url->link("transaction/stock_adjustment/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "altkey"=> $this->language->get(''),
			                                   "id"     => $this->language->get('stockadjust_id'),
				                               "Text" => $this->language->get('text_stock_adjust'),
				                               "icon" =>'fa fa-random'
				                               );

		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "altkey"=> $this->language->get('alt_stocktake'),
			                                   "id"     => $this->language->get('stocktake_id'),
				                               "Text" => $this->language->get('text_stock_take'),
				                               "icon" =>'fa fa-list-alt'
				                               );
		$this->data["stock"][] = array( "link" => $this->url->link("transaction/stock_take/updatelist", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "altkey"=> $this->language->get('alt_stockupdate'),
			                                   "id"     => $this->language->get('stockupdate_id'),
				                               "Text" => $this->language->get('text_stock_updation'),
				                               "icon" =>'fa fa-undo'
				                               );
		*/

	    /*if($this->user->hasPermission('access', 'setting/customers')){
		$this->data["sales"][] =  array( "link" =>$this->url->link("setting/customers", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',//$this->language->get('alt_customermaster'),
			                                  "id"     => $this->language->get('customermaster_id'),
			                               "Text" => 'Customer Master',
			                               "icon" =>'fa fa-user'
			                               );
		}

		if($this->user->hasPermission('access', 'setting/member')){
		$this->data["sales"][] =  array( "link" =>$this->url->link("setting/member", 'token=' . $this->session->data['token'], 'SSL'),
			                                   "altkey"=> '',//$this->language->get('alt_customermaster'),
			                                   "id"     => $this->language->get('customermaster_id'),
			                                   "Text" => 'Members Master',
			                               	   "icon" =>'fa fa-user'
			                               );
		}*/
		if($this->user->hasPermission('access', 'master/vendor')){
		$this->data["sales"][] =  array( "link" =>$this->url->link("master/vendor", 'token=' . $this->session->data['token'], 'SSL'),
			                                "altkey"=> '',//$this->language->get('alt_suppliermaster'),
			                                  "id"     => $this->language->get('suppliermaster_id'),
			                                   "Text" => 'Supplier Master',
			                               	   "icon" =>'fa fa-th-large'
			                               );
		}
		

		$this->data['token'] = $this->session->data['token'];	
		$this->template = 'transaction/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getProductDetailsbyScan() {
			$this->load->model('transaction/purchase');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$_productDetails           = $this->model_transaction_purchase->getProductByName($this->data);
	}

 }
?>