<?php
class ControllerCommonForgotten extends Controller {
	private $error = array();

	public function index() {
		if ($this->user->isLogged()) {
			$this->redirect($this->url->link('common/home', '', 'SSL'));
		}
		
		
		$this->language->load('common/forgotten');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('user/user');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->language->load('mail/forgotten');
			
			$code = substr(sha1(uniqid(mt_rand(), true)), 0, 8);
			
			$this->model_user_user->editCode($this->request->post['email'], $code);

			$data = $this->model_user_user->getUsersByEmailAddress($this->request->post['email']);

			$this->editPasswordEmail($data,$code);		
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('common/login', '', 'SSL'));
		}

      	$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),        	
        	'separator' => false
      	); 
		
      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_forgotten'),
			'href'      => $this->url->link('common/forgotten', '', 'SSL'),       	
        	'separator' => $this->language->get('text_separator')
      	);
		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_your_email'] = $this->language->get('text_your_email');
		$this->data['text_email'] = $this->language->get('text_email');

		$this->data['entry_email'] = $this->language->get('entry_email');

		$this->data['button_reset'] = $this->language->get('button_reset');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} 
		/*10-Mar-2016*/
		if (isset($this->error['email'])) {
			$this->data['error_emailname'] = $this->error['email'];
		} else {
			$this->data['error_emailname'] = '';
		}
		/*10-Mar-2016*/
		$this->data['action'] = $this->url->link('common/forgotten', '', 'SSL');
 
		$this->data['back'] = $this->url->link('common/login', '', 'SSL');
    	
		if (isset($this->request->post['email'])) {
      		$this->data['email'] = $this->request->post['email'];
		} else {
      		$this->data['email'] = '';
    	}
		if ($this->config->get('config_password')) {
			$this->data['forgotten'] = $this->url->link('common/forgotten', '', 'SSL');
		} else {
			$this->data['forgotten'] = '';
		}		
		$this->template = 'common/forgotten.tpl';
		/*$this->children = array(
			'common/header',
			'common/footer'
		);*/
								
		$this->response->setOutput($this->render());		
	}

	public function validate() {		

		if ($this->request->post['email'] == '') {
			$this->error['email'] = $this->language->get('error_emailname');
		} elseif (!$this->model_user_user->getUsersByEmailAddress($this->request->post['email'])) {		
			$this->error['warning'] = $this->language->get('error_email');
		}

		return !$this->error;
		
	}	

	public function editPasswordEmail($data,$password)
	{
    	$html .= '<p style="margin:15px 0 10px 0; font-family:arial; font-size:14px">Dear '.$data["firstname"].' '.$data["lastname"].',</p>';
	  	$html .= '<p style="margin:15px 0 10px 0; font-family:arial; font-size:14px">We have received an online request for "Forgot Password" for your Oblakpos Account. Kindly check the below login Details!</p>';
    	$html .= '<p style="margin:15px 0 10px 0; font-family:arial; font-size:14px">EMail &nbsp;&nbsp;&nbsp;:'.$data["email"].'</p>';
    	$html .= '<p style="margin:15px 0 10px 0; font-family:arial; font-size:14px">Password :'.$password.'</p>';
    	$html .= '<p style="margin:15px 0 10px 0; font-family:arial; font-size:14px">Best Wishes,<br/>Oblakpos</p>';

   		$to    = $this->request->post["email"];
		$subject = 'Oblakpos Forgot Password';
		$headers = "From: " . $this->config->get('config_email') . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$message = $html;		

		mail($to, $subject, $message, $headers); 	   
	}
}
?>