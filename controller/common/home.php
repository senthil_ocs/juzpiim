<?php   
class ControllerCommonHome extends Controller {
	public function index() {
		$this->language->load('common/home');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->getList();
	}
	//31-March-2016 Added Recent Order List//
	public function getList() {
		
		$this->load->model('setting/customers');
		$this->load->model('transaction/sales');
		$this->load->model('common/home');
		$this->data['dashboard'] = $this->url->link('common/home/charts','token='.$this->session->data['token'], 'SSL');
		$this->data['salesman'] = $this->model_transaction_sales->getSalesmanList();
		
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
			$url .= '&filter_date_from=' . $filter_date_from;
		}else{
			$filter_date_from = date('d/m/Y',strtotime('-1 day'));
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
			$url .= '&filter_date_to=' . $filter_date_to;
		}else{
			$filter_date_to = date('d/m/Y');
		}
		$filter_delivery_man = null;
		if (isset($this->request->post['filter_delivery_man'])) {
			$filter_delivery_man = $this->request->post['filter_delivery_man'];
			$url .= '&filter_delivery_man=' . $filter_delivery_man;
		}
		$filter_payment_status = array();
		if (!empty($this->request->post['filter_payment_status'])) {
			$filter_payment_status = $this->request->post['filter_payment_status'];
		}
		$data = array(
			'filter_date_from'     => $filter_date_from,
			'filter_date_to'   	   => $filter_date_to,
			'filter_delivery_man'  => $filter_delivery_man,
			'filter_payment_status'=> $filter_payment_status
		);
		$this->data['filters'] = $data;
		$purchase_history = $this->model_common_home->getSalesPaymentHistory($data);

		foreach ($purchase_history as $history) {
			$delivery_man  = $this->model_common_home->getdeliveryManName($history['delivery_man_id']);
			$purchase_historys[] = array(
				'delivery_man'   => $delivery_man,
				'invoice_no'     => $history['order_id'],
				'do_no'          => $history['do_no'],
				'amount'         => $history['amount'],
				'payment_date'   => date('d/m/Y',strtotime($history['payment_date'])),
				'payment_status' => $history['payment_status']
			);
		}
		$this->data['purchase_history'] = $purchase_historys;
		$this->template = 'common/hq_home.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function chart() {
		$this->language->load('common/home');
	}
	public function login() {
		$route = '';

		if (isset($this->request->get['route'])) {
			$part = explode('/', $this->request->get['route']);

			if (isset($part[0])) {
				$route .= $part[0];
			}

			if (isset($part[1])) {
				$route .= '/' . $part[1];
			}
		}

		$ignore = array(
			'common/login',
			'common/forgotten',
			'common/reset',
			'common/api'
		);	

		if (!$this->user->isLogged() && !in_array($route, $ignore)) {
			return $this->forward('common/login');
		}

		if (isset($this->request->get['route'])) {
			$ignore = array(
				'common/login',
				'common/logout',
				'common/forgotten',
				'common/api',
				'common/reset',
				'error/not_found',
				'error/permission'
			);

			$config_ignore = array();

			if ($this->config->get('config_token_ignore')) {
				$config_ignore = unserialize($this->config->get('config_token_ignore'));
			}

			$ignore = array_merge($ignore, $config_ignore);

			if (!in_array($route, $ignore) && (!isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token']))) {
				return $this->forward('common/login');
			}
		} else {
			if (!isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
				return $this->forward('common/login');
			}
		}
	}

	public function permission() {
		if (isset($this->request->get['route'])) {
			$route = '';

			$part = explode('/', $this->request->get['route']);

			if (isset($part[0])) {
				$route .= $part[0];
			}

			if (isset($part[1])) {
				$route .= '/' . $part[1];
			}

			$ignore = array(
				'common/home',
				'common/login',
				'common/logout',
				'common/forgotten',
				'common/api',
				'common/reset',
				'error/not_found',
				'error/permission',
				'functionkey/home_search'		
			);			
			
			if (!in_array($route, $ignore) && (!$this->user->hasPermission('access', $route) && !$this->user->hasPermission('modify', $route))) {
				return $this->forward('error/permission');
			}
		}
	}	
	public function SalesChart() {
		
		$this->load->model('common/home');
		
		$this->data['SalesChart'] = array();
		$data = array(
		  	'month'=> date('m'),
		);
		
		$this->response->setOutput(json_encode($this->data['SalesChart']));
	}
	public function charts(){
		$this->document->setTitle('Graph');

		$this->template = 'dashboard/column_chart.tpl';
		$this->children = array(
					'common/header',
					'common/sidebar',
					'common/footer'
				);
		$this->response->setOutput($this->render());
	}
	public function getPurchaseChartData(){
		$this->load->model('common/home');
		$type  = $this->request->post['type'];
		$ptype = $this->request->post['ptype'];

		$dates = '';
		if($type=='week'){
			$dates = $this->getWeekDates();
		}
		if($type=='month'){
			$dates = $this->getmonthDates();
		}
		if($type=='year'){
			$dates = $this->getallMonths();
		}

		$jsonData = array();
		foreach($dates as $datesd) {
			// echo $datesd.'<br>';
			$records = $this->model_common_home->getPurchase_BarChart($datesd,$type,$ptype);
			// $records['total'] = $records['total']==null ? 1 : $records['total'];
			$displayDate = date('d',strtotime($datesd)).'th '.date('l',strtotime($datesd));
			if($type=='year'){
				$displayDate = date('M',strtotime($datesd));
			}
			if($type=='month'){
				$displayDate = date('D',strtotime($datesd));
			}
			$jsonData[] = array('country'=>$displayDate,'visits'=>$records['total']);
		}
		$this->response->setOutput(json_encode($jsonData));
	}
	
	public function getSalesChartData(){

		$this->load->model('common/home');
		$type  = $this->request->post['type'];
		$stype = $this->request->post['stype'];

		$dates = '';
		if($type=='week'){
			$dates = $this->getWeekDates();
		}
		if($type=='month'){
			$dates = $this->getmonthDates();
		}
		if($type=='year'){
			$dates = $this->getallMonths();
		}

		$jsonData = array();
		foreach($dates as $datesd) {
			$records = $this->model_common_home->getSales_BarChart($datesd,$type,$stype);
			// $records['total'] = $records['total']==null ? 1 : $records['total'];
			$displayDate = date('d',strtotime($datesd)).'th '.date('l',strtotime($datesd));
			if($type=='year'){
				$displayDate = date('M',strtotime($datesd));
			}
			if($type=='month'){
				$displayDate = date('d',strtotime($datesd)).'-'.date('D',strtotime($datesd));
			}
			$jsonData[] = array('country'=>$displayDate,'visits'=>$records['total']);
		}
		$this->response->setOutput(json_encode($jsonData));		
	}
	public function getWeekDates(){
			$monday = strtotime("last monday");
			$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
			$sunday = strtotime(date("Y-m-d",$monday)." +6 days");
			$data['start_date'] = date("Y-m-d",$monday);
			$data['end_date']   = date("Y-m-d",$sunday);
			return $this->grtDifferentDates($data);

	}
	public function getmonthDates(){

	   $month = date('m');
	   $year = date('Y');
	   $result = strtotime("{$year}-{$month}-01");
	   $data['start_date'] = date('Y-m-d', $result);
	   $result = strtotime('-1 second', strtotime('+1 month', $result));
	   $data['end_date'] =  date('Y-m-d', $result);
	   return $this->grtDifferentDates($data);
	}
	public function grtDifferentDates($data){
		$dates = array(); 
		$Variable1 = strtotime($data['start_date']); 
		$Variable2 = strtotime($data['end_date']); 

		for ($currentDate = $Variable1; $currentDate <= $Variable2; $currentDate += (86400)){
			$dates[] = date('Y-m-d', $currentDate); 
		}
		return $dates;
	}
	public function getallMonths(){
		$month = array();
		for ($i=1; $i <= 12; $i++) { 
			$month[] = date('Y').'-'.$i.'-01';
		}
		return $month;
	}
}
?>