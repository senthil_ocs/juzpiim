<?php
class ControllerCommonLogin extends Controller {
	private $error = array();

	public function index() {
    	$this->language->load('common/login');
		$this->document->setTitle($this->language->get('heading_title'));
		ob_start();
		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
		}
		//printArray($this->request->server); exit;
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			//10-March-2016//
			$username = $this->request->post['email'];
			$password = $this->request->post['password1'];
			if ($this->user->login($username, $password)) {
	            if($this->request->post['rememberme']) {
	            	setcookie('CookieEmailAddress', $email, time() + (86400 * 30), "/" ); // 86400 = 1 day
	                setcookie('CookiePaswrd', $password, time() + (86400 * 30), "/" ); // 86400 = 1 day
	            }
	        }
	      	//10-March-2016//
			if($this->user->isLogged()){
				if($this->session->data['location_code']==''){
					$this->error['warning'] = 'Invalid Location,Please set the Location';
				}else{
					$this->session->data['token'] = md5(mt_rand());
					$redirectUrl	= HTTP_SERVER."index.php?route=common/home&token=".$this->session->data['token'];
					$this->redirect($redirectUrl);
					exit;
				}
			}else{
				$this->error['warning'] = 'Enter Valid details';
			}
		}

    	$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['text_login'] 	  = $this->language->get('text_login');
		$this->data['text_forgotten'] = $this->language->get('text_forgotten');
		$this->data['entry_username'] = $this->language->get('entry_username');
    	$this->data['entry_password'] = $this->language->get('entry_password');
    	$this->data['button_login']   = $this->language->get('button_login');

		if ((isset($this->session->data['token']) && !isset($this->request->get['token'])) || ((isset($this->request->get['token']) && (isset($this->session->data['token']) && ($this->request->get['token'] != $this->session->data['token']))))) {
			//$this->error['warning'] = $this->language->get('error_token');
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
    	$this->data['action'] = $this->url->link('common/login', '', 'SSL');
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif(isset($_COOKIE["email"])){
			$this->data['email'] = 	$_COOKIE["email"];
		} else {
			$this->data['email'] = '';
		}
		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} elseif(isset($_COOKIE["password"])){
			$this->data['password'] = 	$_COOKIE["password"];
		} else {
			$this->data['password'] = '';
		}
		//10-March-2016//
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif(isset($_COOKIE['CookieEmailAddress'])){
			$this->data['email'] = 	$_COOKIE['CookieEmailAddress'];
		} else {
			$this->data['email'] = '';
		}
		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} elseif(isset($_COOKIE['CookiePaswrd'])){
			$this->data['password'] = 	$_COOKIE['CookiePaswrd'];
		} else {
			$this->data['password'] = '';
		}
		//10-March-2016//


		if (isset($this->request->get[''])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);

			if (isset($this->request->get['token'])) {
				unset($this->request->get['token']);
			}

			$url = '';

			if ($this->request->get) {
				$url .= http_build_query($this->request->get);
			}

			$this->data['redirect'] = $this->url->link($route, $url, 'SSL');
		} else {
			$this->data['redirect'] = '';
		}
		$this->data['preview_logo']	='';


		$this->data['forgotten'] = $this->url->link('common/forgotten', '', 'SSL');

		$this->template = 'common/login.tpl';
		/*$this->children = array(
			'common/header',
			'common/footer'
		);*/

		$this->response->setOutput($this->render());
  	}

	protected function validate() {
		if (isset($this->request->post['email']) && isset($this->request->post['password']) && !$this->user->login($this->request->post['email'], $this->request->post['password'],'1')) {
			$this->error['warning'] = $this->language->get('error_login');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
}
?>