<?php 
class ControllerCommonSidebar extends Controller {
	protected function index() {
		$this->data['title'] = $this->document->getTitle(); 

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}
	
	
		$this->language->load('common/header');
		
		$this->data['preview_logo']	='';
		
		$company_id	= $this->session->data['company_id'];
		if (isset($company_id)) {
			$this->load->model('master/company');
			$company_info = $this->model_master_company->getCompanyDetails($company_id);
		}
		$this->load->model('tool/image');
		if(isset($company_info['logo'])) {
			$this->data['preview_logo'] = $this->model_tool_image->resize($company_info['logo'], 331, 55);
		} else {
			$this->data['preview_logo']	='';
		}
		
		$store_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '".$store_id."'");
		
		foreach ($query->rows as $setting) {
			if (!$setting['serialized']) {
				$this->config->set($setting['key'], $setting['value']);
			} else {
				$this->config->set($setting['key'], unserialize($setting['value']));
			}
		}

		

		$this->data['heading_title'] = $this->language->get('heading_title');

		

		if (!$this->user->isLogged() || !isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$this->data['logged'] = '';

			$this->data['home'] = $this->url->link('common/login', '', 'SSL');
			
		} else {
			$this->data['logged'] = $this->user->getUserName();
			$this->data['home'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');
			
		}
		if($this->user->hasPermission('access', 'setting/common') || $this->user->hasPermission('modify', 'setting/common')){
			$this->data['commomsetting'] = $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL');
	    }
	    if($this->user->hasPermission('access', 'inventory/common') || $this->user->hasPermission('modify', 'inventory/common')){
			$this->data['commominventory'] = $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL');
		}
		if($this->user->hasPermission('access', 'transaction/common_purchase') || $this->user->hasPermission('modify', 'transaction/common')){
			$this->data['purchasetransaction'] = $this->url->link('transaction/common_purchase', 'token=' . $this->session->data['token'], 'SSL');
		}
		if($this->user->hasPermission('access', 'transaction/common_sales') || $this->user->hasPermission('modify', 'transaction/common')){
			$this->data['salestransaction'] = $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL');
		}
		if($this->user->hasPermission('access', 'transaction/common_stock') || $this->user->hasPermission('modify', 'transaction/common')){
			$this->data['stocktransaction'] = $this->url->link('transaction/common_stock', 'token=' . $this->session->data['token'], 'SSL');
		}
		if($this->user->hasPermission('access', 'customers/customers') || $this->user->hasPermission('modify', 'customers/customers')){
			$this->data['commomcustomer'] = $this->url->link('customers/customers', 'token=' . $this->session->data['token'], 'SSL');
		}

		if($this->user->hasPermission('access', 'report/report') || $this->user->hasPermission('modify', 'report/report')){
			$this->data['commonreport'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		}

		if($this->user->hasPermission('access', 'pos/common') || $this->user->hasPermission('modify', 'pos/common')){
			$this->data['commonpos'] = $this->url->link('pos/common', 'token=' . $this->session->data['token'], 'SSL');
		}

		$this->data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->get['route'])) {
			$this->data['route'] = $this->request->get['route'];

			$route = '';

			$part = explode('/', $this->request->get['route']);

			if (isset($part[0])) {
				$route .= $part[0];
			}

			if (isset($part[1])) {
				$route .= '/' . $part[1];
			}
			$this->data['route'] = $route;

		}

		$this->template = 'common/sidebar.tpl';

		$this->render();
	}
}
?>