<?php 
class ControllerCommonHeader extends Controller {
	protected function index() {
		$this->data['title'] = $this->document->getTitle(); 

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = HTTPS_SERVER;
		} else {
			$this->data['base'] = HTTP_SERVER;
		}

		$this->data['description'] = $this->document->getDescription();
		$this->data['keywords'] = $this->document->getKeywords();
		$this->data['links'] = $this->document->getLinks();	
		$this->data['styles'] = $this->document->getStyles();
		$this->data['scripts'] = $this->document->getScripts();
		$this->data['lang'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');

		$this->language->load('common/header');
		
		$this->data['preview_logo']	='';
		
		$company_id	= $this->session->data['company_id'];
		if (isset($company_id)) {
			$this->load->model('master/company');
			$company_info = $this->model_master_company->getCompanyDetails($company_id);
		}
		$this->load->model('tool/image');
		if(isset($company_info['logo'])) {
			$this->data['preview_logo'] = $this->model_tool_image->resize($company_info['logo'], 98, 59);
		} else {
			$this->data['preview_logo']	='';
		}
		$config_autosearch = $this->model_master_company->getAutosearchCount();
		$this->data['SEARCH_CHAR_CNT']	= $config_autosearch;
		$store_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '".$store_id."'");
		
		foreach ($query->rows as $setting) {
			if (!$setting['serialized']) {
				$this->config->set($setting['key'], $setting['value']);
			} else {
				$this->config->set($setting['key'], unserialize($setting['value']));
			}
		}

		if (!$this->user->isLogged() || !isset($this->request->get['token']) || !isset($this->session->data['token']) || ($this->request->get['token'] != $this->session->data['token'])) {
			$this->data['logged'] = '';
			$this->data['home'] = $this->url->link('common/login', '', 'SSL');
		} else {
			$this->data['logged'] = $this->user->getUserName();
			$this->data['pp_express_status'] = $this->config->get('pp_express_status');

			$this->data['home'] = $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL');

			$this->data['myprofile']=$this->url->link('user/user/update', 'token=' . $this->session->data['token'] . '&user_id=' . $_SESSION['user_id'] . $url, 'SSL');
			
			$this->data['logout'] = $this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL');
			
		}

		$this->template = 'common/header.tpl';

		$this->render();
	}
}
?>