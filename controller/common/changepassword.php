<?php  
class ControllerCommonChangepassword extends Controller { 
	private $error = array();
	          
	public function index() { 
    	$this->language->load('common/changepassword');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		
		$this->data['old_password'] 	= $this->language->get('entry_old_password');
		$this->data['new_password'] 	= $this->language->get('entry_new_password');
		$this->data['conform_password'] = $this->language->get('entry_conform_password');
    	$this->data['button_change'] 	= $this->language->get('button_change');
		
		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) { 
				$this->load->model('common/changepassword');
			
				$this->model_common_changepassword->editPassword($this->request->post['password']);
      			$this->session->data['success'] = "Successfully! you have change password.";
	  			$this->redirect($this->url->link('common/changepassword', 'token=' . $this->session->data['token'], 'SSL'));
							
			}	
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
    		$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['action'] = $this->url->link('common/changepassword', 'token=' . $this->session->data['token'], 'SSL');

		$this->template = 'common/changepassword.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
  	}
		
	protected function validate() {
		
		if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
			$this->error['warning'] = $this->language->get('error_password');
    	} else if ($this->request->post['confirm'] === $this->request->post['password']) {
			$this->error['warning'] = "Please enter match password.";
    	}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
		
	}
}  
?>