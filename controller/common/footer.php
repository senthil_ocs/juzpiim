<?php
class ControllerCommonFooter extends Controller {   
	protected function index() {
		$this->language->load('common/footer');

		$this->data['text_footer'] = sprintf($this->language->get('text_footer'), VERSION);

		if (file_exists(DIR_SYSTEM . 'config/svn/svn.ver')) {
			$this->data['text_footer'] .= '.r' . trim(file_get_contents(DIR_SYSTEM . 'config/svn/svn.ver'));
		}
		
		$this->data['text_confirm'] = $this->language->get('text_confirm');
		$this->data['route'] = $this->request->get['route'];

		$this->data['config_product_discount'] = $this->config->get('config_product_discount');

		$this->data['addCartPremission'] = $this->user->hasPermission('modify','functionkey/f11');
		/*if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
		$url	='index.php?op=functionkey/home_search';
		$this->load->model('common/footer');
		print_r($data);
		$this->data['filter_name'] = '';
		if (isset($this->request->post['search_product_text'])&& !empty($this->request->post['search_product_text'])) {
			$data['filter_name']	= trim($this->request->post['search_product_text']);
			$this->data['filter_name'] = $this->request->post['search_product_text'];
			$_productDetails		= $this->model_common_footer->getProductByName($data);
		} else {
		    $_productDetails		= $this->model_common_footer->getProductByName($data);
		}
		$product_data = array();	
		if(!empty($_productDetails)) {
		    $total	  = count($_productDetails);
		} else {
		    $total	  = 0;
		}
		//$pagination	= $this->paginationHtml($page,$total,$limit,$url);
		//$this->data['pagination'] = $pagination;
		$this->data['page'] = $page;
		$this->load->model('tool/image');

		if(!empty($_productDetails)) {			
			foreach($_productDetails as $result) {
				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				    $image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
				    $image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}
				$this->data['product_collection'][] = array(
					'product_id' => $result['product_id'],
					'name'       => $result['name'],
					'sku'        => $result['sku'],
					'barcodes'   => $this->model_common_footer->getProductBarcodes($result['product_id']),
					'price'      => $result['price'],
					'special'    => $this->model_common_footer->getProductSpecials($result['product_id']),
					'quantity'   => $result['quantity'],
					'image'   => $image,
					'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
				);
			}
		} */

		/*print '<pre>';
		print_r($_productDetails);
		print '</pre>';*/

		$this->template = 'common/footer.tpl';

		$this->render();
	}
	public function paginationHtml($page,$total,$limit,$url) {
	
		$text = 'Showing {start} to {end} of {total} ({pages} Pages)';
		$text_first = '|&lt;';
		$text_last = '&gt;|';
		$text_next = '&gt;';
		$text_prev = '&lt;';
		$style_links = 'links';
		$style_results = 'results';
	
		if ($page < 1) {
			$page = 1;
		} 
		
		if (!(int)$limit) {
			$limit = 10;
		} 
		$num_links = 10;
		$num_pages = ceil($total / $limit);
		
		$output = '';
		
		$url	= "'".$url."'";
		$param_prev	= "'".($page - 1)."'";
		$param_first	= "'1'";
		
		if ($page > 1) {
			$output .= ' <a onclick="ajaxPagination('.$param_first.','.$url.')">' . $text_first . '</a> <a onclick="ajaxPagination('.$param_prev .','.$url.')">' . $text_prev . '</a> ';
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
			
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
						
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			if ($start > 1) {
				$output .= ' .... ';
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= ' <b>' . $i . '</b> ';
				} else {
					$sequenceParam	="'".$i."'";
					$output .= ' <a onclick="ajaxPagination('.$sequenceParam.','.$url.')">' . $i . '</a> ';
				}	
			}
							
			if ($end < $num_pages) {
				$output .= ' .... ';
			}
		}
		
		if ($page < $num_pages) {
			$param_next	= "'".($page + 1)."'";
			$param_last	= "'".$num_pages."'";
		
			$output .= ' <a onclick="ajaxPagination('.$param_next.','.$url.')">' . $text_next . '</a> <a onclick="ajaxPagination('.$param_last.','.$url.')">' . $text_last . '</a> ';
		}
		
		$find = array(
			'{start}',
			'{end}',
			'{total}',
			'{pages}'
		);
		
		$replace = array(
			($total) ? (($page - 1) * $limit) + 1 : 0,
			((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
			$total, 
			$num_pages
		);
		
		return ($output ? '<div class="' . $style_links . '">' . $output . '</div>' : '') . '<div class="' . $style_results . '">' . str_replace($find, $replace, $text) . '</div>';
	}
}
?>