<?php 
class ControllerFunctionkeyPaymode extends Controller {
	
	public function index() {
	
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/functionkey/paymode.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/functionkey/paymode.tpl';
		} else {
			$this->template = 'default/template/functionkey/paymode.tpl';
		}
		$this->load->model('ajax/cart');
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
			$this->session->data['page_value'] = $this->request->post['page'];
		} else {
			if (!isset($this->session->data['page_value'])) {
			    $page = 1;
				$this->session->data['page_value'] = 1;
			}
		}
		
		$url	='index.php?op=functionkey/paymode';
		
		if (isset($this->request->post['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->post['change_order_id'])) {
			$change_order_id = $this->request->post['change_order_id']; 
			$default_data = array('start' => ($this->session->data['page_value'] - 1) * 5,'limit' => 1,'order_id'=>$change_order_id);
		} else {
			$default_data = array('start' => ($this->session->data['page_value'] - 1) * 5,'limit' => 1);
		}
		
		$defaultSelectedRegards	= $this->model_ajax_cart->getAllInvoiceByUser($default_data);
		$this->data['order_id']	= '';
		if(!empty($defaultSelectedRegards)) {
			foreach($defaultSelectedRegards as $selectInvoice) {
				$this->data['order_id']	= $selectInvoice['order_id'];
			}
		}
		
		$data = array('start' => ($this->session->data['page_value'] - 1) * 5,'limit' => 5);
		$invoiceCollection	= $this->model_ajax_cart->getAllInvoiceByUser($data);
		
		$order_data = array();	
		foreach ($invoiceCollection as $order) {
			if(!empty($order['payment_method'])) {
			    $dat_info['payment_method'] = $order['payment_method'];
			} else {
			    $dat_info['payment_method'] = '';
			}
			$order_data[] = array(
				'order_id' => $order['order_id'],
				'invoice_no'       => $order['invoice_no'],
				'invoice_prefix'   => $order['invoice_prefix'],
				'company_id'   => $order['company_id'],
				'customer_id'      => $order['customer_id'],
				'payment_method'      => $order['payment_method'],
				'payment_code' => $order['payment_code'],
				'total' => $order['total'],
				'tendar' => $order['tendar'],
				'changes' => $order['changes'],
				'silp_number' => $order['silp_number'],
				'order_status_id' => $order['order_status_id'],
				'language_id' => $order['language_id'],
				'currency_id' => $order['currency_id'],
				'currency_code' => $order['currency_code'],
				'currency_value' => $order['currency_value'],
				'ip' => $order['ip'],
				'forwarded_ip' => $order['forwarded_ip'],
				'date_added' => $order['date_added'],
				'date_modified'     => $order['date_modified'],
				'order_time' => $order['order_time'],
				'cashier' => $order['cashier'],
				'total_items' => $order['total_items'],
				'net_amount' => $order['net_amount'],
				'paymode' => $this->model_ajax_cart->getAllPaymode($dat_info)
			); 
		}
		
		$total_invoice	= $this->model_ajax_cart->getTotalInvoiceByUser($data);
		$total	= $total_invoice;
		$limit	= 5;
		$pagination	= $this->paginationHtml($this->session->data['page_value'],$total,$limit,$url);
		$this->data['pagination'] = $pagination;
		
		if(!empty($order_data)) {
			$this->data['invoice_collection']	= $order_data;
		} else {
			$this->data['invoice_collection']	= '';	
		}
		if (isset($this->request->post['page']) || isset($this->request->post['change_order_id'])) {
			$this->children = array(
				'functionkey/payment_bottom'
			);
		} else {
			$this->children = array(
				'common/popup_header',
				'functionkey/payment_bottom',
				'common/popup_footer'
			);
		}
		$this->response->setOutput($this->render());
	}
	
	public function update() {
	
		$is_error	= false;
		$message	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$saveData	= array();
			$saveData['order_id']	= $this->request->post['order_id'];
			$saveData['changes']	= $this->request->post['balance_amount'];
			$saveData['tendar']	= $this->request->post['paid_amount'];
			$saveData['payment_method']	= implode(",", $this->request->post['payment_mode']);
			$saveData['silp_number']	= $this->request->post['silp_number'];
			$saveData['card_amount']	= $this->request->post['card_amount'];
			$saveData['cash_amount']	= $this->request->post['cash_amount'];
			$this->load->model('ajax/cart');
			$invoiceDetails	= $this->model_ajax_cart->getAllInvoiceByUser($saveData);
			if(!empty($invoiceDetails)) {
				$invoiceTotal	= $this->currency->format($invoiceDetails[0]['total'], '', '', false);
			}
			$paymentDetails	= $this->model_ajax_cart->getAllPaymode($saveData);
			if(!empty($paymentDetails)) {
				
				if(count($paymentDetails) > 1) {						    
					if(empty($saveData['silp_number']) || empty($saveData['silp_number']) || empty($saveData['silp_number'])) {
						$is_error	= true;
						$message['error']	= 'Please enter all details.';	
					} else {
						$saveData['tendar'] = (float)$saveData['card_amount'] + (float)$saveData['cash_amount'];
						if($saveData['tendar'] >= $cartTotal) {
							if($saveData['tendar']==$cartTotal) {
								$saveData['changes']	= '0.00';	
							} else {
								$saveData['changes']	= (float)$saveData['tendar'] - (float)$invoiceTotal;	
							}
						} else {
							$is_error	= true;
							$message['error']	= 'Please enter valid paid amount.';
						}								
					}
					
				} else {
					foreach($paymentDetails as $paymentDetail) {
						$saveData['payment_code']	= str_replace(' ','-',strtolower(trim($paymentDetail['payment_name'])));
						if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
							if($saveData['tendar'] >= $invoiceTotal) {
								if($saveData['tendar']==$invoiceTotal) {
									$saveData['changes']	= '0.00';	
								} else {
									$saveData['changes']	= (float)$saveData['tendar'] - (float)$invoiceTotal;	
								}
								$saveData['silp_number']	= '';
							} else {
								$is_error	= true;
								$message['error']	= 'Please enter valid paid amount.';
							}
						} else {
							$saveData['tendar']	= $invoiceTotal;
							$saveData['changes']	= '0.00';
							if(empty($saveData['silp_number'])) {
								$is_error	= true;
								$message['error']	= 'Please enter bill slip number.';	
							}
						}
					}
				}
					
			} else {
				$is_error	= true;
				$message['error']	= 'There is some problem. Please contact admin.';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$paymentDetails	= $this->model_ajax_cart->updatePaymentMethod($saveData);
			$message['success']	= 'You have change payment mode successfully.';	
			echo json_encode($message);
		}
	}
	
	public function paginationHtml($page,$total,$limit,$url) {
	
		$text = 'Showing {start} to {end} of {total} ({pages} Pages)';
		$text_first = '|&lt;';
		$text_last = '&gt;|';
		$text_next = '&gt;';
		$text_prev = '&lt;';
		$style_links = 'links';
		$style_results = 'results';
	
		if ($page < 1) {
			$page = 1;
		} 
		
		if (!(int)$limit) {
			$limit = 10;
		} 
		$num_links = 10;
		$num_pages = ceil($total / $limit);
		
		$output = '';
		
		$url	= "'".$url."'";
		$param_prev	= "'".($page - 1)."'";
		$param_first	= "'1'";
		
		if ($page > 1) {
			$output .= ' <a onclick="ajaxPagination('.$param_first.','.$url.')">' . $text_first . '</a> <a onclick="ajaxPagination('.$param_prev .','.$url.')">' . $text_prev . '</a> ';
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
			
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
						
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			if ($start > 1) {
				$output .= ' .... ';
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= ' <b>' . $i . '</b> ';
				} else {
					$sequenceParam	="'".$i."'";
					$output .= ' <a onclick="ajaxPagination('.$sequenceParam.','.$url.')">' . $i . '</a> ';
				}	
			}
							
			if ($end < $num_pages) {
				$output .= ' .... ';
			}
		}
		
		if ($page < $num_pages) {
			$param_next	= "'".($page + 1)."'";
			$param_last	= "'".$num_pages."'";
		
			$output .= ' <a onclick="ajaxPagination('.$param_next.','.$url.')">' . $text_next . '</a> <a onclick="ajaxPagination('.$param_last.','.$url.')">' . $text_last . '</a> ';
		}
		
		$find = array(
			'{start}',
			'{end}',
			'{total}',
			'{pages}'
		);
		
		$replace = array(
			($total) ? (($page - 1) * $limit) + 1 : 0,
			((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
			$total, 
			$num_pages
		);
		
		return ($output ? '<div class="' . $style_links . '">' . $output . '</div>' : '') . '<div class="' . $style_results . '">' . str_replace($find, $replace, $text) . '</div>';
	}
}
?>