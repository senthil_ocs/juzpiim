<?php 
class ControllerFunctionkeyHomeSearch extends Controller {
	
	public function index() {
		$this->data['show'] = true;
		if(isset($this->request->post['search_form'])){
			$this->data['show'] =false;
		}
		$this->language->load('ajax/information');
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$this->data['out_of_stock'] = $this->language->get('out_of_stock');
		$this->load->model('pos/pos');
		

		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
		if($page<=0){
			$page = 1;
		}

		$data = array(
			'filter_barcodes'	  => $filter_barcodes,
			'filter_vendor'	      => $filter_vendor,
			'filter_department'	  => $filter_department, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_price'	      => $filter_price,
			'filter_priceto'	  => $filter_priceto,
			'filter_description'  => $filter_description,
			'sort'                => $sort,
			'order'               => $order,
			'start'               => ($page - 1) * 10, //$this->config->get('config_admin_limit')
			'limit'               => 10 //$this->config->get('config_admin_limit')
		);
		
		$this->data['filter_name'] = '';
		if (isset($this->request->post['searchkey'])&& !empty($this->request->post['searchkey'])) {
			$data['searchkey']	= trim($this->request->post['searchkey']);
			$this->data['filter_name'] = $this->request->post['searchkey'];
			//$_productDetails		= $this->model_pos_pos->getTotalProducts($data);
		} else {
		    //$_productDetails		= $this->model_pos_pos->getProductByName($data);
		}
		
		/*$product_data = array();	
		if(!empty($_productDetails)) {
		    $total	  = count($_productDetails);
		} else {
		    $total	  = 0;
		}*/
		$product_total = $this->model_pos_pos->getTotalProductByName($data);
		$_productDetails		= $this->model_pos_pos->getProductByName($data);

		$this->load->model('tool/image');		
		if(!empty($_productDetails)) {			
			foreach($_productDetails as $result) {
				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				    $image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
				    $image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}
				if($result['average_cost']=='0.0000'){
					$result['average_cost'] =' ';
				}
				$this->data['product_collection'][] = array(
					'product_id' => $result['product_id'],
					'name'       => $result['name'],
					'sku'        => $result['sku'],
					'barcodes'   => $this->model_pos_pos->getProductBarcodes($result['product_id']),
					'price'      => $result['price'],
					'avgcost'      => $result['average_cost'],
					'special'    => $this->model_pos_pos->getProductSpecials($result['product_id']),
					'quantity'   => $result['quantity'],
					'image'   => $image,
					'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
				);
			}
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = 10; //$this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('functionkey/home_search', '&token=' . $this->session->data['token'] .  '&page={page}', 'SSL');
		$this->data['action'] = $this->url->link('functionkey/home_search', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['pagination'] = $pagination->render();
		

		$this->template = 'functionkey/home_search.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function update() {
	
		$is_error	= false;
		$message	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$saveData	= array();
			$saveData['order_id']	= $this->request->post['order_id'];
			$saveData['changes']	= $this->request->post['balance_amount'];
			$saveData['tendar']	= $this->request->post['paid_amount'];
			$saveData['payment_method']	= implode(",", $this->request->post['payment_mode']);
			$saveData['silp_number']	= $this->request->post['silp_number'];
			$saveData['card_amount']	= $this->request->post['card_amount'];
			$saveData['cash_amount']	= $this->request->post['cash_amount'];
			$this->load->model('ajax/cart');
			$invoiceDetails	= $this->model_ajax_cart->getAllInvoiceByUser($saveData);
			if(!empty($invoiceDetails)) {
				$invoiceTotal	= $this->currency->format($invoiceDetails[0]['total'], '', '', false);
			}
			$paymentDetails	= $this->model_ajax_cart->getAllPaymode($saveData);
			if(!empty($paymentDetails)) {
				
				if(count($paymentDetails) > 1) {						    
					if(empty($saveData['silp_number']) || empty($saveData['silp_number']) || empty($saveData['silp_number'])) {
						$is_error	= true;
						$message['error']	= 'Please enter all details.';	
					} else {
						$saveData['tendar'] = (float)$saveData['card_amount'] + (float)$saveData['cash_amount'];
						if($saveData['tendar'] >= $cartTotal) {
							if($saveData['tendar']==$cartTotal) {
								$saveData['changes']	= '0.00';	
							} else {
								$saveData['changes']	= (float)$saveData['tendar'] - (float)$invoiceTotal;	
							}
						} else {
							$is_error	= true;
							$message['error']	= 'Please enter valid paid amount.';
						}								
					}
					
				} else {
					foreach($paymentDetails as $paymentDetail) {
						$saveData['payment_code']	= str_replace(' ','-',strtolower(trim($paymentDetail['payment_name'])));
						if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
							if($saveData['tendar'] >= $invoiceTotal) {
								if($saveData['tendar']==$invoiceTotal) {
									$saveData['changes']	= '0.00';	
								} else {
									$saveData['changes']	= (float)$saveData['tendar'] - (float)$invoiceTotal;	
								}
								$saveData['silp_number']	= '';
							} else {
								$is_error	= true;
								$message['error']	= 'Please enter valid paid amount.';
							}
						} else {
							$saveData['tendar']	= $invoiceTotal;
							$saveData['changes']	= '0.00';
							if(empty($saveData['silp_number'])) {
								$is_error	= true;
								$message['error']	= 'Please enter bill slip number.';	
							}
						}
					}
				}
					
			} else {
				$is_error	= true;
				$message['error']	= 'There is some problem. Please contact admin.';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$paymentDetails	= $this->model_ajax_cart->updatePaymentMethod($saveData);
			$message['success']	= 'You have change payment mode successfully.';	
			echo json_encode($message);
		}
	}
	
	/*public function paginationHtml($page,$total,$limit,$url) {
	
		$text = 'Showing {start} to {end} of {total} ({pages} Pages)';
		$text_first = '|&lt;';
		$text_last = '&gt;|';
		$text_next = '&gt;';
		$text_prev = '&lt;';
		$style_links = 'links';
		$style_results = 'results';
	
		if ($page < 1) {
			$page = 1;
		} 
		
		if (!(int)$limit) {
			$limit = 10;
		} 
		$num_links = 10;
		$num_pages = ceil($total / $limit);
		
		$output = '';
		
		$url	= "'".$url."'";
		$param_prev	= "'".($page - 1)."'";
		$param_first	= "'1'";
		
		if ($page > 1) {
			$output .= ' <a onclick="ajaxPagination('.$param_first.','.$url.')">' . $text_first . '</a> <a onclick="ajaxPagination('.$param_prev .','.$url.')">' . $text_prev . '</a> ';
		}

		if ($num_pages > 1) {
			if ($num_pages <= $num_links) {
				$start = 1;
				$end = $num_pages;
			} else {
				$start = $page - floor($num_links / 2);
				$end = $page + floor($num_links / 2);
			
				if ($start < 1) {
					$end += abs($start) + 1;
					$start = 1;
				}
						
				if ($end > $num_pages) {
					$start -= ($end - $num_pages);
					$end = $num_pages;
				}
			}

			if ($start > 1) {
				$output .= ' .... ';
			}

			for ($i = $start; $i <= $end; $i++) {
				if ($page == $i) {
					$output .= ' <b>' . $i . '</b> ';
				} else {
					$sequenceParam	="'".$i."'";
					$output .= ' <a onclick="ajaxPagination('.$sequenceParam.','.$url.')">' . $i . '</a> ';
				}	
			}
							
			if ($end < $num_pages) {
				$output .= ' .... ';
			}
		}
		
		if ($page < $num_pages) {
			$param_next	= "'".($page + 1)."'";
			$param_last	= "'".$num_pages."'";
		
			$output .= ' <a onclick="ajaxPagination('.$param_next.','.$url.')">' . $text_next . '</a> <a onclick="ajaxPagination('.$param_last.','.$url.')">' . $text_last . '</a> ';
		}
		
		$find = array(
			'{start}',
			'{end}',
			'{total}',
			'{pages}'
		);
		
		$replace = array(
			($total) ? (($page - 1) * $limit) + 1 : 0,
			((($page - 1) * $limit) > ($total - $limit)) ? $total : ((($page - 1) * $limit) + $limit),
			$total, 
			$num_pages
		);
		
		return ($output ? '<div class="' . $style_links . '">' . $output . '</div>' : '') . '<div class="' . $style_results . '">' . str_replace($find, $replace, $text) . '</div>';
	}*/
}
?>