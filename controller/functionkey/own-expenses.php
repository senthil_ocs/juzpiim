<?php 
class ControllerFunctionkeyOwnExpenses extends Controller {
	
	public function index() {	
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/functionkey/own-expenses.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/functionkey/own-expenses.tpl';
		} else {
			$this->template = 'default/template/functionkey/own-expenses.tpl';
		}
		$this->load->model('ajax/cart');
		
		$this->children = array(
			'common/popup_header',
			'common/popup_footer'
		);

		$this->response->setOutput($this->render());
	}
	
	public function addExpenses() {
	
		$is_error	= false;
		$message	= array();
		$postdata	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {		
			$expense_amount	= $this->request->post['expense_amount'];
			$description	= $this->request->post['description'];
			$postdata['expense_amount'] = $expense_amount;
			$postdata['description'] = $description;
			
			if(!empty($expense_amount) && !empty($description)) {
				if($expense_amount > 0) {					
					$this->load->model('ajax/cart');
					$expenseId	= $this->model_ajax_cart->doInsertOwnExpenses($postdata);
					if(!empty($expenseId)) {
					    $is_error	= false;
					}
				} else {
					$is_error	= true;
					$message['error']	= 'Please enter valid amount';
				}
			} else {
				$is_error	= true;
				$message['error']	= 'Please enter amount and description';	
			}		
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= 'Expense amount added successfully.';
			echo json_encode($message);
		}
	}
	
}
?>