<?php 
class ControllerFunctionkeyF9 extends Controller {

	public function index() {	
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}		
		if($this->session->data['is_exchange_manually']) {
			$this->session->data['is_exchange_manually']	= false;	
		} else {
			$this->session->data['is_exchange_manually']	= true;
		}
		$this->response->setOutput("Session".$this->session->data['is_exchange_manually']);
	}
	
	public function clear() {		
		unset($this->session->data['cart']);
		unset($this->session->data['totals']);
		unset($this->session->data['tender']);
		unset($this->session->data['last_product']);
		unset($this->session->data['order_id']);
		unset($this->session->data['discount']);
		unset($this->session->data['discount_mode']);
		unset($this->session->data['is_manually']);
		unset($this->session->data['is_exchange']);
		return true;		
  	}
	
}
?>