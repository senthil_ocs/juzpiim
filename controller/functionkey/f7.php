<?php 
class ControllerFunctionkeyF7 extends Controller {

	public function index() {
		
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			if(isset($this->request->post['relaease_bill_no']) && ctype_digit($this->request->post['relaease_bill_no'])) {
				$this->load->model('pos/cart');
				$data['bill_no']	= $this->request->post['relaease_bill_no'];
				$productCollection	= $this->model_pos_cart->getHoldProduct($data);
				if(!empty($productCollection)) {
					unset($this->session->data['cart']);
					foreach($productCollection as $_product) {
						$_productId		= $_product['product_id'];
						$_productQty	= $_product['quantity'];
						$this->cart->add($_productId,$_productQty);
					}
					$data = array('success' =>true );
			    } else{
			    	$data = array('error' =>"There is No Products in given Hold Bill " );
			    }
			} else{
				$data = array('error' =>"Please Enter hold Bill Number..." );
			}

				$this->response->setOutput(json_encode($data));
		}
	}
	
}
?>