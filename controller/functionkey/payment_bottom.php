<?php 
class ControllerFunctionkeyPaymentBottom extends Controller {
	
	public function index() {
	
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/functionkey/payment_bottom.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/functionkey/payment_bottom.tpl';
		} else {
			$this->template = 'default/template/functionkey/payment_bottom.tpl';
		}
		$this->load->model('ajax/cart');
		$_paymentCollection	= $this->model_ajax_cart->getAllPaymode();
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
		
		if (isset($this->request->post['change_order_id'])) {
			$change_order_id = $this->request->post['change_order_id']; 
			$default_data = array('start' => ($page - 1) * 1,'limit' => 1,'order_id'=>$change_order_id);
		} else {
			$default_data = array('start' => ($page - 1) * 1,'limit' => 1);
		}
		
		$defaultSelectedRegards	= $this->model_ajax_cart->getAllInvoiceByUser($default_data);
		if(!empty($defaultSelectedRegards)) {
			foreach($defaultSelectedRegards as $selectInvoice) {
				$this->data['order_id']			= $selectInvoice['order_id'];
				$this->data['net_amount']		= $this->currency->format($selectInvoice['total']);
				$this->data['invoice_no']		= $selectInvoice['invoice_no'];
				$this->data['balance_amount']	= $this->currency->format($selectInvoice['changes'], '', '', false);
				$this->data['payment_method']	= $selectInvoice['payment_method'];
				$this->data['silp_number']		= $selectInvoice['silp_number'];
				$this->data['paid_amount']		= $this->currency->format($selectInvoice['tendar'], '', '', false);
				
				$paymentDetails	= $this->model_ajax_cart->getAllPaymode($selectInvoice);
				if(count($paymentDetails) > 1) {
				    
					foreach($paymentDetails as $paymentDetail) {
					    $orderPaymentDetail	= $this->model_ajax_cart->getOrderPayment($selectInvoice['order_id'], $paymentDetail['payment_id']);
						if(strtolower($orderPaymentDetail['payment_method_name']) == 'cash') {
						    $this->data['cash_amount']		= $orderPaymentDetail['paid_amount'];
						} else {
						    $this->data['card_amount']		= $orderPaymentDetail['paid_amount'];
						}
					}
				} else {
				    $this->data['card_amount']		= '';
					$this->data['cash_amount']		= '';
				}
				
			}
		}
		//print_r($defaultSelectedRegards); 
		
		if(!empty($_paymentCollection)) {
			$this->data['paymentCollection']	= $_paymentCollection;
		} else {
			$this->data['paymentCollection']	= '';	
		}
		$this->response->setOutput($this->render());
	}
}
?>