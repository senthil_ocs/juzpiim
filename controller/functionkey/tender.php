<?php 
class ControllerFunctionkeyTender extends Controller {
	
	public function index() {
	
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/functionkey/tender.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/functionkey/tender.tpl';
		} else {
			$this->template = 'default/template/functionkey/tender.tpl';
		}
		$this->response->setOutput($this->render());
	}
}
?>