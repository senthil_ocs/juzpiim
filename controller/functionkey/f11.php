<?php
class ControllerFunctionkeyF11 extends Controller {

	public function index() {

		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$product_key = $this->request->get['product_key'];
		if (isset($this->session->data['cart'][$product_key])) {
			$this->load->model('pos/addcart');
			$_productDetails	= $this->model_pos_addcart->getProductDetails($product_key);
			$this->data['product_details']	= $_productDetails;
		}

		$this->data['changeprice'] = $this->url->link('functionkey/f11/changeprice','token=' .$this->session->data['token'],'SSL');

		$this->template = 'functionkey/f11.tpl';

		$this->response->setOutput($this->render());
	}

	public function changeprice() {
		$is_error	= false;
		$message	= array();
		$postdata	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$price_mode	= $this->request->post['price_mode'];
			$product_price	= $this->request->post['product_price'];
			$product_id	= $this->request->post['product_id'];
			$postdata['price_mode']	= $price_mode;
			$postdata['product_price']	= $product_price;
			$postdata['product_id']	= $product_id;

			if(!empty($price_mode)) {
				$productDetails	= $this->cart->getProduct($product_id);
				$this->load->model('pos/addcart');
				$original_price	= $productDetails['price'];
				if($price_mode == 1) {
					if($product_price <= 100) {
						$product_price	=  ($original_price * $product_price) / 100;
						$product_price	= $original_price - $product_price;
						$this->cart->updatePrice($product_id,$product_price);
					} else {
						$is_error	= true;
						$message['error']	= 'Please enter product price percentage below 100';
					}
				} elseif($price_mode == 2) {
					if($product_price <= $original_price) {
						$product_price = $original_price - $product_price;
						$this->cart->updatePrice($product_id,$product_price);
					} else {
						$is_error	= true;
						$message['error']	= 'Please enter product price percentage below'.$original_price;
					}
				}
			} else {
				$is_error	= true;
				$message['error']	= 'Missing some require field.';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= true;
			$message['postdata']	= $postdata;
			echo json_encode($message);
		}

	}
}
?>