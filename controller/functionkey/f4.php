<?php
class ControllerFunctionkeyF4 extends Controller {
	public function index () {
		//require_once(DIR_SYSTEM."vendor/pdf/mpdf.php");  //4-Apr-2016
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
       $this->data['action'] = $this->url->link("functionkey/f4/placeorder",'&token='.$this->request->get['token'],"SSL");
		$this->load->model('pos/cart');
		$totals = $this->model_pos_cart->getTotalsCollection();
		$_paymentCollection	= $this->model_pos_cart->getAllPaymode();

		$this->data['totals']		= $totals;

		if(!empty($_paymentCollection)) {
			$this->data['paymentCollection']	= $_paymentCollection;
		} else {
			$this->data['paymentCollection']	= '';
		}
		$showSlipmethod	= '1';
		if(!empty($totals)) {
			$count		= count($totals);
		    $cartinfo 	= $totals[$count-1]['value'];
			$cartTotal	= $cartinfo;
		}

		if(!empty($cartTotal)) {
			$this->data['paid_amount']	= $cartTotal;
		} else {
			$this->data['paid_amount']	= '';
		}


		if(!empty($showSlipmethod)) {
			$this->data['show_slip_method']	= '1';
		} else {
			$this->data['show_slip_method']	= '';
		}

		$this->template = 'functionkey/f4.tpl';
		$this->children = array();
		$this->response->setOutput($this->render());
	}

	public function placeorder() {
		$is_error	= false;
		$message	= array();
		//if($this->rules->getAccessConditionByRule('transaction')) {

			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
				$this->load->model('pos/cart');
				$totals	= $this->model_pos_cart->getTotalsCollection();
				if(!empty($totals)) {
					$count		= count($totals);
					//$cartinfo 	= $totals[$count-1]['text'];
					//$cartTotal	= trim(str_replace('$','',$cartinfo));
					$cartinfo 	= $totals[$count-1]['value'];
					$cartTotal	= $cartinfo;
				}
				$saveData	= array();
				$saveData['tendar']	        = $this->request->post['paid_amount'];
				$saveData['changes']	    = (float)$saveData['tendar']-(float)$cartTotal;
				$saveData['silp_number']	= $this->request->post['silp_number'];
				$saveData['card_amount']	= $this->request->post['card_amount'];
				$saveData['cash_amount']	= $this->request->post['cash_amount'];
				$saveData['visa_amount']	= $this->request->post['visa_amount'];
				$paymethod  = array();
				if($this->request->post['cash_amount']!="" && $this->request->post['cash_amount']!='0.00'){
					$paymethod[] = 'cash';
				}
				if($this->request->post['card_amount']!="" && $this->request->post['card_amount']!='0.00'){
					$paymethod[] = 'card';
				}
				if($this->request->post['visa_amount']!="" && $this->request->post['visa_amount']!='0.00'){
					$paymethod[] = 'visa';
				}
				$saveData['payment_method'] = implode(',',$paymethod);



				if(!empty($saveData['payment_method'])) {
					    if((in_array('card',$paymethod ) || in_array('visa',$paymethod )) && empty($saveData['silp_number']) ){
							$is_error	= true;
							$message['error']	= 'Please enter Slip NUmber.';
					    } else{
					    	if($saveData['tendar'] < $cartTotal) {
                                $is_error	= true;
								$message['error']	= 'Please enter valid paid amount.';
					    	}
					    }
				} else {
					$is_error	= true;
					$message['error']	= 'Please select any one payment mode';
				}
			} else {
				$is_error	= true;
				$message['error']	= 'There is some problem. Please contact admin.';
			}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= true;
			$this->data['invoicedata']	= $this->ordersuccess($saveData);
			$this->data['invoiceprint'] = $this->printInvoice($this->data['invoicedata']['last_order_id']);
			$this->data['newsale']      = $this->url->link("pos/pos","&token=".$this->request->get['token'],"SSL");
			//4-Apr-2016
			$this->data['printpdf']     = $this->url->link('functionkey/f4/printPDFInvoice', 'token=' . $this->session->data['token'] . '&invoice_number=' . $this->data['invoicedata']['last_order_id'] , 'SSL');
 			$this->data['sendpdf']     = $this->url->link('functionkey/f4/SendPDFInvoice', 'token=' . $this->session->data['token'] . '&sendpdf_data=' .$this->data['invoicedata']['last_order_id'], 'SSL');

 			$this->model_pos_cart->clear();
			$this->template = 'pos/reprint.tpl';
			$this->response->setOutput($this->render());
		}
	}

	private function printInvoice($invoicenumber)
	{
       $this->load->model('pos/cart');
       $this->load->model('setting/customers');
       $this->template = 'functionkey/print.tpl';
       if (isset($invoicenumber)) {
			$invoice_id	= $invoicenumber;
			$data['order_id']	= $invoice_id;
			$invoiceCollection	= $this->model_pos_cart->getAllInvoiceByUser($data);
			if(!empty($invoiceCollection)) {
				$invoiceCollection = $invoiceCollection[0];
				if(!empty($invoiceCollection)) {
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				$productCollection	= $this->model_pos_cart->getProductsByOrder($invoiceCollection['order_id']);

				/*printArray($productCollection);
				printArray($this->session->data['update_price']);
				die;*/

				$this->load->model('master/company');
				$company_id	= $this->session->data['company_id'];
				$company_details	= $this->model_master_company->getCompanyDetails($company_id);
				$companyInfo	= $this->getCompanyAddress($company_details);
				if($companyInfo) {
					$this->data['companyInfo']	= $companyInfo;
				} else {
					$this->data['companyInfo']	='';
				}
				$cashierInfo	= $this->getCashierAndBillDetails($invoiceCollection);
				//Display Customer Name Starts//
				$customer_id = $this->session->data["attachedcustomer"];

	  			$CustomerInfo = $this->model_setting_customers->getCustomer($customer_id);

				if($invoiceCollection) {
					$this->data['CustomerInfo']	= $CustomerInfo;
				} else {
					$this->data['CustomerInfo']	= '';
				}
				$data['customer_id']	= $CustomerInfo['customer_id'];
				//Display Customer Name Ends//
				if($invoiceCollection) {
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				if($productCollection) {
					$this->data['productCollection']	= $productCollection;
				} else {
					$this->data['productCollection']	= '';
				}
				$data['payment_method']	= $invoiceCollection['payment_method'];
				//$paymentDetails	= $this->model_pos_cart->getAllPaymode($data);
				if($productCollection && $invoiceCollection['payment_details']!="") {
					$this->data['paymentDetails']	= unserialize($invoiceCollection['payment_details']);
				} else {
					$this->data['paymentDetails']	= '';
				}
				$this->data['paymentAmtdetails']    = $invoiceCollection['changes'];
				$data['order_id']	= $invoiceCollection['order_id'];
				$totalDetails	= $this->model_pos_cart->getTotalByOrder($data);
			/*	printArray($totalDetails);
				printArray($invoiceCollection);die;*/
				if($totalDetails) {
					$this->data['totalDetails']	= $totalDetails;
				} else {
					$this->data['totalDetails']	= '';
				}
				$this->data['change'] = $invoiceCollection['changes'];
			}
		}
		return $this->render();
	}

	public function getCompanyAddress($company_details)
	{
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address[]	= strtoupper($company_details['name']);
			$company_address[]	= strtoupper($company_details['address1']);
			$company_address[]	= strtoupper($company_details['address2']);
			$company_address[]	= strtoupper($company_details['city']." ".$company_details['state']);
			$company_address[]	= strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address[]	= "Phone: ".$company_details['phone']."  Fax: ".$company_details['fax'];
			foreach ($company_address as $key=>$value){
				if (strip_tags(trim($value))==''){
					unset($company_address[$key]);
				}
			}
			reset($company_address);
		}
		$company_address = implode("<br>", $company_address);
		return $company_address;
	}

	public function getCashierAndBillDetails($invoiceCollection)
	{
		$cashierAndBillArray	= array();
		$cashierAndBillArray[]	= "Date: ".date('d/m/y',strtotime($invoiceCollection['date_added']))."/t"."Time: ".date('h:m:s',strtotime($invoiceCollection['date_added']));
		$cashierAndBillArray[]	= "Cash Bill#: ".$invoiceCollection['invoice_no'];
		$cashierAndBillArray[]	= "Cashier: ".$invoiceCollection['cashier'];
		$cashierAndBillArray = implode("<br>", $cashierAndBillArray);
		return $cashierAndBillArray;
	}

	public function ordersuccess($saveData)
	{

			$total_data = array();
			$total = 0;
			$taxes = $this->cart->getTaxes();
	        $this->request->post = $saveData;
			$this->load->model('pos/cart');

			$total_data	= $this->model_pos_cart->getTotalsCollection();

			foreach($total_data as $totals) {
				if($totals['code']=='total') {
					//$total	= str_replace('$','',$totals['text']);
					$total	= $totals['value'];
				}
			}
			$data = array();

			$data['invoice_prefix'] = $this->config->get('config_invoice_prefix');

			if(isset($this->request->post['payment_code'])) {
				$data['payment_code']	=  $this->request->post['payment_code'];
			} else {
				$data['payment_code']	= '';
			}

			if(isset($this->request->post['payment_method'])) {
				$data['payment_method']	=  $this->request->post['payment_method'];
			} else {
				$data['payment_method']	= '';
			}

			if(isset($this->request->post['tendar'])) {
				$data['tendar']	=  $this->request->post['tendar'];
			} else {
				$data['tendar']	= '';
			}

			if(isset($this->request->post['changes'])) {
				$data['changes']	=  $this->request->post['changes'];
			} else {
				$data['changes']	= '';
			}

			if(isset($this->request->post['silp_number'])) {
				$data['silp_number']	=  $this->request->post['silp_number'];
			} else {
				$data['silp_number']	= '';
			}

			if(isset($this->request->post['cash_amount'])) {
				$data['cash_amount']	=  $this->request->post['cash_amount'];
				$payedamount['Cash'] =  $this->request->post['cash_amount'];
			} else {
				$data['cash_amount']	= '';
			}

			if(isset($this->request->post['card_amount'])) {
				$data['card_amount']	=  $this->request->post['card_amount'];
				$payedamount['Card']    =  $this->request->post['card_amount'];
			} else {
				$data['card_amount']	= '';
			}



			if(isset($this->request->post['visa_amount'])) {
				$data['visa_amount']	=  $this->request->post['visa_amount'];
				$payedamount['Visa']    = $this->request->post['visa_amount'];
			} else {
				$data['visa_amount']	= '';
			}

			$product_data = array();

			foreach ($this->cart->getProducts() as $product) {
				$product_data[] = array(
					'product_id' => $product['product_id'],
					'name'       => $product['name'],
					'quantity'   => $product['quantity'],
					'subtract'   => $product['subtract'],
					'price'      => $product['price'],
					'total'      => $product['total'],
					'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
					'reward'     => $product['reward']
				);
			}

			$data['products'] = $product_data;
			$data['totals'] = $total_data;
			$data['total'] = $total;
			$data['language_id'] = $this->config->get('config_language_id');
			$data['currency_id'] = $this->currency->getId();
			$data['currency_code'] = $this->currency->getCode();
			$data['currency_value'] = $this->currency->getValue($this->currency->getCode());
			$data['ip'] = $this->request->server['REMOTE_ADDR'];
			$data['payment_details'] = serialize($payedamount);

			if (!empty($this->request->server['HTTP_X_FORWARDED_FOR'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_X_FORWARDED_FOR'];
			} elseif(!empty($this->request->server['HTTP_CLIENT_IP'])) {
				$data['forwarded_ip'] = $this->request->server['HTTP_CLIENT_IP'];
			} else {
				$data['forwarded_ip'] = '';
			}

			$this->load->model('pos/cart');

			$last_order_id	= $this->model_pos_cart->addOrder($data);
			if(!empty($last_order_id)) {
				$invoicedata['order_id']	= $last_order_id;
				$invoiceDetails	= $this->model_pos_cart->getAllInvoiceByUser($invoicedata);
				$invoice_no	= $invoiceDetails[0]['invoice_no'];
			}

			if(!empty($invoice_no)) {
				$this->data['last_order_id']	= $invoice_no;
			} else {
				$this->data['last_order_id']	= '';
			}

			if(isset($this->request->post['changes'])) {
				$this->data['return_amount']	=  $this->currency->format($this->request->post['changes']);
			} else {
				$this->data['return_amount']	= '';
			}

			return $this->data;


	}

	public function getTotalsCollection() {

		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		$this->load->model('pos/addcart');

		$sort_order = array();

		$results = $this->model_pos_addcart->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('total/' . $result['code']);

				$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
			}

			$sort_order = array();

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		$this->session->data['totals']	= $total_data;
		return $total_data;
	}

	public function calculateAmount() {
	    $json = array();
		if (isset($this->request->post['dual_payment_method']) && !empty($this->request->post['dual_payment_method'])) {
		    $this->load->model('pos/cart');
		    $totals = $this->model_pos_cart->getTotalsCollection();

			if(!empty($totals)) {
			    $count		= count($totals);
			    $cartinfo 	= $totals[$count-1]['value'];
			    $cartTotal	= $cartinfo;
				$json = $cartTotal - $this->request->post['dual_payment_amount'];
		    }

			$this->response->setOutput(json_encode($json));
		}
	}

/// Enable Payment Mode
	//04-April-2016 Pdf Generator//
	public function DemoprintInvoice($invoicenumber)
	{
		include(DIR_SYSTEM."vendor/pdf/mpdf.php");
	    $invoicenumber = 100000321;
	  	$html = $this->printInvoice($invoicenumber);
	    $this->generatePDF($html, $invoicenumber.".pdf",true);
	}
	public function printPDFInvoice($invoicenumber)
	{

		include(DIR_SYSTEM."vendor/pdf/mpdf.php");
		$invoicenumber = $this->request->get['invoice_number'];
	  	$html = $this->printInvoice($invoicenumber);
	    $this->generatePDF($html, $invoicenumber.".pdf",true);
	}

	protected function generatePDF($html,$filename="test.pdf",$print =false,$email=false)
	{
	    $mpdf=new mPDF('c',array(80,297),'','',2,3,25,10,0,0);   //$mpdf = new mPDF('', 'A4', '', '', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer);
		$header='<header id="header"><div id="header-inner"><div id="logo"> <img style="margin:15px;"src="view/assets/img/logo.png" alt="Oblak Pos" title="Oblak Pos"></div></div>
			</header>';

		$mpdf->SetHTMLHeader($header);
		$footer='<div id="footer" style="margin:20px;"><p></p></div>';
		$mpdf->SetHTMLFooter($footer);
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

		// LOAD a stylesheet
		//$stylesheet = file_get_contents( HTTPS_SERVER.'view/stylesheet/pdf.css');
		$stylesheet = '
				.invoice-page{ width:400px; font-family: monospace; font-size: 13px; }
				.print-invoice{ margin:0 10px; width: 100%; float:left; font-family: monospace; text-align:left; padding-top: -45px;font-size: 13px; }
				.print-invoice p{ margin:0px auto; float:left; width:100%; float:left; margin-top: -20px;color:#111111;font-weight:normal;}
				.print-invoice .separation{border-top:1px dotted #000; line-height:5px; margin: 0;}
				.print-invoice table{padding: 0; margin:0px;font-family: monospace;}
				.print-invoice table tr{padding: 0 0 4px 0; margin:0px;font-family: monospace;}
				.print-invoice table td{/*padding:0px ; margin:0px;*/ font-family: monospace;}
				.pdf-date{ height: 27px;}
				.pdf-td{padding-top: -10px; padding-bottom: 8px; float: left;}
				.invoice-page { max-width:95%;}';
        if($print){
		    $mpdf->SetJS('this.print();');
         }
		$mpdf->WriteHTML($stylesheet,1);
		// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->WriteHTML($html,2);
		if($email==true){
			return $mpdf->Output($filename,'S');
		} else{
			 $mpdf->Output($filename,'I');
		}

	}
	public function SendPDFInvoice()
	{

		include(DIR_SYSTEM."vendor/pdf/mpdf.php");
		//$invoicenumber = $this->request->get['sendpdf_data'];
	  	$invoicenumber = $this->request->get['sendpdf_data'];
	  	$html = $this->printInvoice($invoicenumber);
	    $this->sendPDF($html, $invoicenumber.".pdf");
	}
	protected function sendPDF($html,$filename="test.pdf")
	{
	   $data = $this->generatePDF($html,$filename,false,true);
		$this->load->model('setting/customers');

		$results  	= $this->model_setting_customers->getCustomerOrderDetails($this->request->get['sendpdf_data']);
		$Email    	= $results['Email'];
		$Name       = $results['name'];
		$title 	  	= $results['customer_title'];
		//$date_added = $results['date_added'];
		$date_added	= date("Y-m-d g:i a", strtotime($results['date_added']));

		$email_from       = $this->config->get('config_email'); // Who the email is from
		$email_subject    = 'Oblakpos Invoice-'.$this->request->get['sendpdf_data']; // The Subject of the email
		$email_to 		  = $_POST['email']; // Who the email is to
		$semi_rand = md5(time());
		$data = chunk_split(base64_encode($data));

		$fileatt_type = "application/pdf"; // File Type
		$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

		// set header ........................
		$headers = "From: ".$email_from;
		$headers .= "\nMIME-Version: 1.0\n" .
		"Content-Type: multipart/mixed;\n" .
		" boundary=\"{$mime_boundary}\"";
		$fileatt_name =$filename;
		// set email message......................
		$email_message = '<html>
	    <head></head>
		<body style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:14px;">
			<table border="0" cellpadding="0" cellspacing="0" width="625px" style="border:1px solid #ddd;">
				<thead style="">
					<tr style="float:left;">
						<td align="left" style="width:400px;">
							<div style="width:100px; height:40px; overflow:hidden;">
								<img style="width:92%; margin:15px 8px 8px 10px;" src="'.HTTPS_SERVER.'view/assets/img/logo.png" alt="Oblak Pos"/>
							</div>
						</td>
					</tr>
					<tr style="float:right; width:145px;margin:4px;" >
						<td align="right" style="color: black; font-size:14px; ">Invoice #'.$this->request->get['sendpdf_data'].'</td>
					</tr>
					<tr style="float:right; width:186px;margin:4px;" >
						<td align="right" style="color: black; font-size:14px;">Date: '.$date_added.'</td>
					</tr>
				</thead>
				<tr>
					<td>
			    		<table border="0" cellpadding="0" cellspacing="0" width="625px" style="border-top:1px solid #ddd;">
						<tr>
							<td>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background:#fff; font-size:12px;">
							<tr><td height="20"></td></tr>
							<tr>
								<td colspan="3" style="color:#000; padding:0px 0px 0px 11px; text-align:left;"><b>Dear Customer,</b> </td>
							</tr>
							<tr>
								<td width="20%" colspan="3" style="vertical-align:top; padding-bottom:10px;"><p style="font-weight:500; padding-left:10px;">Please feel free to contact us at <a href="#"><u><b>oblaksolutions.com</b> </u></a>if you have any questions or concerns about this invoice</p></td>
							</tr>
							<tr>
								<td width="20%" colspan="3" style="vertical-align:top; padding-bottom:10px;"><p style="font-weight:bold; padding-left:10px;">Regards,</p>
								<p style="font-weight:bold; padding-left:10px; width:400px;">Team OblakPos.</p>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</td>
			</tr>
			</table>
		</body></html>';
		$email_message .= "This is a multi-part message in MIME format.\n\n" .
		"--{$mime_boundary}\n" .
		"Content-Type:text/html; charset=\"iso-8859-1\"\n" .
		"Content-Transfer-Encoding: 7bit\n\n" .
		$email_message .= "\n\n";
		$email_message .= "--{$mime_boundary}\n" .
		"Content-Type: {$fileatt_type};\n" .
		" name=\"{$fileatt_name}\"\n" .
		"Content-Disposition: attachment;\n" .
		" filename=\"{$fileatt_name}\"\n" .
		"Content-Transfer-Encoding: base64\n\n" .
		$data .= "\n\n" .
		"--{$mime_boundary}--\n";

		$sent = mail($email_to, $email_subject, $email_message, $headers);
		if($sent) {
		$str = 1;
		} else {
		$str = 0;
		}
		echo $str;
	}

}
?>