<?php 
class ControllerFunctionkeyF2 extends Controller {

	public function index() {
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$this->data['action'] = $this->url->link("functionkey/f2/applydiscount",'&token='.$this->request->get['token'],"SSL");
		
		if(isset($this->session->data['discount_mode']) && $this->session->data['discount_mode']) {
			$this->data['discount_mode']	= $this->session->data['discount_mode'];
		} else {
			$this->data['discount_mode']	= '';
		}
		
		if(isset($this->session->data['discount']) && $this->session->data['discount']) {
			$this->data['voucher_discount']	= $this->session->data['discount'];
		} else {
			$this->data['voucher_discount']	= '';
		}
		
		$this->load->model('pos/cart');
		
		$totals = $this->model_pos_cart->getTotalsCollection();
		$_discountModeCollection		= $this->model_pos_cart->getAllDiscountMode($data);
		

		if(!empty($_discountModeCollection)) {
			$this->data['discountCollection']	= $_discountModeCollection;
		} else {
			$this->data['discountCollection']	= '';	
		}
		
		$this->data['totals']		= $totals;

		$this->template = 'functionkey/f2.tpl';
		$this->children = array();
		$this->response->setOutput($this->render());
	}
	
	public function applydiscount() {

		$is_error	= false;
		$message	= array();
		$postdata	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$discount_mode	= $this->request->post['discount_mode'];
			$voucher_discount	= $this->request->post['voucher_discount'];
			$postdata['discount_mode']	= $discount_mode;
			$postdata['voucher_discount']	= $voucher_discount;
			//$discount_mode	= 1;
			//$voucher_discount	= 25;
			
			if(!empty($discount_mode)) {
				if($discount_mode ==1) {
					if($voucher_discount <= 100) {
						$this->session->data['discount']	= $voucher_discount; 
						$this->session->data['discount_mode']	= 1;	
					} else {
						$is_error	= true;
						$message['error']	= 'Please enter voucher discount percentage below 100';
					}		
				} elseif($discount_mode ==2) {
					$this->load->model('pos/cart');
					$totals	= $this->model_pos_cart->getTotalsCollection();
					if(!empty($totals)) {
						$count		= count($totals);
						foreach($totals as $total) {
							if($total['code'] =='sub_total') {
								$cartinfo 	= $total['value'];
							}
						}
						//$cartinfo 	= $totals[$count-1]['text'];
						//$cartTotal	= trim(str_replace('$','',$cartinfo));
						//$cartinfo 	= $totals['code']['value'];
						$cartTotal	= $cartinfo;
						if($voucher_discount <= $cartTotal) {
							$this->session->data['discount']	= $voucher_discount; 
							$this->session->data['discount_mode']	= 2;
						} else {
							$is_error	= true;
							$message['error']	= 'Please enter voucher discount below '.$cartinfo;
						}
					}
				}
			} else {
				$is_error	= true;
				$message['error']	= 'Please enter voucher discount value';	
			}
		
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= true;
			$message['postdata']	= $postdata;
			echo json_encode($message);
		}
	}
	
	public function reloadcart() {
		
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}

		$this->template = 'default/template/common/content_main.tpl';
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom'
		);
		$this->response->setOutput($this->render());
	}
}
?>