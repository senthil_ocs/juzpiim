<?php 
class ControllerFunctionkeyPrint extends Controller {
	
	public function index() {
	
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$this->template = 'functionkey/print.tpl';

		$this->load->model('pos/cart');
		
		if (isset($this->request->post['invoice_id'])) {
			$invoice_id	= $this->request->post['invoice_id'];
			$data['order_id']	= $invoice_id;
			$invoiceCollection	= $this->model_pos_cart->getAllInvoiceByUser($data);
			if(!empty($invoiceCollection)) {
				$invoiceCollection = $invoiceCollection[0];
				if(!empty($invoiceCollection)) { 
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				$productCollection	= $this->model_pos_cart->getProductsByOrder($invoiceCollection['order_id']);
				$this->load->model('master/company');
				$company_id	= $this->session->data['company_id'];
				$company_details	= $this->model_master_company->getCompanyDetails($company_id);
				$companyInfo	= $this->getCompanyAddress($company_details);
				if($companyInfo) {
					$this->data['companyInfo']	= $companyInfo;
				} else {
					$this->data['companyInfo']	='';
				}
				$cashierInfo	= $this->getCashierAndBillDetails($invoiceCollection);
				if($invoiceCollection) {
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				if($productCollection) {
					$this->data['productCollection']	= $productCollection;
				} else {
					$this->data['productCollection']	= '';
				}
				$data['payment_method']	= $invoiceCollection['payment_method'];
				$paymentDetails	= $this->model_pos_cart->getAllPaymode($data);
				if($productCollection) {
					$this->data['paymentDetails']	= $paymentDetails;
				} else {
					$this->data['paymentDetails']	= '';
				}
				$data['order_id']	= $invoiceCollection['order_id'];
				$totalDetails	= $this->model_pos_cart->getTotalByOrder($data);
				if($totalDetails) {
					$this->data['totalDetails']	= $totalDetails;
				} else {
					$this->data['totalDetails']	= '';
				}
			}
		}
		$this->response->setOutput($this->render());
	}
	
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address[]	= strtoupper($company_details['name']);
			$company_address[]	= strtoupper($company_details['address1']);
			$company_address[]	= strtoupper($company_details['address2']);
			$company_address[]	= strtoupper($company_details['city']." ".$company_details['state']);
			$company_address[]	= strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address[]	= "Phone: ".$company_details['phone']."  Fax: ".$company_details['fax'];
			foreach ($company_address as $key=>$value){
				if (strip_tags(trim($value))==''){
					unset($company_address[$key]);
				}
			}
			reset($company_address);
		}
		$company_address = implode("<br>", $company_address);
		return $company_address;
	}
	
	public function getCashierAndBillDetails($invoiceCollection) {
		$cashierAndBillArray	= array();
		$cashierAndBillArray[]	= "Date: ".date('d/m/y',strtotime($invoiceCollection['date_added']))."/t"."Time: ".date('h:m:s',strtotime($invoiceCollection['date_added']));
		$cashierAndBillArray[]	= "Cash Bill#: ".$invoiceCollection['invoice_no'];
		$cashierAndBillArray[]	= "Cashier: ".$invoiceCollection['cashier'];
		$cashierAndBillArray = implode("<br>", $cashierAndBillArray);
		return $cashierAndBillArray;
	}
	
}
?>