<?php 
class ControllerFunctionkeyTerminalSales extends Controller {
	
	public function index() {	
		$is_error	= false;
		$message	= array();
		$this->data['is_terminal_sale']	= '';
		$this->data['manual_grant_total_value'] = 0;
		$this->data['computer_grant_total_value'] = 0;
		//$this->data['price_types']	= array('1000', '500', '100', '50', '20', '10', '5', '2', '1', '0.50', '0.10', '0.05', '0.01');
		$this->data['price_types']	= array('1000', '100', '50', '10', '5', '2', '1', '0.50', '0.05', '0.01');
		if (isset($this->request->get['op'])) {
			$route = (string)$this->request->get['op'];
		} else {
			$route = 'common/home';
		}
		$this->load->model('ajax/cart');
		
		if (!empty($this->session->data['user_id'])) {
		   $type = 'user';
		   $user_id = $this->session->data['user_id'];
		} else {
			$type = 'cashier';
			$user_id = $this->session->data['cashier_id'];
		}
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {			
			$this->data['is_terminal_sale'] = $this->request->post['is_terminal_sale'];
			$this->data['is_terminal_sale']	= 1;
			if (is_numeric( $this->request->post['amount'] ) && floor( $this->request->post['amount'] ) != $this->request->post['amount']) {
				$amount	= number_format($this->request->post['amount'], 2, '.', '');
			} else {
			   $amount	= $this->request->post['amount'];
			}
			$qty	= $this->request->post['qty'];
			if(!empty($amount) && $qty >= 0) {
				$this->session->data['settlement_cash'][$amount]	= 	 $qty;
			}
		} else {
		
			$terminalCollection = $this->model_ajax_cart->dogetTermialSales();
			if (!empty($terminalCollection)) {
				foreach($terminalCollection as $terminalValue) {
				    if (is_numeric( $terminalValue['cash'] ) && floor( $terminalValue['cash'] ) != $terminalValue['cash']) {
						$amount	= number_format($terminalValue['cash'], 2, '.', '');
					} else {
					   $amount	= (int)$terminalValue['cash'];
					}
					$this->session->data['settlement_cash'][$amount]	= 	 $terminalValue['number_cash_type'];					
				}
			}	
			
		}
		
		$this->data['settlementCash']	= $this->session->data['settlement_cash'];
		
		$pay_mode = "CASH";
		$cashTotal = $this->model_ajax_cart->dogetComputerTotal($pay_mode);		
		$cardTotal = $this->model_ajax_cart->dogetComputerTotal();
		
		$expenseTotal = $this->model_ajax_cart->dogetExpensesTotal();
		
		$this->data['computer_cash_total'] = $this->currency->format($cashTotal['cashTotal']);
		$this->data['computer_card_total'] = $this->currency->format($cardTotal['cardTotal']);
		$this->data['computer_grant_total_value'] = number_format(((float)$cashTotal['cashTotal'] + (float)$cardTotal['cardTotal']) - ((float)$expenseTotal['expenseTotal']), 2, '.', '');
		$this->data['computer_grant_total'] = $this->currency->format($this->data['computer_grant_total_value']);
		$this->data['expense_total_amount'] = $this->currency->format("-".$expenseTotal['expenseTotal']);
		
		$this->session->data['terminal_sale']['computer_cash']        = (float)$cashTotal['cashTotal'];
		$this->session->data['terminal_sale']['computer_card']        = (float)$cardTotal['cardTotal'];
		$this->session->data['terminal_sale']['computer_expenses']    = "-".(float)$expenseTotal['expenseTotal'];
		$this->session->data['terminal_sale']['computer_grant_total'] = $this->data['computer_grant_total_value'];
		
		//if (!empty($this->session->data['settlement_cash'])) {		    
			$totalValue = 0;
			foreach($this->session->data['settlement_cash'] as $key=>$cash) {
			    $totalValue += (float)$key * (float)$cash;
			}
			$this->data['manual_total'] = $this->currency->format($totalValue);
			$this->data['manual_grant_total_value'] = number_format(((float)$totalValue + (float)$cardTotal['cardTotal']) - ((float)$expenseTotal['expenseTotal']), 2, '.', '');
			$this->data['manual_grant_total'] = $this->currency->format($this->data['manual_grant_total_value']);
			
			$this->session->data['terminal_sale']['manual_cash']        = (float)$totalValue;
			$this->session->data['terminal_sale']['manual_card']        = (float)$cardTotal['cardTotal'];
			$this->session->data['terminal_sale']['manual_expenses']    = "-".(float)$expenseTotal['expenseTotal'];
			$this->session->data['terminal_sale']['manual_grant_total'] = $this->data['manual_grant_total_value'];
		//}		
		$cashierDetails = $this->user->dogetCashierDetails($user_id, $type);
		if (!empty($cashierDetails['user_id'])) {
		    $this->data['cashier_id'] = $cashierDetails['user_id'];
		    $this->data['cashier_name'] = $cashierDetails['username'];
		} else {
		    $this->data['cashier_id'] = $cashierDetails['cashier_id'];
		    $this->data['cashier_name'] = $cashierDetails['cashier_name'];
		}		
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/functionkey/terminal_sales.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/functionkey/terminal_sales.tpl';
		} else {
			$this->template = 'default/template/functionkey/terminal_sales.tpl';
		}		
		if (empty($this->request->post['amount']) && ($this->data['is_terminal_sale'] != 1)) {
			$this->children = array(
				'common/popup_header',
				'common/popup_footer'
			);
		}

		$this->response->setOutput($this->render());
	}
	
	public function terminalSaleLogin() {
	    $is_error	= false;
		$message	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$cashier_password	= $this->request->post['cashier_password'];			
			if(!empty($cashier_password)) {
				$email = $this->session->data['email'];
				$loginData = $this->user->login($email, $cashier_password, '');
				if (empty($loginData)) {
				    $is_error	= true;
				    $message['error']	= 'Please enter valid Password';
				}
			} else {
			    $is_error	= true;
				$message['error']	= 'Please enter your Password';
			}			
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}		
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= 'You have verified successfully.';	
			echo json_encode($message);
		}		
	}
	
	public function calculateAmount() {
	    $is_error	= false;
		$result	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$grant_total	= $this->request->post['grant_total'];
			$manual_total	= $this->request->post['manual_total'];
			if ($grant_total > 0 && $manual_total > 0) {
			    $this->load->model('ajax/cart');
				if ($grant_total == $manual_total) {
				    $is_error	= false;					
					$last_terminal_id = $this->model_ajax_cart->addTerminalSales($this->session->data['terminal_sale']);
					$result['success']	= 'Terminal sales updated successfully';
				} else {
				    $is_error	= true;
			        $result['error']	= 'Total values not matching';
				}
			} else {
			    $is_error	= true;
			    $result['error']	= 'Please enter all required values';
			}
			
		} else {
			$is_error	= true;
			$result['error']	= 'There is some problem. Please contact admin.';
		}		
		if($is_error) {
			echo json_encode($result);
		} else {
			echo json_encode($result);
		}		
	}
	
	public function clearSession() {
		unset($this->session->data['settlement_cash']);
		unset($this->session->data['terminal_sale']);
		$result = true;
		echo json_encode($result);		
	}
	
}
?>