<?php 
class ControllerFunctionkeyF6 extends Controller {

	public function index() {
		
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$is_error	= false;
		$message	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
			$this->load->model('pos/cart');
			$this->request->post['hold_bill_no'] = $this->model_pos_cart->getQuoteID();
			if(isset($this->request->post['hold_bill_no']) && ctype_digit($this->request->post['hold_bill_no'])) {
				$cart_products	= $this->cart->getProducts();
				if(!empty($cart_products)) {
					foreach ($cart_products as $product) {
						$product_data[] = array(
							'bill_no' => $this->request->post['hold_bill_no'],
							'product_id' => $product['product_id'],
							'quantity'   => $product['quantity'],
							'discount'   => $product['reward']
						); 
					}
					$data['bill_no']	= $this->request->post['hold_bill_no'];
					$data['products'] = $product_data;
					$result	= $this->model_pos_cart->addHoldProduct($data);
					if(!$result) {
						$is_error	= true;
						$message['error']	= 'Please enter unique bill no.';	
					}
				} else {
					$is_error	= true;
					$message['error']	= 'The Product entry is empty.';
				}
			} else {
				$is_error	= true;
				$message['error']	= 'Please enter valid bill number.';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		
		if($is_error) {
			echo json_encode($message);
		} else {
			$this->data['newsale']      = $this->url->link("pos/pos","&token=".$this->request->get['token'],"SSL");
			//$this->data['hold_bill_no'] = $this->printInvoice($this->request->post['hold_bill_no']);
			$this->data['hold_bill_no'] = $this->request->post['hold_bill_no'];
			$this->model_pos_cart->clear();
			$this->template = 'pos/quotes.tpl';
			$this->response->setOutput($this->render());
		}
	}

	private function printInvoice($invoicenumber)
	{
       $this->load->model('pos/cart');
       $this->template = 'functionkey/print.tpl';
       if (isset($invoicenumber)) {
			$invoice_id	= $invoicenumber;
			$data['order_id']	= $invoice_id;
			$invoiceCollection	= $this->model_pos_cart->getAllInvoiceByUser($data);
			if(!empty($invoiceCollection)) {
				$invoiceCollection = $invoiceCollection[0];
				if(!empty($invoiceCollection)) { 
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				$productCollection	= $this->model_pos_cart->getProductsByOrder($invoiceCollection['order_id']);
				$this->load->model('master/company');
				$company_id	= $this->session->data['company_id'];
				$company_details	= $this->model_master_company->getCompanyDetails($company_id);
				$companyInfo	= $this->getCompanyAddress($company_details);
				if($companyInfo) {
					$this->data['companyInfo']	= $companyInfo;
				} else {
					$this->data['companyInfo']	='';
				}
				$cashierInfo	= $this->getCashierAndBillDetails($invoiceCollection);
				if($invoiceCollection) {
					$this->data['invoiceCollection']	= $invoiceCollection;
				} else {
					$this->data['invoiceCollection']	= '';
				}
				if($productCollection) {
					$this->data['productCollection']	= $productCollection;
				} else {
					$this->data['productCollection']	= '';
				}
				$data['payment_method']	= $invoiceCollection['payment_method'];
				$paymentDetails	= $this->model_pos_cart->getAllPaymode($data);
				if($productCollection) {
					$this->data['paymentDetails']	= $paymentDetails;
				} else {
					$this->data['paymentDetails']	= '';
				}
				$data['order_id']	= $invoiceCollection['order_id'];
				$totalDetails	= $this->model_pos_cart->getTotalByOrder($data);
				if($totalDetails) {
					$this->data['totalDetails']	= $totalDetails;
				} else {
					$this->data['totalDetails']	= '';
				}
			}
		}
		return $this->render();
	}
	
	public function placeorder() {
	
		$is_error	= false;
		$message	= array();
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		
			$saveData	= array();
			$saveData['changes']	= $this->request->post['balance_amount'];
			$saveData['tendar']	= $this->request->post['paid_amount'];
			$saveData['payment_method']	= $this->request->post['payment_mode'];
			$saveData['silp_number']	= $this->request->post['silp_number'];
			
			$this->load->model('pos/cart');
			$totals	= $this->model_pos_cart->getTotalsCollection();
			if(!empty($totals)) {
				$count		= count($totals);
				//$cartinfo 	= $totals[$count-1]['text'];
				//$cartTotal	= trim(str_replace('$','',$cartinfo));
				$cartinfo 	= $totals[$count-1]['value'];
				$cartTotal	= $cartinfo;
			}
			if(!empty($saveData['payment_method'])) {
				$paymentDetails	= $this->model_pos_cart->getAllPaymode($saveData);
				if(!empty($paymentDetails)) {
					foreach($paymentDetails as $paymentDetail) {
						$saveData['payment_code']	= str_replace(' ','-',strtolower(trim($paymentDetail['payment_name'])));
						if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
							if($saveData['tendar'] >= $cartTotal) {
								if($saveData['tendar']==$cartTotal) {
									$saveData['changes']	= '0.00';	
								} else {
									$saveData['changes']	= (float)$saveData['tendar'] - (float)$cartTotal;	
								}
								$saveData['silp_number']	= '';
							} else {
								$is_error	= true;
								$message['error']	= 'Please enter valid paid amount.';
							}
						} else {
							$saveData['tendar']	= $cartTotal;
							$saveData['changes']	= '0.00';
							if(empty($saveData['silp_number'])) {
								$is_error	= true;
								$message['error']	= 'Please enter bill slip number.';	
							}
						}
					}	
				} else {
					$is_error	= true;
					$message['error']	= 'There is some problem. Please contact admin.';
				}
			} else {
				$is_error	= true;
				$message['error']	= 'Please select any one payment mode';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'There is some problem. Please contact admin.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= true;
			$message['orderdata']	= $saveData;
			echo json_encode($message);
		}
	}
}
?>