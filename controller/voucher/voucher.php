<?php 
class ControllerVoucherVoucher extends Controller {
	private $error = array();
 
	public function index() {

		$this->language->load('voucher/voucher');

		$this->document->setTitle($this->language->get('Voucher'));

		$this->load->model('voucher/voucher');

		$this->getList();
		
	}

	public function insert() {

		$this->language->load('voucher/voucher');

		$this->document->setTitle($this->language->get('Voucher'));

		$this->load->model('voucher/voucher');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {  

			$this->model_voucher_voucher->addvoucher($this->request->post);


			$this->session->data['success'] = $this->language->get('Added Successfully');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}


			$this->redirect($this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
		}
		$this->getForm();		
	}


	public function update() {

		$this->language->load('voucher/voucher');

		$this->document->setTitle($this->language->get('Voucher'));
		
		$this->load->model('voucher/voucher');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {  

			$this->model_voucher_voucher->editvoucher($this->request->post,$this->request->get['voucher_code']);
			
			$this->session->data['success'] = $this->language->get('Modified Successfully');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$type = $this->request->get['type'];
			$this->redirect($this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			
			
		}

		$this->getForm();
	}

	public function delete() {
    	$this->language->load('voucher/voucher');
 
		$this->document->setTitle($this->language->get('Voucher'));
		
		$this->load->model('voucher/voucher');
		
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $voucher_code) {
				$this->model_voucher_voucher->deletevoucher($voucher_code);
			}
			
			$this->session->data['success'] = $this->language->get('Deleted Successfully');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}


	public function getList(){


		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$this->data['filter_date_from'] = $filter_date_from;
		} else {
			$filter_date_from = null;
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$this->data['filter_date_from'] = '';
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$this->data['filter_date_to'] = $filter_date_to;
		} else {
			$filter_date_to = null;
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$this->data['filter_date_to'] = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}

		if (isset($_REQUEST['filter_voucher_code'])) {
			$filter_voucher_code = $_REQUEST['filter_voucher_code'];
			$pageUrl.= '&filter_voucher_code='.$filter_voucher_code;
		} else {
			$filter_voucher_code = '';
			$pageUrl.= '&filter_voucher_code='.$filter_voucher_code;
		}

		$this->data['filter_voucher_code'] = $filter_voucher_code;
		$this->data['filter_location'] = $filter_location_code;

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'createdon';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			

		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_voucher_code'])) {
			$url.= '&filter_voucher_code='.$this->request->post['filter_voucher_code'];
		}
				
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('voucher/voucher/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('voucher/voucher/delete', 'token=' . $this->session->data['token'] . $url, 'SSL'); 
		$this->data['vouchers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'filter_voucher_code'=>$filter_voucher_code,
			'filter_location_code'=>$filter_location_code,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_voucher_code' => $filter_voucher_code
		);
			
		 $voucher_total = $this->model_voucher_voucher->getTotalvouchers($data);
		 $this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		 $results = $this->model_voucher_voucher->getvouchers($data);
		// printArray($results); die;
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('voucher/voucher/update', 'token=' . $this->session->data['token'] . '&voucher_code=' . $result['voucher_code'] . $url, 'SSL')
			);

			$this->data['vouchers'][] = array(
				'voucher_code'			=> $result['voucher_code'],
				'voucher_amount'      	=> $result['voucher_amount'],
				'valid_from'			=> date('d/m/Y',strtotime($result['valid_from'])),
				'valid_to'				=> date('d/m/Y',strtotime($result['valid_to'])),
				'is_active'				=> $result['is_active'],
				'createdon'				=> date('d/m/Y H:i:s',strtotime($result['createdon'])),
				'selected'          	=> isset($this->request->post['selected']) && in_array($result['voucher_code'], $this->request->post['selected']),
				'action'            	=> $action
			);
		}
		//printArray($vouchers);exit();
		
		$this->data['heading_title'] = $this->language->get('Voucher');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Voucher List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		//$this->data['sort_voucher_name'] = $this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . '&sort=voucher_name' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $voucher_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'voucher/voucher_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());

	}

	public function getForm() {

		$this->load->model('voucher/voucher');

		$this->data['heading_title'] = $this->language->get('Voucher');
		$this->data['entry_voucher_name'] = $this->language->get('entry_voucher_name');
		$this->data['entry_voucher_amount'] = $this->language->get('entry_voucher_amount');
		$this->data['entry_valid_from'] = $this->language->get('entry_valid_from');
		$this->data['entry_valid_to'] = $this->language->get('entry_valid_to');
		$this->data['entry_is_active'] = $this->language->get('entry_is_active');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['voucher_amount'])) {
			$this->data['error_voucher_amount'] = $this->error['voucher_amount'];
		} else {
			$this->data['error_voucher_amount'] = '';
		}		

		if (isset($this->error['voucher_amount1'])) {
			$this->data['error_voucher_amount1'] = $this->error['voucher_amount1'];
		} else {
			$this->data['error_voucher_amount1'] = '';
		}
		if (isset($this->error['voucher_code'])) {
			$this->data['error_voucher_code'] = $this->error['voucher_code'];
		} else {
			$this->data['error_voucher_code'] = '';
		}
		if (isset($this->error['from_code'])) {
			$this->data['error_from_code'] = $this->error['from_code'];
		} else {
			$this->data['error_from_code'] = '';
		}
		if (isset($this->error['to_code'])) {
			$this->data['error_to_code'] = $this->error['to_code'];
		} else {
			$this->data['error_to_code'] = '';
		}
	
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Settings',
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Voucher List'),
			'href'		=> $this->url->link('voucher/voucher', 'token=' .$this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['voucher_code'])) {
			
		   	$this->data['action'] = $this->url->link('voucher/voucher/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
		
			$this->data['action'] = $this->url->link('voucher/voucher/update', 'token=' . $this->session->data['token'] . '&voucher_code=' . $this->request->get['voucher_code'] . $url, 'SSL');
		}	
		$this->data['cancel'] = $this->url->link('voucher/voucher', 'token=' . $this->session->data['token'] . $url, 'SSL');

		
		
		if (isset($this->request->get['voucher_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$voucher_info = $this->model_voucher_voucher->getvoucher($this->request->get['voucher_code']);
		}

		$this->load->model('setting/location');
		$this->data['locations'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
		$this->data['user_location_code']  = $this->session->data['location_code'];

		if(isset($this->request->post['voucher_code'])){
			$this->data['voucher_code'] =$this->request->post['voucher_code'];
		} elseif (!empty($voucher_info)) {
			$this->data['voucher_code'] = trim($voucher_info['voucher_code']);
		} else {			
			$this->data['voucher_code'] ='';
		}

		if(isset($this->request->post['prefix'])){
			$this->data['prefix'] =$this->request->post['prefix'];
		} else {			
			$this->data['prefix'] ='';
		}

		if(isset($this->request->post['from_code'])){
			$this->data['from_code'] =$this->request->post['from_code'];
		} else {			
			$this->data['from_code'] ='';
		}
		
		if(isset($this->request->post['to_code'])){
			$this->data['to_code'] =$this->request->post['to_code'];
		} else {			
			$this->data['to_code'] ='';
		}	
		
		if(isset($this->request->post['voucher_amount'])){
			$this->data['voucher_amount'] =$this->request->post['voucher_amount'];
		} elseif (!empty($voucher_info)) {
			$this->data['voucher_amount'] = trim($voucher_info['voucher_amount']);
		} else {			
			$this->data['voucher_amount'] ='';
		}


		
		if(isset($this->request->post['valid_from'])) {
			$this->data['valid_from'] = $this->request->post['valid_from'];
		} elseif(!empty($voucher_info)) {
			$this->data['valid_from'] = $voucher_info['valid_from'];
		} else {
			$this->data['valid_from'] == '';
		}

		if (isset($this->request->post['valid_to'])) {
			$this->data['valid_to'] = $this->request->post['valid_to'];
		} elseif(!empty($voucher_info)) {
			$this->data['valid_to'] = $voucher_info['valid_to'];
		} else {
			$this->data['valid_to'] = '';
		}		
	
		if (isset($this->request->post['is_active'])) {
			$this->data['is_active'] = $this->request->post['is_active'];
		} elseif (!empty($voucher_info)) {
			$this->data['is_active'] = $voucher_info['is_active'];
		} else {
			$this->data['is_active'] = '';
		}
		if(isset($this->request->post['voucher_amount1'])){
			$this->data['voucher_amount1'] =$this->request->post['voucher_amount1'];
		} else {			
			$this->data['voucher_amount1'] ='';
		}


		
		if(isset($this->request->post['valid_from1'])) {
			$this->data['valid_from1'] = $this->request->post['valid_from1'];
		} else {
			$this->data['valid_from1'] == '';
		}

		if (isset($this->request->post['valid_to1'])) {
			$this->data['valid_to1'] = $this->request->post['valid_to1'];
		} else {
			$this->data['valid_to1'] = '';
		}		
	
		if (isset($this->request->post['is_active1'])) {
			$this->data['is_active1'] = $this->request->post['is_active1'];
		} else {
			$this->data['is_active1'] = '';
		}
		if (isset($this->request->post['location'])) {
			$this->data['location_code'] = $this->request->post['location'];
		} elseif (!empty($voucher_info)) {
			$this->data['location_code'] = explode(',',$voucher_info['location_code']);
		} else {
			$this->data['location_code'] = '';
		}
		$this->data['token'] = $this->session->data['token'];
		$this->data['route'] = 'voucher/voucher';

		$this->template = 'voucher/voucher_form.tpl';

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	

	public function validateForm()
	{
		if (!$this->user->hasPermission('modify', 'voucher/voucher')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	if($this->request->post['type'] == "direct"){

		if (empty($this->request->post['voucher_amount'])) {
			$this->error['voucher_amount'] = $this->language->get('Enter Amount');
		}	
		
		if (empty($this->request->post['voucher_code'])) {
			$this->error['voucher_code'] = $this->language->get('Enter Code');
		}
	}
	if($this->request->post['type'] == "range"){
		if (empty($this->request->post['voucher_amount1'])) {
			$this->error['voucher_amount1'] = $this->language->get('Enter Amount');
		}	
		
		if (empty($this->request->post['from_code'])) {
			$this->error['from_code'] = $this->language->get('Enter Start Range');
		}
		if (empty($this->request->post['to_code'])) {
			$this->error['to_code'] = $this->language->get('Enter End Range');
		}
	}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
public function validateFormType()
	{
		if (!$this->user->hasPermission('modify', 'voucher/voucher')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		$voucher_info = $this->model_voucher_voucher->getvoucherGroup($this->request->post['voucher_code'],$this->request->post['voucher_amount']);
		
		if (empty($this->request->post['voucher_amount'])) {
			$this->error['voucher_amount'] = $this->language->get('Enter Amount');
		} elseif($this->request->post['voucher_amount'] != '' && count($voucher_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}

		if (empty($this->request->post['voucher_code'])) {
			$this->error['voucher_code'] = $this->language->get('Enter code');
		} elseif($this->request->post['voucher_code'] != '' && count($voucher_info) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}

?>