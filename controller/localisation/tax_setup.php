<?php
class ControllerLocalisationTaxSetup extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('localisation/tax_setup'); 

		$this->document->setTitle($this->language->get('heading_title'));	
		
		$this->data['heading_title']  = $this->language->get('heading_title');
		$this->data['text_general']   = $this->language->get('text_general');
		$this->data['text_tax_rate']       = $this->language->get("text_tax_rate");
		$this->data['tax_classes']    = $this->language->get("text_tax_classes");		
		
		$this->data['tax_rate'] 	= $this->url->link('localisation/tax_rate', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['tax_class']	= $this->url->link("localisation/tax_class", 'token=' . $this->session->data['token'], 'SSL');
		
			
		$this->template = 'localisation/tax_setup.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}	
}
?>