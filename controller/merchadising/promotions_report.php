<?php 
class ControllerMerchadisingPromotionsReport extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('merchadising/promotions_report');
		
		$this->language->load('inventory/inventory');

		$this->document->setTitle($this->language->get('promotion_heading_title'));
		
		$this->load->model('merchadising/promotions_report');
		
		$this->load->model('inventory/inventory');
		
		$this->getList();
	}

	protected function getList() {

		if (isset($this->request->get['date_start'])) {
			$date_start = $this->request->get['date_start'];
			$this->data['date_start'] = $this->request->get['date_start'];
		} else {
			$date_start = '';
			$this->data['date_start'] = '';
		}
		
		if (isset($this->request->get['date_end'])) {
			$date_end = $this->request->get['date_end'];
			$this->data['date_end'] = $this->request->get['date_end'];
		} else {
			$date_end = '';
			$this->data['date_end'] = '';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['date_start'])) {
			$url .= '&date_start=' . $this->request->get['date_start'];
		}
		
		if (isset($this->request->get['date_end'])) {
			$url .= '&date_end=' . $this->request->get['date_end'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['action'] = $this->url->link('merchadising/promotions_report/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['products'] = array();

		$data = array(
			'date_start'      => $date_start,
			'date_end'        => $date_end,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$product_total = $this->model_merchadising_promotions_report->getTotalSpecialProducts($data);

		$results = $this->model_merchadising_promotions_report->getSpecialProducts($data);

		foreach ($results as $result) {
			$special = false;
			$date_start = '';
			$date_end = '';

			$product_specials = $this->model_inventory_inventory->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] <= date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] >= date('Y-m-d'))) {
					$special = $product_special['price'];
					$date_start = date('d/m/Y', strtotime($product_special['date_start']));
					$date_end = date('d/m/Y', strtotime($product_special['date_end']));
					break;
				}					
			}

			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'name'       => $result['name'],
				'sku'        => $result['sku'],
				'price'      => $result['price'],
				'special'    => $special,
				'date_start' => $date_start,
				'date_end'   => $date_end,
				'quantity'   => $result['quantity'],
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
			);
		}

		$this->data['heading_title'] = $this->language->get('promotion_heading_title');		

		$this->data['text_enabled'] = $this->language->get('text_enabled');		
		$this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['text_promotion_period'] = $this->language->get('text_promotion_period');
		$this->data['text_from_date'] = $this->language->get('text_from_date');
		$this->data['text_to_date'] = $this->language->get('text_to_date');
		
		$this->data['column_invt_code'] = $this->language->get('column_invt_code');			
		$this->data['column_name'] = $this->language->get('column_name');		
		$this->data['column_price'] = $this->language->get('column_price');
		$this->data['column_promo_price'] = $this->language->get('column_promo_price');		
		$this->data['column_quantity_on_hand'] = $this->language->get('column_quantity_on_hand');		
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_promotion_period'] = $this->language->get('column_promotion_period');
		$this->data['column_selling_price'] = $this->language->get('column_selling_price');

		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');		
		$this->data['button_print'] = $this->language->get('button_print');
		$this->data['button_search'] = $this->language->get('button_search');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('merchadising/promotions_report', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'merchadising/promotions_report_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	public function printReports() {
		
		$this->language->load('merchadising/promotions_report');
		$this->language->load('inventory/inventory');
		
		$this->load->model('merchadising/promotions_report');		
		$this->load->model('inventory/inventory');

		$this->document->setTitle($this->language->get('promotion_heading_title'));
		$this->data['heading_title'] = $this->language->get('promotion_heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		if (isset($this->request->get['date_start'])) {
			$date_start = $this->request->get['date_start'];
			$this->data['date_start'] = $this->request->get['date_start'];
		} else {
			$date_start = '';
			$this->data['date_start'] = '';
		}
		
		if (isset($this->request->get['date_end'])) {
			$date_end = $this->request->get['date_end'];
			$this->data['date_end'] = $this->request->get['date_end'];
		} else {
			$date_end = '';
			$this->data['date_end'] = '';
		}
		
		$url = '';
		
		if (isset($this->request->get['date_start'])) {
			$url .= '&date_start=' . $this->request->get['date_start'];
		}
		
		if (isset($this->request->get['date_end'])) {
			$url .= '&date_end=' . $this->request->get['date_end'];
		}
		
		$this->data['back'] = $this->url->link('merchadising/promotions_report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		    
			$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_merchadising_promotions_report->getCompanyDetails($company_id);
			$companyInfo	= $this->getCompanyAddress($company_details);
			if($companyInfo) {
				$this->data['companyInfo']	= $companyInfo;
			} else {
				$this->data['companyInfo']	='';
			}
			
			$this->data['products'] = array();
			$data = array(
				'date_start'      => $date_start,
				'date_end'        => $date_end
			);	
			$results = $this->model_merchadising_promotions_report->getSpecialProducts($data);
	
			foreach ($results as $result) {
				$special = false;
				$date_start = '';
				$date_end = '';
	
				$product_specials = $this->model_inventory_inventory->getProductSpecials($result['product_id']);
	
				foreach ($product_specials  as $product_special) {
					if (($product_special['date_start'] == '0000-00-00' || $product_special['date_start'] <= date('Y-m-d')) && ($product_special['date_end'] == '0000-00-00' || $product_special['date_end'] >= date('Y-m-d'))) {
						$special = $product_special['price'];
						$date_start = date('d/m/Y', strtotime($product_special['date_start']));
						$date_end = date('d/m/Y', strtotime($product_special['date_end']));
						break;
					}					
				}
	
				$this->data['products'][] = array(
					'product_id' => $result['product_id'],
					'name'       => $result['name'],
					'sku'        => $result['sku'],
					'price'      => $result['price'],
					'special'    => $special,
					'date_start' => $date_start,
					'date_end'   => $date_end,
					'quantity'   => $result['quantity']
				);
			}
		} else {
		    $this->redirect($this->url->link('merchadising/promotions_report', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->template = 'merchadising/print.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']." ".$company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
		}
		return $company_address;
	}

}
?>