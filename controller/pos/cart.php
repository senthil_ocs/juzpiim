<?php 
class ControllerPosCart extends Controller {
	public function index() {
		$this->load->model('pos/cart');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$totals	= $this->model_ajax_cart->getTotalsCollection();
			$order_total	= '0.00';
			foreach($totals as $total) {
				if($total['code']=='total') {
					$order_total	= $total['value'];	
				}
			}
			if (isset($this->request->post['amount']) && $order_total !='0.00') {
				$amount	= (int)$this->request->post['amount'];
				if($amount >= $order_total) {
					$this->session->data['tender']['amount']	= $amount;
					$this->session->data['tender']['change']	= $amount - $order_total;
					$this->template = 'default/template/common/order.tpl';
					$this->children = array(
						'common/column_left',
						'common/order_block',
						'common/content_top',
						'common/content_bottom'
					);
					//$this->placeorder();
				} elseif($amount=='exact') {
					$this->session->data['tender']['amount']	= $order_total;
					$this->session->data['tender']['change']	= 0;
					$this->template = 'default/template/common/order.tpl';
					$this->children = array(
						'common/column_left',
						'common/order_block',
						'common/content_top',
						'common/content_bottom'
					);
				} else {
					$this->template = 'default/template/common/order.tpl';
					$this->children = array(
						'common/column_left',
						'common/column_right',
						'common/content_top',
						'common/content_bottom'
					);
				}
				$this->response->setOutput($this->render());
			}
		}
	}
	
	public function clear() {
	
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		
		unset($this->session->data['cart']);
		unset($this->session->data['totals']);
		unset($this->session->data['tender']);
		unset($this->session->data['last_product']);
		unset($this->session->data['order_id']);
		unset($this->session->data['discount']);
		unset($this->session->data['discount_mode']);
		unset($this->session->data['is_manually']);
		unset($this->session->data['is_exchange']);
		unset($this->session->data['attachedcustomer']);
		unset($this->session->data['update_price']);
		
		
		$this->response->setOutput('1');
		
  	}
	
	public function cancelorder() {
		$is_message	= false;		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$order_id	= $this->request->post['void_bill_no'];
			$is_verify	= isset($this->request->post['is_verify'])?$this->request->post['is_verify']:true;
			//$order_id	='100000001';
			//$is_verify	=false;

			if(!empty($order_id)) {
				$this->load->model('pos/cart');
				if($is_verify) {
					$order_info	= $this->model_pos_cart->isCheckOrder($order_id);
					if(!empty($order_info)) {
						$is_message	= true;
						$message    = 'Are youu Surely want to cancel invoice #'.$order_id ." ?";
					} else{
						$message    = 'Please Check the  invoice Number Entered';
					}
				} else {
					$order_info	= $this->model_pos_cart->cancelOrder($order_id);
					if(!empty($order_info)) {
						$is_message	= true;
						$message    = 'Invoice no #'.$order_id ." cancel success fully";
					}
				}
			}	
		} 
        $data = array("success"=>$is_message,"message"=>$message,"is_verify"=>$is_verify,"order_id"=>$order_id);
        $this->response->setOutput(json_encode($data));	
		
	}
	
	public function placeorder() {
	
		$is_error	= false;
		$message	= array();
		
		if($this->rules->getAccessConditionByRule('transaction')) {
			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
				$saveData	= array();
				$this->load->model('ajax/cart');
				$paymentCollection	= $this->model_ajax_cart->getDefaultPaymentMode();
				$tenderAmount	= $this->session->data['tender'];
				if(!empty($paymentCollection)) {
					$saveData['changes']	= $tenderAmount['change'];
					$saveData['tendar']		= $tenderAmount['amount'];
					$saveData['payment_method']	= $paymentCollection['payment_id'];
					$saveData['silp_number']	= '';
					$totals	= $this->model_ajax_cart->getTotalsCollection();
					if(!empty($totals)) {
						$count		= count($totals);
						$cartinfo 	= $totals[$count-1]['value'];
						$cartTotal	= $cartinfo;
					}
					if(!empty($saveData['payment_method'])) {
						$paymentDetails	= $this->model_ajax_cart->getAllPaymode($saveData);
						if(!empty($paymentDetails)) {
							foreach($paymentDetails as $paymentDetail) {
								$saveData['payment_code']	= str_replace(' ','-',strtolower(trim($paymentDetail['payment_name'])));
								if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
									if($saveData['tendar'] >= $cartTotal) {
										if($saveData['tendar']==$cartTotal) {
											$saveData['changes']	= '0.00';	
										} else {
											$saveData['changes']	= (float)$saveData['tendar'] - (float)$cartTotal;	
										}
										$saveData['silp_number']	= '';
									} else {
										$is_error	= true;
										$message['error']	= 'Please enter valid paid amount.';
									}
								} else {
									$saveData['tendar']	= $cartTotal;
									$saveData['changes']	= '0.00';
									if(empty($saveData['silp_number'])) {
										$is_error	= true;
										$message['error']	= 'Please enter bill slip number.';	
									}
								}
							}	
						} else {
							$is_error	= true;
							$message['error']	= 'There is some problem. Please contact admin.';
						}
					} else {
						$is_error	= true;
						$message['error']	= 'Please select any one payment mode';
					}
				} else {
					$is_error	= true;
					$message['error']	= 'Your store have no cash payment mode found.';
				}
			} else {
				$is_error	= true;
				$message['error']	= 'There is some problem. Please contact admin.';
			}
		} else {
			$is_error	= true;
			$message['error']	= 'Your domain have not transaction limit. Please upgrade and try again.';
		}
		if($is_error) {
			echo json_encode($message);
		} else {
			$message['success']	= true;
			$message['orderdata']	= $saveData;
			echo json_encode($message);
		}
	}
}
?>