<?php  
class ControllerPosPos extends Controller { 

	private $error = array();
	          
	public function index() 
	{ 
	   $this->language->load('pos/pos');

	   //$this->load->model('pos/cart');
	   //$this->data['LastOrder']  = $this->model_pos_cart->getLastOrderProduct();
       //$this->data['urlforhold'] = $this->url->link("functionkey/f6","&token=".$this->request->get['token'],'SSL');
       //$this->data['urlforrelease'] = $this->url->link('functionkey/f7','&token=' . $this->request->get['token'], 'SSL');
       //$this->data['reprint_order'] = $this->url->link('functionkey/print', '&token=' .$this->request->get['token'], 'SSL');
       //$this->data['void_order'] = $this->url->link('pos/cart/cancelorder', '&token=' .$this->request->get['token'], 'SSL');
        $this->data['search_customer'] = $this->url->link("pos/pos/getCustomers",'&token=' . $this->request->get['token'].'&type=pos', 'SSL');
        $this->data['payment_tender'] = $this->url->link("functionkey/f4/placeorder",'&token='.$this->request->get['token'],"SSL");
        $this->data['add_customer'] = $this->url->link("setting/customers/insert",'&token=' . $this->request->get['token'].'&type=pos', 'SSL');
        $this->data['add_newproduct'] = $this->url->link('inventory/inventory/insert','token=' . $this->request->get['token'].'&type=pos','SSL');

	   $this->data['logged'] = $this->user->getUserName();

		$this->template = 'pos/pos.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function getCategoryByDept()
	{
		$this->load->model('inventory/search');
		$fDepartment = $this->request->post['filter_department'];
		$Category = $this->model_inventory_search->getCategoryByDepart($fDepartment);
		echo json_encode($Category);
	}

	public function addProducts()
	{
		$this->language->load('setting/customers');
		$this->data['show'] = true;
		if(isset($this->request->post['search_form'])){
			$this->data['show'] =false;
		}
        $this->load->model('inventory/search');
		
		$this->load->model('inventory/inventory');

		$this->language->load('ajax/information');

		if (isset($this->request->post['filter_barcodes'])) {
			$filter_barcodes = $this->request->post['filter_barcodes'];
		} else {
			$filter_barcodes = null;
		}
		
		if (isset($this->request->post['filter_vendor'])) {
			$filter_vendor = $this->request->post['filter_vendor'];
		} else {
			$filter_vendor = null;
		}
		
		if (isset($this->request->post['filter_department'])) {
			$filter_department = $this->request->post['filter_department'];
		} else {
			$filter_department = null;
		}
		
		if (isset($this->request->post['filter_category'])) {
			$filter_category = $this->request->post['filter_category'];
		} else {
			$filter_category = null;
		}
		
		if (isset($this->request->post['filter_brand'])) {
			$filter_brand = $this->request->post['filter_brand'];
		} else {
			$filter_brand = null;
		}

		if (isset($this->request->post['filter_price'])) {
			$filter_price = $this->request->post['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->post['filter_priceto'])) {
			$filter_priceto = $this->request->post['filter_priceto'];
		} else{
           $filter_priceto = null;
		}
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->post['sort'])) {
			$sort = $this->request->post['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->post['order'])) {
			$order = $this->request->post['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->get['route'])) {
			$route = (string)$this->request->get['route'];
		} else {
			$route = 'common/home';
		}
		$this->data['out_of_stock'] = $this->language->get('out_of_stock');
		$this->load->model('pos/pos');
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
		if($page<=0){
			$page = 1;
		}

		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		// Category
		if($this->request->post['filter_department'] != ''){
			$this->load->model('inventory/search');			
			$this->data['category_collection'] = $this->model_inventory_search->getCategoryByDepart($this->request->post['filter_department']);
		}
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);

		$data = array(
			'filter_barcodes'	  => $filter_barcodes,
			'filter_vendor'	      => $filter_vendor,
			'filter_department'	  => $filter_department, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_price'	      => $filter_price,
			'filter_priceto'	  => $filter_priceto,
			'filter_name'  		  => $filter_name,
			'sort'                => $sort,
			'order'               => $order,
			'start'               => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'               => $this->config->get('config_admin_limit')
		);

		
		//$url	='index.php?route=functionkey/home_search';
		$this->data['filter_name'] = '';
		if (isset($this->request->post['searchkey'])&& !empty($this->request->post['searchkey'])) {
			$data['searchkey']	= trim($this->request->post['searchkey']);
			$this->data['filter_name'] = $this->request->post['searchkey'];
			//$_productDetails		= $this->model_pos_pos->getTotalProducts($data);
		} else {
		    //$_productDetails		= $this->model_pos_pos->getProductByName($data);
		}
        
		/*$product_data = array();	
		if(!empty($_productDetails)) {
		    $total	  = count($_productDetails);
		} else {
		    $total	  = 0;
		}*/
		$product_total = $this->model_inventory_inventory->getTotalProducts($data);
		$_productDetails		= $this->model_inventory_inventory->getProducts($data);
		/*$limit	= 5;
		$pagination	= $this->paginationHtml($page,$total,$limit,$url);
		$this->data['pagination'] = $pagination;
		$this->data['page'] = $page;*/
		$this->load->model('tool/image');
		
		if(!empty($_productDetails)) {			
			foreach($_productDetails as $result) {
				if ($result['image'] && file_exists(DIR_IMAGE . $result['image'])) {
				    $image = $this->model_tool_image->resize($result['image'], 40, 40);
				} else {
				    $image = $this->model_tool_image->resize('no_image.jpg', 40, 40);
				}
				$this->data['product_collection'][] = array(
					'product_id' => $result['product_id'],
					'name'       => $result['name'],
					'sku'        => $result['sku'],
					'barcodes'   => $this->model_pos_pos->getProductBarcodes($result['product_id']),
					'price'      => $result['price'],
					'special'    => $this->model_pos_pos->getProductSpecials($result['product_id']),
					'quantity'   => $result['quantity'],
					'image'   => $image,
					//'selected'   => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),

					'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
				);
			}
		} 
		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('pos/pos/addProducts', '&token=' . $this->session->data['token'] .  '&page={page}', 'SSL');
		$this->data['action'] = $this->url->link('pos/pos/addProducts', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['token'] = $this->request->get['token'];

		$this->template = 'pos/addproducts.tpl';
		$this->children = array(
			/*'common/header',
			'common/sidebar',
			'common/footer'*/
		);
		$this->response->setOutput($this->render());

	}
	public function addDemoPayment()
	{
		$this->template = 'pos/demopayment.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	public function addCommonPage()
	{
		$this->template = 'pos/commonpage.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	
	public function addMiscellaneouseCharge()
	{
		$this->template = 'pos/miscellaneouse.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	public function getCustomer()
	{
		$this->language->load('setting/customers');

		//$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/customers');

		//$this->getList();

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'firstname';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_email'])) {
			$url.= '&filter_email='.$this->request->post['filter_email'];
		} 
		if (isset($this->request->post['filter_Type'])) {
			$url.= '&filter_Type='.$this->request->post['filter_Type'];
		} 
		
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
		} else {
			$filter_email ="";
		}
		if (isset($this->request->post['filter_Type'])) {
			$filter_Type = $this->request->post['filter_Type'];
		} else {
			$filter_Type ="";
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
		} else {
			$filter_date_to ="";
		}
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/customers/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_email' => $filter_email,
			'filter_Type' => $filter_Type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to
		);

		$customer_total = $this->model_setting_customers->getTotalCustomers();
		
		$results = $this->model_setting_customers->getCustomers($data);
	
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customer_id'=> $result['customer_id'],
				'firstname'  => $result['firstname'],
				'lastname'   => $result['lastname'],
				'email'		 => $result['primary_email'],
				'mobile'	 => $result['mobile'],
				'added_date' => $result['added_date'],
				'home'		 => $result['home'],	
				'work'		 => $result['work'],			
				'selected'   => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

			$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);		
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Customer List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['sort_name'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=firstname' . $url, 'SSL');
		$this->data['sort_lastname'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=lastname' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'pos/customer_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getCustomers()
	{
		$this->language->load('setting/customers');
		$this->data['show'] = true;
		if(isset($this->request->post['search_form'])){
			$this->data['show'] =false;
		}

		//$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/customers');

		//$this->getList();

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
				$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->post['page'])) {
			$page = $this->request->post['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		/*** Filter- Starts***/
		if (isset($this->request->post['filter_name'])) {
			$url.= '&filter_name='.$this->request->post['filter_name'];
		}
		if (isset($this->request->post['filter_email'])) {
			$url.= '&filter_email='.$this->request->post['filter_email'];
		} 
		if (isset($this->request->post['filter_Type'])) {
			$url.= '&filter_Type='.$this->request->post['filter_Type'];
		} 
		
		if (isset($this->request->post['filter_date_from'])) {
			$url.= '&filter_date_from='.$this->request->post['filter_date_from'];
		} 
		if (isset($this->request->post['filter_date_to'])) {
			$url.= '&filter_date_to='.$this->request->post['filter_date_to'];
		} 
		if (isset($this->request->post['filter_name'])) {
			$filter_name = $this->request->post['filter_name'];
		} else {
			$filter_name ="";
		}
		if (isset($this->request->post['filter_email'])) {
			$filter_email = $this->request->post['filter_email'];
		} else {
			$filter_email ="";
		}
		if (isset($this->request->post['filter_Type'])) {
			$filter_Type = $this->request->post['filter_Type'];
		} else {
			$filter_Type ="";
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from ="";
		}
		if (isset($this->request->post['searchkey'])) {
			$filter_date_to = $this->request->post['searchkey'];
		} else {
			$filter_date_to ="";
		}
		if (isset($this->request->post['searchkey'])) {
			$searchkey = $this->request->post['searchkey'];
		} else {
			$searchkey ="";
		}
		$this->data["filtertype"] = $filter_Type;
		/*** Filter-Ends***/
  		$this->data['insert'] = $this->url->link('setting/customers/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('setting/customers/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['action'] = $this->url->link('pos/pos/getCustomers', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['customers'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit'),
			'filter_name' => $filter_name,
			'filter_email' => $filter_email,
			'filter_Type' => $filter_Type,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'searchkey'     => $searchkey
		);

		$customer_total = $this->model_setting_customers->getTotalCustomers($data);
		
		$results = $this->model_setting_customers->getCustomers($data);
	
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $result['customer_id'] . $url, 'SSL')
			);

			$this->data['customers'][] = array(
				'customer_id'=> $result['customer_id'],
				'name'  => $result['name'],
				'email'		 => $result['primary_email'],
				'mobile'	 => $result['mobile'],
				'added_date' => $result['added_date'],
				'home'		 => $result['home'],	
				'work'		 => $result['work'],			
				'selected'   => isset($this->request->post['selected']) && in_array($result['customer_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results']	= $this->language->get('text_no_results');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
	
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}		
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);	
		

		$this->data['sort_name'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('setting/customers', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		
		$url = '';
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		$pagination = new Pagination();
		$pagination->total = $customer_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('pos/pos/getCustomers', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'pos/customers.tpl';
		
				
		$this->response->setOutput($this->render());
	}

	public function attachCustomer()
	{
	  $this->load->model('setting/customers');
	  $customer_id = $this->request->post['customer_id'];
	  $customersDetails = $this->model_setting_customers->getCustomer($customer_id);
	  if(count($customersDetails) > 0){
		  $this->session->data["attachedcustomer"] = $customer_id;
		 // print_r($this->session->data['attachedcustomer']); die();
		  $data = array("success"=> true,"c_id" => $customersDetails['customer_id'],
		  	            "Name"=> strtoupper($customersDetails['customer_title']." ".$customersDetails['name']) ,
		  	            "Editurl"=>$this->url->link('setting/customers/update', 'token=' . $this->session->data['token'] . '&customer_id=' . $customersDetails['customer_id'].'&type=pos', 'SSL'));
	  } else{
	  	 $data = array("success"=> false);
	  	 $this->session->data["attachedcustomer"] ="";
	  	 unset($this->session->data["attachedcustomer"]);
	  }

	  $this->response->setOutput(json_encode($data));
	}

}

?>
	