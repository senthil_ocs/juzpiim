<?php  
class ControllerPosCommon extends Controller { 

	private $error = array();

	public function index() {

		$this->document->setTitle($this->language->get('POS'));


		$this->data['count_sales'] = $this->cart->hasProducts();

		$this->data['continue_sale'] = $this->url->link('pos/pos','token=' .$this->request->get['token'], 'SSL');
		$this->data['new_sale'] = $this->url->link('pos/common/newsale','token=' .$this->request->get['token'], 'SSL');
		
		if($this->user->hasPermission('modify', 'pos/common/refund')) {
			$this->data['refund_sale'] = $this->url->link('pos/common/refund','token=' .$this->request->get['token'], 'SSL');
		}
			
		$this->template = 'pos/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	function newsale()
	{
		$this->load->model('pos/cart');

		$this->session->data['is_exchange_manually']	= false;

		$this->model_pos_cart->clear();

		$this->redirect($this->url->link('pos/pos','&token=' .$this->request->get['token'], 'SSL'));

	}

	function refund()
	{
		$this->session->data['is_exchange_manually']	= true;

		$this->redirect($this->url->link('pos/pos','&token=' .$this->request->get['token'], 'SSL'));
	}
}
?>