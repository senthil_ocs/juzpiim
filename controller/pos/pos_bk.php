<?php  
class ControllerPosPos extends Controller { 

	private $error = array();
	          
	public function index() 
	{ 
	   $this->language->load('pos/pos');

	   $this->load->model('pos/cart');
	   

	   $this->data['LastOrder']  = $this->model_pos_cart->getLastOrderProduct();
       $this->data['urlforhold'] = $this->url->link("functionkey/f6","&token=".$this->request->get['token'],'SSL');
       $this->data['urlforrelease'] = $this->url->link('functionkey/f7','&token=' . $this->request->get['token'], 'SSL');
       $this->data['reprint_order'] = $this->url->link('functionkey/print', '&token=' .$this->request->get['token'], 'SSL');
       $this->data['void_order'] = $this->url->link('pos/cart/cancelorder', '&token=' .$this->request->get['token'], 'SSL');

	   $this->data['logged'] = $this->user->getUserName();   


		$this->template = 'pos/pos.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	

}  
?>