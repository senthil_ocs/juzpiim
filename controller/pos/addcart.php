<?php
class ControllerPosAddcart extends Controller {
	public function index() {
		$this->language->load('pos/addcart');
		$this->load->model('pos/addcart');
		$this->load->model('pos/cart');
		$this->session->data['last_product']	= '';
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			//$this->request->post['code']	='00601';
			//printArray($this->request->post); //die;
			if(isset($this->request->post['code']) && $this->request->post['code']!="") {

				$_code	= explode('*',$this->request->post['code']);
				//printArray($_code); die;
				$inventoryCode = $_code[0];
				$addQty = 1;
				$_productDetails	= $this->model_pos_addcart->getProductInformation($_code[0]);
				//printArray($_productDetails); exit;
				if(!empty($_productDetails)) {
					$_productId	= $_productDetails['product_id'];
					$_quantity	= $_productDetails['quantity'];
					$remainQty	= $_quantity;
					$outOfStock	= false;
					$this->session->data['stock_out_action']	= false;
					if (!empty($this->request->post['setExchangeProduct'])) {
					    //$this->session->data['is_exchange'][] = $_productId;
						$is_exchange_item = true;
					} else {
						$is_exchange_item = false;
					}
					if(isset($this->session->data['is_exchange_manually']))	{
						$is_exchange_item = $this->session->data['is_exchange_manually'];
					}

					$_productQty	= 1;
					if(array_key_exists($_productId, $this->session->data['cart'])) {
						$cartQty	= 	$this->session->data['cart'][$_productId];
						$remainQty	= (int)$_quantity - (int)$cartQty;
						if($_quantity == $cartQty) {
							$outOfStock	= true;
						}
					}

					if(!empty($addQty)) {
						if((int)$addQty > $remainQty) {
							$outOfStock	= true;
						} else {
							$_productQty	= $addQty;
						}
					}
					if(empty($outOfStock) && $remainQty > 0 ) {
						$this->cart->add($_productId,$_productQty, '', '', $is_exchange_item);
					} else {
						$this->session->data['stock_out_action']	= true;
						//$this->cart->addDiscount('fixed','4');
					}

					$total_data	= $this->model_pos_cart->getTotalsCollection();

					if (!empty($total_data) && array_key_exists($_productId, $total_data)) {
						$this->session->data['last_action']	= 'Recently Updated';
					} else {
						$this->session->data['last_action']	= 'Recently Added';
					}
				 }
			   }


				$this->session->data['tender']['amount']	= $this->currency->format(0);
				$this->session->data['tender']['change']	= $this->currency->format(0);
				$this->session->data['last_product']	= $_productId;

				 //exit;

				$cartDetails	= $this->getCartDetails();

				$cartDetails    = $cartDetails;
				//printArray($cartDetails);
				$totals		= $this->model_pos_cart->getTotalsCollection();

				$cart       = array('products'=>$cartDetails,"total"=>$totals);
				$this->response->setOutput(json_encode($cart));


		}
  	}

	public function update() {
		$updateQty 		= $this->request->post['qty'];
		$product_key 	= $this->request->post['key'];
		//$updateQty 		= 15;
		//$product_key 		= 1;
		if(!empty($product_key)) {
			$this->session->data['stock_out_action']	= false;
			$this->load->model('pos/addcart');
			$_productDetails	= $this->model_pos_addcart->getProductDetails($product_key);
			if(!empty($_productDetails)) {
				$_productId	= $_productDetails['product_id'];
				$_quantity	= $_productDetails['quantity'];
			}
			$outOfStock	= false;
			if($_quantity >= $updateQty || ctype_digit($_quantity)) {
				$_productQty	= $updateQty;
			} else {
				$outOfStock	= true;
				$this->session->data['stock_out_action']	= true;
				//$_productQty	= $_quantity;
			}
			if((int)$updateQty && ((int)$updateQty > 0) && empty($outOfStock)) {
				$this->cart->update($product_key, $_productQty);
				$this->session->data['last_action']	= 'Recently Updated';
			}
			if((int)$updateQty <= 0) {
				$this->cart->remove($product_key);
				$this->session->data['last_action']	= 'Recently Deleted';
			}
		}
		$this->session->data['tender']['amount']	= $this->currency->format(0);
		$this->session->data['tender']['change']	= $this->currency->format(0);
		$this->session->data['last_product']	= $product_key;
		$this->response->setOutput('1');
	}

	public function tender() {
		$this->language->load('pos/addcart');
		$this->load->model('pos/addcart');
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$totals	= $this->session->data['totals'];
			$order_total	= '0.00';
			foreach($totals as $total) {
				if($total['code']=='total') {
					//$order_total	= str_replace('$','',$total['text']);
					$order_total	= $total['value'];
				}
			}
			if (isset($this->request->post['amount']) && $order_total !='0.00') {
				$amount	= (int)$this->request->post['amount'];
				if($amount >= $totals) {
					$this->session->data['tender']['amount']	= $this->currency->format($amount);
					$this->session->data['tender']['change']	= $this->currency->format($amount - $order_total);
				} elseif($amount=='exact') {
					$this->session->data['tender']['amount']	= $this->currency->format($order_total);
					$this->session->data['tender']['change']	= $this->currency->format(0);
				}

				$this->template = 'default/template/common/content_main.tpl';
				$this->children = array(
					'common/column_left',
					'common/column_right',
					'common/content_top',
					'common/content_bottom'
				);
				$this->response->setOutput($this->render());
			}
		}
	}

	public function getCartDetails() {
		$product_data = array();
		foreach ($this->cart->getProducts() as $product) {
			$product_data[] = array(
				'key' => $product['product_id'],
				'product_id' => $product['key'],
				'code' 		=> $product['sku'],
				'name'       => $product['name'],
				'quantity'   => $product['quantity'],
				'subtract'   => $product['subtract'],
				'price'      => $this->currency->format($product['price']),
				'sub_total' => $this->currency->format($product['sub_total']),
				'discount_amount' => $this->currency->format($product['discount_amount']),
				'total'      => $this->currency->format($product['net_amount']),
				'tax'        => $this->tax->getTax($product['price'], $product['tax_class_id']),
				'reward'     => $this->currency->format($product['reward']),
			);
		}
		return $product_data;
	}
}
?>