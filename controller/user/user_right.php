<?php
class ControllerUserUserRight extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		$this->getList();
	}

	public function insert() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('new_title'));
		$this->data['heading_title'] = $this->language->get('new_title');

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			if(!empty($this->request->post["permission"]['access'])){
            $accessstr =  implode(',', array_map(function($str) {  return sprintf("'%s'", $str);
	}, $this->request->post["permission"]['access']));
            $Controlleraccess = $this->model_user_user_group->getControllerbysublinks($accessstr);
            for($i=0; $i<count($Controlleraccess);$i++){
            	$objArray["permission"]["access"][] = $Controlleraccess[$i]["controller"];
            }
        }

        if(!empty($this->request->post["permission"]['modify'])){
            $modifystr =  implode(',', array_map(function($str) {  return sprintf("'%s'", $str);
	}, $this->request->post["permission"]['modify']));
            $Controllermodifystr = $this->model_user_user_group->getControllerbysublinks($modifystr);
            for($i=0; $i<count($Controllermodifystr);$i++){
            	$objArray["permission"]["modify"][] = $Controllermodifystr[$i]["controller"];
            }
           }
            $objArray["name"]       = $this->request->post["name"];
            $objArray["max_discount"]       = $this->request->post["max_discount"];
            $objArray["pers"]       = $this->request->post['permission'];
            $objArray["accesstype"] = $this->request->post['accesstype'];
			$this->model_user_user_group->addUserGroupwithrights($objArray);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('edit_title'));
		$this->data['heading_title'] = $this->language->get('edit_title');

		$this->load->model('user/user_group');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
        if(!empty($this->request->post["permission"]['access'])){
			$accessstr =  implode(',', array_map(function($str) {  return sprintf("'%s'", $str);
			}, $this->request->post["permission"]['access']));
			$Controlleraccess = $this->model_user_user_group->getControllerbysublinks($accessstr);
			for($i=0; $i<count($Controlleraccess);$i++){
				$objArray["permission"]["access"][] = $Controlleraccess[$i]["controller"];
			}
		}
    
            if(!empty($this->request->post["permission"]['modify'])){
				$modifystr =  implode(',', array_map(function($str) {  return sprintf("'%s'", $str);
				}, $this->request->post["permission"]['modify']));
				$Controllermodifystr = $this->model_user_user_group->getControllerbysublinks($modifystr);
				for($i=0; $i<count($Controllermodifystr);$i++){
					$objArray["permission"]["modify"][] = $Controllermodifystr[$i]["controller"];
				}
			} 

			$objArray["name"] = $this->request->post["name"];
			/*$objArray["max_discount"]       = $this->request->post["max_discount"];*/
			$objArray["pers"]= $this->request->post['permission'];
			 $objArray["accesstype"] = $this->request->post['accesstype'];
			$this->model_user_user_group->editUserGroupwithrights($this->request->get['user_group_id'], $objArray);

			// $this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() { 
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('user/user_group');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $user_group_id) {
				$this->model_user_user_group->deleteUserGroup($user_group_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}	

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['insert'] = $this->url->link('user/user_right/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('user/user_right/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');	

		$this->data['user_groups'] = array();

		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$user_group_total = $this->model_user_user_group->getTotalUserGroups();

		$results = $this->model_user_user_group->getUserGroups($data);
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('user/user_right/update', 'token=' . $this->session->data['token'] . '&user_group_id=' . $result['user_group_id'] . $url, 'SSL')
			);		

			$this->data['user_groups'][] = array(
				'user_group_id' => $result['user_group_id'],
				'name'          => $result['name'],
				'max_discount'	=> $result['max_discount'],
				'selected'      => isset($this->request->post['selected']) && in_array($result['user_group_id'], $this->request->post['selected']),
				'action'        => $action,
				'reportaccess'  => $this->url->link('user/user_right/reportupdate', 'token=' . $this->session->data['token'] . '&user_group_id=' . $result['user_group_id'] . $url, 'SSL')
			);
		}	

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $user_group_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();				

		$this->data['sort'] = $sort; 
		$this->data['order'] = $order;

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['user_group'] = $this->url->link("user/user_right", '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'user/user_group_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		//$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_modify'] = $this->language->get('entry_modify');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['max_discount'])) {
			$this->data['error_max_discount'] = $this->error['max_discount'];
		} else {
			$this->data['error_max_discount'] = '';
		}
		if (isset($this->error['accesstype'])) {
			$this->data['error_warning'] = $this->error['accesstype'];
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['user_group_id'])) {
			$this->data['action'] = $this->url->link('user/user_right/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('user/user_right/update', 'token=' . $this->session->data['token'] . '&user_group_id=' . $this->request->get['user_group_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
		}
	

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($user_group_info)) {
			$this->data['name'] = $user_group_info['name'];
		} else {
			$this->data['name'] = '';
		}
		if (isset($this->request->post['max_discount'])) {
			$this->data['max_discount'] = $this->request->post['max_discount'];
		} elseif (!empty($user_group_info)) {
			$this->data['max_discount'] = $user_group_info['max_discount'];
		} else {
			$this->data['max_discount'] = '';
		}
		$ignore = array(
			'common/home',
			'common/startup',
			'common/login',
			'common/logout',
			'common/forgotten',
			'common/reset',			
			'error/not_found',
			'error/permission',
			'common/footer',
			'common/header',
			'common/sidebar'
		);
		// admin access 
        $rightsData = $this->model_user_user_group->getRights('2');
        foreach ($rightsData as $key => $value) {
        	     $permission[$value["mainlinks"]][] = $value["sublinks"];
        }
        //printArray($this->data['permissions']);

		$this->data['permissions'] =$permission;

		// POS access 
        $rightsData = $this->model_user_user_group->getRights('1');
        foreach ($rightsData as $key => $value) {
        	     $permissionpos[$value["mainlinks"]][] = $value["sublinks"];
        }
        //printArray($this->data['permissions']);
        //printArray($this->data['pos_permissions']);

		$this->data['pos_permissions'] =$permissionpos;


		
  
		if (isset($this->request->post['permission']['access'])) {
			$this->data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$this->data['access'] = $user_group_info['common_permissions']['access'];
		} else { 
			$this->data['access'] = array();
		}
		
		if (isset($this->request->post['permission']['modify'])) {
			$this->data['modify'] = $this->request->post['permission']['modify'];
		} elseif (isset($user_group_info['permission']['modify'])) {
			$this->data['modify'] = $user_group_info['common_permissions']['modify'];
		} else { 
			$this->data['modify'] = array();
		}
		if (isset($this->request->post['accesstype'])) {
			$this->data['accesstype'] = $this->request->post['accesstype'];
		} elseif (isset($user_group_info['accesstype'])) {
			$this->data['accesstype'] = $user_group_info['accesstype'];
		} else { 
			$this->data['accesstype'] = array();
		}

		if (isset($this->request->post['permission']['access_pos'])) {
			$this->data['access_pos'] = $this->request->post['permission']['access_pos'];
		} elseif (isset($user_group_info['permission']['access'])) {
			$this->data['access'] = $user_group_info['common_permissions']['access'];
		} else { 
			$this->data['access'] = array();
		}

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['user_group'] = $this->url->link("user/user_right", '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'user/user_right';

		$this->template = 'user/user_right_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'user/user_right')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$user_group = $this->model_user_user_group->getUserGroupName($this->request->post['name'],$this->request->get['user_group_id']); 
		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
			$this->error['name'] = $this->language->get('error_name');
		} elseif($this->request->post['name'] != '' && count($user_group) > 0){
			$this->error['warning'] = $this->language->get('GroupName already in use!.');
		}

		if (empty($this->request->post['accesstype']) || empty($this->request->post['permission']) ) {
			$this->error['accesstype'] = "Access Type and one of Permissions are mandatory ";
		}

		/*if (empty($this->request->post['max_discount'])) {
			$this->error['max_discount'] = "Max Discount is mandatory ";
		}*/

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'user/user_right')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('user/user');

		foreach ($this->request->post['selected'] as $user_group_id) {
			$user_total = $this->model_user_user->getTotalUsersByGroupId($user_group_id);

			if ($user_total) {
				$this->error['warning'] = sprintf($this->language->get('error_user'), $user_total);
			}
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	public function reportupdate() {
		//$this->data['heading_title'] = $this->language->get('heading_title');
		$this->language->load('user/user_group');

		$this->document->setTitle($this->language->get('edit_title'));
		$this->data['heading_title'] = $this->language->get('edit_title');

		$this->load->model('user/user_group');

		$this->data['text_select_all'] = $this->language->get('text_select_all');
		$this->data['text_unselect_all'] = $this->language->get('text_unselect_all');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_access'] = $this->language->get('entry_access');
		$this->data['entry_modify'] = $this->language->get('entry_modify');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if(!empty($this->request->post["permission"]['access'])){
				$str = implode(",",$this->request->post["permission"]['access']);
				$this->model_user_user_group->updateusergroupReportsAccess($str,$this->request->get['user_group_id']);
				//$this->session->data['success'] = 'updated success';
			}
			if(!empty($this->request->post["permission"]['access_pos'])){
				$strpos = implode(",",$this->request->post["permission"]['access_pos']);
				$this->model_user_user_group->updateusergroupReportsAccessPOS($strpos,$this->request->get['user_group_id']);
				//$this->session->data['success'] = 'updated success';
			}
			 $this->data['success'] = $this->language->get('text_success');       
        }

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>',
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => "Setting",
			'href'      => $this->url->link('setting/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['user_group_id'])) {
			$this->data['action'] = $this->url->link('user/user_right/reportupdate', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('user/user_right/reportupdate', 'token=' . $this->session->data['token'] . '&user_group_id=' . $this->request->get['user_group_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('user/user_right', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['user_group_id']) && $this->request->server['REQUEST_METHOD'] != 'POST') {
			$user_group_info = $this->model_user_user_group->getUserGroup($this->request->get['user_group_id']);
		}
		// printArray($user_group_info); die;
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($user_group_info)) {
			$this->data['name'] = $user_group_info['name'];
		} else {
			$this->data['name'] = '';
		}
		
		$ignore = array(
			'common/home',
			'common/startup',
			'common/login',
			'common/logout',
			'common/forgotten',
			'common/reset',			
			'error/not_found',
			'error/permission',
			'common/footer',
			'common/header',
			'common/sidebar'
		);
		

		// POS access 
        $rightsData = $this->model_user_user_group->getGroupAccessRights('reports');
        foreach ($rightsData as $key => $value) {
        	     $permissionpos['reports'][] = $value;
        }
		$this->data['reports_permissions'] = $permissionpos;



		$rightsposData = $this->model_user_user_group->getGroupAccessRights('pos_access');
        foreach ($rightsposData as $key => $value) {
        	     $permissionpossection['reportspos'][] = $value;
        }
       // printArray($permissionpossection);
		$this->data['reports_permissions'] = $permissionpos;
		$this->data['reports_permissions_pos'] = $permissionpossection;
		
		$existingPermissions = $this->model_user_user_group->getusergroupReportsAccess($this->request->get['user_group_id'],'access_reports');
		$existingPermissions_pos = $this->model_user_user_group->getusergroupReportsAccess($this->request->get['user_group_id'],'access_reports_pos');

		if (isset($this->request->post['permission']['access'])) {
			$this->data['access'] = $this->request->post['permission']['access'];
		} elseif (isset($existingPermissions)) {
			$this->data['access']= explode(",", $existingPermissions['access_reports']);
	       
        } else { 
			$this->data['access'] = array();
		}

		if (isset($this->request->post['permission']['access_pos'])) {
			$this->data['access_pos'] = $this->request->post['permission']['access_pos'];
		} elseif (isset($existingPermissions_pos)) {
			$this->data['access_pos']= explode(",", $existingPermissions_pos['access_reports_pos']);	       
        } else { 
			$this->data['access_pos'] = array();
		}

	
		
		if (isset($this->request->post['permission']['modify'])) {
			$this->data['modify'] = $this->request->post['permission']['modify'];
		} elseif (isset($user_group_info['permission']['modify'])) {
			$this->data['modify'] = $user_group_info['common_permissions']['modify'];
		} else { 
			$this->data['modify'] = array();
		}
		if (isset($this->request->post['accesstype'])) {
			$this->data['accesstype'] = $this->request->post['accesstype'];
		} elseif (isset($user_group_info['accesstype'])) {
			$this->data['accesstype'] = $user_group_info['accesstype'];
		} else { 
			$this->data['accesstype'] = array();
		}

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['settings'] = $this->url->link('setting/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['user_group'] = $this->url->link("user/user_right", '&token=' . $this->session->data['token'], 'SSL');

		$this->data['route'] = 'user/user_right';

		$this->template = 'user/user_report_right_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
		
	}

	


}
?>