<?php  
class ControllerInventoryCategory extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->language->load('inventory/category');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/category');
		$this->load->model('inventory/department');
		
		$this->getList();
		
  	}
	
	public function insert() {

		$this->language->load('inventory/category');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/category');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// printArray($this->request->post);die;
			$this->model_inventory_category->addCategory($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('inventory/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function update() {
	
		$this->language->load('inventory/category');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/category');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// printArray($this->request);die;
			$this->model_inventory_category->editcategory($this->request->get['category_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('inventory/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	
	public function delete() {
    	$this->language->load('inventory/category');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/category');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $category_id) {
				$this->model_inventory_category->deleteCategory($category_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('inventory/category', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {
		

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}
  		$this->data['insert'] = $this->url->link('inventory/category/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/category/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['categorys'] = array();

		$data = array(
			'filter_name'	    => $filter_name,
			'filter_department' => $filter_department,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$this->data['filter_name']     = $filter_name;
		$this->data['filter_department'] = $filter_department;
		
		$category_total = $this->model_inventory_category->getTotalCategorys($data);
		
		$results = $this->model_inventory_category->getCategorys($data);
			
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		foreach ($results as $result) {
			$action = array();
			$status = 'inActive';
			if($result['status']=='1'){
				$status = 'Active';
			}
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $result['category_id'] . $url, 'SSL')
			);

			$department_name = $this->model_inventory_department->getDepartmentName($result['department_code']);
			$this->data['categorys'][] = array(
				'category_id' 	=> $result['category_id'],
				'name'      	 	=> $result['category_name'],
				'code'      	 	=> $result['category_code'],
				'department_code'   => $result['department_code'],
				'department_name'   => $department_name['department_name'],
				'remarks'      		=> $result['remarks'],
				'status'            => $status,
				'selected'   => isset($this->request->post['selected']) && in_array($result['category_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_remarks'] = $this->language->get('column_remarks');
		$this->data['column_department'] = $this->language->get('column_department');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/category', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['token'] =  $this->session->data['token'];
		$this->data['sort_name'] = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_remarks'] = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $category_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		//$this->data['route'] = $this->request->get['route'];
		$this->data['route'] = 'inventory/category';

		$this->template = 'inventory/category_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/category', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		/*$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/category/insert', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);*/

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

		if (isset($this->error['department_code'])) {
			$this->data['error_department'] = $this->error['department_code'];
		} else {
			$this->data['error_department'] = '';
		}

		if (isset($this->error['remarks'])) {
			$this->data['error_remarks'] = $this->error['remarks'];
		} else {
			$this->data['error_remarks'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['category_id'])) { 
			$this->data['action'] = $this->url->link('inventory/category/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('inventory/category/update', 'token=' . $this->session->data['token'] . '&category_id=' . $this->request->get['category_id'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->load->model('inventory/department');
		$this->data['departmentCollection']	= $this->model_inventory_department->getDepartments();

		if (isset($this->request->get['category_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$category_info = $this->model_inventory_category->getCategory($this->request->get['category_id']);
		}
		if (isset($this->request->post['department_code'])) {
			$this->load->model('inventory/department');
			$department_info = $this->model_inventory_department->getDepartment($this->request->post['department_code']);
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($category_info)) {
			$this->data['name'] = trim($category_info['category_name']);
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($category_info)) {
			$this->data['code'] = trim($category_info['category_code']);
		} else {
			$this->data['code'] = '';
		}

		if (isset($this->request->post['department_code'])) {
			$this->data['department'] = $this->request->post['department_code'];
		} elseif (!empty($category_info)) {
			$this->data['department'] = $category_info['department_code'];
		} else {
			$this->data['department'] = '';
		}

		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($category_info)) {
			$this->data['remarks'] = $category_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		if (isset($this->request->post['status'])) {
			$this->data['status'] = (string)$this->request->post['status'];
		} elseif (!empty($category_info)) {
			$this->data['status'] = (string) $category_info['status'];
		} else {
			$this->data['status'] = '';
		}
		// printArray($this->data);die;

		$this->data['route'] = 'inventory/category';
		$this->template = 'inventory/category_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		// printArray($this->request);die;
		if (!$this->user->hasPermission('modify', 'inventory/category')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (empty($this->request->post['code'])) {
			$this->error['code'] = $this->language->get('error_code');
		}
		if (empty($this->request->post['department_code'])) {
			$this->error['department_code'] = $this->language->get('error_department');
		}
		if($this->request->get['route']  != 'inventory/category/update'){
			$exist = $this->model_inventory_category->getCategoryByCode($this->request->post['code'],$this->request->get['category_id']);
			if($exist>=1){
				$this->error['warning'] = 'Given Category code already Exist';
			}		
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'inventory/category')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
		
	  	$this->load->model('inventory/inventory');
		$this->load->model('inventory/brand');
		
		foreach ($this->request->post['selected'] as $category_id) {
			$product_total = $this->model_inventory_inventory->getTotalProductsByCategoryId($category_id);
			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
		}
		
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('inventory/category');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_inventory_category->getCategorys($data);

			foreach ($results as $result) {
				$json[] = array(
					'category_id' => $result['category_id'], 
					'name'        => strip_tags(html_entity_decode($result['category_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}
	public function subCateList(){

		$this->language->load('inventory/category');
		$this->document->setTitle($this->language->get('subCate_title'));
		$this->load->model('inventory/category');
		$this->load->model('inventory/department');

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}

  		$this->data['insert'] = $this->url->link('inventory/category/subCate_insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/category/subCate_delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['categorys'] = array();

		$data = array(
			'filter_name'=> $filter_name,
			'filter_category'=> $filter_category,
			'filter_department'=> $filter_department,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		$this->data['filter_name']     = $filter_name;
		$this->data['filter_category'] = $filter_category;
		$this->data['filter_department'] = $filter_department;

		$this->data['categoryCollection']	= $this->model_inventory_category->getAllCategorys();
		$total_count = $this->model_inventory_category->getAllSubCategorysTotal($data);
		// echo $total_count; die;
		$results = $this->model_inventory_category->getAllSubCategorys($data);

		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		foreach ($results as $result) {
			$action = array();
			$status = 'InActive';
			if($result['status']=='1'){
				$status = 'Active';
			}
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/category/subCate_update', 'token=' . $this->session->data['token'] . '&id=' . $result['id'] . $url, 'SSL')
			);
			$this->data['subCategorys'][] = array(
				'category_id' 	=> $result['category_id'],
				'id' 			=> $result['id'],
				'category_name' => $result['category_name'],
				'name'      	=> $result['name'],
				'subCate_code' 	=> $result['subCate_code'],
				'department_name'=> $result['department_name'],
				'status'        => $status,
				'action'     	=> $action
			);
		}
		// printArray($this->data['subCategorys']); die;

		$this->data['heading_title'] = $this->language->get('subCate_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_remarks'] = $this->language->get('column_remarks');
		$this->data['column_department'] = $this->language->get('column_department');
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			// printArray($this->session->data['success']); die;
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
			
		} else if(isset($this->session->data['success_text'])) {
			$this->data['success'] = $this->session->data['success_text'];
			unset($this->session->data['success_text']);
		}else{
			$this->data['success'] = '';
		}	

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sub Category',
			'href'      => $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['token'] =  $this->session->data['token'];
		$this->data['sort_name'] = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_remarks'] = $this->url->link('inventory/category', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination         = new Pagination();
		$pagination->total  = $total_count;
		$pagination->page   = $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text   = $this->language->get('text_pagination');
		$pagination->url    = $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort']  = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = 'inventory/category';

		$this->template = 'inventory/subcate_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	public function subCate_insert(){

		$this->language->load('inventory/category');
		$this->document->setTitle($this->language->get('subCate_title'));
		$this->load->model('inventory/category');
		$this->load->model('inventory/department');
	
		$this->data['heading_title'] = $this->language->get('subCate_title');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('subCate_title'),
			'href'      => $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		/*$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/category/insert', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);*/

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

		if (isset($this->error['remarks'])) {
			$this->data['error_remarks'] = $this->error['remarks'];
		} else {
			$this->data['error_remarks'] = '';
		}
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if($this->request->get['id']!=''){
			$this->data['action'] = $this->url->link('inventory/category/subCate_update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}else{
			$this->data['action'] = $this->url->link('inventory/category/subCate_insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		$this->data['cancel'] = $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['categoryCollection']	= $this->model_inventory_category->getAllCategorys();
		
		$this->data['insert_type'] = 'Insert';
		if (isset($this->request->get['id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			
			$this->data['insert_type'] = 'Update';
			$this->data['subCategory_info'] = $this->model_inventory_category->getSubCate($this->request->get['id']);
			// $this->request->post = $this->data['subCategory_info'];
			$this->data['id'] = $this->request->get['id'];
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['category_id'])) {
			$this->data['category_id'] = $this->request->post['category_id'];
		} else {
			$this->data['category_id'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} else {
			$this->data['status'] = '';
		}

		if (isset($this->request->post['subCate_code'])) {
			$this->data['subCate_code'] = $this->request->post['subCate_code'];
		} else {
			$this->data['subCate_code'] = '';
		}

		$this->data['created_by']  = $this->session->data['user_id'];
		$this->data['modified_by'] = $this->session->data['user_id'];

		if($this->data['category_id']!='' && $this->data['name']!='' && $this->data['status']!=''){
			if($this->data['insert_type']=='Insert'){
				$this->model_inventory_category->addSubCategory($this->data);
				$this->sesssion->data['success'] = "Sub Category Created Successfully";
			}
			$this->redirect($this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->data['route'] = 'inventory/category';
		$this->template = 'inventory/subcate_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);				
		$this->response->setOutput($this->render());
	}
	public function subCate_delete(){
		$this->load->model('inventory/category');
		if(count($this->request->post['selected'])>0){
			foreach ($this->request->post['selected'] as $value) {
				$this->model_inventory_category->subCate_delete($value);
			}
		}
		$this->sesssion->data['success_text'] = "Sub Category Deleted Successfully";
			$this->redirect($this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url, 'SSL'));

	}
	public function subCate_update(){

		$this->language->load('inventory/category');
		$this->document->setTitle($this->language->get('subCate_title'));
		$this->load->model('inventory/category');
		$this->load->model('inventory/department');
	
		$this->data['heading_title'] = $this->language->get('subCate_title');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['category_id'] 	= '';
		$this->data['name'] 		= '';
		$this->data['status'] 		= '';
		$this->data['id'] 			= '';
		$this->data['subCate_code'] = '';
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('subCate_title'),
			'href'      => $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if($this->request->get['id'] !=''){
			$subCategory_info = $this->model_inventory_category->getSubCate($this->request->get['id']);
			$this->request->post = $subCategory_info;
		}
		$this->data['categoryCollection']	= $this->model_inventory_category->getAllCategorys();

		if(isset($this->request->post['category_id']) && $this->request->post['category_id']!=''){
			$this->data['category_id'] = $this->request->post['category_id'];
		}
		if(isset($this->request->post['name']) && $this->request->post['name']!=''){
			$this->data['name'] = $this->request->post['name'];
		}
		if(isset($this->request->post['status']) && $this->request->post['status']!=''){
			$this->data['status'] = $this->request->post['status'];
		}
		if(isset($this->request->post['id']) && $this->request->post['id']!=''){
			$this->data['id'] = $this->request->post['id'];
		}
		if(isset($this->request->post['subCate_code']) && $this->request->post['subCate_code']!=''){
			$this->data['subCate_code'] = $this->request->post['subCate_code'];
		}

		$this->data['action'] = $this->url->link('inventory/category/subCate_update', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['cancel'] = $this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if($this->request->server['REQUEST_METHOD'] == 'POST') {	

			$this->model_inventory_category->updateSubCategory($this->data);
			$this->redirect($this->url->link('inventory/category/subCateList', 'token=' . $this->session->data['token'] . $url, 'SSL'));

		}
		
		$this->data['route'] = 'inventory/category';
		$this->template = 'inventory/subcate_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);				
		$this->response->setOutput($this->render());
	}
	public function ajaxGetCategory(){
		$this->load->model('inventory/category');
		if($this->request->post['department_code']!=''){
			$category = $this->model_inventory_category->ajaxGetCategory($this->request->post['department_code']);	
		}
		$category_select = '';
		if($this->request->post['category']!=''){
			$category_select = $this->request->post['category'];
		}
		$str = "<option value=''>Select Category</option>";
		if(count($category)> 0){
			foreach ($category as $value) {
				$select  = '';
				if($value['category_code']==$category_select){
					$select = 'Selected';
				}
				$str .="<option value='".$value['category_code']."' ".$select.">".$value['category_name']."</option>"; 
			}
		}
		echo $str;
	}
	public function ajaxGetSubcategory(){
		$this->load->model('inventory/category');
		if($this->request->post['cate']!=''){
			$subcategory = $this->model_inventory_category->ajaxGetSubcategory($this->request->post['cate']);	
		}
		$subCate = '';
		if($this->request->post['subcate']!=''){
			$subCate = $this->request->post['subcate'];
		}

		$str = "<option value=''>Select Sub Category</option>";
		if(count($subcategory)> 0){
			foreach ($subcategory as $value) {
				$select  = '';
				if($value['subCate_code']==$subCate){
					$select = 'Selected';
				}
				$str .="<option value='".$value['subCate_code']."' ".$select.">".$value['name']."</option>"; 
			}
		}
		echo $str;
	}
}  
?>