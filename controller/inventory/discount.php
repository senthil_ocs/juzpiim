<?php 
class ControllerInventoryDiscount extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('inventory/discount');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/discount');
		$this->getList();
	}

	
	public function insert() {
		$this->language->load('inventory/discount');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/discount');
		/*printArray($this->request->post);die;*/
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='insert')) {
			$this->model_inventory_discount->addDiscount($this->request->post);
			$this->session->data['success'] = "Pair Discount updated successfully";
			$url = '';
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		
			$this->redirect($this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}
	public function update() {
		$this->language->load('inventory/discount');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/discount');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='update')) {
			
			$this->model_inventory_discount->editDiscountHeader($this->request->get['discount_code'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getForm();
	}
 
	public function delete() {
		$this->language->load('inventory/discount');
 		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/discount');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $discount_id) {
				$this->model_inventory_discount->deleteDiscount($discount_id);
			}
			
			$this->session->data['success'] = 'Deleted successfully';
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			//$this->redirect($this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getList();
	}
	protected function getList() {

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'discount_code';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		if (isset($this->request->post['filter_date_to'])) {
			$filter_date_to = $this->request->post['filter_date_to'];
			$this->data['filter_date_to'] =  $this->request->post['filter_date_to'];
		} else {
			$filter_date_to = null;
			$this->data['filter_date_to'] = '';
		}
		if (isset($this->request->post['filter_date_from'])) {
			$filter_date_from = $this->request->post['filter_date_from'];
			$this->data['filter_date_from'] = $this->request->post['filter_date_from'];
		} else {
			$filter_date_from = null;
			$this->data['filter_date_from'] = '';
		}
		if ($_REQUEST['filter_location']) {
			 $filter_location = $_REQUEST['filter_location']; 
			 $pageUrl.='&filter_location='.$filter_location;
		}else{
			 $filter_location = $this->session->data['location_code']; 
			 $pageUrl.='&filter_location='.$filter_location;	
		} 
		if ($_REQUEST['filter_sku']) {
			 $filter_sku = $_REQUEST['filter_sku']; 
			 $pageUrl.='&filter_sku='.$filter_sku;
		}else{
			 $filter_sku = ''; 
			 $pageUrl.='&filter_sku='.$filter_sku;	
		} 


		$this->data['filter_location'] = $filter_location;
		$this->data['filter_sku'] = $filter_sku;
		

		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Pair Discount List',
			'href'      => $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
  		$this->data['insert'] = $this->url->link('inventory/discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/discount/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		 
		$this->data['uoms'] = array();
		$data = array(
			'sort'  => $sort,
			'order' => $order,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'filter_location'	=> $filter_location,
			'filter_sku'	=> $filter_sku,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);	
		$discount_total = $this->model_inventory_discount->getTotalDiscounts($data);
		$results = $this->model_inventory_discount->getDiscounts($data);
		// printArray($results); die;
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => 'View / Edit',
				'delete' => $this->url->link('inventory/discount/discountDelete', 'token=' . $this->session->data['token'] . '&discount_code=' . $result['pair_discount_code'] . $url, 'SSL'),
				'href' => $this->url->link('inventory/discount/update', 'token=' . $this->session->data['token'] . '&discount_code=' . $result['pair_discount_code'])
			);
			$this->data['discounts'][] = array(
				'pair_discount_code' 	 	 => $result['pair_discount_code'],
				'pair_discount_description'  => $result['pair_discount_description'],
				'pair_discount_qty'  		 => $result['pair_discount_qty'],
				'pair_discount_price'    	 => $result['pair_discount_price'],
				'pair_discount_from'    	 => $result['pair_discount_from'],
				'pair_discount_to'    		 => $result['pair_discount_to'],
				'createdon'    		 		 => $result['createdon'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['discount_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_discount_type'] = $this->language->get('column_discount_type');
		$this->data['column_amount'] = $this->language->get('column_amount');
		$this->data['column_action'] = $this->language->get('column_action');	
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		$url = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.='&filter_date_to='.$filter_date_to; 
		} else {
			$filter_date_to = null;
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_discount_name'] = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . '&sort=discount_name' . $url, 'SSL');
		$this->data['sort_discount_code'] = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . '&sort=discount_code' . $url, 'SSL');
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $discount_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['route'] = 'inventory/discount';
		$this->template = 'inventory/discount_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	public function addproducts_discount(){
		$this->language->load('inventory/discount');
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/discount');
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			$this->model_inventory_discount->addProductDiscount($this->request->post);
			
		}
	}
	protected function getForm() {
		$this->load->model('setting/location');
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_amount1'] = $this->language->get('entry_amount1');
		$this->data['entry_amount2'] = $this->language->get('entry_amount2');
		$this->data['entry_type'] = $this->language->get('entry_type');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}		
 		if (isset($this->error['discount_code'])) {
			$this->data['error_discount_code'] = $this->error['discount_code'];
		} else {
			$this->data['error_discount_code'] = '';
		}		
		if (isset($this->error['discount_quantity'])) {
			$this->data['error_discount_quantity'] = $this->error['discount_quantity'];
		} else {
			$this->data['error_discount_quantity'] = '';
		}	
		if (isset($this->error['discount_price'])) {
			$this->data['error_discount_price'] = $this->error['discount_price'];
		} else {
			$this->data['error_discount_price'] = '';
		}
		$this->data['locations'] = $this->model_setting_location->getLocationsCode($this->session->data['location_code']);
		$this->data['user_location_code']  = $this->session->data['location_code'];
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
				$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Discount List',
			'href'      => $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
  		if (!isset($this->request->get['discount_code'])) { 
			$this->data['action'] = $this->url->link('inventory/discount/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('inventory/discount/update', 'token=' . $this->session->data['token'] . '&discount_code=' . $this->request->get['discount_code'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('inventory/discount', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['discount_code']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$discount_info = $this->model_inventory_discount->getDiscount($this->request->get['discount_code']);
		}
		
		if (isset($this->request->post['discount_code'])) {
			$this->data['discount_code'] = $this->request->post['discount_code'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_code'] = $discount_info['pair_discount_code'];
		} else {
			$this->data['discount_code'] = '';
		}

		if(isset($this->request->post['mixed'])) {
			$this->data['mixeditem'] = $this->request->post['mixed'];
		} elseif (!empty($discount_info)) {
			$this->data['mixeditem'] = $discount_info['mixeditem'];
		} else {
			$this->data['mixeditem'] = '';
		}	

		if (isset($this->request->post['discount_description'])) {
			$this->data['discount_description'] = $this->request->post['discount_description'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_description'] = $discount_info['pair_discount_description'];
		} else {
			$this->data['discount_description'] = '';
		}	

		if (isset($this->request->post['discount_quantity'])) {
			$this->data['discount_quantity'] = $this->request->post['discount_quantity'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_quantity'] = $discount_info['pair_discount_qty'];
		} else {
			$this->data['discount_quantity'] = '';
		}	
		if (isset($this->request->post['discount_price'])) {
			$this->data['discount_price'] = $this->request->post['discount_price'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_price'] = $discount_info['pair_discount_price'];
		} else {
			$this->data['discount_price'] = '';
		}
		if (isset($this->request->post['discount_fromdate'])) {
			$this->data['discount_fromdate'] = $this->request->post['discount_fromdate'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_fromdate'] = $discount_info['pair_discount_from'];
		} else {
			$this->data['discount_fromdate'] = '';
		}
		if (isset($this->request->post['discount_todate'])) {
			$this->data['discount_todate'] = $this->request->post['discount_todate'];
		} elseif (!empty($discount_info)) {
			$this->data['discount_todate'] = $discount_info['pair_discount_to'];
		} else {
			$this->data['discount_todate'] = '';
		}

		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
		} elseif (!empty($discount_info)) {
			$this->data['product_specials'] =  $this->model_inventory_discount->getDiscountProducts($this->request->get['discount_code']);
		} else {
			$this->data['product_specials'] = '';
		}
		if (isset($this->request->post['location'])) {
			$this->data['location_code'] = $this->request->post['location'];
		} elseif (!empty($discount_info)) {
			$this->data['location_code'] =$this->model_inventory_discount->getDiscountLocation($this->request->get['discount_code']);
			$this->data['location_code'] =array_column($this->data['location_code'],'location_code');
		} else {
			$this->data['location_code'] = '';
		}

		$this->data['route'] = 'inventory/discount';
		$this->data['token'] = $this->session->data['token'];
		
		if(isset($this->request->get['discount_code'])){

			$this->template = 'inventory/discount_form_view.tpl';
		}else{
			$this->template = 'inventory/discount_form.tpl';
		}

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	protected function validateForm($type='') {
		if (!$this->user->hasPermission('modify', 'inventory/discount')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if(isset($this->request->post['discount_code']) && trim($this->request->post['discount_code'])==''){
			$this->error['discount_code'] = 'discount code should not empty';
		}	
		if(isset($this->request->post['discount_quantity']) && trim($this->request->post['discount_quantity'])==''){
			$this->error['discount_quantity'] = 'discount quantity should not empty';
		}
		if(isset($this->request->post['discount_price']) && trim($this->request->post['discount_price'])==''){
			$this->error['discount_price'] = 'discount price should not empty';
		}
		if(isset($this->request->post['discount_fromdate']) && trim($this->request->post['discount_fromdate'])==''){
			$this->error['discount_fromdate'] = 'discount from Date should not empty';
		}

		if(isset($this->request->post['discount_todate']) && trim($this->request->post['discount_todate'])==''){
			$this->error['discount_todate'] = 'discount to date should not empty';
		}
		if(!isset($this->request->post['product_special']) && trim($this->request->post['product_special'])==''){
			$this->error['warning'] = 'Please add product and save';
		}
		

		if($type!='update'){
			 $checkduplication =  $this->model_inventory_discount->checkCode(trim($this->request->post['discount_code']));
			 if(!empty($checkduplication)){
				 $this->error['warning'] = 'Discount code already Exist';
			   }
		}
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'master/uom')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
	  	  
		foreach ($this->request->post['selected'] as $user_id) {
			if ($this->user->getId() == $user_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}
		}
		 
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
  	public function autocomplete() {
		$this->load->model('inventory/inventory');
		
		/*if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
		}*/
		if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
			$data['filter_sku'] = $this->request->get['filter_name'];
			$data['filter_barcodes'] = $this->request->get['filter_name'];
		}
		if(strlen($data['filter_name'])>=4){
			//$fresults = $this->model_inventory_inventory->getProducts($data);
			$fresults = $this->model_inventory_inventory->getProductsTouchMenu($data,$type="touchmenu");
			/*printArray($fresults);die;*/
			$str ='<ul id="product-list" style="width:auto;">';
			$x=0;
			foreach ($fresults as $result) {
				/*printArray($result['sku_price']);die;*/
				$x++;
				$name = addslashes(trim($result['name']));
				$sku = trim($result['psku']);
				$str.= '<li class="autocomplete-suggestion" data-index="'.$x.'" tabindex ="'.$x.'" onClick="getValue(\'' .$name.'\',\'' .$sku.'\',\'' .$result['sku_price'].'\');">'.$name.'</li>';
			}
		}else{
			$str = '';
		}
		$str.='</ul>';
		echo $str;
	}
	public function autocompleteView() {
		$this->load->model('inventory/inventory');
		
		if (isset($this->request->get['discount_code'])) {
			$discount_code = $this->request->get['discount_code'];
		}
		if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
			$data['filter_sku'] = $this->request->get['filter_name'];
			$data['filter_barcodes'] = $this->request->get['filter_name'];
		}
		if(strlen($data['filter_name'])>=4){
			//$fresults = $this->model_inventory_inventory->getProducts($data);
			$fresults = $this->model_inventory_inventory->getProductsTouchMenu($data,$type="touchmenu");
			$str ='<ul id="product-list" style="width:auto;">';
			$x=0;
			foreach ($fresults as $result) {
				$x++;
				$name = addslashes(trim($result['name']));
				$sku = trim($result['psku']);
				$str.= '<li class="autocomplete-suggestion" data-index="'.$x.'" tabindex ="'.$x.'" onClick="getValue(\'' .$name.'\',\'' .$sku.'\',\'' .$discount_code.'\');">'.$name.'</li>';
			}
		}else{
			$str = '';
		}
		$str.='</ul>';
		echo $str;
	}
  	function getProductDetails(){
		$key = $this->request->get['key'];
		if (isset($key)) {
			$data['filter_name'] = $key;
			$data['filter_sku'] = $key;
			$data['filter_barcodes'] = $key;
		}
		$this->load->model('inventory/discount');
		$res = $this->model_inventory_discount->getProductsforSearch($data,$type="touchmenu");
		if(count($res)>=1){
	      foreach($res as $r){
		 	$newAry[]= trim($r['name']).'-'.trim($r['psku']);		 	
		  } 
		}else{
			$newAry[] = '0';
		}
		$this->response->setOutput(json_encode($newAry));
	}
	function deleteProducts(){
		$discount_code = $this->request->post['discount_code'];
		$psku 		   = $this->request->post['psku'];
		$this->load->model('inventory/discount');
		$this->model_inventory_discount->deleteProducts($discount_code,$psku);
		echo 1;
	}

	public function discountDelete()
	{
		$this->load->model('inventory/discount');
		if($this->request->get['discount_code']){
 			$deleteall =  $this->model_inventory_discount->deleteAllPairDiscount($this->request->get['discount_code']);
			if($deleteall){
				return true;
			}
		}
	}
}
?>