<?php 
class ControllerInventoryReports extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('inventory/reports');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/search');
		$this->load->model('inventory/inventory');
		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['action'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['stock_action'] = $this->url->link('inventory/reports/printStock', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');		
		$this->data['text_department'] = $this->language->get('text_department');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_brand'] = $this->language->get('text_brand');
		$this->data['text_barcode'] = $this->language->get('text_barcode');
		
		$this->data['text_invet_masters'] = $this->language->get('text_invet_masters');
		$this->data['text_stock_reports'] = $this->language->get('text_stock_reports');
		$this->data['text_barcode_list'] = $this->language->get('text_barcode_list');
		$this->data['text_stockreports'] = $this->language->get('text_stockreports');
		$this->data['text_stockreports_valuatioin'] = $this->language->get('text_stockreports_valuatioin');
		
		$this->data['button_print'] = $this->language->get('button_print');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->template = 'inventory/reports_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	public function printReports() {
		
		$this->language->load('inventory/reports');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$url = '';
		$this->data['back'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->load->model('inventory/reports');
		if (($this->request->server['REQUEST_METHOD'] == 'POST' ) || isset($this->request->get['master_name'])) {			
			$company_id	= $this->session->data['company_id'];
			$master_name = $this->request->get['master_name'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$companyInfo	= $this->url->getCompanyAddress($company_details);
			if($companyInfo) {
				$this->data['companyInfo']	= $companyInfo;
			} else {
				$this->data['companyInfo']	='';
			}
			
			if (strtolower($master_name) == 'department') {
				$this->load->model('inventory/department');
				$this->data['departmentList'] = $this->model_inventory_department->getDepartments();
			} elseif (strtolower($master_name) == 'category') {
				$this->data['categoryList'] = $this->model_inventory_reports->getReportCategory();
			} elseif (strtolower($master_name) == 'brand') {
				$this->load->model('inventory/brand');
				$brandData = $this->model_inventory_brand->getBrands();
				$this->data['brandList'] = array();
				foreach ($brandData as $brand) {
				    $category_name = array();
					$product_categories = $this->model_inventory_brand->getBrandCategory($brand['brand_id']);
					if(!empty($product_categories)) {
						foreach ($product_categories  as $product_category) {
							$categoryDetails = $this->model_inventory_brand->getCategoryDetailsById($product_category);
							if(!empty($categoryDetails)) {
								$category_name[]	= $categoryDetails['category_name'];
							}
						}
						$category_name	= implode(',<br>',$category_name);
					}
					if(empty($product_categories)) {
						$category_name	='';	
					}
					$this->data['brandList'][] = array(
						'brand_id' 	        => $brand['brand_id'],
						'name'      	 	=> $brand['brand_name'],
						'remarks'      	 	=> $brand['remarks'],
						'category'      	=> $category_name,
						'status'      		=> $brand['status']
					);
				}
			}
			
			$this->data['master_name'] = $master_name;
			$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		} else {
		    $this->redirect($this->url->link('inventory/reports', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'inventory/print.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function masterReport() {
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['back'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);

		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		if($this->session->data['location_code']){
			$location_code = $this->session->data['location_code'];
			$this->data['location'] = $this->cart->getLocation($this->session->data['location_code']);
		}else{
			$location_code = 'HQ';
		}

		$pageUrl ='';

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $location_code;
			$pageUrl.= '&filter_location='.$filter_location;
		}
		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}

		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_brand = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = '';
		}

		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}

		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}

		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}
		$url = '';		
		
		$page = $this->request->get['page'];

		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_department'	  => $filter_department, 
			'filter_location_code'	  => $filter_location, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_vendor'	      => $filter_vendor,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'location'=>$location_code,
			'filter_sku'=>$filter_sku,
			'filter_name'=>$filter_name
		);

		$this->data['last_page'] = '0';
		if(!$page){
			$page = 1;
		}
		$list_total_arys = $this->model_inventory_inventory->getTotalProductsReport($data);
		
		$list_total = $list_total_arys['total'];
		$results = $this->model_inventory_inventory->getProductsReport($data);
		foreach ($results as $result) {
			$taxAmount = 0;
			if($result['gst']==1){
				if($result['tax_method']==1){
					$taxAmount = $result['sku_cost'] * (7/100);
				}
			}
			$subCateName = $this->model_inventory_inventory->getSubCategoryByCode($result['sku_subcategory_code']);
			$this->data['products'][] = array(
				'sku'        			=> $result['sku'],
				'name'       			=> $result['name'],
				'pagecnt'       		=> ($page-1)*$this->config->get('config_admin_limit'), 
				'product_id' 			=> $result['product_id'],
				'selling_price'      	=> $result['sku_price'],
				'subCategoryName'		=> $subCateName,
				'sku_category_code'   	=> $result['sku_category_code'],
				'sku_department_code' 	=> $result['sku_department_code'],
				'department_name' 		=> $result['department_name'],
				'sku_brand_code'      	=> $result['brand_name'],
				'category_name'   		=> $result['category_name'],
				'vendor_name'   		=> $result['vendor_name'],
				'cost_value'  			=> $result['sku_cost'] + $taxAmount,
				'sku_price'        	  	=> $result['sku_price'],
				'barcodes'   			=> $barcodes,
				'createon'   			=> $result['createon'],
				'quantity'   			=> $result['createon'],
				'price'      			=> $result['sku_cost'],
				'status'     			=> ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
			);
		}

		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		// Category
		
		//$this->data['category_collection'] = $this->model_inventory_category->getCategorys();
		//if($this->request->get['filter_department'] != ''){
		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();
		//}
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();

		$this->data['stock_report'] = $stock_report;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token='.$this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report','token='.$this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);


		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		} 
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		} 
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		} 
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_supplier'])) {
			$url .= '&filter_supplier=' . $this->request->get['filter_supplier'];
		}
		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . $this->request->get['filter_vendor'];
		}
		
		$url .= $pageUrl;
		$pagination 		= new Pagination();
		$pagination->total 	= $list_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('inventory/reports/masterReport', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] 	  = $sort;
		$this->data['order'] 	  = $order;

		$this->data['link_csvexport'] = $this->url->link('inventory/reports/exportcsv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/exportpdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$this->data['filter_vendor'] 	= $filter_vendor;
		$this->data['filter_department']= $filter_department;
		$this->data['filter_location'] 	= $filter_location;
		$this->data['filter_category'] 	= $filter_category;
		$this->data['filter_brand'] 	= $filter_brand;
		$this->data['filter_sku'] 		= $filter_sku;
		$this->data['filter_name'] 		= $filter_name;

		$this->data['token'] 	= $this->session->data['token'];


		$this->template = 'inventory/report_master.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	
	public function exportcsv(){
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/inventory');
		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}

		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$pageUrl.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_brand = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = '';
		}

		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$pageUrl.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}

		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}

		$data = array(
			'show_hold'	  => $show_hold,
			'filter_department'	  => $filter_department, 
			'filter_location_code'	  => $filter_location, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_vendor'	      => $filter_vendor,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_name'=>$filter_name,
			'filter_sku'=>$filter_sku
		);
		

		$results = $this->model_inventory_inventory->getProductsReport($data);


	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=skuMasterReport.csv');
	        print "S.No,Product Code,Vendor,Department,Category,Sub Category,Brand,Description,Cost,CostWithGST,SellingPrice\r\n";
	        $this->load->model('user/user');

			$j=0;
			foreach ($results as $result) {	
				$subCateName = $this->model_inventory_inventory->getSubCategoryByCode($result['sku_subcategory_code']);
				$j++;
				$sku    	   = $this->csvEscape($result['sku']);
				$invoice_date  = '';
				$description   = $this->csvEscape($result['name']);
				$qty           = $result['qty'];
				$department    = $result['department_name'];
				$category      = $result['category_name'];
				$brand		   = $result['brand_name'];
				$selling_price = $result['sku_price'];
				$price         = $result['sku_cost'];
				$sub_total     = $result['sub_total'];
				$discount      = $result['discount'];
				$gst           = $result['gst'];
				$actual_total  = $result['actual_total'];
				$round_off     = $result['round_off'];
				$net_total	   = $result['net_total'];
				$vendor_name   = $result['vendor_name'];
				$cost_value    = $result['sku_cost'] + ($result['sku_cost'] * (7/100));
				$cost_value    = number_format($cost_value,2);
				print "$j,\"$sku\",\"$vendor_name\",\"$department\",\"$category\",\"$subCateName\",\"$brand\",\"$description\",\"$price\",\"$cost_value\",\"$selling_price\"\r\n";
			}			
			
		}  
	}

	public function csvEscape($value){
	    return htmlspecialchars($value);
	    //return '"' . str_replace('"', '""', $value) . '"';
	}
	public function exportpdf(){
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department');
		$this->load->model('inventory/category');
		
		$filterStr = '';

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}


		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];		
		} 
		
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];		
		} 

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];		
		} 

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_brand = $_REQUEST['filter_supplier'];
		} 
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
		}

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
		}
		
		$data = array(
			'show_hold'	  => $show_hold,
			'filter_department'	  => $filter_department, 
			'filter_location_code'=> $filter_location, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_vendor'	      => $filter_vendor,
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_sku'=>$filter_sku,
			'filter_name'=>$filter_name
		);
		
		$results = $this->model_inventory_inventory->getProductsReport($data);
		
		if($filter_location){
			$filterStr.=" Location: ".$filter_location.',';
		}

		if($filter_department){
			$departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
			$filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_category){
			$categoryResult = $this->model_inventory_category->getCategoryNameByCode($filter_category);
			$filterStr.=" Category: ".$categoryResult['category_name'].',';
		}
		if($filter_brand){
			$filterStr.=" Brand: ".$filter_brand.',';
		}
		if($filter_name){
			$filterStr.=" Sku: ".$filter_name.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}



		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">SKU Master Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
             if($filterStr){
        	$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
        }
       
       	$str.="<table class='listing' style='width:100%;'><thead>
           		<tr>
	           		<th>S.No</th>
	           		<th>Product Code</th>
	           		<th>Vendor</th>
	           		<th>Department</th>
	           		<th>Category</th>
	           		<th>Sub Category</th>
	           		<th>Brand</th>
	           		<th>Description</th>
	           		<th>Cost</th>
	           		<th>Cost With GST</th>
	           		<th>Selling Price</th>
           		</tr></thead> <tbody>";     

           $j=0;
           $total_price = 0;
           $total_selling_price =0;
			foreach ($results as $result) {	
				$subCateName = $this->model_inventory_inventory->getSubCategoryByCode($result['sku_subcategory_code']);
				$j++;
				$sku    		    = $result['sku'];
				$invoice_date       = '';
				$description      	= $result['name'];
				$qty                = $result['qty'];
				$department         =$result['department_name'];
				$category           =$result['category_name'];
				$brand				=$result['brand_name'];
				$selling_price      = $result['sku_price'];
				$price          	= $result['sku_cost'];
				$sub_total          = $result['sub_total'];
				$discount           = $result['discount'];
				$gst                = $result['gst'];
				$actual_total       = $result['actual_total'];
				$round_off          = $result['round_off'];
				$net_total			= $result['net_total'];
				$vendor_name 		= $result['vendor_name'];
				$cost_value         = $result['sku_cost'] + ($result['sku_cost'] * (7/100));
				$cost_value         = number_format($cost_value,2);

				$total_price+=$price;
				$total_selling_price+=$selling_price;

			$str.="<tr><td>".$j."</td>
					   <td>".$sku."</td>
					   <td>".$vendor_name."</td>
					   <td>".$department."</td>
					   <td>".$category."</td>
					   <td>".$subCateName."</td>
					   <td>".$brand."</td>
					   <td>".$description."</td>
					   <td align='right'>".number_format($price,2)."</td>
					   <td align='right'>".$cost_value."</td>
					   <td align='right'>".number_format($selling_price,2)."</td>
				</tr>";	
			}

			$str.="
			</tbody></table>";
			if($str){
				$filename = 'SkuReport_'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
			
	   
	}

	public function printStock() {
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('transaction/sales');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$locationDetails = $this->model_transaction_sales->getLocationCode($this->session->data['company_id']);

		$url = '';
		$this->data['back'] = $this->url->link('inventory/reports/printreports', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') || isset($this->request->get['stock_report'])) {			
			$company_id	= $this->session->data['company_id'];
			$stock_report = $this->request->get['stock_report'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$companyInfo	= $this->url->getCompanyAddress($company_details);
			//print_r($companyInfo);exit;
			if($companyInfo) {
				$this->data['companyInfo']	= $companyInfo;
			} else {
				$this->data['companyInfo']	='';
			}
			$pageUrl ='';

			if (strtolower($stock_report) == 'stock') {
				/*** 25-Feb-2016 ***/
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				$url = '';
				$company_details['location_code'] = $this->session->data['location_code'];
				$this->data['location_code'] 	  = $this->session->data['location_code'];
				if(trim($company_details['location_code'])=='HQ'){
					
					if($_REQUEST['location_code']!=''){
						$filter_location_code = $_REQUEST['location_code']; 
					}else{
						$filter_location_code = $company_details['location_code']; 
					}
					if($_REQUEST['stock_date']!=''){
						$filter_stock_date = $_REQUEST['stock_date']; 
					}else{
						$filter_stock_date    = date('d/m/Y');
					}
					$this->data['location_code'] = $filter_location_code;
					$this->data['stock_date']    = $filter_stock_date;
				}

				if (isset($_REQUEST['filter_department'])) {
					$filter_department = $_REQUEST['filter_department'];
				} else {
					$filter_department = '';
				}
				if($_REQUEST['stock_date']!=''){
					$filter_stock_date = $_REQUEST['stock_date']; 
				}else{
					$filter_stock_date    = date('d/m/Y');
				}
				if($_REQUEST['filter_minimum_status']!=''){
					$filter_minimum_status = $_REQUEST['filter_minimum_status']; 
				}else{
					$filter_minimum_status    = '0';
				}
				if (isset($_REQUEST['filter_vendor'])) {
					$filter_vendor = $_REQUEST['filter_vendor'];
				} else {
					$filter_vendor = '';
				}

				if (isset($_REQUEST['filter_name'])) {
					$filter_name = $_REQUEST['filter_name'];
				} else {
					$filter_name = '';
				}


				$this->data['stock_date']    	   	= $filter_stock_date;
				$this->data['filter_department']	= $filter_department;
				$this->data['filter_minimum_status']= $filter_minimum_status;
				$this->data['filter_name']			= $filter_name;
				$this->data['filter_vendor']		= $filter_vendor;

				$data['filter_stock'] = $this->config->get('config_report_qty_negative');
				$data['filter_stock'] = $this->data['filter_minimum_status'];
				//$this->model_inventory_inventory->updateConfigSetting($this->data['filter_minimum_status']);
				// printArray($data['filter_stock']); die;
				$data = array(
					'sort'            => $sort,
					'order'           => $order,
					'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit'           	   => $this->config->get('config_admin_limit'),
					'filter_department'	   => $this->data['filter_department'],
					'filter_location_code' => $this->data['location_code'],
					'filter_stock_date'    => $this->data['stock_date'],
					'filter_stock' 		   => $data['filter_stock'],
					'filter_minimum_status'=> $this->data['filter_minimum_status'],
					'filter_vendor'		   => $this->data['filter_vendor'],
					'filter_name'		   => $this->data['filter_name']
				);

				$list_total_ary = $this->model_inventory_inventory->getTotalProductsNew($data);
				$list_total 	= $list_total_ary['total'];
				$total_avg 		= $list_total_ary['total_avg'];
				$total_price    = $list_total_ary['total_price'];

				if($page==''){
					$page = 1;
				}	
				$results = $this->model_inventory_inventory->getProductsNew($data);

				$tot_quantity = 0;
				$tot_value = 0;
				foreach ($results as $result) {
					$result['NormalreservedQty']= $this->model_inventory_inventory->getResevedStockforNormalItems($result['product_id'],$this->data['location_code']);
					$result['ChildreservedQty'] = $this->model_inventory_inventory->getResevedStockforpackageItems($result['product_id'],$this->data['location_code']);
					$result['reservedQty']   	= $result['NormalreservedQty'] + $result['ChildreservedQty'];
					$result['availableQty']  	= $result['sku_qty'] - $result['reservedQty'];

						$QOH 	= 	$result['sku_qty']-$result['reserved_qty'];
						$tot_quantity+=$result['sku_qty'];
						// $total_price += $result['sku_qty'] * $result['sku_avg'];		
						$this->data['products'][] = array(
							'pagecnt'      		 => ($page-1)*$this->config->get('config_admin_limit'), 
							'product_id'         => $result['product_id'],
							'location'           => $result['location_Code'],
							'name'               => $result['name'],
							'departmentname'     => $result['department_name'],
							'sku'                => $result['sku'],
							'sku_category_code'  => $result['sku_category_code'],
							'sku_brand_code'     => $result['sku_brand_code'],
							'sku_price'          => $result['sku_avg'],
							'price'              => $result['price'],
							'quantity'           => $result['sku_qty'],
							'reserved_qty'       => $result['reserved_qty'],
							'availableQty'       => $result['availableQty'],
							'qoh'                => number_format($QOH,2),
							'total'              => number_format($QOH * $result['sku_avg'],2),
							'status'             => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'))
						);
				}
                  
				$this->data['products_sum']['tot_quantity'] = $tot_quantity;
				//$this->data['products_sum']['tot_value'] = $list_total_ary['total_price'];

				$this->data['products_sum']['tot_value'] =$total_price;
				// Vendor Collection
				$this->load->model('master/vendor');
				$this->data['vendor_collection'] 	 = $this->model_master_vendor->getVendors();
				$this->load->model('inventory/department');
				$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
				
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['filter_department'])) {
					$url .= '&filter_department=' . $this->request->get['filter_department'];
				}
				if (isset($this->data['location_code'])) {
					$url .= '&location_code=' . $this->data['location_code'];
				}
				if (isset($this->data['stock_date'])) {
					$url .= '&stock_date=' . $this->data['stock_date'];
				}
				if (isset($this->data['filter_minimum_status'])) {
					$url .= '&filter_minimum_status=' . $this->data['filter_minimum_status'];
				}
				if (isset($this->data['filter_vendor'])) {
					$url .= '&filter_vendor=' . $this->data['filter_vendor'];
				}
				if (isset($this->data['filter_name'])) {
					$url .= '&filter_name=' . urlencode($this->data['filter_name']);
				}	 
				$url.= $pageUrl;
				$pagination = new Pagination();
				$pagination->total = $list_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_admin_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->url->link('inventory/reports/printStock', 'token=' . $this->session->data['token'] . $url . '&stock_report=stock&page={page}', 'SSL');
				$this->data['pagination'] = $pagination->render();
				$this->data['filter_department'] = $filter_department;
				$this->data['filter_minimum_status'] = $filter_minimum_status;

			}else if (strtolower($stock_report) == 'master') {
				$results = $this->model_inventory_inventory->getProducts($data);			

			}
			$url.=$pageurl;
			$this->data['stock_report'] = $stock_report;
			$this->data['exportAction'] = $this->url->link('inventory/reports/export_stock_csv', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
			$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_stock_pdf', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
			$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		} else {
		    $this->redirect($this->url->link('inventory/reports', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['token'] = $this->session->data['token'];
				
			$this->data['location'] = $this->cart->getLocation($company_details['location_code']);
			$this->template = 'inventory/print_stock_hq.tpl';
		

		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']." ".$company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
		}
		return $company_address;
	}
	
	public function export_stock_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';
		
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		}else{
			$filter_department = '';
		}

		if (isset($this->request->get['location_code'])) {
			$filter_location_code = $this->request->get['location_code'];
		}else{
			$filter_location_code = '';
		}
		if (isset($this->request->get['stock_date'])) {
			$filter_stock_date = $this->request->get['stock_date'];
		}else{
			$filter_stock_date = '';
		}
		if (isset($this->request->get['filter_minimum_status'])) {
			$filter_minimum_status = $this->request->get['filter_minimum_status'];
		} else{
			$filter_minimum_status =0;
		}	
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
		} 
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = html_entity_decode($_REQUEST['filter_name']);
		}else{
			$filter_name = '';
		}
		$page 		  = $_REQUEST['page'];
		$filter_stock = $this->config->get('config_report_qty_negative');
		$filter_stock = $filter_minimum_status;

		$data = array(
			'filter_department'=>$filter_department,
			'filter_location_code' =>$filter_location_code,
			'filter_stock_date' =>$filter_stock_date,
			'filter_vendor' 	=>$filter_vendor,
			'filter_stock' 	=>$filter_stock,
			'filter_minimum_status' 	=>$filter_minimum_status,
			'filter_name' 		=>$filter_name
		);
		$results = $this->model_inventory_inventory->getProductsNew($data,'csv');

		if($filter_location_code){
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($filter_stock_date){
			$filterStr.=" Date: ".$filter_stock_date.',';
		}

		if($filter_department){
			$depatmentAry = $this->model_inventory_department->getDepartmentName($filter_department);
			$filterStr.=" Department: ".$depatmentAry['department_name'].',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$tot_quantity = 0;
		$tot_value = 0;

		if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
	             if($filterStr){
	        		$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
	        	 }
	        $str.="<table class='listing'><thead>
           		<tr>
	           		<th>S.No</th>
	           		<th>Location</th>
	           		<th>InvCode</th>
	           		<th>Name</th>
	           		<th>Department</th>
	           		<th>Stock</th>
	           		<th>AvgCost</th>
	           		<th>StockValue</th>
	           		<th>Actual Stock</th>
           		</tr></thead> <tbody>";  

		
			$j=0; 
			foreach ($results as $result) {
				$result['NormalreservedQty']= $this->model_inventory_inventory->getResevedStockforNormalItems($result['product_id'],$filter_location_code);
				$result['ChildreservedQty'] = $this->model_inventory_inventory->getResevedStockforpackageItems($result['product_id'],$filter_location_code);
				$result['reservedQty']   	= $result['NormalreservedQty'] + $result['ChildreservedQty'];
				$availableQty  				= $result['sku_qty'] - $result['reservedQty'];

			    $QOH 	= $result['sku_qty']-$result['reserved_qty'];
				$tot_quantity+=$result['sku_qty'];
				$tot_value+= $QOH * $result['sku_avg'];
				$j++;
				$name       = $result['name'];
				$sku        = $result['sku'];
				$location   = $result['location_Code'];
				$sku_price  = $result['sku_avg'];
				$quantity   = $result['sku_qty'];
				$reserved_qty   = $result['reserved_qty'];
				$QOH        = $QOH;
				$department = $result['department_name'];
				$total   	= number_format($QOH * $result['sku_avg'],2);
				
				$str.="<tr><td>".$j."</td>
							<td>".$location."</td>
					   		<td>".$sku."</td>
					   		<td>".$name."</td>
					   		<td>".$department."</td>
					   		<td>".$quantity."</td>
					   	    <td align='right'>".$sku_price."</td>
					   	    <td align='right'>".$total."</td>
					   	    <td align='right'>".$availableQty."</td>
				</tr>";	
			}
				$tot_value = number_format($tot_value,2);
				$str.="<tr><td colspan='7' align='right'>Total</td>
						   <td align='right'>".$tot_value."</td><td></td></tr>
				</tbody></table>";
				if($str){
					$filename = 'StockReport_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
			
		}  

	}

	public function export_stock_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		 
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		}
		if (isset($this->request->get['location_code'])) {
			$filter_location_code = $this->request->get['location_code'];
		}
		if (isset($this->request->get['stock_date'])) {
			$filter_stock_date = $this->request->get['stock_date'];
		}
		if (isset($this->request->get['filter_minimum_status'])) {
			$filter_minimum_status = $this->request->get['filter_minimum_status'];
		}else{
			$filter_minimum_status = 0;
		}
		if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} 
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = html_entity_decode($_REQUEST['filter_name']);
		}else{
			$filter_name = '';
		}
		$page 		  = $_REQUEST['page'];
		$filter_stock = $this->config->get('config_report_qty_negative');
		$filter_stock = $filter_minimum_status;	

		$data = array(
			'filter_department'=>$filter_department,
			'filter_location_code' =>$filter_location_code,
			'filter_stock_date' =>$filter_stock_date,
			'filter_vendor' 	=>$filter_vendor,
			'filter_stock' 	=>$filter_stock,
			'filter_minimum_status' 	=>$filter_minimum_status,
			'filter_name' 		=>$filter_name
		);

		$results = $this->model_inventory_inventory->getProductsNew($data,'csv');
		$tot_quantity = 0;
		$tot_value = 0;

		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=StockReport.csv');
	        print "S.No,Name,Inv.Code,Stock,Avg Cost,Stock Value,Actual Stock\r\n";
		
			$j=0; 
			foreach ($results as $result) {

				$result['NormalreservedQty']= $this->model_inventory_inventory->getResevedStockforNormalItems($result['product_id'],$filter_location_code);
				$result['ChildreservedQty'] = $this->model_inventory_inventory->getResevedStockforpackageItems($result['product_id'],$filter_location_code);
				$result['reservedQty']   	= $result['NormalreservedQty'] + $result['ChildreservedQty'];
				$availableQty  				= $result['sku_qty'] - $result['reservedQty'];

				$QOH = $result['sku_qty']-$result['reserved_qty'];
				$tot_quantity+=$result['sku_qty'];
				$tot_value+= $QOH * $result['sku_avg'];
			//$avg_price = $this->model_transaction_purchase->getPurchaseAvgCost($result['product_id']);
				$j++;
				$name       = $result['name'];
				$sku        = $result['sku'];
				$sku_price  = $result['sku_avg'];
				$quantity   = $result['sku_qty'];
				$reserved_qty   = $result['reserved_qty'];
				$QOH        = $QOH;
				$total   	 = number_format($QOH * $result['sku_avg'],2);
				$availableQty= $availableQty;
				print "$j,\"$name\",\"$sku\",\"$quantity\",\"$sku_price\",\"$total\",\"$availableQty\"\r\n";
			}
				$tot_value = number_format($tot_value,2);
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_value\",\"\"\r\n";
			
		}  

	}
	public function stockvaluationreport() {
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/bin');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['back'] = $this->url->link('inventory/reports/stockvaluationreport', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}


		$pageUrl ='';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = null;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}
		if (isset($_REQUEST['filter_dept_code'])) {
			$filter_dept_code = $_REQUEST['filter_dept_code'];
			$pageUrl.= '&filter_dept_code='.$filter_dept_code;
		} else {
			$filter_dept_code = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}

	
		
		$url = '';
			if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				$data = array(
					'sort'            => $sort,
					'order'           => $order,
					'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit'           => $this->config->get('config_admin_limit'),
					'filter_location_code'	  => $filter_location_code,
					'filter_date_from' => $filter_date_from,
					'filter_date_to' => $filter_date_to,
					'filter_dept_code' => $filter_dept_code,
					'filter_sku' => $filter_sku
				);
				//printArray($data); exit;

		if($filter_date_from!='' && $filter_date_to!='' &&$filter_location_code!=''){
			// Run the Sp_ProductStockSummaryByMonth Procedure 
			$this->model_inventory_inventory->runSp_ProductStockSummaryByMonth($data);
		}

		$list_total_arys = $this->model_inventory_inventory->getProductstockTotalNew($data);
		$list_total = $list_total_arys['total'];
			$this->data['products_sum']['total_Opening_Stock'] = $list_total_arys['total_Opening_Stock'];
			$this->data['products_sum']['total_Purchase'] = $list_total_arys['total_Purchase'];
			$this->data['products_sum']['total_Purchase_Return'] = $list_total_arys['total_Purchase_Return'];
			$this->data['products_sum']['total_Sales'] = $list_total_arys['total_Sales'];
			$this->data['products_sum']['total_Sales_Return'] = $list_total_arys['total_Sales_Return'];
			$this->data['products_sum']['total_Closing_Stock'] = $list_total_arys['total_Closing_Stock'];
			$this->data['products_sum']['total_Avg_Cost'] = $list_total_arys['total_Avg_Cost'];
			$this->data['products_sum']['total_Stock_Value'] = $list_total_arys['total_Stock_Value'];
			$this->data['products_sum']['total_transfer_qty'] = $list_total_arys['total_transfer_qty'];
		// End
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['Todepartment']= $this->model_inventory_inventory->getDepartment();

		$Dep_results = $this->model_inventory_inventory->getDepartment();
		foreach ($Dep_results as $result) {
			$depName = $this->model_inventory_department->getDepartment($result['Dept_Code']);
			$this->data['TodepartmentNew'][] = array(
				'department_code' => $depName['department_code'],
				'department_name' => $depName['department_name'],
			);
		}

		// printArray($this->data['TodepartmentNew']);die;
		$results = $this->model_inventory_inventory->getProductstock($data);
		foreach ($results as $result) {
			$this->data['productstock'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'),
				'Location_Code' 		=> $result['Location_Code'],
				'Stock_Date'       		=> $result['Stock_Date'],
				'Sku'        			=> $result['Sku'],
				'Sku_Description' 		=> $result['Sku_Description'],
				'Dept_Code'   			=> $result['Dept_Code'],
				'Opening_Stock'      	=> $result['Opening_Stock'],
				'Purchase'        	  	=> $result['Purchase'],
				'Purchase_Return'   	=> $result['Purchase_Return'],
				'Sales'      			=> $result['Sales'],
				'Sales_Return'   		=> $result['Sales_Return'],
				'transfer_qty'   		=> $result['transfer_qty'],
				'Closing_Stock'   		=> $result['Closing_Stock'],
				'Avg_Cost'      		=> $result['Avg_Cost'],
				'Stock_Value'   		=> $result['Stock_Value']
			);
		}

		$this->data['stock_report'] = $stock_report;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);


		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$url .= '&filter_dept_code=' . $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}
		
		$url.= $pageUrl;
		$pagination = new Pagination();
		$pagination->total = $list_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit'); //5
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/stockvaluationreport', 'token=' . $this->session->data['token'] . $url. '&stock_report=stock&page={page}', 'SSL');
		$this->data['exportAction'] = $this->url->link('inventory/reports/export_Productstock_csv', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_Productstock_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['token'] = $this->session->data['token'];
		$this->template = 'inventory/stock_valuation_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	public function export_Productstock_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Valuation Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		 
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$filter_dept_code = $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		}
		$page = $_REQUEST['page'];
		$data = array(
			'filter_location_code'	  => $filter_location_code,
			'filter_date_from' => $filter_date_from,
			'filter_date_to' => $filter_date_to,
			'filter_dept_code' => $filter_dept_code,
			'filter_sku' => $filter_sku
		);

		$results= $this->model_inventory_inventory->getProductstock($data);
		$Opening_Stock_cnt 	 = 0;
		$Purchase_cnt 	   	 = 0;
		$Purchase_Return_cnt = 0;
		$Sales_cnt 	   	 	 = 0;
		$Sales_Return_cnt 	 = 0;
		$Closing_Stock_cnt   = 0;
		$Stock_Value_cnt     = 0; 


	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=StockValuationReport.csv');
	        print "S.No,Location Code,Stock Date,Sku,Sku Description,Dept Code,Opening Stock,Purchase,Purchase Return,Sales,Sales Return,Transfer Qty,Closing Stock,Avg Cost,Stock Value\r\n";
			$j=0; 
			$tot_value = 0;
			foreach ($results as $result) {	
					$j++;
					$Location_Code       = $result['Location_Code'];
					$Stock_Date        	 = $result['Stock_Date'];
					$Sku 			 	 = $result['Sku'];
					$Sku_Description   	 = $result['Sku_Description'];
					$Dept_Code   		 = $result['Dept_Code'];
					$Opening_Stock       = $result['Opening_Stock'];
					$Purchase        	 = $result['Purchase'];
					$Purchase_Return 	 = $result['Purchase_Return'];
					$Sales   		 	 = $result['Sales'];
					$Sales_Return   	 = $result['Sales_Return'];
					$transfer_qty        = $result['transfer_qty'];
					$Closing_Stock       = $result['Closing_Stock'];
					$Avg_Cost 			 = $result['Avg_Cost'];
					$Stock_Value   		 = $result['Stock_Value'];
					$tot_value+=$Stock_Value;
	
				print "$j,\"$Location_Code\",\"$Stock_Date\",\"$Sku\",\"$Sku_Description\",\"$Dept_Code\",\"$Opening_Stock\",\"$Purchase\",\"$Purchase_Return\",\"$Sales\",\"$Sales_Return\",\"$transfer_qty\",\"$Closing_Stock\",\"$Avg_Cost\",\"$Stock_Value\"\r\n";
			}

			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$tot_value\"\r\n";
			
		}  

	}

	public function export_Productstock_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('inventory/department');

		$filterStr = '';

		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$filter_dept_code = $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		}

		$data = array(
			'filter_location_code'	  => $filter_location_code,
			'filter_date_from' => $filter_date_from,
			'filter_date_to' => $filter_date_to,
			'filter_dept_code' => $filter_dept_code,
			'filter_sku' => $filter_sku
		);
		$results= $this->model_inventory_inventory->getProductstock($data);
		if($filter_location_code){
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($filter_date_from){
			$filterStr.=" Date From: ".$filter_date_from.',';
		}

		if($filter_date_to){
			$filterStr.=" Date To: ".$filter_date_to.',';
		}

		if($filter_dept_code){
			$depatmentAry = $this->model_inventory_department->getDepartmentName($filter_dept_code);
			$filterStr.=" Department: ".$depatmentAry['department_name'].',';
		}

		if($filter_sku){
			$filterStr.=" SKU: ".$filter_sku.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Valuation Details Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
             if($filterStr){
        	$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
        }

       	$str.="<table class='listing'><thead>
   		<tr>
	   		<th>S.No</th>
	   		<th>Location Code</th>
	   		<th>Stock Date</th>
	   		<th>Sku</th>
	   		<th>Sku Description</th>
	   		<th>Dept Code</th>
	   		<th>Opening Stock</th>
	   		<th>Purchase</th>
	   		<th>Purchase Return</th>
	   		<th>Sales</th>
	   		<th>Sales Return</th>
	   		<th>Transfer Qty</th>
	   		<th>Closing Stock</th>
	   		<th>Avg Cost</th>
	   		<th>Stock Value</th>
	   		</tr></thead> <tbody>";   
	   	$j=0;
	   	$tot_value = 0;
	   	foreach ($results as $result) {
			$j++;
			$Location_Code       = $result['Location_Code'];
			$Stock_Date        	 = $result['Stock_Date'];
			$Sku 			 	 = $result['Sku'];
			$Sku_Description   	 = $result['Sku_Description'];
			$Dept_Code   		 = $result['Dept_Code'];
			$Opening_Stock       = $result['Opening_Stock'];
			$Purchase        	 = $result['Purchase'];
			$Purchase_Return 	 = $result['Purchase_Return'];
			$Sales   		 	 = $result['Sales'];
			$Sales_Return   	 = $result['Sales_Return'];
			$transfer_qty        = $result['transfer_qty'];
			$Closing_Stock       = $result['Closing_Stock'];
			$Avg_Cost 			 = $result['Avg_Cost'];
			$Stock_Value   		 = $result['Stock_Value'];
			$tot_value+=$Stock_Value;
			$str.="<tr><td>".$j."</td>
				   <td>".$Location_Code."</td>
				   <td>".$Stock_Date."</td>
				   <td>".$Sku."</td>
				   <td>".$Sku_Description."</td>
				   <td>".$Dept_Code."</td>
				   <td>".$Opening_Stock."</td>
				   <td>".$Purchase."</td>
				   <td>".$Purchase_Return."</td>
				   <td>".$Sales."</td>
				   <td>".$Sales_Return."</td>
				   <td>".$transfer_qty."</td>
				   <td>".$Closing_Stock."</td>
				   <td>".$Avg_Cost."</td>
				   <td>".$Stock_Value."</td>
			</tr>";	
	   	}
		$str.="<tr><td colspan='14' align='right'>Total</td>
						   <td align='right'>".$tot_value."</td></tr>
		</tbody></table>";
		if($str){
			$filename = 'stockvalutionReport'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function stockvaluationSummaryreport() {
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/bin');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['back'] = $this->url->link('inventory/reports/stockvaluationreport', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}


		$pageUrl ='';
           //printArray($_REQUEST); exit;
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}
		if (isset($_REQUEST['filter_dept_code'])) {
			$filter_dept_code = $_REQUEST['filter_dept_code'];
			$pageUrl.= '&filter_dept_code='.$filter_dept_code;
		} else {
			$filter_dept_code = '';
		}
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$pageUrl.= '&filter_name='.$filter_name;
		} else {
			$filter_name = '';
		}
		if (isset($_REQUEST['filter_sku'])) {
			$filter_sku = $_REQUEST['filter_sku'];
			$pageUrl.= '&filter_sku='.$filter_sku;
		} else {
			$filter_sku = '';
		}
		

	
		
		$url = '';
			if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				$data = array(
					'sort'            => $sort,
					'order'           => $order,
					'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit'           => $this->config->get('config_admin_limit'),
					'filter_location_code'	  => $filter_location_code,
					'filter_date_from' => $filter_date_from,
					'filter_date_to' => $filter_date_to,
					'filter_dept_code' => $filter_dept_code,
					'filter_sku' => $filter_sku
				);
         
		$this->data['data']		= $data;
		if($filter_date_from!='' && $filter_date_to!='' &&$filter_location_code!=''){
			// Run the Sp_ProductStockSummaryByMonth Procedure 
			$this->model_inventory_inventory->runSp_ProductStockSummaryByMonth($data);
		}

		$list_total_arys = $this->model_inventory_inventory->getProductstockTotalNew($data);

		$list_total = $list_total_arys['total'];
		$last_page  = ceil($list_total / $this->config->get('config_admin_limit'));
		// if($page == $last_page){
			$this->data['products_sum']['total_Opening_Stock'] = $list_total_arys['total_Opening_Stock'];
			$this->data['products_sum']['total_Purchase'] = $list_total_arys['total_Purchase'];
			$this->data['products_sum']['total_Purchase_Return'] = $list_total_arys['total_Purchase_Return'];
			$this->data['products_sum']['total_Sales'] = $list_total_arys['total_Sales'];
			$this->data['products_sum']['total_Sales_Return'] = $list_total_arys['total_Sales_Return'];
			$this->data['products_sum']['total_Closing_Stock'] = $list_total_arys['total_Closing_Stock'];
			$this->data['products_sum']['total_Avg_Cost'] = $list_total_arys['total_Avg_Cost'];
			$this->data['products_sum']['total_Stock_Value'] = $list_total_arys['total_Stock_Value'];
			//$this->data['products_sum']['total_transfer_qty'] = $list_total_arys['total_transfer_qty'];
		// }
		// End
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['Todepartment']= $this->model_inventory_inventory->getDepartment();

		$Dep_results = $this->model_inventory_inventory->getDepartment();
		foreach ($Dep_results as $result) {
			$depName = $this->model_inventory_department->getDepartment($result['Dept_Code']);
			if(isset($depName['department_code']) && $depName['department_name']){
				$this->data['TodepartmentNew'][] = array(
					'department_code' => $depName['department_code'],
					'department_name' => $depName['department_name']
				);
			}
		}
		$results = $this->model_inventory_inventory->getProductstock($data);
		foreach ($results as $result) {
			$this->data['productstock'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'), 
				'Location_Code' 		=> $result['Location_Code'],
				'Stock_Date'       		=> $result['Stock_Date'],
				'Sku'        			=> $result['Sku'],
				'Sku_Description' 		=> $result['Sku_Description'],
				'Dept_Code'   			=> $result['Dept_Code'],
				'Opening_Stock'      	=> $result['Opening_Stock'],
				'Purchase'        	  	=> $result['Purchase'],
				'Purchase_Return'   	=> $result['Purchase_Return'],
				'Sales'      			=> $result['Sales'],
				'Sales_Return'   		=> $result['Sales_Return'],
				'transfer_qty'   		=> $result['transfer_qty'],
				'Closing_Stock'   		=> $result['Closing_Stock'],
				'Avg_Cost'      		=> $result['Avg_Cost'],
				'Stock_Value'   		=> $result['Stock_Value']
			);
		}

		$this->data['stock_report'] = $stock_report;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);


		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$url .= '&filter_dept_code=' . $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}
		
		$url.= $pageUrl;
		$pagination = new Pagination();
		$pagination->total = $list_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit'); //5
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/stockvaluationSummaryreport', 'token=' . $this->session->data['token'] . $url. '&stock_report=stock&page={page}', 'SSL');
		$this->data['exportAction'] = $this->url->link('inventory/reports/export_ProductstockSummary_csv', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_ProductstockSummary_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		// printArray($pagination);die;
		$this->data['pagination'] = $pagination->render();

		$this->data['token'] = $this->session->data['token'];
		$this->template = 'inventory/stock_valuation_Summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());

	}
	public function export_ProductstockSummary_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Valuation Summary Report';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		 
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$filter_dept_code = $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		}
		$page = $_REQUEST['page'];
		$data = array(
			// 'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			// 'limit'       => $this->config->get('config_admin_limit'),
			'filter_location_code'	  => $filter_location_code,
			'filter_date_from' => $filter_date_from,
			'filter_date_to' => $filter_date_to,
			'filter_dept_code' => $filter_dept_code,
			'filter_sku' => $filter_sku
		);

		$results= $this->model_inventory_inventory->getProductstock($data);

	    if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=StockValuationSummaryReport.csv');
	        print "S.No,Location Code,Stock Date,Sku,Sku Description,Dept Code,Opening Stock,Purchase,Purchase Return,Sales,Sales Return,Transfer Qty,Closing Stock,Avg Cost,Stock Value\r\n";
			$j=0;
			$tot_value = 0; 
		foreach ($results as $result) {	
			
						$j++;
						$Location_Code       = $result['Location_Code'];
						$Stock_Date        	 = $result['Stock_Date'];
						$Sku 			 	 = $result['Sku'];
						$Sku_Description   	 = $result['Sku_Description'];
						$Dept_Code   		 = $result['Dept_Code'];
						$Opening_Stock       = $result['Opening_Stock'];
						$Purchase        	 = $result['Purchase'];
						$Purchase_Return 	 = $result['Purchase_Return'];
						$Sales   		 	 = $result['Sales']; 
						$Sales_Return   	 = $result['Sales_Return'];
						$transfer_qty        = $result['transfer_qty'];
						$Closing_Stock       = $result['Closing_Stock'];
						$Avg_Cost 			 = $result['Avg_Cost'];
						$Stock_Value   		 = $result['Stock_Value'];
						$tot_value+=$Stock_Value;
				print "$j,\"$Location_Code\",\"$Stock_Date\",\"$Sku\",\"$Sku_Description\",\"$Dept_Code\",\"$Opening_Stock\",\"$Purchase\",\"$Purchase_Return\",\"$Sales\",\"$Sales_Return\",\"$transfer_qty\",\"$Closing_Stock\",\"$Avg_Cost\",\"$Stock_Value\"\r\n";
		
		}
			$tot_value = number_format($tot_value,2);
			print "\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$tot_value\"\r\n";
		}  

	}

	public function export_ProductstockSummary_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		 
		if (isset($this->request->get['filter_location_code'])) {
			$filter_location_code = $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$filter_dept_code = $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$filter_sku = $this->request->get['filter_sku'];
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		}
		$data = array(
			// 'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			// 'limit'       => $this->config->get('config_admin_limit'),
			'filter_location_code'	  => $filter_location_code,
			'filter_date_from' => $filter_date_from,
			'filter_date_to' => $filter_date_to,
			'filter_dept_code' => $filter_dept_code,
			'filter_sku' => $filter_sku
		);

		
		$results= $this->model_inventory_inventory->getProductstock($data);
		// printArray($results);die;

		if($filter_location_code){
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($filter_date_from){
			$filterStr.=" Date From: ".$filter_date_from.',';
		}

		if($filter_date_to){
			$filterStr.=" Date To: ".$filter_date_to.',';
		}

		if($filter_dept_code){
			$filterStr.=" Department: ".$results[0]['department_name'].',';
		}

		/*if($filter_sku){
			$filterStr.=" SKU: ".$filter_sku.',';
		}*/

		if($filter_name){
			$filterStr.=" Sku: ".$filter_name.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}


		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Valuation Summary Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
		     if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing'><thead>
			<tr>
			<th>S.No</th>
			<th>Location Code</th>
			<th>Stock Date</th>
			<th>Sku</th>
			<th>Sku Description</th>
			<th>Dept Code</th>
			<th>Opening Stock</th>
			<th>Purchase</th>
			<th>Purchase Return</th>
			<th>Sales</th>
			<th>Sales Return</th>
			<th>Transfer Qty</th>
			<th>Closing Stock</th>
			<th>Avg Cost</th> 
			<th>Stock Value</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		foreach ($results as $result) {
			$j++;
			$Location_Code       = $result['Location_Code'];
			$Stock_Date        	 = $result['Stock_Date'];
			$Sku 			 	 = $result['Sku'];
			$Sku_Description   	 = $result['Sku_Description'];
			$Dept_Code   		 = $result['Dept_Code'];
			$Opening_Stock       = $result['Opening_Stock'];
			$Purchase        	 = $result['Purchase'];
			$Purchase_Return 	 = $result['Purchase_Return'];
			$Sales   		 	 = $result['Sales'];
			$Sales_Return   	 = $result['Sales_Return'];
			$transfer_qty        = $result['transfer_qty'];
			$Closing_Stock       = $result['Closing_Stock'];
			$Avg_Cost 			 = $result['Avg_Cost'];
			$Stock_Value   		 = $result['Stock_Value'];
			
			$total_Stock_Value+=$Stock_Value;

			$str.="<tr><td>".$j."</td>
				   <td>".$Location_Code."</td>
				   <td>".$Stock_Date."</td>
				   <td>".$Sku."</td>
				   <td>".$Sku_Description."</td>
				   <td>".$Dept_Code."</td>
				   <td>".$Opening_Stock."</td>
				   <td>".$Purchase."</td>
				   <td>".$Purchase_Return."</td>
				   <td>".$Sales."</td>
				   <td>".$Sales_Return."</td>
				   <td>".$transfer_qty."</td>
				   <td align='right'>".$Closing_Stock."</td>
				   <td align='right'>".$Avg_Cost."</td>
				   <td align='right'>".$Stock_Value."</td>
			</tr>";	
		}
		$total_Stock_Value = number_format($total_Stock_Value,2);
		$str.="<tr><td colspan='14' align='right'>Total</td>
						   <td align='right'>".$total_Stock_Value."</td></tr>
		</tbody></table>";
		if($str){
			$filename = 'StockvaluationsummaryReport_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}

	}
	public function purchase_invoice_details() {
		
		$this->load->model('transaction/purchase');
		$this->document->setTitle('Purchase Invoice Details Report');
		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			//$filter_date_from = '';
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if ($_REQUEST['filter_location_code']) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}


		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			//$filter_date_to = '';
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno;
		
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}
		if (isset($_GET['filter_transactionno']) && !$_POST) {
			$filter_date_from='';
			$filter_date_to='';
			$pageUrl.= '&filter_date_from=';
            $pageUrl.= '&filter_date_to='; 
		}
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		//printArray($data); exit;
		$this->data['data'] = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPurchaseInvoice($data);
		
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = $purchase_grand_total['grandtotal'];
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);

		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
	
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$test = 0; 
		$grandtotal = 0;
		$netTotal =0;
		foreach ($results as $result) {	
			$grandtotal+=$netTotal; 
			$itemDetails = $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($result['transaction_no']);

			$this->data['purchaseHeader'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_code'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'salesDetails'      => $itemDetails
				
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/purchase_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_purchase_invoice_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_purchase_invoice_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_invoice_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function purchase_details() {
		
		$this->load->model('transaction/purchase');
		$this->document->setTitle('Purchase Order Details Report');
		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			//$filter_date_from = '';
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if ($_REQUEST['filter_location_code']) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}


		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			//$filter_date_to = '';
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno;
		
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}
		if (isset($_GET['filter_transactionno']) && !$_POST) {
			$filter_date_from='';
			$filter_date_to='';
			$pageUrl.= '&filter_date_from=';
            $pageUrl.= '&filter_date_to='; 
		}
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		//printArray($data); exit;
		$this->data['data'] = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPurchaseNew($data);
		
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = $purchase_grand_total['grandtotal'];
		$results = $this->model_transaction_purchase->getPurchaseList($data);

		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
	
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_summary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_summary&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');

		foreach ($results as $result) {	
			$itemDetails = $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($result['transaction_no']);

			$this->data['purchaseHeader'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_code'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'salesDetails'      => $itemDetails
				
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/purchase_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_purchase_details_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_purchase_details_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_purchase_invoice_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} 


		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);

		$tot_quantity = 0;
		$tot_value = 0;

		$salesHeader =  $this->model_transaction_purchase->getPurchaseInvoiceList($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$refno 		= $value['reference_no'];
				$refdate 	= date('d/m/Y',strtotime($value['reference_date']));
				$results= $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($value['transaction_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=purchaseInvoiceDetailReport.csv');
		        print "\"$invoice_no\",\"\",\"Ref No : $refno\",\"\",\"Ref Date: $refdate\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_title'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];

					  $actualtotal+=$result['total'];
	                  $tot_subtotal+=$result['net_price'];
	                  $tot_gst+=$result['tax_price'];
	                  $tot_discount+=$result['discount_price'];

			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}   
	}

	public function export_purchase_invoice_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Invoice Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no   = $value['transaction_no'];
				$invoice_date = date('d/m/Y',strtotime($value['transaction_date']));
				$refno        = $value['reference_no'];
				$refdate      = date('d/m/Y',strtotime($value['reference_date']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='2' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference No:".$refno."</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference Date:".$refdate."	</th>
						<th colspan='3' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($invoice_no);
		    	
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_title'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];

					  $actualtotal+=$result['total'];
	                  $tot_subtotal+=$result['net_price'];
	                  $tot_gst+=$result['tax_price'];
	                  $tot_discount+=$result['discount_price'];

					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($tot_subtotal,2)."</td>
							<td class='right'>".number_format($tot_discount,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($actualtotal,2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'purchase_invoice_details_Report_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
	}

	public function export_purchase_details_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} 


		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);

		$tot_quantity = 0;
		$tot_value = 0;

		$salesHeader =  $this->model_transaction_purchase->getPurchaseList($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$refno 		= $value['reference_no'];
				$refdate 	= $value['reference_date'];

				$results= $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($value['transaction_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=purchaseDetailReport.csv');
		        print "\"$invoice_no\",\"\",\"Ref No : $refno\",\"\",\"Ref Date: $refdate\"\r\n";
		        print "S.No,SKU,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];
					  $actualtotal+=$result['total'];
	                  $tot_subtotal+=$result['net_price'];
	                  $tot_gst+=$result['tax_price'];
	                  $tot_discount+=$result['discount_price'];

			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$actualtotal\"\r\n";
			}
		}   
	}

	public function export_purchase_details_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		$results = $this->model_transaction_purchase->getPurchaseList($data);

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Order Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$invoice_date = $value['transaction_date'];
				$refno       = $value['reference_no'];
				$refdate     = $value['reference_date'];
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
						<th colspan='2' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference No:".$refno."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference Date:".$refdate."</th>
						<th colspan='3' align='right' style='border-left:none;'>Invoice Date:".date('d/m/Y',strtotime($invoice_date))."</th></tr>
						<tr>
							<th>SKU</th>
							<th colspan='2'>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th align='right'>Sub Total</th>
							<th align='right'>GST</th>
							<th align='right'>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($invoice_no);
		    	
				$j=0; 

				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_name'];
					$name               = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];

					$str.="<tr>
							   <td>".$sku."</td>
							   <td colspan='2'>".$name."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='5' align='right'>Total</td>
							<td class='right'>".number_format($value['sub_total'],2)."</td>
							<td class='right'>".number_format($value['gst'],2)."</td>
							<td class='right'>".number_format($value['total'],2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
			/*<td class='right'>".number_format($value['discount'],2)."</td>*/
				if($str){
					$filename = 'purchase_details_Report_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
	}
	public function purchase_return_details() {
		$this->load->model('transaction/purchase_return');
		$this->load->model('transaction/purchase');
		$this->document->setTitle('Purchase Return Details Report');
		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else if (isset($_REQUEST['filter_transactionno']) && !isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = '';
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else if (isset($_REQUEST['filter_transactionno']) && !isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = '';
		}  else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}


		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else if (isset($_REQUEST['filter_transactionno']) && !isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = '';
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);

		$this->data['data'] = $data;
		$purchase_grand_totals = $this->model_transaction_purchase_return->getTotalPurchaseListNew($data);
		
		$purchase_total = $purchase_grand_totals['total'];
		$this->data['purchase_return_grand_total'] = $purchase_grand_totals['grandtotal'];
		$results = $this->model_transaction_purchase_return->getPurchaseList($data);

		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
		$grandtotal = 0;
		$netTotal =0;
		
		foreach ($results as $result) {	
			/*$totalAry = $this->model_transaction_purchase->getTotalPurchaseHistoryNew($result['purchase_id']);*/
			$itemDetails = $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($result['transaction_no']);

			$this->data['purchaseHeader'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'reference_no'  	=> $result['reference_no'],
				'reference_date'  	=> date('d/m/Y',strtotime($result['reference_date'])),
				'vendor_code'       => $result['vendor_code'],
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'discount'			=> $result['discount'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'salesDetails'      =>$itemDetails
				
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/purchase_return_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_purchase_returndetails_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_purchase_returndetails_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_return_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_purchase_returndetails_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('transaction/purchase_return');
		$this->load->model('transaction/purchase');
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else if((!isset($_REQUEST['filter_date_from'])) && isset($_REQUEST['filter_transactionno'])) {
			$filter_date_from = '';
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else if ((!isset($_REQUEST['filter_date_to'])) && isset($_REQUEST['filter_transactionno'])){
			$filter_date_to = '';
		} 
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
		} 
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);

		$tot_quantity = 0;
		$tot_value = 0;

		$salesHeader =  $this->model_transaction_purchase_return->getPurchaseList($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$reference_no       = $value['reference_no'];
				$reference_date     = date('d/m/Y',strtotime($value['reference_date']));
				$results= $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($value['transaction_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=purchaseReturnDetailReport.csv');
		        print "\"$invoice_no\",\"\",\"Reference No\",\"\",\"$reference_no\",\"Reference Date\",\"\",\"$reference_date\"\r\n";
		        print "S.No,sku,Name,Qty,Price,Sub Total,Discount,GST,Net Total\r\n";
				$j=0; 
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];
			
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$sku_price\",\"$sub_total\",\"$discount\",\"$gst\",\"$actual_total\"\r\n";
				}
				$tot_subtotal = $value['sub_total'];
				$tot_discount = $value['discount'];
				$tot_gst      = $value['gst'];
				$tot_total    = $value['total'];

				print "\"\",\"\",\"\",\"\",\"\",\"$tot_subtotal\",\"$tot_discount\",\"$tot_gst\",\"$tot_total\"\r\n";
			}
		}   
	}

	public function export_purchase_returndetails_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('transaction/purchase_return');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else if((!isset($_REQUEST['filter_date_from'])) && isset($_REQUEST['filter_transactionno'])) {
			$filter_date_from = '';
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else if ((!isset($_REQUEST['filter_date_to'])) && isset($_REQUEST['filter_transactionno'])){
			$filter_date_to = '';
		} else {
			$filter_date_to = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$filterStr.=" Supplier: ".$filter_supplier.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);
		$results = $this->model_transaction_purchase_return->getPurchaseList($data);

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Return Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$invoice_date = date('d/m/Y',strtotime($value['transaction_date']));
				$reference_no       = $value['reference_no'];
				$reference_date     = date('d/m/Y',strtotime($value['reference_date']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='2' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference No:".$reference_no."	</th>
						<th colspan='2' class='left' style='border-right:none;'>Reference Date:".$reference_date."	</th>
						<th colspan='5' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Qty</th>
							<th>Price</th>
							<th>Sub Total</th>
							<th>Discount</th>
							<th>GST</th>
							<th>Net Total</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_purchase->getpurchaseDetailsbyInvoice($invoice_no);
		    	
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_name'];
					$description        = $result['name'];
					$qty    	        = $result['quantity'];
					$sku_price    		= $result['price'];
					$sub_total          = $result['net_price'];
					$discount    		= $result['discount_price'];
					$gst    			= $result['tax_price'];
					$actual_total       = $result['total'];
					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$qty."</td>
							   <td class='right'>".$sku_price."</td>
							   <td class='right'>".$sub_total."</td>
							   <td class='right'>".$discount."</td>
							   <td class='right'>".$gst."</td>							   
							   <td class='right'>".$actual_total."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='4' align='right'>Total</td>
							<td class='right'>".number_format($value['sub_total'],2)."</td>
							<td class='right'>".number_format($value['discount'],2)."</td>
							<td class='right'>".number_format($value['gst'],2)."</td>
							<td class='right'>".number_format($value['total'],2)."</td>

				</tr><tr><td colspan='8' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'purchase_Returndetails_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
	}

	// PO Request reprt start
	public function po_request() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		$this->document->setTitle('PO Request Report');
		$this->load->model('transaction/purchase');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];

		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}


		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);

		$this->data['data'] = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPOrequest($data);
		// printArray($purchase_grand_total); die;
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = number_format($purchase_grand_total['grandtotal'],2);
		$results = $this->model_transaction_purchase->getPOrequestList($data);


		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
		$test = 0; 
		$grandtotal = 0;
		$netTotal =0;
		foreach ($results as $result) {	
			$grandtotal+=$result['Nettotal'];
			$this->data['purchases'][] = array(
				'transaction_no'    => $result['PO_Req_No'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['PO_Req_Date'])),
				'vendor_code'       => $result['Vendor_Code'],
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['Subtotal'],
				'gst'				=> $result['Gst'],
				'total'             => $result['Nettotal'],
				'view_button' => $this->url->link('inventory/reports/po_requestView', 'token=' . $this->session->data['token'] . '&transId_id=' . $result['PO_Req_No'], 'SSL')			
			);
		}

		$this->data['grandtotal'] = $purchase_grand_total['grandtotal'];
	
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/po_request', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['link_csvexport'] = $this->url->link('transaction/transaction_reports/export_purchase_summary_csv', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_porequest_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/po_request_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function po_requestView(){
		
		$this->language->load('transaction/purchase');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$this->document->setTitle('PO Request Details');

		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no'] = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_tran_type'] = $this->language->get('text_tran_type');
		$this->data['text_vendor_code'] = $this->language->get('text_vendor_code');
		$this->data['text_vendor_name'] = $this->language->get('text_vendor_name');
		$this->data['text_net_total'] = $this->language->get('text_net_total');
		$this->data['text_created_by'] = $this->language->get('text_created_by');
		$this->data['text_created_on'] = $this->language->get('text_created_on');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');
		$this->data['text_print'] = $this->language->get('text_print');
		$this->data['text_modify'] = $this->language->get('text_modify');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'PO Request View',
			'href'      => $this->url->link('inventory/reports/po_request', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['back_button'] = $this->url->link('inventory/reports/po_request', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$fdata = array('filter_transactionno'=>$this->request->get['transId_id']);
		$details	= $this->model_transaction_purchase->getPOrequestList($fdata );
		$pageUrl    = 'transaction_no='.$this->request->get['transId_id'];
		$pageUrl2   = 'poreq_num='.$this->request->get['transId_id'];
		$this->data['purchaseInfo']  = $details[0];
		$this->data['productDetails'] 	= $this->model_transaction_purchase->gePORequestDetailsById($this->request->get['transId_id']);
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_porequestview_pdf', 'token=' . $this->session->data['token'] .'&'.$pageUrl,'SSL');
		$this->data['link_convert'] = $this->url->link('transaction/purchase/insert', 'token=' . $this->session->data['token'] .'&'.$pageUrl2,'SSL');
		 //printArray($this->data['productDetails']); die;
		$this->template = 'transaction/porequest_view.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	
	public function export_porequestview_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('transaction/purchase');
		$this->load->model('master/vendor');

		$filterStr = '';

		if (isset($_REQUEST['transaction_no'])) {
			$filter_transactionno = $_REQUEST['transaction_no'];
		}


		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_transactionno'=>$filter_transactionno
		);
		$HeaderResults = $this->model_transaction_purchase->getPOrequestList($data);
		$results 	   = $this->model_transaction_purchase->gePORequestDetailsById($filter_transactionno);
		
		$company_id		 = $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
		$supplier_name   = $HeaderResults[0]['vendor_name'];
		$location_code   =  $HeaderResults[0]['location_code'];
		$vendorDetails   = $this->model_master_vendor->getSuppliersDetailsbyCode($HeaderResults[0]['Vendor_Code']);
		if($vendorDetails){
			$address = $vendorDetails['address1'].', '.$vendorDetails['address2'].', '.$vendorDetails['country'].', '.$vendorDetails['postal_code'];
			$phone   = $vendorDetails['phone'];
		}
	
		$str = $headerStr;
		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">PO Request No:'.$filter_transactionno.'</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';

		if($supplier_name){
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:16px;">Supplier Name :'.$supplier_name.'</td>
				<td align="right">Location: '.$location_code.'</td></tr>';
			if($address){
				$str.= '<tr><td align="left">Address: '.$address.'</td><td align="right">Phone: '.$phone.'</td></tr>';
			}
			$str.= '</table>';
		}

		$str.="<table class='listing' style='width:100%;'><thead>
			<tr>
             <th>SNo</th>
             <th>SKU</th>
             <th>Description</th>
             <th>Quantity</th>
             <th>SkuCost</th>
             <th>SubTotal</th>
             <th>GST</th>
             <th>NetTotal</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		$total = 0;
		foreach ($results as $result) {
			$j++;
			$total+=$result['Nettotal']; 

		
			$str.="<tr>
					<td>".$j."</td>
				   <td>".$result['Sku']."</td>
				   <td>".$result['Description']."</td>
				   <td>".$result['Qty']."</td>
				   <td class='right'>".$result['Skucost']."</td>
				   <td class='right'>".$result['Subtotal']."</td>
				   <td class='right'>".$result['Gst']."</td>
				   <td class='right'>".$result['Nettotal']."</td>
			</tr>";	
		}
		$total = number_format($total,2);
		$str.="<tr><td colspan='7' class='right'>Total</td><td class='right'>".$total."</td></tr>";
		$str.="</tbody></table>";
		if($str){
			$filename = 'porequestDetails_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function export_porequest_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}


		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}

		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		
		if($filter_supplier) {
			$filterStr.=" Supplier: ".$filter_supplier.',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location_code'	  => $filter_location_code
		);
		// $results= $this->model_inventory_inventory->getProductstock($data);
		$results = $this->model_transaction_purchase->getPOrequestList($data);
		// printArray($results);die;

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

		$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">PO Request Report</td>
				<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';

		if($filterStr){
			$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
		}

		$str.="<table class='listing'><thead>
			<tr>
             <th>S No</th>
             <th>Tran No</th>
             <th>Tran Date</th>
             <th>Location</th>
             <th>Supplier</th>
             <th>Subtotal</th>
             <th>GST</th>
             <th>Net Total</th>
			</tr></thead> <tbody>"; 
		$j=0;
		$total_Stock_Value = 0;
		$total = 0;
		foreach ($results as $result) {
			$j++;
			$subTotal = $result['Subtotal'];
			$gst 	  = $result['Gst'];
			$netTotal = $result['Nettotal'];		
			$total +=$netTotal; 

			$transaction_no   = $result['PO_Req_No'];
			$transaction_date = date('d/m/Y',strtotime($result['PO_Req_Date']));
			$location_code    = $result['location_code'];
			$vendor_name      = $result['vendor_name'];

			$str.="<tr>
					<td>".$j."</td>
				   <td>".$transaction_no."</td>
				   <td>".$transaction_date."</td>
				   <td>".$location_code."</td>
				   <td>".$vendor_name."</td>
				   <td class='right'>".$subTotal."</td>
				   <td class='right'>".$gst."</td>
				   <td class='right'>".$netTotal."</td>
			</tr>";	
		}
		$total = number_format($total,2);
		$str.="<tr><td></td><td></td><td></td><td></td><td></td><td></td><td>Total</td>
		<td class='right'>".$total."</td></tr>";
		$str.="</tbody></table>";
		if($str){
			$filename = 'porequest_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}
	public function profitSummary() {
		$this->language->load('inventory/reports');
	//	$this->load->model('inventory/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Profit Summary Report Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Profit Summary Report');
			$this->getprofitSummarydetailList();
		}
		/*$this->data['printAction'] = $this->url->link('inventory/profitsummary/exportpdfprofitsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('inventory/profitsummary/salesdetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');*/
	}

	protected function getprofitSummarydetailList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Department Sales Detail';
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';

		if ($_REQUEST['filter_date_from']=='' && $_REQUEST['filter_date_to']=='') {
			$_REQUEST['filter_date_from'] = date('d/m/Y');
			$_REQUEST['filter_date_to'] = date('d/m/Y');
		}
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filter_date_to = null;
		}


		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location;			 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location;	
		}

		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}

		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$pageUrl.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}

		
		
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page=1;
		}
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal,
			'filter_category' => $filter_category,
			'filter_department'=>$filter_department
		);
		$this->data['data'] = $data;
		$report_totalAry =  $this->model_inventory_reports->getprofitsummaryDetailsCount2($data);
		// printArray($report_totalAry);
		$report_total    = count($report_totalAry);
		$asTot=0;
		$csTot=0;
		$profitTot=0;
		foreach ($report_totalAry as $value) {
				$asTot+= $value['Actual_sales'];
				$csTot+= $value['CostOfSales'];
				$profitTot+= $value['Profit'];
		}
		$this->data['total'] = array("Actual_salesTotal"=>number_format($asTot,2),"CostOfSalesTot"=>number_format($csTot,2),
			"ProfitTot"=>number_format($profitTot,2));
		$results = $this->model_inventory_reports->getprofitsummaryDetails($data);
		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_profitsummarydetail_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_profitsummarydetail_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		foreach ($results as $result) {	
			$this->data['sales'][] = array(
				'salesmonth'            => $result['salesmonth'],
				'department'          	=> $result['department_name'],
				'category'          	=> $result['category_name'],
				'actualsales'     		=> number_format($result['Actual_sales'],2),
				'costOfsales'			=> number_format($result['CostOfSales'],2),			
				'profit'			  	=> number_format($result['Profit'],2)			
			);
		}
		$pagination = new Pagination();
		$pagination->total = $report_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/profitSummary', 'token=' . $this->session->data['token'] . $pageUrl . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();

		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();

		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->template = 'inventory/profit_summary_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_profitsummarydetail_csv()
	{
		$this->load->model('inventory/reports');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];			
		}

		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
		} else {
			$filter_category = '';
		}
		
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filterVal' =>$filterVal,
			'filter_category' =>$filter_category,
			'filter_department'=>$filter_department
		);

		$results = $this->model_inventory_reports->getprofitsummaryDetails($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=profitSummarydetail.csv');
	        print "Sales Month, Department, Actual Sales, Cost Of Sales, Profit\r\n";
			$j=0; 
			$actualTotal=0;
			$costTotal=0;
			$profitTotal=0;

			foreach ($results as $result) {	
				
					$salesmonth     = $result['salesmonth'];
					$department     = $result['department_name'];
					$actualsales    = $result['Actual_sales'];
					$costOfsales	= $result['CostOfSales'];			
					$profit			= $result['Profit'];
					$actualTotal+=$actualsales;
					$costTotal+=$costOfsales;
					$profitTotal+=$profit;

				print "\"$salesmonth\",\"$department\",\"$actualsales\",\"$costOfsales\",\"$profit\"\r\n";
			}
			
			print "\"\",\"Total\",\"$actualTotal\",\"$costTotal\",\"$profitTotal\"\r\n";
		}  
	}
	public function export_profitsummarydetail_pdf()
	{
		
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department');
		$this->load->model('inventory/category');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
	
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];			
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		
		if($filter_department) {
			$departmentResults=$this->model_inventory_department->getDepartmentName($filter_department);
			$filterStr.=" Department: ".$departmentResults['department_name'].',';
		}
		if($filter_category) { 
			$categoryResults=$this->model_inventory_category->getCategoryNameByCode($filter_category);
			$filterStr.=" Category: ".$categoryResults['category_name'].',';
		}
        
        if($_REQUEST['filter_location']) {
			$filterStr.=" Location: ".$_REQUEST['filter_location'].',';
		}
		
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
        
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department,
			'filter_category' =>$filter_category
		);

		$results = $this->model_inventory_reports->getprofitsummaryDetails($data);

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Profit Summary Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Sales Month</th>
							<th>Department</th>
							<th>Actual Sales</th>
							<th>Cost of Sales</th>							
							<th>Profit</th>							
						</tr></thead> <tbody>"; 

	       
			$actualTotal=0;
			$costTotal=0;
			$profitTotal=0;
			foreach ($results as $result) {	
					$salesmonth     = $result['salesmonth'];
					$department     = $result['department_name'];
					$actualsales    = $result['Actual_sales'];
					$costOfsales	= $result['CostOfSales'];			
					$profit			= $result['Profit'];
					$actualTotal+=$actualsales;
					$costTotal+=$costOfsales;
					$profitTotal+=$profit;	
		
				$str.="<tr>
				   <td>".$salesmonth."</td>
				   <td>".$department."</td>
				   <td class='right'>".number_format($actualsales,2)."</td>
				   <td class='right'>".number_format($costOfsales,2)."</td>
				   <td class='right'>".number_format($profit,2)."</td>
			</tr>";	
			}
			$str.="<tr><td colspan='2' align='right'>Total</td><td  align='right'>".number_format($actualTotal,2)."</td><td align='right'>".number_format($costTotal,2)."</td><td class='right'>".number_format($profitTotal,2)."</td></tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'Profit_Summary_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}

	public function profitDetails() {
		$this->language->load('inventory/reports');
	//	$this->load->model('inventory/sales');
		$type = $this->request->get['type'];
		$print = $this->request->get['print'];
		if($print == 'printsummary'){
			$this->document->setTitle('Profit Details Report Print');
			$this->printsummary();
		} else {
			$this->document->setTitle('Profit Details Report');
			$this->getprofitdetailList();
		}
		/*$this->data['printAction'] = $this->url->link('inventory/profitDetails/exportpdfprofitsummary&print=printsummary'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports/salesdetail&export=yes'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');*/
	}

	protected function getprofitdetailList() {		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = 'Department Sales Detail';
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';

		if ($_REQUEST['filter_date_from']=='' && $_REQUEST['filter_date_to']=='') {
			$_REQUEST['filter_date_from'] = date('d/m/Y');
			$_REQUEST['filter_date_to'] = date('d/m/Y');
		}
		
		if (isset($_REQUEST['filter_date_from']) && $_REQUEST['filter_date_from']!='') {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
			$filterVal = 1;
		} else {
			$filter_date_from = null;
		}
		if (isset($_REQUEST['filter_date_to']) && $_REQUEST['filter_date_to']!='') {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
			$filterVal = 1;
		} else {
			$filter_date_to = null;
		}


		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$pageUrl.='&filter_location='.$filter_location;			 
		} else {
			$filter_location = $this->session->data['location_code'];
			$pageUrl.='&filter_location='.$filter_location;	
		}

		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$pageUrl.='&filter_department='.$filter_department; 	
			$filterVal = 1;		
		} else {
			$filter_department = null;
		}
		
		
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('report/report', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['sales'] = array();
		$page = $_REQUEST['page'];
		if($_POST){
			$page=1;
		}
		$data = array(
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_location'=>$filter_location,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department
		);
		$this->data['data'] = $data;
	    $report_totalAry =  $this->model_inventory_reports->getprofitDetailsCount($data);
	    $report_total = count($report_totalAry);

	    foreach ($report_totalAry as $value) {
		    $actualTot+= $value['Actual_sales'];
			$costTot+= $value['CostOfSales'];
			$profitTot+= $value['Profit'];
	    }
	    $this->data['total'] = array("Actual_salesTotal"=>number_format($actualTot,2),"CostOfSalesTot"=>number_format($costTot,2),
			"ProfitTot"=>number_format($profitTot,2));

		
		$results = $this->model_inventory_reports->getprofitDetails($data);
		$this->load->model('user/user');
		$userDetail = array();
		$gst = 0;
		$strUrl = '&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_profitdetail_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_profitdetail_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$actualTot=0;
		$costTot=0;
		$profitTot=0;
		foreach ($results as $result) {	
			$this->data['sales'][] = array(
				'invoicedate'            => date('d/m/Y',strtotime($result['Invoice_Date'])),
				'department'          	 => $result['department_name'],
				'sku'          			 => $result['sku'],
				'sku_description'        => $result['sku_description'],
				'actualsales'     		 => number_format($result['Actual_sales'],2),
				'costOfsales'			 => number_format($result['CostOfSales'],2),			
				'profit'			  	 => number_format($result['Profit'],2)			
			);
		}
		
		$pagination = new Pagination();
		$pagination->total = $report_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/profitDetails', 'token=' . $this->session->data['token'] . $pageUrl . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();

		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		$this->data['Tolocations'] =  $this->cart->getLocation($this->session->data['location_code']);
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	     //print_r($data); exit;
		$this->template = 'inventory/profit_details_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_profitdetail_csv()
	{
		$this->load->model('inventory/reports');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
		
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];			
		}
		
		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department
		);

		$results = $this->model_inventory_reports->getprofitDetails($data);
		if(count($results>=1)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=profitdetail.csv');
	        print "Invoice Date, Sku, Sku Description, Department, Actual Sales, Cost Of Sales, Profit\r\n";
			$j=0; 
			$actualTotal=0;
			$costTotal=0;
			$profitTotal=0;

			foreach ($results as $result) {	
					$invoicedate     = date('d/m/Y',strtotime($result['Invoice_Date']));
					$sku     		 = $result['sku'];
					$sku_description = $result['sku_description'];
					$department     = $result['department_name'];
					$actualsales    = $result['Actual_sales'];
					$costOfsales	= $result['CostOfSales'];			
					$profit			= $result['Profit'];
					$actualTotal+=$actualsales;
					$costTotal+=$costOfsales;
					$profitTotal+=$profit;


				print "\"$invoicedate\",\"$sku\",\"$sku_description\",\"$department\",\"$actualsales\",\"$costOfsales\",\"$profit\"\r\n";
			}
			
			print "\"\",\"\",\"\",\"Total\",\"$actualTotal\",\"$costTotal\",\"$profitTotal\"\r\n";
		}  
	}
	public function export_profitdetail_pdf()
	{
		
		$this->load->model('inventory/reports');
		$this->load->model('inventory/department');

		if (isset($this->request->get['filter_date_from'])) {
			$filter_date_from = $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$filter_date_to = $this->request->get['filter_date_to'];
		}
	
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];			
		}
		
		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		
		if($filter_department) {
			$departmentResults = $this->model_inventory_department->getDepartmentName($filter_department);
			$filterStr.=" Department: ".$departmentResults['department_name'].',';
		}

		
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}

		$data = array(
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filterVal' =>$filterVal,
			'filter_department'=>$filter_department
		);

		$results = $this->model_inventory_reports->getprofitDetails($data);

	    if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Profit Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
			$str.="<table class='listing' style='width:100%;'><thead>
						<tr>
							<th>Invoice Date</th>
							<th>Sku</th>
							<th>Sku Description</th>
							<th>Department</th>
							<th>Actual Sales</th>
							<th>Cost of Sales</th>							
							<th>Profit</th>							
						</tr></thead> <tbody>"; 

	       
			$actualTotal=0;
			$costTotal=0;
			$profitTotal=0;
			foreach ($results as $result) {	
					$invoicedate     = date('d/m/Y',strtotime($result['Invoice_Date']));
					$sku     		 = $result['sku'];
					$sku_description = $result['sku_description'];
					$department     = $result['department_name'];
					$actualsales    = number_format($result['Actual_sales'],2);
					$costOfsales	= number_format($result['CostOfSales'],2);			
					$profit			= number_format($result['Profit'],2);

					$actualTotal+=$actualsales;
					$costTotal+=$costOfsales;
					$profitTotal+=$profit;	
		
				$str.="<tr>
				   <td>".$invoicedate."</td>
				   <td>".$sku."</td>
				   <td>".$sku_description."</td>
				   <td>".$department."</td>
				   <td class='right'>".$actualsales."</td>
				   <td class='right'>".$costOfsales."</td>
				   <td class='right'>".$profit."</td>
			</tr>";	
			}
			$str.="<tr><td colspan='4' align='right'>Total</td><td  align='right'>".number_format($actualTotal,2)."</td><td  align='right'>".number_format($costTotal,2)."</td><td class='right'>".number_format($profitTotal,2)."</td></tr>";

		$str.="</tbody></table>";
		if($str){
			$filename = 'Profit_details_Report'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
		}  
	}


	public function purchase_comparison_details() {
		
		$this->load->model('transaction/purchase');
		$this->document->setTitle('Purchase Comparison Details Report');
		
		//language assign code for transaction purchase
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_tran_no']   = $this->language->get('text_tran_no');
		$this->data['text_tran_dt'] = $this->language->get('text_tran_dt');
		$this->data['text_supplier'] = $this->language->get('text_supplier');
		$this->data['text_total'] = $this->language->get('text_total');
		$this->data['button_show_hold'] = $this->language->get('button_show_hold');
		$this->data['button_back'] = $this->language->get('button_back');
		$this->data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->load->model('inventory/reports');
		$company_id	= $this->session->data['company_id'];
		$stock_report = $this->request->get['stock_report'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		
		$pageUrl ='';
		

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			//$filter_date_from = '';
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}

		if ($_REQUEST['filter_location_code']) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}


		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			//$filter_date_to = '';
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}

		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno;
		
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}
		if (isset($_GET['filter_transactionno']) && !$_POST) {
			$filter_date_from='';
			$filter_date_to='';
			$pageUrl.= '&filter_date_from=';
            $pageUrl.= '&filter_date_to='; 
		}
	
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);		
		
		
		$hold_button = '&show_hold=1';
		$this->data['back_button'] = $this->url->link('transaction/purchase', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['purchases'] = array();
		$page = $_REQUEST['page'];
		$data = array(
			'show_hold'	  => $show_hold,
			'start'       => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'       => $this->config->get('config_admin_limit'),
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		//printArray($data); exit;
		$this->data['data'] = $data;
		$purchase_grand_total = $this->model_transaction_purchase->getTotalPurchaseInvoice($data);
		
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = $purchase_grand_total['grandtotal'];
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);

		$this->data['vendors'] = $this->model_transaction_purchase->getVendors();
		$this->load->model('master/vendor');
		$this->load->model('user/user');
		$vendorDetail = array();
		$userDetail = array();
		$gst = 0;
	
		$strUrl = '&filter_supplier='.$filter_supplier.'&filter_transactionno='.$filter_transactionno.'&filter_date_from='.$filter_date_from.'&filter_date_to='.$filter_date_to;

		$this->data['printAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&print=printinvoice'.$strUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['exportAction'] = $this->url->link('transaction/transaction_reports&type=purchase_invoice&export=yes'.$pageUrl, 'token=' . $this->session->data['token'] . $url, 'SSL');
		$test = 0; 
		$grandtotal = 0;
		$netTotal =0;
		foreach ($results as $result) {	
			$grandtotal+=$netTotal; 
			$itemDetails = $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($result['transaction_no']);

			$this->data['purchaseHeader'][] = array(
				'purchase_id'       => $result['purchase_id'],
				'transaction_no'    => $result['transaction_no'],
				'location_code'     => $result['location_code'],
				'transaction_date'  => date('d/m/Y',strtotime($result['transaction_date'])),
				'transaction_type'  => $result['transaction_type'],
				'vendor_code'       => $result['vendor_code'],
				'reference_no'      => $result['reference_no'],
				'reference_date'    => date('d/m/Y',strtotime($result['reference_date'])),
				'vendor_name'       => $result['vendor_name'],
				'sub_total'         => $result['sub_total'],
				'gst'				=> $result['gst'],
				'total'             => $result['total'],
				'created_by'        => $result['firstname'].' '.$result['lastname'],
				'created_date'      => $result['created_date'],
				'salesDetails'      => $itemDetails
				
			);
		}
		
		$url = '';
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$url.= $pageUrl;
		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$pagination = new Pagination();
		$pagination->total = $purchase_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/purchase_details', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['link_csvexport'] = $this->url->link('inventory/reports/export_purchase_comparsion_csv', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_purchase_comparsion_pdf', 'token=' . $this->session->data['token'] . '&'.$pageUrl, 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		$this->template = 'transaction/purchase_comparison_detail_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function export_purchase_comparsion_csv()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock Report';
		$url = '';		
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} 


		$page = $_REQUEST['page'];

		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);

		$tot_quantity = 0;
		$tot_value = 0;

		$salesHeader =  $this->model_transaction_purchase->getPurchaseInvoiceList($data);

	    if(count($salesHeader>=1)){
			foreach ($salesHeader as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$results= $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($value['transaction_no']);
		    	ob_end_clean();
		        header( 'Content-Type: text/csv' );
		        header( 'Content-Disposition: attachment;filename=purchaseComparisonDetailReport.csv');
		        print "\"$invoice_no\"\r\n";
		        print "S.No,SKU,Name,Reference No,Reference Date,PO Qty,Received Qty,Balance Qty\r\n";
				$j=0; 
				$actualtotal=$tot_subtotal=$tot_gst=$tot_discount=0;
				foreach ($results as $result) {	
					$j++;

					$sku       			= $result['sku_title'];
					$description        = $result['name'];
					$refno 		        = $value['reference_no'];
					$refdate 		    = $value['reference_date'];
					$org_qty    	    = $result['original_qty'];
					$qty    	        = $result['quantity'];
					$balance			= $result['original_qty'] - $result['quantity']; 

			
					print "$j,\"$sku\",\"$description\",\"$refno\",\"$refdate\",\"$org_qty\",\"$qty\",\"$balance\"\r\n";
				}
			}
		}   
	}

	public function export_purchase_comparsion_pdf()
	{
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_supplier'])) {
			$filter_supplier = $_REQUEST['filter_supplier'];
			$pageUrl.= '&filter_supplier='.$filter_supplier;
		} else {
			$filter_supplier = null;
		}
		if (isset($_REQUEST['filter_transactionno'])) {
			$filter_transactionno = $_REQUEST['filter_transactionno'];
			$pageUrl.='&filter_transactionno='.$filter_transactionno; 
		} else {
			$filter_transactionno = null;
		}
		if (isset($_REQUEST['filter_reference_date'])) {
			$filter_reference_date = $_REQUEST['filter_reference_date'];
			$pageUrl.='&filter_reference_date='.$filter_reference_date; 
		} else {
			$filter_reference_date = '';
		}

		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}


		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_supplier) {
			$supplierDetails = $this->model_master_vendor->getSuppliersDetailsbyid($filter_supplier);
			$filterStr.=" Supplier: ".$supplierDetails['vendor_name'].',';
		}
		if($filter_transactionno) {
			$filterStr.=" Transaction No: ".$filter_transactionno.',';
		}

		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}

		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_supplier'=>$filter_supplier,
			'filter_transactionno'=>$filter_transactionno,
			'filter_date_from'=>$filter_date_from,
			'filter_date_to'=>$filter_date_to,
			'filter_reference_date'=>$filter_reference_date,
			'filter_location_code'	  => $filter_location_code
		);
		$results = $this->model_transaction_purchase->getPurchaseInvoiceList($data);

		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Purchase Comparison Details Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$invoice_no = $value['transaction_no'];
				$invoice_date = date('d/m/Y',strtotime($value['transaction_date']));
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='4' class='left' style='border-right:none;'>Invoice No:".$invoice_no."	</th>
						<th colspan='3' align='right' style='border-left:none;'>Invoice Date:".$invoice_date."</th></tr>
						<tr>
							<th>SKU</th>
							<th>Name</th>
							<th>Reference No</th>
							<th>Reference Date</th>
							<th>PO Qty</th>
							<th>Received Qty</th>
							<th>Balance Qty</th>
						</tr></thead><tbody>";
				$results= $this->model_transaction_purchase->getpurchaseinvoiceDetailsbyInvoice($invoice_no);
		    	
				$j=0; 
				foreach ($results as $result) {	
					$j++;
					$sku       			= $result['sku_title'];
					$description        = $result['name'];
					$refno       		= $value['reference_no'];
					$refdate       		= $value['reference_date'];
					$org_qty  	        = $result['original_qty'];
					$qty    	        = $result['quantity'];
					$balance   	        = $result['original_qty'] - $result['quantity'];

					
					$str.="<tr>
							   <td>".$sku."</td>
							   <td>".$description."</td>
							   <td>".$refno."</td>
							   <td>".$refdate."</td>
							   <td>".$org_qty."</td>
							   <td>".$qty."</td>
							   <td>".$balance."</td>
						</tr>";	
				
				}
				$str.="<tr><td colspan='7' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
		if($str){
			$filename = 'purchase_comparison_details_Report_'.date('dmyHis').'.pdf';
			include(DIR_SERVER.'pdf_print.php');
		}
	}

	public function minimum_quantity_report(){
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->document->setTitle('Minimum Quantity Report');
		
		$company_id		= $this->session->data['company_id'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$url ='';
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $this->session->data['location_code'];
			$url.= '&filter_location='.$filter_location;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$url.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}

		$filter_product_id = '';
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url.= '&filter_product_id='.$filter_product_id;
		}
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$url.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$url.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		}

		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$url.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}

		$filter_name = '';
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$url.= '&filter_name='.$filter_name;
		}
		if(isset($this->request->post['page'])) {
			$page = 1;
		}
		else if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data = array(
			'start'               => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'               => $this->config->get('config_admin_limit'),
			'filter_location'     => $filter_location,
			'filter_department'   => $filter_department,
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_vendor'	      => $filter_vendor,
			'filter_name'	      => $filter_name,
			'filter_product_id'   => $filter_product_id,
			'page'			      => $page
		); 
        
		$this->data['data']		  = $data;
		// Category
		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();
		$this->data['locations']  = $this->cart->getLocation();
		$this->data['departments']= $this->model_inventory_department->getAllDepartments();

		$total_products			  = $this->model_inventory_inventory->getMinimumQuantityReportCount($data);
		$tocken 				  = $this->session->data['token'];

		$products	  = $this->model_inventory_inventory->getMinimumQuantityReports($data);
		foreach ($products as $product) {
			$resQty = $this->model_inventory_inventory->getReservedQtyByProduct($product['product_id']);
			$product['reservedQty']   = $product['RESERVED_QTY'] - $resQty;
			$product['availableQty']   = $product['sku_qty'] - $product['reservedQty'];
			$this->data['products'][] = $product;
		}
		$this->data['back'] 	  = $this->url->link('report/report','token='.$tocken,'SSL');
		$this->data['export_csv'] = $this->url->link('inventory/reports/minimum_quantity_report_csv','token='.$tocken.$url,'SSL');
		$this->data['export_pdf'] = $this->url->link('inventory/reports/minimum_quantity_report_pdf','token='.$tocken.$url,'SSL');

		$this->data['breadcrumbs'][] = array(
			'text'     => $this->language->get('text_home'),
			'href'     => $this->url->link('common/home', 'token=' . $tocken, 'SSL'),
			'separator'=> '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'     => 'Reports',
			'href'     => $this->url->link('report/report','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text' => 'Minimum Quantity Report',
			'href' => $this->url->link('inventory/reports/minimum_quantity_report','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);

		$pagination 		= new Pagination();
		$pagination->total 	= $total_products;
		$pagination->page 	= $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('inventory/reports/minimum_quantity_report', 'token=' . $tocken. $url. '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['token']= $this->session->data['token'];

		$this->template 	= 'inventory/minimum_quantity_report.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function minimum_quantity_report_csv(){
		
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department'); 
		$url ='';

		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $this->session->data['location_code'];
			$url.= '&filter_location='.$filter_location;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$url.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		$filter_product_id = '';
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url.= '&filter_product_id='.$filter_product_id;
		}
		$filter_name = '';
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$url.= '&filter_name='.$filter_name;
		}
		if (isset($_REQUEST['filter_category'])) {
			$filter_category = $_REQUEST['filter_category'];
			$url.= '&filter_category='.$filter_category;
		} else {
			$filter_category = '';
		}

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
			$url.= '&filter_brand='.$filter_brand;
		} else {
			$filter_brand = '';
		}

		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$url.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		
		$data = array(
			'filter_location'   => $filter_location,
			'filter_department' => $filter_department,
			'filter_department'   => $filter_department,
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_vendor'	      => $filter_vendor,
			'filter_name'		=> $filter_name,
			'filter_product_id'	=> $filter_product_id,
			'page'				=> $page
		);
		$products	  = $this->model_inventory_inventory->getMinimumQuantityReports($data);

		if(!empty($products)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=MinimumQuantityReport.csv');
	       // print "S.No,Location,Name,SKU,Department,Aval Stock,Min Stock\r\n";
	        print "S.No,Product ID,SKU,Name,Actual Stock,Reserved Stock,Available Stock,Minimum Stock\r\n";
			$j=0; 
			foreach ($products as $product) {
				$j++;
				$resQty = $this->model_inventory_inventory->getReservedQtyByProduct($product['product_id']);
				$reservedQty  = $product['RESERVED_QTY'] - $resQty;
				$availableQty = $product['sku_qty'] - $product['reservedQty'];
				$product_id 	 = $product['product_id'];
				$location 	 = $product['location_Code'];
				$name     	 = $product['name'];
				$sku      	 = $product['sku'];
				$description = '';//$product['sku_shortdescription'];
				$department  = $product['department_name'];
				$sku_qty  	 = $product['sku_qty'];
				$sku_min   	 = $product['sku_min'];
				
				print "$j,\"$product_id\",\"$sku\",\"$name\",\"$sku_qty\",\"$reservedQty\",\"$availableQty\",\"$sku_min\"\r\n";
				//print "$j,\"$location\",\"$name\",\"$sku\",\"$department\",\"$sku_qty\",\"$sku_min\"\r\n";
			}
		}  
	}

	public function minimum_quantity_report_pdf(){
		
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');

		$url ='';
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $this->session->data['location_code'];
			$url.= '&filter_location='.$filter_location;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$url.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		$filter_product_id = '';
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url.= '&filter_product_id='.$filter_product_id;
		}
		$filter_name = '';
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$url.= '&filter_name='.$filter_name;
		}
        if (isset($_REQUEST['filter_category'])) {
            $filter_category = $_REQUEST['filter_category'];
            $url.= '&filter_category='.$filter_category;
        } else {
            $filter_category = '';
        }
        if (isset($_REQUEST['filter_brand'])) {
            $filter_brand = $_REQUEST['filter_brand'];
            $url.= '&filter_brand='.$filter_brand;
        } else {
            $filter_brand = '';
        }
        if (isset($_REQUEST['filter_vendor'])) {
            $filter_vendor = $_REQUEST['filter_vendor'];
            $url.= '&filter_vendor='.$filter_vendor;
        } else {
            $filter_vendor = '';
        }

		$data = array(
			'filter_location'   => $filter_location,
			'filter_department' => $filter_department,
			'filter_name'		=> $filter_name,
            'filter_category'     => $filter_category,
            'filter_brand'        => $filter_brand,
            'filter_vendor'       => $filter_vendor,
			'filter_product_id'	=> $filter_product_id 
		); 

		$products	  = $this->model_inventory_inventory->getMinimumQuantityReports($data);

		if(!empty($products)){

	    	$company_id		 = $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
			$str 			 = $headerStr;
			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Minimum Stock Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}
	        $str.="<table class='listing'><thead>
	           		<tr>
		           		<th>S.No</th>
		           		<th>Product ID</th>
		           		<th>SKU</th>
		           		<th>Name</th>
		           		<th>Actual Stock</th>
		           		<th>Reserved Stock</th>
		           		<th>Available Stock</th>
		           		<th>Minimum Stock</th>
	           		</tr>
           		  </thead><tbody>";
			$j=0; 
			foreach ($products as $product) {
				$j++;
				$resQty=$this->model_inventory_inventory->getReservedQtyByProduct($product['product_id']);
				$reservedQty  = $product['RESERVED_QTY'] - $resQty;
				$availableQty = $product['sku_qty'] - $product['reservedQty'];
				$product_id   = $product['product_id'];
				$location     = $product['location_Code'];
				$name         = $product['name'];
				$sku          = $product['sku'];
				$description  = $product['sku_shortdescription'];
				$department   = $product['department_name'];
				$sku_qty      = $product['sku_qty'];
				$sku_min      = $product['sku_min'];
				
				$str.="<tr>
						<td>".$j."</td>
				   		<td>".$product_id."</td>
				   		<td>".$sku."</td>
				   		<td>".$name."</td>
				   		<td align='center'>".$sku_qty."</td>
				   		<td align='center'>".$reservedQty."</td>
				   		<td align='center'>".$availableQty."</td>
				   	    <td align='center'>".$sku_min."</td>
					</tr>";	
			}
			$str.="</tbody></table>"; 
			if($str){
				$filename = 'MinimumStockReport-'.date('m-Y').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}
		}
	} 

	public function stock_valuation_with_reserved_stock(){
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/vendor');
		$this->document->setTitle('Stock Valuation With Reserved Stock');
		

		$this->data['vendors'] = $this->model_master_vendor->getVendors();
		$company_id		     = $this->session->data['company_id'];
		$company_details	= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo		= $this->url->getCompanyAddress($company_details);

		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}
		$url ='';
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $this->session->data['location_code'];
			$url.= '&filter_location='.$filter_location;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$url.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		$filter_product_id = '';
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url.= '&filter_product_id='.$filter_product_id;
		}
		$filter_name = '';
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$url.= '&filter_name='.urlencode($filter_name);
		}
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
			$url.= '&filter_vendor='.$filter_vendor;
		} else {
			$filter_vendor = '';
		}
		if(isset($this->request->post['page'])) {
			$page = 1;
		}
		else if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$data = array(
			'start'             => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'             => $this->config->get('config_admin_limit'),
			'filter_location'   => $filter_location,
			'filter_department' => $filter_department,
			'filter_name'	     => $filter_name,
			'filter_product_id' => $filter_product_id,
			'filter_vendor'     => $filter_vendor,
			'page'  			=> $page
		);
		
		$this->data['data']		 = $data;
		$this->data['locations']  = $this->cart->getLocation();
		$this->data['departments']= $this->model_inventory_department->getAllDepartments();

		$total_products 	= $this->model_inventory_inventory->getStockCount($data);
		$tocken   		= $this->session->data['token'];
		$products 		= $this->model_inventory_inventory->getStockProduts($data,$_REQUEST['export']);
		$this->data['back'] = $this->url->link('report/report','token='.$tocken,'SSL');

		if(!empty($products)){
			$csv = '&export=csv';
			$pdf = '&export=pdf';
		} else {
			$csv = '';
			$pdf = '';
		}
		
		foreach ($products as $product) {
			if($product['package']!='1'){
				$product['NormalreservedQty']   = $this->model_inventory_inventory->getResevedStockforNormalItems($product['product_id'],$filter_location); 
                $doQty                          = $this->model_inventory_inventory->getPartiallyReservedStock($product['product_id'],$filter_location);
                $product['NormalreservedQty']   = $product['NormalreservedQty'] + $doQty;
				$product['ChildreservedQty']    = $this->model_inventory_inventory->getResevedStockforpackageItems($product['product_id'],$filter_location);
				$product['reservedQty']         = $product['NormalreservedQty'] + $product['ChildreservedQty'];
			}else{
				$product['reservedQty']         = 0;
			}			
			$product['availableQty']  = $product['sku_qty'] -$product['reservedQty'];
			$this->data['products'][] = $product;
		}

		$this->data['export_csv'] = $this->url->link('inventory/reports/stock_valuation_with_reserved_stock','token='.$tocken.$url.$csv,'SSL');
		$this->data['export_pdf'] = $this->url->link('inventory/reports/stock_valuation_with_reserved_stock','token='.$tocken.$url.$pdf,'SSL');
		$this->data['breadcrumbs'][] = array(
			'text'     => $this->language->get('text_home'),
			'href'     => $this->url->link('common/home', 'token=' . $tocken, 'SSL'),
			'separator'=> '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'     => 'Reports',
			'href'     => $this->url->link('report/report','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'     => 'Stock Valuation With Reserved Stock Report',
			'href'     => $this->url->link('inventory/reports/stock_valuation_with_reserved_stock','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);

		if($_REQUEST['export'] == 'csv' || $_REQUEST['export'] == 'pdf'){
			if($_REQUEST['export'] == 'csv'){
				$products = $this->data['products'];
				if(!empty($products)){
			    	ob_end_clean();
			        header( 'Content-Type: text/csv' );
			        header( 'Content-Disposition: attachment;filename=stock_valuation_with_reserved_stock.csv');
			        print "S.No,Sku,Name,Actual Stock,Reserved Stock,Available\r\n";
					$j=0; 
					foreach ($products as $product) {
						$j++;
						$invoice_no 	 = $product['sku'];
						$name     	 = $product['name'];
						$actualStock    = $product['sku_qty'];
						$reservedStock  = $product['reservedQty'];
						$aval  	 	 = $product['availableQty'];
						print "$j,\"$invoice_no\",\"$name\",\"$actualStock\",\"$reservedStock\",\"$aval\"\r\n";
					}
				}  die;
			}
			if($_REQUEST['export'] == 'pdf'){
				$products = $this->data['products'];
				if(!empty($products)){
			    	$company_id		 = $this->session->data['company_id'];
					$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
					$headerStr	 = $this->url->getCompanyAddressHeaderString($company_details);
					$str 		 = $headerStr;
					$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Valuation With Reserved Stock</td>
							<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
					if($filterStr){
						$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
					}
			        $str.="<table class='listing' style='width:100%;'><thead>
			           		<tr>
				           		<th>S.No</th>
				           		<th>SKU</th>
				           		<th>Name</th>
				           		<th>Actual Stock</th>
				           		<th>Reserved Stock</th>
				           		<th>Available</th>
			           		</tr>
		           		  </thead><tbody>";
					$j=0; 
					foreach ($products as $product) {
						$j++;
						$invoice_no 	= $product['sku'];
						$name     	= $product['name'];
						$actualStock   = $product['sku_qty'];
						$reservedStock = $product['reservedQty'];
						$aval  		= $product['availableQty'];
						
						$str.="<tr>
								<td>".$j."</td>
								<td>".$invoice_no."</td>
						   		<td>".$name."</td>
						   		<td>".$actualStock."</td>
						   		<td>".$reservedStock."</td>
						   		<td>".$aval."</td>
							</tr>";	
					}
					$str.="</tbody></table>";
					if($str){
						$filename = 'stock_valuation_with_reserved_stock-'.date('m-Y').'.pdf';
						include(DIR_SERVER.'pdf_print.php');
					}
				}die;
			}
		}

		$pagination 		= new Pagination();
		$pagination->total 	= $total_products;
		$pagination->page 	= $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('inventory/reports/stock_valuation_with_reserved_stock', 'token=' . $tocken. $url. '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['token']= $this->session->data['token'];
		$this->template 	= 'inventory/stock_valuation_with_reserved_stock_report.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function stock_valuation_with_reserved_stock_and_value(){
		
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->document->setTitle('Stock Valuation With Reserved Stock');
		
		$company_id		= $this->session->data['company_id'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);

		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}

		$url ='';
		if (isset($_REQUEST['filter_location'])) {
			$filter_location = $_REQUEST['filter_location'];
			$url.= '&filter_location='.$filter_location;
		} else {
			$filter_location = $this->session->data['location_code'];
			$url.= '&filter_location='.$filter_location;
		}
		if (isset($_REQUEST['filter_department'])) {
			$filter_department = $_REQUEST['filter_department'];
			$url.= '&filter_department='.$filter_department;
		} else {
			$filter_department = '';
		}
		$filter_product_id = '';
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
			$url.= '&filter_product_id='.$filter_product_id;
		}
		$filter_name = '';
		if (isset($_REQUEST['filter_name'])) {
			$filter_name = $_REQUEST['filter_name'];
			$url.= '&filter_name='.$filter_name;
		}
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$url.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}

		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$url.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
		}
		if(isset($this->request->post['page'])) {
			$page = 1;
		}
		else if(isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$data = array(
			'start'             => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'             => $this->config->get('config_admin_limit'),
			'filter_location'   => $filter_location,
			'filter_department' => $filter_department,
			'filter_name'	    => $filter_name,
			'filter_product_id' => $filter_product_id,
			'filter_date_from'	=> $filter_date_from,
			'filter_date_to'	=> $filter_date_to,
			'page'			    => $page
		);

		$this->data['data']		  = $data;
		$this->data['locations']  = $this->cart->getLocation();
		$this->data['departments']= $this->model_inventory_department->getAllDepartments();
		$total_products			  = $this->model_inventory_inventory->getstock_valuation_with_reserved_stock_ReportCount($data);
		$tocken 				  = $this->session->data['token'];
		$this->data['products']	  = $this->model_inventory_inventory->getgetstock_valuation_with_reserved_stock_Reports($data,$_REQUEST['export']);

		/*$purchase_grand_total = $this->model_inventory_inventory->getgetstock_valuation_with_reserved_stock_Reports_total($data);
		// printArray($purchase_grand_total); die;
		$purchase_total = $purchase_grand_total['totalPurchase'];
		$this->data['products_sum']['grandtotal'] = number_format($purchase_grand_total['grandtotal'],2);*/

		$this->data['back'] 	  = $this->url->link('report/report','token='.$tocken,'SSL');

		if(!empty($this->data['products'])){
			$csv= '&export=csv';
			$pdf = '&export=pdf';
		} else {
			$csv= '';
			$pdf = '';
		}
		$this->data['export_csv'] = $this->url->link('inventory/reports/stock_valuation_with_reserved_stock_and_value','token='.$tocken.$url.$csv,'SSL');
		$this->data['export_pdf'] = $this->url->link('inventory/reports/stock_valuation_with_reserved_stock_and_value','token='.$tocken.$url.$pdf,'SSL');

		$this->data['breadcrumbs'][] = array(
			'text'     => $this->language->get('text_home'),
			'href'     => $this->url->link('common/home', 'token=' . $tocken, 'SSL'),
			'separator'=> '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'     => 'Reports',
			'href'     => $this->url->link('report/report','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'    => 'Stock Valuation With Reserved Stock Report And Value',
			'href'    => $this->url->link('inventory/reports/stock_valuation_with_reserved_stock_and_value','token='.$tocken,'SSL'),
			'separator'=> '<i class="fa fa-angle-right"></i>'
		);

		if($_REQUEST['export'] == 'csv' || $_REQUEST['export'] == 'pdf'){
			if($_REQUEST['export'] == 'csv'){
				$products = $this->data['products'];
				if(!empty($products)){
			    	ob_end_clean();
			        header( 'Content-Type: text/csv' );
			        header( 'Content-Disposition: attachment;filename=stock_valuation_with_reserved_stock.csv');
			        print "S.No,SKU,Name,Actual Stock,Reserved Stock,Available,Avg Cost,Stock Value\r\n";
					$j=0; 
					$total=0;
					foreach ($products as $product) {
						$j++;
						$invoice_no 	 = $product['sku'];
						$name     	 	 = $product['name'];
						$actualStock     = $product['sku_qty'];
						$reservedStock   = $product['RESERVED_QTY'];
						$aval  	 		 = $product['sku_qty']-$product['RESERVED_QTY'];
						$sku_price  	 = $product['sku_price'];
						$stock_value  	 = $product['sku_price']*$aval;
						$total+=$stock_value;
						print "$j,\"$invoice_no\",\"$name\",\"$actualStock\",\"$reservedStock\",\"$aval\",\"$sku_price\",\"$stock_value\"\r\n";
					}
					print "\"\",\"\",\"\",\"\",\"\",\"\",\"Total\",\"$total\"\"\r\n";
				}  die;
			}
			if($_REQUEST['export'] == 'pdf'){
				$products = $this->data['products'];
				if(!empty($products)){
			    	$company_id		 = $this->session->data['company_id'];
					$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
					$headerStr		 = $this->url->getCompanyAddressHeaderString($company_details);
					$str 			 = $headerStr;
					$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Valuation With Reserved Stock and Value</td>
							<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
					if($filterStr){
						$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
					}
			        $str.="<table class='listing' width='100%'><thead>
			           		<tr>
				           		<th>S.No</th>
				           		<th>SKU</th>
				           		<th>Name</th>
				           		<th>Actual Stock</th>
				           		<th>Reserved Stock</th>
				           		<th>Available</th>
				           		<th align='right'>Avg Cost</th>
				           		<th align='right'>Stock Value</th>
			           		</tr>
		           		  </thead><tbody>";
					$j=0; 
					$actualTotal=0;
					foreach ($products as $product) {
						$j++;
						$invoice_no 	 	= $product['sku'];
						$name     	 		= $product['name'];
						$actualStock      	= $product['sku_qty'];
						$reservedStock 		= $product['RESERVED_QTY'];
						$aval  				= $product['sku_qty']-$product['RESERVED_QTY'];
						$sku_price 			= $product['sku_price'];
						$stock_value  		= $product['sku_price']*$aval;
						$actualTotal +=$stock_value;
						$str.="<tr>
								<td>".$j."</td>
								<td>".$invoice_no."</td>
						   		<td>".$name."</td>
						   		<td>".$actualStock."</td>
						   		<td>".$reservedStock."</td>
						   		<td>".$aval."</td>
						   		<td  align='right'>".$sku_price."</td>
						   		<td align='right'>".number_format($stock_value,2)."</td>
							</tr>";	
					}
					$str.="<tr><td colspan='7' align='right'>Total</td><td  align='right'>".number_format($actualTotal,2)."</td></tr>";
					$str.="</tbody></table>";
					//echo $str; die;
					if($str){
						$filename = 'stock_valuation_with_reserved_stock-'.date('m-Y').'.pdf';
						include(DIR_SERVER.'pdf_print.php');
					}
				}die;
			}
		}

		$pagination 		= new Pagination();
		$pagination->total 	= $total_products;
		$pagination->page 	= $page;
		$pagination->limit  = $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('inventory/reports/stock_valuation_with_reserved_stock_and_value', 'token=' . $tocken. $url. '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();
		$this->data['token']= $this->session->data['token'];
		$this->template 	= 'inventory/stock_valuation_with_reserved_stock_and_value_report.tpl';
		$this->children 	= array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function stockAdjestmentReport(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		$this->load->model('transaction/sales');
		$this->document->setTitle('Stock Adjustment Report');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title']   = $this->language->get('heading_title');
		$locationDetails = $this->model_transaction_sales->getLocationCode($this->session->data['company_id']);

		$url = '';
		$this->data['back'] = $this->url->link('inventory/reports/stockAdjestmentReport', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')|| isset($this->request->get['stock_report'])) {			
			$company_id	= $this->session->data['company_id'];
			$stock_report = $this->request->get['stock_report'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$companyInfo	= $this->url->getCompanyAddress($company_details);
			//print_r($companyInfo);exit;
			if($companyInfo) {
				$this->data['companyInfo']	= $companyInfo;
			} else {
				$this->data['companyInfo']	='';
			}
			$pageUrl ='';

			if (strtolower($stock_report) == 'stock') {
				
				if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				$url = '';
				
				$company_details['location_code'] = $this->session->data['location_code'];
				$this->data['location_code'] = $this->session->data['location_code'];
				if(trim($company_details['location_code'])=='HQ'){
					
					if($_REQUEST['location_code']!=''){
						$filter_location_code = $_REQUEST['location_code']; 
					}else{
						$filter_location_code = $company_details['location_code']; 
					}
					$this->data['location_code']  = $filter_location_code;
				}

					if($_REQUEST['stock_date_from']!=''){
						$filter_stock_date_from = $_REQUEST['stock_date_from']; 
					}else{
						$filter_stock_date_from = date('d/m/Y');
					}
					if($_REQUEST['stock_date_to']!=''){
						$filter_stock_date_to = $_REQUEST['stock_date_to']; 
					}else{
						$filter_stock_date_to = date('d/m/Y');
					}

					$this->data['stock_date_from']= $filter_stock_date_from;
					$this->data['stock_date_to']  = $filter_stock_date_to;
					$this->data['location_name'] = $this->data['location_code'];

				$data = array(
					'sort'            => $sort,
					'order'           => $order,
					'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit'           => $this->config->get('config_admin_limit'),
					'stock_date_from' => $this->data['stock_date_from'],
					'stock_date_to'	  => $this->data['stock_date_to'],
					'filter_location_code' =>$this->data['location_code']
				);
				
				$data['filter_stock'] =$this->config->get('config_report_qty_negative');

				if($page==''){
					$page = 1;
				}	
				$results    = $this->model_inventory_inventory->getstockHeader($data);
				$list_total = count($results);
				$total_price= 0;

					foreach ($results as $result) {
						$total_price+= $result['net_total'];
						$this->data['products'][] = array(
							'pagecnt'  		 => ($page-1)*$this->config->get('config_admin_limit'), 
							'stkAdjustment_date' => date("d-m-Y",strtotime($result['stkAdjustment_date'])),
							'total_value'        => $result['total_value'],
							'gst'                => $result['gst'],
							'location_code'      => $result['location_code'],
							'net_total'          => $result['net_total'],
							'remarks'            => $result['remarks']
						);
				}
                  
				$this->data['products_sum']['tot_value'] =$total_price;
				$this->load->model('inventory/department');
				$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
				
				// printArray($this->data); die;
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->data['location_code'])) {
					$url .= '&location_code=' . $this->data['location_code'];
				}
				if (isset($this->data['stock_date_from'])) {
					$url .= '&stock_date_from=' . $this->data['stock_date_from'];
				}
				if (isset($this->data['stock_date_to'])) {
					$url .= '&stock_date_to=' . $this->data['stock_date_to'];
				}
				
				$url.= $pageUrl;
				$pagination = new Pagination();
				$pagination->total = $list_total;
				$pagination->page = $page;
				$pagination->limit = $this->config->get('config_admin_limit');
				$pagination->text = $this->language->get('text_pagination');
				$pagination->url = $this->url->link('inventory/reports/stockAdjestmentReport', 'token=' . $this->session->data['token'] . $url . '&stock_report=stock&page={page}', 'SSL');
				$this->data['pagination'] = $pagination->render();
				$this->data['filter_department'] = $filter_department;

			}else if (strtolower($stock_report) == 'master') {
				$results = $this->model_inventory_inventory->getProducts($data);
			}
			$url.=$pageurl;
			$this->data['stock_report'] = $stock_report;
			$this->data['exportAction'] = $this->url->link('inventory/reports/export_stockAdjustment_csv', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
			$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_stockAdjustment_pdf', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
			$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
		} else {
		    $this->redirect($this->url->link('inventory/reports', 'token=' . $this->session->data['token'], 'SSL'));
		}
		$this->data['token'] = $this->session->data['token'];
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['location'] = $this->cart->getLocation($company_details['location_code']);
		$this->template = 'inventory/stockadjusmentreport.tpl';
		
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_stockAdjustment_csv(){
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock AdjestmentReport';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['location_code'])) {
			$filter_location_code = $this->request->get['location_code'];
		}
		if (isset($this->request->get['stock_date_from'])) {
			$stock_date_from = $this->request->get['stock_date_from'];
		}
		if (isset($this->request->get['stock_date_to'])) {
			$stock_date_to = $this->request->get['stock_date_to'];
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location_code'=> $filter_location_code,
			'stock_date_from' 	  => $stock_date_from,
			'stock_date_to' 	  => $stock_date_to
		);
		$data['filter_stock'] =$this->config->get('config_report_qty_negative');
		$results = $this->model_inventory_inventory->getstockHeader($data);
		
		if(count($results > 0)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=StockAdjestmentReport.csv');
	        print "S.No,location,Date,Total,GST,Net Total\r\n";
		
			$j=0; 
			foreach ($results as $result) {
				$total += $result['net_total'];

				$j++;
				$location_code = $result['location_code'];
				$date          = date("d-m-Y",strtotime($result['stkAdjustment_date']));
				$total_value   = $result['total_value'];
				$gst           = $result['gst'];
				$net_total     = $result['net_total'];
				
				print "$j,\"$location_code\",\"$date\",\"$total_value\",\"$gst\",\"$net_total\"\r\n";
			}
			$tot_value = number_format($total,2);
			print "\"\",\"\",\"\",\"\",\"Total\",\"$tot_value\"\r\n";
		}
	}
	public function export_stockAdjustment_pdf(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');		
		$this->data['heading_title'] = 'Stock Adjustment Report';

		$url = '';
		if (isset($this->request->get['location_code'])) {
			$filter_location_code = $this->request->get['location_code'];
		}
		if (isset($this->request->get['stock_date_from'])) {
			$stock_date_from = $this->request->get['stock_date_from'];
		}
		if (isset($this->request->get['stock_date_to'])) {
			$stock_date_to = $this->request->get['stock_date_to'];
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location_code' =>$filter_location_code,
			'stock_date_from' =>$stock_date_from,
			'stock_date_to' =>$stock_date_to
		);

		$data['filter_stock'] =$this->config->get('config_report_qty_negative');
		$results = $this->model_inventory_inventory->getstockHeader($data);

		if($filter_location_code){
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($stock_date_from){
			$filterStr.=" From: ".$stock_date_from.',';
		}
		if($stock_date_to){
			$filterStr.=" To: ".$stock_date_to.',';
		}

		if(count($results>=1)){
	    	$company_id	= $this->session->data['company_id'];
			$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
			$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
			$str = $headerStr;
			$str.= '<table style="width:100%;"><tr><td style="font-size:20px;">Stock Adjustment Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
	             if($filterStr){
	        		$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
	        	 }
	        $str.="<table class='listing' style='width:100%;'><thead>
           		<tr>
	           		<th>S.No</th>
	           		<th>Location</th>
	           		<th>Date</th>
	           		<th>Total</th>
	           		<th>GST</th>
	           		<th>Net Total</th>
           		</tr></thead><tbody>";  

		
			$j=0; 
			foreach ($results as $result) {
			    $j++;
				$location_code= $result['location_code'];
				$date          = date("d-m-Y",strtotime($result['stkAdjustment_date']));
				$total_value  = $result['total_value'];
				$gst          = $result['gst'];
				$net_total    = $result['net_total'];
				$tot_value   += $result['net_total'];

				$str.="<tr><td>".$j."</td>
							<td>".$location_code."</td>
					   		<td>".$date."</td>
					   		<td align='right'>".$total_value."</td>
					   		<td align='right'>".$gst."</td>
					   		<td align='right'>".$net_total."</td>
 				        </tr>";	
			}
			$tot_value = number_format($tot_value,2);
			$str.="<tr><td colspan='5' align='right'>Total</td>
					   <td align='right'>".$tot_value."</td></tr>
			</tbody></table>";
			// echo $str; die;

			if($str){
				$filename = 'StockAdjustmentReport_'.date('dmyHis').'.pdf';
				include(DIR_SERVER.'pdf_print.php');
			}	
		}
	}
	public function stockAdjestmentDetailsReport(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('inventory/department');
		$this->load->model('master/bin');
		$this->document->setTitle('Stock Adjustment Details Report');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['back'] = $this->url->link('inventory/reports/stockAdjestmentDetailsReport', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$company_id	    = $this->session->data['company_id'];
		$stock_report   = $this->request->get['stock_report'];
		$company_details= $this->model_inventory_reports->getCompanyDetails($company_id);
		$companyInfo	= $this->url->getCompanyAddress($company_details);
		if($companyInfo) {
			$this->data['companyInfo']	= $companyInfo;
		} else {
			$this->data['companyInfo']	='';
		}


		$pageUrl ='';
           //printArray($_REQUEST); exit;
		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}
		if (isset($_REQUEST['filter_transactionNo'])) {
			$filter_transactionNo = $_REQUEST['filter_transactionNo'];
			$pageUrl.= '&filter_transactionNo='.$filter_transactionNo;
		}

		$url = '';
			if (isset($this->request->get['page'])) {
					$page = $this->request->get['page'];
				} else {
					$page = 1;
				}
				$data = array(
					'sort'             => $sort,
					'order'            => $order,
					'start'            => ($page - 1) * $this->config->get('config_admin_limit'),
					'limit'               => $this->config->get('config_admin_limit'),
					'filter_location_code'=> $filter_location_code,
					'stock_date_from'     => $filter_date_from,
					'stock_date_to'       => $filter_date_to,
					'filter_transactionNo'=> $filter_transactionNo,
				);
        $this->data['data']		= $data;
		if($filter_date_from!='' && $filter_date_to!='' &&$filter_location_code!=''){
			// $this->model_inventory_inventory->runSp_ProductStockSummaryByMonth($data);
		}

		// $list_total_arys = $this->model_inventory_inventory->getProductstockTotalNew($data);
		$last_page  = ceil($list_total / $this->config->get('config_admin_limit'));

		$this->data['Tolocations']= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['Todepartment']= $this->model_inventory_inventory->getDepartment();

		$Dep_results = $this->model_inventory_inventory->getDepartment();
		foreach ($Dep_results as $result) {
			$depName = $this->model_inventory_department->getDepartment($result['Dept_Code']);
			if(isset($depName['department_code']) && $depName['department_name']){
				$this->data['TodepartmentNew'][] = array(
					'department_code' => $depName['department_code'],
					'department_name' => $depName['department_name']
				);
			}
		}
		$results = $this->model_inventory_inventory->getstockHeader($data);

		foreach ($results as $result) {
			$details = $this->model_inventory_inventory->getstockHeaderDetails($result['stkAdjustment_id']);

			$this->data['stockAdjustmentDetails'][] = array(
				'pagecnt'       => ($page-1)*$this->config->get('config_admin_limit'), 
				'Location_Code' => $result['stkAdjustment_id'],
				'type' 			=> $result['add_or_deduct'],
				'Stock_Date'    => date("d-m-Y",strtotime($result['stkAdjustment_date'])),
				'details'   	=> $details
			);
		}
		$this->data['stock_report'] = $stock_report;
		$this->data['back'] = $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Reports',
			'href'      => $this->url->link('report/report', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_date_from'];
		}
		if (isset($this->request->get['filter_date_from'])) {
			$url .= '&filter_date_from=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_date_to'])) {
			$url .= '&filter_date_to=' . $this->request->get['filter_date_to'];
		}
		if (isset($this->request->get['filter_dept_code'])) {
			$url .= '&filter_dept_code=' . $this->request->get['filter_dept_code'];
		}
		if (isset($this->request->get['filter_sku'])) {
			$url .= '&filter_sku=' . $this->request->get['filter_sku'];
		}
		
		$url.= $pageUrl;
		$pagination = new Pagination();
		$pagination->total = $list_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit'); //5
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/reports/stockvaluationSummaryreport', 'token=' . $this->session->data['token'] . $url. '&stock_report=stock&page={page}', 'SSL');
		$this->data['exportAction'] = $this->url->link('inventory/reports/export_StockAdjustmentDetail_csv', 'token=' . $this->session->data['token'] . '&'.$url, 'SSL');
		$this->data['link_pdfexport'] = $this->url->link('inventory/reports/export_StockAdjustmentDetail_pdf', 'token=' . $this->session->data['token'] . '&&'.$pageUrl, 'SSL');
		// printArray($pagination);die;
		$this->data['pagination'] = $pagination->render();

		$this->data['token'] = $this->session->data['token'];
		$this->template = 'inventory/stock_adjustment_details_report.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function export_StockAdjustmentDetail_csv(){

		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/bin');
		$this->load->model('transaction/purchase');
		
		$this->data['heading_title'] = 'Stock AdjestmentReport';
		$url = '';		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		} else {
			$filter_date_to = date('d/m/Y');
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		}

		$page = $_REQUEST['page'];
		$data = array(
			'filter_location_code'=> $filter_location_code,
			'stock_date_from' 	  => $filter_date_from,
			'stock_date_to' 	  => $filter_date_to
		);
		$data['filter_stock'] =$this->config->get('config_report_qty_negative');
		$results = $this->model_inventory_inventory->getstockHeader($data);
				// printArray($results); die;
		if(count($results > 0)){
	    	ob_end_clean();
	        header( 'Content-Type: text/csv' );
	        header( 'Content-Disposition: attachment;filename=StockAdjestmentDetailsReport.csv');
		
			$j=0; 
			foreach ($results as $result) {
			$type = $result['add_or_deduct'];

			$details = $this->model_inventory_inventory->getstockHeaderDetails($result['stkAdjustment_id']);
			$date     = date("d-m-Y",strtotime($result['stkAdjustment_date']));
			$location = $result['location_code'];
				$tot_qty = $tot_unitcost=$tot_total_value=$tot_gst=$tot_netval=0;
	        print "Location,\"$location\",\"\",\"\",\"\",\"\",Date,\"$date\" \r\n";
	        print "S.No,SKU,Description,Qty,Unit Cost,Total Value,GST,Net Total\r\n";
				foreach ($details as $value) {
					
					$j++;
					$tot_qty       += $value['qty'];
					$tot_unitcost  += $value['unit_cost'];
					$tot_total_value+= $value['net_total'];
					$tot_gst       += $value['gst'];
					$tot_netval    += $value['net_value'];

					$sku         = $value['sku'];
					$description = $value['sku_description'];
					if($type=='1'){
						$qty         = "-".$value['qty'];			
					}else{
						$qty         = $value['qty'];		
					}
				
					$unit_cost   = $value['unit_cost'];
					$total_value = $value['total_value'];
					$gst         = $value['gst'];
					$net_value   = $value['net_value'];

					
					print "$j,\"$sku\",\"$description\",\"$qty\",\"$unit_cost\",\"$total_value\",\"$gst\",\"$net_value\"\r\n";
				}
					$tot_value = number_format($total,2);
					if($type=='1'){
						$tot_qty         = "-".$tot_qty;			
					}else{
						$tot_qty         = $tot_qty;					
					}

			print "\"\",\"\",\"Total\",\"$tot_qty\",\"$tot_unitcost\",\"$tot_total_value\",\"$tot_gst\",\"$tot_netval\"\r\n";
			}
			
		}
	}
	public function export_StockAdjustmentDetail_pdf(){
		$this->language->load('inventory/reports');
		$this->load->model('inventory/reports');
		$this->load->model('inventory/inventory');
		$this->load->model('master/vendor');
		$this->load->model('transaction/purchase');

		$filterStr = '';

		if (isset($_REQUEST['filter_date_from'])) {
			$filter_date_from = $_REQUEST['filter_date_from'];
			$pageUrl.= '&filter_date_from='.$filter_date_from;
		} else {
			$filter_date_from = date('d/m/Y');
		}
		if (isset($_REQUEST['filter_date_to'])) {
			$filter_date_to = $_REQUEST['filter_date_to'];
			$pageUrl.= '&filter_date_to='.$filter_date_to;
		}
		if (isset($_REQUEST['filter_transactionNo'])) {
			$filter_transactionNo = $_REQUEST['filter_transactionNo'];
			$pageUrl.= '&filter_transactionNo='.$filter_transactionNo;
		}
		if (isset($_REQUEST['filter_location_code'])) {
			$filter_location_code = $_REQUEST['filter_location_code'];
			$pageUrl.= '&filter_location_code='.$filter_location_code;
		} else {
			$filter_location_code = $this->session->data['location_code'];
		}

		if($filter_date_from){
			$filterStr.=" From Date: ".$filter_date_from.',';
		}
		if($filter_date_to){
			$filterStr.=" To Date: ".$filter_date_to.',';
		}
		if($filter_location_code) {
			$filterStr.=" Location: ".$filter_location_code.',';
		}
		if($filter_transactionNo) {
			$filterStr.=" Transaction No: ".$filter_transactionNo.',';
		}
		if($filterStr){
			$filterStr = rtrim($filterStr,",");
		}
		$data = array(
			'filter_location_code'=>$filter_location_code,
			'stock_date_from'=>$filter_date_from,
			'stock_date_to'=>$filter_date_to
		);
		$results = $this->model_inventory_inventory->getstockHeader($data);
		$company_id	= $this->session->data['company_id'];
		$company_details = $this->model_inventory_reports->getCompanyDetails($company_id);
		$headerStr	= $this->url->getCompanyAddressHeaderString($company_details);
		$str = $headerStr;

			$str.= '<table style="width:100%;"><tr><td align="left" style="font-size:20px;">Stock Ajustment Detail Report</td>
					<td align="right">Time: '.date('d-m-Y H:i').'</td></tr></table>';
			if($filterStr){
				$str.="<p class='reportFilter'>Filter By ".$filterStr."</p>";	
			}

			foreach ($results as $key => $value) {
				$stkAdjustment_id = $value['stkAdjustment_id'];
				$stkAdjustment_date = date("d-m-Y",strtotime($value['stkAdjustment_date']));
				$type = $value['add_or_deduct'];
				$str.="<table class='listing' style='width:100%;'><thead>
						<tr><th colspan='6' class='left' style='border-right:none;'>Transaction No:000".$value['stkAdjustment_id']."</th>
							<th class='left' style='border-left:none;'>Date:".$stkAdjustment_date."</th>
						</tr>
						<tr>
							<th>SKU</th>
							<th>Descrription</th>
							<th>Qty</th>
							<th>Unit Cost</th>
							<th>Total Value</th>
							<th>GST</th>
							<th>Net Value</th>
						</tr></thead><tbody>";
				$results= $this->model_inventory_inventory->getstockHeaderDetails($stkAdjustment_id);
		    	
				$j=0; 
				$tot_qty=$tot_unitcost=$tot_tot_val=$tot_gst=$tot_netVal=0;
				foreach ($results as $result) {	
					$j++;
					$tot_qty     += $result['qty'];
					$tot_unitcost+= $result['unit_cost'];
					$tot_tot_val += $result['total_value'];
					$tot_gst     += $result['gst'];
					$tot_netVal  += $result['net_value'];

					if($type=='1'){
						$result['qty']         = "-".$result['qty'];			
					}else{
						$result['qty']        = $result['qty'];					
					}

					$str.="<tr>
							   <td>".$result['sku']."</td>
							   <td>".$result['sku_description']."</td>
							   <td class='right'>".$result['qty']."</td>
							   <td class='right'>".$result['unit_cost']."</td>
							   <td class='right'>".$result['total_value']."</td>
							   <td class='right'>".$result['gst']."</td>
							   <td class='right'>".$result['net_value']."</td>
						</tr>";	
				
				}
				if($type=='1'){
						$tot_qty         = "-".$tot_qty;			
					}else{
						$tot_qty         = $tot_qty;					
					}

				$str.="<tr><td colspan='2' align='right'>Total</td>
							<td class='right'>".$tot_qty."</td>
							<td class='right'>".number_format($tot_unitcost,2)."</td>
							<td class='right'>".number_format($tot_tot_val,2)."</td>
							<td class='right'>".number_format($tot_gst,2)."</td>
							<td class='right'>".number_format($tot_netVal,2)."</td>

				</tr><tr><td colspan='7' style='border: none;padding: 10px;'></td></tr>";
				$str.="</tbody></table>";
			}
				if($str){
					$filename = 'Stock_adjustment_details_report_'.date('dmyHis').'.pdf';
					include(DIR_SERVER.'pdf_print.php');
				}
	}
}
?>