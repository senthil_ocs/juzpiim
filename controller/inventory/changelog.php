<?php 
class ControllerInventoryChangelog extends Controller {
	private $error = array();
 	
	public function index() {
		$this->load->model('tool/image');
		$this->language->load('inventory/inventory');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/inventory');
		
        if (isset($this->request->get['filter_fromdate'])) {
			$filter_fromdate = $this->request->get['filter_fromdate'];
		} else {
			$filter_fromdate = null;
		}
		
		if (isset($this->request->get['filter_todate'])) {
			$filter_todate = $this->request->get['filter_todate'];
		} else {
			$filter_todate = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		
		if (isset($this->request->get['filter_fromdate'])) {
			$url .= '&filter_fromdate=' . urlencode(html_entity_decode($this->request->get['filter_fromdate'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_todate'])) {
			$url .= '&filter_todate=' . urlencode(html_entity_decode($this->request->get['filter_todate'], ENT_QUOTES, 'UTF-8'));
		}
		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		
		$this->data['productchange'] = array();

		$data = array(
			'filter_fromdate' => $filter_fromdate, 
			'filter_todate'	  => $filter_todate, 
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           => $this->config->get('config_admin_limit')
		);

		$this->load->model('tool/image');

		$productchange_total = $this->model_inventory_inventory->getTotalProductChanges($this->request->get['product_id'],$data);
		$results = $this->model_inventory_inventory->getProductChanges($this->request->get['product_id'],$data);

		foreach($results as $result){
			$this->data['productchange'][]= array(
				'date'       =>$result['dt_datetime'],
				'action'     =>$result['action'],
				'sku'        =>$result['sku'],
				'price'      =>$result['price'],
				'weight'     =>$result['weight'],
				'tax'        =>$result['tax_class_id'],
				'bin'        =>$result['bin_id'],
				'updated_by' =>$result['updated_by'],
				'vendor'     =>$result['vendor_id'],
				'image'      =>$result['image'],
				'status'     =>$result['status']

			);

		}

		$this->data['productdescription'] = $this->model_inventory_inventory->getProductDescription($this->request->get['product_id'],$data);
		$this->data['productbarcode'] = $this->model_inventory_inventory->getProductBarcode($this->request->get['product_id'],$data);

		$this->data['text_no_results'] = $this->language->get('text_no_results');

        $this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}




	   $url = '';
		
		if (isset($this->request->get['filter_fromdate'])) {
			$url .= '&filter_fromdate=' . urlencode(html_entity_decode($this->request->get['filter_fromdate'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_todate'])) {
			$url .= '&filter_todate=' . urlencode(html_entity_decode($this->request->get['filter_todate'], ENT_QUOTES, 'UTF-8'));
		}
		

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}		

		$url = '';

		if (isset($this->request->get['filter_fromdate'])) {
			$url .= '&filter_fromdate=' . urlencode(html_entity_decode($this->request->get['filter_fromdate'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_todate'])) {
			$url .= '&filter_todate=' . urlencode(html_entity_decode($this->request->get['filter_todate'], ENT_QUOTES, 'UTF-8'));
		}
		

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $productchange_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/changelog', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_fromdate'] = $filter_fromdate;
		$this->data['filter_todate'] = $filter_todate;
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;


		$this->data['route'] = $this->request->get['route'];
		$this->data['product_id'] = $this->request->get['product_id'];

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		 $this->data['inventory'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory_changelog'] = $this->url->link('inventory/changelog', '&token=' . $this->session->data['token'], 'SSL');

		$this->template = 'inventory/changelog.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
}
?>