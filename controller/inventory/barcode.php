<?php  
class ControllerInventoryBarcode extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->language->load('inventory/barcode');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/barcode');
		
		$this->getForm();
		
  	}

  	public function getDetails() {

	 $this->load->model('inventory/barcode');
     $bar_code  = $this->request->post["bar_code"];
     $Details = $this->model_inventory_barcode->getProductsByBarcode($bar_code);
     echo json_encode($Details);

  	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['entry_bar_code'] = $this->language->get('entry_bar_code');
		$this->data['entry_inventory_code'] = $this->language->get('entry_inventory_code');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_labels'] = $this->language->get('entry_labels');
		
		$this->data['button_cancel'] = $this->language->get('button_cancel');		
		$this->data['button_print'] = $this->language->get('button_print');

		
		if (isset($this->request->post['bar_code'])) {			
			$productInfo = $this->model_inventory_barcode->getProductsByBarcode($this->request->post['bar_code']);
			if(empty($productInfo)) {
				$this->error['warning'] = "Please enter valid barcode.";	
			}
		}		

 		if (isset($this->error['warning'])) {	
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/barcode', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
				
		$url = '';

		$this->data['action'] = $this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['cancel'] = $this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (!empty($productInfo)) {
			$this->data['bar_code'] = $productInfo['barcode'];
		} else {
			$this->data['bar_code'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['inventory_code'] = $productInfo['sku'];
		} else {
			$this->data['inventory_code'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['name'] = $productInfo['name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['price'] = $productInfo['price'];
		} else {
			$this->data['price'] = '';
		}

		$this->data['back'] = $this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
		    $this->data['info']['bar_code'] = $this->request->post['bar_code'];
			$this->data['info']['price'] = $this->currency->format($this->request->post['price']);
			$this->data['info']['no_labels'] = $this->request->post['no_labels'];
		}

		$this->data['token'] = $this->request->get['token'];

		$this->data['route'] = 'inventory/barcode';
		$this->template = 'inventory/barcode_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'inventory/barcode')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['bar_code'])) {
			$this->error['bar_code'] = 1;
		}
		
		if (empty($this->request->post['price'])) {
			$this->error['price'] = 1;
		}
		
		if (empty($this->request->post['no_labels'])) {
			$this->error['no_labels'] = 1;
		}
		
		if (!$this->error) {
			return true;
		} else {
			$this->error['warning'] = $this->language->get('error_invalid');
			$this->redirect($this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			return false;
		}
	}
	
}  
?>