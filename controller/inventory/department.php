<?php  
class ControllerInventoryDepartment extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->language->load('inventory/department');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/department');
		
		$this->getList();
		
  	}
	
	public function insert() {

		$this->language->load('inventory/department');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/department');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm($type='insert')) {
			
			$this->model_inventory_department->addDepartment($this->request->post);
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('inventory/department', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function update() {
	
		$this->language->load('inventory/department');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/department');

		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_inventory_department->editDepartment($this->request->get['department_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('inventory/department', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	
	public function delete() {
		//echo 'here'; exit;
    	$this->language->load('inventory/department');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/department');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $department_id) {
				$this->model_inventory_department->deleteDepartment($department_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//$this->redirect($this->url->link('inventory/department', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'createdon';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
			
		$url = '';
			
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		$this->data['insert'] = $this->url->link('inventory/department/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/department/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['departments'] = array();

		$data = array(
			'filter_name'=> $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$department_total = $this->model_inventory_department->getTotalDepartments($data);

		$results = $this->model_inventory_department->getDepartments($data);

		
		foreach ($results as $result) {
			$action = array();
			
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/department/update', 'token=' . $this->session->data['token'] . '&department_id=' . $result['department_id'] . $url, 'SSL')
			);

			$this->data['departments'][] = array(
				'department_id' 	=> $result['department_id'],
				'name'      	 	=> $result['department_name'],
				'code'      	 	=> $result['department_code'],
				'remarks'      		=> $result['remarks'],
				'status'      		=> $result['status'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['department_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_remarks'] = $this->language->get('column_remarks');
		$this->data['column_salesman'] = $this->language->get('column_salesman');
		
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/department', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('inventory/department', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_remarks'] = $this->url->link('inventory/department', 'token=' . $this->session->data['token'] . '&sort=remarks' . $url, 'SSL');
		$this->data['sort_salesman'] = $this->url->link('inventory/department', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $department_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('inventory/department', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		//$this->data['route'] = $this->request->get['route'];
		$this->data['route'] = 'inventory/department';

		$this->data['token'] =  $this->session->data['token'];
		$this->template = 'inventory/department_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_salesman'] = $this->language->get('entry_salesman');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/department', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/department/insert', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}

		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}
		
		if (isset($this->error['remarks'])) {
			$this->data['error_remarks'] = $this->error['remarks'];
		} else {
			$this->data['error_remarks'] = '';
		}
				
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['department_id'])) { 
			$this->data['action'] = $this->url->link('inventory/department/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('inventory/department/update', 'token=' . $this->session->data['token'] . '&department_id=' . $this->request->get['department_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('inventory/department', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['department_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$department_info = $this->model_inventory_department->getDepartment($this->request->get['department_id']);
		}
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($department_info)) {
			$this->data['name'] = $department_info['department_name'];
		} else {
			$this->data['name'] = '';
		}

		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($department_info)) {
			$this->data['code'] = $department_info['department_code'];
		} else {
			$this->data['code'] = '';
		}
		
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($department_info)) {
			$this->data['remarks'] = $department_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($department_info)) {
			$this->data['status'] = $department_info['status'];
		} else {
			$this->data['status'] = '';
		}
		
		if (isset($this->request->post['salesman'])) {
			$this->data['salesman'] = (string)$this->request->post['salesman'];
		} elseif (!empty($department_info)) {
			$this->data['salesman'] = (string)$department_info['salesman_required'];
		} else {
			$this->data['salesman'] = '';
		}
		// printArray($this->data);die;
		$this->data['route'] = 'inventory/department';

		$this->template = 'inventory/department_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm($type='') {
		if (!$this->user->hasPermission('modify', 'inventory/department')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (empty($this->request->post['code'])) {
			$this->error['code'] = $this->language->get('error_code');
		}

		
		$exist = $this->model_inventory_department->getDepartmentByCode($this->request->post['code'],$this->request->get['department_id']);
		if($exist>=1){
			$this->error['warning'] = 'Given department code already Exist';
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'inventory/department')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
		
	  	$this->load->model('inventory/inventory');
		$this->load->model('inventory/category');
		
		foreach ($this->request->post['selected'] as $dept_id) {
		
			$product_total = $this->model_inventory_inventory->getTotalProductsByDepartmentId($dept_id);
			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
			
			/*$category_total = $this->model_inventory_category->getTotalCategoryByDepartmentId($dept_id);
			if ($category_total) {
				$this->error['warning'] = sprintf($this->language->get('error_category'), $category_total);
			}*/
			
		}
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('inventory/department');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_inventory_department->getDepartments($data);

			foreach ($results as $result) {
				$json[] = array(
					'department_id' => $result['department_id'], 
					'name'        => strip_tags(html_entity_decode($result['department_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

}  
?>