<?php  
class ControllerInventoryBrand extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->language->load('inventory/brand');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/brand');
		
		$this->getList();
		
  	}
	
	public function insert() {

		$this->language->load('inventory/brand');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/brand');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_inventory_brand->addBrand($this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			$this->redirect($this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}
	
	public function update() {
	
		$this->language->load('inventory/brand');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/brand');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_inventory_brand->editBrand($this->request->get['brand_id'], $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
					
			$this->redirect($this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	
	public function delete() {
    	$this->language->load('inventory/brand');
 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/brand');
		
		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $brand_id) {
				$this->model_inventory_brand->deleteBrand($brand_id);
			}
			
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
  	}
	
	protected function getList() {

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}	

		$url = '';	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		$this->data['filter_name'] = $filter_name;
  		$this->data['insert'] = $this->url->link('inventory/brand/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/brand/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		 
		$this->data['brands'] = array();

		$data = array(
			'filter_name'	    => $filter_name,
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);
		
		$brand_total = $this->model_inventory_brand->getTotalBrands($data);
		
		$results = $this->model_inventory_brand->getBrands($data);
		
		foreach ($results as $result) {
			
			$action = array();
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/brand/update', 'token=' . $this->session->data['token'] . '&brand_id=' . $result['brand_id'] . $url, 'SSL')
			);
			
			$category_name = array();
			$department_name = array();			
			$product_categories = $this->model_inventory_brand->getBrandCategory($result['brand_id']);
			if(!empty($product_categories)) {
				foreach ($product_categories  as $product_category) {
					$categoryDetails = $this->model_inventory_brand->getCategoryDetailsById($product_category);
					if(!empty($categoryDetails)) {
						$category_name[]	= $categoryDetails['category_name'];
					}
				}
				$category_name	= implode(',<br>',$category_name);
			}
			if(empty($product_categories)) {
				$category_name	='';	
			}
			
			$product_departments = $this->model_inventory_brand->getBrandDepartment($result['brand_id']);
			
			if(!empty($product_departments)) {
				foreach ($product_departments  as $product_department) {
					$departmentDetails = $this->model_inventory_brand->getDepartmentDetailsById($product_department['department_id']);
					$department_name[]	= $departmentDetails['department_name'];
				}
				$department_name	= implode(',<br>',$department_name);
			}
			
			if(empty($product_departments)) {
				$department_name	='';	
			}

			$this->data['brands'][] = array(
				'brand_id' 	=> $result['brand_id'],
				'name'      	 	=> $result['brand_name'],
				'code'      	 	=> trim($result['brand_code']),
				'department'      	=> $department_name,
				'category'      	=> $category_name,
				'status'      		=> $result['status'],
				'selected'   => isset($this->request->post['selected']) && in_array($result['brand_id'], $this->request->post['selected']),
				'action'     => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_code'] = $this->language->get('column_code');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_category'] = $this->language->get('column_category');
		$this->data['column_department'] = $this->language->get('column_department');
		$this->data['column_action'] = $this->language->get('column_action');	

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');
 
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Brand List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_name'] = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . '&sort=B.brand_name' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . '&sort=B.status' . $url, 'SSL');
		$this->data['sort_department'] = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . '&sort=D.department_name' . $url, 'SSL');
		$this->data['sort_category'] = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . '&sort=C.category_name' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
												
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $brand_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');

		$pagination->url = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['token'] =  $this->session->data['token'];
		$this->template = 'inventory/brand_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');
	
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_code'] = $this->language->get('entry_code');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_status'] = $this->language->get('entry_status');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Brand List'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}		
		
		
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

  		if (!isset($this->request->get['brand_id'])) { 
			$this->data['action'] = $this->url->link('inventory/brand/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('inventory/brand/update', 'token=' . $this->session->data['token'] . '&brand_id=' . $this->request->get['brand_id'] . $url, 'SSL');
		}
		
		$this->data['cancel'] = $this->url->link('inventory/brand', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->get['brand_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$brand_info = $this->model_inventory_brand->getBrand($this->request->get['brand_id']);
		}
		
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($brand_info)) {
			$this->data['name'] = trim($brand_info['brand_name']);
		} else {
			$this->data['name'] = '';
		}
		if (isset($this->request->post['code'])) {
			$this->data['code'] = $this->request->post['code'];
		} elseif (!empty($brand_info)) {
			$this->data['code'] = trim($brand_info['brand_code']);
		} else {
			$this->data['code'] = '';
		}
		
		if (isset($this->request->post['category'])) {
			$this->data['product_categories'] = $this->request->post['category'];
		} elseif (isset($brand_info)) {
			$this->data['product_categories'] = $this->model_inventory_brand->getBrandCategory($this->request->get['brand_id']);
		} else {
			$this->data['product_categories'] = array();
		}
		
		
		if (isset($this->request->post['category'])) {
			$this->data['category'] = $this->request->post['category'];
		} elseif (!empty($brand_info)) {
			$this->data['category'] = $brand_info['category_id'];
		} else {
			$this->data['category'] = '';
		}
		
		if (isset($this->request->post['department'])) {
			$this->data['department'] = $this->request->post['department'];
		} elseif (!empty($brand_info)) {
			$this->data['department'] = $brand_info['department_id'];
		} else {
			$this->data['department'] = '';
		}
		
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($brand_info)) {
			$this->data['remarks'] = $brand_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = (string) $this->request->post['status'];
		} elseif (!empty($brand_info)) {
			$this->data['status'] = (string) $brand_info['status'];
		} else {
			$this->data['status'] = '';
		}

		$this->data['route'] = 'inventory/brand';

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['brand'] = $this->url->link('inventory/brand', '&token=' . $this->session->data['token'], 'SSL');
		
		$this->template = 'inventory/brand_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'inventory/brand')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		if (empty($this->request->post['name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}
		if (empty($this->request->post['code'])) {
			$this->error['code'] = $this->language->get('error_code');
		}
		
		$exist = $this->model_inventory_brand->getBrandByCode($this->request->post['code'],$this->request->get['brand_id']);
		if($exist>=1){
			$this->error['warning'] = 'Given brand code already Exist';
		}		
		
		/*if (!isset($this->request->post['department'])) {
			$this->error['department'] = $this->language->get('error_department');
		}*/	
		
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}
	
	protected function validateDelete() { 
    	if (!$this->user->hasPermission('modify', 'inventory/brand')) {
      		$this->error['warning'] = $this->language->get('error_permission');
    	} 
		
	  	$this->load->model('inventory/inventory');
		foreach ($this->request->post['selected'] as $brand_id) {
			$product_total = $this->model_inventory_inventory->getTotalProductsByBrandId($brand_id);
			if ($product_total) {
				$this->error['warning'] = sprintf($this->language->get('error_product'), $product_total);
			}
		}
		 
		if (!$this->error) {
	  		return true;
		} else { 
	  		return false;
		}
  	}
	
	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('inventory/brand');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_inventory_brand->getBrands($data);

			foreach ($results as $result) {
				$json[] = array(
					'department_id' => $result['brand_id'], 
					'name'        => strip_tags(html_entity_decode($result['brand_name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}

}  
?>