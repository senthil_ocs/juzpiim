<?php 
class ControllerInventorySearch extends Controller {
	private $error = array();
 
	public function index() {
		$this->language->load('inventory/search');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/search');
		
		$this->load->model('inventory/inventory');
		
		$this->getList();
	}

	public function getCategoryByDept()
	{
		$this->load->model('inventory/search');
		$fDepartment = $this->request->post['filter_department'];
		$Category = $this->model_inventory_search->getCategoryByDepart($fDepartment);
		echo json_encode($Category);
	}
	public function delete() {
	
		$this->language->load('inventory/search');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('inventory/inventory');
         //printArray($this->request->post); //exit;
		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $product_id) {
				$strCheckTransProductCount = $this->model_inventory_inventory->checkTranscationProductCount($product_id);
				if($strCheckTransProductCount['CNT'] == 0){
					$strGetSku = $this->model_inventory_inventory->getSkuDetailsByProductId($product_id);
					$strCheckTransSalesProductCount = $this->model_inventory_inventory->checkTranscationSalesProductCount($strGetSku['sku']);
					if($strCheckTransSalesProductCount['CNT'] == 0){
						$this->model_inventory_inventory->deleteProduct($product_id);
						$this->session->data['success'] = $this->language->get('text_success');
					} else {
						$this->session->data['error'] = "This Inventory has ".$strCheckTransSalesProductCount['CNT']." transactions! You can't delete this Inventory";
					}
				} else {
					$this->session->data['error'] = "This Inventory has ".$strCheckTransProductCount['CNT']." transactions! You can't delete this Inventory";
				}
			}

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}	

			/*if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}*/

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			//$this->redirect($this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function getList() {
		
		if (isset($this->request->get['filter_barcodes'])) {
			$filter_barcodes = $this->request->get['filter_barcodes'];
		} else {
			$filter_barcodes = null;
		}
		
		if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} else {
			$filter_vendor = null;
		}
		
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}
		
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}
		
		if (isset($this->request->get['filter_brand'])) {
			$filter_brand = $this->request->get['filter_brand'];
		} else {
			$filter_brand = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}
		if (isset($this->request->get['filter_priceto'])) {
			$filter_priceto = $this->request->get['filter_priceto'];
		} else{
           $filter_priceto = null;
		}
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_location_code'])) {
       	   $location = $this->request->get['filter_location_code'];
		} else {
		   $location = $this->session->data['location_code'];
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.product_id ';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';
		
		if (isset($this->request->get['filter_barcodes'])) {
			$url .= '&filter_barcodes=' . urlencode(html_entity_decode($this->request->get['filter_barcodes'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . $this->request->get['filter_vendor'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}	
		if (isset($this->request->get['filter_priceto'])) {
			$url .= '&filter_priceto=' . $this->request->get['filter_priceto'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
		}	


		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['insert'] = $this->url->link('inventory/search/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('inventory/search/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		// Category
		
		//$this->data['category_collection'] = $this->model_inventory_category->getCategorys();
		//if($this->request->get['filter_department'] != ''){
		$this->load->model('inventory/category');			
		$this->data['category_collection'] =$this->model_inventory_category->getCategorys();
		//}
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		//printArray($this->data['vendor_collection']);
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();

		$this->data['products'] = array();
		
		$data = array(
			'filter_barcodes'	  => $filter_barcodes,
			'filter_vendor'	      => $filter_vendor,
			'filter_department'	  => $filter_department, 
			'filter_category'	  => $filter_category,
			'filter_brand'	      => $filter_brand,
			'filter_price'	      => $filter_price,
			'filter_priceto'	  => $filter_priceto,
			'filter_name'         => $filter_name,
			'filter_location_code' =>$location,
			'sort'                => $sort,
			'order'               => $order,
			'start'               => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'               => $this->config->get('config_admin_limit')
		);
		//print_r($data); exit;
		$this->load->model('tool/image');

		$product_totalAry  = $this->model_inventory_inventory->getTotalProductsReport($data);
		$product_total = $product_totalAry['total'];

		$results = $this->model_inventory_inventory->getProductsReport($data);
		
    	// printArray($results); exit;
		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('inventory/inventory/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . '&type=search'.$url, 'SSL')
			);

		
			$barcodes	= array();
			$product_barcodes = $this->model_inventory_inventory->getProductBarcodes($result['product_id']);
			if(!empty($product_barcodes)) {
				foreach ($product_barcodes  as $product_barcode) {
					$barcodes[]	= $product_barcode['barcode'];
				}
				$barcodes	= implode(',<br>',$barcodes);
			}
			if(empty($barcodes)) {
				$barcodes	='';	
			}
			$this->load->model('inventory/search');
			$product_brand = $this->model_inventory_search->getProductBrandName($result['product_id']);
			
			$desc_limit = 50;
			$short_description = (strlen(utf8_decode($result['short_description'])) > $desc_limit) ? mb_substr(strip_tags(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8')), 0, $desc_limit) . '...' : strip_tags(html_entity_decode($result['short_description'], ENT_QUOTES, 'UTF-8'));
			 $sku_qty=number_format($result['sku_qty']-$result['reserved_qty'],2);
			
			$this->data['products'][] = array(
				'product_id' => $result['product_id'],
				'name'       => $result['name'],
				'sku'        => $result['sku'],
				'barcodes'   => $barcodes,
				'price'      => $result['sku_price'],
				'special'    => $special,
				'image'      => $image,
				'sku_qty'      => $result['sku_qty'],
				'sku_cost'     => $result['sku_cost'],
				'reserved_qty' => $result['reserved_qty'],
				'quantity'   => $sku_qty,
				'status'     => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'   => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
				'action'     => $action,
				'short_description'  => $short_description,
				'product_department' => $product_department,
				'product_category'   => $product_category,
				'department_code'    => $result['department_name'],
				'category_code'      => $result['category_name'],
				'brand_code'         => $result['brand_name'],
				'product_brand'      => implode(", ", $product_brand)
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');		
		$this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');		
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['column_image'] = $this->language->get('column_image');	
		$this->data['column_code'] = $this->language->get('column_code');			
		$this->data['column_name'] = $this->language->get('column_name');		
		$this->data['column_price'] = $this->language->get('column_price');		
		$this->data['column_quantity'] = $this->language->get('column_quantity');		
		$this->data['column_status'] = $this->language->get('column_status');		
		$this->data['column_action'] = $this->language->get('column_action');				
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['text_department'] = $this->language->get('text_department');
		$this->data['text_category'] = $this->language->get('text_category');
		$this->data['text_brand'] = $this->language->get('text_brand');
		$this->data['text_barcode'] = $this->language->get('text_barcode');
		$this->data['text_vendor'] = $this->language->get('text_vendor');
		$this->data['text_search'] = $this->language->get('text_search');

		$this->data["column_pricefrom"] = $this->language->get('column_pricefrom');
		$this->data["column_priceto"]   = $this->language->get('column_priceto');
		$this->data["txt_description"]  = $this->language->get('txt_description');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else if(isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('inventory/search', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$url = '';
		
		if (isset($this->request->get['filter_barcodes'])) {
			$url .= '&filter_barcodes=' . urlencode(html_entity_decode($this->request->get['filter_barcodes'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . $this->request->get['filter_vendor'];
		}
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		if (isset($this->request->get['filter_priceto'])) {
			$url .= '&filter_priceto=' . $this->request->get['filter_priceto'];
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_name'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . '&sort=p.name' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . '&sort=pd.sku_price' . $url, 'SSL');
		$this->data['sort_quantity'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . '&sort=pd.sku_qty' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');

		$url = '';
		
		if (isset($this->request->get['filter_barcodes'])) {
			$url .= '&filter_barcodes=' . urlencode(html_entity_decode($this->request->get['filter_barcodes'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . $this->request->get['filter_vendor'];
		}	
		
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}	

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		if (isset($this->request->get['filter_priceto'])) {
			$url .= '&filter_priceto=' . $this->request->get['filter_priceto'];
		}
		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . $this->request->get['filter_product'];
		}	

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_barcodes'] = $filter_barcodes;
		$this->data['filter_vendor'] = $filter_vendor;
		$this->data['filter_department'] = $filter_department;
		$this->data['filter_category'] = $filter_category;
		$this->data['filter_brand'] = $filter_brand;
		$this->data['filter_price'] = $filter_price;
		$this->data['filter_priceto'] = $filter_priceto;
		$this->data['filter_name'] = $filter_name;
		$this->data['Tolocations'] = $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $location;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['route'] = $this->request->get['route'];

		$this->template = 'inventory/search_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	public function update() {
		
		$this->language->load('inventory/search');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_inventory_inventory->editProduct($this->request->get['product_id'], $this->request);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}	

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}
	
	protected function getForm() {
	
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_plus'] = $this->language->get('text_plus');
		$this->data['text_minus'] = $this->language->get('text_minus');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');
		
		$this->data['entry_sku'] = $this->language->get('entry_code');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_brand'] = $this->language->get('entry_brand');
		$this->data['entry_origin'] = $this->language->get('entry_origin');
		$this->data['entry_uom'] = $this->language->get('entry_uom');
		$this->data['entry_bin'] = $this->language->get('entry_bin');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_shortdescription'] = $this->language->get('entry_shortdescription');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_qoh'] = $this->language->get('entry_qoh');
		$this->data['entry_special'] = $this->language->get('entry_special');
		$this->data['entry_allowzeroprice'] = $this->language->get('entry_allowzeroprice');
		$this->data['entry_nontaxitem'] = $this->language->get('entry_nontaxitem');
		$this->data['entry_nontaxitem'] = $this->language->get('entry_nontaxitem');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_discontinued'] = $this->language->get('entry_discontinued');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_specialprice'] = $this->language->get('entry_specialprice');
		$this->data['entry_discountcode'] = $this->language->get('entry_discountcode');
		$this->data['entry_unitcost'] = $this->language->get('entry_unitcost');
		$this->data['entry_avgcost'] = $this->language->get('entry_avgcost');
		$this->data['entry_profit'] = $this->language->get('entry_profit');
		$this->data['entry_barcode'] = $this->language->get('entry_barcode');
		$this->data['tab_activec'] = true;		
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_date_available'] = $this->language->get('entry_date_available');
		$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_subtract'] = $this->language->get('entry_subtract');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_length'] = $this->language->get('entry_length');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_barcode'] = $this->language->get('button_add_barcode');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_discount'] = $this->language->get('tab_discount');
		$this->data['tab_special'] = $this->language->get('tab_special');
		$this->data['tab_barcode'] = $this->language->get('tab_barcode');
		$this->data['tab_description'] = $this->language->get('tab_description');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}
		
 		if (isset($this->error['sku'])) {
			$this->data['error_sku'] = $this->error['sku'];
		} else {
			$this->data['error_sku'] = '';
		}
		
		if (isset($this->error['department'])) {
			$this->data['error_department'] = $this->error['department'];
		} else {
			$this->data['error_department'] = '';
		}
		
		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = '';
		}
		
		if (isset($this->error['brand'])) {
			$this->data['error_brand'] = $this->error['brand'];
		} else {
			$this->data['error_brand'] = '';
		}
		
		if (isset($this->error['bin'])) {
			$this->data['error_bin'] = $this->error['bin'];
		} else {
			$this->data['error_bin'] = '';
		}
		
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		
		if (isset($this->error['barcode'])) {
			$this->data['error_barcode'] = $this->error['barcode'];
			$this->data['tab_barcodeactive'] = $this->error['tab_barcode'];
			$this->data['error_warning'] =$this->error['barcode'];
			$this->data['tab_activec'] = false;
		} else {
			$this->data['error_barcode'] = '';
		}	

		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}	
		
		if (isset($this->error['tab_general'])) {
			$this->data['error_general'] = $this->error['tab_general'];
		} else {
			$this->data['error_general'] = '';
		}
		
		if (isset($this->error['tab_price'])) {
			$this->data['error_tab_price'] = $this->error['tab_price'];
		} else {
			$this->data['error_tab_price'] = '';
		}
		
		if (isset($this->error['tab_barcode'])) {
			$this->data['error_tab_barcode'] = $this->error['tab_barcode'];
		} else {
			$this->data['error_tab_barcode'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}	

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'		=> $this->url->link('inventory/search', 'token=' . $this->session->data['token'], 'SSL'),			
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);

		if (!isset($this->request->get['product_id'])) {
			$this->data['action'] = $this->url->link('inventory/search/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('inventory/search/update', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url, 'SSL');

		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
			if(empty($product_info)) {
				$this->redirect($this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url, 'SSL'));	
			}
		}
		
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		// Category
		$this->load->model('inventory/category');
		$this->data['category_collection'] = $this->model_inventory_category->getCategorys();
		
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();
		
		// Uom Collection
		$this->load->model('master/uom');
		$this->data['uom_collection'] = $this->model_master_uom->getUoms();
		
		// Bin Collection
		$this->load->model('master/bin');
		$this->data['bin_collection'] = $this->model_master_bin->getBins();
		
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		
		$this->data['token'] = $this->session->data['token'];
		$this->data['cancel'] = $this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($product_info)) {
			$this->data['name'] = $product_info['name'];
		} else {
			$this->data['name'] = '';
		}
		
		$inventoryType	= $this->config->get('config_inventory_increment');
		
		if(isset($this->request->get['product_id'])) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
			if (!empty($product_info)) {
				$this->data['sku'] = $product_info['sku'];
				$this->data['auto_inventory'] = '';	
			} else {
				$this->data['sku'] = '';
				$this->data['auto_inventory'] = $inventoryType;	
			}
		} else {
			if (empty($inventoryType)) {
				$this->data['auto_inventory'] = $inventoryType;
				$inventory_sku	= $this->model_inventory_search->getInventoryAutoId();
				$this->data['sku'] = $inventory_sku['sku'];
			} elseif(isset($this->request->post['sku'])) {
				$this->data['sku'] = $this->request->post['sku'];
				$this->data['auto_inventory'] = $inventoryType;	
			} else {
				$this->data['auto_inventory'] = $inventoryType;
				$this->data['sku'] = '';
			}
		}
		
		$this->load->model('inventory/category');

		if (isset($this->request->post['product_category'])) {
			$this->data['product_category'] = $this->request->post['product_category'];
		} elseif (isset($product_info)) {
			$this->data['product_category'] = $this->model_inventory_inventory->getProductCategories($this->request->get['product_id']);
		} else {
			$this->data['product_category'] = array();
		}
		
		if (isset($this->request->post['product_barcode'])) {
			$this->data['product_barcodes'] = $this->request->post['product_barcode'];
		} elseif (isset($product_info)) {
			$this->data['product_barcodes'] = $this->model_inventory_inventory->getProductBarcodes($this->request->get['product_id']);
		} else {
			$this->data['product_barcodes'] = array();
		}
		
		if (isset($this->request->post['product_brand'])) {
			$this->data['product_brand'] = $this->request->post['product_brand'];
		} elseif (isset($product_info)) {
			$this->data['product_brand'] = $this->model_inventory_inventory->getProductBrands($this->request->get['product_id']);
		} else {
			$this->data['product_brand'] = array();
		}
		if (isset($this->request->post['product_department'])) {
			
			$this->data['product_department'] = $this->request->post['product_department'];
		} elseif (isset($product_info)) {
			$this->data['product_department'] = $this->model_inventory_inventory->getProductDepartments($this->request->get['product_id']);
		} else {
			$this->data['product_department'] = array();
		}
		
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($product_info)) {
			$this->data['description'] = $product_info['description'];
		} else {
			$this->data['description'] = '';
		}
		
		if (isset($this->request->post['short_description'])) {
			$this->data['short_description'] = $this->request->post['short_description'];
		} elseif (!empty($product_info)) {
			$this->data['short_description'] = $product_info['short_description'];
		} else {
			$this->data['short_description'] = '';
		}
		
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
		} elseif (!empty($product_info)) {
			$this->data['vendor'] = $product_info['vendor_id'];
		} else {
			$this->data['vendor'] = '';
		}
		
		if (isset($this->request->post['bin'])) {
			$this->data['bin'] = $this->request->post['bin'];
		} elseif (!empty($product_info)) {
			$this->data['bin'] = $product_info['bin_id'];
		} else {
			$this->data['bin'] = '';
		}
		

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();
	
		if (isset($this->request->post['product_description'])) {
			$this->data['product_description'] = $this->request->post['product_description'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description'] = $this->model_inventory_inventory->getProductDescriptions($this->request->get['product_id']);
		} else {
			$this->data['product_description'] = array();
		}
		
		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($product_info)) {
			$this->data['image'] = $product_info['image'];
		} else {
			$this->data['image'] = '';
		}

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($product_info) && $product_info['image'] && file_exists(DIR_IMAGE . $product_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 100, 100);
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', 100, 100);
		}

		
		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
		} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
		} else {
			$this->data['price'] = '';
		}

		$this->load->model('localisation/tax_class');

		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['tax_class_id'] = $product_info['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = 0;
		}

		if (isset($this->request->post['date_available'])) {
			$this->data['date_available'] = $this->request->post['date_available'];
		} elseif (!empty($product_info)) {
			$this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
		} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

		if (isset($this->request->post['quantity'])) {
			$this->data['quantity'] = $this->request->post['quantity'];
		} elseif (!empty($product_info)) {
			$this->data['quantity'] = $product_info['quantity'];
		} else {
			$this->data['quantity'] = 1;
		}

		if (isset($this->request->post['minimum'])) {
			$this->data['minimum'] = $this->request->post['minimum'];
		} elseif (!empty($product_info)) {
			$this->data['minimum'] = $product_info['minimum'];
		} else {
			$this->data['minimum'] = 1;
		}

		if (isset($this->request->post['subtract'])) {
			$this->data['subtract'] = $this->request->post['subtract'];
		} elseif (!empty($product_info)) {
			$this->data['subtract'] = $product_info['subtract'];
		} else {
			$this->data['subtract'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($product_info)) {
			$this->data['sort_order'] = $product_info['sort_order'];
		} else {
			$this->data['sort_order'] = 1;
		}

		$this->load->model('localisation/stock_status');

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if (isset($this->request->post['stock_status_id'])) {
			$this->data['stock_status_id'] = $this->request->post['stock_status_id'];
		} elseif (!empty($product_info)) {
			$this->data['stock_status_id'] = $product_info['stock_status_id'];
		} else {
			$this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['weight'])) {
			$this->data['weight'] = $this->request->post['weight'];
		} elseif (!empty($product_info)) {
			$this->data['weight'] = $product_info['weight'];
		} else {
			$this->data['weight'] = '';
		}

		$this->load->model('localisation/weight_class');

		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['weight_class_id'])) {
			$this->data['weight_class_id'] = $this->request->post['weight_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['weight_class_id'] = $product_info['weight_class_id'];
		} else {
			$this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
		}

		if (isset($this->request->post['length'])) {
			$this->data['length'] = $this->request->post['length'];
		} elseif (!empty($product_info)) {
			$this->data['length'] = $product_info['length'];
		} else {
			$this->data['length'] = '';
		}
		
		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_specials'] = $this->model_inventory_inventory->getProductSpecials($this->request->get['product_id']);
		} else {
			$this->data['product_specials'] = array();
		}

		$this->data['route'] = 'inventory/search';

		$this->template = 'inventory/inventory_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	
	protected function validateForm() {
	
		if (!$this->request->post['name']) {
			$this->error['name'] = "Please enter product name.";
			$this->error['tab_general'] = true;
		}
		
		if (!$this->request->post['sku']) {
			$this->error['sku'] = "Please enter product inventory code.";
			$this->error['tab_general'] = true;
		}
		
		$this->load->model('inventory/inventory');
		$inventory_info = $this->model_inventory_inventory->getInventoryDetails($this->request->post['sku'],$this->request->get['product_id']);
		if ($this->request->post['sku'] && !empty($inventory_info)) {
			$this->error['sku'] = "Please enter unique inventory code.";
			$this->error['tab_general'] = true;
		}
		$inventory_info = $this->model_inventory_inventory->getInventoryDetailsById($this->request->get['product_id']);
		$inventoryType	= $this->config->get('config_inventory_increment');
		if(!empty($inventoryType)) {
			$company_id	= $this->session->data['company_id'];
			$likeIncrement	= $company_id."000";
			if(!isset($this->request->get['product_id'])) {
				if (strpos($this->request->post['sku'],$likeIncrement) !== false) {
					$this->error['sku'] = "Sku similar to auto incremant. Please change sku value.";
					$this->error['tab_general'] = true;
				}
			}
		}
		
		/*if (!$this->request->post['product_department']) {			
			$this->error['department'] = "Please select any one department.";
			$this->error['tab_general'] = true;
		}
		
		if (!$this->request->post['product_category']) {
			$this->error['category'] = "Please select any one category.";
			$this->error['tab_general'] = true;
		}
		
		if (!$this->request->post['product_brand']) {
			$this->error['brand'] = "Please select any one brand.";
			$this->error['tab_general'] = true;
		}*/		
		
		/*if (!$this->request->post['vendor']) {
			$this->error['vendor'] = "Please select any one vendor.";
			$this->error['tab_general'] = true;
		}*/
		
		if (!$this->request->post['price']) {
			$this->error['price'] = "Please enter product price.";
			$this->error['tab_price'] = true;
		}
		
		$barCodesArray	= array();
		if ($this->request->post['product_barcode']) {
			foreach($this->request->post['product_barcode'] as $barcode) {
				$barcode_info = $this->model_inventory_inventory->getBarcodeDetails($barcode['barcode'],$this->request->get['product_id']);
				if(in_array($barcode['barcode'],$barCodesArray) || !empty($barcode_info) || empty($barcode['barcode'])) {
					$this->error['barcode'] = "Please enter unique or valid barcode.";
					$this->error['tab_barcode'] = true;
				} else {
					$barCodesArray[]	= $barcode['barcode'];
				}
			}
		}
		if (!$this->error) {
			return true;
		} else {
			$this->error['warning'] = "Please check error tab and enter mandatory field";
			return false;
		}
	}
	
}
?>