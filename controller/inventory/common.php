<?php
class ControllerInventoryCommon extends Controller {
	private $error = array();
 
	public function index() {
		// print_r('654987984'); exit;
		
		$this->language->load('inventory/common'); 
		$this->load->model('setting/setting');

		$this->document->setTitle($this->language->get('heading_title'));
		if (($this->request->server['REQUEST_METHOD'] == 'POST') /*&& $this->validate()*/) {
			$this->model_setting_setting->editSetting('config', $this->request->post);

			if ($this->config->get('config_currency_auto')) {
				$this->load->model('localisation/currency');
				$this->model_localisation_currency->updateCurrencies();
			}	
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('inventory/category', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_master'] = $this->language->get('text_master');	
		$this->data['text_inventory'] = $this->language->get('text_inventory');
		$this->data['text_pricing'] = $this->language->get('text_pricing');		
		
		
		if($this->user->hasPermission('access', 'inventory/department') || $this->user->hasPermission('modify', 'inventory/department')){
			$this->data["master"][] = array( "link" => $this->url->link("inventory/department", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text"  => $this->language->get('text_main_category'),
				                               "icon"  =>'fa fa-th-large'

				                               );
		}

		if($this->user->hasPermission('access', 'inventory/category') || $this->user->hasPermission('modify', 'inventory/category')){
			$this->data["master"][] = array( "link" => $this->url->link("inventory/category", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => $this->language->get('text_sub_category'),
				                               "icon" =>'fa fa-th'
				                               );
		}

		if($this->user->hasPermission('access', 'inventory/category/subCateList') || $this->user->hasPermission('modify', 'inventory/category/subCateList')){
			$this->data["master"][] = array( "link" => $this->url->link("inventory/category/subCateList", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => 'Sub Category',
				                               "icon" =>'fa fa-th'
				                               );
		}

		if($this->user->hasPermission('access', 'inventory/brand') || $this->user->hasPermission('modify', 'inventory/brand')){
			$this->data["master"][] = array( "link" => $this->url->link("inventory/brand", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => $this->language->get('text_brand'),
				                               "icon" =>'fa fa-location-arrow'
				                               );
		}

		if($this->user->hasPermission('access', 'master/uom') || $this->user->hasPermission('modify', 'master/uom')){
			$this->data["master"][] =  array( "link" =>$this->url->link("master/uom", 'token=' . $this->session->data['token'], 'SSL'),
		                               "Text" => $this->language->get('text_uom_master'),
		                               "icon" =>'fa fa-user'
		                               );
		}
		if($this->user->hasPermission('access', 'master/uomtype') || $this->user->hasPermission('modify', 'master/uomtype')){
			$this->data["master"][] =  array( "link" =>$this->url->link("master/uomtype", 'token=' . $this->session->data['token'], 'SSL'),
			                               "Text" => $this->language->get('text_uom_type_master'),
			                               "icon" =>'fa fa-user'
			                               );
		}
		/*if($this->user->hasPermission('access', 'inventory/inventory/insert') || $this->user->hasPermission('modify', 'inventory/inventory/insert')){*/
			if($this->config->get('config_manage_inventory')== 10) { 
		$this->data["inventory"][] = array( "link" => $this->url->link("inventory/inventory/insert", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => $this->language->get('text_new_nventory'),
				                               "icon" =>'fa fa-plus'
				                               );
	}
		//}

		if($this->user->hasPermission('access', 'inventory/inventory') || $this->user->hasPermission('modify', 'inventory/inventory')){
			$this->data["inventory"][] = array( "link" => $this->url->link("inventory/inventory", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => 'Product',
				                               "icon" =>'fa fa-pencil'
				                               );
		}
		if($this->user->hasPermission('access', 'inventory/product_import') || $this->user->hasPermission('modify', 'inventory/product_import')){
			$this->data["inventory"][] = array( "link" => $this->url->link("inventory/product_import", 'token=' . $this->session->data['token'], 'SSL'),
				                               "Text" => 'Product Import',
				                               "icon" =>'fa fa-download'
				                               );
		}
		// if($this->user->hasPermission('access', 'inventory/barcode') || $this->user->hasPermission('modify', 'inventory/barcode')){
		// 	$this->data["inventory"][] = array( "link" => $this->url->link("inventory/barcode", 'token=' . $this->session->data['token'], 'SSL'),
		// 		                                "altkey"=> $this->language->get('alt_barcode'),
		// 		                                 "id"    => $this->language->get('barcode_id'),
		// 		                               "Text" => $this->language->get('text_barcode_print'),
		// 		                               "icon" =>'fa fa-barcode'
		// 		                               );
		// }

		// if($this->user->hasPermission('access', 'inventory/search') || $this->user->hasPermission('modify', 'inventory/search')){
		// 	$this->data["inventory"][] = array( "link" => $this->url->link("inventory/search", 'token=' . $this->session->data['token'], 'SSL'),
		// 		                                "altkey"=> $this->language->get('alt_search'),
		// 		                                 "id"    => $this->language->get('search_id'),
		// 		                               "Text" => $this->language->get('text_inventory_search'),
		// 		                               "icon" =>'fa fa-search'
		// 		                               );
		// }

		// $this->data["inventory"][] =  array( "link" =>"#",
		// 	                               "altkey"=> $this->language->get('alt_edit'),
		// 	                               "id"    => $this->language->get('qedit_id'),
		// 	                               "Text" => $this->language->get('text_quick_edit'),
		// 	                               "icon" =>'fa fa-pencil'
		// 	                               );
		/*if($this->user->hasPermission('access', 'inventory/inventorymovement') || $this->user->hasPermission('modify', 'inventory/inventorymovement')){
		$this->data["inventory"][] =  array( "link" =>$this->url->link("inventory/inventorymovement", 'token=' . $this->session->data['token'], 'SSL'),
			                               "altkey"=> $this->language->get('alt_inv_moment'),
			                                "id"    => $this->language->get('invmoment_id'),
			                               "Text" => $this->language->get('text_inventory_movement'),
			                               "icon" =>'fa fa-signal'
			                               );
		}
*/
		// if($this->user->hasPermission('access', 'inventory/discount') || $this->user->hasPermission('modify', 'inventory/discount')){	
		// 	$this->data["inventory"][] =  array( "link" => $this->url->link("inventory/discount", 'token=' . $this->session->data['token'], 'SSL'),
		// 	                               "altkey"=> $this->language->get('alt_discount'),
		// 	                                "id"    => $this->language->get('discount_id'),
		// 	                               "Text" => 'Pair Discount',
		// 	                               "icon" =>'fa fa-chevron-circle-down'
		// 	                               );
		// } 



		$this->data['location_code'] = $this->session->data['location_code'];
		$this->data['token'] = $this->session->data['token'];
		$this->template = 'inventory/common.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	public function getProductIdByBarcode()
	{
		$this->load->model('inventory/search');
		$qedit_barcode = $this->request->post['qedit_barcode'];
		$location_code = $this->request->post['location_code'];

		$Product = $this->model_inventory_search->getProductsDetailsByBarcode($qedit_barcode,$location_code);
		if(count($Product)>=1){
			echo json_encode($Product);
		}else{
			echo 0;
		}
	}

	public function updateqe()
	{	
		$this->request->post['created_by'] = $this->session->data['username'];
		$this->load->model('inventory/search');
		$this->model_inventory_search->updateqe($this->request->post);
		echo 1;
	}
	
}
?>