<?php
class ControllerInventoryProductImport extends Controller { // 01-07-2021 |^|
	private $error = array();
	public function index() {
		$this->load->model('inventory/product_import');
		$this->document->setTitle('QSM Order Import');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		if (isset($this->session->data['error'])) {
			$this->data['error'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error'] = '';
		}

		$this->data['breadcrumbs'] = array();
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Sales',
			'href'      => $this->url->link('transaction/common_sales', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['breadcrumbs'][] = array(
			'text'      => 'QSM Order Import',
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		$this->data['fileUploadAction'] = $this->url->link('inventory/product_import/insert', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['back']   = $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['clearImportedOrders'] = $this->url->link('inventory/product_import/clearImportedOrders', 'token=' . $this->session->data['token'], 'SSL');

		$products = $this->model_inventory_product_import->getTempProducts();
		$temproducts = array();
		$m = 0;
		if(!empty($products)){
			foreach ($products as $value) {
					$tempOrders[] 	= array(
						'orderID' 		=> $value['id'],
						'sku' 			=> $value['sku'],
						'name' 			=> $value['name'],
						'description' 	=> $value['description'],
						'department' 	=> $value['department'],
						'category' 		=> $value['category'],
						'subcategory' 	=> $value['subcategory'],
						'brand' 		=> $value['brand'],
						'vendor' 		=> $value['vendor'],
						'price' 		=> $value['price'],
						'cost' 			=> $value['cost'],
						'deleteBtn'     => $this->url->link('inventory/product_import/delete', 'token=' . $this->session->data['token'].'&id='.$value['id'], 'SSL')
					);
				}
			}
		$this->data['action'] = $this->url->link('inventory/product_import/insertProduct', 'token=' . $this->session->data['token'], 'SSL'); 
		if($m != 0 || empty($tempOrders)){
			$this->data['action'] = ''; 
		}
		$this->data['deleteAllBtn'] = $this->url->link('inventory/product_import/deleteAllData', 'token=' . $this->session->data['token'], 'SSL'); 

		$this->data['products'] = $tempOrders;
		$this->data['token']  = $this->session->data['token'];
		$this->data['route']  = 'inventory/product_import';
		$this->template 	  = 'inventory/product_import.tpl';
		$this->children 	  = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
    function readCSV($csvFile){
        $file_handle = fopen($csvFile, 'r');
        while (!feof($file_handle) ) {
            $line_of_text[] = fgetcsv($file_handle, '',",");
        }
        fclose($file_handle);
        return $line_of_text;
	}
    
	public function insert() {
		$this->load->model('inventory/product_import');

		if(!empty($_FILES['file']['size'])){

			$res = $this->uploadFile();
			if($res['status']){
                $products   = $this->readCSV(DIR_SERVER.'doc/product_import/'.$res['new_file_name']);

					if(!empty($products)){
						$header = $products[0];
				        if($header[0]=='sku' && $header[1] == 'name' && $header[2] == 'description' && $header[3] == 'department' && $header[4] == 'category' && $header[5] == 'subcategory' && $header[6] == 'brand' && $header[7] == 'vendor' && $header[8] == 'price' && $header[9] == 'cost'){
					        foreach ($products as $key => $product) {
					            if($key != 0 && !empty($product)){
					                if($product[0] != ''){
	                                    $this->model_inventory_product_import->insertTempTable($product);
	                                }
					            }
					        }
						}else{
							$this->session->data['error']   = 'Error: Uploaded file format wrong';
				    	}
			    	}
				}else{
					$this->session->data['error']   = 'Error: '.$res['message'];
				}
		}else{
			$this->session->data['error']   	= 'Error: Please select file to import';
		}
		$this->redirect($this->url->link('inventory/product_import', 'token=' . $this->session->data['token'], 'SSL'));
	}
	public function uploadFile(){
        if($_FILES["file"]){
            $temp = explode(".", $_FILES["file"]["name"]);

            if(end($temp) == 'csv'){
	            $newfilename = 'Product_'.date('Y-m-d_His') . '.' . end($temp);
	            if(move_uploaded_file($_FILES["file"]["tmp_name"], DIR_SERVER."doc/product_import/" . $newfilename)){
	                return $res = array('status' => 1, 'new_file_name' => $newfilename);
	            }
            }else{
            	return $res = array('status' => 0, 'message' => 'Uploaded file format not supported!'); 
            }
        }
    }
    public function getProductDetails(){
    	$this->load->model('transaction/sales');

    	$productDetails = $this->model_transaction_sales->getProductByNamenew($this->request->post);
    	$str.='<ul id="country-list">';
    	foreach ($productDetails as $details) {
    		$str.= "<li onClick=\"selectedProduct('".$details['sku']."','".$details['product_id']."');\">".trim($details['sku']).' ('.trim($details['name']).")</li>";
    	}
    	$str.='</ul>';
    	echo $str;
    }
    public function updateQSMProduct(){
		$this->load->model('inventory/product_import');
    	echo $this->model_inventory_product_import->updateQSMProduct($this->request->post);
    }
    public function insertProduct(){
    	$this->load->model('inventory/product_import');
    	$tempProduct = $this->model_inventory_product_import->getDetailsFromTempTable();

	    if(!empty($tempProduct)){
	        foreach ($tempProduct as $product) {
	            if(!empty($product)){
	                $this->model_inventory_product_import->addProduct($product);
	            }
	        }
	    }
	    $this->redirect($this->url->link('inventory/product_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function deleteAllData(){
    	$this->load->model('inventory/product_import');
    	$this->model_inventory_product_import->deleteAllData();
    	
    	$this->redirect($this->url->link('inventory/product_import', 'token=' . $this->session->data['token'], 'SSL'));
    }
    public function delete(){
    	$this->load->model('inventory/product_import');
    	$this->model_inventory_product_import->delete($this->request->get['id']);
    	$this->redirect($this->url->link('inventory/product_import', 'token=' . $this->session->data['token'], 'SSL'));        
    }
    public function getInvoiceNo(){
		$salesLastId = $this->model_inventory_product_import->getSalesLastId();
		return $this->config->get('config_sales_prefix').date('ym').str_pad($salesLastId +1, 4, '0', STR_PAD_LEFT);
    }
    public function clearImportedOrders(){
    	$this->load->model('inventory/product_import');
    	$this->model_inventory_product_import->clearImportedOrders();
    	$this->redirect($this->url->link('inventory/product_import', 'token=' . $this->session->data['token'], 'SSL')); 	
    }
	public function getRandomCustCode(){
    	$this->load->model('setting/customers');
	    $lastCustId = $this->model_setting_customers->getCustLastId();
		return 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);
	}
}
?>