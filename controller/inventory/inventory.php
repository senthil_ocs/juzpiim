<?php 
class ControllerInventoryInventory extends Controller {
	private $error = array();
 	
	public function index() {
		$this->load->model('tool/image');
		$this->language->load('inventory/inventory');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/inventory');
		$this->getList();
	}
	public function getProductDetailsbyScan($data='', $type = '') {
			$this->load->model('inventory/inventory');
			$this->data['filter_name'] = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$_productDetails           = $this->model_inventory_inventory->getProductByName($this->data);
			
			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){
					if($_productDetails[$i]['unit_cost']!='' && $_productDetails[$i]['unit_cost']!='0.00'){
						$_productDetails[$i]['average_cost'] = $_productDetails[$i]['unit_cost'];
					}
					$var =  $_productDetails[$i]['product_id'].'||'.trim($_productDetails[$i]['sku']).'||'.trim($_productDetails[$i]['name']).'||'.$_productDetails[$i]['average_cost'].'||'.$_productDetails[$i]['price'].'||'.trim($_productDetails[$i]['sku_uom']).'||'.trim($_productDetails[$i]['sku_qty']);
					$str.= "<li onClick=\"selectedProduct('".$var."');\">".trim($_productDetails[$i]['sku']).' ('.trim($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
				$str = $var;
			}
			echo $str;
	}

	public function insert() {
		$this->language->load('inventory/inventory');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/inventory');	
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

			if($this->validateForm()){
				$this->request->post['image'] = $this->uploadImage();
				$InvId = $this->model_inventory_inventory->addProduct($this->request);
				$this->session->data['success'] = $this->language->get('text_success');
				
				$url = '';
				if (isset($this->request->get['filter_name'])) {
					$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
				}
				if (isset($this->request->get['filter_price'])) {
					$url .= '&filter_price=' . $this->request->get['filter_price'];
				}
				if (isset($this->request->get['filter_quantity'])) {
					$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
				}
				if(isset($this->request->post['transaction_set'])) {
					$data['transaction_set']	=  $this->request->post['transaction_set'];
				} else {
					$data['transaction_set']	= '';
				}
				if (isset($this->request->get['sort'])) {
					$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
					$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
					$url .= '&page=' . $this->request->get['page'];
				}
				$type = $this->request->get['type'];
				
				if($type == 'pos') {
					$this->redirect($this->url->link('pos/pos', 'token=' . $this->session->data['token'] . $url, 'SSL'));
				} else if($this->request->get['from'] !=''){
					$this->redirect($this->url->link('transaction/'.$this->request->get['from'], 'token=' . $this->session->data['token'], 'SSL'));
				}else {
					$url ='';
					$this->redirect($this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . $url, 'SSL'));
				}
			}
		}
		$this->getForm();
	}
	public function update() {
		$this->language->load('inventory/inventory');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/inventory');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			// printArray($this->request->post); exit;
			$this->request->post['image'] = $this->uploadImage();
			$this->model_inventory_inventory->editProduct($this->request->get['product_id'], $this->request);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['filter_department'])) {
				$url .= '&filter_department=' . $this->request->get['filter_department'];
			}
			if (isset($this->request->get['filter_category'])) {
				$url .= '&filter_category=' . $this->request->get['filter_category'];
			}
			if (isset($this->request->get['filter_sub_category'])) {
				$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
			}
			if (isset($this->request->get['filter_location_code'])) {
				$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
			}
			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}
			if (isset($this->request->get['filter_barcodes'])) {
				$url .= '&filter_barcodes=' . $this->request->get['filter_barcodes'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			$type = $this->request->get['type'];
			if($type == 'pos') {
			   $this->redirect($this->url->link('pos/pos', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} elseif($type == 'search') {
				$this->redirect($this->url->link('inventory/search', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			} else {
			   $this->redirect($this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			}
		}
		$this->data['edit_form']	= true;
		$this->getForm();
	}

	public function UpdateSpecifiedFields()
	{
		$this->language->load('inventory/inventory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateSpecfic()) {
			
			$this->model_inventory_inventory->editProductSpecified($this->request->get['product_id'], $this->request);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';			

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}			

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->session->data['success'])) {
				$this->data['success'] = $this->session->data['success'];
				unset($this->session->data['success']);
			} else {
				$this->data['success'] = '';
			}

			   //$this->redirect($this->url->link('inventory/common', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->getSpecifiedForm();
	}
 
	public function delete() { 
		
		$this->language->load('inventory/inventory'); 
		$this->document->setTitle($this->language->get('heading_title')); 
		$this->load->model('inventory/inventory');

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $product_id) {
				$strCheckTransProductCount = $this->model_inventory_inventory->checkTranscationProductCount($product_id);
				if($strCheckTransProductCount['CNT'] == 0){
					$strGetSku = $this->model_inventory_inventory->getSkuDetailsByProductId($product_id);
					$strCheckTransSalesProductCount = $this->model_inventory_inventory->checkTranscationSalesProductCount($strGetSku['sku']);
					if($strCheckTransSalesProductCount['CNT'] == 0){
						$this->model_inventory_inventory->deleteProduct($product_id);
						$this->session->data['success'] = $this->language->get('text_success');
					} else {
						$this->session->data['error'] = "This Inventory has ".$strCheckTransSalesProductCount['CNT']." transactions! You can't delete this Inventory";
					}
				} else {
					$this->session->data['error'] = "This Inventory has ".$strCheckTransProductCount['CNT']." transactions! You can't delete this Inventory";
				}
			} 
			$url = ''; 
			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			} 
			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			} 
			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			} 
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			} 
			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			} 
		}
		$this->getList();
	}

	protected function getList() {
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_department'])) {
			$filter_department = $this->request->get['filter_department'];
		} else {
			$filter_department = null;
		}
		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = null;
		}
		if (isset($this->request->get['filter_sub_category'])) {
			$filter_sub_category = $this->request->get['filter_sub_category'];
		} else {
			$filter_sub_category = null;
		} 
		if (isset($_REQUEST['filter_product_id'])) {
			$filter_product_id = $_REQUEST['filter_product_id'];
		} else {
			$filter_product_id = null;
		}

		if (isset($_REQUEST['filter_brand'])) {
			$filter_brand = $_REQUEST['filter_brand'];
		} else {
			$filter_brand = null;
		}
		if (isset($_REQUEST['filter_vendor'])) {
			$filter_vendor = $_REQUEST['filter_vendor'];
		} else {
			$filter_vendor = null;
		}

		if (isset($this->request->get['filter_barcodes'])) {
			$filter_barcodes = $this->request->get['filter_barcodes'];
		} else {
			$filter_barcodes = null;
		}
        if (isset($this->request->get['filter_location_code'])) {
       	   $location = $this->request->get['filter_location_code'];
		} else {
		   $location = $this->session->data['location_code'];
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.product_id';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		$url = '';
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '&page='.$page;

		if (isset($this->request->get['filter_barcodes'])) {
			$url .= '&filter_barcodes=' . urlencode(html_entity_decode($this->request->get['filter_barcodes'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_product_id'])) {
			$url .= '&filter_product_id=' . urlencode(html_entity_decode($this->request->get['filter_product_id'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_status'])) {
       	   $filter_status = $this->request->get['filter_status'];
		} else {
		   $filter_status = null;
		}
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		if (isset($this->request->get['filter_sub_category'])) {
			$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
		}

		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
		}		

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['filter_brand'])) {
			$url .= '&filter_brand=' . $this->request->get['filter_brand'];
		}
		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . $this->request->get['filter_vendor'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if(is_numeric($filter_name)){
			$psku = $this->model_inventory_inventory->getProductSKuByBarCode(trim($filter_name));
			$filter_name = $psku;
		}
		$company_id	= $this->session->data['company_id'];
		if (isset($company_id)) {
      		$company_info = $this->model_inventory_inventory->getCompanyDetails($company_id);
    	}
    	// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
			// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();
        
		$this->data['insert'] 	= $this->url->link('inventory/inventory/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] 	= $this->url->link('inventory/inventory/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['products'] = array();
		$data = array(
			'order'           	  => $order,
			'location' 			  => $location,
			'filter_product_id'   => $filter_product_id,
			'filter_name'	  	  => $filter_name, 
			'filter_department'   => $filter_department,
			'filter_sub_category' => $filter_sub_category,
			'filter_category'     => $filter_category,
			'filter_brand'     	  => $filter_brand,
			'filter_vendor'       => $filter_vendor,
			'filter_barcodes'	  => $filter_barcodes,
			'filter_status'   	  => $filter_status,
			'start'           	  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'           	  => $this->config->get('config_admin_limit'),
			'sort'            	  => $sort
		); 
		$this->load->model('tool/image');
	    $product_total 	= $this->model_inventory_inventory->getTotalProducts($data);
		$results 		= $this->model_inventory_inventory->getProducts($data);
		
		foreach ($results as $result) {
			$action 	= array();
			$action[] 	= array(
				'text' 	=> $this->language->get('text_edit'),
				'href' 	=> $this->url->link('inventory/inventory/update', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
			$market_place 	= array();
			$market_place[] = array(
				'text' 	=> 'Networks',
				'href' 	=> $this->url->link('inventory/inventory/market_place', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
			$multiple_images   = array();
			$multiple_images[] = array(
				'text' 	=> 'Image',
				'href' 	=> $this->url->link('inventory/inventory/multiple_images', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL')
			);
			$special = false;
			$special ='';
			$barcodes= array();
			$product_barcodes = $this->model_inventory_inventory->getProductBarcodesbySKU($result['sku']);
			if(!empty($product_barcodes)) {
				foreach ($product_barcodes  as $product_barcode) {
					$barcodes[]	= $product_barcode['barcode'];
				}
				$barcodes = implode(',<br>',$barcodes);
			}
			if(empty($barcodes)) {
				$barcodes = '';	
			}
			$delitable = '1';
			$counts = $this->model_inventory_inventory->getPurchaseAndSalesProductsCount($result['product_id']);
			if($counts['purchase'] > 0 || $counts['sales'] > 0){
				$delitable = '0';
			}

			if($result['expiry_date']){
				$result['expiry_date'] = $result['expiry_date'];
			}else{
				$result['expiry_date'] = '';
			}
			$sku_qty = number_format($result['sku_qty']-$result['reserved_qty'],2);
			$this->data['products'][] = array(
				'product_id' 	  => $result['product_id'],
				'xero_id' 	 	  => $result['xero_id'],
				'name'       	  => $result['name'],
				'sku'        	  => $result['inventory_id'],
				'barcodes'   	  => $barcodes,
				'price'      	  => $result['sku_price'],
				'sku_cost'   	  => $result['sku_cost'],
				'special'    	  => $special,
				'image'      	  => $image,
				'quantity'   	  => $sku_qty,
				'department_code' => $result['sku_department_code'],
				'category_code'   => $result['sku_category_code'],
				'brand_code'      => $result['sku_brand_code'],
				'sku_gstallowed'  => ($result['sku_gstallowed'] ? 'Yes' : 'No'),
				'expiry_date' 	  => $result['expiry_date'],
				'status'     	  => ($result['sku_status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'        => isset($this->request->post['selected']) && in_array($result['product_id'], $this->request->post['selected']),
				'action'          => $action,
				'delitable'       => $delitable,
				'market_place'    => $market_place,
				'multiple_images' => $multiple_images,
			);
		}

		$this->data['heading_title'] 	= $this->language->get('heading_title');		
		$this->data['text_enabled'] 	= $this->language->get('text_enabled');		
		$this->data['text_disabled'] 	= $this->language->get('text_disabled');		
		$this->data['text_no_results'] 	= $this->language->get('text_no_results');		
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');		
		$this->data['column_image'] 	= $this->language->get('column_image');	
		$this->data['column_code'] 		= $this->language->get('column_code');	
		$this->data['column_barcodes'] 	= $this->language->get('column_barcodes');	
		$this->data['column_name'] 		= $this->language->get('column_name');		
		$this->data['column_price'] 	= $this->language->get('column_price');		
		$this->data['column_quantity'] 	= $this->language->get('column_quantity');		
		$this->data['column_status'] 	= $this->language->get('column_status');		
		$this->data['column_action'] 	= $this->language->get('column_action');		
		$this->data['button_insert'] 	= $this->language->get('button_insert');		
		$this->data['button_delete'] 	= $this->language->get('button_delete');		
		$this->data['button_filter'] 	= $this->language->get('button_filter');
		$this->data['token'] 			= $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else if(isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error_warning'] = '';
		} 
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success']  = '';
		} 
		$this->data['sort_name'] 	= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . '&sort=p.name' . $url, 'SSL');
		$this->data['sort_price'] 	= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . '&sort=pd.sku_price' . $url, 'SSL');
		$this->data['sort_quantity']= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . '&sort=p.sku_qty' . $url, 'SSL');
		$this->data['sort_status'] 	= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . '&sort=p.sku_status' . $url, 'SSL');
		$this->data['sort_order'] 	= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');
		
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		// Category
		$this->load->model('inventory/category');
		$this->data['category_collection'] = $this->model_inventory_category->getCategorys();

		$pagination 		= new Pagination();
		$pagination->total 	= $product_total;
		$pagination->page 	= $page;
		$pagination->limit 	= $this->config->get('config_admin_limit');
		$pagination->text 	= $this->language->get('text_pagination');
		$pagination->url 	= $this->url->link('inventory/inventory', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');
		$this->data['pagination'] = $pagination->render();

		$this->data['filter_sub_category']	= $filter_sub_category;
		$this->data['filter_department']	= $filter_department;
		$this->data['filter_barcodes'] 		= $filter_barcodes;
		$this->data['filter_category'] 		= $filter_category;
		$this->data['filter_status']		= $filter_status;
		$this->data['filter_name'] 			= $filter_name;
		$this->data['filter_sku']			= $filter_sku;
		$this->data['filter_product_id']	= $filter_product_id;
		$this->data['order'] 				= $order;
		$this->data['sort']  				= $sort;
		$this->data['Tolocations'] 			= $this->cart->getLocation($this->session->data['location_code']);
		$this->data['filter_location_code'] = $location;
		
		$this->data['route'] 	  = 'inventory/inventory';
		$this->data['home'] 	  = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory']  = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');

		$this->template = 'inventory/inventory_list.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	protected function getForm() {

		$this->load->model('tool/image');
		$this->data['heading_title'] 	= $this->language->get('heading_title');
		$this->data['text_enabled'] 	= $this->language->get('text_enabled');
		$this->data['text_disabled'] 	= $this->language->get('text_disabled');
		$this->data['text_none'] 		= $this->language->get('text_none');
		$this->data['text_yes'] 		= $this->language->get('text_yes');
		$this->data['text_no'] 			= $this->language->get('text_no');
		$this->data['text_plus'] 		= $this->language->get('text_plus');
		$this->data['text_minus'] 		= $this->language->get('text_minus');
		$this->data['text_default'] 	  = $this->language->get('text_default');
		$this->data['text_image_manager']  = $this->language->get('text_image_manager');
		$this->data['text_browse'] 		  = $this->language->get('text_browse');
		$this->data['text_clear'] 		= $this->language->get('text_clear');
		$this->data['text_option'] 		= $this->language->get('text_option');
		$this->data['text_option_value']= $this->language->get('text_option_value');
		$this->data['text_select'] 		= $this->language->get('text_select');
		$this->data['text_none'] 		= $this->language->get('text_none');
		$this->data['text_percent'] 	= $this->language->get('text_percent');
		$this->data['text_amount'] 		= $this->language->get('text_amount');
		$this->data['entry_sku'] 		= $this->language->get('entry_code');
		$this->data['entry_name'] 		= $this->language->get('entry_name');
		$this->data['entry_image'] 		= $this->language->get('entry_image');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_category'] 	= $this->language->get('entry_category');
		$this->data['entry_brand'] 		= $this->language->get('entry_brand');
		$this->data['entry_origin'] 	= $this->language->get('entry_origin');
		$this->data['entry_uom'] 		= $this->language->get('entry_uom');
		$this->data['entry_bin'] 		 = $this->language->get('entry_bin');
		$this->data['entry_vendor'] 	   = $this->language->get('entry_vendor');
		$this->data['entry_description'] 	 = $this->language->get('entry_description');
		$this->data['entry_shortdescription'] = $this->language->get('entry_shortdescription');
		$this->data['entry_remarks'] 		 = $this->language->get('entry_remarks');
		$this->data['entry_qoh'] 			= $this->language->get('entry_qoh');
		$this->data['entry_special'] 		= $this->language->get('entry_special');
		$this->data['entry_allowzeroprice'] = $this->language->get('entry_allowzeroprice');
		$this->data['entry_nontaxitem'] 	= $this->language->get('entry_nontaxitem');
		$this->data['entry_nontaxitem'] 	= $this->language->get('entry_nontaxitem');
		$this->data['entry_status'] 		= $this->language->get('entry_status');
		$this->data['entry_discontinued'] 	= $this->language->get('entry_discontinued');
		$this->data['entry_price']  		= $this->language->get('entry_price');
		$this->data['entry_specialprice'] 	= $this->language->get('entry_specialprice');
		$this->data['entry_discountcode'] 	= $this->language->get('entry_discountcode');
		$this->data['entry_unitcost'] 		= $this->language->get('entry_unitcost');
		$this->data['entry_avgcost'] 		= $this->language->get('entry_avgcost');
		$this->data['entry_profit'] 		= $this->language->get('entry_profit');
		$this->data['entry_barcode'] 		= $this->language->get('entry_barcode');
		$this->data['entry_unit_cost'] 		= $this->language->get('entry_unit_cost');
		$this->data['entry_average_cost']	= $this->language->get('entry_average_cost');
		$this->data['entry_minimum'] 		= $this->language->get('entry_minimum');
		$this->data['entry_date_available']	= $this->language->get('entry_date_available');
		$this->data['entry_quantity'] 		= $this->language->get('entry_quantity');
		$this->data['entry_stock_status']	= $this->language->get('entry_stock_status');
		$this->data['entry_price']  		= $this->language->get('entry_price');
		$this->data['entry_tax_class'] 		= $this->language->get('entry_tax_class');
		$this->data['entry_subtract'] 		= $this->language->get('entry_subtract');
		$this->data['entry_weight_class']	= $this->language->get('entry_weight_class');
		$this->data['entry_weight'] 		= $this->language->get('entry_weight');
		$this->data['entry_length'] 		= $this->language->get('entry_length');
		$this->data['entry_filter'] 		= $this->language->get('entry_filter');
		$this->data['entry_text']   		= $this->language->get('entry_text');
		$this->data['entry_sort_order'] 	= $this->language->get('entry_sort_order');
		$this->data['entry_status'] 		= $this->language->get('entry_status');
		$this->data['entry_date_start'] 	= $this->language->get('entry_date_start');
		$this->data['entry_date_end'] 		= $this->language->get('entry_date_end');
		$this->data['entry_priority'] 		= $this->language->get('entry_priority');
		$this->data['entry_special_label'] 	= $this->language->get('entry_special_label');
		$this->data['button_save']  		= $this->language->get('button_save');
		$this->data['button_cancel'] 		= $this->language->get('button_cancel');
		$this->data['button_add_option']	 = $this->language->get('button_add_option');
		$this->data['button_add_attribute']    = $this->language->get('button_add_attribute');
		$this->data['button_add_option_value']  = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] 	   = $this->language->get('button_add_discount');
		$this->data['button_add_special'] 	 = $this->language->get('button_add_special');
		$this->data['button_add_barcode'] 	= $this->language->get('button_add_barcode');
		$this->data['button_add_image'] 	= $this->language->get('button_add_image');
		$this->data['button_remove'] 		= $this->language->get('button_remove');
		$this->data['tab_activec']  		= true;
		$this->data['tab_general']  		= $this->language->get('tab_general');
		$this->data['tab_data'] 			= $this->language->get('tab_data');
		$this->data['tab_discount'] 		= $this->language->get('tab_discount');
		$this->data['tab_special']  		= $this->language->get('tab_special');
		$this->data['tab_barcode']  		= $this->language->get('tab_barcode');
		$this->data['tab_description'] 		= $this->language->get('tab_description');
		$this->data['tab_stock']    		= $this->language->get('tab_stock');
		$this->load->model('setting/company');
		
    	if(isset($this->request->get['errortrans_no'])){
			$this->error['warning'] = $_SESSION['purchase_error'];
			unset($_SESSION['purchase_error']);
		}
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}	
		if (isset($this->error['skuprice'])) {
			$this->data['error_skuprice'] = $this->error['skuprice'];
		} else {
			$this->data['error_skuprice'] = '';
		}
		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = '';
		}		
		if (isset($this->error['brand'])) {
			$this->data['error_brand'] = $this->error['brand'];
		} else {
			$this->data['error_brand'] = '';
		}
		if (isset($this->error['bin'])) {
			$this->data['error_bin'] = $this->error['bin'];
		} else {
			$this->data['error_bin'] = '';
		}
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		if (isset($this->error['barcode'])) {
			$this->data['error_barcode']     = $this->error['barcode'];
			$this->data['tab_barcodeactive'] = $this->error['tab_barcode'];
			$this->data['error_warning'] 	 = $this->error['barcode'];
			$this->data['tab_activec'] 	 	 = false;
		} else {
			$this->data['error_barcode'] = '';
		}
		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}
		if (isset($this->error['tab_general'])) {
			$this->data['error_general'] = $this->error['tab_general'];
		} else {
			$this->data['error_general'] = '';
		}
		if (isset($this->error['tab_price'])) {
			$this->data['error_tab_price'] = $this->error['tab_price'];
		} else {
			$this->data['error_tab_price'] = '';
		}
		if (isset($this->error['tab_barcode'])) {
			$this->data['error_tab_barcode'] = $this->error['tab_barcode'];
		} else {
			$this->data['error_tab_barcode'] = '';
		}
		$url = '';
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}
		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		if (isset($this->request->get['filter_department'])) {
			$url .= '&filter_department=' . $this->request->get['filter_department'];
		}
		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . $this->request->get['filter_category'];
		}
		if (isset($this->request->get['filter_sub_category'])) {
			$url .= '&filter_sub_category=' . $this->request->get['filter_sub_category'];
		}
		if (isset($this->request->get['filter_location_code'])) {
			$url .= '&filter_location_code=' . $this->request->get['filter_location_code'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['filter_barcodes'])) {
			$url .= '&filter_barcodes=' . $this->request->get['filter_barcodes'];
		}
		$type = $this->request->get['type'];
		
		$this->data['location'] = $this->session->data['location_code'];
    	if($this->data['location'] == 'HQ'){
    		$this->data['locations'] = $this->model_inventory_inventory->getLocations();
    	}else{
    		$this->data['locations'] = $this->model_inventory_inventory->getLocationss($this->data['location']);
    	} 
    	$this->data['edit_form'] = 0;
		if (isset($this->request->get['product_id'])) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
			$this->data['parent_sku'] = $product_info['parent_sku'];
			$this->data['edit_form'] = 1;
			$location_codes = $this->model_inventory_inventory->getStockdetails($product_info['sku'],$this->data['location']);
			// $product_info['price'] = $location_codes[0]['sku_price'];
			$product_info['minimum'] = $location_codes[0]['sku_min'];
			foreach ($location_codes as $loc) {
                $Reservedstock = $this->model_inventory_inventory->getReservedStockdetails($loc['location_Code'],$loc['sku']);
				$this->data['location_codes'][] = array(
					'location_code'	=> $loc['location_Code'],
					'sku'			=> $loc['sku'],
					'sku_qty'		=> $loc['sku_qty'],
					'reserved_qty'	=> $Reservedstock['reserved_qty'],
					'qoh_qty'		=> $loc['sku_qty']-$Reservedstock['reserved_qty'],
					'sku_salesQty' 	=> $loc['sku_salesQty'],
					'sku_cost'      => $loc['sku_cost'],
					'sku_price'     => $loc['sku_price'],
					'sku_avg'		=> $loc['sku_avg'],
					'sku_min'       => $loc['sku_min'],
					'sku_reorder'	=> $loc['sku_reorder']
				);	
			}			
		}else{
			$this->data['locations'] = $this->model_inventory_inventory->getLocations(); 
			$import_info = '';
			if(isset($this->request->get['lazada_id'])){
				$import_info = $this->model_inventory_inventory->getlazadaInfo($this->request->get['lazada_id']);
				$import_info['sku']   = $import_info['SellerSKU']; 
				$import_info['name']  = $import_info['ItemName']; 
				$import_info['price'] = $import_info['UnitPrice']; 
			}
			else if(isset($this->request->get['qsm_id'])){
				$import_info = $this->model_inventory_inventory->getQsmInfo($this->request->get['qsm_id']);
				$import_info['sku']  = str_replace("'", '', $import_info['OptionCode']);
				$import_info['name'] = $import_info['Item'];
				$import_info['price']= $import_info['SellPrice'];
			}
		} 

		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		// Category
		$this->load->model('inventory/category');
		$this->data['category_collection'] = $this->model_inventory_category->getCategorys();

		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();

		// Uom Collection
		$this->load->model('master/uom');
		$this->data['uom_collection'] = $this->model_master_uom->getUoms();
		// Bin Collection
		$this->load->model('master/bin');
		$this->data['bin_collection'] = $this->model_master_bin->getBins();

		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		 
		if (!empty($this->request->get['product_id'])) {
		    $this->load->model('transaction/purchase');
		    $this->data['unit_cost'] = $product_info['sku_cost'];
			$this->data['average_cost'] = $product_info['avg_cost'];
		} else {
		    $this->data['average_cost'] = '';
			$this->data['unit_cost'] = '';
		}		
		$this->data['token'] = $this->session->data['token'];		

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($product_info)) {
			$this->data['name'] = $product_info['name'];
		} elseif (isset($this->request->get['filter_name'])) {
			$this->data['name'] = $this->request->get['filter_name'];
		} elseif (isset($import_info['name'])) {
			$this->data['name'] = $import_info['name'];
		} else {
			$this->data['name'] = '';
		} 
		if (isset($this->request->post['expiry_date'])) {
			$this->data['expiry_date'] = $this->request->post['expiry_date'];
		} elseif (!empty($product_info['expiry_date'])) {
			$this->data['expiry_date'] = $product_info['expiry_date']->format('Y-m-d');
		} else {
			$this->data['expiry_date'] = '';
		}
		$inventoryType	= $this->config->get('config_inventory_increment');
		if (isset($this->request->post['sku'])) {
			$this->data['sku'] = $this->request->post['sku'];
		} elseif (isset($product_info)) {
			$this->data['sku'] = $product_info['sku'];
		} elseif (isset($import_info['sku'])) {
			$this->data['sku'] = $import_info['sku'];
		} else {
			$this->data['sku'] = '';
		}
		if (isset($this->request->post['transaction_set'])) {
			$this->data['transaction_set'] = $this->request->post['transaction_set'];
		} else { 
			$code = $this->model_inventory_inventory->getInventoryLastId();
			$this->data['transaction_set'] = '';  
		} 
		$this->load->model('inventory/category'); 
		if (isset($this->request->post['product_category'])) {
			$this->data['product_category'] = $this->request->post['product_category'];
		} elseif (isset($product_info)) { 
			$this->data['product_category'] = trim($product_info['sku_category_code']);
		} else {
			$this->data['product_category'] = array();
		} 
		if (isset($this->request->post['sub_category'])) {
			$this->data['sub_category'] = $this->request->post['sub_category'];
		} elseif (isset($product_info)) {
			$this->data['sub_category'] = trim($product_info['sku_subcategory_code']);
		} else {
			$this->data['sub_category'] = '';
		} 
		if (isset($this->request->post['product_barcode'])) {
			$this->data['product_barcodes'] = $this->request->post['product_barcode'];
		} elseif (isset($product_info)) {
			$this->data['product_barcodes'] = $this->model_inventory_inventory->getProductBarcodes($this->request->get['product_id']);
		} else if(isset($this->request->get['filter_barcodes'])){
			$this->data['product_barcodes'][0] = array("sku"=>"","barcode"=>$this->request->get['filter_barcodes'],"sku_status"=>"1");
		} else {
			$this->data['product_barcodes'] = array();
		}
		if (isset($this->request->post['child_pruducts'])) {
			$this->data['child_pruducts'] = $this->request->post['child_pruducts'];
		} elseif (isset($product_info)) {
			$this->data['child_pruducts'] = $this->model_inventory_inventory->getChildProducts($product_info['product_id']);
		} else {
			$this->data['child_pruducts'] = array();
		} 
		if (isset($this->request->post['product_brand'])) {
			$this->data['product_brand'] = $this->request->post['product_brand'];
		} elseif (isset($product_info)) {
			 $this->data['product_brand'] = trim($product_info['sku_brand_code']);
		} else {
			$this->data['product_brand'] = array();
		} 
		if (isset($this->request->post['product_department'])) {
			$this->data['product_department'] = $this->request->post['product_department'];
		} elseif (isset($product_info)) {
			$this->data['product_department'] = trim($product_info['sku_department_code']);
		} else {
			$this->data['product_department'] = array();
		} 
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($product_info)) {
			$this->data['description'] = $product_info['sku_description'];
		} else {
			$this->data['description'] = '';
		} 
		if (isset($this->request->post['short_description'])) {
			$this->data['short_description'] = $this->request->post['short_description'];
		} elseif (!empty($product_info)) {
			$this->data['short_description'] = $product_info['sku_shortdescription'];
		} else {
			$this->data['short_description'] = '';
		} 
		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($product_info)) {
			$this->data['remarks'] = $product_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		} 
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
		} elseif (!empty($product_info)) {
			$this->data['vendor'] = trim($product_info['sku_vendor_code']);
		} else {
			$this->data['vendor'] = '';
		} 
		if (isset($this->request->post['sku_uom'])) {
			$this->data['product_uom'] = $this->request->post['sku_uom'];
		} elseif (!empty($product_info)) {
			$this->data['product_uom'] = $product_info['sku_uom'];
		} else {
			$this->data['product_uom'] = 'PCS';
		} 
		if (isset($this->request->post['sku_uom_type'])) {
			$this->data['sku_uom_type'] = $this->request->post['sku_uom_type'];
		} elseif (!empty($product_info)) {
			$this->data['sku_uom_type'] = $product_info['sku_uom_type'];
		} else {
			$this->data['sku_uom_type'] = '1';
		} 
		if (isset($this->request->post['sku_gstallowed'])) {
			$this->data['sku_gstallowed'] = $this->request->post['sku_gstallowed'];
		} elseif (!empty($product_info)) {
			$this->data['sku_gstallowed'] = $product_info['sku_gstallowed'];
		} else {
			$this->data['sku_gstallowed'] = '1';
		} 
		if (isset($this->request->post['bin'])) {
			$this->data['bin'] = $this->request->post['bin'];
		} elseif (!empty($product_info)) {
			$this->data['bin'] = $product_info['bin_id'];
		} else {
			$this->data['bin'] = '';
		} 
		$this->load->model('localisation/language'); 
		$this->data['languages'] = $this->model_localisation_language->getLanguages();
		$this->data['networks'] = $this->model_inventory_inventory->getNetworks();
		 
		if (isset($this->request->post['product_description'])) {
			$this->data['product_description']=$this->request->post['product_description'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description']=$this->model_inventory_inventory->getProductDescriptions($this->request->get['product_id']);
		} else {
			$this->data['product_description'] = array();
		}
		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
		} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
		} elseif (!empty($import_info['price'])) {
			$this->data['price'] = $import_info['price'];
		} else {
			$this->data['price'] = '';
		}
		if (isset($this->request->post['open_price'])) {
			$this->data['open_price'] = $this->request->post['open_price'];
		} elseif (!empty($product_info)) {
			$this->data['open_price'] = $product_info['open_price'];
		} else {
			$this->data['open_price'] = '0';
		}
		if (isset($this->request->post['quantity'])) {
			$this->data['quantity'] = $this->request->post['quantity'];
		} elseif (!empty($product_info)) {
			$this->data['quantity'] = $product_info['quantity'];
		} else {
			$this->data['quantity'] = 0;
		}
		if (isset($this->request->post['minimum'])) {
			$this->data['minimum'] = $this->request->post['minimum'];
		} elseif (!empty($product_info)) {
			$this->data['minimum'] = $product_info['minimum'];
		} else {
			$this->data['minimum'] = 0;
		}
		if (isset($this->request->post['subtract'])) {
			$this->data['subtract'] = $this->request->post['subtract'];
		} elseif (!empty($product_info)) {
			$this->data['subtract'] = $product_info['subtract'];
		} else {
			$this->data['subtract'] = 1;
		}
		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($product_info)) {
			$this->data['image'] = $product_info['sku_image'];
		} else {
			$this->data['image'] = '';
		}

		$this->load->model('localisation/stock_status');
		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if (isset($this->request->post['stock_status_id'])) {
			$this->data['stock_status_id'] = $this->request->post['stock_status_id'];
		} elseif (!empty($product_info)) {
			$this->data['stock_status_id'] = $product_info['stock_status_id'];
		} else {
			$this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['sku_status'];
		} else {
			$this->data['status'] = 1;
		}
		
		if (isset($this->request->post['ispackage'])) {
			$this->data['ispackage'] = $this->request->post['ispackage']=='1' ? 'checked':'';
		} elseif (!empty($product_info)) {
			$this->data['ispackage'] = $product_info['package']=='1'? 'checked' : '';
		} else {
			$this->data['ispackage'] = '';
		}
		if (isset($this->request->post['isserviced'])) {
			$this->data['isserviced'] = $this->request->post['isserviced']=='1' ? 'checked':'';
		} elseif (!empty($product_info)) {
			$this->data['isserviced'] = $product_info['serviced']=='1'? 'checked' : '';
		} else {
			$this->data['isserviced'] = '';
		}

		$this->data['uom_type_list'] = $this->model_inventory_inventory->getTypeListbyUOMCode($product_info['sku_uom']);
		$this->data['route'] = 'inventory/inventory';
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');

		if($this->request->get['type'] == 'search'){
			$this->data['cancel'] = $this->url->link('inventory/search', '&token='.$this->session->data['token'], 'SSL');
		}else if($this->request->get['from'] !=''){
			$this->data['cancel'] = $this->url->link('transaction/'.$this->request->get['from'], 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('inventory/inventory','&token='.$this->session->data['token'].$url,'SSL');
        }
		$this->template = 'inventory/inventory_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function getSpecifiedForm(){

		$this->load->model('tool/image');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_price'] = $this->language->get('entry_price');

		if(isset($this->request->get['product_id'])) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
		 }
		 if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
		} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
		} else {
			$this->data['price'] = '';
		}

		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($product_info)) {
			$this->data['description'] = $product_info['sku_description'];
		} else {
			$this->data['description'] = '';
		}

		$this->data['route'] = 'inventory/inventory';

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('inventory/common', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->template = 'inventory/quickedit.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());

	}

	public function getSpecifiedFormx()
	{
		$this->load->model('tool/image');
	
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_plus'] = $this->language->get('text_plus');
		$this->data['text_minus'] = $this->language->get('text_minus');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');
		
		$this->data['entry_sku'] = $this->language->get('entry_code');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_department'] = $this->language->get('entry_department');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_brand'] = $this->language->get('entry_brand');
		$this->data['entry_origin'] = $this->language->get('entry_origin');
		$this->data['entry_uom'] = $this->language->get('entry_uom');
		$this->data['entry_bin'] = $this->language->get('entry_bin');
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_shortdescription'] = $this->language->get('entry_shortdescription');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_qoh'] = $this->language->get('entry_qoh');
		$this->data['entry_special'] = $this->language->get('entry_special');
		$this->data['entry_allowzeroprice'] = $this->language->get('entry_allowzeroprice');
		$this->data['entry_nontaxitem'] = $this->language->get('entry_nontaxitem');
		$this->data['entry_nontaxitem'] = $this->language->get('entry_nontaxitem');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_discontinued'] = $this->language->get('entry_discontinued');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_specialprice'] = $this->language->get('entry_specialprice');
		$this->data['entry_discountcode'] = $this->language->get('entry_discountcode');
		$this->data['entry_unitcost'] = $this->language->get('entry_unitcost');
		$this->data['entry_avgcost'] = $this->language->get('entry_avgcost');
		$this->data['entry_profit'] = $this->language->get('entry_profit');
		$this->data['entry_barcode'] = $this->language->get('entry_barcode');
		$this->data['entry_unit_cost'] = $this->language->get('entry_unit_cost');
		$this->data['entry_average_cost'] = $this->language->get('entry_average_cost');
		
		$this->data['entry_minimum'] = $this->language->get('entry_minimum');
		$this->data['entry_date_available'] = $this->language->get('entry_date_available');
		$this->data['entry_quantity'] = $this->language->get('entry_quantity');
		$this->data['entry_stock_status'] = $this->language->get('entry_stock_status');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_subtract'] = $this->language->get('entry_subtract');
		$this->data['entry_weight_class'] = $this->language->get('entry_weight_class');
		$this->data['entry_weight'] = $this->language->get('entry_weight');
		$this->data['entry_length'] = $this->language->get('entry_length');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_text'] = $this->language->get('entry_text');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_date_start'] = $this->language->get('entry_date_start');
		$this->data['entry_date_end'] = $this->language->get('entry_date_end');
		$this->data['entry_priority'] = $this->language->get('entry_priority');
		$this->data['entry_special_label'] = $this->language->get('entry_special_label');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_attribute'] = $this->language->get('button_add_attribute');
		$this->data['button_add_option'] = $this->language->get('button_add_option');
		$this->data['button_add_option_value'] = $this->language->get('button_add_option_value');
		$this->data['button_add_discount'] = $this->language->get('button_add_discount');
		$this->data['button_add_special'] = $this->language->get('button_add_special');
		$this->data['button_add_barcode'] = $this->language->get('button_add_barcode');
		$this->data['button_add_image'] = $this->language->get('button_add_image');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['tab_activec'] = true;
		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_discount'] = $this->language->get('tab_discount');
		$this->data['tab_special'] = $this->language->get('tab_special');
		$this->data['tab_barcode'] = $this->language->get('tab_barcode');
		$this->data['tab_description'] = $this->language->get('tab_description');
		$this->data['tab_stock'] = $this->language->get('tab_stock');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = '';
		}		
		
		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = '';
		}
		
		if (isset($this->error['brand'])) {
			$this->data['error_brand'] = $this->error['brand'];
		} else {
			$this->data['error_brand'] = '';
		}
		
		if (isset($this->error['bin'])) {
			$this->data['error_bin'] = $this->error['bin'];
		} else {
			$this->data['error_bin'] = '';
		}
		
		if (isset($this->error['vendor'])) {
			$this->data['error_vendor'] = $this->error['vendor'];
		} else {
			$this->data['error_vendor'] = '';
		}
		
		if (isset($this->error['price'])) {
			$this->data['error_price'] = $this->error['price'];
		} else {
			$this->data['error_price'] = '';
		}
		
		if (isset($this->error['barcode'])) {
			$this->data['error_barcode'] = $this->error['barcode'];
			$this->data['tab_barcodeactive'] = $this->error['tab_barcode'];
			$this->data['error_warning'] =$this->error['barcode'];
			$this->data['tab_activec'] = false;
		} else {
			$this->data['error_barcode'] = '';
		}	

		if (isset($this->error['date_available'])) {
			$this->data['error_date_available'] = $this->error['date_available'];
		} else {
			$this->data['error_date_available'] = '';
		}	
		
		if (isset($this->error['tab_general'])) {
			$this->data['error_general'] = $this->error['tab_general'];
		} else {
			$this->data['error_general'] = '';
		}
		
		if (isset($this->error['tab_price'])) {
			$this->data['error_tab_price'] = $this->error['tab_price'];
		} else {
			$this->data['error_tab_price'] = '';
		}
		
		if (isset($this->error['tab_barcode'])) {
			$this->data['error_tab_barcode'] = $this->error['tab_barcode'];
		} else {
			$this->data['error_tab_barcode'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}	

		/*if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}*/

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$type = $this->request->get['type'];

		if (!isset($this->request->get['product_id'])) {
				$this->data['action'] = $this->url->link('inventory/inventory/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
				$this->data['action'] = $this->url->link('inventory/inventory/UpdateSpecifiedFields', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');
		}		

		if($type == 'pos') {
			$this->data['cancel'] = $this->url->link('pos/pos','&token=' . $this->request->get['token'], 'SSL');
		} else {
			$this->data['cancel'] = $this->url->link('inventory/common', 'token=' . $this->session->data['token'] . $url, 'SSL');
		}
		
		
		if (isset($this->request->get['product_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
		
		
			if(empty($product_info)) {
				$this->redirect($this->url->link('inventory/common', 'token=' . $this->session->data['token'] . $url, 'SSL'));	
			}
		}
		
		if(isset($product_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], 75, 75);
		} else {
			$this->data['thumb']	='';
		}
		
		
		// Departmnet
		$this->load->model('inventory/department');
		$this->data['department_collection'] = $this->model_inventory_department->getDepartments();
		
		// Category
		$this->load->model('inventory/category');
		$this->data['category_collection'] = $this->model_inventory_category->getCategorys();
		
		// Brand
		$this->load->model('inventory/brand');
		$this->data['brand_collection'] = $this->model_inventory_brand->getBrands();
		
		// Uom Collection
		$this->load->model('master/uom');
		$this->data['uom_collection'] = $this->model_master_uom->getUoms();
		
		// Bin Collection
		$this->load->model('master/bin');
		$this->data['bin_collection'] = $this->model_master_bin->getBins();
		
		// Vendor Collection
		$this->load->model('master/vendor');
		$this->data['vendor_collection'] = $this->model_master_vendor->getVendors();
		
		// Purchase Collection
		if (!empty($this->request->get['product_id'])) {
		    $this->load->model('transaction/purchase');
			$this->data['unit_cost'] = $this->model_transaction_purchase->getPurchaseUnitCost($this->request->get['product_id']);
			$this->data['average_cost'] = number_format($this->model_transaction_purchase->getPurchaseAvgCost($this->request->get['product_id']), 4);
		} else {
		    $this->data['average_cost'] = '';
			$this->data['unit_cost'] = '';
		}
		
		$this->data['token'] = $this->session->data['token'];		
		
		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($product_info)) {
			$this->data['name'] = $product_info['name'];
		} else {
			$this->data['name'] = '';
		}
		
		$inventoryType	= $this->config->get('config_inventory_increment');
		
		if(isset($this->request->get['product_id'])) {
			$product_info = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
			if (!empty($product_info)) {
				$this->data['sku'] = $product_info['sku'];
				$this->data['auto_inventory'] = '';	
			} else {
				$this->data['sku'] = '';
				$this->data['auto_inventory'] = $inventoryType;	
			}
		} else {
			if (empty($inventoryType)) {
				$this->data['auto_inventory'] = $inventoryType;
				$inventory_sku	= $this->model_inventory_inventory->getProductNextSKUfromSettings();
				$this->data['sku'] = $inventory_sku['sku'];
			} elseif(isset($this->request->post['sku'])) {
				$this->data['sku'] = $this->request->post['sku'];
				$this->data['auto_inventory'] = $inventoryType;	
			} else {
				$this->data['auto_inventory'] = $inventoryType;
				$this->data['sku'] = '';
			}
		}
		
		$this->load->model('inventory/category');

		if (isset($this->request->post['product_category'])) {
			$this->data['product_category'] = $this->request->post['product_category'];
		} elseif (isset($product_info)) {
			$this->data['product_category'] = $this->model_inventory_inventory->getProductCategories($this->request->get['product_id']);
		} else {
			$this->data['product_category'] = array();
		}
		
		if (isset($this->request->post['product_barcode'])) {
			$this->data['product_barcodes'] = $this->request->post['product_barcode'];
		} elseif (isset($product_info)) {
			$this->data['product_barcodes'] = $this->model_inventory_inventory->getProductBarcodes($this->request->get['product_id']);
		} else {
			$this->data['product_barcodes'] = array();
		}
		
		if (isset($this->request->post['product_brand'])) {
			$this->data['product_brand'] = $this->request->post['product_brand'];
		} elseif (isset($product_info)) {
			$this->data['product_brand'] = $this->model_inventory_inventory->getProductBrands($this->request->get['product_id']);
		} else {
			$this->data['product_brand'] = array();
		}
		if (isset($this->request->post['product_department'])) {
			
			$this->data['product_department'] = $this->request->post['product_department'];
		} elseif (isset($product_info)) {
			$this->data['product_department'] = $this->model_inventory_inventory->getProductDepartments($this->request->get['product_id']);
		} else {
			$this->data['product_department'] = array();
		}
		
		if (isset($this->request->post['description'])) {
			$this->data['description'] = $this->request->post['description'];
		} elseif (!empty($product_info)) {
			$this->data['description'] = $product_info['sku_description'];
		} else {
			$this->data['description'] = '';
		}
		
		if (isset($this->request->post['short_description'])) {
			$this->data['short_description'] = $this->request->post['short_description'];
		} elseif (!empty($product_info)) {
			$this->data['short_description'] = $product_info['short_description'];
		} else {
			$this->data['short_description'] = '';
		}

		if (isset($this->request->post['remarks'])) {
			$this->data['remarks'] = $this->request->post['remarks'];
		} elseif (!empty($product_info)) {
			$this->data['remarks'] = $product_info['remarks'];
		} else {
			$this->data['remarks'] = '';
		}

		
		if (isset($this->request->post['vendor'])) {
			$this->data['vendor'] = $this->request->post['vendor'];
		} elseif (!empty($product_info)) {
			$this->data['vendor'] = $product_info['vendor_id'];
		} else {
			$this->data['vendor'] = '';
		}
		
		if (isset($this->request->post['bin'])) {
			$this->data['bin'] = $this->request->post['bin'];
		} elseif (!empty($product_info)) {
			$this->data['bin'] = $product_info['bin_id'];
		} else {
			$this->data['bin'] = '';
		}
		

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();
	
		if (isset($this->request->post['product_description'])) {
			$this->data['product_description'] = $this->request->post['product_description'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_description'] = $this->model_inventory_inventory->getProductDescriptions($this->request->get['product_id']);
		} else {
			$this->data['product_description'] = array();
		}
		
		

		
		if (isset($this->request->post['price'])) {
			$this->data['price'] = $this->request->post['price'];
		} elseif (!empty($product_info)) {
			$this->data['price'] = $product_info['price'];
		} else {
			$this->data['price'] = '';
		}

		$this->load->model('localisation/tax_class');

		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['tax_class_id'])) {
			$this->data['tax_class_id'] = $this->request->post['tax_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['tax_class_id'] = $product_info['tax_class_id'];
		} else {
			$this->data['tax_class_id'] = 0;
		}

		if (isset($this->request->post['date_available'])) {
			$this->data['date_available'] = $this->request->post['date_available'];
		} elseif (!empty($product_info)) {
			$this->data['date_available'] = date('Y-m-d', strtotime($product_info['date_available']));
		} else {
			$this->data['date_available'] = date('Y-m-d', time() - 86400);
		}

		if (isset($this->request->post['quantity'])) {
			$this->data['quantity'] = $this->request->post['quantity'];
		} elseif (!empty($product_info)) {
			$this->data['quantity'] = $product_info['quantity'];
		} else {
			$this->data['quantity'] = 0;
		}

		if (isset($this->request->post['minimum'])) {
			$this->data['minimum'] = $this->request->post['minimum'];
		} elseif (!empty($product_info)) {
			$this->data['minimum'] = $product_info['minimum'];
		} else {
			$this->data['minimum'] = 1;
		}

		if (isset($this->request->post['subtract'])) {
			$this->data['subtract'] = $this->request->post['subtract'];
		} elseif (!empty($product_info)) {
			$this->data['subtract'] = $product_info['subtract'];
		} else {
			$this->data['subtract'] = 1;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($product_info)) {
			$this->data['sort_order'] = $product_info['sort_order'];
		} else {
			$this->data['sort_order'] = 1;
		}

		$this->load->model('localisation/stock_status');

		$this->data['stock_statuses'] = $this->model_localisation_stock_status->getStockStatuses();

		if (isset($this->request->post['stock_status_id'])) {
			$this->data['stock_status_id'] = $this->request->post['stock_status_id'];
		} elseif (!empty($product_info)) {
			$this->data['stock_status_id'] = $product_info['stock_status_id'];
		} else {
			$this->data['stock_status_id'] = $this->config->get('config_stock_status_id');
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		if (isset($this->request->post['weight'])) {
			$this->data['weight'] = $this->request->post['weight'];
		} elseif (!empty($product_info)) {
			$this->data['weight'] = $product_info['weight'];
		} else {
			$this->data['weight'] = '';
		}

		$this->load->model('localisation/weight_class');

		$this->data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();

		if (isset($this->request->post['weight_class_id'])) {
			$this->data['weight_class_id'] = $this->request->post['weight_class_id'];
		} elseif (!empty($product_info)) {
			$this->data['weight_class_id'] = $product_info['weight_class_id'];
		} else {
			$this->data['weight_class_id'] = $this->config->get('config_weight_class_id');
		}

		if (isset($this->request->post['length'])) {
			$this->data['length'] = $this->request->post['length'];
		} elseif (!empty($product_info)) {
			$this->data['length'] = $product_info['length'];
		} else {
			$this->data['length'] = '';
		}
		
		if (isset($this->request->post['product_special'])) {
			$this->data['product_specials'] = $this->request->post['product_special'];
		} elseif (isset($this->request->get['product_id'])) {
			$this->data['product_specials'] = $this->model_inventory_inventory->getProductSpecials($this->request->get['product_id']);
		} else {
			$this->data['product_specials'] = array();
		}

		$this->data['route'] = 'inventory/inventory';

		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');

		$this->template = 'inventory/quickedit.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	
	protected function validateForm() {
		$this->load->model('inventory/inventory');
		if (!$this->request->post['name']) {
			$this->error['warning'] = "Please enter product name.";
			$this->error['tab_general'] = true;
		}
		if (!$this->request->post['vendor']) {
			$this->error['warning'] = "Please select vendor";
			$this->error['tab_general'] = true;
		}
		if (!$this->request->post['product_category']) {
			$this->error['warning'] = "Please Select Category";
			$this->error['tab_general'] = true;
		}
		if (!$this->request->post['product_department']) {
			$this->error['warning'] = "Please Select Department";
			$this->error['tab_general'] = true;
		}

		$inventory_info = $this->model_inventory_inventory->getInventoryDetails($this->request->post['sku'],$this->request->get['product_id']);

		if ($this->request->post['sku'] =='') {
			$this->error['warning'] = "Please enter Product code";
		}
		if (($this->request->post['sku'] && !empty($inventory_info))) {
			$this->error['warning'] = "Please enter unique Product code.";
			$this->error['tab_general'] = true;
		}

		$inventory_info = $this->model_inventory_inventory->getInventoryDetailsById($this->request->get['product_id']);
		$inventoryType	= $this->config->get('config_inventory_increment');
		if(!empty($inventoryType)) {
			$company_id	= $this->session->data['company_id'];
			$likeIncrement	= $company_id."000";
			if(!isset($this->request->get['product_id'])) {
				if (strpos($this->request->post['sku'],$likeIncrement) !== false) {
					$this->error['warning'] = "Sku similar to auto incremant. Please change sku value.";
					$this->error['tab_general'] = true;
				}
			}
		}
		if($this->request->post['open_price'] !='1'){
			if ($this->request->post['price'] =='') {
				$this->error['warning'] = "Please enter product price.";
				$this->error['tab_price'] = true;
			}
			 if ($this->request->post['location_code'][0]['sku_price'] <= 0) {
				if($this->request->post['isserviced'] =='1'){
					//no need
				}else{
					$this->error['warning'] = "Please enter product SKU price";
					$this->error['tab_general1'] = true;
				}
			 }
		}

		$barCodesArray	= array();
		if ($this->request->post['product_barcode']) {
			
			foreach($this->request->post['product_barcode'] as $barcode) {
				$barcode_info = $this->model_inventory_inventory->getBarcodeDetails($barcode['barcode'],$this->request->get['product_id']);
				if(in_array($barcode['barcode'],$barCodesArray) || !empty($barcode_info) || empty($barcode['barcode']) || !empty($inventory_infob)) {
					$url ='';
					$this->error['warning'] = "Please enter unique or valid barcode.";
					$pary = $this->model_inventory_inventory->getProductDetailsByBarcode($barcode['barcode']);
					$pid  = $pary['product_id'];
					$pName  = $pary['name'];
					$url = $this->url->link('inventory/inventory/update', 'token=' . $this->session->data['token'] . '&product_id=' . $pid . $url, 'SSL');
					$barcodeExistList.="<a href=".$url." target='_blank'>".$pName."</a>,";
					$this->error['tab_barcode'] = true;
				} else {
					$barCodesArray[]	= $barcode['barcode'];
				}
			}
		}
		if (!$this->error) {
			return true;
		} else {
			if(isset($barcodeExistList)){
				$this->error['barcode'] = $this->error['barcode']."Because Given Bar codes added this Products ".$barcodeExistList;
			}
			$this->error['warning'] = $this->error['warning'];
			return false;
		}
	}

	public function validateSpecfic()
	{
		
		if (!$this->request->post['price']) {
			$this->error['price'] = "Please enter product price.";
		}
		
		
		if (!$this->error) {
			return true;
		} else {
			$this->error['warning'] = "Please check error tab and enter mandatory field";
			return false;
		}
	}

	public function autocomplete() {
		$this->load->model('inventory/inventory');
		
		if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
			$data['filter_sku'] = $this->request->get['filter_name'];
			$data['filter_barcodes'] = $this->request->get['filter_barcodes'];
		}
		if(strlen($data['filter_name'])>=3 || strlen($data['filter_barcodes'])>=3 ){
			$fresults = $this->model_inventory_inventory->getProductsTouchMenuNew($data,"touchmenu");
			$str      = '<ul id="product-list">';
			$x = 0;
			foreach ($fresults as $result) {
				$x++;
				$sku = $result['psku'];
				if(isset($this->request->get['filter_from']) && $this->request->get['filter_from']=='sales_filter'){
					$sku = $result['product_id'];
				}
				$str.= '<li class="autocomplete-suggestion" data-index="'.$x.'" tabindex ="'.$x.'" onClick="getValue1(\'' .replaceSpecials($result['name']).'\',\'' .replaceSpecials($sku).'\',\'' .replaceSpecials($result['product_id']).'\');">'.$result['name'].'</li>';
			}
		}else{
			$str = '';
		}
		$str.='</ul>';
		echo $str;
	}
	public function autocompleteForTouchForm() {
		$this->load->model('inventory/inventory');
		
		if (isset($this->request->get['filter_name'])) {
			$data['filter_name'] = $this->request->get['filter_name'];
			$data['filter_sku'] = $this->request->get['filter_name'];
			$data['filter_barcodes'] = $this->request->get['filter_name'];
		}
		if(strlen($data['filter_name'])>=4){
			$fresults = $this->model_inventory_inventory->getProductsTouchMenu($data,$type="touchmenu");
			$str ='<ul id="product-list">';
			$x=0;
			foreach ($fresults as $result) {
				$x++;
				$name = addslashes(trim($result['name']));
				$sku = trim($result['psku']);
				$str.= '<li class="autocomplete-suggestion" data-index="'.$x.'" tabindex ="'.$x.'" onClick="getValue(\'' .$name.'\',\'' .$sku.'\');">'.$name.'</li>';
			}
		}else{
			$str = '';
		}
		$str.='</ul>';
		echo $str;
	}
	
	public function category() {
	
		$json['category'] = array();
		$this->load->model('inventory/category');
		$category_info = $this->model_inventory_category->getCategoryByDept($this->request->get['department_id']);
		if ($category_info) {
			foreach($category_info as $category) {
				$json['category'][] = array(
					'category_id'        => $category['category_id'],
					'name'              => $category['category_name']
				);
			}
		}
		$this->response->setOutput(json_encode($json));
	}
	
	public function brand() {
		$json['brand'] = array();
		$this->load->model('inventory/brand');
		$brand_info = $this->model_inventory_brand->getBrandByCategory($this->request->get['category_id']);
		if ($brand_info) {
			foreach($brand_info as $brand) {
				$json['brand'][] = array(
					'brand_id'        => $brand['brand_id'],
					'name'              => $brand['brand_name']
				);
			}
		}
		$this->response->setOutput(json_encode($json));
	}
	
	public function getExtension($str) {
	
	 $i = strrpos($str,".");
	 if (!$i) { return ""; } 
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
	 
	}
	
	public function imageUploads($sku='') {
		
		if(empty($_FILES['image']['error'])) {
			$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
			$name = $_FILES['image']['name'];
			$size = $_FILES['image']['size'];
			if(strlen($name)){
				$ext = $this->getExtension($name);
				
				$datastring = file_get_contents($_FILES['image']['tmp_name']);
				$data         = unpack("H*hex", $datastring);
				$imagedata = "0x".$data['hex'];
				return $imagedata;


				/*if(in_array($ext,$valid_formats)) {
					if($size<(1024*1024)) {
						$txt	='';
						$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
						$tmp = $_FILES['image']['tmp_name'];
						if(move_uploaded_file($tmp, DIR_IMAGE.$actual_image_name)) {
							return $actual_image_name;
						}
					}
				}*/

			}
			$this->error['warning'] = 'unable to upload image. Please contact administrator';
			return false;
		} else {
			return true;
		}
	}
	public function getProductDetails() {
			$this->load->model('inventory/inventory');
			$this->data['filter_name']   = $this->request->post['sku'];
			$this->data['location_code'] = $this->request->post['location_code'];
			$this->data['selected_productsStr']= $this->request->post['selectedSku'];
			$product_row   = $this->request->post['product_row'];
			$_productDetails  = $this->model_inventory_inventory->getProductByNames($this->data);
			
			$str = '';
			if(count($_productDetails)>1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){

					$sku = replaceSpecials($_productDetails[$i]['product_id']).'_'.replaceSpecials($_productDetails[$i]['sku']);
					$str.= "<li onClick=\"selectChildProduct('".$sku."','".$product_row."');\">".($_productDetails[$i]['sku']).' ('.($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}

			if(count($_productDetails)==1){
				$str.='<ul id="country-list">';
				for($i=0;$i<count($_productDetails);$i++){

					$sku = replaceSpecials($_productDetails[$i]['product_id']).'_'.replaceSpecials($_productDetails[$i]['sku']);
					$str.= "<li onClick=\"selectChildProduct('".$sku."','".$product_row."');\">".($_productDetails[$i]['sku']).' ('.($_productDetails[$i]['name']).")</li>";
				}
				$str.='</ul>';
			}
			echo $str;
	}
	public function uploadImage(){
	    // Get Image Dimension

	    $fileinfo = @getimagesize($_FILES["image"]["tmp_name"]);
	    $width = $fileinfo[0];
	    $height = $fileinfo[1];
	    
	    $allowed_image_extension = array(
	        "png",
	        "jpg",
	        "jpeg"
	    );
	    
	    // Get image file extension
	    $file_extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
	    
	   		 // Validate file input to check if is not empty
	    if (! file_exists($_FILES["image"]["tmp_name"])) {
	            $response = '';
	    }    // Validate file input to check if is with valid extension
	    else if (! in_array($file_extension, $allowed_image_extension)) {
	            $response = '';
	    }    // Validate image file size
	    else if (($_FILES["image"]["size"] > 2000000)) {
	            $response = '';
	    } 
	    else {
			$temp = explode(".", $_FILES["image"]["name"]);
			$newfilename = round(microtime(true)) . '.' . end($temp);
	        if (move_uploaded_file($_FILES["image"]["tmp_name"], DIR_SERVER.'uploads/'.$newfilename)) {
	            $response =  $newfilename;
	        } else {
	            $response = '';
	        }
	    }
		return $response;
	}
	public function removeImage(){
		$this->load->model('inventory/inventory');
		$imageFileName = $this->request->post['imageFileName'];
		if($imageFileName != ''){
			$response = $this->model_inventory_inventory->removeImage($this->request->post['product_id']);
			$oldPicture = DIR_SERVER.'uploads/'.$imageFileName;
			/*chmod($oldPicture, 0644);*/
    		unlink($oldPicture);
		} else {
	        $response = '';
		}
		echo $response;
	}

	public function market_place() {

		$this->language->load('inventory/inventory');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		
		}
		$this->getMarketPlaceForm();
	}

	public function getMarketPlaceForm() {

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = '';
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['cancel'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->data['networks'] = $this->model_inventory_inventory->getNetworks();

		$this->data['product_networks']=$this->model_inventory_inventory->getProductNetwork($this->request->get['product_id']);

		$this->data['route'] = 'inventory/inventory';
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/inventory/market_place', '&token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['product_id'])) {
			$this->data['product_info'] = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
			$this->data['product_info']['name'] = preg_replace('/[^A-Za-z0-9 \-]/', '', $this->data['product_info']['name']);
			$this->data['product_info']['sku_shortdescription'] = $this->data['product_info']['name'] =='' ? $this->data['product_info']['sku_description'] : $this->data['product_info']['name'];
		} else {
			$this->data['product_info'] = '';
		}

		$this->template = 'inventory/market_place_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}
	public function multiple_images() {

		$this->language->load('inventory/inventory');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		}

		$this->getMultipleImagesForm();

	}

	public function getMultipleImagesForm() {

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = '';
		}

		if (isset($this->error['description'])) {
			$this->data['error_description'] = $this->error['description'];
		} else {
			$this->data['error_description'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['update_image'] = $this->url->link('inventory/inventory/update_image', 'token=' . $this->session->data['token'] . '&product_id=' . $this->request->get['product_id'] . $url, 'SSL');

		$this->data['cancel'] = $this->url->link('inventory/inventory', '&token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->data['multiple_images']=$this->model_inventory_inventory->getMultipleImages($this->request->get['product_id']);
		
		$this->data['route'] = 'inventory/inventory';
		$this->data['home'] = $this->url->link('common/home', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inven_comm'] = $this->url->link('inventory/common', '&token=' . $this->session->data['token'], 'SSL');
		$this->data['inventory'] = $this->url->link('inventory/inventory/market_place', '&token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['product_id'])) {
			$this->data['product_info'] = $this->model_inventory_inventory->getProduct($this->request->get['product_id']);
		} else {
			$this->data['product_info'] = '';
		}

		$this->template = 'inventory/multiple_images_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
		$this->response->setOutput($this->render());
	}

	public function update_image() {

		$this->load->model('inventory/inventory');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')){
			
			foreach ($this->request->post['multiple_image'] as $key => $value) {
				if(isset($value['id'])){
					$this->model_inventory_inventory->updateMultipleImage($value);
				} else {
					$files['name'] = $_FILES['multiple_image']['name'][$key];
					$files['type'] = $_FILES['multiple_image']['type'][$key];
					$files['tmp_name'] = $_FILES['multiple_image']['tmp_name'][$key];
					$files['error'] = $_FILES['multiple_image']['error'][$key];
					$files['size'] = $_FILES['multiple_image']['size'][$key];
					$value['sku'] = $this->request->post['sku'];
					$value['fileName'] = $this->uploadMultipleImage($files,$value['sku'],$value['priority']);
					$value['product_id'] = $this->request->get['product_id'];

					$this->model_inventory_inventory->addMultipleImage($value);
				}
			}
		}
			
		$this->getMultipleImagesForm();
	}

	public function updateMarketPlace(){
		$this->load->model('inventory/inventory');

		$data['price']              = $this->request->post['price'];
		$data['sku_cost']           = $this->request->post['sku_cost'];
		$data['qty']                = $this->request->post['qty'];
		$data['sku']                = $this->request->post['sku'];
		$data['prod_id']            = $this->request->post['prod_id'];
		$data['network']            = $this->request->post['network'];
		$data['network_response']   = $this->request->post['network_response'];
		$data['product_department'] = $this->request->post['prod_dept'];
		$data['name']               = $this->request->post['name'];
		$data['description']        = $this->request->post['description'];
		$product = $this->model_inventory_inventory->getProduct($data['prod_id']);

		if($this->request->post['network'] == 'Shopee' || $this->request->post['network'] == 'Lazada' || $this->request->post['network'] == 'Lazmall') {	
			if($product['sku_shortdescription'] == ''){
				$data['description']     = trim(preg_replace('/\s+/',' ', $data['name']));
				$data['description']     = str_replace('"','',$data['description']);
			} else {
				$data['description']     = trim(preg_replace('/\s+/',' ', $product['sku_shortdescription']));
				$data['description']     = str_replace('"','',$data['description']);
			}
		} else {
			if($product['sku_description'] != ''){
				$data['description']  = $product['sku_description'];
			} else {
				$data['description']  = $this->request->post['name'];
			}
		}
		$response = $this->model_inventory_inventory->networkUpdate($data);
		$this->response->setOutput(json_encode($response));
	}

	public function uploadMultipleImage($file ,$sku = '', $priority = ''){
	    
	    $fileinfo = @getimagesize($file["tmp_name"]);
	    $width = $fileinfo[0];
	    $height = $fileinfo[1];
	    
	    $allowed_image_extension = array("png","jpg","jpeg");
	    
	    $file_extension = pathinfo($file["name"], PATHINFO_EXTENSION);
	    if (! file_exists($file["tmp_name"])) {
	            $response = '';
	    }   
	    else if (! in_array($file_extension, $allowed_image_extension)) {
	            $response = '';
	    }  
	    else if (($file["size"] > 2000000)) {
	            $response = '';
	    } else {
			$temp = explode(".", $file["name"]);
			if($sku != '' && $priority != ''){
				$newfilename = $sku . '_' . round(microtime(true)) . '_' . $priority . '.' . end($temp);
			} else {
				$newfilename = round(microtime(true)) . '.' . end($temp);
			}
	        if (move_uploaded_file($file["tmp_name"], DIR_SERVER.'uploads/'.$newfilename)) {
	            $response =  $newfilename;
	        } else {
	            $response = '';
	        }
	    }
		return $response;
	}

	public function removeMultipleImage(){
		$this->load->model('inventory/inventory');
		$image_id = $this->request->post['image_id'];
		$image_name = $this->request->post['image_name'];
		if($image_name != ''){
			$response = $this->model_inventory_inventory->removeMultipleImage($this->request->post['image_id']);
			$oldPicture = DIR_SERVER.'uploads/'.$image_name;
			/*chmod($oldPicture, 0644);*/
    		unlink($oldPicture);
		} else {
	        $response = '';
		}
		echo $response;
	}


	public function ajaxGetCategory(){
		$this->load->model('inventory/category');
		if($this->request->post['department_code']!=''){
			$category = $this->model_inventory_category->ajaxGetCategory($this->request->post['department_code']);	
		}
		$category_slect = '';
		if($this->request->post['category']!=''){
			$category_slect = $this->request->post['category'];
		}
		$str = "<option value=''>Select Category</option>";
		if(count($category)> 0){
			foreach ($category as $value) {
				$select  = '';
				if($value['category_code']==$category_slect){
					$select = 'Selected';
				}
				$str .="<option value='".$value['category_code']."' ".$select.">".$value['category_name']."</option>"; 
			}
		}
		echo $str;
	}
	public function ajaxGetSubcategory(){
		$this->load->model('inventory/category');
		if($this->request->post['cate']!=''){
			$subcategory = $this->model_inventory_category->ajaxGetSubcategory($this->request->post['cate']);	
		}
		$subCate = '';
		if($this->request->post['subcate']!=''){
			$subCate = $this->request->post['subcate'];
		}
		$str = "<option value=''>Select Sub Category</option>";
		if(count($subcategory)> 0){
			foreach ($subcategory as $value) {
				$select  = '';
				if($value['subCate_code']==$subCate){
					$select = 'Selected';
				}
				$str .="<option value='".$value['subCate_code']."' ".$select.">".$value['name']."</option>"; 
			}
		}
		echo $str;
	}
	public function deleteProduct(){
		$this->load->model('inventory/inventory');
		$product_id = $this->request->post['product_id'];
		
		$counts = $this->model_inventory_inventory->getPurchaseAndSalesProductsCount($product_id);
		if($counts['purchase'] > 0 || $counts['sales'] > 0){
			$purchase = $counts['purchase'].'- Purchase, ';
			$sales    = $counts['sales'].'- Sales.';
			$res = array('status' => false, 'msg'=> "This Product has ".$purchase.$sales." You can't delete this!");
		}else{
			$this->model_inventory_inventory->deleteProduct($product_id);
			$res = array('status' => true );
		}
		$this->response->setOutput(json_encode($res));
	}
}
?>