<?php  
class ControllerInventoryInventoryMovement extends Controller { 
	private $error = array();
	          
	public function index() { 
	
		$this->language->load('inventory/barcode');

		$this->document->setTitle($this->language->get('Inventory Movement'));
		
		$this->load->model('inventory/inventorymovement');
		
		$this->getForm();
		
  	}

  	public function getDetails() {

	 $this->load->model('inventory/inventorymovement');
     $inventory_code  = $this->request->post["inventory_code"];
     $Details = $this->model_inventory_inventorymovement->getProductsByInventoryCode($inventory_code);
     echo json_encode($Details);

  	}
	
	public function getForm() {
	
		$this->data['heading_title'] = $this->language->get('Inventory Movement');
		
		$this->data['entry_bar_code'] = $this->language->get('entry_bar_code');
		$this->data['entry_inventory_code'] = $this->language->get('entry_inventory_code');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_price'] = $this->language->get('entry_price');
		$this->data['entry_remarks'] = $this->language->get('entry_remarks');
		$this->data['entry_labels'] = $this->language->get('entry_labels');
		$this->data['text_no_data'] = $this->language->get('No data !');
		
		$this->data['button_cancel'] = $this->language->get('button_cancel');		
		$this->data['button_print'] = $this->language->get('button_print');

		if ($this->request->server['REQUEST_METHOD'] == 'POST'/* && $this->validateForm()*/) {

		if ($this->request->post['inventory_code'] != '') {			
			$this->data['product'] = $this->model_inventory_inventorymovement->getProductsByInventoryCode($this->request->post['inventory_code']);
			//printArray($this->data['product']);
			$productInfo = $this->data['product'];

			if($productInfo['product_id'] !=''){
				$this->data['purchase'] = $this->model_inventory_inventorymovement->getPurchaseByProductId($productInfo['product_id']);

				$this->data['purchaseReturn'] = $this->model_inventory_inventorymovement->getPurchaseReturnByProductId($productInfo['product_id']);

				$this->data['sales'] = $this->model_inventory_inventorymovement->getSalesByProductId($productInfo['product_id']);

				$this->data['salesReturn'] = $this->model_inventory_inventorymovement->getSalesReturnByProductId($productInfo['product_id']);

			}		
		}
	 }	

	 if (!empty($productInfo)) {
			$this->data['bar_code'] = $productInfo['barcode'];
		} else {
			$this->data['bar_code'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['inventory_code'] = $productInfo['sku'];
		} else {
			$this->data['inventory_code'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['name'] = $productInfo['name'];
		} else {
			$this->data['name'] = '';
		}
		
		if (!empty($productInfo)) {
			$this->data['price'] = $productInfo['price'];
		} else {
			$this->data['price'] = '';
		}			

 		if (isset($this->error['warning'])) {	
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => '<i class="fa fa-home"></i>'
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => 'Inventory',
			'href'      => $this->url->link('inventory/common', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('Inventory Movement'),
			'href'      => $this->url->link('inventory/inventorymovement', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' <i class="fa fa-angle-right"></i> '
		);
				
		$url = '';

		$this->data['action'] = $this->url->link('inventory/inventorymovement', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		$this->data['cancel'] = $this->url->link('inventory/inventorymovement', 'token=' . $this->session->data['token'] . $url, 'SSL');
		

		$this->data['token'] = $this->request->get['token'];

		$this->data['route'] = 'inventory/inventorymovement';
		$this->template = 'inventory/inventorymovement_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'inventory/barcode')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['bar_code'])) {
			$this->error['bar_code'] = 1;
		}
		
		if (empty($this->request->post['price'])) {
			$this->error['price'] = 1;
		}
		
		if (empty($this->request->post['no_labels'])) {
			$this->error['no_labels'] = 1;
		}
		
		if (!$this->error) {
			return true;
		} else {
			$this->error['warning'] = $this->language->get('error_invalid');
			$this->redirect($this->url->link('inventory/inventorymovement', 'token=' . $this->session->data['token'] . $url, 'SSL'));
			return false;
		}
	}
	
	/*public function printLabels() { 
	    
		$this->language->load('inventory/barcode');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('inventory/barcode');
		
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
		
		$url = '';

		$this->data['back'] = $this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateForm()) {
		    $this->data['info']['bar_code'] = $this->request->post['bar_code'];
			$this->data['info']['price'] = $this->currency->format($this->request->post['price']);
			$this->data['info']['no_labels'] = $this->request->post['no_labels'];
		} else {
		    $this->redirect($this->url->link('inventory/barcode', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}
		$this->template = 'inventory/barcode_form.tpl';
		$this->children = array(
			'common/header',
			'common/sidebar',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}*/
	
	
}  
?>