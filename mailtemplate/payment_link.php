<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link rel='stylesheet' type='text/css' href='../payment/css/style.css' />
    <link rel='stylesheet' type='text/css' href='../payment/css/responsive.css' />
    <link rel='stylesheet' type='text/css' href='../payment/css/print.css' media="print" />
    <script type='text/javascript' src='../payment/js/jquery-1.3.2.min.js'></script>
    <script type='text/javascript' src='../payment/js/example.js'></script>

</head>
<body style="font-size:13px;font-family:calibri !important">
<div style=" width:816px; margin:0px auto; height:auto;">
  <div style=" float:left; width:90%; height:92px; border:1px solid #eeeeee; padding:20px; color:#333333; line-height:15px; background-color:#eeeeee;">
    <img src="<?php echo HTTP_SERVER.PDF_HEADER_IMAGE; ?>" alt="Juz Apps" style="float:left; margin:5px 0 0 25px;"/>
  </div>  
  <div style="font-size:14px !important;float:left; border: 1px solid #EAEAEA; float:left; padding: 20px; width:90%; color:#000000;font-family:calibri !important;">
       {Content}
  </div>
</div>
</body>
</html>