<?php
class Modelcustomerscustomersb2b extends Model {

	public function getTotalB2BCustomers($data = array())
	{
		$company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "b2b_customers WHERE customer_id!= ''";
		
		$sort_data = array(
			'name',
			'email',				
		);
       
        if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR primary_email LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
        }

		if ($data['filter_name']){
			$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
		}

		if ($data['filter_email']){
			$sql.= " AND primary_email LIKE '" . $this->db->escape($data['filter_email']) . "%'"; 
		}

		//echo "$sql";	 exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getB2BCustomers($data = array())
	{		
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE company_id = '" . (int)$company_id . "'";
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "b2b_customers WHERE customer_id!= ''";
			$sort_data = array(
				'name',
				'email',				
			);

			 if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR primary_email LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
             }
			
			if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}

			if ($data['filter_email']){
				$sql.= " AND primary_email LIKE '" . $this->db->escape($data['filter_email']) . "%'"; 
			}
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY customercode";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			//echo "$sql";	
			$query = $this->db->query($sql);

			return $query->rows;
			
		} else {
			
				$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "b2b_customers ORDER BY name ASC");
				$customer_data = $query->rows;
				return $customer_data;	
		}	
	}

	public function getB2BCustomer($customer_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "b2b_customers WHERE customer_id = '" . (int)$customer_id . "' AND company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}

	public function getCustomerUniqueId()
	{
		$query = $this->db->query("SELECT TOP 1 customercode FROM " . DB_PREFIX . "customers ORDER BY customercode DESC");
		//printArray($query); exit;
		return $query->row;
	}

	public function addB2BCustomer($data)
	{
		//echo "string"; exit;
		$company_id	= $this->session->data['company_id'];	  

	   if($data['do_not']){
	   	  $do_nots = serialize($data['do_not']);
	   }	
	   $data['birth_date'] = changeDate($data['birth_date']);
         $custom_fields=0;
		$sql = "INSERT INTO " . DB_PREFIX . "b2b_customers (customercode,company_id,customer_type,discount,salestax,tax_allow,status,name,customer_title,company,birth_date,home,work,mobile,pager,fax,address1,address2,city,state,zipcode,website,primary_email,secondary_email,custom,tag,do_not,custom_fields,notes,added_date) VALUES ('".$data['customer_code']."','".$company_id."','".$data['customer_type']."','".$data['discount']."','".$data['salestax']."','".$data['tax_allow']."','".$data['status']."','".$data['name']."','".$data['customer_title']."','".$data['company']."','".$data['birth_date']."','".$data['home']."','".$data['work']."','".$data['mobile']."','".$data['pager']."','".$data['fax']."','".$data['address1']."','".$data['address2']."','".$data['city']."','".$data['state']."','".$data['zipcode']."','".$data['website']."','".$data['primary_email']."','".$data['secondary_email']."','".$data['custom']."','".$data['tag']."','".$do_nots."','".$custom_fields."','".$data['notes']."',curdate())";

		$res = $this->db->queryNew($sql);
		if($res=='error'){
			header('Location: '.HTTP_SERVER.'index.php?route=customers/customersb2b/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['
				customer_code']);						
				exit;
		}
		$customer_id = $this->db->getLastId();
		if($customer_id){
			$updateSql = "update tbl_setting set value=value+1 where [key]='next_member_no'";
			$this->db->query($updateSql);
		}
		
		if(in_array($this->session->data['location_code'],API_LOCATION) &&  $customer_id){
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "b2b_customers  WHERE customer_id = '" . $customer_id . "'");
			$customer_data = array(
				"firstname"		=> $query->row['name'],
				"lastname"		=> $query->row['name'],
				"email"			=> $query->row['primary_email'],
				"telephone"		=> $query->row['mobile'],
				"password"		=> '',
				"company"		=> $query->row['company'],
				"address_1"		=>  $query->row['address1'],
				"unitno"		=> $query->row['address2'],
				"postcode"		=>$query->row['zipcode'],
			);
			$res = curlpost('create_customer',$customer_data);	
		}

		return $customer_id;
	}

	public function editB2BCustomer($data,$customer_id)
	{
		$company_id	= $this->session->data['company_id'];

		 if($data['do_not']){
	   	  	$do_nots = serialize($data['do_not']);
	   	}
         $custom_fields=0;

		$query = $this->db->query("UPDATE " . DB_PREFIX . "b2b_customers SET customercode = '".$data['customer_code']."',
									customer_type   = '".$data['customer_type']."',
									discount		= '".$data['discount']."',
									salestax		= '".$data['salestax']."',
									tax_allow		= '".$data['tax_allow']."',
									status 			= '".$data['status']."',
									name		    = '".$data['name']."',
									customer_title  = '".$data['customer_title']."',
									company 		= '".$data['company']."',
									birth_date		= '".$data['birth_date']."',
									home 			= '".$data['home']."',
									work 			= '".$data['work']."',
									mobile 			= '".$data['mobile']."',
									pager 			= '".$data['pager']."',
									fax 			= '".$data['fax']."',
									address1 		= '".$data['address1']."',
									address2 		= '".$data['address2']."',
									city 			= '".$data['city']."',
									state 			= '".$data['state']."',
									zipcode 		= '".$data['zipcode']."',
									website 		= '".$data['website']."',
									primary_email 	= '".$data['primary_email']."',
									secondary_email = '".$data['secondary_email']."',
									custom 			= '".$data['custom']."',
									tag 			= '".$data['tag']."',
									do_not 			= '".$do_nots."',
									custom_fields   = '".$custom_fields."',
									notes 			= '".$data['notes']."',
									added_date 			= curdate() WHERE customer_id = '".$customer_id."' AND company_id = '".$company_id."'");

		}


		public function deleteB2BCustomer($customer_id)
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "b2b_customers WHERE customer_id = '" . (int)$customer_id . "'");
		}

		public function getCustomerb2bByPhone($mobile,$customer_id)
		{
			if($customer_id){
				$sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "b2b_customers WHERE mobile = '".$mobile."' AND customer_id!='".$customer_id."'";
			}else {
				$sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "b2b_customers WHERE mobile = '".$mobile."'";
			}
			$query = $this->db->query($sql);
			return $query->row['cnt'];
		}
		public function getCustomerb2bByCode($code)
		{
		    $sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "b2b_customers WHERE customercode = '".$code."'";
			$query = $this->db->query($sql);
			return $query->row['cnt'];
		}
		public function getCustomerb2bByEmail($email)
		{
			$company_id	= $this->session->data['company_id'];
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "b2b_customers WHERE primary_email = '".$email."' AND company_id = '".$company_id."'");
			return $query->row;
		}

		public function getTaxRates()
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT DISTINCT * FROM " .DB_PREFIX . "tax_rate WHERE company_id = '".$company_id."'");

			return $query->rows;
		}

		public function getDiscounts()
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "discount WHERE company_id = '".$company_id."'");

			return $query->rows;
		}
		public function getCustomerOrderDetails($invoicenumber){
				
			$query = $this->db->query("SELECT DISTINCT A.user_id,A.invoice_no,A.date_added,B.customer_id,B.primary_email as Email,B.firstname,B.lastname,B.customer_title from tbl_order as A LEFT JOIN tbl_customers as B ON A.user_id = B.customer_id WHERE A.invoice_no = '".$invoicenumber."'");
			return $query->row;
		}
		public function getTypeDetailsList($customertype_id)
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT T.name, D.discount_name,C.discount,C.taxcategory FROM " . DB_PREFIX ."tax_rate AS T," . DB_PREFIX . "discount AS D, " . DB_PREFIX . "customer_type AS C WHERE T.tax_rate_id = C.taxcategory AND D.discount_id = C.discount AND C.customertype_id = '".$customertype_id."' AND C.company_id = '".$company_id."'" );

			return $query->row;
		}

		
}
?>