<?php
class ModelCustomersCustomerspricing extends Model {

	public function getLastTerminalCode(){
		//$terminal_code	= $this->session->data['terminal_code']; 

		$query = $this->db->query("SELECT MAX(terminal_code) as terminalcode FROM " . DB_PREFIX . "terminal_configuration");
		if(!empty($query->row)){
			$termcode=$query->row['terminalcode'];

		}else{
			$termcode=1;
		}
		
		return $termcode;
	}
	public function addCustomerpricing($data,$customerspriceid){
	  
		$date_from = changeDates($data['filter_date_from']); 
		$date_to   = changeDates($data['filter_date_to']);


		for($i=0;$i<count($data['sku']);$i++){
			if($data['sku'][$i]){
			 $sql = "INSERT INTO " . DB_PREFIX . "customers_price (customers_price_id,customers_code,location_code,product_name,selling_price,FromDate,ToDate,sku,lpp_price,customer_price,price_difference,added_by,added_date) VALUES ('".$customerspriceid."','".$data['filter_customer']."','".$data['filter_location']."','".$data['name'][$i]."','".$data['sku_price'][$i]."','".$date_from."','". $date_to."','".$data['sku'][$i]."','".$data['lpp_price'][$i]."','".$data['cust_price'][$i]."','".$data['diff_price'][$i]."','".$this->session->data['username']."', curdate())"; 
			 $query =$this->db->query($sql); 
			}
		}
	   $res = $this->db->getLastId();
       return $res;
	}

	public function getTotalCustomerpricing($data){

            $sql = "SELECT customers_code,location_code,customers_price_id,FromDate,ToDate FROM " . DB_PREFIX . "customers_price WHERE customers_code !=''";
			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND  FromDate >='" .changeDate($data['filter_date_from']). "' AND ToDate <='" .changeDate($data['filter_date_to']). "'";
			}

			if($data['filter_sku']!=''){
				$sql .= " AND sku LIKE '%".$data['filter_sku']."%'";
			}
			$sql .= "group by customers_price_id,customers_code,location_code,FromDate,ToDate";
			$query = $this->db->query($sql);	   
			return $query->num_rows;
	}

	public function getCustomerpricing($data){
		if ($data) {
			$sql="SELECT CP.customers_code,CP.location_code,CP.customers_price_id,CP.FromDate,CP.ToDate,cust.name from ".DB_PREFIX ."customers_price as CP LEFT JOIN ".DB_PREFIX ."customers as cust on cust.customercode = CP.customers_code where customers_code !=''";

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND  FromDate >='" .changeDate($data['filter_date_from']). "' AND ToDate <='" .changeDate($data['filter_date_to']). "'";
			}
			if($data['filter_sku']!=''){
				$sql .= " AND sku LIKE '%".$data['filter_sku']."%'";
			}
			$sql .= "group by customers_price_id,customers_code,location_code,FromDate,ToDate";
			if (isset($data['sort'])) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY customers_code";	
			}
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
			$query = $this->db->query($sql);
			return $query->rows;
		} 	

	}
	public function getExistingReport($data){

		$date_from = changeDates($data['filter_date_from']); 
		$date_to   = changeDates($data['filter_date_to']); 

        $sql="SELECT * from ".DB_PREFIX ."customers_price where customers_code !=''"; 
		if($date_from !='' &&  $date_to !=''){
			$sql .= " AND  FromDate >='" .$date_from. "' AND ToDate <='" . $date_to. "'";
		}
		if($data['filter_customer']!=''){
			$sql .= " AND customers_code='".$data['filter_customer']."'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY added_date";	
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		$query = $this->db->query($sql);
	    return $query->rows;
	} 
	 
	public function editCustomerpricing($data,$customerpriceid)
	{
	    $date_from = changeDates($data['filter_date_from']); 
		$date_to   = changeDates($data['filter_date_to']);
     
		 for($i=0;$i<count($data['exist_sku']);$i++){
	       $sql="UPDATE " . DB_PREFIX . "customers_price SET 
	                            FromDate	        = '".$date_from."',
								ToDate    	        = '".$date_to."',
								sku 		        = '".$data['exist_sku'][$i]."',
								product_name	    = '".$data['exist_name'][$i]."',
								selling_price       = '".$data['exist_sku_price'][$i]."',
								lpp_price    	    = '".$data['exist_lpp_price'][$i]."',
								customer_price    	= '".$data['exist_cust_price'][$i]."',
								price_difference    = '".$data['exist_diff_price'][$i]."'
								WHERE sku = '".$data['exist_sku'][$i]."'";  
		    $query = $this->db->query($sql); 
		  }
	  
		 if(isset($data['sku'])){
			for($j=0;$j<count($data['sku']);$j++){
		    $sql = "INSERT INTO " . DB_PREFIX . "customers_price (customers_price_id,customers_code,location_code,product_name,selling_price,FromDate,ToDate,sku,lpp_price,customer_price,price_difference,added_by,added_date) VALUES ('".$customerpriceid."','".$data['filter_customer']."','".$data['filter_location']."','".$data['name'][$j]."','".$data['sku_price'][$j]."','".$date_from."','".$date_to."','".$data['sku'][$j]."','".$data['lpp_price'][$j]."','".$data['cust_price'][$j]."','".$data['diff_price'][$j]."','".$this->session->data['username']."', curdate())"; 
			$query =$this->db->query($sql); 
			}
	    }
	}
	public function deleteCustomerpricing($customers_id)
	{
		 $this->db->query("DELETE FROM " . DB_PREFIX . "customers_price WHERE customers_price_id = '" . $customers_id."'"); 
	}
	public function deletepricedetailsbysku($sku)
	{
		 $sql="DELETE FROM " . DB_PREFIX . "customers_price WHERE sku = '" . $sku . "'"; 
         $this->db->query($sql);
	}

	public function getCustomersTerminal($customersdetails = array())	
	{
		    $sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customers_price WHERE customers_code = '" . $customersdetails['customers_code']. "'";
		 
		 $FromDate =$customersdetails['Fromdate'];
         $ToDate =$customersdetails['Todate'];

		   if($FromDate!='' && $ToDate!=''){
				$sql .= " AND  FromDate >='" .$FromDate. "' AND ToDate <='" .$ToDate. "'";
			}
			
	      $query = $this->db->query($sql);
		
		return $query->row;
	}
	public function viewcustomerdetailsbycode($terminalinfo=array(),$customercode){

		 $sql="SELECT  * FROM " . DB_PREFIX . "customers_price WHERE customers_code = '" .$terminalinfo['customers_code']. "'";

           $FromDate =$terminalinfo['Fromdate'];
           $ToDate =$terminalinfo['Todate'];

		   if($FromDate!='' && $ToDate!=''){
				$sql .= " AND  FromDate >='" .$FromDate. "' AND ToDate <='" .$ToDate. "'";
			}
				$sql .= " ORDER BY added_date";	
				//echo $sql; exit;
		  $query = $this->db->query($sql);
		  return $query->rows;
	}
	public function getcustomerDetailsBypriceid()
	{
		//$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers_price ORDER by id DESC ");
		return $query->row['customers_price_id'];
	}
	
	public function getSkuDetailsById($data){

		$date_from = changeDates($data['filter_date_from']); 
		$date_to   = changeDates($data['filter_date_to']);

		$sql = "SELECT sku FROM " . DB_PREFIX . "customers_price WHERE customers_code = '".$data['filter_customer']."'";
			if($data['filter_location_code']){
				$sql .=" AND location_code = '".$data['filter_location_code']."' ";
			}
			if($date_from !='' &&  $date_to !=''){
				$sql .= " AND  FromDate >='" .$date_from. "' AND ToDate <='" . $date_to. "'";
			}
		$this->db->query($sql);
		return $query->rows;
	}
	public function getSkuCount($promotionId){
		$query = $this->db->query("SELECT COUNT(*) as totalItems FROM " . DB_PREFIX . "promotions_sku WHERE promotion_id = '".$promotionId."'");
		return $query->row;
	}
	/* customer pricing*/
	public function getTotalCustomersname(){
		$sql= "SELECT name,customercode FROM " . DB_PREFIX . "customers";
		$query = $this->db->query($sql);
		$data =  $query->rows;
		return $data;
	}
	public function getProductsReport($data = array(),$type="") {
		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT p.product_id,p.sku,p.name,p.sku_brand_code,p.sku_status as status,p.sku as inventory_id,d.department_name,c.category_name,ps.sku_price,ps.sku_cost,b.brand_name,ps.sku_qty,ps.location_Code,p.sku_vendor_code,v.gst,v.tax_method FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "department as d ON d.department_code = p.sku_department_code
				LEFT JOIN " . DB_PREFIX . "category as c ON c.category_code = p.sku_category_code
				LEFT JOIN ". DB_PREFIX . "product_stock as ps ON ps.sku = p.sku
				LEFT JOIN " . DB_PREFIX . "brand as b ON b.brand_code = p.sku_brand_code
				LEFT JOIN tbl_vendor as v on p.sku_vendor_code = v.vendor_code";

		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON (p.sku = pb.sku)";
		}

		$sql .= " WHERE p.product_id !='0'";
		
		
		/*if (!empty($data['filter_name'])) {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}*/

		if (!empty($data['filter_sku'])) {
			 $sql .= " AND  p.sku ='" . $this->db->escape($data['filter_sku']) . "'";
		}
		

		if (!empty($data['filter_location_code'])) {
			$sql .= " AND  ps.location_code ='" . $this->db->escape($data['filter_location_code']) . "'";
		}

		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND  pb.barcode ='" . $this->db->escape($data['filter_barcodes']) . "'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		//$sql .= "AND p.sku NOT IN(406,3569)";
		
		$sort_data = array(
			'p.name',
			'ps.sku_price',
			'ps.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}

}
?>