<?php
class ModelCommonHome extends Model {

	public function getOrderList($data = array()){
		$company_id	= $this->session->data['company_id'];
		//$sql = "SELECT A.name as Cname,A.customer_id,B.*,C.name,C.company_id,C.order_status_id from tbl_order as B LEFT JOIN tbl_customers as A ON A.customer_id = B.user_id JOIN tbl_order_status as C ON C.order_status_id = B.order_status_id WHERE C.company_id ='".$company_id."' ORDER BY B.order_id DESC LIMIT 0,10";
		//echo $sql = "SELECT * FROM tbl_sales_header WHERE location_code = '".$data['code']."'";
		$sql = "SELECT * FROM tbl_sales_header WHERE MONTH([createdon]) = '".$data['month']."'";
		//$sql = "SELECT * FROM tbl_purchase order by purchase_id DESC ";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalOrderAmount(){
		$company_id	= $this->session->data['company_id'];
		//$sql = "SELECT SUM(total) as total_amount from tbl_order WHERE company_id ='".$company_id."' AND Date(date_added) = CURDate()";
		$sql = "SELECT SUM(total) as total_amount from tbl_order WHERE company_id ='".$company_id."'";
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->row['total_amount'];
	}
	public function getTotalOrders(){
		$company_id	= $this->session->data['company_id'];
		//$sql = "SELECT COUNT(B.order_id) as total_order from tbl_order as B  JOIN tbl_order_status as C ON C.order_status_id = B.order_status_id WHERE C.company_id ='".$company_id."' AND Date(B.date_added) = CURDate()";
		$sql = "SELECT COUNT(B.order_id) as total_order from tbl_order as B  JOIN tbl_order_status as C ON C.order_status_id = B.order_status_id WHERE C.company_id ='".$company_id."'";
		$query = $this->db->query($sql);
		return $query->row['total_order'];
	}
	public function getTotalUser() {
		$company_id = $this->session->data['company_id'];
		$sql = "SELECT COUNT(customer_id) as TotalUser FROM tbl_customers WHERE company_id = '".$company_id."' AND status='0'";
		$query = $this->db->query($sql);
		return $query->row['TotalUser'];
	}

	public function getTotalStocks() {
		$company_id = $this->session->data['company_id'];
		$sql   = "SELECT SUM(p.quantity) AS TotalQuantity FROM tbl_product p JOIN tbl_product_to_company p2co ON (p.product_id = p2co.product_id) WHERE p2co.company_id='".$company_id."'";
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->row['TotalQuantity'];
	}
	/*public function getTotalSales($data = array()) {
		$company_id = $this->session->data['company_id'];
		$sql = "SELECT COUNT(order_id) as Sales ,DATE(date_added) as sale_date FROM `tbl_order` WHERE company_id = '".$company_id."' AND YEAR(date_added)= YEAR(CURDATE()) GROUP BY DATE(`date_added`)";
		//echo "SELECT COUNT(order_id) as Sales ,DATE(date_added) as sale_date FROM `tbl_order` WHERE company_id = '".$company_id."' AND YEAR(date_added)= YEAR(CURDATE()) GROUP BY DATE(`date_added`)";
		$query = $this->db->query($sql);
		return $query->rows;
	}	*/
	public function getTotalSales($data = array()) {
		if($data['filter_year']!='') {
			$whereClause ="YEAR(date_added)= '".$this->db->escape($data['filter_year'])."'";
		} else {
			$whereClause ="YEAR(date_added)= YEAR(CURDATE())";
		}
		$company_id = $this->session->data['company_id'];
		$sql = "SELECT COUNT(order_id) as Sales ,DATE(date_added) as sale_date FROM `tbl_order` WHERE company_id = '".$company_id."' AND  $whereClause GROUP BY DATE(`date_added`)";
		$query = $this->db->query($sql);
		return $query->rows;
	}	

	public function getTopTenTotalSales($data = array()) {
		
		$company_id = $this->session->data['company_id'];
		//echo $sql = "SELECT TOP(10) createdon as sale_date,invoice_no,net_total FROM tbl_sales_header WHERE CAST([createdon] AS DATE) = '".$data['date']."' and invoice_no IS NOT NULL ORDER BY createdon DESC"; 
		$sql = "SELECT TOP(10) createdon as sale_date,invoice_no,net_total FROM tbl_sales_header WHERE invoice_no IS NOT NULL ORDER BY createdon DESC"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getTotalSalesAmount($data = array()){
		$sql = "SELECT SUM(net_total) as total_amount, SUM(actual_total) as totalwogst,COUNT(invoice_no) as totalbills from tbl_sales_header WHERE CAST([createdon] AS DATE) = '".$data['date']."'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getTotalSalesChart($data = array()) {		
		$sql = "SELECT COUNT(*) as Sales , CAST(createdon AS DATE) as createdon from tbl_sales_header WHERE MONTH([createdon]) = '".$data['month']."' group by CAST(createdon AS DATE)";
		$query = $this->db->query($sql);
		return $query->rows;
	}	
	public function getLocationDetailsByCompanyId(){
		$company_id = $this->session->data['company_id'];
		$sql = "SELECT LC.location_code,LC.type FROM tbl_company as CP LEFT JOIN tbl_location as LC ON CP.location_code = LC.location_code WHERE company_id = '".$company_id."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getLineChart(){
		$sql = "SELECT created_date, sum(distinct sub_total) as  total from tbl_purchase WHERE hold='0' AND purchase_return='0' group by created_date";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getPurchase_BarChart($date,$type,$ptype){
		$purchase_return = ($ptype=='purchase') ? 0 : 1;
		$sql = "SELECT transaction_date, sum(sub_total) as  total from tbl_purchase WHERE hold='0' AND purchase_return='".$purchase_return."' ";

		if($type!='year'){
			$sql .= "AND  DATE(transaction_date)='".$date."' ";
		}else{
			$sql .="AND  MONTH(transaction_date) = ".date('m',strtotime($date))." AND YEAR(transaction_date) = ".date('Y')." ";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getSales_BarChart($date,$type,$stype){
		$sales_return = ($stype=='sales') ? 0 : 1;
		$sql = "SELECT invoice_date, sum(net_total) as  total from tbl_sales_header WHERE hold='0' AND sales_return='".$sales_return."' AND deleted='0' ";
		// echo $sql."<br>";
		if($type!='year'){
			$sql .= "AND  DATE(invoice_date)='".$date."' ";
		}else{
			$sql .="AND  MONTH(invoice_date) = ".date('m',strtotime($date))." AND YEAR(invoice_date) = ".date('Y')." ";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getSalesPaymentHistory($data)
	{
		$query = "SELECT * FROM ".DB_PREFIX."order_payment WHERE order_id != ''";
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$query .= " AND payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_delivery_man']){
			$query .= " AND delivery_man_id = '".$data['filter_delivery_man']."'";
		}
		if($data['filter_date_from'] !='' && $data['filter_date_from'] !=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']);
			$data['filter_date_to']   = changeDates($data['filter_date_to']);
			$query .= " AND date(payment_date) between '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		$query .=" ORDER BY id DESC";
		return $this->db->query($query)->rows;
	}
	public function getdeliveryManName($delivery_man_id){
		if($delivery_man_id){
			return $this->db->query("SELECT name FROM ".DB_PREFIX."salesman where id='".$delivery_man_id."' ")->row['name'];
		}else{
			return '-';
		}
	}
}
?>