<?php
class ModelCommonFooter extends Model {

	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT p.product_id, p.sku, p.quantity, p.price, p.status, p.image, pd.name FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_company pc ON (p.product_id = pc.product_id)
				WHERE pc.company_id = '".$company_id."' AND p.status = '1' AND p.date_available <= curdate() ";		
		if(!empty($data['filter_name'])) {
			$sql .= " AND (";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
			}			
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";			
			$sql .= " OR LCASE(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";			
			$sql .= ")";			
		}
		$sql .= " GROUP BY p.product_id";	
		//echo $sql;			
		$query        = $this->db->query($sql);	
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
		
	}
	public function getProductSKu($product_id){
		$sql = "SELECT sku FROM " . DB_PREFIX . "product where product_id='".$product_id."'";	
		$query = $this->db->query($sql);
		return $query->row['sku'];
	}
	public function getProductBarcodes($product_id) {
		$p_sku      = $this->getProductSKu($product_id);
		$product_department_data = array();
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . (int)$p_sku . "'");
		$result = array();
		foreach ($query->rows as $key => $value){
		    $result[$key] = $value['barcode'];
		}
		return $result;
	}
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND CURDATE() >= date_start AND CURDATE() <= date_end ORDER BY priority, price");
		return $query->row;
	}
		
}
?>