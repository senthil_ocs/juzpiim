<?php
class ModelTotalTax extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$route = explode("/", $this->request->get['route']);
		$apply_tax_type	= (string)$this->config->get('config_apply_tax');
		if($route['1']=='purchase'){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax_purchase');
		}
		
		if($apply_tax_type=='0') {
			if($total > 0) {
				$total_data[] = array(
					'code'       => 'tax',
					'title'      => 'Zero Tax (0%)', 
					'text'       => $this->currency->format(0),
					'value'      => 0,
					'sort_order' => $this->config->get('tax_sort_order')
				);	
			}
		} else {
			if($apply_tax_type	== '1') {
				$tax_type	= '(Excl)';
			} else {
				$tax_type	= '(Incl)';

			}
			foreach ($taxes as $key => $value) {
				if ($value > 0) {
					$tax_title	= $this->tax->getRateName($key)." ".$tax_type;
					
					$total_data[] = array(
						'code'       => 'tax',
						'title'      => $tax_title, 
						'text'       => $this->currency->format($value),
						'value'      => $value,
						'sort_order' => $this->config->get('tax_sort_order')
					);
					if($apply_tax_type=='2'){
						$total = $total;
					}else{
						$total += $value;
					}
				}
			}
			
		} 
	}
}
?>