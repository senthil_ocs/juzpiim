<?php
class ModelTotalDiscount extends Model {
	public function getTotal(&$total_data, &$total, &$taxes) {
		$product_discount	= 0;
		foreach ($this->cart->getProducts() as $product) {
			//$product_discount	= $product_discount + $product['discount_amount']; // ragu disabled
		}
		if ((isset($this->session->data['bill_discount']) && $this->session->data['bill_discount']) || !empty($product_discount)) {
			$discount_amount	= $this->session->data['bill_discount'];
			$subTotal = $this->cart->getSubTotal();
			$subTotal = $subTotal - $product_discount;
			$discount_title	= 'Discount ($)';
			if($this->session->data['bill_discount_mode'] == "1"){
				$percentage = $discount_amount/100;
				$discount_amount = $subTotal-($subTotal*(1-$percentage));
				$discount_title	= 'Discount (%)';
			}
			if($this->session->data['bill_discount_mode'] == "2"){
				$discount_amount = $discount_amount;
				$discount_title	= 'Discount ($)';
			}
			
			$discount_amount	= $discount_amount + $product_discount;
			$total_data[] = array( 
				'code'       => 'discount',
				'title'      => $discount_title,
				'text'       => $this->currency->format($discount_amount),
				'value'      => $discount_amount,
				'sort_order' => $this->config->get('discount_sort_order')
			);
			$total -= $discount_amount;
			
			// if($this->session->data['handling_fee'] > 0){
			// 	$total = $total + $this->session->data['handling_fee'];
			// }
		}
	}
}
?>