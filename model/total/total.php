<?php
class ModelTotalTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes,$apply_tax_type='') {
		$this->language->load('total/total');
			
		if($apply_tax_type =='2'){			
			foreach ($taxes as $key => $value) {
			if ($value > 0) {
					$total += $value;
				}
			}
		}
		$total_data[] = array(
			'code'       => 'total',
			'title'      => $this->language->get('text_total'),
			'text'       => $this->currency->format(max(0, $total)),
			'value'      => max(0, $total),
			'sort_order' => $this->config->get('total_sort_order')
		);
	}
}
?>