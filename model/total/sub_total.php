<?php
class ModelTotalSubTotal extends Model {
	public function getTotal(&$total_data, &$total, &$taxes, &$apply_tax_type) {
		$this->language->load('total/sub_total');
		
		$sub_total = $this->cart->getSubTotal();

		if (isset($this->session->data['vouchers']) && $this->session->data['vouchers']) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}
		if($apply_tax_type ==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax');
		}

		if($apply_tax_type=='2') {
			foreach ($taxes as $key => $value) {
			if ($value > 0) {
					$sub_total -= $value;
				}
			}
		}
		

		$total_data[] = array( 
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'text'       => $this->currency->format($sub_total),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
		// printArray($sub_total); die;
		
		if($apply_tax_type=='2') {
			$total = $sub_total;
		}else{
			$total += $sub_total;
		}	
		if($this->session->data['handling_fee'] > 0){
			$total = $total + $this->session->data['handling_fee'];
		}
	}
}
?>