<?php
class ModelTransactionStockRequest extends Model {
	
	public function getNextSRNofromSettings(){
		$sql = "select value from tbl_setting where `key` = 'next_stock_request_no'";
		$query = $this->db->query($sql);
		return $query->row['value'];
	}
	public function getLocationsDetailsByCode($locationCode){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code = '".$locationCode."' ");
		return $query->row;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}
	public function updatesettingNextNo($no){
		if($no){
			$next_no = $no+1;
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '".$next_no."'  WHERE `key` = 'next_stock_request_no'");		
		}
	}

	public function getNameByProductId($pid){
		 $sql 	= "SELECT name FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['name'];
	}
	public function addPurchase($data,$avg_method='',$fromHold='')
	{
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$reference_noAry =  explode("|",$data['reference_no']);
		$reference_no = $reference_noAry[0];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 0;
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';
		$posted = 0;
		
		$nextSRNO = $this->getNextSRNofromSettings();
		$data['transaction_no'] = str_pad($nextSRNO,6,"0",STR_PAD_LEFT);

		
		$insertSql = "INSERT INTO " . DB_PREFIX . "stockrequest_header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	posted,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon,
																	reference_no,
																	reference_date
																	) VALUES('" . $data['transaction_no'] . "',
																						'" . $data['transaction_date'] . "',	
																						 '" . $frmLocationId . "',
																						 '" . $frmLocationStatus . "',
																						 '" . $toLocationId . "',
																						 '" . $toLocationStatus . "',
																						 '" . $transfer_remarks . "',
																						 '" . $transfer_value . "',
																						 '" . $hold_status . "',
																						 '" . $posted . "',
																						 '" . $accepted_status . "',
																						 '" . $accepted_user . "',
																						 '" . $accepted_date . "',
																						 '" . $revoke_status . "',
																						 '" . $revoke_user . "',
																						 '" . $revoke_date . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $reference_no . "',
																						 '" . $data['reference_date'] . "')";
	
		

		$this->db->query($insertSql);
		$purchaseId = $this->db->getLastId();
		$i=0;
		
		foreach ($data['products'] as $product) {
				$i++;
				
		
			   $pSku = $this->getSkuByProductId($product['product_id']);
			   $pName = $this->getNameByProductId($product['product_id']);

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "stockrequest_detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "',
			   					'" . $pSku . "',
			   					'" . $this->db->escape($pName) . "', 
			   					'" . $sku_uom."',
			   					'" . $product['quantity'] . "',
			   					'" . $product['price'] . "',
			   					'" . $transfer_value . "',
			   					'" . $accepted_qty . "',
			   					'" . $accepted_value . "',
			   					'" . $seq_no . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";
							 
	   		 $this->db->query($detailsInsertSql);

		}
		$this->updatesettingNextNo($data['transaction_no']);
		return $data['transaction_no'];
	}
	public function addHoldPurchase($data,$avg_method='',$fromHold='')
	{
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$reference_noAry =  explode("|",$data['reference_no']);
		$reference_no = $reference_noAry[0];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 1;
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';
		$posted = 0;
		
		$nextSRNO = $this->getNextSRNofromSettings();
		$data['transaction_no'] = str_pad($nextSRNO,6,"0",STR_PAD_LEFT);

		
		$insertSql = "INSERT INTO " . DB_PREFIX . "stockrequest_header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	posted,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon,
																	reference_no,
																	reference_date
																	) VALUES('" . $data['transaction_no'] . "',
																						'" . $data['transaction_date'] . "',	
																						 '" . $frmLocationId . "',
																						 '" . $frmLocationStatus . "',
																						 '" . $toLocationId . "',
																						 '" . $toLocationStatus . "',
																						 '" . $transfer_remarks . "',
																						 '" . $transfer_value . "',
																						 '" . $hold_status . "',
																						 '" . $posted . "',
																						 '" . $accepted_status . "',
																						 '" . $accepted_user . "',
																						 '" . $accepted_date . "',
																						 '" . $revoke_status . "',
																						 '" . $revoke_user . "',
																						 '" . $revoke_date . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $reference_no . "',
																						 '" . $data['reference_date'] . "')";
	
		

		$this->db->query($insertSql);
		$purchaseId = $this->db->getLastId();
		$i=0;
		
		foreach ($data['products'] as $product) {
				$i++;
				
		
			   $pSku = $this->getSkuByProductId($product['product_id']);
			   $pName = $this->getNameByProductId($product['product_id']);

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "stockrequest_detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "',
			   					'" . $pSku . "',
			   					'" . $pName . "',
			   					'" . $sku_uom."',
			   					'" . $product['quantity'] . "',
			   					'" . $product['price'] . "',
			   					'" . $transfer_value . "',
			   					'" . $accepted_qty . "',
			   					'" . $accepted_value . "',
			   					'" . $seq_no . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";
							 
	   		 $this->db->query($detailsInsertSql);
		}
		$this->updatesettingNextNo($data['transaction_no']);
		return $data['transaction_no'];
	}
	public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      
      	
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "stockrequest_header";

		$sql .= " WHERE transfer_no != '0'";
		
		if($data['hold_status']==1){
			$sql .= " AND hold_status = 1";
		}else{
			$sql .= " AND hold_status = 0";
		}
		if($data['filter_from_location'] != ''){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		if($data['filter_accepted_status'] == '1'){
			$sql .= " AND posted = 1 ";
		}else if($data['filter_accepted_status'] == '0'){
			$sql .= " AND posted = 0 ";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND CAST(createdon AS DATE) between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "stockrequest_header WHERE transfer_no!='0'";
		$query = $this->db->query($sql);
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND CAST(createdon AS DATE) between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_from_location']){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		if($data['filter_accepted_status'] == '1'){
			$sql .= " AND posted = 1 ";
		}else if($data['filter_accepted_status'] == '0'){
			$sql .= " AND posted = 0 ";
		}

		if($data['hold_status']==1){
			$sql .= " AND hold_status = 1";
		}else{
			$sql .= " AND hold_status = 0";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price
				FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}

		$sql.= " WHERE p.sku_status= '1'";
		if($data['filter_location_code']){
		   $sql .= " AND ps.location_code = '" . $data['filter_location_code'] . "'";
		}
		// /$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			//$sql .= ") and p2b.sku_status= '1'";
			$sql .= ")";
		}
		
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stockrequest_header WHERE transfer_no = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getProductDetailsById($purchaseId){
		$sql = "SELECT DISTINCT PP.sku, P.product_id,PP.sku_qty as quantity,PP.sku_cost as price,PP.transfer_value as total,P.sku,P.name FROM " . DB_PREFIX . "stockrequest_detail as PP LEFT JOIN " . DB_PREFIX . "product as P ON P.sku = PP.sku WHERE PP.transfer_no = '".$purchaseId."' ORDER BY PP.sku ASC";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	 function getHoldHeaderbyId($purchase_id){
        $sql = "SELECT transfer_no as transaction_no,transfer_date,transfer_from_location,transfer_to_location,transfer_value,hold_status,reference_no,reference_date FROM " . DB_PREFIX . "stockrequest_header";
       $sql.= " WHERE transfer_no = '".$purchase_id."'";
        $res = $this->db->query($sql);
        return $res->row;
  }
  function getHoldItemdetailsByTransNo($purchase_id){
    $sql = "SELECT STD.transfer_no,STD.sku,STD.sku_description,STD.sku_qty as quantity,STD.sku_cost as price,STD.transfer_value,P.product_id FROM tbl_stockrequest_detail as STD LEFT JOIN tbl_product as P on P.sku = STD.sku WHERE transfer_no = '".$purchase_id."'";
    $res = $this->db->query($sql);
    return $res->rows;
  }
  public function editPurchase($data,$purchaseId='')
	{
		
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 0;
		if($data['hold']=='1'){
			$hold_status = 1;
		}
		
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';

		

		$insertSql = "UPDATE " . DB_PREFIX . "stockrequest_header SET transfer_date = '".$data['transaction_date']."'
																	,transfer_from_location= '".$frmLocationId."'
																	,transfer_from_location_status= '".$frmLocationStatus."'
																	,transfer_to_location= '".$toLocationId."'
																	,transfer_to_location_status= '".$toLocationStatus."'
																	,transfer_remarks= '".$transfer_remarks."'
																	,transfer_value= '".$transfer_value."'
																	,hold_status= '".$hold_status."'
																	,accepted_status='".$accepted_status."'
																	,accepted_user='".$accepted_user."'
																	,accepted_date='".$accepted_date."'
																	,revoke_status='".$revoke_status."'
																	,revoke_user='".$revoke_user."'
																	,revoke_date='".$revoke_date."'
																	,createdby='".$userName."'
																	,createdon= curdate()
																	,modifyby='".$userName."'
																	,modifyon=curdate() where transfer_no='".$data['transaction_no']."'";

		$this->db->query($insertSql);
		
		$sqlSte = "DELETE FROM " . DB_PREFIX . "stockrequest_detail where transfer_no='".$data['transaction_no']."'";
		$this->db->query($sqlSte);

		$i = 0;
		foreach ($data['products'] as $product) {
				$i++;
			
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			    $pSku = $this->getSkuByProductId($product['product_id']);
			    $pName = $this->getNameByProductId($product['product_id']);

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   // delete existing Details

			    

			  $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "stockrequest_detail(
			   transfer_no,sku,sku_description,sku_uom,sku_qty,sku_cost,transfer_value,accepted_qty,accepted_value,seq_no,createdby,createdon, modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "','" . $pSku . "','" . $pName . "','" . $sku_uom."','" . $product['quantity'] . "',
			   					'" . $product['price'] . "','" . $transfer_value . "','" . $accepted_qty . "','" . $accepted_value . "','" . $seq_no . "',
			   					'" . $userName . "', curdate(),'" . $userName. "',curdate())";
			  $this->db->query($detailsInsertSql);   	 

		}
		return $purchaseId;
	}
	public function updatePosted($data)
	{
	
		
		$insertSql = "UPDATE " . DB_PREFIX . "stockrequest_header SET posted= '1' where transfer_no='".$data['transaction_no']."'";
		$this->db->query($insertSql);
	
		for($i=0;$i<count($data['sku']);$i++){
			$updateSql = "UPDATE " . DB_PREFIX . "stockrequest_detail SET accepted_qty= '".$data['received_qty'][$i]."' 
						  where transfer_no='".$data['transaction_no']."' AND sku='".$data['sku'][$i]."'";
			$this->db->query($updateSql);   	 

			$stockAdjustmentSql = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty= sku_qty-".$data['received_qty'][$i]." 
						  where location_Code='".$data['to_location_code']."' AND sku='".$data['sku'][$i]."'";
			$this->db->query($stockAdjustmentSql);

			$stockAdjustmentSq2 = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty= sku_qty+".$data['received_qty'][$i]." 
						  where location_Code='".$data['from_location_code']."' AND sku='".$data['sku'][$i]."'";
			$this->db->query($stockAdjustmentSq2);

		}
		
		return $purchaseId;
	}


}
?>