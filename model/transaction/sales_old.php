<?php

class ModelTransactionSales extends Model {

		

	public function getTotalSales($data) {

	    $company_id	= $this->session->data['company_id'];

      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "sales";

		$sql .= " WHERE company_id = '". (int)$company_id ."' AND sales_return = '0' "; 

		

		$query = $this->db->query($sql);

		return $query->row['totalPurchase'];

	}

		

	public function getPurchaseList($data) {

	    $company_id	= $this->session->data['company_id'];

		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales WHERE company_id = '" . (int)$company_id . "' AND sales_return = '0' ";

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {

			$sql .= " ORDER BY " . $data['sort'];	

		} else {

			$sql .= " ORDER BY sales_id";	

		}

		if (isset($data['order']) && ($data['order'] == 'ASC')) {

			$sql .= " ASC";

		} else {

			$sql .= " DESC";

		}

		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}				



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}	



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}

		$query = $this->db->query($sql);

		return $query->rows;

	}

	

	public function addPurchase($data) {

		$companyId	= $this->session->data['company_id'];

		$userId	= $this->session->data['user_id'];

		$this->db->query("INSERT INTO " . DB_PREFIX . "purchase SET 

			company_id = '" . (int)$companyId . "'

			, transaction_no = '" . $data['transaction_no'] . "'

			, transaction_date = '" . $data['transaction_date'] . "'

			, transaction_type = 'PURINV'

			, vendor_id = '" . $data['vendor'] . "'

			, reference_no = '" . $data['reference_no'] . "'

			, reference_date = '" . $data['reference_date'] . "'

			, remarks = '" . $data['remarks'] . "'

			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'

			, bill_discount_price = '" . $data['bill_discount_price'] . "'

			, total = '" . $data['total'] . "'

			, hold = '" . $data['hold'] . "'

			, created_by = '" . (int)$userId . "'

			, created_date = curdate()");

		$purchaseId = $this->db->getLastId();		

		foreach ($data['products'] as $product) { 

			$discount_percentage = '';

			$discount_price      = '';

			if ($product['purchase_discount_mode'] == 1) {

			    $discount_percentage = $product['purchase_discount_value'];

			} elseif ($product['purchase_discount_mode'] == 2) {

				$discount_price      = $product['purchase_discount_value'];

			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product SET 

					purchase_id           = '" . (int)$purchaseId . "'

					, product_id          = '" . (int)$product['product_id'] . "'

					, weight_class_id     = '" . (int)$product['weight_class_id'] . "'

					, quantity            = '" . (int)$product['quantity'] . "'

					, price               = '" . (float)$product['price'] . "'

					, discount_percentage = '" . $discount_percentage . "'

					, discount_price      = '" . $discount_price . "'

					, tax_class_id        = '" . (int)$product['tax_class_id'] . "'

					, net_price           = '" . (float)$product['net_price'] . "'

					, tax_price           = '" . (float)$product['tax_price'] . "'

					, total               = '" . (float)$product['total'] . "'");

		}



		foreach ($data['totals'] as $total) {

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total SET 

				purchase_id = '" . (int)$purchaseId . "'

				, code = '" . $this->db->escape($total['code']) . "'

				, title = '" . $this->db->escape($total['title']) . "'

				, text = '" . $this->db->escape($total['text']) . "'

				, `value` = '" . (float)$total['value'] . "'

				, sort_order = '" . (int)$total['sort_order'] . "'");

		}

		return $purchaseId;

	}

	

	public function getPurchase($purchaseId) {		

		$companyId	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");

		return $query->row;

	}

	

	public function getPurchaseProduct($purchaseId) {		

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");

		return $query->rows;

	}

	

	public function editPurchase($purchaseId, $data) {				

		$this->db->query("UPDATE " . DB_PREFIX . "purchase SET 

			transaction_no = '" . $data['transaction_no'] . "'

			, transaction_date = '" . $data['transaction_date'] . "'

			, transaction_type = 'PURINV'

			, vendor_id = '" . $data['vendor'] . "'

			, reference_no = '" . $data['reference_no'] . "'

			, reference_date = '" . $data['reference_date'] . "'

			, remarks = '" . $data['remarks'] . "'

			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'

			, bill_discount_price = '" . $data['bill_discount_price'] . "'

			, total = '" . $data['total'] . "'

			, hold = '" . $data['hold'] . "'

			, created_by = '" . (int)$userId . "'

			, date_modified = curdate()

			WHERE purchase_id = '" . (int)$purchaseId . "'");



		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");		

		foreach ($data['products'] as $product) { 

			$discount_percentage = '';

			$discount_price      = '';

			if ($product['purchase_discount_mode'] == 1) {

			    $discount_percentage = $product['purchase_discount_value'];

			} elseif ($product['purchase_discount_mode'] == 2) {

				$discount_price      = $product['purchase_discount_value'];

			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product SET 

					purchase_id           = '" . (int)$purchaseId . "'

					, product_id          = '" . (int)$product['product_id'] . "'

					, weight_class_id     = '" . (int)$product['weight_class_id'] . "'

					, quantity            = '" . (int)$product['quantity'] . "'

					, price               = '" . (float)$product['price'] . "'

					, discount_percentage = '" . $discount_percentage . "'

					, discount_price      = '" . $discount_price . "'

					, tax_class_id        = '" . (int)$product['tax_class_id'] . "'

					, net_price           = '" . (float)$product['net_price'] . "'

					, tax_price           = '" . (float)$product['tax_price'] . "'

					, total               = '" . (float)$product['total'] . "'");

		}	

		

		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_total WHERE purchase_id = '" . (int)$purchaseId . "'");		

		foreach ($data['totals'] as $total) {

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total SET 

				purchase_id = '" . (int)$purchaseId . "'

				, code = '" . $this->db->escape($total['code']) . "'

				, title = '" . $this->db->escape($total['title']) . "'

				, text = '" . $this->db->escape($total['text']) . "'

				, `value` = '" . (float)$total['value'] . "'

				, sort_order = '" . (int)$total['sort_order'] . "'");

		}

		

		return $purchaseId;		

	}

	

	public function getPurchaseByTransNo($transNumber) {		

		$companyId	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");

		return $query->row;

	}

	

}

?>