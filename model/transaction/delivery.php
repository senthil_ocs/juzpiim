<?php
class ModelTransactionDelivery extends Model {
	
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		return $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'")->rows;
	}
	public function getVendor($vendor_id) {
		return $this->db->query("SELECT * FROM ".DB_PREFIX."vendor where vendor_id = '".$vendor_id."'")->row;
	}
	public function getSalesAutoIdnew() 
	{
		$query = $this->db->query("SELECT purchase_id  FROM " . DB_PREFIX . "quotation ORDER BY purchase_id DESC LIMIT 1");
		return $query->row['purchase_id'];
	}
	public function getB2BCustomers()
	{
	  return $this->db->query("SELECT * FROM " . DB_PREFIX . "customers WHERE customercode !='' order by name asc")->rows;
	}
	public function getCustomerDetails($customercode)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers WHERE customercode ='".$customercode."'")->row;
	}
	public function getproductDetails($sku)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE sku ='".$sku."' ")->row;
	}
	public function getproductDetailsById($product_id)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id ='".$product_id."' ")->row;
	}
	public function getCustomerShippingDetails($shipping_id,$from='')
	{
		if($from =='Purchase_Service'){
			return $this->db->query("SELECT *,email as contact_no FROM ".DB_PREFIX."vendor WHERE vendor_id ='".$shipping_id."'")->row;
		}
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping WHERE id ='".$shipping_id."'")->row;
	}
	public function getShippingAddress($vendor_id) {
		return $this->db->query("SELECT * FROM " . DB_PREFIX . "shipping where customer_id = '" .$vendor_id. "'")->rows;
	}
	public function getDeliveryMansList(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."salesman ")->rows;
	}
	public function salesPersonList(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_person where status='1' ")->rows;
	}	
	public function getTotalDelivery($data){
		$sql = "SELECT count(*) as total FROM ".DB_PREFIX."sales_do_header as dh LEFT JOIN ".DB_PREFIX."customers as cu ON dh.customer_id=cu.customercode where dh.sales_invoice_id !='' ";
		if($data['filter_customer_name'] !=''){
			$sql .=" AND cu.name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number'] !=''){
			$sql .=" AND cu.mobile LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction'] !=''){
			$sql .=" AND dh.sales_transaction_no LIKE '%".$data['filter_transaction']."%' ";
		}
		if($data['filter_do_number'] !=''){
			$sql .=" AND dh.do_no LIKE '%".$data['filter_do_number']."%' ";
		}
		if($data['filter_delivery_man'] !=''){
			$sql .=" AND dh.deliveryman_id ='".$data['filter_delivery_man']."' ";
		}
		if(!empty($data['filter_delivery_status'])){
			foreach ($data['filter_delivery_status'] as $delivery_status) {
				if($delivery_status == 'OnDelivery'){
					array_push($data['filter_delivery_status'], 'On Delivery');
				}
			}
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND dh.status IN ('".$data['filter_delivery_status']."')";
		}else{
			/*$sql .= " AND dh.status !='Canceled' ";*/
			$sql .= " AND dh.status IN ('Scheduling','Pending_Delivery','On_Delivery','Delivered','Canceled')";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
				$sql .=" AND ((dh.assignedon between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."') OR dh.assignedon is NULL) ";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$sql .= " AND dh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		return $this->db->query($sql)->row['total'];
	}
	public function getDeliveryList($data){

		$sql = "SELECT dh.*,cu.name FROM ".DB_PREFIX."sales_do_header as dh LEFT JOIN ".DB_PREFIX."customers as cu ON dh.customer_id=cu.customercode where dh.sales_invoice_id !='' ";
		if($data['filter_customer_name'] !=''){
			$sql .=" AND cu.name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number'] !=''){
			$sql .=" AND cu.mobile LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction'] !=''){
			$sql .=" AND dh.sales_transaction_no LIKE '%".$data['filter_transaction']."%' ";
		}
		if($data['filter_do_number'] !=''){
			$sql .=" AND dh.do_no LIKE '%".$data['filter_do_number']."%' ";
		}
		if($data['filter_delivery_man'] !=''){
			$sql .=" AND dh.deliveryman_id ='".$data['filter_delivery_man']."' ";
		}
		if(!empty($data['filter_delivery_status'])){
			foreach ($data['filter_delivery_status'] as $delivery_status) {
				if($delivery_status == 'OnDelivery'){
					array_push($data['filter_delivery_status'], 'On Delivery');
				}
			}
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND dh.status IN ('".$data['filter_delivery_status']."')";
		}else{
			/*$sql .= " AND dh.status !='Canceled' ";*/
			$sql .= " AND dh.status IN ('Scheduling','Pending_Delivery','On_Delivery','Delivered','Canceled')";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
				$sql .=" AND ((dh.assignedon between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."') OR dh.assignedon is NULL) ";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$sql .= " AND dh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		//$sql .=" ORDER BY dh.id DESC";
		$sql .=" ORDER BY dh.sort_id ASC";
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		return $this->db->query($sql)->rows;
	}
	public function getTotalDeliveryDetails($data){
		//$sql = "SELECT count(*) as total FROM ".DB_PREFIX."sales_do_header as dh LEFT JOIN ".DB_PREFIX."customers as cu ON dh.customer_id=cu.customercode where dh.sales_invoice_id !='' ";

		$sql ="SELECT count(*) as total FROM tbl_sales_do_details AS SD LEFT JOIN tbl_sales_do_header AS SH ON SD.do_id = SH.id WHERE SH.sales_invoice_id !='' ";

		if($data['filter_delivery_man'] !=''){
			$sql .=" AND SH.deliveryman_id ='".$data['filter_delivery_man']."' ";
		}
		if(!empty($data['filter_delivery_status'])){
			foreach ($data['filter_delivery_status'] as $delivery_status) {
				if($delivery_status == 'OnDelivery'){
					array_push($data['filter_delivery_status'], 'On Delivery');
				}
			}
			if(isset($data['from']) && $data['from'] == 'Picklist'){
				$sql .= " AND SH.status IN ('Pending_Delivery','Scheduling','On_Delivery')";
			} else {
				$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
				$sql .= " AND SH.status IN ('".$data['filter_delivery_status']."')";
			}
		}else{
			if(isset($data['from']) && $data['from'] == 'Picklist'){
				$sql .= " AND SH.status IN ('Pending_Delivery','Scheduling','On_Delivery')";
			} else {
				$sql .= " AND SH.status !='Canceled' ";
			}
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
				$sql .=" AND ((SH.assignedon between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."') OR SH.assignedon is NULL) ";
		}	
		if(isset($data['from']) && $data['from'] == 'Picklist'){
			$sql .= " AND (SH.assignedon !='' OR SH.deliveryman_id != '')";
		}
		return $this->db->query($sql)->row['total'];
	}
	public function getDeliveryDetailsList($data){
		$sql ="SELECT SD.*,SH.customer_id,SH.do_no,SH.assignedon,SH.deliveryman_id,SH.status,SH.froms FROM tbl_sales_do_details AS SD LEFT JOIN tbl_sales_do_header AS SH ON SD.do_id = SH.id WHERE SH.sales_invoice_id !='' ";
		
		if($data['filter_delivery_man'] !=''){
			$sql .=" AND SH.deliveryman_id ='".$data['filter_delivery_man']."' ";
		}
		if(!empty($data['filter_delivery_status'])){
			foreach ($data['filter_delivery_status'] as $delivery_status) {
				if($delivery_status == 'OnDelivery'){
					array_push($data['filter_delivery_status'], 'On Delivery');
				}
			}
			if(isset($data['from']) && $data['from'] == 'Picklist'){
				$sql .= " AND SH.status IN ('Pending_Delivery','Scheduling','On_Delivery')";
			} else {
				$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
				$sql .= " AND SH.status IN ('".$data['filter_delivery_status']."')";
			}
		}else{
			if(isset($data['from']) && $data['from'] == 'Picklist'){
				$sql .= " AND SH.status IN ('Pending_Delivery','Scheduling','On_Delivery')";
			} else {
				$sql .= " AND SH.status !='Canceled' ";
			}
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
				$sql .=" AND ((SH.assignedon between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."') OR SH.assignedon is NULL) ";
		}
		if(isset($data['from']) && $data['from'] == 'Picklist'){
			$sql .= " AND (SH.assignedon !='' OR SH.deliveryman_id != '')";
		}
		if(isset($data['from']) && $data['from'] == 'Picklist'){
			$sql .=" ORDER BY assignedon DESC,invoice_no DESC";
		} else {
			$sql .=" ORDER BY id DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		return $this->db->query($sql)->rows;
	}
	public function getDeliveryManDetails($delivery_man_id){
		return $this->db->query("SELECT sm.*,rt.code as route_code FROM ".DB_PREFIX."salesman as sm LEFT JOIN ".DB_PREFIX."route as rt ON sm.route_id=rt.id where sm.id ='".$delivery_man_id."' ")->row;
	}
	public function getSalesInvoiceList($data){
		
		$sql = "SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no !='' AND              (sh.delivery_status='Pending' OR sh.delivery_status='Partial_Scheduling')";
		
		if($data['filter_customer_name']){
			$sql .=" AND cu.name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number']){
			$sql .=" AND cu.mobile LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction']){
			$sql .=" AND (sh.invoice_no LIKE '%".$data['filter_transaction']."%' OR sh.network_order_id LIKE '%".$data['filter_transaction']."%')";
		}
		if($data['filter_sales_person']){
			$sql .=" AND sh.sales_man ='".$data['filter_sales_person']."' ";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date']  	 = changeDates($data['filter_from_date']);
			$data['filter_to_date']  	 = changeDates($data['filter_to_date']);
			$sql .=" AND sh.invoice_date between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."'";
		}
		$sql .=" ORDER BY sh.istagged DESC ";
		// echo $sql; die;
		return $this->db->query($sql)->rows;
	}

	public function getSalesServiceList($data){
		
		$sql = "SELECT sh.*,cu.name FROM ".DB_PREFIX."service_sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no !='' AND sh.delivery_status='Pending'";
		
		if($data['filter_customer_name']){
			$sql .=" AND cu.name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number']){
			$sql .=" AND cu.mobile LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction']){
			$sql .=" AND (sh.invoice_no LIKE '%".$data['filter_transaction']."%' OR sh.network_order_id LIKE '%".$data['filter_transaction']."%')";
		}
		if($data['filter_sales_person']){
			$sql .=" AND sh.sales_man ='".$data['filter_sales_person']."' ";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date']  	 = changeDates($data['filter_from_date']);
			$data['filter_to_date']  	 = changeDates($data['filter_to_date']);
			$sql .=" AND sh.invoice_date between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."'";
		}
		return $this->db->query($sql)->rows;
	}
	public function getSalesReturnList($data){
		
		$sql = "SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no !='' AND sh.delivery_status='Pending' AND sales_return='1'";
		
		if($data['filter_customer_name']){
			$sql .=" AND cu.name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number']){
			$sql .=" AND cu.mobile LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction']){
			$sql .=" AND (sh.invoice_no LIKE '%".$data['filter_transaction']."%' OR sh.network_order_id LIKE '%".$data['filter_transaction']."%')";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date']  	 = changeDates($data['filter_from_date']);
			$data['filter_to_date']  	 = changeDates($data['filter_to_date']);
			$sql .=" AND sh.invoice_date between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."'";
		}
		// echo $sql; die;
		return $this->db->query($sql)->rows;
	}
	public function getPurchaseServiceList($data){
		
		$sql = "SELECT ph.transaction_no as invoice_no, ph.transaction_date as invoice_date, ph.total as net_total, cu.vendor_name as name FROM ".DB_PREFIX."service_purchase_header as ph LEFT JOIN ".DB_PREFIX."vendor as cu ON ph.vendor_id=cu.vendor_id where ph.deleted ='0' AND ph.delivery_status='Pending'";
		
		if($data['filter_customer_name']){
			$sql .=" AND cu.vendor_name LIKE '%".$data['filter_customer_name']."%' ";
		}
		if($data['filter_contact_number']){
			$sql .=" AND cu.phone LIKE '%".$data['filter_contact_number']."%' ";
		}
		if($data['filter_transaction']){
			$sql .=" AND ph.transaction_no LIKE '%".$data['filter_transaction']."%'";
		}
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date']  	 = changeDates($data['filter_from_date']);
			$data['filter_to_date']  	 = changeDates($data['filter_to_date']);
			$sql .=" AND ph.transaction_date between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."'";
		}
		return $this->db->query($sql)->rows;	
	}

	public function getDoNo() 
	{
		$query = $this->db->query("SELECT id  FROM " . DB_PREFIX . "sales_do_header ORDER BY id DESC LIMIT 1");
		return $query->row['id'];
	}
	public function getSalesInvoiceHeader($invoice_no) 
	{
		return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no='".$invoice_no."' ")->row;
	}
	public function getSalesInvoiceDetails($invoice_no){
		return $this->db->query("SELECT si.*,ps.sku_qty as avl_qty FROM ".DB_PREFIX."sales_invoice_details as si LEFT JOIN ".DB_PREFIX."product_stock as ps ON si.product_id=ps.product_id AND si.location_code=ps.location_Code where si.invoice_no='".$invoice_no."' ")->rows;
	}
	public function addDelivery($data){  /* |^| */

		if($data['from'] == 'Sales_Invoice'){
			$salesHeader  	  = $this->getSalesInvoiceHeader($data['invoice_no']);
			$salesDetails  	  = $this->getSalesInvoiceDetails($data['invoice_no']); 

			$user_id 	  	  = $this->session->data['user_id'];
			$do_delivery_date = '';
			$sh_delivery_date = '';
			$delivery_status  = 'Pending_Delivery';
			if($data['delivery_date'] !=''){
				$data['delivery_date'] = changeDates($data['delivery_date']);
				if($data['delivery_date'] == date('Y-m-d')){
					$delivery_status   = 'On_Delivery';
				}else if($data['delivery_date'] > date('Y-m-d')){
					$delivery_status   = 'Scheduling';
				}
				$do_delivery_date = ",assignedon='".$data['delivery_date']."'";
				$sh_delivery_date = ",header_remarks='".$data['delivery_date']."'";
			}else{
				$data['delivery_date'] = '';
			}
			if($delivery_status != 'Pending_Delivery' && $data['delivery_man'] !=''){
				$devices[] =  $this->db->query("SELECT * FROM ".DB_PREFIX."salesman WHERE id ='".$data['delivery_man']."'")->row['device_token'];
				$message ="New Delivery Order ".$data['do_no'].' Assigned For You';
				$this->doSendNotificationAndroid($message,$devices,$do_no,'new');
			}

			$sql = "INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,customer_id,shipping_id,deliveryman_id,status,assignedby,froms, location_code, created_by) VALUES('".$salesHeader['id']."','".$salesHeader['invoice_no']."','".$data['do_no']."','".$salesHeader['customer_code']."','".$salesHeader['shipping_id']."','".$data['delivery_man']."','".$delivery_status."','".$user_id."','Sales_Invoice','".$salesHeader['location_code']."', '".$this->session->data['username']."')";

			if($salesHeader['shipping_id']){
				$this->db->query($sql);
				$do_id = $this->db->getLastId();
				$m = $n = 0; //to validate in-stock and outoff-stock
				$err = '';
				foreach ($salesDetails as $details) {
					if($details['qty'] > $details['do_qty']){
						$qty = (int)$details['qty'] - (int)$details['do_qty'];
						$n++;
						$product  = $this->getproductDetailsById($details['product_id']);
						$errs = '';

						if($qty > 0 && $product['package']=='1'){
							$childStk = $this->cart->checkChildItemsStock($details['product_id'],$qty,$salesHeader['location_code']);
							if(!$childStk['status']){
								$errs = $childStk['err'];
							}
							$details['avl_qty'] = $qty; // for avoid parant sku qty validation
						}

						$orderHeader = $this->getSalesOrder($salesHeader['sales_trans_no']);
						if($details['avl_qty'] >= $qty && $errs ==''){
							// Validate that product tagged anywhere 
							if($salesHeader['istagged']){
								$updatAry['invoice_no']    = $salesHeader['invoice_no'];
								$updatAry['product_id']    = $details['product_id'];
								$this->cart->updatetaggedSalesOrder($updatAry);
								$check = true;
							}else{
								$details['needed_qty'] = $qty;
								$check = $this->cart->checkTaggedProduct($details, $orderHeader['invoice_date']);
							}

							if($check){
								$m++;
								$this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,product_id,qty,price)VALUES('".$do_id."','".$data['do_no']."','".$data['invoice_no']."','".$details['sku']."','".$details['product_id']."','".$qty."','".$details['sku_price']."')");

								$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set do_qty = qty, purchase_id='', tagged=0 where sku='".$details['sku']."' AND invoice_no='".$data['invoice_no']."'");
								if($product['package']=='0'){
								}
								$this->db->query("UPDATE ".DB_PREFIX."product_stock set sku_qty = sku_qty-'".$qty."' where product_id='".$details['product_id']."' AND location_Code='".$salesHeader['location_code']."'");
                                
								$child['product_id']    = $details['product_id'];
								$child['location_code'] = $salesHeader['location_code'];
								$child['qty'] 		    = $qty;

								$this->cart->updatechildProductQty($child,'S');
							}else{
								$details['invoice_date']= $orderHeader['invoice_date'];
								$taggedOrders 			= $this->getTaggedOrdersString($details);
								$taggedorderAndCust 	= '';
								foreach($taggedOrders as $order){
									$cust = $this->getSalesOrder($order['order_no']);
									$taggedorderAndCust .= $order['order_no'].'- '.$cust['name'].', ';
								}
								$err .= '<span style="color:red;">'.$salesHeader['invoice_no'].' - '.$details['description'].' - Has tagged to ('.$taggedorderAndCust.') '.'</span><br>';
							}
						}else{ 
							if($errs==''){
							    $err .= '<span style="color:red;">'.$salesHeader['invoice_no'].' - '.$details['description'].' - No stock! </span><br>';
							}else{
								$err .= '<span style="color:red;">'.$salesHeader['invoice_no'].' - '.$errs.'</span><br>';
							}
						}
					}
				}
		        $this->cart->updateOrderandInvoice($salesHeader['invoice_no']);				
				
				if($m == $n){
					$err .= $salesHeader['invoice_no'].' Scheduled<br>';
				}
				if($m){
					if($m != $n){
						$delivery_status = 'Partial_Scheduling';
					}
					$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set sort_id='".$do_id."' ".$do_delivery_date." where id='".$do_id."'");
					$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set delivery_status='".$delivery_status."' ".$sh_delivery_date." where invoice_no='".$salesHeader['invoice_no']."'");
					$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='".$delivery_status."' where invoice_no='".$salesHeader['sales_trans_no']."'");
					return $err;
				}else{
					$this->db->query("DELETE FROM ".DB_PREFIX."sales_do_header where id='".$do_id."'");
					return $err;
				}
			}
		}
		else if($data['from'] == 'Sales_Service'){

			$serviceHeader    = $this->getSalesServiceHeader($data['invoice_no']);
			$serviceDetails	  = $this->getSalesServiceDetails($data['invoice_no']);
			$user_id 	  	  = $this->session->data['user_id'];
			$do_delivery_date = '';
			$sh_delivery_date = '';
			$delivery_status  = 'Pending_Delivery';
			if($data['delivery_date'] !=''){
				$data['delivery_date'] = changeDates($data['delivery_date']);
				if($data['delivery_date'] == date('Y-m-d')){
					$delivery_status  = 'On_Delivery';
				}else if($data['delivery_date'] > date('Y-m-d')){
					$delivery_status  = 'Scheduling';
				}
				$do_delivery_date = ",assignedon='".$data['delivery_date']."'";
				$sh_delivery_date = ",header_remarks='".$data['delivery_date']."'";
			}else{
				$data['delivery_date'] = '';
			}

			$sql = "INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,customer_id,shipping_id,deliveryman_id,status,assignedby,froms) VALUES('".$serviceHeader['id']."','".$serviceHeader['invoice_no']."','".$data['do_no']."','".$serviceHeader['customer_code']."','".$serviceHeader['shipping_id']."','".$data['delivery_man']."','".$delivery_status."','".$user_id."','Sales_Service')";
			
			if($serviceHeader['shipping_id']){
				$this->db->query($sql);
				$do_id = $this->db->getLastId();
				foreach ($serviceDetails as $details) {
					$qty = $details['qty'];
						$this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,product_id,qty,price)VALUES('".$do_id."','".$data['do_no']."','".$data['invoice_no']."','".$details['sku']."','".$details['product_id']."','".$qty."','".$details['sku_price']."')");
				}
				$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set sort_id='".$do_id."' ".$do_delivery_date." where id='".$do_id."'");
				$this->db->query("UPDATE ".DB_PREFIX."service_sales_header set delivery_status='".$delivery_status."' ".$sh_delivery_date." where invoice_no='".$serviceHeader['invoice_no']."'");
				return $serviceHeader['invoice_no'];
			}
		}
		else if($data['from'] == 'Sales_Return'){

			$returnHeader     = $this->getSalesReturnHeader($data['invoice_no']);
			$returnDetails	  = $this->getSalesReturnDetails($data['invoice_no']);
			$user_id 	  	  = $this->session->data['user_id'];
			$do_delivery_date = '';
			$sh_delivery_date = '';
			$delivery_status  = 'Pending_Delivery';
			if($data['delivery_date'] !=''){
				$data['delivery_date'] = changeDates($data['delivery_date']);
				if($data['delivery_date'] == date('Y-m-d')){
					$delivery_status  = 'On_Delivery';
				}else if($data['delivery_date'] > date('Y-m-d')){
					$delivery_status  = 'Scheduling';
				}
				$do_delivery_date = ",assignedon='".$data['delivery_date']."'";
			}else{
				$data['delivery_date'] = '';
			}
			$sql = "INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,customer_id,shipping_id,deliveryman_id,status,assignedby,froms) VALUES('".$returnHeader['id']."','".$returnHeader['invoice_no']."','".$data['do_no']."','".$returnHeader['customer_code']."','".$returnHeader['shipping_id']."','".$data['delivery_man']."','".$delivery_status."','".$user_id."','Sales_Return')";
			
			// echo $sql; die;
			if($returnHeader['shipping_id']){
				$this->db->query($sql);
				$do_id = $this->db->getLastId();
				foreach ($returnDetails as $details) {
					$qty = $details['qty'];
						$this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,product_id,qty,price)VALUES('".$do_id."','".$data['do_no']."','".$data['invoice_no']."','".$details['sku']."','".$details['product_id']."','".$qty."','".$details['sku_price']."')");
				}
				$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set sort_id='".$do_id."' ".$do_delivery_date." where id='".$do_id."'");
				$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='".$delivery_status."' where invoice_no='".$returnHeader['invoice_no']."'");
				return $returnHeader['invoice_no'];
			}
		}
		else if($data['from'] == 'Purchase_Service'){

			$pServiceHeader   = $this->getPurchaseServiceHeader($data['invoice_no']);
			$pServiceDetails  = $this->getPurchaseServiceDetails($data['invoice_no']);
			$user_id 	  	  = $this->session->data['user_id'];
			$do_delivery_date = '';
			$sh_delivery_date = '';
			$delivery_status  = 'Pending_Delivery';
			if($data['delivery_date'] !=''){
				$data['delivery_date'] = changeDates($data['delivery_date']);
				if($data['delivery_date'] == date('Y-m-d')){
					$delivery_status   = 'On_Delivery';
				}else if($data['delivery_date'] > date('Y-m-d')){
					$delivery_status   = 'Scheduling';
				}
				$do_delivery_date      = ",assignedon='".$data['delivery_date']."'";
			}else{
				$data['delivery_date'] = '';
			}

			$sql = "INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,customer_id,shipping_id,deliveryman_id,status,assignedby,froms) VALUES('".$pServiceHeader['purchase_id']."','".$pServiceHeader['transaction_no']."','".$data['do_no']."','".$pServiceHeader['vendor_id']."','".$pServiceHeader['vendor_id']."','".$data['delivery_man']."','".$delivery_status."','".$user_id."','Purchase_Service')";
			
			if($pServiceHeader['vendor_id']){
				$this->db->query($sql);
				$do_id = $this->db->getLastId();
				foreach ($pServiceDetails as $details) {
					$qty = $details['quantity'];
						$this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,product_id,qty,price)VALUES('".$do_id."','".$data['do_no']."','".$data['invoice_no']."','".$details['sku']."','".$details['product_id']."','".$qty."','".$details['price']."')");
				}
				$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set sort_id='".$do_id."' ".$do_delivery_date." where id='".$do_id."'");
				$this->db->query("UPDATE ".DB_PREFIX."service_purchase_header set delivery_status='".$delivery_status."' where transaction_no='".$pServiceHeader['transaction_no']."'");
				return $pServiceHeader['transaction_no'];
			}
		}
		return '';
	}
	public function getSalesOrder($invoice_no)
	{	
		return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode WHERE sh.invoice_no='".$invoice_no."'")->row;
	}	
	public function getTaggedOrdersString($data){
		$sql = "SELECT order_no FROM `tbl_tagged_sales_orders` WHERE order_no IN (SELECT sh.invoice_no FROM tbl_sales_detail as sd LEFT JOIN tbl_sales_header as sh ON sd.invoice_no=sh.invoice_no WHERE sh.location_code='".$data['location_code']."' AND sd.product_id='".$data['product_id']."' AND sh.delivery_status='Pending' AND sh.invoice_date <= '".$data['invoice_date']."' AND sd.tagged='1' GROUP BY sh.invoice_no) AND status='1' GROUP BY order_no";
		return $this->db->query($sql)->rows;
	}
	public function checkTaggedProduct($data){
		$sql = "SELECT sh.sales_trans_no FROM ".DB_PREFIX."sales_invoice_details as sd LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON sd.invoice_no=sh.invoice_no WHERE sd.location_code='".$data['location_code']."' AND sd.product_id='".$data['product_id']."' GROUP BY sh.sales_trans_no";
		$realateOrders = $this->db->query($sql)->rows;

		if(!empty($realateOrders)){
			$realateOrders = array_column($realateOrders, 'sales_trans_no');
			$taggedOrders  = $this->db->query("SELECT order_no FROM ".DB_PREFIX."tagged_sales_orders where order_no IN (".implode($realateOrders,',').") AND status='1' ")->rows;

			if(!empty($taggedOrders)){
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	public function updatetaggedSalesOrder($order_no){
		$this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set status='0' where order_no='".$order_no."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged='0' where invoice_no='".$order_no."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged='0' where sales_trans_no='".$order_no."' ");
	}
	public function getDeliveryManName($deliveryman){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."salesman where id='".$deliveryman."' ")->row['name'];
	}
	public function getDeliveryManAndDate($do_no){
		$delivery = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_do_header where do_no='".$do_no."' ")->row;
		if($delivery['froms'] == 'Sales_Invoice'){
			$remarks    = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where id='".$delivery['sales_invoice_id']."' ")->row['delivery_remarks'];
		}
		if($delivery['froms'] == 'Sales_Service'){
			$remarks    = $this->db->query("SELECT * FROM ".DB_PREFIX."service_sales_header where id='".$delivery['sales_invoice_id']."' ")->row['delivery_remarks'];
		}
		if($delivery['froms'] == 'Sales_Return'){
			$remarks    = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where id='".$delivery['sales_invoice_id']."' ")->row['delivery_remarks'];
		}
		if($delivery['froms'] == 'Purchase_Service'){
			$remarks    = $this->db->query("SELECT * FROM ".DB_PREFIX."service_purchase_header where purchase_id='".$delivery['sales_invoice_id']."' ")->row['delivery_remarks'];
		}
		$delivery['delivery_remarks'] = $remarks;
		return $delivery;
	}
	public function updateDelivery($data){

		$delivery_status = 'Scheduling';
		if($data['delivery_date'] !=''){
			$data['delivery_date'] = changeDates($data['delivery_date']);
			if($data['delivery_date'] == date('Y-m-d')){
				$delivery_status   = 'On_Delivery';
			}
			else if($data['delivery_date'] > date('Y-m-d')){
				$delivery_status   = 'Pending_Delivery';
			}
		}
		if($data['delivery_status'] !=''){
			$delivery_status   = $data['delivery_status'];
		}
		$delivered_on = '';
		if($delivery_status == 'Delivered'){
			$delivered_on = ", deliveredon='".date('Y-m-d H:i:s')."'";
		}
		$doHeader 	   = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_do_header where do_no='".$data['do_no']."'")->row;
		$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set deliveryman_id='".$data['delivery_man']."',assignedon='".$data['delivery_date']."', status='".$this->db->escape($delivery_status)."',reason='".$this->db->escape($data['delivery_remarks'])."' ".$delivered_on." where do_no='".$data['do_no']."'");

		if($doHeader['froms']== 'Sales_Invoice'){
			$invoiceHeader   = $this->getinvoiceHeader($doHeader['sales_transaction_no']);
			// below query for multiple DO all delivered or not
			$undeliverCount  = $this->db->query("SELECT do_no FROM ".DB_PREFIX."sales_do_header where sales_transaction_no='".$doHeader['sales_transaction_no']."' AND status IN ('Pending_Delivery','Scheduling','On_Delivery') AND do_no !='".$doHeader['do_no']."' ")->num_rows;

			// below query for still not DO generated qty  
			$unDOCount 		 = $this->db->query("SELECT (sum(qty) - SUM(do_qty)) as qty FROM ".DB_PREFIX."sales_invoice_details where invoice_no = '".$doHeader['sales_transaction_no']."' GROUP by invoice_no")->row['qty'];
			$totQty          = (int)$undeliverCount + (int)$unDOCount;
			$delivery_status = $totQty > 0 ? 'Partial_Scheduling' : $delivery_status; 
			
			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set delivery_status='".$delivery_status."',delivery_remarks='".$this->db->escape($data['delivery_remarks'])."' where invoice_no ='".$doHeader['sales_transaction_no']."'");
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='".$delivery_status."', delivery_remarks='".$this->db->escape($data['delivery_remarks'])."' where invoice_no ='".$invoiceHeader['sales_trans_no']."'");
			return 1;
		}
		if($doHeader['froms'] == 'Sales_Service'){
			$this->db->query("UPDATE ".DB_PREFIX."service_sales_header set delivery_status='".$delivery_status."',delivery_remarks='".$this->db->escape($data['delivery_remarks'])."'  where id ='".$doHeader['sales_invoice_id']."'");
			return 1;
		}
		if($doHeader['froms'] == 'Sales_Return'){
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='".$delivery_status."',delivery_remarks='".$this->db->escape($data['delivery_remarks'])."' where id ='".$doHeader['sales_invoice_id']."'");
			return 1;
		}
		if($doHeader['froms'] == 'Purchase_Service'){
			$this->db->query("UPDATE ".DB_PREFIX."service_purchase_header set delivery_status='".$delivery_status."',delivery_remarks='".$this->db->escape($data['delivery_remarks'])."' where purchase_id ='".$doHeader['sales_invoice_id']."'");
			return 1;
		}
		if(($doHeader['deliveryman_id'] != $data['delivery_man'] || $doHeader['assignedon'] != $data['delivery_date']) && $delivery_status != 'Scheduling'){
			$devices[] =  $this->db->query("SELECT * FROM ".DB_PREFIX."salesman WHERE id ='".$data['delivery_man']."'")->row['device_token'];
			$message ="Delivery Order ".$do_no." Changed By admin!";
			$this->doSendNotificationAndroid($message,$devices,$do_no);
		}
	}

	public function CancelDelivery($do_no){
		$doHeader  = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_do_header where do_no='".$do_no."'")->row;
		// $doDetails = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_do_details where do_no='".$do_no."'")->rows;
		$doDetails = $this->getDeliveryDetails($do_no);

		$invoice_no= $doHeader['sales_transaction_no'];
		if($doHeader['froms'] == 'Sales_Invoice'){
			if($doHeader['status'] !='Canceled'){
				$invoiceHeader = $this->getinvoiceHeader($invoice_no);
				$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set status='Canceled', reason='Canceled by admin' where do_no='".$do_no."'");
				
				foreach ($doDetails as $doDetail) {
					$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set do_qty=do_qty-'".$doDetail['qty']."' where invoice_no ='".$invoice_no."' AND product_id='".$this->db->escape($doDetail['product_id'])."'");
					if($doDetail['package']=='0'){
						$this->db->query("UPDATE ".DB_PREFIX."product_stock set sku_qty=sku_qty+'".$doDetail['qty']."' where product_id='".$doDetail['product_id']."' AND location_Code='".$invoiceHeader['location_code']."'");
					}
					$doDetail['location_code'] = $invoiceHeader['location_code'];
					$this->cart->updatechildProductQty($doDetail,'P');
				}
				$totCount = $this->db->query("SELECT count(*) as t FROM ".DB_PREFIX."sales_invoice_details WHERE invoice_no='".$invoice_no."'")->row['t'];
				$balanceCnt = $this->db->query("SELECT count(*) as t FROM ".DB_PREFIX."sales_invoice_details WHERE do_qty = 0 AND invoice_no='".$invoice_no."'")->row['t'];

				$delivery_status = 'Partial_Scheduling';
				if($totCount == $balanceCnt){
					$delivery_status = 'Pending';
				}
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set delivery_status='".$delivery_status."' where invoice_no ='".$invoice_no."'");
				$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='".$delivery_status."' where invoice_no ='".$invoiceHeader['sales_trans_no']."'");


				$devices[] =  $this->db->query("SELECT * FROM ".DB_PREFIX."salesman WHERE id ='".$doHeader['deliveryman_id']."'")->row['device_token'];
				$message ="Delivery Order ".$do_no." Canceled By admin!";
				$this->doSendNotificationAndroid($message,$devices,$do_no,'cancel');
			}
		}else{
			$this->updateOtherStatus($doHeader);
		}
		return 1;	
	}
	public function sendDoToDriver($id,$invoice_no){
		$devices[] =  $this->db->query("SELECT * FROM ".DB_PREFIX."salesman WHERE id ='".$id."'")->row['device_token'];
		if(!empty($devices)){
			$this->doSendNotificationAndroid('You have a new Order',$devices,$invoice_no,'new');
		}
	}
	function doSendNotificationAndroid($message,$deviceToken,$invoice_no,$type='',$push_type='',$unreadcnt='',$msgcnt='')
	{
		$apiKey = ANDROID_APIKEY;
	    //$registrationIDs[] = $deviceToken;
	    $url = 'https://fcm.googleapis.com/fcm/send';
	    $title = ($type =='new') ? '':'cancel';
	    $fields = array(
		    'registration_ids' => $deviceToken,
		    'notification'     => array("title" => 'Megafurniture Order '.$title.' Notification',"body" => $message),
	    );
	    $headers = array(
		    'Authorization: key=' . $apiKey,
		    'Content-Type: application/json'
	    );
	    // Open connection
	    $ch = curl_init();
	    // Set the url, number of POST vars, POST data
	    curl_setopt( $ch, CURLOPT_URL, $url );
	    curl_setopt( $ch, CURLOPT_POST, true );
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
	    // Execute post
	    $result = curl_exec($ch);
	    // Close connection
	    curl_close($ch);
	}
	public function updateOtherStatus($doHeader){
		
		$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set status='Canceled', reason='Canceled by admin' where do_no='".$doHeader['do_no']."'");
		if($doHeader['froms'] == 'Sales_Return'){
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='Pending' where invoice_no='".$doHeader['sales_transaction_no']."'");
		}else if($doHeader['froms'] == 'Sales_Service'){
			$this->db->query("UPDATE ".DB_PREFIX."service_sales_header set delivery_status='Pending' where invoice_no='".$doHeader['sales_transaction_no']."'");
		}else if($doHeader['froms'] == 'Purchase_Service'){
			$this->db->query("UPDATE ".DB_PREFIX."service_purchase_header set delivery_status='Pending' where transaction_no='".$doHeader['sales_transaction_no']."'");
		}
	}
	public function getDeliveryDetails($do_no){
		return $this->db->query("SELECT sd.*,p.name,p.package as package FROM ".DB_PREFIX."sales_do_details as sd LEFT JOIN ".DB_PREFIX."product as p ON sd.sku=p.sku where sd.do_no='".$do_no."' ")->rows;
	}
	public function getinvoiceHeader($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no='".$invoice_no."' ")->row;
	}
	public function getSalesServiceHeader($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."service_sales_header where invoice_no='".$invoice_no."'")->row;
	}
	public function getSalesServiceDetails($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."service_sales_detail where invoice_no='".$invoice_no."'")->rows;
	}
	public function getSalesReturnHeader($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where invoice_no='".$invoice_no."'")->row;
	}
	public function getSalesReturnDetails($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where invoice_no='".$invoice_no."'")->rows;
	}
	public function getPurchaseServiceHeader($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."service_purchase_header where transaction_no='".$invoice_no."'")->row;
	}
	public function getPurchaseServiceDetails($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."service_purchase_detail where transaction_no='".$invoice_no."'")->rows;
	}
	public function sortdelivery($sortorder)
	{
    	$sort = 1;
    	foreach ($sortorder  as $key => $do_no) { 
            $sql = $this->db->query("UPDATE ".DB_PREFIX."sales_do_header set sort_id= ".$sort." where do_no='".$do_no."'");
            $sort++;
        }             
        return $sql;
	} 
}
?>