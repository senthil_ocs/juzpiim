<?php
class ModelTransactionSales extends Model { 

   public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "purchase";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='SALINV' ";

		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}

		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getCreditPendingCount($data){

		$sql = "select sum(a.payment_amount) as payment_amount,count(a.invoice_no) as total_records   from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_credit_sales_collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";

	/*	$sql = "SELECT count(SP.invoice_no) as total_records,SUM(SP.payment_amount) as payment_amount FROM vw_sales_paymode as SP
				LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
				LEFT JOIN tbl_credit_sales_collection as SC on SC.invoice_no = SP.invoice_no
				where SP.payment_type='CREDIT' and SP.payment_refno=''";*/

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCreditPendingList($data){

		$sql = "select a.*, b.invoice_date, c.name  from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_credit_sales_collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";


		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}

		$sql .= " order by location_code ASC, invoice_no ASC,invoice_date DESC";
		

		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
	
	$query = $this->db->query($sql);
	return $query->rows;
	}
	public function getCreditPendingListAll($data){

		$sql = "select a.*, b.invoice_date, c.name  from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_credit_sales_collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";


		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}

		$sql .= " order by location_code, invoice_no";
		
	$query = $this->db->query($sql);
	return $query->rows;
	}

	public function getTotalPurchaseHistory($purchase_id,$field_value)

	{

	    $company_id	= $this->session->data['company_id'];

      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";

		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";

		$query = $this->db->query($sql);

		return $query->row['tot'];

	}





	public function getPurchaseProductList($data)

	{

	    $company_id	= $this->session->data['company_id'];

		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "purchase as A, ".DB_PREFIX."purchase_to_product as B WHERE A.purchase_id = B.purchase_id";



		if($data['filter_supplier']){

			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";

		}

		if($data['filter_transactionno']){

			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";

		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){

			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";

		}



		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}

		$query = $this->db->query($sql);

		return $query->rows;

	}
	public function getVendor($code) {
		return $this->db->query("SELECT * FROM ".DB_PREFIX."vendor where vendor_code = '".$code."'")->row;
	}
	public function getVendors() {

		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");

		return $query->rows;

	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$company_id . "' AND transaction_type='SALINV' AND purchase_return IS NULL ";
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}

		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY purchase_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	 public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
  	public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT pt.price AS unit_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC LIMIT 1");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT p.product_id, p.sku, p.quantity, p.price, p.status, p.image, pd.name FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_company pc ON (p.product_id = pc.product_id)
				WHERE pc.company_id = '".$company_id."' AND p.status = '1' AND p.date_available <= NOW() ";
		if(!empty($data['filter_name'])) {
			$sql .= " AND (";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			$sql .= " OR p.sku_shortdescription LIKE '%".$this->db->escape($data['filter_name'])."%'";
			$sql .= " OR LCASE(p.sku) LIKE '%".$this->db->escape(utf8_strtolower($data['filter_name']))."%')";
		}
		$sql 	.= " GROUP BY p.product_id";
		$query   = $this->db->query($sql);
		$results = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{
		$query = $this->db->query("SELECT product_id,quantity,price,sku FROM " . DB_PREFIX . "product WHERE product_id= '".$productId."'");
		return $query->row;
	}
	public function getproductAlldetails($productId)
	{
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE product_id='".$productId."'");
		return $query->row;
	}
	public function getproductdetailsBySKU($sku)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE sku= '".$sku."'");
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{

			$row	= $data[$i];

			$sno 	= $i+1;

			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";



		}

	}

	public function export_purchase_details_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=purchase-details.csv');

		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{

			$row	= $data[$i];

			$sno 	= $i+1;

			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";



		}

	}

	public function getTotalSales($data)
	{
	    $sql = "SELECT COUNT(*) AS totalSale,sum(net_total) as net_total FROM " . DB_PREFIX . "sales_header WHERE invoice_no !='' AND delivery_status !='Canceled' AND sales_return='0' ";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND network_id =  '" . $data['filter_channel'] . "'";
		}
		if($data['filter_location']){
			$sql .= " AND location_code =  '" . $data['filter_location'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no =  '" . $data['filter_transactionno'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getSalesList($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		} 
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE delivery_status!='Canceled' AND invoice_no!='' AND sales_return='0' ";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND network_id =  '" . $data['filter_channel'] . "'";
		}
		if($data['filter_location']){
			$sql .= " AND location_code =  '" . $data['filter_location'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesListPdf($data)
	{
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE delivery_status!='Canceled' AND invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND network_id =  '" . $data['filter_channel'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function export_sales_summary_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=sales-summary.csv');

		print "S No,Location Code,Invoice No,Invoice Date,Sub Total,Discount,GST, Actual Total,Round Off,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			$total+=$row['net_total'];
			print "\"$sno\",\"$row[location_code]\",\"$row[invoice_no]\",\"$row[invoice_code]\",\"$row[sub_total]\",\"$row[discount]\",\"$row[gst]\",\"$row[actual_total]\",\"$row[round_off]\",\"$row[net_total]\"\r\n";
		}
		
	}

	public function getTotalSalesDetail($data)
	{
	    //$company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "sales_detail WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";			

		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
		}
		if($sqldate!=''){
			$sql .= "AND invoice_no IN(".$sqldate.")";
		}
		
		if($data['group']=='1'){
			//$sql .= " GROUP By sku,invoice_date";
		}
		
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	
	public function getSalesListDetail($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_do_detail WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}

		if($data['filter_sku']){
			$data['filter_sku'] = replaceQuotes($data['filter_sku']);
			$sql .= " AND sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			
		}

		if($sqldate!=''){
			$sql .= "AND invoice_no IN(".$sqldate.")";
		}
		if($data['group']=='1'){
			//$sql .= " GROUP By sku,invoice_date";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		// echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalSalesDetailRe($data)
	{
		$sql ="SELECT count(a.sku) as total,sum(a.net_total) net_total from ".DB_PREFIX."sales_detail a
	            INNER JOIN ".DB_PREFIX."sales_header b on a.invoice_no=b.invoice_no
	            INNER JOIN ".DB_PREFIX."product c on c.sku=a.sku 
	            LEFT JOIN  ".DB_PREFIX."department d on d.department_code=c.sku_department_code 
				LEFT JOIN  ".DB_PREFIX."category cat on cat.category_code=c.sku_category_code
				LEFT JOIN  ".DB_PREFIX."brand br on br.brand_code=c.sku_brand_code where b.invoice_date!='' ";

        if($data['filter_transactionno']){
            $sql .= " AND b.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
        }
        if($data['filter_department']){
            $sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
            $sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        }
        if($data['filter_location']){
            $sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
        }    
        if($data['filter_sku']){
            $sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
        }
        if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}

        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $data['filter_date_to']  = changeDates($data['filter_date_to']);
            
            $sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
            
        } else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
            
        } else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
            $data['filter_date_to'] = changeDates($data['filter_date_to']);
            $sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
        }
        $sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku, b.location_code,d.department_name,br.brand_name,cat.category_name';
		$sql.= ' order by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';

        //$sql.= ' group by h.location_code,d.description,d.sku, d.sku_price,h.invoice_date';
        //$sql.= ' order by h.invoice_date desc';
        
        // $sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku,b.location_code';
        // printArray($sql);
        $query = $this->db->query($sql);
        return $query->row;
	}
	public function getSalesSummaryReportBySkuTotalCount($data){
		$sql ="SELECT COUNT(a.sku) as rows_total, sum(a.qty * a.price) as net_total from ".DB_PREFIX."sales_do_details a
	            left join ".DB_PREFIX."sales_do_header b on a.do_no=b.do_no
	            left join ".DB_PREFIX."sales_invoice_header sih on sih.invoice_no=b.sales_transaction_no
	            left join ".DB_PREFIX."product c on c.sku=a.sku 
	            left join ".DB_PREFIX."department d on d.department_code=c.sku_department_code 
					left join ".DB_PREFIX."category cat on cat.category_code=c.sku_category_code
					left join ".DB_PREFIX."brand br on br.brand_code=c.sku_brand_code where sih.invoice_date!='' AND b.status!='Canceled' ";

        if($data['filter_transactionno']){
            $sql .= " AND b.sales_transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
        }
        if($data['filter_department']){
            $sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
            $sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        }
        if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if($data['filter_location']){
			$sql .= " AND sih.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_channel']){
			$sql .= " AND sih.network_id = '" . $this->db->escape($data['filter_channel']) . "'";
		}
		if($data['filter_product_id']){
			$sql .= " AND a.product_id = '" . $this->db->escape(trim($data['filter_product_id'])) . "'";
		}
		else if($data['filter_sku']){
			$sql .= " AND a.sku = '" . $this->db->escape(trim($data['filter_sku'])) . "'";
		}
		if($data['filter_date_from'] != '' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $data['filter_date_to']   = changeDates($data['filter_date_to']);
            $sql .= " AND DATE(b.created_at) between '".$data['filter_date_from']."' AND '".$data['filter_date_to']."'";
        }
        // $sql.= ' GROUP BY sih.location_code';
		  $sql.= ' GROUP BY a.product_id, sih.location_code, a.sku';

        // echo ($sql);
        return $this->db->query($sql)->rows;
	}

	public function getTotalcntFastMovingReport($data)
	{
 		$sql ="SELECT max(b.invoice_date) as invoice_date,b.location_code,a.sku,c.sku_description,d.department_name,sum(a.qty) as sale_qty 
 				from tbl_sales_detail a 
 				inner join tbl_sales_header b on a.invoice_no=b.invoice_no 
 				inner join tbl_product c on c.sku=a.sku 
 				left join tbl_department d on d.department_code = c.sku_department_code
 				where b.invoice_date!='' AND b.delivery_status!='Canceled'";

		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_channel']){
			$sql .= " AND b.network_id = '" . $this->db->escape($data['filter_channel']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}
		
		$sql .= "group by a.sku,b.location_code,c.sku_description,d.department_name";
		if($data['filter_quantity_start']!='' && $data['filter_quantity_end']!=''){
			$sql .= " having (sum(a.qty)>=".$data['filter_quantity_start']." and sum(a.qty)<=".$data['filter_quantity_end'].")";
		}else if($data['filter_quantity_start']!=''){
			$sql .= " having sum(a.qty)>=".$data['filter_quantity_start'];
		}else if($data['filter_quantity_end']!=''){
			$sql .= " having sum(a.qty)<=".$data['filter_quantity_end'];
		}
		if($data['filter_fastlow']=='fast'){
			$sql.= ' order by sum(a.qty) desc';
		}else{
			$sql.= ' order by sum(a.qty) asc';
		}
		$query = $this->db->query($sql);
		return count($query->rows);
	}

	public function getFastMovingDetail($data)
	{
       $sql = "SELECT max(b.invoice_date) as invoice_date,b.location_code,a.sku,c.sku_description,d.department_name,sum(a.qty) as sale_qty,c.name FROM tbl_sales_detail a 
 				INNER JOIN tbl_sales_header b on a.invoice_no=b.invoice_no 
 				INNER JOIN tbl_product c on c.product_id=a.product_id 
 				LEFT JOIN tbl_department d on d.department_code = c.sku_department_code
 				WHERE b.invoice_date!='' AND b.delivery_status!='Canceled' ";

		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_channel']){
			$sql .= " AND b.network_id = '" . $this->db->escape($data['filter_channel']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}
		$sql .= "group by a.sku,b.location_code,c.sku_description,d.department_name";
		if($data['filter_quantity_start']!='' && $data['filter_quantity_end']!=''){
			$sql .= " having (sum(a.qty)>=".$data['filter_quantity_start']." and sum(a.qty)<=".$data['filter_quantity_end'].")";
		}else if($data['filter_quantity_start']!=''){
			$sql .= " having sum(a.qty)>=".$data['filter_quantity_start'];
		}else if($data['filter_quantity_end']!=''){
			$sql .= " having sum(a.qty)<=".$data['filter_quantity_end'];
		}
		if($data['filter_fastlow']=='fast'){
			$sql.= ' order by sum(a.qty) desc';
		}else{
			$sql.= ' order by sum(a.qty) asc';
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSalesListDetailRe($data)
	{
		$sql ="SELECT sih.location_code,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku,sum(a.qty) qty, max(a.price) sku_price,sum(a.qty * a.price) net_total,d.department_name,br.brand_name,cat.category_name from tbl_sales_do_details a
			   left join tbl_sales_do_header b on a.do_no=b.do_no
			   left join tbl_sales_invoice_header sih on sih.invoice_no = b.sales_transaction_no
			   left join tbl_product c on c.product_id=a.product_id 
			   left join tbl_department d on d.department_code=c.sku_department_code 
			   left join tbl_category cat on cat.category_code=c.sku_category_code
 			   left join tbl_brand br on br.brand_code=c.sku_brand_code  where b.created_at !='' AND b.status !='Canceled' AND sih.delivery_status='Delivered' ";
 		
		if($data['filter_transactionno']){
			$sql .= " AND b.sales_transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_department']){
			$sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_category']){
			$sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if($data['filter_location']){
			$sql .= " AND sih.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_channel']){
			$sql .= " AND sih.network_id = '" . $this->db->escape($data['filter_channel']) . "'";
		}
		if($data['filter_product_id']){
			$sql .= " AND a.product_id = '" . $this->db->escape(trim($data['filter_product_id'])) . "'";
		}
		else if($data['filter_sku']){
			$sql .= " AND a.sku = '" . $this->db->escape(trim($data['filter_sku'])) . "'";
		}
		if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND sih.invoice_date between '".$data['filter_date_from']."' AND '".$data['filter_date_to']."'";
			
		}

		$sql.= ' GROUP BY a.product_id,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code,  sih.location_code,d.department_name,br.brand_name,cat.category_name';
		$sql.= ' ORDER BY c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';
		
		$data['total_rows'] =  $this->db->query($sql)->rows;

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			} 
			if($data['filterVal']!='1'){
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		// echo $sql; 
		$data['results'] =  $this->db->query($sql)->rows;
		return $data;
	}
	
	public function getSalesListDetailReCSV($data)
	{
	  
		$sql ="select b.location_code,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code a.sku,sum(a.qty) qty,
			   max(a.sku_price) sku_price,sum(a.discount)discount,  sum(a.net_total) net_total,d.department_name,br.brand_name from tbl_sales_detail a
			   inner join tbl_sales_header b on a.invoice_no=b.invoice_no
			   inner join tbl_product c on c.sku=a.sku 
			   left join tbl_department d on d.department_code=c.sku_department_code 
 			   left join tbl_brand br on br.brand_code=c.sku_brand_code  where b.invoice_date!=''
			   ";

		if($data['filter_transactionno']){
			$sql .= " AND b.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}	
		if($data['filter_sku']){
			$sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}
		if($data['filter_department']){
			$sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_category']){
			$sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}

		
		
		$sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku, b.location_code,d.department_name,br.brand_name';
		$sql.= ' order by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';



		
		//	echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getExpensesListDetailReCSV($data)
	{
	  
		$sql ="select payout_refno,payout_location_code,payout_terminal_code,payout_date,payout_remarks,payout_amount,
			   payout_shift from tbl_pay_out where";
		
		if($data['filter_location']){
			$sql .= " payout_location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			
			$sql .= " AND payout_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} 
			//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalSalesListDetailDepartment($data)
    {
       
     $sql = "SELECT P.sku_department_code,Max(D.department_name) department_name, P.sku_category_code, Max(C.Category_Name) Category_Name, SUM(SD.qty) as total_qty,sum(SD.qty * SD.price) as actual_total FROM tbl_sales_do_details as SD ";

     $sql.= " LEFT JOIN ".DB_PREFIX."sales_do_header as sdh on sdh.do_no = SD.do_no
              LEFT JOIN ".DB_PREFIX."sales_invoice_header as sih on sih.invoice_no = sdh.sales_transaction_no
              LEFT JOIN ".DB_PREFIX."product as P on SD.sku = P.sku
              LEFT JOIN ".DB_PREFIX."department as D on D.department_code = P.sku_department_code
              LEFT JOIN ".DB_PREFIX."category as C on C.category_code = P.sku_category_code WHERE sdh.status !='Canceled' ";

        if($data['filter_location']){
            $sql .= " AND sih.location_code = '" . $this->db->escape($data['filter_location']) . "'";
        }
        if($data['filter_department']){
            $sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
           $sql .= " AND P.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        }
        if($data['filter_channel']){
           $sql .= " AND sih.network_id = '" . $this->db->escape($data['filter_channel']) . "'";
        }
        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $data['filter_date_to']   = changeDates($data['filter_date_to']);
            $sql .= " AND DATE(sdh.created_at) between '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
        }
        $sql.=" Group by P.sku_department_code,P.sku_category_code order by P.sku_department_code,P.sku_category_code";
        $query = $this->db->query($sql);
        return $query->rows;
    
    }
	public function getExpensesListDetailTotal($data)
	{
	  
	    $sql = "SELECT count(payout_refno) as totalrecords,sum(payout_amount) as totalamount  FROM tbl_pay_out WHERE";

		if($data['filter_location']){
			$sql .= " payout_location_code   = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND CAST(payout_date as DATE)  between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	
	}


	public function getExpensesListDetail($data)
	{
	  
	    $sql = "SELECT payout_refno,payout_location_code ,payout_terminal_code,payout_date,payout_remarks,payout_amount,payout_shift  FROM tbl_pay_out WHERE payout_refno !='' ";

		if($data['filter_location']){
			$sql .= " AND payout_location_code ='".$this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND CAST(payout_date as DATE)  between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		
		$sql.= ' order by payout_date desc';

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = $data['limit'];
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}

		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	
	}

	public function getSalesListDetailDepartment($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		//$sql = "SELECT SD.Sku,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL";
		
		$sql = "SELECT P.sku_department_code,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total,SUM(Round_off) as roundoffTot FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL ";

		if($data['filter_sku']){
			$sql .= " AND SD.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND SD.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_department']){
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		}

		$sql.=" Group by P.sku_department_code";

		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.sku_department_code";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		//exit;
		
		// function for get the header_discount , header_round_off
			
		$query = $this->db->query($sql);
		
		$resAry['Listing'] = $query->rows;
			
		return $resAry;
	}
	public function getDiscountDetails($data){
		$sql = "Select Sum(Discount) as dicountTot,Sum(Round_off) as roundoffTot from " . DB_PREFIX . "sales_header where Invoice_Date!=''";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND Invoice_Date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND Invoice_Date  = '" . $data['filter_date_from']."'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND Invoice_Date  = '" . $data['filter_date_to']."'";
		}

		if($data['filter_location']){
			$sql .= " AND Location_Code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row; 
	}
	public function getSalesListDetailDepartmentX($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		//$sql = "SELECT SD.Sku,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL";

		$sql = "SELECT P.sku_department_code,SUM(SD.qty) as qty,SUM(SD.sub_total) as sub_total,SUM(SD.discount) as discount,SUM(SD.net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku 
			LEFT JOIN " . DB_PREFIX . "sales_header as SH on SD.invoice_no = SH.invoice_no WHERE SD.invoice_no!=''";

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SH.invoice_date  = '" . $data['filter_date_from']."'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date  = '" . $data['filter_date_to']."'";
		}

		if($data['filter_location']){
			$sql .= " AND SD.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_department']){
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		

		$sql.=" Group by P.sku_department_code";

		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.sku_department_code";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		
		/*$sql = "SELECT P.sku_department_code,SUM(SD.qty) as qty,SUM(SD.sub_total) as sub_total,SUM(SD.discount) as discount,SUM(SD.net_total) as net_total FROM tbl_sales_detail as SD 
LEFT JOIN tbl_product as P on SD.sku=P.sku 
LEFT JOIN tbl_sales_header as SH on SD.invoice_no = SH.invoice_no
WHERE SD.invoice_no IS NOT NULL AND SD.location_code = 'BB283' AND SH.invoice_date between '2017-12-01' AND '2017-12-01'
Group by P.sku_department_code ORDER BY P.sku_department_code ASC";*/


		// function for get the header_discount , header_round_off
			
		$query = $this->db->query($sql);
		
		
		/*$discountSQL = "SELECT sum(discount) as header_discount,sum(round_off) as header_round_off  FROM " . DB_PREFIX . "sales_header 
							WHERE invoice_no IN('" . implode("', '", $invinos) . "')";
		
		if($data['filter_location']){
			$discountSQL.= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}		

		$discountQry = 	$this->db->query($discountSQL);
		$resAry['cntDetails'] = $discountQry->row;	
		*/
		$resAry['Listing'] = $query->rows;
			
		return $resAry;
	}
	public function getroundOffBasedonDepartment($data,$departmentCode){
		
		$sql = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_no IS NOT NULL";

		if($data['filter_location']){
			$sql.= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
	
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql.= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql.= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		}else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql.= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		

		/*$sql = "SELECT sum(discount) as header_discount,sum(round_off) as header_round_off FROM " . DB_PREFIX . "sales_header 
						WHERE invoice_no IN ('" . implode("', '", $invinos) . "')";*/
		echo $sql;exit;

	}
	public function getDepartmentNameByCode($code){
		$sql = "SELECT department_name FROM " . DB_PREFIX . "department where department_code='".$code."'"; 
		$query = $this->db->query($sql);
		return $query->row['department_name'];
	}
	public function getDepartmentDetailsbySKU($sku){
		$sql = "SELECT sku_department_code,sku_category_code, sku_brand_code FROM " . DB_PREFIX . "product where sku='".$sku."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getInvoiceDateByNo($invoiceno) {
		$sql = "SELECT invoice_date FROM ".DB_PREFIX."sales_header WHERE invoice_no = '".$invoiceno."'";
		$query = $this->db->query($sql);
		return $query->row['invoice_date'];
	}
	public function settlementdetails($date) {
		//$date = '2017-09-12';
		$date = changeDates($date); 
		$sql = "SELECT SUM(net_total) as totalsales, SUM(discount) as discount,SUM(gst) as gst,SUM(round_off) as round_off FROM " . DB_PREFIX . "sales_header WHERE Cast(createdon As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		//echo $sql ;exit;
		return $query->row;
	}

	public function paymentmodedetails($type,$date) {
		$date = changeDates($date); 
		$sql = "SELECT SUM(payment_amount) as paymentamount FROM " . DB_PREFIX . "sales_paymode WHERE payment_type = '".$type."' AND Cast(createdon As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getLocation(){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id != ''"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getLocationCode($companyId){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "company as C LEFT JOIN " . DB_PREFIX . "location as L ON C.location_code = L.location_code WHERE company_id = '".$companyId."' "; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getTotalSalesByLocationCode($data)
	{
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
      	$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "sales_header WHERE $where";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	public function getSalesListByLocationCode($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT * FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY invoice_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCollectionPaymentDetails($location_code,$invoice_date,$terminal_code=''){
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_paymode WHERE location_code = '".$location_code."' AND invoice_no = '".$invoice_no."'"; 
		$sql = "SELECT SUM(payment_amount) as payment_amount,payment_type FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE invoice_no IN (SELECT invoice_no FROM " . DB_PREFIX . "Credit_Sales_Collection where collection_date = '".$invoice_date."' AND location_code='".$location_code."') GROUP BY(payment_type)"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getPaymentDetails($location_code,$invoice_date){
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_paymode WHERE location_code = '".$location_code."' AND invoice_no = '".$invoice_no."'"; 
		$sql = "SELECT SUM(payment_amount) as payment_amount,payment_type FROM " . DB_PREFIX . "sales_paymode WHERE invoice_no IN (SELECT invoice_no FROM " . DB_PREFIX . "sales_header where invoice_date = '".$invoice_date."' AND location_code='".$location_code."') GROUP BY(payment_type)"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getTotalSalesListByDate($data){
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,invoice_date  FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesListByDate($data){
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,invoice_date FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY invoice_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if($data['filterVal'] !='1'){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalCreditSalesListByDate($data){
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,collection_date  FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND collection_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCreditSalesListByDate($data){
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,collection_date FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND collection_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY collection_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if($data['filterVal'] !='1'){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSalesListByCodeDate($location_code,$invoice_date){
		$sql = "SELECT SUM(actual_total) AS actual_total, SUM(gst) as gst, SUM(net_total) as net_total, SUM(discount) as discount,SUM(round_off) as round_off FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getSalesInvoiceByCodeDate($location_code,$invoice_date){
		$sql = "SELECT invoice_no FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getCCListByCodeDate($location_code,$invoice_date,$terminal_code=''){
		$sql = "SELECT SUM(payment_amount) AS payment_amount FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getCCListByCodeDateNew($location_code,$invoice_date,$terminal_code=''){
		//$sql = "SELECT SUM(payment_amount) AS payment_amount FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$sql = "select sc.shift_paymode as payment_type,sum(shift_system_amount) as payment_amount from " . DB_PREFIX . "shift_creditdetail as sc left join " . DB_PREFIX . "shift_header as sh on sh.shift_refno = sc.shift_ref_no where  sh.shift_location_code='".$location_code."' and sh.shift_date='".$invoice_date."' GROUP by(sc.shift_paymode)";

		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getCCListByCodeDateNewTotal($location_code,$invoice_date,$terminal_code=''){
		$sql = "select sum(shift_system_amount) as payment_amount from " . DB_PREFIX . "shift_creditdetail as sc left join " . DB_PREFIX . "shift_header as sh on sh.shift_refno = sc.shift_ref_no where  sh.shift_location_code='".$location_code."' and sh.shift_date='".$invoice_date."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}

	public function getCCInvoiceByCodeDate($location_code,$invoice_date,$terminal_code=''){
		$sql = "SELECT invoice_no FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getTerminalsByLocation($date='',$location_code=''){
		$date = changeDates($date); 
		$sql = "SELECT DISTINCT shift_terminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_terminal!='' and shift_location_code='".$location_code."'";  
		//$sql = "SELECT DISTINCT shift_teminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_teminal!='' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCashierByTerminalLocation($date='',$location_code='',$terminal_code=''){
		$date = changeDates($date); 
		$sql = "SELECT DISTINCT shift_cashier  FROM " . DB_PREFIX . "shift_header where shift_date ='".$date."' AND shift_terminal='".$terminal_code."' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getShiftByTerminalLocation($date='',$location_code='',$terminal_code=''){
		$date = changeDates($date); 
		$sql = "SELECT DISTINCT shift_no  FROM " . DB_PREFIX . "shift_header where shift_date ='".$date."' AND shift_terminal='".$terminal_code."' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTerminals($date='',$location_code=''){
		$date = changeDates($date); 
		$sql = "SELECT DISTINCT shift_terminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_terminal!='' and shift_location_code='".$location_code."'";
		//$sql = "SELECT DISTINCT shift_teminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_teminal!='' and shift_location_code='".$location_code."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getLocations(){
		$sql = "SELECT DISTINCT L.location_code,L.location_name FROM " . DB_PREFIX . "shift_header as SH LEFT JOIN " . DB_PREFIX . "location as L ON SH.shift_location_code = L.location_code"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getReportsLocations($date){
		$date = changeDates($date); 
		$sql = "SELECT DISTINCT L.location_code,L.location_name FROM " . DB_PREFIX . "shift_header as SH LEFT JOIN " . DB_PREFIX . "location as L ON SH.shift_location_code = L.location_code WHERE Cast(shift_date As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function settlementshiftsums($date,$teminal='',$location_code='',$shift_no='') {
		$date = changeDates($date); 
		$sql = "SELECT SUM(shift_nettotal) as totalsales,SUM(shift_total) as subtotalsales, SUM(shift_discount) as discount,SUM(shift_gst) as gst FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."' and shift_location_code='".$location_code."'"; 
		if($teminal){
			$sql.= "AND shift_terminal = '".$teminal."' AND shift_no='".$shift_no."'";
			//$sql.= "AND shift_teminal = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function settlementshiftdetails($date,$teminal='',$shift_no='') {
		$date = changeDates($date); 
		$sql = "SELECT shift_refno, shift_date, shift_cashier, shift_terminal as shift_teminal  FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."'"; 
		//$sql = "SELECT shift_refno, shift_date, shift_cashier, shift_teminal as shift_teminal  FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."'"; 
		if($teminal){
			$sql.= "AND shift_terminal = '".$teminal."' AND shift_no='".$shift_no."'";
			//$sql.= "AND shift_teminal = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function paymentmodeshiftdetails($type,$regno) {
		$date = changeDates($date); 
		$sql = "SELECT SUM(shift_system_amount) as paymentamount FROM " . DB_PREFIX . "shift_detail WHERE shift_paymode = '".$type."' AND shift_ref_no = '".$regno."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}	

	public function paymentdetailsSettlement($regno) {
		$date = changeDates($date); 
		$sql = "SELECT SUM(shift_system_amount) as sys_paymentamount,SUM(shift_manual_amount) as mnl_paymentamount,sum(shift_difference) as diff, shift_paymode FROM " . DB_PREFIX . "shift_detail WHERE shift_ref_no = '".$regno."' group by shift_paymode";
		$query = $this->db->query($sql);
		return $query->rows;
	}	


	public function paymentCreditdetails($regno) {
		$date = changeDates($date); 
		$sql = "SELECT SUM(shift_system_amount) as paymentamount,shift_paymode FROM " . DB_PREFIX . "shift_creditdetail WHERE shift_ref_no = '".$regno."' GROUP BY(shift_paymode)"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}	

	public function settlementcollectiondetails($date,$shift_refno='',$teminal='') {
		$date = changeDates($date); 
		$sql = "SELECT * FROM " . DB_PREFIX . "daily_collection_summary WHERE Cast(collection_date As Date) = '".$date."'"; 
		if($shift_refno){
			$sql.= "AND shift_refno = '".$shift_refno."'";
		}
		if($teminal){
			$sql.= "AND terminal_code = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	
	public function getTotalVoidSalesDetail($data)
	{
		$sql = "SELECT COUNT(*) AS totalSale,SUM(net_total) as net_total FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getVoidSalesListDetail($data)
	{
	    /*if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}*/
		$sql = "SELECT * FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}
		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}
		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		// }
		
		$query = $this->db->query($sql);
		return $query->rows;
	}	
	public function getInvoiceVoidDateByNo($invoiceno) {
		$sql = "SELECT invoice_date FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no = '".$invoiceno."'";
		$query = $this->db->query($sql);
		return $query->row['invoice_date'];
	}
	public function getTotalVoidSalesListDetailByInvNo($data)
	{
		$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "void_sales_detail WHERE invoice_no = '".$data['invoice_no']."'";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		}
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	public function getVoidSalesListDetailByInvNo($data){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "void_sales_detail WHERE invoice_no = '".$data['invoice_no']."'";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
public function getCashflowList($data){
	
		$sql = "SELECT DCS.*, SH.shift_location_code FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if($data['filter_teminal']){
			$sql .= " AND DCS.terminal_code LIKE '%" . $this->db->escape($data['filter_teminal']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['short_by']) && $data['short_by'] !='') {
            $sql .= " ORDER BY  DCS." . $data['short_by'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['short_by_order']) && ($data['short_by_order'] !='')) {
            $sql .=" ".$data['short_by_order'];
        } else {
            $sql .= " ASC";
        }
		/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " ASC";
        } */
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		/*$query = $this->db->query("select * from tbl_shift_header"); 
       printArray($query);*/
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCashflowCount($data){

		$sql = "SELECT count(DCS.shift_refno) as total_records FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if($data['filter_teminal']){
			$sql .= " AND DCS.terminal_code LIKE '%" . $this->db->escape($data['filter_teminal']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";

		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}

         //echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCashflowListAll($data){
        
		$sql = "SELECT DCS.*, SH.shift_location_code FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if(!empty($data['filter_teminal'])){
			$sql .= " AND DCS.terminal_code = '" . $data['filter_teminal'] . "'";
		}

		if(!empty($data['filter_location'])){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";

		}
		
		if(!empty($data['filter_date_from']!='' && $data['filter_date_to']!='')){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if(!empty($data['filter_date_from']!='' && $data['filter_date_to']=='')){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if(!empty($data['filter_date_from']=='' && $data['filter_date_to']!='')){
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['short_by']) && $data['short_by'] !='') {
            $sql .= " ORDER BY " .$data['short_by'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['short_by_order']) && ($data['short_by_order'] != '')) {
            $sql .= " ".$data['short_by_order'];
        } else {
            $sql .= " ASC";
        }
		/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " ASC";
        }*/
		
	$query = $this->db->query($sql);
	return $query->rows;
	}

	public function getcreditpendingsummaryCount($data){

	$sql = "SELECT COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
		where SP.invoice_no not in(select invoice_no from tbl_credit_sales_collection) and SP.payment_type='CREDIT' 
		AND SP.payment_refno=''";

		if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}


		// $sql.=" GROUP BY SP.location_code,SH.customer_code";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCustomernameByCode($code){
		$sql = "SELECT name from tbl_member where member_code='".$code."'"; 
		$query = $this->db->query($sql);
		return $query->row['name'];	

	}
	public function getcreditpendingsummaryList($data){

	$sql = "SELECT SP.location_code,SH.customer_code,COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
	    where SP.invoice_no not in(select invoice_no from tbl_credit_sales_collection) and SP.payment_type='CREDIT' and SP.payment_refno=''";
	    
	    if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}

	    $sql.="GROUP BY SP.location_code,SH.customer_code";
		if (isset($data['start']) || isset($data['limit'])) {
			$sql .= " ORDER BY SH.customer_code ASC";
			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		

	$query = $this->db->query($sql);
	return $query->rows;
	}
	public function getcreditpendingsummaryListAll($data){

		$sql = "SELECT SP.location_code,SH.customer_code,COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP
LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
where SP.invoice_no not in(select invoice_no from tbl_credit_sales_collection) and SP.payment_type='CREDIT' and SP.payment_refno=''"; 


	
		if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}
		
		$sql .= "GROUP BY SP.location_code,SH.customer_code";


		if(!empty($data['order'])&&!empty($data['sort'])){
			$sql .= " order  by '".$data['sort']."''  '".$data['order']."' ";

		}/*else{
			$sql.="order by  createdon DESC";
		}*/
		
	$query = $this->db->query($sql);
	return $query->rows;
	}
	/*public function getsalesDetailsbyInvoice($invoiceno){
		$sql = "select * from tbl_sales_detail where invoice_no='".$invoiceno."' order by createdon asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}*/
	public function getsalesDetailsbyInvoice($invoiceno){
		$sql = "SELECT sd.*,prod.name,prod.sku as sku_name from tbl_sales_detail as sd Left Join tbl_product as prod on sd.product_id = prod.product_id where invoice_no='".$invoiceno."' order by createdon asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCustomer(){
		$sql = "SELECT member_code,name from tbl_member order by name asc"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getTotalSalesnew($data)
	{
		if($data['filter_product_id'] !=''){
			$details = " LEFT JOIN ".DB_PREFIX."sales_detail as sd ON sh.invoice_no=sd.invoice_no ";
		}
      	$sql = "SELECT COUNT(*) AS totalsales FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode ".$details." WHERE sh.sales_return='0' AND sh.deleted='0'";
		
		if($data['filter_customer_name']!=''){
			$sql .= " AND cu.name LIKE '%".$data['filter_customer_name']."%'";
		}
		if($data['filter_contact_number']!=''){
			$sql .= " AND cu.mobile LIKE '%".$data['filter_contact_number']."%'";
		}
		if($data['filter_transaction']){
			$sql .= " AND sh.invoice_no LIKE '%".$data['filter_transaction']."%'";
		}
		if($data['filter_order_number']){
			$sql .= " AND sh.network_order_id LIKE '%".$data['filter_order_number']."%'";
		}
		if($data['filter_sales_person']){
			$sql .= " AND sh.sales_man ='".$data['filter_sales_person']."'";
		}
		if($data['filter_isinvoiced'] !=''){
			$sql .= " AND sh.isinvoice ='".$data['filter_isinvoiced']."'";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		} 
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			if($data['filter_is_delivery']){
				$sql .= " AND sh.header_remarks between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}else{
				$sql .= " AND sh.invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}
		}
		if(!empty($data['filter_delivery_status'])){
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		}else{
			$sql .= " AND sh.delivery_status NOT IN ('Canceled') ";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$data['filter_payment_status'] = str_replace('Pendings','Pending',$data['filter_payment_status']);
			$sql .= " AND sh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_product_id'] !=''){
			$sql .= " AND (sd.product_id ='".$data['filter_product_id']."' OR sd.product_id IN (SELECT parant_sku from ".DB_PREFIX."child_products where child_sku='".$data['filter_product_id']."'))";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row['totalsales'];

	}
	public function addSalesnew($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		// printArray($data); die;
		$total = 0;
		$gst   = 0;
		$sub_total =0;
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}
		}

		   $data['header_remarks']   = $data['reference_date'];
		   $data['transaction_date'] = changeDates($data['transaction_date']);
		   $data['header_remarks']   = changeDates($data['header_remarks']);
		   $data['reference_to_date']= changeDates($data['reference_to_date']);

			if(empty($data['bill_discount_price'])) {
				$data['bill_discount_price'] = '0.00';
			}
			if(empty($data['bill_discount_percentage'])) {
				$data['bill_discount_percentage'] = '0';
			}
			
			if($data['currency_code']=='SGD'){
				$data['conversion_rate'] = '1';
			}
			$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
			$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
			$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
			$data['fc_nettotal'] 	  = $data['conversion_rate'] * $data['total']; 

		    $sqlheader= "INSERT INTO " . DB_PREFIX . "sales_header (location_code,invoice_no,invoice_date,customer_code,sub_total,discount,gst,actual_total,round_off,net_total,cashier,terminal_code,shift_no,detail_lines,header_remarks,createdby,modifiedby,
		    discount_type,hold,sales_quotation_trans_no,reference_no,tax_class_id,tax_type,bill_discount_percentage,bill_discount_price,handling_fee,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,term_id,shipping_id,sales_man,remarks,delivery_remarks,reference_to_date,discount_remarks,network_order_id,network_id) VALUES(
		         '" .$this->db->escape($data['location_code']). "', '" . $this->db->escape($data['transaction_no']) . "', '" . $this->db->escape($data['transaction_date']) . "', '" . $this->db->escape($data['vendor']) . "', '" . $this->db->escape($data['fc_subtotal'])."', '" . $this->db->escape($data['fc_discount'])."', '" . $this->db->escape($data['fc_tax']). "', '" . $sub_total . "', '0.00', '" . $this->db->escape($data['fc_nettotal']). "', '" . $cashier . "', '" . $terminal_code . "', '0', '0', '" . $this->db->escape($data['header_remarks']) . "', '" . $userName . "','" . $userName . "','Sales', '".$this->db->escape($data['hold'])."', '".$this->db->escape($data['sales_quotation_trans_no'])."', '".$this->db->escape($data['reference_no'])."', '".$this->db->escape($data['tax_class_id'])."', '".$this->db->escape($data['tax_type'])."', '".$this->db->escape($data['bill_discount_percentage'])."', '".$this->db->escape($data['bill_discount_price'])."', '".$this->db->escape($data['fc_handling_fee'])."', '".$this->db->escape($data['currency_code'])."', '".$this->db->escape($data['conversion_rate'])."', '".$sub_total."', '".$gst."', '".$discount."', '".$handling_fee."', '".$this->db->escape($data['total'])."', '".$this->db->escape($data['term_id'])."', '".$this->db->escape($data['shipping_id'])."','".$this->db->escape($data['sales_man'])."','".$this->db->escape($data['remarks'])."','".$this->db->escape($data['delivery_remarks'])."','".$this->db->escape($data['reference_to_date'])."','".$this->db->escape($data['discount_remarks'])."','".$this->db->escape($data['reference_no'])."','".$this->db->escape($data['network_id'])."')";
			// echo $sqlheader; die;
		    $res = $this->db->queryNew($sqlheader);
        
		    if(!$res){
			  header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
				exit;
		    }
		    $updteSql = "UPDATE ".DB_PREFIX."quotation set issaled='1' where transaction_no='".$data['sales_quotation_trans_no']."' ";
		    $this->db->query($updteSql);

		for ($i=0; $i <count($data['products']) ; $i++) {
    	      $data['round_off']='0.00';
    	      if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		      }
			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}
			$psku = $this->getSkuByProductId($data['products'][$i]['product_id']);
			$tax_class_id = $data['products'][$i]['tax_class_id'];
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='0'){
			    $data['product'][$i]['net_price'] = $data['product'][$i]['total'] + $data['product'][$i]['tax_price'];
			}
			$childAry['product_id'] = $data['products'][$i]['product_id'];
			$childAry['order_no']   = $data['transaction_no'];
			$childAry['type']   		= 'SO';
			$this->cart->insertChildItems($childAry);

			if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
			    $data['product'][$i]['total'] = $data['product'][$i]['total'] - $data['product'][$i]['tax_price'];
			}
            $data['products'][$i]['name'] = str_replace("'",'',$data['products'][$i]['name']);
		    $sqlsales= "INSERT INTO " . DB_PREFIX . "sales_detail (location_code,invoice_no,sku,product_id,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id,conversion_rate,cust_description) VALUES(
				  '" . $this->db->escape($data['location_code']). "',
				  '" . $this->db->escape($data['transaction_no']) . "',
				  '" . $psku."',
				  '" . $this->db->escape($data['products'][$i]['product_id'])."',
				  '" . $this->db->escape($data['products'][$i]['name']). "',
				  '" . $this->db->escape($data['products'][$i]['quantity']) . "',
				  '" . $this->db->escape($data['products'][$i]['price']) . "',
				  '" . $this->db->escape($data['products'][$i]['total']). "',
				  '" . $this->db->escape($data['products'][$i]['purchase_discount']) . "',
				  '" . $this->db->escape($data['products'][$i]['tax_price']). "',
				  '" . $this->db->escape($data['products'][$i]['net_price']) . "',
				  '" . $this->db->escape($data['round_off']) . "',
				  '" . $this->db->escape($data['products'][$i]['total']) . "',
				  '" . $this->db->escape($data['remarks']) . "',
				  '" . $this->db->escape($data['products'][$i]['sku_cost']) . "',
				  '" . $this->db->escape($data['products'][$i]['sku_avg']) . "',
				  '" . $userName . "',
				  curdate(),
				  '" . $userName . "',
				  curdate(),
				  '" . $discount_percentage."',
				  '" . $discount_price."',
				  '" . $tax_class_id."',
				  '" . $this->db->escape($data['conversion_rate'])."',
				  '" . $this->db->escape($data['cust_description'][$data['products'][$i]['product_id']])."' )";

            $res = $this->db->queryNew($sqlsales);
			   if(!$res){
			   		$this->db->query("DELETE " . DB_PREFIX . "sales_detail where invoice_no='".$data['transaction_no']."'");
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);
					exit;
			   }
		}
		$purchaseId = $this->db->getLastId();
		return $purchaseId;		
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}

	public function editSalesnew($purchaseId, $data)
	{
		// printArray($data); die;
	   $companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];

		   $discount='0.00';
           foreach ($data['totals'] as $totals) {
				if($totals['code']=='sub_total'){
					$sub_total  = $totals['value'];
				}else if($totals['code']=='tax'){
					$gst   = $totals['value'];
				}else if($totals['code']=='total'){
					$total 	 = $totals['value'];
				}else if($totals['code']=='discount'){
					$discount = $totals['value'];
				}else if($totals['code']=='handling'){
					$handling_fee = $totals['value'];
				}
		   }
		   $transaction_date           = changeDates($data['transaction_date']);
		   $data['header_remarks']     = '';
		   if($data['reference_date'] !=''){
			   $data['header_remarks']  = changeDates($data['reference_date']);
		   }
		   if($data['reference_to_date'] !=''){
		   		$data['reference_to_date'] = changeDates($data['reference_to_date']);
		   }
		   // printArray($data['reference_to_date']); die;
		    if(empty($data['bill_discount_price'])) {
				$data['bill_discount_price'] = '0.00';
			}
			if(empty($data['bill_discount_percentage'])) {
				$data['bill_discount_percentage'] = '0';
			}

			$sales_header = $this->getSalesByinvNonew($data['transaction_no']);
			$data['conversion_rate']  = $sales_header['conversion_rate'];
			$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
			$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
			$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
			$data['fc_nettotal'] 	  = $data['conversion_rate'] * $total;
			$data['sales_man'] 		  = $data['sales_man'] =='' ? 0 : $data['sales_man'];

		  $sqlheader="UPDATE " . DB_PREFIX . "sales_header SET
				location_code = '" . $this->db->escape($data['location_code']) . "'
				, invoice_date = '" . $transaction_date . "'
				, customer_code = '" . $this->db->escape($data['vendor']) . "'
				, sub_total = '" . $this->db->escape($data['fc_subtotal']). "'
				, discount = '" .$this->db->escape($data['fc_discount']). "'
				, tax_class_id = '" .$this->db->escape($data['tax_class_id']). "'
				, tax_type = '" .$this->db->escape($data['tax_type']). "'
				, gst = '" . $this->db->escape($data['fc_tax'])."'
				, bill_discount_price = '" . $this->db->escape($data['bill_discount_price'])."'
				, bill_discount_percentage = '" . $this->db->escape($data['bill_discount_percentage'])."'
				, handling_fee = '" . $this->db->escape($data['fc_handling_fee'])."'
				, actual_total = '" . $sub_total."'
				, round_off = '0.00'
				, net_total = '" . $this->db->escape($data['fc_nettotal']). "'
				, discount_type = 'Sales'
				, hold = '" . $this->db->escape($data['hold']) . "'
				, reference_no = '" . $this->db->escape($data['reference_no']) . "'
				, header_remarks = '" . $this->db->escape($data['header_remarks']) . "'
				, modifiedby = '" . $userName . "'
				, modifiedon = '".date('Y-m-d H:i:s')."'
				, fc_subtotal = '".$sub_total."'
				, fc_tax = '".$gst."'
				, fc_discount = '".$discount."'
				, fc_handling_fee = '".$handling_fee."'
				, fc_nettotal = '".$total."'
				, term_id = '".$this->db->escape($data['term_id'])."'
				, shipping_id = '".$this->db->escape($data['shipping_id'])."'
				, sales_man = '".$this->db->escape($data['sales_man'])."'
				, remarks = '".$this->db->escape($data['remarks'])."'
				, delivery_remarks  = '".$this->db->escape($data['delivery_remarks'])."'
				, reference_to_date = '".$this->db->escape($data['reference_to_date'])."'
				, discount_remarks = '".$this->db->escape($data['discount_remarks'])."'
				, network_id = '".$this->db->escape($data['network_id'])."'
				, network_order_id = '".$this->db->escape($data['reference_no'])."'
				WHERE invoice_no = '" . $this->db->escape($data['transaction_no']). "'";
        
        // echo $sqlheader; die;
	    $this->db->query($sqlheader);
		$available_items = array_column($data['products'], 'product_id');
       $this->db->query("DELETE FROM ".DB_PREFIX."sales_detail WHERE invoice_no = '" . $data['transaction_no']."' AND product_id NOT IN (".implode($available_items,',').") ");
       $this->db->query("DELETE FROM ".DB_PREFIX."order_child_items WHERE order_no = '" . $data['transaction_no']."'");

		for ($i=0; $i <count($data['products']) ; $i++) {
    	    $data['round_off']='0.00';
    	    if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		    }
			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}
			
			$tax_class_id = $data['products'][$i]['tax_class_id']; 
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='0'){
			    $data['products'][$i]['net_price'] = $data['products'][$i]['total'] + $data['products'][$i]['tax_price'];
			}
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
			    $data['products'][$i]['total'] = $data['products'][$i]['total'] - $data['products'][$i]['tax_price'];
			}
			$childAry['product_id'] = $data['products'][$i]['product_id'];
			$childAry['order_no']   = $data['transaction_no'];
			$childAry['type']   		= 'SO';
			$this->cart->insertChildItems($childAry);
			$exist = $this->checkProductExist($data['transaction_no'], $data['products'][$i]['product_id']);
			
			if(empty($exist)){		
				$psku = $this->getSkuByProductId($data['products'][$i]['product_id']);
				$data['products'][$i]['name'] = str_replace("'",'',$data['products'][$i]['name']);
			    $sqlsales= "INSERT INTO " . DB_PREFIX . "sales_detail (location_code,invoice_no,sku,product_id,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id,conversion_rate,cust_description) VALUES(
					  '" . $this->db->escape($data['location_code']). "',
					  '" . $this->db->escape($data['transaction_no']) . "',
					  '" . $psku. "',
					  '" . $this->db->escape($data['products'][$i]['product_id']). "',
					  '" . $this->db->escape($data['products'][$i]['name']) . "',
					  '" . $this->db->escape($data['products'][$i]['quantity']) . "',
					  '" . $this->db->escape($data['products'][$i]['price']) . "',
					  '" . $this->db->escape($data['products'][$i]['total']). "',
					  '" . $this->db->escape($data['products'][$i]['purchase_discount']) . "',
					  '" . $this->db->escape($data['products'][$i]['tax_price']). "',
					  '" . $this->db->escape($data['products'][$i]['total']) . "',
					  '" . $data['round_off'] . "',
					  '" . $this->db->escape($data['products'][$i]['net_price'])."',
					  '" . $this->db->escape($data['remarks']) . "',
					  '" . $this->db->escape($data['products'][$i]['sku_cost']) . "',
					  '" . $this->db->escape($data['products'][$i]['sku_avg']) . "',
					  '" . $userName . "',
					  curdate(),
					  '" . $userName . "',
					  curdate(),
					  '" . $discount_percentage. "',
					  '" . $discount_price. "',
					  '" . $tax_class_id. "',
					  '" . $data['conversion_rate']."',
					  '" . $this->db->escape($data['cust_description'][$data['products'][$i]['product_id']])."' )";
	               $res = $this->db->queryNew($sqlsales);
             }else{
             	// printArray($data['products']); die;
				    $sql = "UPDATE ".DB_PREFIX."sales_detail set 
				    qty 				= '".$this->db->escape($data['products'][$i]['quantity'])."',
				    sku_price 		= '".$this->db->escape($data['products'][$i]['price'])."',
				    sub_total 		= '".$this->db->escape($data['products'][$i]['total'])."',
				    discount 		= '".$this->db->escape($data['products'][$i]['purchase_discount'])."',
				    gst 				= '".$this->db->escape($data['products'][$i]['tax_price'])."',
				    actual_total 	= '".$this->db->escape($data['products'][$i]['total'])."',
				    round_off 		= '".$this->db->escape($data['products'][$i]['round_off'])."',
				    net_total 		= '".$this->db->escape($data['products'][$i]['net_price'])."',
				    remarks 		= '".$this->db->escape($data['remarks'])."',
				    sku_cost 		= '".$data['products'][$i]['sku_cost']."',
				    sku_avg 		= '".$data['products'][$i]['sku_avg']."',
				    modifiedby 			= '".$userName."',
				    modifiedon 			= curdate(),
				    discount_percentage = '".$discount_percentage."',
				    discount_price 		= '".$discount_price."',
				    tax_class_id 			= '".$tax_class_id."',
				    conversion_rate 		= '".$data['conversion_rate']."',
				    cust_description 	= '".$this->db->escape($data['cust_description'][$data['products'][$i]['product_id']])."'
				    where invoice_no 	= '".$data['transaction_no']."' AND product_id ='".$data['products'][$i]['product_id']."' ";
				    // echo $sql; die;
             	 $this->db->queryNew($sql);
             }
		}
		return $purchaseId;
	}
	public function getSalesAutoIdnew() 
	{
		$query = $this->db->query("SELECT id  FROM " . DB_PREFIX . "sales_header ORDER BY id DESC LIMIT 1");
		return $query->row['id'];
	}
	public function checkProductExist($invoice_no, $product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where invoice_no='".$invoice_no."' AND product_id='".$product_id."' ")->row;
	}
	public function getProductByNamenew($data = array()) 
	{
		$company_id	= $this->session->data['company_id'];
		$location   = $this->session->data['location_code'];
		$sql 		= "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,ps.location_code FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_stock ps ON (p.sku = ps.sku) WHERE p.product_id !='0' AND ps.location_code='".$location."' ";

		if(!empty($data['filter_name'])) {
			$data['filter_name_convert']= html_entity_decode($data['filter_name']);
			$data['filter_name_entity'] = htmlentities($data['filter_name']);

			$sql .= " AND ( p.sku_shortdescription LIKE '%".$this->db->escape(trim($data['filter_name']))."%' 
			OR p.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' 
			OR p.name LIKE '%".$this->db->escape(trim($data['filter_name_convert']))."%' 
			OR p.name LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%' 
			OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%'
			OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name_convert']))."%'
			OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%') ";
		}
		$sql .=" GROUP BY p.product_id";
		// echo $sql;
		return $this->db->query($sql)->rows;
	}
	public function getB2BCustomers()
	{
	  $query = $this->db->query("SELECT tax_allow,tax_type,customercode,name FROM " . DB_PREFIX . "customers WHERE customercode !='' order by name asc");
	  return $query->rows;
	}
	public function getB2BCustomersbyCode($customercode,$from='')
	{
		if($from =='Purchase_Service'){
	  		$query = $this->db->query("SELECT *,vendor_name as name FROM ".DB_PREFIX."vendor WHERE vendor_id ='".$customercode."'");
		}else{
	  		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers WHERE customercode ='".$customercode."'");
		}
		return $query->row;
	}
	public function getcustomerPrice($data = array()) 
	{
		$sql = "SELECT sku,customer_price FROM " . DB_PREFIX . "customers_price";

	     $sql.= " WHERE customers_code != ''";
	    if($data['sku']!=''){
			$sql .= " AND sku =  '" . $data['sku'] . "'";
		}
		if($data['transaction_date']!=''){
			$sql .= " AND ('" .$data['transaction_date']. "' between FromDate and ToDate)";
		}
		if($data['customer_code']!=''){
			$sql .= " AND customers_code =  '" . $data['customer_code'] . "'";
		}
		if($data['location_code']!=''){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		//echo $sql; exit;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getcustomerPriceByProduct($data) 
	{
		$sql = "SELECT customer_price FROM ".DB_PREFIX."customers_price WHERE ('".date('Y-m-d')."' between FromDate and ToDate) AND customers_code = '".$data['cust']."' AND sku ='".$data['sku']."' AND location_code = '".$data['location_code']."' ";
		return $this->db->query($sql)->row['customer_price'];
	}
	public function getSalesByinvNonew($invoiceNumber)
	{
		return $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" . $invoiceNumber . "'")->row;
	}
	public function getSalesInvoiceByinvoice($invoiceNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_invoice_header WHERE invoice_no = '" . $invoiceNumber . "'");
		return $query->row;
	}
	public function DeleteSalesBysku($sku,$invoiceNumber)
	{
	   $sql = "DELETE FROM " . DB_PREFIX . "b2b_sales_detail WHERE sku = '" . $sku . "' and invoice_no = '" . $invoiceNumber . "'"; 
		$this->db->query($sql);

	}
	public function getSalesListnew($data)
	{
		if($data['filter_product_id'] !=''){
			$details = " LEFT JOIN ".DB_PREFIX."sales_detail as sd ON sh.invoice_no=sd.invoice_no ";
		}
		$sql = "SELECT sh.*,l.location_name FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode LEFT JOIN ".DB_PREFIX."location as l ON sh.location_code=l.location_code ".$details." WHERE sh.sales_return='0'AND sh.deleted='0' ";

		if($data['filter_customer_name']!=''){
			$sql .= " AND cu.name LIKE '%".$data['filter_customer_name']."%'";
		}
		if($data['filter_contact_number']!=''){
			$sql .= " AND cu.mobile LIKE '%".$data['filter_contact_number']."%'";
		}
		if($data['filter_transaction']){
			$sql .= " AND sh.invoice_no LIKE '%".$data['filter_transaction']."%'";
		}
		if($data['filter_order_number']){
			$sql .= " AND sh.network_order_id LIKE '%".$data['filter_order_number']."%'";
		}
		if($data['filter_sales_person']){
			$sql .= " AND sh.sales_man ='".$data['filter_sales_person']."'";
		}
		if($data['filter_isinvoiced'] !=''){
			$sql .= " AND sh.isinvoice ='".$data['filter_isinvoiced']."'";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		}

		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			if($data['filter_is_delivery']){
				$sql .= " AND sh.header_remarks between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}else{
				$sql .= " AND sh.invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}
		}
		if(!empty($data['filter_delivery_status'])){
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		} else{
			$sql .= " AND sh.delivery_status NOT IN ('Canceled') ";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$data['filter_payment_status'] = str_replace('Pendings','Pending',$data['filter_payment_status']);
			$sql .= " AND sh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_product_id'] !=''){
			$sql .= " AND (sd.product_id ='".$data['filter_product_id']."' OR sd.product_id IN (SELECT parant_sku from ".DB_PREFIX."child_products where child_sku='".$data['filter_product_id']."'))";
		}
		$sql .=' ORDER BY sh.invoice_date DESC,sh.id DESC ';
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesnew($purchaseId)
	{
		return $this->db->query("SELECT  * FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" .$purchaseId . "'")->row;
	}
	public function getQuotation($purchaseId)
	{
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "quotation WHERE purchase_id = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getSalesnewtotal($purchaseId)
	{
		$query = $this->db->query("SELECT  customer_code,sub_total,discount,gst,actual_total,net_total FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getSalesProductnew($purchaseId)
	{
		$sql = "SELECT SD.*,P.sku as sku,P.name as description from ".DB_PREFIX."sales_detail as SD
				LEFT join " . DB_PREFIX . "product as P on SD.product_id=P.product_id WHERE SD.invoice_no = '" .$purchaseId . "' "; 
		return $this->db->query($sql)->rows;
	}
	public function getQuotationProducts($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "quotation_products WHERE purchase_id = '" .$purchaseId . "'");
		return $query->rows;
	}
	public function getSalesProductbysku($sku,$transaction_no)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_detail WHERE sku = '" .$sku . "' and invoice_no = '" .$transaction_no. "'");
		return $query->row;
	}
	public function clear_type(){

		$query = $this->db->query("SELECT void_type FROM ".DB_PREFIX."clearcart_detail WHERE void_type != '' group by void_type ");
		return $query->rows;
	}

	public function get_clearcartdetailsTotal($data){

		$sql = "SELECT count(sku) as total FROM " . DB_PREFIX . "clearcart_detail ";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDates($data['filter_date_from']); 
			$data['filter_date_to']    = changeDates($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function get_suppliersales($data){

			$sql = "SELECT SD.sku,P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name, count(SD.sku) as saleQty from tbl_sales_header as SH
				LEFT join tbl_sales_detail as SD on SH.invoice_no=SD.invoice_no
				LEFT join tbl_product as P on SD.sku=P.sku
				LEFT join tbl_vendor as V on V.vendor_code = P.sku_vendor_code ";

			if($data['filter_date_from'] && $data['filter_date_to']){
				$sql .=" where SH.invoice_date between '".$data['filter_date_from']."' and '".$data['filter_date_to']."' ";
			}
			if($data['filter_location']){
				$sql .=" AND SH.location_code='".$data['filter_location']."' ";
			}
			if($data['filter_supplier'] !='All'){
				$sql .= "AND P.sku_vendor_code= '".$data['filter_supplier']."' ";
			}
			$sql .= " AND P.sku_vendor_code!='' GROUP BY SD.sku";
		
				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY SD.sku";
				}
				if (isset($data['order']) && ($data['order'] == 'ASC')) {
					$sql .= " ASC";
				} else {
					$sql .= " DESC";
				}
				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}
					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}
					
					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
				}
			$query = $this->db->query($sql);
			return $query->rows;
	}
	public function get_suppliersalesNew($data){
			$sql = "SELECT SD.sku,P.name,SD.price as sku_price,P.sku_vendor_code,V.vendor_name, count(SD.sku) as saleQty from tbl_sales_do_header as SH
				LEFT join tbl_sales_do_details as SD on SH.do_no=SD.do_no
				LEFT join tbl_sales_invoice_header as sih on sih.invoice_no=SH.sales_transaction_no
				LEFT join tbl_product as P on SD.sku=P.sku
				LEFT join tbl_vendor as V on V.vendor_code = P.sku_vendor_code WHERE SH.status!='Canceled' ";

			if($data['filter_date_from'] && $data['filter_date_to']){
				$sql .=" AND sih.invoice_date between '".$data['filter_date_from']."' and '".$data['filter_date_to']."' ";
			}
			if($data['filter_location']){
				$sql .=" AND sih.location_code='".$data['filter_location']."' ";
			}
			if($data['filter_channel']){
				$sql .=" AND sih.network_id ='".$data['filter_channel']."' ";
			}
			if($data['filter_supplier'] !='All'){
				$sql .= "AND P.sku_vendor_code= '".$data['filter_supplier']."' ";
			}
			$sql .= " AND P.sku_vendor_code!='' GROUP BY SD.sku";
			$data['rows'] = $this->db->query($sql)->rows;

				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY SD.sku";
				}
				if (isset($data['order']) && ($data['order'] == 'ASC')) {
					$sql .= " ASC";
				} else {
					$sql .= " DESC";
				}
				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}
					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}
					
					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
				}
			$data['results'] = $this->db->query($sql)->rows;
			return $data;
	}	
	public function get_clearcartdetails($data){
		$sql = "SELECT * FROM " . DB_PREFIX . "clearcart_detail ";
		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDates($data['filter_date_from']); 
			$data['filter_date_to']    = changeDates($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function get_totalclearcartdetails($data)
	{
		$sql = "SELECT sum(net_total) as totalSale FROM " . DB_PREFIX . "clearcart_detail ";

		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDates($data['filter_date_from']); 
			$data['filter_date_to']    = changeDates($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function get_totalsupplierssku($data){

			$sql = "SELECT SD.sku,P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name, count(SD.sku) as saleQty from tbl_sales_header as SH
				LEFT join tbl_sales_detail as SD on SH.invoice_no=SD.invoice_no
				LEFT join tbl_product as P on SD.sku=P.sku
				LEFT join tbl_vendor as V on V.vendor_code = P.sku_vendor_code ";

			if($data['filter_date_from'] && $data['filter_date_to']){
				$sql .=" where SH.invoice_date between '".$data['filter_date_from']."' and '".$data['filter_date_to']."' ";
			}
			if($data['filter_location']){
				$sql .=" AND SH.location_code='".$data['filter_location']."' ";
			}
			if($data['filter_supplier']!='All'){
				$sql .= "AND P.sku_vendor_code= '".$data['filter_supplier']."' ";
			}
			$sql .= " AND P.sku_vendor_code!='' GROUP BY SD.sku ";
			$query = $this->db->query($sql);
			return count($query->rows);
	}
	/*sales new end */
	public function getVoidSalesDetailreport($data) {
			$sql = "SELECT * FROM ".DB_PREFIX."sales_header where delivery_status='Canceled' AND invoice_no!='' AND sales_return='0' ";

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDates($data['filter_date_from']); 
				$data['filter_date_to']  = changeDates($data['filter_date_to']); 
				$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			if($data['filter_invoiceno']){
				$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_invoiceno']) . "%'";
			}
			if($data['filter_location']){
				$sql .= " AND location_code = '" . $data['filter_location'] . "'";
			}
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY invoice_no";
			}
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);
			return $query->rows;
		}

	public function getVoidSalesDetail($invoice_no,$field_value='')
	{
	  	$sql = "SELECT * FROM ".DB_PREFIX."sales_detail WHERE invoice_no = '".$invoice_no."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function updateInvoiceNumber($id){

		$lastId = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key`='config_next_sales_invoiceno' ")->row;

		$update = $this->db->query("UPDATE ".DB_PREFIX."sales_header set invoice_nom = '".$lastId['value']."' where invoice_no='".$id."' ");
		if($update){
			$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."setting set value = value+1 WHERE `key`='config_next_sales_invoiceno' ");
		}
		return $lastId['value'];
	}
	public function getProductName($id){
		$sql= $this->db->query("SELECT name FROM ".DB_PREFIX."product where product_id='".$id."' ")->row;
		return $sql['name'];
	}
	public function updateStatus($id){
		$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."sales_header set status ='Void' WHERE invoice_no='".$id."' ");
	}
	public function deletePurchase($invoice_no){
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set deleted='1' where invoice_no='".$invoice_no."'");
	}
	public function updatechildProductQty($product_id,$qty){  //need to add location as well
		$child_product = $this->db->query("SELECT child_sku FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ")->rows;
		$location 	   = $this->session->data['location_code'];
		
		if(!empty($child_product)){
			foreach ($child_product as $value) {
				$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'" .$qty."' WHERE product_id = '".$value['child_sku']."' AND location_Code='".$location."' ");
			}
		}
	}
	//Sales Invoice querys
	public function getTotalSalesInvoice($data)
	{
		if($data['filter_product_id'] !=''){
			$details = " LEFT JOIN ".DB_PREFIX."sales_invoice_details as sd ON sh.invoice_no=sd.invoice_no ";
		}
	    $sql = "SELECT COUNT(*) AS totalsales FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode ".$details." WHERE sh.customer_code != '' AND sh.deleted='0' ";

		if($data['filter_customer_name'] !=''){
			$sql .= " AND cu.name LIKE '%".$data['filter_customer_name']."%'";
		}
		if($data['filter_contact_number'] !=''){
			$sql .= " AND cu.mobile LIKE '%".$data['filter_contact_number']."%'";
		}
		if($data['filter_transaction'] !=''){
			$sql .= " AND sh.invoice_no LIKE '%".$data['filter_transaction']."%'";
		}
		if($data['filter_order_number'] !=''){
			$sql .= " AND sh.network_order_id LIKE '%".$data['filter_order_number']."%'";
		}
		if($data['filter_sales_person'] !=''){
			$sql .= " AND sh.sales_man ='".$data['filter_sales_person']."'";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		}
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			if($data['filter_is_delivery']){
				$sql .= " AND sh.header_remarks between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}else{
				$sql .= " AND sh.invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}
		}
		$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
		if(!empty($data['filter_delivery_status'])){
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		}else{
			$sql .= " AND sh.delivery_status NOT IN ('Canceled')";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$data['filter_payment_status'] = str_replace('Pendings','Pending',$data['filter_payment_status']);
			$sql .= " AND sh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_xero'] != 0){
			if($data['filter_xero'] == '1') {
				$sql .= " AND sh.xero_sales_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND sh.xero_sales_id  IS NULL ";
			}
		}
		if($data['filter_product_id'] !=''){
			$sql .= " AND (sd.product_id ='".$data['filter_product_id']."' OR sd.product_id IN (SELECT parant_sku from ".DB_PREFIX."child_products where child_sku='".$data['filter_product_id']."'))";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row['totalsales'];
	}
	
	public function getTotalSalesInvoiceForReports($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalsales, sum(net_total) as net_total FROM " . DB_PREFIX . "sales_invoice_header WHERE customer_code != '' AND deleted='0' ";

		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			$sql .= " AND invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_transaction']){
			$sql .= " AND (invoice_no LIKE '%".$data['filter_transaction']."%' OR network_order_id LIKE '%".$data['filter_transaction']."%' )";
		}
		if($data['filter_delivery_status']){
			$sql .= " AND delivery_status = '" . $data['filter_delivery_status'] . "'";
		}else{
			$sql .= " AND delivery_status != 'Canceled' ";
		}
		if($data['filter_payment_status'] !=''){
			$sql .= " AND payment_status = '" . $data['filter_payment_status'] . "'";
		}
		if($data['filter_network'] !=''){
			$sql .= " AND network_id = '" . $data['filter_network'] . "'";
		}
		if($data['filter_customer'] !=''){
			$sql .= " AND customer_code = '" . $data['filter_customer'] . "'";
		}
		if($data['filter_agent'] !=''){
			$sql .= " AND sales_man = '" . $data['filter_agent'] . "'";
		}
		if($data['filter_xero'] != 0){
			if($data['filter_xero'] == '1') {
				$sql .= " AND xero_sales_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND xero_sales_id  IS NULL ";
			}
		}
		// echo $sql;
		return $this->db->query($sql)->row;
	}
	public function getSalesInvoiceHeaderForReports($data)
	{
      $sql = "SELECT * FROM ".DB_PREFIX."sales_invoice_header WHERE customer_code != '' AND deleted='0' ";
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			$sql .= " AND invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_transaction']){
			$sql .= " AND (invoice_no LIKE '%".$data['filter_transaction']."%' OR network_order_id LIKE '%".$data['filter_transaction']."%' )";
		}
		if($data['filter_delivery_status']){
			$sql .= " AND delivery_status = '" . $data['filter_delivery_status'] . "'";
		}else{
			$sql .= " AND delivery_status != 'Canceled' ";
		}
		if($data['filter_payment_status'] !=''){
			$sql .= " AND payment_status = '" . $data['filter_payment_status'] . "'";
		}
		if($data['filter_network'] !=''){
			$sql .= " AND network_id = '" . $data['filter_network'] . "'";
		}
		if($data['filter_customer'] !=''){
			$sql .= " AND customer_code = '" . $data['filter_customer'] . "'";
		}
		if($data['filter_agent'] !=''){
			$sql .= " AND sales_man = '" . $data['filter_agent'] . "'";
		}
		if($data['filter_xero'] != ''){
			if($data['filter_xero'] == '1') {
				$sql .= " AND xero_sales_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND xero_sales_id  IS NULL ";
			}
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		return $this->db->query($sql)->rows;
	}
	public function getTotalSalesQuotation($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalsales, sum(total) as net_total FROM " . DB_PREFIX . "quotation WHERE  deleted='0' ";
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date'] = changeDates($data['filter_to_date']);
			$sql .= " AND transaction_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_channel']!=''){
			$sql .= " AND network_id = '" . $data['filter_channel'] . "'";
		}
		if($data['filter_transaction']){
		 $sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transaction']) . "%'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getSalesQuotationList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "quotation WHERE  deleted='0' ";
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date'] = changeDates($data['filter_to_date']);
			$sql .= " AND transaction_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_channel']!=''){
			$sql .= " AND network_id = '" . $data['filter_channel'] . "'";
		}
		if($data['filter_transaction']){
		 $sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transaction']) . "%'";
		}
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY transaction_no";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesQuotationListPdf($data)
	{
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "quotation WHERE transaction_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND transaction_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date =  '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_channel']){
			$sql .= " AND network_id =  '" . $data['filter_channel'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY created_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getsalesquotationDetailsbyInvoice($purchase_id){
		$sql = "SELECT Qp.*,prod.name,prod.sku as sku_name,prod.sku_shortdescription as description from tbl_quotation_products as Qp LEFT JOIN tbl_product as prod on Qp.sku = prod.sku where purchase_id='".$purchase_id."' order by transaction_no asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesInvoiceListPdf($data)
	{
		$sql = "SELECT DISTINCT * FROM ".DB_PREFIX."sales_invoice_header WHERE invoice_no IS NOT NULL";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getsalesinvoiceDetailsbyInvoice($invoiceno){
		$sql = "SELECT * FROM ".DB_PREFIX."sales_invoice_details where invoice_no='".$invoiceno."' order by createdon asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function export_sales_invoice_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=sales-invoice.csv');

		print "S No,Location Code,Invoice No,Invoice Date,Sub Total,Discount,GST, Actual Total,Round Off,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			$total+=$row['net_total'];
			print "\"$sno\",\"$row[location_code]\",\"$row[invoice_no]\",\"$row[invoice_code]\",\"$row[sub_total]\",\"$row[discount]\",\"$row[gst]\",\"$row[actual_total]\",\"$row[round_off]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_sales_quotation_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=sales-quotation.csv');
		print "S No,Location Code,Transaction No,Transaction Date,Sub Total,Discount,GST, Actual Total,Round Off,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			$total+=$row['net_total'];
			print "\"$sno\",\"$row[location_code]\",\"$row[transaction_no]\",\"$row[transaction_date]\",\"$row[sub_total]\",\"$row[discount]\",\"$row[gst]\",\"$row[actual_total]\",\"$row[round_off]\",\"$row[net_total]\"\r\n";
		}
	}

	public function getSalesInvoiceList($data)
	{
		if($data['filter_product_id'] !=''){
			$details = " LEFT JOIN ".DB_PREFIX."sales_invoice_details as sd ON sh.invoice_no=sd.invoice_no ";
		}
		$sql = "SELECT sh.*,cu.xero_id as xero_cust_id,cu.name as cust_name,l.location_name FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode LEFT JOIN ".DB_PREFIX."location as l ON sh.location_code=l.location_code ".$details." WHERE sh.customer_code != '' AND sh.deleted='0' ";

		if($data['filter_customer_name'] !=''){
			$sql .= " AND cu.name LIKE '%".$data['filter_customer_name']."%'";
		}
		if($data['filter_contact_number'] !=''){
			$sql .= " AND cu.mobile LIKE '%".$data['filter_contact_number']."%'";
		}
		if($data['filter_customer'] !=''){ //only for report modules
			$sql .= " AND sh.customer_code = '".$data['filter_customer']."' ";
		}
		if($data['filter_transaction'] !=''){
			$sql .= " AND sh.invoice_no LIKE '%".$data['filter_transaction']."%'";
		}
		if($data['filter_order_number'] !=''){
			$sql .= " AND sh.network_order_id LIKE '%".$data['filter_order_number']."%'";
		}
		if($data['filter_sales_person'] !=''){
			$sql .= " AND sh.sales_man ='".$data['filter_sales_person']."'";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		}
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			if($data['filter_is_delivery']){
				$sql .= " AND sh.header_remarks between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}else{
				$sql .= " AND sh.invoice_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
			}
		}
		if(!empty($data['filter_delivery_status'])){
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		}else{
			$sql .= " AND sh.delivery_status NOT IN ('Canceled')";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$data['filter_payment_status'] = str_replace('Pendings','Pending',$data['filter_payment_status']);
			$sql .= " AND sh.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_xero'] != 0){
			if($data['filter_xero'] == '1') {
				$sql .= " AND sh.xero_sales_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND sh.xero_sales_id  IS NULL ";
			}
		}
		if($data['filter_product_id'] !=''){
			$sql .= " AND (sd.product_id ='".$data['filter_product_id']."' OR sd.product_id IN (SELECT parant_sku from ".DB_PREFIX."child_products where child_sku='".$data['filter_product_id']."'))";
		}
		$sql .= " ORDER BY sh.id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT ".(int)$data['start'].",".(int)$data['limit'];
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesInvoiceAutoId() 
	{
		$query = $this->db->query("SELECT id  FROM " . DB_PREFIX . "sales_invoice_header ORDER BY id DESC LIMIT 1");
		return $query->row['id'];
	}
	
	public function getOrderPaymentStatus($invoice_no)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_payment AS op LEFT JOIN " . DB_PREFIX . "customers AS cust ON op.customer_id = cust.customercode WHERE op.order_id = '".$invoice_no."' ORDER BY op.id DESC")->rows;
		return $query;
	}
	public function addSalesInvoice($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId	   = $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$total 		= 0;
		$gst   		= 0;
		$sub_total 	= 0;

		foreach ($data['totals'] as $totals) {
			if($totals['code']=='sub_total'){
				$sub_total = $totals['value'];
			}else if($totals['code']=='tax'){
				$gst = $totals['value'];
			}else if($totals['code']=='discount'){
				$discount = $totals['value'];
			}else if($totals['code']=='total'){
				$total = $totals['value'];
			}else if($totals['code']=='handling'){
				$handling_fee = $totals['value'];
			}
		}
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0.00';
		}
		if(empty($data['bill_discount_percentage'])) {
			$data['bill_discount_percentage'] = '0';
		}
		

	    $data['total']=$sub_total+$gst;
	    $data['header_remarks']   = changeDates($data['reference_date']);
	    $data['delivery_to_date'] = changeDates($data['delivery_to_date']);
		if($data['currency_code']=='SGD'){
			$data['conversion_rate'] = '1';
		}
		$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
		$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal'] 	  = $data['conversion_rate'] * $total;
		
		$payment_status = 'Pending';
		$allowedNetworks= array('1','2','3','4','5');
		$paid_amount    = '0';

		$old_invoice  = $this->getSalesInvoice($data['transaction_no']);
		if(!empty($old_invoice)){
			$transNumber = $this->getSalesInvoiceAutoId();
			$transNumber = $transNumber + 1;
			 $data['transaction_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transNumber, 4, '0', STR_PAD_LEFT);
		}
		if($this->config->get('network_default_payment_method') == '1'){
			if(in_array($data['network_id'], $allowedNetworks)){

				$allowPay = true;
				if(($data['network_id'] == '1' || $data['network_id'] =='3')){
					$allowPay = false;
					if($data['remarks'] !=''){
						$p_status = explode('-', $data['remarks']);
						if($p_status[0] == 'Paid'){
							$allowPay = true;	
						}
					}
				}
				if($allowPay){
					$data['invoice_no']    = $data['transaction_no'];
					$data['customer_code'] = $data['vendor'];
					$data['net_total']     = $data['fc_nettotal'];
					$res = $this->addDirectPayment($data);
					if($res =='1'){
						$payment_status = 'Paid';
						$paid_amount    = $data['net_total'];
					}
				}
			}
		}
		/*if($this->config->get('network_default_payment_method') == '1'){
			if(in_array($data['network_id'], $allowedNetworks) && $data['remarks'] !=''){
				$p_status = explode('-', $data['remarks']);
				if($p_status[0] == 'Paid'){
					$data['invoice_no']    = $data['transaction_no'];
					$data['customer_code'] = $data['vendor'];
					$data['net_total']     = $data['fc_nettotal'];
					$res = $this->addDirectPayment($data);
					if($res =='1'){
						$payment_status = 'Paid';
						$paid_amount    = $data['net_total'];
					}
				}
			}
		}*/

	    $sqlheader = "INSERT INTO " . DB_PREFIX . "sales_invoice_header (location_code,invoice_no,invoice_date,customer_code,sub_total,discount,gst,actual_total,round_off,net_total,cashier,terminal_code,shift_no,detail_lines,header_remarks,createdby,modifiedby,discount_type,reference_no,sales_trans_no,tax_class_id,tax_type,bill_discount_percentage,bill_discount_price,handling_fee,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,term_id,shipping_id,network_id,network_order_id,remarks,delivery_remarks,delivery_to_date,sales_man,discount_remarks,istagged,payment_status,paid_amount) VALUES('" . $this->db->escape($data['location_code']). "', '" . $this->db->escape($data['transaction_no']) . "', curdate(), '" . $this->db->escape($data['vendor']) . "', '" . $this->db->escape($data['fc_subtotal'])."', '" . $this->db->escape($data['fc_discount'])."', '" . $this->db->escape($data['fc_tax']). "', '" . $sub_total. "', '0.00', '" . $this->db->escape($data['fc_nettotal'])."', '" . $cashier . "', '" . $terminal_code . "', '0', '0', '" . $this->db->escape($data['header_remarks']) . "', '" . $userName . "', '" . $userName . "', 'Sales', '".$this->db->escape($data['reference_no'])."', '".$this->db->escape($data['sales_trans_no'])."', '".$this->db->escape($data['tax_class_id'])."', '".$this->db->escape($data['tax_type'])."', '".$this->db->escape($data['bill_discount_percentage'])."', '".$this->db->escape($data['bill_discount_price'])."','".$this->db->escape($data['fc_handling_fee'])."','".$this->db->escape($data['currency_code'])."', '".$this->db->escape($data['conversion_rate'])."', '".$sub_total."', '".$gst."', '".$discount."','".$handling_fee."', '".$total."', '".$this->db->escape($data['term_id'])."', '".$this->db->escape($data['shipping_id'])."', '".$this->db->escape($data['network_id'])."', '".$this->db->escape($data['network_order_id'])."','".$this->db->escape($data['remarks'])."','".$this->db->escape($data['delivery_remarks'])."','".$this->db->escape($data['delivery_to_date'])."','".$this->db->escape($data['sales_man'])."','".$this->db->escape($data['discount_remarks'])."','".$this->db->escape($data['istagged'])."','".$payment_status."','".$paid_amount."')";

		     $res = $this->db->queryNew($sqlheader);
        	 $data['invoice_id'] = $this->db->getLastId();
		    if(!$res){
			  header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
				exit;
		    }
		    $updSql = "UPDATE ".DB_PREFIX."sales_header set isinvoice='1' where invoice_no='".$data['sales_trans_no']."' ";
		    $this->db->query($updSql);

		for ($i=0; $i <count($data['products']) ; $i++) {
    	   $data['round_off']='0.00';
    	   if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		   }
			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}

			$location_code = $this->session->data['location_code'];
			$psku 			= $this->getSkuByProductId($data['products'][$i]['product_id']);
			/*Qty will update when DO genarate (sku_qty=sku_qty-'".$qty."',) */
			$avgCostupdate = "UPDATE " . DB_PREFIX . "product_stock SET modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" .$psku. "' AND location_Code='".$location_code."'";
			$this->db->query($avgCostupdate);
			
			$childAry['product_id'] = $data['products'][$i]['product_id'];
			$childAry['order_no']   = $data['transaction_no'];
			$childAry['type']   		= 'SI';
			$this->cart->insertChildItems($childAry);

			$tagged = 0;
			if($data['sales_trans_no'] !=''){
				$tagged = $this->checkProductTaggedOrNot($data['sales_trans_no'], $data['products'][$i]['product_id']);
			}
			$tax_class_id 	= $data['products'][$i]['tax_class_id'];
    	   $total 			= $data['products'][$i]['total']+$data['products'][$i]['tax_price'];
            $data['products'][$i]['name'] = str_replace("'",'',$data['products'][$i]['name']);
		   $sqlsales 		= "INSERT INTO ".DB_PREFIX."sales_invoice_details (location_code,invoice_no,sku,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id,conversion_rate,product_id,cust_description,tagged) VALUES(
				  '" . $this->db->escape($data['location_code']). "',
				  '" . $this->db->escape($data['transaction_no']) . "',
				  '" . $psku. "',
				  '" . $this->db->escape($data['products'][$i]['name']) . "',
				  '" . $this->db->escape($data['products'][$i]['quantity']) . "',
				  '" . $this->db->escape($data['products'][$i]['price']) . "',
				  '" . $this->db->escape($data['products'][$i]['total']). "',
				  '" . $this->db->escape($data['products'][$i]['purchase_discount']) . "',
				  '" . $this->db->escape($data['products'][$i]['tax_price']). "',
				  '" . $this->db->escape($data['products'][$i]['net_price']) . "',
				  '" . $data['round_off'] . "',
				  '" . $total . "',
				  '" . $this->db->escape($data['remarks']) . "',
				  '" . $this->db->escape($data['products'][$i]['sku_cost']) . "',
				  '" . $this->db->escape($data['products'][$i]['sku_avg']) . "',
				  '" . $userName . "',
				  curdate(),
				  '" . $userName . "',
				  curdate(),
				  '" . $discount_percentage."',
				  '" . $discount_price."',
				  '" . $tax_class_id."',
				  '" . $data['conversion_rate']."',
				  '" . $this->db->escape($data['products'][$i]['product_id'])."',
				  '" . $this->db->escape($data['products'][$i]['description'])."','".$tagged."')";

		   		$res = $this->db->queryNew($sqlsales);                 
               $salesinvoiceid = $this->db->getLastId();
			   if(!$res){
			   		$delSql = "DELETE " . DB_PREFIX . "sales_detail where invoice_no='".$data['transaction_no']."'";
					$this->db->query($delSql);
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);
					exit;
			   }
		}
		$data['invoice_no']    = $data['transaction_no'];
		$data['customer_code'] = $data['vendor'];
		// $this->insertDeliveryDetails($data);   26-01-2021
		$purchaseId = $this->db->getLastId();
		return $data['invoice_no'];		
	}

	public function insertDeliveryDetails($data){
		
		$shippingDetails = $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where id='".$data['shipping_id']."' ")->row;
		$custZipcode = mb_substr($shippingDetails['zip'],0,2);
		$salesmanid  = $this->db->query("SELECT B.id as salesmanid FROM ".DB_PREFIX."route_ziprange A, ".DB_PREFIX."salesman B where A.zip_code='".$custZipcode."' AND A.route_id=B.route_id")->row['salesmanid'];
		
		$data['deliveryman_id'] = $salesmanid;
		if($data['deliveryman_id']){
			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set delivery_man_id='".$salesmanid."' , delivery_status='Pending_Delivery' where invoice_no='".$data['invoice_no']."' ");
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set delivery_status='Pending_Delivery' where invoice_no='".$data['sales_trans_no']."' ");
			$this->assingnToDriver($data);
		}
	}
	public function addBulkSalesInvoice($data){

		$old_invoice  = $this->getSalesInvoice($data['invoice_no']);
		if(!empty($old_invoice)){
			$transNumber = $this->getSalesInvoiceAutoId();
			$transNumber = $transNumber + 1;
			 $data['invoice_no'] = $this->config->get('config_sales_invoice_prefix').date('ym').str_pad($transNumber, 4, '0', STR_PAD_LEFT);
		}

		$payment_status  = 'Pending';
		$paid_amount     = '0';
		$allowedNetworks = array('1','2','3','4','5');
		if($this->config->get('network_default_payment_method') == '1'){
			if(in_array($data['network_id'], $allowedNetworks)){
				$allowPay = true;
				if(($data['network_id'] == '1' || $data['network_id'] =='3')){
					$allowPay = false;
					if($data['remarks'] !=''){
						$p_status = explode('-', $data['remarks']);
						if($p_status[0] == 'Paid'){
							$allowPay = true;	
						}
					}
				}
				if($allowPay){
					$res = $this->addDirectPayment($data);
					if($res =='1'){
						$payment_status = 'Paid';
						$paid_amount    = $data['net_total'];
					}
				}
			}
		}

		$companyId	= $this->session->data['company_id'];
		$userId	   = $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$data['header_remarks']    = date('Y-m-d');
		$customerTermDays 		   = $this->getCustomersTermDays($data['customer_code']);
		$data['reference_to_date'] = date('Y-m-d', strtotime('+'.$customerTermDays.' day'));
		$terminal_code= '';
	    $sqlheader = "INSERT INTO " . DB_PREFIX . "sales_invoice_header (location_code,invoice_no,invoice_date,customer_code,sub_total,discount,gst,actual_total,round_off,net_total,terminal_code,shift_no,detail_lines,header_remarks,createdby,modifiedby,discount_type,reference_no,sales_trans_no,tax_class_id,tax_type,bill_discount_percentage,bill_discount_price,handling_fee,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,term_id,shipping_id,network_id,network_order_id,remarks,delivery_remarks,discount_remarks,delivery_to_date,istagged,payment_status,paid_amount) VALUES('".$this->db->escape($data['location_code'])."','".$this->db->escape($data['invoice_no'])."', curdate(), '".$this->db->escape($data['customer_code'])."','".$this->db->escape($data['sub_total'])."', '".$this->db->escape($data['discount'])."', '".$this->db->escape($data['gst'])."', '".$this->db->escape($data['sub_total'])."', '0.00', '".$this->db->escape($data['net_total'])."',  '" . $terminal_code . "', '0', '0', '" . $this->db->escape($data['header_remarks']) . "', '" . $userName . "', '" . $userName . "', 'Sales', '".$this->db->escape($data['network_order_id'])."', '".$this->db->escape($data['sales_trans_no'])."', '".$this->db->escape($data['tax_class_id'])."', '".$this->db->escape($data['tax_type'])."', '".$this->db->escape($data['bill_discount_percentage'])."', '".$this->db->escape($data['bill_discount_price'])."','".$this->db->escape($data['handling_fee'])."','".$this->db->escape($data['currency_code'])."', '".$this->db->escape($data['conversion_rate'])."', '".$this->db->escape($data['fc_subtotal'])."', '".$this->db->escape($data['fc_tax'])."', '".$this->db->escape($data['fc_discount'])."','".$this->db->escape($data['fc_handling_fee'])."', '".$this->db->escape($data['fc_nettotal'])."', '".$this->db->escape($data['term_id'])."', '".$this->db->escape($data['shipping_id'])."', '".$this->db->escape($data['network_id'])."', '".$this->db->escape($data['network_order_id'])."','".$this->db->escape($data['remarks'])."','".$this->db->escape($data['delivery_remarks'])."','".$this->db->escape($data['delivery_remarks'])."','".$this->db->escape($data['reference_to_date'])."','".$this->db->escape($data['istagged'])."','".$payment_status."','".$paid_amount."')";
	    	// echo $sqlheader; die;

		   $res = $this->db->queryNew($sqlheader);
        	$data['invoice_id'] = $this->db->getLastId();
		   if(!$res){
			   	header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales&token=' . $this->session->data['token']);	exit;
		   }
		   $updSql = "UPDATE ".DB_PREFIX."sales_header set isinvoice='1' where invoice_no='".$data['sales_trans_no']."' ";
		   $this->db->query($updSql);

		$m = 0;
		foreach ($data['products'] as $product) {

			$location_code 	= $this->session->data['location_code'];
			$psku 			= $this->getSkuByProductId($product['product_id']);
			$avgCostupdate 	= "UPDATE ".DB_PREFIX."product_stock SET modifiedon=curdate(),modifiedby='".$userName."' WHERE product_id ='".$product['product_id']."' AND location_Code='".$location_code."'";
			$this->db->query($avgCostupdate);
			$product['actual_total']   = $product['actual_total'] 	=='' ? 0 : $product['actual_total'];
			$product['round_off']      = $product['round_off'] 		=='' ? 0 : $product['round_off'];
			$product['sku_cost']       = $product['sku_cost'] 		=='' ? 0 : $product['sku_cost'];
			$product['conversion_rate']= $product['conversion_rate']=='' ? 0 : $product['conversion_rate'];
			$product['sku_avg'] 	   = $product['sku_avg']=='' ? 0 : $product['sku_avg'];

			$childAry['product_id'] = $product['product_id'];
			$childAry['order_no']   = $data['invoice_no'];
			$childAry['type']   		= 'SI';
			$this->cart->insertChildItems($childAry);

			$tax_class_id = $product['tax_class_id'];
		    $sqlsales = "INSERT INTO " . DB_PREFIX . "sales_invoice_details (location_code,invoice_no,sku,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id,conversion_rate,product_id,cust_description,tagged) VALUES('".$data['location_code']."','".$data['invoice_no']."','".$psku."','".$this->db->escape($product['description'])."','".$this->db->escape($product['qty'])."','".$this->db->escape($product['sku_price'])."','".$this->db->escape($product['sub_total'])."','".$this->db->escape($product['discount'])."','".$this->db->escape($product['gst'])."','".$this->db->escape($product['actual_total'])."','".$this->db->escape($product['round_off'])."','".$this->db->escape($product['net_total'])."','".$this->db->escape($product['remarks'])."','".$this->db->escape($product['sku_cost'])."','".$this->db->escape($product['sku_avg'])."','".$userName."',curdate(),'".$userName."',curdate(),'".$this->db->escape($product['discount_percentage'])."','".$this->db->escape($product['discount_price'])."','".$this->db->escape($product['tax_class_id'])."','".$data['conversion_rate']."','".$this->db->escape($product['product_id'])."','".$this->db->escape($product['cust_description'])."','".$this->db->escape($product['tagged'])."')";
		    	// echo $sqlsales; die;

				$res			= $this->db->queryNew($sqlsales);
				$salesinvoiceid = $this->db->getLastId();
				$data['products'][$m]['quantity'] = $product['qty'];
				$m++;
		   if(!$res){
		   		$delSql = "DELETE ".DB_PREFIX."sales_detail where invoice_no='".$data['transaction_no']."'";
				$this->db->query($delSql);
				header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);
				exit;
		   }
		}
		// $this->insertDeliveryDetails($data); 26-01-2021
		return array('order_no' => $data['sales_trans_no'], 'invoice_no' => $data['invoice_no']);
	}

	public function checkProductTaggedOrNot($invoice_no, $product_id){
		$tagg = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where invoice_no='".$invoice_no."' AND product_id ='".$product_id."' ")->row;
		return $tagg['tagged']; 
	}

	public function getSalesInvoiceForDo($invoice_no,$from='')
	{	
		if($from == 'Sales_Invoice'){
			return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode WHERE sh.invoice_no='".$invoice_no."'")->row;
		}
		if($from == 'Sales_Service'){
			return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."service_sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode WHERE sh.invoice_no='".$invoice_no."'")->row;
		}
		if($from == 'Sales_Return'){
			return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode WHERE sh.invoice_no='".$invoice_no."'")->row;
		}
		if($from == 'Purchase_Service'){
			return $this->db->query("SELECT sh.transaction_no as invoice_no,sh.location_code,sh.transaction_date as invoice_date,sh.tax_class_id,sh.tax_type,v.vendor_name as name FROM ".DB_PREFIX."service_purchase_header as sh LEFT JOIN ".DB_PREFIX."vendor as v ON sh.vendor_id=v.vendor_id WHERE sh.transaction_no='".$invoice_no."'")->row;
		}
	}

	public function getSalesInvoice($invoice_no)
	{	
		return $this->db->query("SELECT sh.*,cu.name FROM ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode WHERE sh.invoice_no='".$invoice_no."'")->row;
	}
	
    public function getShippingAddress($shippingId)
	{
		return $this->db->query("SELECT  * FROM ".DB_PREFIX."shipping WHERE id = '" .$shippingId. "'")->row;
	}

	public function getSalesmanList()
	{
		return $this->db->query("SELECT  * FROM ".DB_PREFIX."salesman ")->rows;
	}
	
    public function getSalesInvoiceDetails($purchaseId)
	{
		return $this->db->query("SELECT SID.*,P.sku as sku,P.name as description from ".DB_PREFIX."sales_invoice_details as SID
				LEFT join ".DB_PREFIX."product as P on SID.product_id=P.product_id WHERE SID.invoice_no = '" .$purchaseId . "'")->rows;
	}
	public function getSalesInvoiceDetailsforDO($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_invoice_details WHERE invoice_no = '" .$purchaseId . "' and qty!=do_qty and cancel='0' ");
		return $query->rows;
	}
	public function getOrderSignature($id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_do_header WHERE sales_order_id='".$id."' ");
		return $query->row['signature'];
	}
	public function editSalesInvoice($purchaseId, $data){

		$companyId	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$gst = '0';
		   $data['discount']='0.00';
           foreach ($data['totals'] as $totals) {
				if($totals['code']=='sub_total'){
					$sub_total = $totals['value'];
				}else if($totals['code']=='tax'){
					$gst = $totals['value'];
				}else if($totals['code']=='discount'){
					$discount = $totals['value'];
				}else if($totals['code']=='total'){
					$total = $totals['value'];
				}else if($totals['code']=='handling'){
					$handling_fee = $totals['value'];
				}
		   }
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0.00';
		}
		if(empty($data['bill_discount_percentage'])) {
			$data['bill_discount_percentage'] = '0';
		}		
        $transaction_date 	      = changeDates($data['transaction_date']);
        $data['header_remarks']   = changeDates($data['reference_date']);
        $data['delivery_to_date'] = changeDates($data['delivery_to_date']);
		$sales_invoice_header     = $this->getSalesInvoiceByinvoice($data['transaction_no']);
		$data['conversion_rate']  = $sales_invoice_header['conversion_rate'];
		
		$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
		$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal'] 	  = $data['conversion_rate'] * $total;
		   
		$sqlheader="UPDATE " . DB_PREFIX . "sales_invoice_header SET
				location_code 			= '".$data['location_code']."'
				, invoice_date 			= '".$transaction_date."'
				, customer_code 		= '".$data['vendor']."'
				, tax_class_id 		    = '".$data['tax_class_id']."'
				, tax_type     		    = '".$data['tax_type']."'
				, sub_total 			= '".$data['fc_subtotal']."'
				, discount 				= '".$data['fc_discount']."'
				, gst 					= '".$data['fc_tax']."'
				, actual_total 			= '".$sub_total."'
				, round_off 			= '0.00'
				, net_total 			= '".$data['fc_nettotal']."'
				, bill_discount_percentage 	= '".$data['bill_discount_percentage']."'
				, bill_discount_price 		= '".$data['bill_discount_price']."'
				, handling_fee 		    = '".$data['fc_handling_fee']."'
				, discount_type 		= 'Sales'
				, reference_no 			= '".$data['reference_no']."'
				, header_remarks 		= '".$data['header_remarks']."'
				, modifiedby 			= '".$userName."'
				, modifiedon 			= curdate()
				, fc_subtotal 			= '".$sub_total."'
				, fc_tax 				= '".$gst."'
				, fc_discount 			= '".$discount."'
				, fc_handling_fee 		= '".$handling_fee."'
				, fc_nettotal 			= '".$total."'
				, term_id 				= '".$data['term_id']."'
				, shipping_id 			= '".$data['shipping_id']."'
				, status 			    = '".$data['status']."'
				, remarks	    		= '".$data['remarks']."'
				, delivery_remarks	    = '".$data['delivery_remarks']."'
				, delivery_to_date	    = '".$data['delivery_to_date']."'
				WHERE invoice_no = '" . $data['transaction_no']. "'";
			    $this->db->query($sqlheader); 

		for ($i=0; $i <count($data['products']) ; $i++) {
    	      $data['round_off']='0.00';
    	      if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		      }
			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}

			$location_code= $this->session->data['location_code'];
			$psku 		  = $this->getSkuByProductId($data['products'][$i]['product_id']);
			$tax_class_id = $data['products'][$i]['tax_class_id'];
    	    $total 		  = $data['products'][$i]['total'] + $data['products'][$i]['tax_price'];
    	    $existProduct = $this->checkSalesInvoiceDetails($data['transaction_no'],$psku);
    	    
    	    if(!empty($existProduct)){
				$this->updateProductQty($data['products'][$i]['product_id'],$existProduct['qty'],$data['products'][$i]['quantity']);
    	    	$sql = "UPDATE ".DB_PREFIX."sales_invoice_details set qty='".$data['products'][$i]['quantity']."', sku_price='".$data['products'][$i]['price']."', sub_total='".$data['products'][$i]['total']."', gst='".$data['products'][$i]['tax_price']."', actual_total='".$data['products'][$i]['net_price']."', net_total='".$total."', remarks='".$data['products'][$i]['remarks']."', sku_cost='".$data['products'][$i]['sku_cost']."', discount_percentage='".$discount_percentage."', discount_price='".$discount_price."', tax_class_id='".$tax_class_id."' where invoice_no='".$data['transaction_no']."' AND sku='".$psku."' "; 
            	$res = $this->db->query($sql);                 
    	    	$salesinvoiceid = $existProduct['id'];
    	    }else{

			    $sql = "INSERT INTO " . DB_PREFIX . "sales_invoice_details (location_code,invoice_no,sku,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id,conversion_rate) VALUES('" .$data['location_code']. "', '" . $data['transaction_no'] . "', '" . $psku. "', '" . $data['products'][$i]['name'] . "', '" . $data['products'][$i]['quantity'] . "', '" . $data['products'][$i]['price'] . "', '" . $data['products'][$i]['total']. "', '" . $data['products'][$i]['purchase_discount'] . "', '" . $data['products'][$i]['tax_price']. "', '" . $data['products'][$i]['net_price'] . "', '" . $data['round_off'] . "', '" .  $total . "', '" . $data['remarks'] . "', '" . $data['products'][$i]['sku_cost'] . "', '" . $data['products'][$i]['sku_avg'] . "', '" . $userName . "', curdate(), '" . $userName . "', curdate(), '" . $discount_percentage."', '" . $discount_price."', '" . $tax_class_id."', '" .$data['conversion_rate']."' )";
            	$res = $this->db->queryNew($sql);                 
            	$salesinvoiceid = $this->db->getLastId();
    	    }
			if(!$res){
			   	$this->db->query("DELETE " . DB_PREFIX . "sales_invoice_details where invoice_no='".$data['transaction_no']."'");
				header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales_invoice/insert&token=' . $this->session->data['token'].'&purchase_id='.$data['transaction_no']);
				exit;
			}
			$getHeaderId = $this->getSalesInvoice($data['transaction_no']);
			/*$this->insertDeliveryDetails($getHeaderId['id'],$data['transaction_no'],$data['vendor'],$salesinvoiceid);*/
			$insertedItems .= '"'.$psku.'",';
		}
		$insertedItems = substr($insertedItems, 0, -1);
		$this->db->query("DELETE FROM ".DB_PREFIX."sales_invoice_details where invoice_no='".$data['transaction_no']."' AND sku NOT IN (".$insertedItems.")");
		return $purchaseId;
	}
	public function updateProductQty($product_id,$oldQty,$newQty){
		if($oldQty > $newQty){
			$qty = $oldQty - $newQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'".$qty."' WHERE product_id = '".$product_id."' ");
		}else{
			$qty = $newQty - $oldQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'".$qty."' WHERE product_id = '".$product_id."' ");
		}
	}
	public function checkSalesInvoiceDetails($invoice_no,$sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_details where invoice_no = '".$invoice_no."' AND sku='".$sku."' ")->row;
	}
	public function getProductIdby_sku($sku){
		$product = $this->db->query("SELECT * FROM ".DB_PREFIX."product where sku='".$sku."' ")->row;
		return $product['product_id'];
	}
	public function getcompanyDetails(){
		return $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$this->session->data['company_id']."' ")->row;	
	}	
	public function checkCompanyCurrency($data){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."' and currency_code='".$data['currency']."' ");
		return $query->row;	
	}
	public function getVendorCurrency($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers where customercode = '" .$vendor_id. "'");
		return $query->row['currency_code'];
	}

	public function getSalesTransNo($salesTransNo) {
		$sqlheader="SELECT status FROM " . DB_PREFIX . "sales_invoice_header where sales_trans_no = '".$salesTransNo."'";
		$query = $this->db->query($sqlheader)->row; 
		if($query['status'] == 'Canceled'){
			$status = 'Canceled';
		} else {
			$status = '';
		}
		return $status;
	}
	public function cancelSalesOrder($invoice_no,$reason) {

		$this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status = 'Canceled', payment_status='Canceled' ,cancel_reason = '".$reason."' WHERE invoice_no = '".$invoice_no."'");

		$this->db->query("UPDATE ".DB_PREFIX."sales_detail SET tagged='0', purchase_id='', modifiedon='".date('Y-m-d')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$invoice_no."' ");
		
		$this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set status=0 where order_no='".$invoice_no."' ");
	}
	public function assingnToDriver($data) {

		$userName		= $this->session->data['username'];
		$insert_header 	= $this->db->query("INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,froms,customer_id,shipping_id,deliveryman_id,status,assignedby,assignedon,created_by) VALUES('".$data['invoice_id']."','".$data['invoice_no']."','".$data['do_no']."','Sales_Invoice','".$data['customer_code']."','".$data['shipping_id']."','".$data['deliveryman_id']."','Pending_Delivery','".$this->session->data['user_id']."','".date('Y-m-d')."','".$userName."')");
        $salesDoId 	= $this->db->getLastId();
        $this->db->query("UPDATE ".DB_PREFIX."sales_do_header SET sort_id='".$salesDoId."' where id='".$salesDoId."' ");
        $this->sendDoToDriver($data['deliveryman_id'],$data['invoice_no']); //send notification

        foreach ($data['products'] as $value) {
			$insert_details = $this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,product_id,qty) VALUES('".$salesDoId."','".$data['do_no']."','".$data['invoice_no']."','".$value['sku']."','".$value['product_id']."','".$value['quantity']."') ");
			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set do_qty=do_qty+'".$value['quantity']."' where invoice_no='".$data['invoice_no']."' AND sku='".$value['sku']."'");

			$this->db->query("UPDATE ".DB_PREFIX."product_stock set sku_qty = sku_qty - '".$value['quantity']."' where product_id='".$value['product_id']."' AND location_Code='".$value['location_code']."' ");
        }
		return $insert_details;
	}
	public function add_do($do,$product_id) {  /*|^|*/

		$invoiceHeader 		 = $this->getSalesInvoice($do['sales_transaction_no']);
		$userName	   		 = $this->session->data['username'];
		$delivery_status 	 = 'Pending_Delivery';
		$do['delivery_date'] = changeDates($do['delivery_date']);
		
		if($do['delivery_date'] == date('Y-m-d')){
			$delivery_status 	= 'On_Delivery';
		}else if($do['delivery_date'] > date('Y-m-d')){
			$delivery_status 	= 'Scheduling';
		}
		$invoice_status = 'Scheduling';

		if($do['total_invoice_items'] == count($product_id)){
			$count = 0;
			foreach ($product_id as $value) {
				$qty 	=  $do['quantity'][$value] + $do['do_quantity'][$value];
				if($qty == $do['real_quantity'][$value]){
					$invoice_status = 'Scheduling';
				} else {
					$count ++;
				}
			}
			$invoice_status = $count != 0 ? 'Partial_Scheduling' : 'Scheduling';
		}else{
			$invoice_status = 'Partial_Scheduling';
		}

		$insert_header = $this->db->query("INSERT INTO ".DB_PREFIX."sales_do_header (sales_invoice_id,sales_transaction_no,do_no,customer_id,shipping_id,deliveryman_id,status,created_by,assignedon,froms,location_code) VALUES('".$do['sales_invoice_id']."','".$do['sales_transaction_no']."','".$do['do_no']."','".$do['customer_id']."','".$do['shipping_id']."','".$do['deliveryman_id']."','".$delivery_status."','".$userName."','".$do['delivery_date']."','Sales_Invoice','".$invoiceHeader['location_code']."') ");

        $salesDoId = $this->db->getLastId();
        $this->db->query("UPDATE ".DB_PREFIX."sales_do_header SET sort_id=id where id='".$salesDoId."'");
        
        $err = '';
        $m   = $n = 1;
        foreach ($product_id as $key => $value) {
			$prodDetails = $this->db->query("SELECT si.sku,si.do_qty,si.qty,si.product_id,ps.sku_qty as avl_qty,si.location_code,si.description,si.sku_price as price FROM ".DB_PREFIX."sales_invoice_details as si LEFT JOIN ".DB_PREFIX."product_stock as ps ON si.product_id=ps.product_id AND si.location_code=ps.location_Code where si.product_id='".$value."' AND si.invoice_no='".$invoiceHeader['invoice_no']."' ")->row;
			$product = $this->getproductAlldetails($value);

			$n ++;
			$errs = '';
			$qty  = $do['quantity'][$value];
			if($qty > 0 && $product['package']=='1'){
				$childStk = $this->cart->checkChildItemsStock($value,$qty,$invoiceHeader['location_code']);
				if(!$childStk['status']){
					$errs  = $childStk['err'];
				}
				$prodDetails['avl_qty'] = $qty; // for avoid parant sku qty validation
			}

			$orderHeader = $this->getSalesnew($invoiceHeader['sales_trans_no']);
			if($prodDetails['avl_qty'] >= $qty && $errs==''){
				if($invoiceHeader['istagged']){
					if($invoice_status == 'Scheduling'){
						$updatAry['invoice_no']    = $invoiceHeader['invoice_no'];
						$updatAry['product_id']    = $value;
						$this->cart->updatetaggedSalesOrder($updatAry);
					}
					$check = true;
				}else{
					$prodDetails['needed_qty'] = $qty;
					$check = $this->cart->checkTaggedProduct($prodDetails, $orderHeader['invoice_date']);
				}

				if($check){
					$m++;
					$insert_details = $this->db->query("INSERT INTO ".DB_PREFIX."sales_do_details (do_id,do_no,invoice_no,sku,qty,product_id,price) VALUES('".$salesDoId."','".$do['do_no']."','".$do['sales_transaction_no']."','".$prodDetails['sku']."','".$qty."','".$value."','".$prodDetails['price']."') ");
					
					$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set do_qty=do_qty+'".$qty."', tagged=0, purchase_id='' where invoice_no='".$do['sales_transaction_no']."' AND product_id='".$prodDetails['product_id']."'");

					$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged=0, purchase_id='' where invoice_no IN (SELECT sales_trans_no from ".DB_PREFIX."sales_invoice_header where invoice_no='".$do['sales_transaction_no']."' ) AND product_id='".$prodDetails['product_id']."'");
					
					if($product['package']=='0'){
					}
					$this->db->query("UPDATE ".DB_PREFIX."product_stock set sku_qty = sku_qty - '".$qty."' where product_id='".$prodDetails['product_id']."' AND location_Code='".$invoiceHeader['location_code']."' ");
					
                    $upCh['product_id']   = $value;
					$upCh['qty'] 		    = $qty;
					$upCh['location_code']= $invoiceHeader['location_code'];
					$this->cart->updatechildProductQty($upCh,'S');
				}else{
					$prodDetails['location_code'] = $invoiceHeader['location_code'];
					$prodDetails['invoice_date']  = $orderHeader['invoice_date'];
					$taggedOrders = $this->getTaggedOrdersString($prodDetails);
					$taggedorderAndCust = '';
					foreach($taggedOrders as $order){
						$cust = $this->getCustomerDetails($this->getSalesnew($order['order_no'])['customer_code']);
						$taggedorderAndCust .= $order['order_no'].'- '.$cust['name'].', ';
					}
					$err .= '<span style="color:red;">'.$invoiceHeader['invoice_no'].' - '.$prodDetails['description'].' - Has tagged to ('.$taggedorderAndCust.') '.'</span><br>';
				}
			}else{
				if($errs==''){
					$err .= '<span style="color:red;">'.$invoiceHeader['invoice_no'].' - '.$prodDetails['description'].' - No stock! </span><br>';
				}else{
					$err .= '<span style="color:red;">'.$invoiceHeader['invoice_no'].' - '.$errs.'</span><br>';
				}
			}
      }
      $this->cart->updateOrderandInvoice($invoiceHeader['invoice_no']);

        if($m > 1){

	        $this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header SET delivery_status='".$invoice_status."' where invoice_no='".$do['sales_transaction_no']."' ");
	        $this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status='".$invoice_status."' where invoice_no='".$invoiceHeader['sales_trans_no']."' ");
	        $this->sendDoToDriver($do['deliveryman_id'],$do['sales_transaction_no']); //send notification
        }else{
        	$this->db->query("DELETE FROM ".DB_PREFIX."sales_do_header where id='".$salesDoId."'");
        }
       	return $err;
	}
	public function getTaggedOrdersString($data){
		$sql = "SELECT order_no FROM ".DB_PREFIX."tagged_sales_orders WHERE order_no IN (SELECT sh.invoice_no FROM ".DB_PREFIX."sales_detail as sd LEFT JOIN ".DB_PREFIX."sales_header as sh ON sd.invoice_no=sh.invoice_no WHERE sh.location_code='".$data['location_code']."' AND sd.product_id='".$data['product_id']."' AND sh.delivery_status='Pending' AND sh.invoice_date <= '".$data['invoice_date']."' AND sd.tagged='1' GROUP BY sh.invoice_no) AND status='1' GROUP BY order_no";
		return $this->db->query($sql)->rows;
	}
	public function getSalesDoDetails($purchaseId)
	{
		$query = "SELECT soh.do_no,cust.name as cust_name,prod.name as prod_name,delivery.name as delivery_man_name,sod.sku,sod.qty,soh.deliveryman_id,soh.id as id FROM " . DB_PREFIX . "sales_do_details as sod 
			LEFT JOIN " . DB_PREFIX . "sales_do_header as soh on soh.id = sod.do_id 
			LEFT JOIN " . DB_PREFIX . "customers as cust on cust.customercode = soh.customer_id 
			LEFT JOIN " . DB_PREFIX . "product as prod on prod.sku = sod.sku
			LEFT JOIN " . DB_PREFIX . "salesman as delivery on delivery.id = soh.deliveryman_id
			WHERE soh.sales_transaction_no = '" .$purchaseId . "'";
		// echo $query; die;
		return $this->db->query($query)->rows;
	}
	public function getSalesDoDetail($purchaseId){ //For pdf download
		$query = "SELECT sod.*,prod.name as prod_name FROM ".DB_PREFIX."sales_do_details as sod
			LEFT JOIN ".DB_PREFIX."product as prod on prod.sku = sod.sku WHERE sod.do_id = '".$purchaseId."' ";
		return $this->db->query($query)->rows;		
	}
	public function getSalesDoHeader($purchaseId)
	{
		$query = $this->db->query("SELECT soh.do_no,delivery.name as delivery_man_name,soh.deliveryman_id as deliveryman_id,soh.id,soh.status,soh.signature,soh.sales_transaction_no as invoice_no,soh.assignedon FROM " . DB_PREFIX . "sales_do_header as soh LEFT JOIN " . DB_PREFIX . "salesman as delivery on delivery.id = soh.deliveryman_id WHERE soh.sales_transaction_no = '" .$purchaseId . "'");
		return $query->rows;
	}
	public function getSalesDoHeaderById($id)
	{
		return $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_do_header WHERE id ='".$id."'")->row;
	}
	public function getSalesDoAutoId() 
	{
		$query = $this->db->query("SELECT id  FROM " . DB_PREFIX . "sales_do_header ORDER BY id DESC LIMIT 1");
		return $query->row['id'];
	}
	public function doStatusUpdate($product_id,$status)
	{
		$query = $this->db->query("UPDATE " . DB_PREFIX . "sales_do_header SET status = '".$status."' WHERE id = '".$product_id."'");
		return $query;
	}
	public function cancelOrder($invoice_no,$reason) {

		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header SET delivery_status = 'Canceled',payment_status='Canceled', cancel_reason = '".$reason."' WHERE invoice_no = '".$invoice_no."'");
		$this->db->query("UPDATE ".DB_PREFIX."sales_do_header SET status = 'Canceled',payment_status='Canceled',reason = '".$reason."' WHERE sales_transaction_no ='".$invoice_no."'");
		$this->db->query("UPDATE ".DB_PREFIX."order_payment SET payment_status = 'Canceled',stripe_response = '".$reason."' WHERE order_id ='".$invoice_no."'");
		
		$sales_trans_no = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header WHERE invoice_no = '".$invoice_no."'")->row['sales_trans_no'];
		$this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status = 'Pending', payment_status='Pending',isinvoice = '0' WHERE invoice_no ='".$sales_trans_no."'");
		
		$device = $this->db->query("SELECT sm.device_token FROM ".DB_PREFIX."sales_do_header as sd LEFT JOIN ".DB_PREFIX."salesman as sm ON sd.deliveryman_id=sm.id where sd.sales_transaction_no='".$invoice_no."' AND sm.device_token!='' GROUP BY sd.deliveryman_id")->rows;
		$devices = array();
		foreach ($device as $value) {
			$devices[] = $value['device_token'];
		}
		$message ="Sales Invoice ".$invoice_no." Canceled By admin due to ".$reason;
		$this->doSendNotificationAndroid($message,$devices,$invoice_no);
	}
	public function sendDoToDriver($id,$invoice_no){
		$devices[] =  $this->db->query("SELECT * FROM ".DB_PREFIX."salesman WHERE id ='".$id."'")->row['device_token'];
		if(!empty($devices)){
			$this->doSendNotificationAndroid('You have a new Order',$devices,$invoice_no,'new');
		}
	}
	function doSendNotificationAndroid($message,$deviceToken,$invoice_no,$type='',$push_type='',$unreadcnt='',$msgcnt='')
	{
		$apiKey = ANDROID_APIKEY;
	    //$registrationIDs[] = $deviceToken;
	    $url = 'https://fcm.googleapis.com/fcm/send';
	    $title = ($type =='new') ? '':'cancel';
	    $fields = array(
		    'registration_ids' => $deviceToken,
		    'notification'     => array("title" => 'Megafurniture Order '.$title.' Notification',"body" => $message),
	    );
	    $headers = array(
		    'Authorization: key=' . $apiKey,
		    'Content-Type: application/json'
	    );
	    // Open connection
	    $ch = curl_init();
	    // Set the url, number of POST vars, POST data
	    curl_setopt( $ch, CURLOPT_URL, $url );
	    curl_setopt( $ch, CURLOPT_POST, true );
	    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
	    // Execute post
	    $result = curl_exec($ch);
	    // Close connection
	    curl_close($ch);
	}
	public function getTermById($term_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."terms where terms_id='".$term_id."' ")->row;
	}
	public function getTotalPaidAmount($invoice_no){
		return $this->db->query("SELECT sum(amount) as paid_total FROM ".DB_PREFIX."order_payment where order_id='".$invoice_no."' AND (payment_status='Paid' or payment_status='Hold') ")->row['paid_total'];
	}
	public function getNetworks(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks WHERE status = 'Active'")->rows;
	}
	public function getNetworkById($id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks WHERE id = '".$id."'")->row;
	}
	public function getProduct($product_id,$location_code='') {
		$sql = "SELECT p.*,pd.sku_price as price,pd.sku_qty as quantity,pd.sku_avg as avg_cost,pd.sku_cost FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
		$sql .= " WHERE p.product_id = '" . (int)$product_id . "'";
		if($location_code!=''){
			$sql .= " AND pd.location_code = '" . (int)$location_code . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getPaymentTypes(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."payment_type_master where status='1'")->rows;
	}
	public function getNetworkName($network_id){
		if($network_id){
			return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where id='".$network_id."'")->row['name'];
		}else{
			return 'Show Room';
		}
	}
	public function getUnpaidOrders($data){
		$sql = "SELECT * FROM ".DB_PREFIX."sales_invoice_header where payment_status IN ('Pending','Partial') ";
		if($data['filter_customer'] !=''){
			$sql .=" AND customer_code='".$data['filter_customer']."' ";
		}
		if($data['filter_invoice_date'] !=''){
			$data['filter_invoice_date'] = changeDates($data['filter_invoice_date']);
			$sql .=" AND invoice_date='".$data['filter_invoice_date']."' ";
		}
		if($data['filter_network'] !=''){
			if($data['filter_network'] == 'HQ' || $data['filter_network'] == 'TS'){
				$sql .=" AND location_code='".$data['filter_network']."' ";
			}else{
				$sql .=" AND network_id='".$data['filter_network']."' ";
			}
		}
		// echo $sql;
		return $this->db->query($sql)->rows;
	}
	public function updatePaymentStatus($data){
		$paid_status = 'Pending';
		if($data['is_paid']){
			$paid_status = 'Paid';
		}
		$paymentIds = array();
		if(count($data['selecteds']) > 0){
			foreach ($data['selecteds'] as $key => $value) {
				$amount 	  = $data['payment_amount'][$key];
				$salesHeader  = $this->getSalesInvoiceByinvoice($value);

				$this->db->query("INSERT INTO ".DB_PREFIX."order_payment (order_id,customer_id,payment_type,payment_method,amount,payment_status,payment_date,added_on) VALUES('".$value."','".$salesHeader['customer_code']."','Full','".$data['payment_type']."','".$amount."','".$paid_status."',curdate(),curdate()) ");
				$paymentIds[] = $this->db->getLastId();

				$status = 'Partial';
				if(($salesHeader['paid_amount'] + $amount) >= $salesHeader['net_total']){
					$status = 'Paid';
				}
				$payment_status = 'Pending';
				if($data['is_paid'] && $amount > 0){
					$payment_status = 'Partial';
				}
				if($data['is_paid'] && $status == 'Paid'){
					$payment_status = 'Paid';
				}
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set payment_status='".$payment_status."', paid_amount = paid_amount +'".$amount."'  where invoice_no='".$value."' ");
				$invoiceHeader = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no='".$value."' ")->row;

				if($invoiceHeader['sales_trans_no']){
					$this->db->query("UPDATE ".DB_PREFIX."sales_header set payment_status='".$payment_status."' where invoice_no='".$invoiceHeader['sales_trans_no']."' ");
				}
			}
			return $paymentIds;
		}
	}
	public function updateInvoiceHeaderOnly($data){
		
		$this->db->query("UPDATE ".DB_PREFIX."order_payment set payment_status='".$data['payment_status']."', payment_date='".changeDates($data['payment_date'])."', payment_method='".$data['payment_method']."' where id='".$data['payment_id']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set payment_status='".$data['payment_status']."' where invoice_no='".$data['invoice_no']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set payment_status='".$data['payment_status']."' where invoice_no='".$data['sales_invoice_no']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_do_header set payment_status='".$data['payment_status']."' where sales_transaction_no = '".$data['invoice_no']."' ");
	}
	public function getTotalPaymentCount($data){

		$sql = "SELECT count(*) as total FROM ".DB_PREFIX."order_payment as op LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON op.order_id=sh.invoice_no where op.order_id !='' ";
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			$sql .=" AND date(op.payment_date) between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."' ";
		}
		if($data['filter_transaction'] !=''){
			$sql .=" AND (op.order_id='".$data['filter_transaction']."' OR sh.network_order_id='".$data['filter_transaction']."' )";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$sql .= " AND op.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		}
		if(!empty($data['filter_delivery_status'])){
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		}
		if($data['filter_customer']){
			$sql .= " AND sh.customer_code= '".$this->db->escape($data['filter_customer'])."' ";
		}
		// echo $sql; die;
		return $this->db->query($sql)->row['total'];
	}
	public function getTotalPayment($data){

		$sql = "SELECT op.* FROM ".DB_PREFIX."order_payment as op LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON op.order_id=sh.invoice_no where op.order_id !='' ";
		if($data['filter_from_date'] !='' && $data['filter_to_date'] !=''){
			$data['filter_from_date'] = changeDates($data['filter_from_date']);
			$data['filter_to_date']   = changeDates($data['filter_to_date']);
			$sql .=" AND date(op.payment_date) between '".$data['filter_from_date']."' AND '".$data['filter_to_date']."' ";
		}
		if($data['filter_transaction'] !=''){
			$sql .=" AND (op.order_id='".$data['filter_transaction']."' OR sh.network_order_id='".$data['filter_transaction']."') ";
		}
		if(!empty($data['filter_payment_status'])){
			$data['filter_payment_status'] = implode("','", $data['filter_payment_status']);
			$sql .= " AND op.payment_status IN ('".$data['filter_payment_status']."')";
		}
		if($data['filter_location'] !=''){
			$sql .= " AND sh.location_code ='".$data['filter_location']."'";
		}
		if(!empty($data['filter_network'])){
			$filter_network = implode("','", $data['filter_network']);
			$sql .= " AND sh.network_id IN ('".$filter_network."')";
		}

		if(!empty($data['filter_delivery_status'])){
			$data['filter_delivery_status'] = implode("','", $data['filter_delivery_status']);
			$sql .= " AND sh.delivery_status IN ('".$data['filter_delivery_status']."')";
		}
		if($data['filter_customer']){
			$sql .= " AND sh.customer_code= '".$this->db->escape($data['filter_customer'])."' ";
		}
		$sql .= " ORDER BY op.id DESC ";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		return $this->db->query($sql)->rows;
	}
	public function getPaymentDetailsView($order_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."order_payment where id ='".$order_id."' ")->row;
	}
	public function addAttachment($attachment,$invoice_no)
	{
		$sql = "UPDATE " . DB_PREFIX . "sales_header SET
				attachment = '" . $attachment . "' WHERE invoice_no = '".$invoice_no."' ";
		$this->db->query($sql);
	}
	public function removeattachment($invoice_no)
	{
		$sql = "UPDATE " . DB_PREFIX . "sales_header SET
				attachment = '" . $attachment . "' WHERE invoice_no = '".$invoice_no."' ";
		$this->db->query($sql);
	}
	public function getShippingAddressById($id)
	{
		$query = $this->db->query("SELECT  * FROM ".DB_PREFIX."shipping WHERE id = '".$id."'");
		return $query->row;
	}
	public function getSalesInvoiceDetails_csv($order_id){  // For export invoice csv
		$sql = "SELECT sd.qty,sd.sku_price,sd.description,cu.name as cust_name,sh.delivery_status as delivery_status, sh.payment_status as payment_status,sh.tax_class_id as sh_tax_class, sh.tax_type as tax_type,sh.network_order_id,sh.invoice_no,sh.reference_no,sh.invoice_date,sh.header_remarks,sd.discount as discount,ln.location_name,sh.network_order_id 
		FROM ".DB_PREFIX."sales_invoice_details as sd 
		LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON sd.invoice_no=sh.invoice_no 
		LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode 
		LEFT JOIN ".DB_PREFIX."location as ln ON ln.location_code=sh.location_code 
		where sh.id='".$order_id."'";
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesOrderDetails_csv($invoice_no){  // For export invoice csv
		$sql = "SELECT sd.qty,sd.sku_price,sd.description,cu.name as cust_name,sh.delivery_status as delivery_status, sh.payment_status as payment_status,sh.tax_class_id as sh_tax_class, sh.tax_type as tax_type,sh.network_id,sh.invoice_no,sh.reference_no,sh.invoice_date,sh.header_remarks,sd.discount as discount,ln.location_name,sh.network_order_id, sh.shipping_id FROM ".DB_PREFIX."sales_detail as sd LEFT JOIN ".DB_PREFIX."sales_header as sh ON sd.invoice_no=sh.invoice_no LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode LEFT JOIN ".DB_PREFIX."location as ln ON sd.location_code=ln.location_code where sh.invoice_no='".$invoice_no."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getInvoiceNoById($id){
		return $this->db->query("SELECT invoice_no FROM ".DB_PREFIX."sales_invoice_header where id='".$id."'")->row['invoice_no'];
	}
	public function getSalesMansList(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_person where status='1' ")->rows;
	}
	public function getSalesMansDetails($id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_person where status='1' AND id='".$id."' ")->row;
	}
	public function getCustomerDetails($customercode)
	{
	  $query = $this->db->query("SELECT * FROM ".DB_PREFIX."customers WHERE customercode ='".$customercode."'");
	  return $query->row;
	}
	public function checkInvoiceCanceled($invoice_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where id='".$invoice_id."' ")->row;
	}
	public function AddCopyOrders($data){
		
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$location	= $this->session->data['location_code'];
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}
		}
		$tax_class_id 	   = $data['tax_class_id'] == '0' ? 1 : 2;
		if($tax_class_id  == '2'){
			$tax_type      = $data['tax_class_id'] == '1' ? 0 : 1;
		}
		$shipping_id 	   = $this->getCustomerDefaultShipping($data['customer']);
		$transaction_date  = date('Y-m-d');
		$reference_to_date = date('Y-m-d',strtotime('+1 day'));

		$sql = "INSERT INTO " . DB_PREFIX . "sales_header (location_code,invoice_no,invoice_date,customer_code,currency_code,conversion_rate,tax_class_id,tax_type,gst,sub_total,actual_total,net_total,header_remarks,createdby,company_id,fc_subtotal,fc_nettotal,term_id,shipping_id,reference_to_date,network_id,modifiedby) VALUES('".$location."','".$this->db->escape($data['invoice_no'])."','".$transaction_date."','".$this->db->escape($data['customer'])."','SGD','1','".$tax_class_id."','".$tax_type."','".$gst."','".$sub_total."','".$sub_total."','".$this->db->escape($data['total'])."','".$transaction_date."','".$userName."','".$companyId."','".$sub_total."','".$this->db->escape($data['total'])."','".$this->db->escape($data['term_id'])."','".$shipping_id."','".$reference_to_date."','".$data['network_id']."','".$userName."')";
		
		$this->db->queryNew($sql);
		$sales_id = $this->db->getLastId();
		foreach ($data['products'] as $product) {
			$pqty  = (int)$product['quantity'];
			$price = (float)$product['price'];

			$sqls = "INSERT INTO " . DB_PREFIX . "sales_detail (location_code,invoice_no,sku,product_id,description,qty,sku_price,sub_total,gst,net_total,createdon,conversion_rate,cust_description) VALUES('".$location."','".$data['invoice_no']."','".$this->db->escape($product['sku'])."','".$this->db->escape($product['product_id']) ."','".$this->db->escape($product['name'])."','".$this->db->escape($product['quantity'])."','".$this->db->escape($product['price'])."','".$this->db->escape($product['sub_total'])."','".$data['purchase_tax']."','".$this->db->escape($product['total'])."',curdate(),'1','".$this->db->escape($product['description'])."')";

			$res = $this->db->queryNew($sqls);
		   if(!$res){
		   		$this->db->query("DELETE ".DB_PREFIX."purchase where purchase_id='".$purchaseId."'"); exit;
		   }
		}
		return $sales_id;
	}
	public function getCustomerDefaultShipping($customer){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id='".$customer."' AND isdefault='1' ")->row['id'];
	}
	public function getTotalSalesService($data){
	    $sql = "SELECT COUNT(*) AS totalSale,sum(net_total) as net_total FROM " . DB_PREFIX . "service_sales_header WHERE invoice_no IS NOT NULL AND delivery_status!='Canceled'";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no =  '" . $data['filter_transactionno'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getSalesServiceList($data){

	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		} 
		$sql = "SELECT DISTINCT * FROM ".DB_PREFIX."service_sales_header WHERE invoice_no IS NOT NULL AND delivery_status!='Canceled' ";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getSalesServiceDetails($invoiceno){
		$sql = "SELECT sd.*,prod.name,prod.sku as sku_name from ".DB_PREFIX."service_sales_detail as sd Left Join ".DB_PREFIX."product as prod on sd.product_id = prod.product_id where invoice_no='".$invoiceno."' order by createdon asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getnetworkIds(){
		return $this->db->query("SELECT id FROM ".DB_PREFIX."networks  WHERE status='Active'")->rows;
	}
	public function customerSearch($str){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where cust_code LIKE '%".$str."%' OR name LIKE '%".$str."%' OR email LIKE '%".$str."%' OR customercode LIKE '%".$str."%' ")->rows;
	}
	public function getTagSalesOrder($order_no){
		return $this->db->query("SELECT ts.*,vn.vendor_code as vendor_name FROM ".DB_PREFIX."tagged_sales_orders as ts LEFT JOIN ".DB_PREFIX."vendor as vn ON ts.vendor=vn.vendor_id where ts.order_no ='".$order_no."' ")->row;
	}
	public function getProductChildItems($product_id){
		return $this->db->query("SELECT cp.*,pd.name,pd.sku as childsku FROM ".DB_PREFIX."child_products as cp LEFT JOIN ".DB_PREFIX."product as pd ON cp.child_sku=pd.product_id where cp.parant_sku='".$product_id."' ")->rows;
	}
	public function getCustomersTermDays($cust_code){
		return $this->db->query("SELECT t.noof_days as nd FROM ".DB_PREFIX."customers as cu LEFT JOIN ".DB_PREFIX."terms as t ON cu.term_id=t.terms_id where cu.customercode='".$cust_code."' ")->row['nd'];
	}
    public function getTaggedPurchaseId($invoice_no){
        return $this->db->query("SELECT * FROM ".DB_PREFIX."tagged_sales_orders where order_no='".$invoice_no."' ")->row['purchase_id'];
    }
    public function getEmailTemplate($id){
    	return $this->db->query("SELECT * FROM ".DB_PREFIX."email_template where status='1' AND id='".$id."' ")->row;
    }
    public function addDirectPayment($data){
	    	$networkDetails = $this->getNetworkById($data['network_id']);
	    	$res = '0';
	    	if($networkDetails['allow_default']=='1'){
				$this->db->query("INSERT INTO ".DB_PREFIX."order_payment (order_id,customer_id,payment_type,payment_method,amount,payment_status,payment_date,added_on) VALUES('".$data['invoice_no']."','".$data['customer_code']."','Full','".$networkDetails['payment_method']."','".$data['net_total']."','Paid',curdate(),curdate()) ");
				$data['payment_id'] = $this->db->getLastId();
				$this->sendReceiptToCustomer($data);
				$this->db->query("UPDATE ".DB_PREFIX."sales_header set payment_status='Paid' where invoice_no='".$data['sales_trans_no']."' ");
	    		$res = '1';
	    	}
		return $res;
    }

	public function sendReceiptToCustomer($data){

		$this->load->model('transaction/sales');
		$payment     = $this->getPaymentDetailsView($data['payment_id']);
		$invoice_no  = $payment['order_id'];

		$salesHeader = $data;
		$customers 	 = $this->getB2BCustomersbyCode($salesHeader['customer_code']);
		$company 	 = $this->getcompanyDetails();
		$emailTemp 	 = $this->getEmailTemplate('2');
		$dynamicTemp = $subject = $trading_name = $contact_name = '';

		if(!empty($emailTemp)){
			$trading_name = $company['name'];
			$link         = $company['web_url'];
			$email        = $company['email'];
			$phone        = $company['phone'];
			$contact_name = $customers['name'];
			$currency_code= $salesHeader['currency_code'] !='' ? $salesHeader['currency_code'] : ''; 
			$amount       = $salesHeader['net_total']; 
			$invoice_date = $salesHeader['header_remarks'] !='' ? date('d M Y',strtotime($salesHeader['header_remarks'])) : '';
			$break        = '<br>';
			$paid_amount  = $salesHeader['net_total'];

			$searchArr    = array('[invoice_number]','[trading_name]','[contact_name]','[currency_code]','[amount]','[invoice_date]','[break]','[link]','[email]','[phone]');
			$replaceArr  = array($invoice_no,$trading_name,$contact_name,$currency_code,$amount,$invoice_date,$break,$link,$email,$phone);
			$subject     = str_replace($searchArr, $replaceArr, $emailTemp['subject']);
			$dynamicTemp = str_replace($searchArr, $replaceArr, $emailTemp['description']);
		}
		$str = "<h5 style='font-size:25px;margin: 0px 25%;'>".$subject."</h5><br>
					<table style='width:100%;font-size:16px;'>
					<tr>
						<td align='center'><b>AMOUNT PAID</b></td>
						<td align='center'><b>DATE PAID</b></td>
						<td align='center'><b>PAYMENT METHOD</b></td>
					</tr>
					<tr>
						<td align='center'>".$payment['amount']."</td>
						<td align='center'>".date('d/m/Y',strtotime($payment['payment_date']))."</td>
						<td align='center'>".$payment['payment_method']."</td>
					</tr>
				</table>";
		$str .= "<br><br><h4 style='margin: 0 10%;font-size: 16px;'>SUMMARY<h4><br>";
		$str .= "<table style='width:100%;font-size:16px;background-color: #f5f9fc;'>
					<tr style=''>
						<td style='padding: 10px 10%;'>Payment for invoice(s)<br>SI32433425</td>
						<td align='left'>".$currency_code." ".$amount."</td>
					</tr>
					<tr style='border-top: 1px solid #e8ecef;'>
						<td style='border-top: 1px solid #e8ecef;padding: 10px 10%;font-size:17px;'><b>Amount Charged<b></td>
						<td align='left' style='border-top: 1px solid #e8ecef;font-size:17px;'><b>".$currency_code." ".$paid_amount."<b></td>
					</tr>
				</table><br><hr><br><p style='margin: 0 10%;'>".$dynamicTemp."</p>";

    	$postArr['Content'] = $str;
    	$postArr['Subject'] = $subject;
    	$postArr['Name'] 	= $customers['name'];
    	$postArr['Email']  	= $customers['email'];
    	$postArr['Type']  	= 'payments';

    	if($customers['email']!='' && $dynamicTemp!='' && SEND_EMAIL_TO_CUSTOMER){
			$this->mail->sendEmail($postArr);
    	}
		return true;
	}
	public function getShippingAddressWithCust($shipping_id){
		if($shipping_id > 0){
			return $this->db->query("SELECT cu.name as customer_name,sh.* FROM ".DB_PREFIX."shipping as sh LEFT JOIN ".DB_PREFIX."customers as cu on cu.customercode=sh.customer_id where id='".$shipping_id."' ")->row;
		}
	}
	public function validateRefNo($data, $from){
		if($from == 'SO'){ 
			$sql = "SELECT count(*) as s FROM ".DB_PREFIX."sales_header where network_order_id ='".$data['ref']."' AND delivery_status!='Canceled' ";
			if($data['sales_id']){
				$sql .= " AND invoice_no !='".$data['sales_id']."' ";
			}
			return $this->db->query($sql)->row['s'];
		}
		else if($from == 'SI'){
			$sql = "SELECT count(*) as s FROM ".DB_PREFIX."sales_invoice_header where network_order_id ='".$data['ref']."' AND delivery_status!='Canceled' ";
			if($data['sales_id']){
				$sql .= " AND invoice_no !='".$data['sales_id']."' ";
			}
			return $this->db->query($sql)->row['s'];
		}
	}
	public function cancelPayment($payment_id){
		$payment = $this->db->query("SELECT * FROM ".DB_PREFIX."order_payment where id='".$payment_id."' ")->row;
		$iHeader = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no ='".$payment['order_id']."' ")->row;

		$balance = $iHeader['paid_amount'] - $payment['amount'];
		$payment_status = 'Pending';
		if($balance >0){
			$payment_status = 'Partial';
		}
		if($balance < 0 || $balance == 0){
			$payment_status = 'Pending';
		}

		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set paid_amount=paid_amount-'".$payment['amount']."' ,payment_status='".$payment_status."' where invoice_no ='".$payment['order_id']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set payment_status='".$payment_status."' where invoice_no ='".$iHeader['sales_trans_no']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."order_payment set payment_status='Canceled' where id ='".$payment_id."' ");
	}
	public function getAgingReport($data){
		$sql = "SELECT sih.*,cu.name as cust_name FROM ".DB_PREFIX."sales_invoice_header as sih LEFT JOIN ".DB_PREFIX."customers as cu ON sih.customer_code=cu.customercode where sih.payment_status IN ('Pending','Partial') ";
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND sih.invoice_date between '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){
			$sql .= " AND sih.location_code = '".$data['filter_location']."' ";
		}
		if($data['filter_invoice_no']){
			$sql .= " AND sih.invoice_no LIKE '%".$data['filter_invoice_no']."%' ";
		}
		$res['total_rows'] = $this->db->query($sql)->rows;
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$res['results'] = $this->db->query($sql)->rows;
		return $res;
	}
	public function getAgingReportForExport($data){
		$sql = "SELECT sih.*,cu.name as cust_name FROM ".DB_PREFIX."sales_invoice_header as sih LEFT JOIN ".DB_PREFIX."customers as cu ON sih.customer_code=cu.customercode where sih.payment_status IN ('Pending','Partial') ";
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND sih.invoice_date between '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){
			$sql .= " AND sih.location_code = '".$data['filter_location']."' ";
		}
		if($data['filter_invoice_no']){
			$sql .= " AND sih.invoice_no LIKE '%".$data['filter_invoice_no']."%' ";
		}
		return $this->db->query($sql)->rows;
	}
	public function cancelPartiallInvoice($data) 
	{	
		$this->updateSOandSIHeader($data);
		$tagged = $this->db->query(" SELECT * FROM ".DB_PREFIX."sales_invoice_details where invoice_no='".$data['invoice_no']."' AND product_id='".$data['product_id']."' AND tagged='1' ")->num_rows;
		if($tagged){
			$this->db->query("INSERT INTO ".DB_PREFIX."untag_history (`source`, `transaction_no`, `product_id`, `location`, `username`) VALUES ( 'SI', '".$data['invoice_no']."', '".$data['product_id']."', '', '".$this->session->data['username']."') ");
		}
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0', purchase_id='', cancel='1', modifiedon='".date('Y-m-d H:i:s')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$data['invoice_no']."' and product_id='".$data['product_id']."' ");

		$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0', purchase_id='', cancel='1', modifiedon='".date('Y-m-d H:i:s')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$data['order_no']."' and product_id='".$data['product_id']."' ");
	}
	public function UpdateSalesInvoice($data){

		$sub_total = $gst = $discount = $total = 0;
		foreach ($data['totals'] as $totals) {
			if($totals['code']=='sub_total'){
				$sub_total = $totals['value'];
			}else if($totals['code']=='tax'){
				$gst = $totals['value'];
			}else if($totals['code']=='discount'){
				$discount = $totals['value'];
			}else if($totals['code']=='total'){
				$net_total = $totals['value'];
			}else if($totals['code']=='handling'){
				$handling_fee = $totals['value'];
			}
		}
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set 
			sub_total 	='".$sub_total * $data['conversion_rate']."', 
			discount 	='".$discount * $data['conversion_rate']."',
			gst 			='".$gst * $data['conversion_rate']."', 
			actual_total='".$sub_total * $data['conversion_rate']."', 
			net_total 	='".$net_total * $data['conversion_rate']."',
			fc_subtotal ='".$sub_total."', 
			fc_tax 		='".$gst."', 
			fc_discount ='".$discount."', 
			fc_handling_fee='".$handling_fee."', 
			fc_nettotal ='".$net_total."', 
			modifiedon  = '".date('Y-m-d H:i:s')."', 
			modifiedby  ='".$this->session->data['username']."' 
			where invoice_no='".$data['invoice_no']."' ");

		$this->db->query("UPDATE ".DB_PREFIX."sales_header set 
			sub_total 	='".$sub_total * $data['conversion_rate']."', 
			discount 	='".$discount * $data['conversion_rate']."',
			gst 			='".$gst * $data['conversion_rate']."', 
			actual_total='".$sub_total * $data['conversion_rate']."', 
			net_total 	='".$net_total * $data['conversion_rate']."',
			fc_subtotal ='".$sub_total."', 
			fc_tax 		='".$gst."', 
			fc_discount ='".$discount."', 
			fc_handling_fee='".$handling_fee."', 
			fc_nettotal ='".$net_total."', 
			modifiedon  = '".date('Y-m-d H:i:s')."', 
			modifiedby  ='".$this->session->data['username']."' 
			where invoice_no='".$data['order_no']."' ");
	}
	public function untagProduct($data)
	{
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0', purchase_id='', modifiedon='".date('Y-m-d H:i:s')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$data['invoice_no']."' and product_id='".$data['product_id']."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0', purchase_id='', modifiedon='".date('Y-m-d H:i:s')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$data['order_no']."' and product_id='".$data['product_id']."' ");
		$this->updateSOandSIHeader($data);

		$this->db->query("INSERT INTO ".DB_PREFIX."untag_history (`source`, `transaction_no`, `product_id`, `location`, `username`) VALUES ( 'SI', '".$data['invoice_no']."', '".$data['product_id']."', '', '".$this->session->data['username']."') ");
	}
	public function updateSOandSIHeader($data)
	{
			$taggedCount = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_details where invoice_no='".$data['invoice_no']."' AND tagged='1' ")->num_rows;
			if($taggedCount){
				$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged=0 where invoice_no='".$data['order_no']."' ");
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged=0, modifiedon='".date('Y-m-d H:i:s')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$data['invoice_no']."' ");
				$this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set status=0 where order_no='".$data['order_no']."' ");
			}
	}

	public function getSalesReturnList($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		} 

		$sql = "SELECT SD.*,SH.invoice_date,SH.customer_code FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "sales_header as SH ON SH.invoice_no = SD.invoice_no WHERE SH.delivery_status!='Canceled' AND SH.invoice_no!='' AND SH.sales_return='1'";

		if($data['filter_transactionno']){
			$sql .= " AND SH.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SH.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND SH.network_id =  '" . $data['filter_channel'] . "'";
		}
		if($data['filter_location']){
			$sql .= " AND SH.location_code =  '" . $data['filter_location'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY SH.createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalSalesReturn($data)
	{
	    // $sql = "SELECT COUNT(*) AS totalSale,sum(net_total) as net_total FROM " . DB_PREFIX . "sales_header WHERE invoice_no !='' AND delivery_status !='Canceled' AND sales_return='1' ";

		$sql ="SELECT COUNT(*) AS totalSale,sum(SD.sub_total) as net_total,SH.invoice_date FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "sales_header as SH ON SH.invoice_no = SD.invoice_no WHERE SH.delivery_status!='Canceled' AND SH.invoice_no!='' AND SH.sales_return='1'";
		
		if($data['filter_transactionno']){
			$sql .= " AND SD.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND SH.network_id =  '" . $data['filter_channel'] . "'";
		}
		if($data['filter_location']){
			$sql .= " AND SH.location_code =  '" . $data['filter_location'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND SH.invoice_no =  '" . $data['filter_transactionno'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getSalesReturnListPdf($data)
	{
		
		$sql = "SELECT SD.*,SH.invoice_date,SH.customer_code FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "sales_header as SH ON SH.invoice_no = SD.invoice_no WHERE SH.delivery_status!='Canceled' AND SH.invoice_no!='' AND SH.sales_return='1'";

		if($data['filter_transactionno']){
			$sql .= " AND SH.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$sql .= " AND SH.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDates($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_channel']){
			$sql .= " AND SH.network_id =  '" . $data['filter_channel'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY SH.createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
}
?>