<?php

class ModelTransactionQuotation extends Model { 

   public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "quatation";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='SALINV' deleted!='1' ";
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getCreditPendingCount($data){
		$sql = "SELECT sum(a.payment_amount) as payment_amount,count(a.invoice_no) as total_records   from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";

	/*	$sql = "SELECT count(SP.invoice_no) as total_records,SUM(SP.payment_amount) as payment_amount FROM vw_sales_paymode as SP
				LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
				LEFT JOIN tbl_Credit_Sales_Collection as SC on SC.invoice_no = SP.invoice_no
				where SP.payment_type='CREDIT' and SP.payment_refno=''";*/

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCreditPendingList($data){

		$sql = "select a.*, b.invoice_date, c.name  from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";


		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}
		$sql .= " order by location_code ASC, invoice_no ASC,invoice_date DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
	
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCreditPendingListAll($data){

		$sql = "select a.*, b.invoice_date, c.name  from vw_sales_paymode a, vw_sales_header b
				left outer join tbl_member c  on b.customer_code= c.member_code
				where  a.invoice_no=b.invoice_no AND a.location_code= b.location_code 
				AND a.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) 
				AND payment_refno ='' AND payment_type='CREDIT'";

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_customer']){
			$sql .= " AND b.customer_code = '" . $data['filter_customer'] . "'";
		}

		$sql .= " order by location_code, invoice_no";
		
	$query = $this->db->query($sql);
	return $query->rows;
	}

	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "quotation as A, ".DB_PREFIX."quotation_products as B WHERE A.purchase_id = B.purchase_id";

		if($data['filter_supplier']){
			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";
		}

		if($data['filter_transactionno']){
			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		// $sql = "SELECT * FROM tbl_quotation_products";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getVendors() {

		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];
	    /*if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}*/
		$sql = "SELECT DISTINCT * FROM ".DB_PREFIX."quotation WHERE company_id = '".(int)$company_id."' AND deleted='0' ";
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_from_date'])));
			$data['filter_to_date'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_to_date'])));
			$sql .= " AND transaction_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_transaction']){
		 	$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transaction']) . "%'";
		}
			$sql .= " ORDER BY purchase_id DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function addPurchase($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId	   = $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0.00';
		}
		$total = 0;
		$gst   = 0;
		$sub_total=0;
		$discount =0;

		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}
		}
		if($data['currency_code']=='SGD'){
			$data['conversion_rate'] = '1';
		}

		$data['transaction_date'] = changeDates($data['transaction_date']);
		$data['reference_date']   = changeDates($data['reference_date']);
		$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
		$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal'] 	  = $data['conversion_rate'] * $data['total'];

		$data['tax_type']  = $data['tax_type'] == '1' ? 0:1;
		// printArray($data); die;
		$data['bill_discount_percentage'] = $data['bill_discount_percentage'] == ''  ? 0 : $data['bill_discount_percentage'];
		$sql = "INSERT INTO " . DB_PREFIX . "quotation (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,handling_fee,total,location_code,sub_total,gst,discount,created_by,tax_class_id,tax_type,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,currency_code,conversion_rate,term_id,shipping_id,network_id,modified_by) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','SALINV','" . $data['vendor'] . "','" . $data['reference_no'] . "','" . $data['reference_date'] . "','" . $data['remarks'] . "','" .$data['bill_discount_percentage'] . "','" . $data['bill_discount_price'] . "','".$data['fc_handling_fee']."','" . $data['fc_nettotal'] . "','".$data['location_code']."','".$data['fc_subtotal']."','".$data['fc_tax']."','".$data['fc_discount']."','" .$userName . "','".$data['tax_class_id']."','".$data['tax_type']."','".$sub_total."','".$gst."','".$discount."','".$handling_fee."','".$data['total']."','".$data['currency_code']."','".$data['conversion_rate']."','".$data['term_id']."','".$data['shipping_id']."','".$data['network_id']."','".$userName."')";

		// echo $sql; die;
		$this->db->query($sql);
		$purchaseId = $this->db->getLastId();
		foreach ($data['products'] as $product) {
				$discount_percentage = '';
				$discount_price      = '';
				if ($product['purchase_discount_mode'] == 1) {
				    $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price      = $product['purchase_discount_value'];
				}
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
				
			if($avg_method=='1'){
			    $avg_method ='WAM';
				$avg_price  = $this->WAM($product['product_id'],$pqty,$price);
		   	}else{
				$avg_method ='FIFO';
			    $productDetails = $this->getproductdetails($product['product_id']);
			    $avg_price      = (( $productDetails['quantity'] * $productDetails['price'] ) + ( $pqty * $price)) / ( $productDetails['quantity'] + $pqty );
		   	}
			    /************************************/
	    		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}

				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}

				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}

				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}

				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}

				if(empty($discount_price)) {
					$discount_price = '0.00';
				}

				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
				$foc = '0';
				$product['raw_cost'] = $product['price'];
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='0'){
				    $product['net_price'] = $product['total'] + $product['tax_price'];
				}				
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
				    $product['total'] = $product['total'] - $product['tax_price'];
				}
				$sqls = "INSERT INTO " . DB_PREFIX . "quotation_products (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,sku,conversion_rate) VALUES('".(int)$purchaseId."','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "','".$foc."','".$product['sku']."','".$data['conversion_rate']."')";
				// echo $sqls; die;
				$this->db->query($sqls);
		}

		$invoice_no	    = $this->getInventoryAutoId();
		$invoice_no	    = (int)$invoice_no['last_id'];
		$lastInvoiceNo	= $invoice_no + 1;
		$lastInvoiceNo  =  str_pad($lastInvoiceNo, 6, "0", STR_PAD_LEFT);
		$company_id	    = $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "entity_increment SET increment_last_id = '".$lastInvoiceNo."' WHERE entity_type_id = '2' AND company_id = '" . (int)$company_id . "'");
		return $purchaseId;

	}
	public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}

  	public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;

		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quotation WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quotation_products WHERE purchase_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	public function editPurchase($purchaseId, $data)
	{
      $companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		// printArray($data); die;
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}
		}
		$quote_info = $this->getSalesByinvNonew($data['purchase_id']);
		$data['conversion_rate'] = $quote_info['conversion_rate'];

		$data['transaction_date'] = changeDates($data['transaction_date']);
		$data['reference_date']   = changeDates($data['reference_date']);
		$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
		$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee']  = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal'] 	  = $data['conversion_rate'] * $data['total'];
		$data['tax_type']         = $data['tax_type'] =='1' ? '0':'1';

		  $sql="UPDATE " . DB_PREFIX . "quotation SET
				location_code 		= '".$data['location_code'] . "'
				, transaction_date  = '".$data['transaction_date'] . "'
				, vendor_id 		= '".$data['vendor'] . "'
				, sub_total 		= '".$data['fc_subtotal']. "'
				, discount 			= '".$data['fc_discount']."'
				, gst 				= '".$data['fc_tax']. "'
				, total 				= '".$data['fc_nettotal'] . "'
				, bill_discount_percentage  = '".$data['bill_discount_percentage'] . "'
				, bill_discount_price 		 = '".$data['bill_discount_price'] . "'
				, handling_fee 	 			 = '".$data['fc_handling_fee'] . "'
				, tax_class_id 	= '".$data['tax_class_id'] . "'
				, tax_type 		   = '".$data['tax_type'] . "'
				, reference_no 	= '".$data['reference_no'] . "'
				, date_modified 	= '".date('Y-m-d H:i:s')."'
				, modified_by 	   = '".$userName."'
				, fc_subtotal 		= '".$sub_total."'
				, fc_tax 			= '".$gst."'
				, fc_discount 		= '".$discount."'
				, fc_handling_fee = '".$handling_fee."'
				, fc_nettotal 		= '".$data['total']."'
				, term_id 			= '".$data['term_id']."'
				, shipping_id 		= '".$data['shipping_id']."'
				, remarks 		   = '".$data['remarks']."'
				, network_id      = '".$data['network_id']."'
				WHERE transaction_no= '".$data['transaction_no']. "'";
		// echo $sql; die;
		$this->db->query($sql);

		$this->db->query("DELETE FROM " . DB_PREFIX . "quotation_products WHERE purchase_id = '" . (int)$purchaseId . "'");
		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = 0;
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];

			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}
			$foc = $avg_cost = 0;
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='0'){
			    $product['net_price'] = $product['total'] + $product['tax_price'];
			}				
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
			    $product['total'] = $product['total'] - $product['tax_price'];
			}
			$sqls = "INSERT INTO " . DB_PREFIX . "quotation_products (purchase_id,product_id,weight_class_id,quantity,price,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,sku,conversion_rate) VALUES('".(int)$purchaseId."','".(int)$product['product_id']."','".(int)$product['weight_class_id']."','".(int)$product['quantity']."','".(float)$product['price']."','" .$discount_percentage."','".$discount_price."','".(int)$product['tax_class_id']."','".(float)$product['net_price']."','".(float)$product['tax_price']."','".(float)$product['total']."','".$avg_cost."','".$avg_cost."','".$foc."','".$product['sku']."','".$data['conversion_rate']."')";

			$this->db->query($sqls);
		}

		return $purchaseId;
	}
	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quotation WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "quotation_products AS pt LEFT JOIN " . DB_PREFIX . "quotation AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");

		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT pt.price AS unit_cost FROM " . DB_PREFIX . "quotation_products AS pt LEFT JOIN " . DB_PREFIX . "quotation AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC LIMIT 1");

		return $query->row['unit_cost'];

	}

	public function updatebuyPrice($data) {

		$pId = $data['product_id'];

		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");

	}

	public function getproductdetails($productId)

	{

		$query = $this->db->query("SELECT product_id,quantity,price,sku FROM " . DB_PREFIX . "product WHERE product_id= '".$productId."'");

		return $query->row;

	}
	public function getproductdetailsBySKU($sku)

	{

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product WHERE sku= '".$sku."'");
		return $query->row;

	}

	public function export_purchase_summary_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');

		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{

			$row	= $data[$i];

			$sno 	= $i+1;

			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";



		}

	}

	public function export_purchase_details_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=purchase-details.csv');

		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{

			$row	= $data[$i];

			$sno 	= $i+1;

			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";



		}

	}

	public function getTotalSales($data)
	{
	    $sql = "SELECT COUNT(*) AS totalSale,sum(net_total) as net_total FROM " . DB_PREFIX . "sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		//echo $sql; //exit;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getSalesList($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		} 
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; 
		 //exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesListPdf($data)
	{
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}

		if($data['location_code']){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; 
		// exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function export_sales_summary_to_csv($data){

		if(!$data) return false;

		ob_end_clean();

		header( 'Content-Type: text/csv' );

		header( 'Content-Disposition: attachment;filename=sales-summary.csv');

		print "S No,Location Code,Invoice No,Invoice Date,Sub Total,Discount,GST, Actual Total,Round Off,Net Total\r\n";

		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			$total+=$row['net_total'];
			print "\"$sno\",\"$row[location_code]\",\"$row[invoice_no]\",\"$row[invoice_code]\",\"$row[sub_total]\",\"$row[discount]\",\"$row[gst]\",\"$row[actual_total]\",\"$row[round_off]\",\"$row[net_total]\"\r\n";
		}
		
	}

	public function getTotalSalesDetail($data)
	{
	    //$company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "sales_detail WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";			

		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
		}
		if($sqldate!=''){
			$sql .= "AND invoice_no IN(".$sqldate.")";
		}
		
		if($data['group']=='1'){
			//$sql .= " GROUP By sku,invoice_date";
		}
		
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	
	public function getSalesListDetail($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_detail WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}

		if($data['filter_sku']){
			$sql .= " AND sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			
		}

		if($sqldate!=''){
			$sql .= "AND invoice_no IN(".$sqldate.")";
		}
		if($data['group']=='1'){
			//$sql .= " GROUP By sku,invoice_date";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalSalesDetailRe($data)
	{

	   
		$sql ="select count(a.sku) as total,sum(a.net_total) net_total from tbl_sales_detail a
               inner join tbl_sales_header b on a.invoice_no=b.invoice_no
               inner join tbl_product c on c.sku=a.sku where b.invoice_date!=''";

        if($data['filter_transactionno']){
            $sql .= " AND b.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
        }
        if($data['filter_department']){
            $sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
            $sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        }
        if($data['filter_location']){
            $sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
        }    
        if($data['filter_sku']){
            $sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
        }
        if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}

        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDate($data['filter_date_from']);
            $data['filter_date_to']  = changeDate($data['filter_date_to']);
            
            $sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
            
        } else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
            $data['filter_date_from'] = changeDate($data['filter_date_from']);
            $sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
            
        } else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
            $data['filter_date_to'] = changeDate($data['filter_date_to']);
            $sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
        }
        
        //$sql.= ' group by h.location_code,d.description,d.sku, d.sku_price,h.invoice_date';
        //$sql.= ' order by h.invoice_date desc';
        
        // $sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku,b.location_code';
        //echo $sql; exit;
        $query = $this->db->query($sql);
        return $query->row;
	}
	public function getTotalcntFastMovingReport($data)
	{
		
 		$sql ="select max(b.invoice_date) as invoice_date,b.location_code,a.sku,c.sku_description,d.department_name,sum(a.qty) as sale_qty 
 				from tbl_sales_detail a 
 				inner join tbl_sales_header b on a.invoice_no=b.invoice_no 
 				inner join tbl_product c on c.sku=a.sku 
 				left join tbl_department d on d.department_code = c.sku_department_code
 				where b.invoice_date!=''";


		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}	
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}
		
		$sql .= "group by a.sku,b.location_code,c.sku_description,d.department_name";
		if($data['filter_quantity_start']!='' && $data['filter_quantity_end']!=''){
			$sql .= " having (sum(a.qty)>=".$data['filter_quantity_start']." and sum(a.qty)<=".$data['filter_quantity_end'].")";
		}else if($data['filter_quantity_start']!=''){
			$sql .= " having sum(a.qty)>=".$data['filter_quantity_start'];
		}else if($data['filter_quantity_end']!=''){
			$sql .= " having sum(a.qty)<=".$data['filter_quantity_end'];
		}
		if($data['filter_fastlow']=='fast'){
			$sql.= ' order by sum(a.qty) desc';
		}else{
			$sql.= ' order by sum(a.qty) asc';
		}
		$query = $this->db->query($sql);
		return count($query->rows);
	}

	public function getFastMovingDetail($data)
	{

       $sql ="select max(b.invoice_date) as invoice_date,b.location_code,a.sku,c.sku_description,d.department_name,sum(a.qty) as sale_qty 
 				from tbl_sales_detail a 
 				inner join tbl_sales_header b on a.invoice_no=b.invoice_no 
 				inner join tbl_product c on c.sku=a.sku 
 				left join tbl_department d on d.department_code = c.sku_department_code
 				where b.invoice_date!=''";


		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}	
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}
		
		$sql .= "group by a.sku,b.location_code,c.sku_description,d.department_name";
		
		if($data['filter_quantity_start']!='' && $data['filter_quantity_end']!=''){
			$sql .= " having (sum(a.qty)>=".$data['filter_quantity_start']." and sum(a.qty)<=".$data['filter_quantity_end'].")";
		}else if($data['filter_quantity_start']!=''){
			$sql .= " having sum(a.qty)>=".$data['filter_quantity_start'];
		}else if($data['filter_quantity_end']!=''){
			$sql .= " having sum(a.qty)<=".$data['filter_quantity_end'];
		}
		if($data['filter_fastlow']=='fast'){
			$sql.= ' order by sum(a.qty) desc';
		}else{
			$sql.= ' order by sum(a.qty) asc';
		}

	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSalesListDetailRe($data)
	{
	    /*if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}*/
		
		$sql ="select b.location_code,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku,sum(a.qty) qty,
			   max(a.sku_price) sku_price,sum(a.discount)discount,  sum(a.net_total) net_total,d.department_name,br.brand_name,cat.category_name from tbl_sales_detail a
			   inner join tbl_sales_header b on a.invoice_no=b.invoice_no
			   inner join tbl_product c on c.sku=a.sku 
			   left join tbl_department d on d.department_code=c.sku_department_code 
			   left join tbl_category cat on cat.category_code=c.sku_category_code
 			   left join tbl_brand br on br.brand_code=c.sku_brand_code  where b.invoice_date!=''";


		if($data['filter_transactionno']){
			$sql .= " AND b.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_department']){
			$sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_category']){
			$sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}	
		if($data['filter_sku']){
			//$sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
			$sql .= " AND a.sku = '" . $this->db->escape(trim($data['filter_sku'])) . "'";
		}
		if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}

			$sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku, b.location_code,d.department_name,br.brand_name,cat.category_name';
			$sql.= ' order by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';

	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSalesListDetailReCSV($data)
	{
	  
		$sql ="select b.location_code,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code a.sku,sum(a.qty) qty,
			   max(a.sku_price) sku_price,sum(a.discount)discount,  sum(a.net_total) net_total,d.department_name,br.brand_name from tbl_sales_detail a
			   inner join tbl_sales_header b on a.invoice_no=b.invoice_no
			   inner join tbl_product c on c.sku=a.sku 
			   left join tbl_department d on d.department_code=c.sku_department_code 
 			   left join tbl_brand br on br.brand_code=c.sku_brand_code  where b.invoice_date!=''
			   ";

		if($data['filter_transactionno']){
			$sql .= " AND b.invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_location']){
			$sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}	
		if($data['filter_sku']){
			$sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}
		if($data['filter_department']){
			$sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_category']){
			$sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND b.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_from'] . "'";
			
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND b.invoice_date = '" . $data['filter_date_to'] . "'";
		}

		
		
		$sql.= ' group by c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, a.sku, b.location_code,d.department_name,br.brand_name';
		$sql.= ' order by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';



		
		//	echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getExpensesListDetailReCSV($data)
	{
	  
		$sql ="select payout_refno,payout_location_code,payout_terminal_code,payout_date,payout_remarks,payout_amount,
			   payout_shift from tbl_pay_out where";
		
		if($data['filter_location']){
			$sql .= " payout_location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND payout_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			
		} 
			//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalSalesListDetailDepartment($data)
    {
       
     $sql = "SELECT P.sku_department_code,Max(D.department_name) department_name, P.sku_category_code, Max(C.Category_Name) Category_Name, SUM(SD.qty) as total_qty,sum(SD.net_total) as actual_total FROM tbl_sales_detail as SD ";
               
     $sql.= " LEFT JOIN tbl_sales_header as SH on SH.invoice_no = SD.invoice_no
              LEFT JOIN tbl_product as P on SD.sku = P.sku
              LEFT JOIN tbl_department as D on D.department_code = P.sku_department_code
              LEFT JOIN tbl_category as C on C.category_code = P.sku_category_code";
       $sql.= " WHERE SH.invoice_no!=''";
        if($data['filter_location']){
            $sql .= " AND SH.location_code = '" . $this->db->escape($data['filter_location']) . "'";
        }
        if($data['filter_department']){
            $sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
           $sql .= " AND P.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
       }
        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDate($data['filter_date_from']);
            $data['filter_date_to']  = changeDate($data['filter_date_to']);
            $sql .= " AND SH.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
        }
        $sql.=" Group by P.sku_department_code,P.sku_category_code order by P.sku_department_code,P.sku_category_code";
        $query = $this->db->query($sql);
        return $query->rows;
    
    }
	public function getExpensesListDetailTotal($data)
	{
	  
	    $sql = "SELECT count(payout_refno) as totalrecords,sum(payout_amount) as totalamount  FROM tbl_pay_out WHERE";

		if($data['filter_location']){
			$sql .= " payout_location_code   = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND CAST(payout_date as DATE)  between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	
	}


	public function getExpensesListDetail($data)
	{
	  
	    $sql = "SELECT payout_refno,payout_location_code ,payout_terminal_code,payout_date,payout_remarks,payout_amount,payout_shift  FROM tbl_pay_out WHERE";

		if($data['filter_location']){
			$sql .= " payout_location_code   = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND CAST(payout_date as DATE)  between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		
		$sql.= ' order by payout_date desc';

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = $data['limit'];
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}

		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	
	}

	public function getSalesListDetailDepartment($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		//$sql = "SELECT SD.Sku,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL";
		
		$sql = "SELECT P.sku_department_code,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total,SUM(Round_off) as roundoffTot FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL ";

		if($data['filter_sku']){
			$sql .= " AND SD.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND SD.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_department']){
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			//$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND SD.invoice_no IN ('" . implode("', '", $invinos) . "')";
		}

		$sql.=" Group by P.sku_department_code";

		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.sku_department_code";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		//exit;
		
		// function for get the header_discount , header_round_off
			
		$query = $this->db->query($sql);
		
		$resAry['Listing'] = $query->rows;
			
		return $resAry;
	}
	public function getDiscountDetails($data){
		$sql = "Select Sum(Discount) as dicountTot,Sum(Round_off) as roundoffTot from " . DB_PREFIX . "sales_header where Invoice_Date!=''";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND Invoice_Date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND Invoice_Date  = '" . $data['filter_date_from']."'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND Invoice_Date  = '" . $data['filter_date_to']."'";
		}

		if($data['filter_location']){
			$sql .= " AND Location_Code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row; 
	}
	public function getSalesListDetailDepartmentX($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		//$sql = "SELECT SD.Sku,SUM(qty) as qty,SUM(sub_total) as sub_total,SUM(discount) as discount,SUM(net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku WHERE SD.invoice_no IS NOT NULL";

		$sql = "SELECT P.sku_department_code,SUM(SD.qty) as qty,SUM(SD.sub_total) as sub_total,SUM(SD.discount) as discount,SUM(SD.net_total) as net_total FROM " . DB_PREFIX . "sales_detail as SD LEFT JOIN " . DB_PREFIX . "product as P on SD.sku=P.sku 
			LEFT JOIN " . DB_PREFIX . "sales_header as SH on SD.invoice_no = SH.invoice_no WHERE SD.invoice_no!=''";

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND SH.invoice_date  = '" . $data['filter_date_from']."'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND SH.invoice_date  = '" . $data['filter_date_to']."'";
		}

		if($data['filter_location']){
			$sql .= " AND SD.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}

		if($data['filter_department']){
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		

		$sql.=" Group by P.sku_department_code";

		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.sku_department_code";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		
		/*$sql = "SELECT P.sku_department_code,SUM(SD.qty) as qty,SUM(SD.sub_total) as sub_total,SUM(SD.discount) as discount,SUM(SD.net_total) as net_total FROM tbl_sales_detail as SD 
LEFT JOIN tbl_product as P on SD.sku=P.sku 
LEFT JOIN tbl_sales_header as SH on SD.invoice_no = SH.invoice_no
WHERE SD.invoice_no IS NOT NULL AND SD.location_code = 'BB283' AND SH.invoice_date between '2017-12-01' AND '2017-12-01'
Group by P.sku_department_code ORDER BY P.sku_department_code ASC";*/


		// function for get the header_discount , header_round_off
			
		$query = $this->db->query($sql);
		
		
		/*$discountSQL = "SELECT sum(discount) as header_discount,sum(round_off) as header_round_off  FROM " . DB_PREFIX . "sales_header 
							WHERE invoice_no IN('" . implode("', '", $invinos) . "')";
		
		if($data['filter_location']){
			$discountSQL.= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}		

		$discountQry = 	$this->db->query($discountSQL);
		$resAry['cntDetails'] = $discountQry->row;	
		*/
		$resAry['Listing'] = $query->rows;
			
		return $resAry;
	}
	public function getroundOffBasedonDepartment($data,$departmentCode){
		
		$sql = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "sales_header WHERE invoice_no IS NOT NULL";

		if($data['filter_location']){
			$sql.= " AND location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
	
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql.= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql.= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		}else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql.= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		

		/*$sql = "SELECT sum(discount) as header_discount,sum(round_off) as header_round_off FROM " . DB_PREFIX . "sales_header 
						WHERE invoice_no IN ('" . implode("', '", $invinos) . "')";*/
		echo $sql;exit;

	}
	public function getDepartmentNameByCode($code){
		$sql = "SELECT department_name FROM " . DB_PREFIX . "department where department_code='".$code."'"; 
		$query = $this->db->query($sql);
		return $query->row['department_name'];
	}
	public function getDepartmentDetailsbySKU($sku){
		$sql = "SELECT sku_department_code,sku_category_code, sku_brand_code FROM " . DB_PREFIX . "product where sku='".$sku."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getInvoiceDateByNo($invoiceno) {
		$sql = "SELECT invoice_date FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '".$invoiceno."'";
		$query = $this->db->query($sql);
		return $query->row['invoice_date'];
	}
	public function settlementdetails($date) {
		//$date = '2017-09-12';
		$date = changeDate($date); 
		$sql = "SELECT SUM(net_total) as totalsales, SUM(discount) as discount,SUM(gst) as gst,SUM(round_off) as round_off FROM " . DB_PREFIX . "sales_header WHERE Cast(createdon As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		//echo $sql ;exit;
		return $query->row;
	}

	public function paymentmodedetails($type,$date) {
		$date = changeDate($date); 
		$sql = "SELECT SUM(payment_amount) as paymentamount FROM " . DB_PREFIX . "sales_paymode WHERE payment_type = '".$type."' AND Cast(createdon As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getLocation(){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id != ''"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getLocationCode($companyId){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "company as C LEFT JOIN " . DB_PREFIX . "location as L ON C.location_code = L.location_code WHERE company_id = '".$companyId."' "; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getTotalSalesByLocationCode($data)
	{
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
      	$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "sales_header WHERE $where";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	public function getSalesListByLocationCode($data)
	{
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT * FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY invoice_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCollectionPaymentDetails($location_code,$invoice_date,$terminal_code=''){
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_paymode WHERE location_code = '".$location_code."' AND invoice_no = '".$invoice_no."'"; 
		$sql = "SELECT SUM(payment_amount) as payment_amount,payment_type FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE invoice_no IN (SELECT invoice_no FROM " . DB_PREFIX . "Credit_Sales_Collection where collection_date = '".$invoice_date."' AND location_code='".$location_code."') GROUP BY(payment_type)"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getPaymentDetails($location_code,$invoice_date){
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_paymode WHERE location_code = '".$location_code."' AND invoice_no = '".$invoice_no."'"; 
		$sql = "SELECT SUM(payment_amount) as payment_amount,payment_type FROM " . DB_PREFIX . "sales_paymode WHERE invoice_no IN (SELECT invoice_no FROM " . DB_PREFIX . "sales_header where invoice_date = '".$invoice_date."' AND location_code='".$location_code."') GROUP BY(payment_type)"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getTotalSalesListByDate($data){
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,invoice_date  FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesListByDate($data){
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,invoice_date FROM " . DB_PREFIX . "sales_header WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY invoice_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if($data['filterVal'] !='1'){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalCreditSalesListByDate($data){
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,collection_date  FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND collection_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCreditSalesListByDate($data){
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		
		if($data['location_code'] !=''){
			$where = "location_code = '".$data['location_code']."'";
		} else {
			$where = "invoice_no IS NOT NULL";
		}
		//$sql = "SELECT location_code,CAST(invoice_date as DATE),SUM(actual_total) as actual_total, SUM(gst) as gst, SUM(net_total) as net_total,CAST(createdon as DATE) FROM " . DB_PREFIX . "sales_header WHERE $where ";
		$sql = "SELECT DISTINCT location_code,collection_date FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE $where ";
		
		/*if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND collection_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND collection_date =  '" . $data['filter_date_to'] . "'";
		}
		//$sql .= " GROUP BY invoice_date";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY collection_date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if($data['filterVal'] !='1'){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function getSalesListByCodeDate($location_code,$invoice_date){
		$sql = "SELECT SUM(actual_total) AS actual_total, SUM(gst) as gst, SUM(net_total) as net_total, SUM(discount) as discount,SUM(round_off) as round_off FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getSalesInvoiceByCodeDate($location_code,$invoice_date){
		$sql = "SELECT invoice_no FROM " . DB_PREFIX . "sales_header WHERE invoice_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getCCListByCodeDate($location_code,$invoice_date,$terminal_code=''){
		$sql = "SELECT SUM(payment_amount) AS payment_amount FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getCCListByCodeDateNew($location_code,$invoice_date,$terminal_code=''){
		//$sql = "SELECT SUM(payment_amount) AS payment_amount FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'"; 
		$sql = "select sc.shift_paymode as payment_type,sum(shift_system_amount) as payment_amount from " . DB_PREFIX . "shift_creditdetail as sc left join " . DB_PREFIX . "shift_header as sh on sh.shift_refno = sc.shift_ref_no where  sh.shift_location_code='".$location_code."' and sh.shift_date='".$invoice_date."' GROUP by(sc.shift_paymode)";

		$query = $this->db->query($sql);
		return $query->rows;	
	}

	public function getCCListByCodeDateNewTotal($location_code,$invoice_date,$terminal_code=''){
		$sql = "select sum(shift_system_amount) as payment_amount from " . DB_PREFIX . "shift_creditdetail as sc left join " . DB_PREFIX . "shift_header as sh on sh.shift_refno = sc.shift_ref_no where  sh.shift_location_code='".$location_code."' and sh.shift_date='".$invoice_date."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}

	public function getCCInvoiceByCodeDate($location_code,$invoice_date,$terminal_code=''){
		$sql = "SELECT invoice_no FROM " . DB_PREFIX . "Credit_Sales_Collection WHERE collection_date = '".$invoice_date."' AND location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getTerminalsByLocation($date='',$location_code=''){
		$date = changeDate($date); 
		$sql = "SELECT DISTINCT shift_terminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_terminal!='' and shift_location_code='".$location_code."'";  
		//$sql = "SELECT DISTINCT shift_teminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_teminal!='' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCashierByTerminalLocation($date='',$location_code='',$terminal_code=''){
		$date = changeDate($date); 
		$sql = "SELECT DISTINCT shift_cashier  FROM " . DB_PREFIX . "shift_header where shift_date ='".$date."' AND shift_terminal='".$terminal_code."' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getShiftByTerminalLocation($date='',$location_code='',$terminal_code=''){
		$date = changeDate($date); 
		$sql = "SELECT DISTINCT shift_no  FROM " . DB_PREFIX . "shift_header where shift_date ='".$date."' AND shift_terminal='".$terminal_code."' and shift_location_code='".$location_code."'";  
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTerminals($date='',$location_code=''){
		$date = changeDate($date); 
		$sql = "SELECT DISTINCT shift_terminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_terminal!='' and shift_location_code='".$location_code."'";
		//$sql = "SELECT DISTINCT shift_teminal as shift_teminal FROM " . DB_PREFIX . "shift_header where shift_teminal!='' and shift_location_code='".$location_code."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getLocations(){
		$sql = "SELECT DISTINCT L.location_code,L.location_name FROM " . DB_PREFIX . "shift_header as SH LEFT JOIN " . DB_PREFIX . "location as L ON SH.shift_location_code = L.location_code"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getReportsLocations($date){
		$date = changeDate($date); 
		$sql = "SELECT DISTINCT L.location_code,L.location_name FROM " . DB_PREFIX . "shift_header as SH LEFT JOIN " . DB_PREFIX . "location as L ON SH.shift_location_code = L.location_code WHERE Cast(shift_date As Date) = '".$date."'"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function settlementshiftsums($date,$teminal='',$location_code='',$shift_no='') {
		$date = changeDate($date); 
		$sql = "SELECT SUM(shift_nettotal) as totalsales,SUM(shift_total) as subtotalsales, SUM(shift_discount) as discount,SUM(shift_gst) as gst FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."' and shift_location_code='".$location_code."'"; 
		if($teminal){
			$sql.= "AND shift_terminal = '".$teminal."' AND shift_no='".$shift_no."'";
			//$sql.= "AND shift_teminal = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function settlementshiftdetails($date,$teminal='',$shift_no='') {
		$date = changeDate($date); 
		$sql = "SELECT shift_refno, shift_date, shift_cashier, shift_terminal as shift_teminal  FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."'"; 
		//$sql = "SELECT shift_refno, shift_date, shift_cashier, shift_teminal as shift_teminal  FROM " . DB_PREFIX . "shift_header WHERE Cast(shift_date As Date) = '".$date."'"; 
		if($teminal){
			$sql.= "AND shift_terminal = '".$teminal."' AND shift_no='".$shift_no."'";
			//$sql.= "AND shift_teminal = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function paymentmodeshiftdetails($type,$regno) {
		$date = changeDate($date); 
		$sql = "SELECT SUM(shift_system_amount) as paymentamount FROM " . DB_PREFIX . "shift_detail WHERE shift_paymode = '".$type."' AND shift_ref_no = '".$regno."'"; 
		$query = $this->db->query($sql);
		return $query->row;
	}	

	public function paymentdetailsSettlement($regno) {
		$date = changeDate($date); 
		$sql = "SELECT SUM(shift_system_amount) as sys_paymentamount,SUM(shift_manual_amount) as mnl_paymentamount,sum(shift_difference) as diff, shift_paymode FROM " . DB_PREFIX . "shift_detail WHERE shift_ref_no = '".$regno."' group by shift_paymode";
		$query = $this->db->query($sql);
		return $query->rows;
	}	


	public function paymentCreditdetails($regno) {
		$date = changeDate($date); 
		$sql = "SELECT SUM(shift_system_amount) as paymentamount,shift_paymode FROM " . DB_PREFIX . "shift_creditdetail WHERE shift_ref_no = '".$regno."' GROUP BY(shift_paymode)"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}	

	public function settlementcollectiondetails($date,$shift_refno='',$teminal='') {
		$date = changeDate($date); 
		$sql = "SELECT * FROM " . DB_PREFIX . "daily_collection_summary WHERE Cast(collection_date As Date) = '".$date."'"; 
		if($shift_refno){
			$sql.= "AND shift_refno = '".$shift_refno."'";
		}
		if($teminal){
			$sql.= "AND terminal_code = '".$teminal."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	
	public function getTotalVoidSalesDetail($data)
	{
		$sql = "SELECT COUNT(*) AS totalSale,SUM(net_total) as net_total FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getVoidSalesListDetail($data)
	{
	    /*if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}*/
		$sql = "SELECT * FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no IS NOT NULL";
		
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND invoice_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}
		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}
		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		// }
		
		$query = $this->db->query($sql);
		return $query->rows;
	}	
	public function getInvoiceVoidDateByNo($invoiceno) {
		$sql = "SELECT invoice_date FROM " . DB_PREFIX . "void_sales_header WHERE invoice_no = '".$invoiceno."'";
		$query = $this->db->query($sql);
		return $query->row['invoice_date'];
	}
	public function getTotalVoidSalesListDetailByInvNo($data)
	{
		$sql = "SELECT COUNT(*) AS totalSale FROM " . DB_PREFIX . "void_sales_detail WHERE invoice_no = '".$data['invoice_no']."'";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		}
		$query = $this->db->query($sql);
		return $query->row['totalSale'];
	}
	public function getVoidSalesListDetailByInvNo($data){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "void_sales_detail WHERE invoice_no = '".$data['invoice_no']."'";
		if($data['filter_transactionno']){
			$sql .= " AND invoice_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_from'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sqldate = "SELECT DISTINCT(invoice_no) as invoice_no  FROM " . DB_PREFIX . "void_sales_header WHERE invoice_date = '" . $data['filter_date_to'] . "'";
			$querydate = $this->db->query($sqldate);
			$res = $querydate->rows;
			foreach($querydate->rows as $invno) {
				$invinos[] = $invno['invoice_no'];
			}
			$invoicenos =  implode(',',$invinos);
			$sql .= " AND invoice_no IN ('" . implode("', '", $invinos) . "')";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
public function getCashflowList($data){
	
		$sql = "SELECT DCS.*, SH.shift_location_code FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if($data['filter_teminal']){
			$sql .= " AND DCS.terminal_code LIKE '%" . $this->db->escape($data['filter_teminal']) . "%'";
		}
		
		if($data['filter_location']){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['short_by']) && $data['short_by'] !='') {
            $sql .= " ORDER BY  DCS." . $data['short_by'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['short_by_order']) && ($data['short_by_order'] !='')) {
            $sql .=" ".$data['short_by_order'];
        } else {
            $sql .= " ASC";
        }
		/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " ASC";
        } */
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		/*$query = $this->db->query("select * from tbl_shift_header"); 
       printArray($query);*/
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getCashflowCount($data){

		$sql = "SELECT count(DCS.shift_refno) as total_records FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if($data['filter_teminal']){
			$sql .= " AND DCS.terminal_code LIKE '%" . $this->db->escape($data['filter_teminal']) . "%'";
		}

		if($data['filter_location']){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";

		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}

         //echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCashflowListAll($data){
        
		$sql = "SELECT DCS.*, SH.shift_location_code FROM " . DB_PREFIX . "daily_collection_summary as DCS LEFT JOIN " . DB_PREFIX . "shift_header as SH ON SH.shift_refno = DCS.shift_refno WHERE terminal_code!=''";

		if(!empty($data['filter_teminal'])){
			$sql .= " AND DCS.terminal_code = '" . $data['filter_teminal'] . "'";
		}

		if(!empty($data['filter_location'])){
			$sql .= " AND SH.shift_location_code = '" . $data['filter_location'] . "'";

		}
		
		if(!empty($data['filter_date_from']!='' && $data['filter_date_to']!='')){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if(!empty($data['filter_date_from']!='' && $data['filter_date_to']=='')){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_from'] . "'";
		} else if(!empty($data['filter_date_from']=='' && $data['filter_date_to']!='')){
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND DCS.collection_date = '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['short_by']) && $data['short_by'] !='') {
            $sql .= " ORDER BY " .$data['short_by'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['short_by_order']) && ($data['short_by_order'] != '')) {
            $sql .= " ".$data['short_by_order'];
        } else {
            $sql .= " ASC";
        }
		/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY DCS.collection_date,DCS.terminal_code,DCS.shift_refno";
        }
        if (isset($data['order']) && ($data['order'] == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " ASC";
        }*/
		
	$query = $this->db->query($sql);
	return $query->rows;
	}

	public function getcreditpendingsummaryCount($data){

	$sql = "SELECT COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
		where SP.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) and SP.payment_type='CREDIT' 
		AND SP.payment_refno=''";

		if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}


		// $sql.=" GROUP BY SP.location_code,SH.customer_code";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCustomernameByCode($code){
		$sql = "SELECT name from tbl_member where member_code='".$code."'"; 
		$query = $this->db->query($sql);
		return $query->row['name'];	

	}
	public function getcreditpendingsummaryList($data){

	$sql = "SELECT SP.location_code,SH.customer_code,COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
	    where SP.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) and SP.payment_type='CREDIT' and SP.payment_refno=''";
	    
	    if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}

	    $sql.="GROUP BY SP.location_code,SH.customer_code";
		if (isset($data['start']) || isset($data['limit'])) {
			$sql .= " ORDER BY SH.customer_code ASC";
			if ($data['start'] < 0) {
			$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
			$data['limit'] = 20;
			}
		$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		

	$query = $this->db->query($sql);
	return $query->rows;
	}
	public function getcreditpendingsummaryListAll($data){

		$sql = "SELECT SP.location_code,SH.customer_code,COUNT(SP.invoice_no) as total_invoice_count,SUM(payment_amount) as payment_amount FROM vw_sales_paymode as SP
LEFT JOIN vw_sales_header as SH ON SH.invoice_no = SP.invoice_no
where SP.invoice_no not in(select invoice_no from tbl_Credit_Sales_Collection) and SP.payment_type='CREDIT' and SP.payment_refno=''"; 


	
		if($data['filter_location']){
			$sql .= " AND SP.location_code = '" . $data['filter_location'] . "'";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND SP.createdon =  '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_customer']){
			$sql .= " AND SH.customer_code = '" . $data['filter_customer'] . "'";
		}
		
		$sql .= "GROUP BY SP.location_code,SH.customer_code";


		if(!empty($data['order'])&&!empty($data['sort'])){
			$sql .= " order  by '".$data['sort']."''  '".$data['order']."' ";

		}/*else{
			$sql.="order by  createdon DESC";
		}*/
		
	$query = $this->db->query($sql);
	return $query->rows;
	}
	public function getsalesDetailsbyInvoice($invoiceno){
		$sql = "select * from tbl_sales_detail where invoice_no='".$invoiceno."' order by createdon asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCustomer(){
		$sql = "select member_code,name from tbl_member order by name asc"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
   /*sales new start*/
	public function getTotalSalesnew($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalsales FROM " . DB_PREFIX . "quotation WHERE transaction_no!='' AND deleted='0' ";
		if($data['filter_from_date']!='' && $data['filter_to_date']!=''){
			$data['filter_from_date'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_from_date'])));
			$data['filter_to_date'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_to_date'])));
			$sql .= " AND transaction_date between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		if($data['filter_location']!=''){
			$sql .= " AND location_code = '" . $data['filter_location'] . "'";
		}
		if($data['filter_transaction']){
		 $sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transaction']) . "%'";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row['totalsales'];
	}
	public function getSalesAutoIdnew() 
	{
		$query = $this->db->query("SELECT purchase_id  FROM " . DB_PREFIX . "quotation ORDER BY purchase_id DESC LIMIT 1");
		return $query->row['purchase_id'];
	}

	public function getB2BCustomers()
	{
	  $query = $this->db->query("SELECT tax_allow,tax_type,customercode,name FROM " . DB_PREFIX . "customers WHERE customercode !='' order by name asc");
	  return $query->rows;
	}
	public function getB2BCustomersbyCode($customercode)
	{
	  $customercode = str_pad($customercode, 4, "0", STR_PAD_LEFT);
	  $query = $this->db->query("SELECT * FROM ".DB_PREFIX."customers WHERE customercode ='".$customercode."'");
	  return $query->row;
	}
	public function getCustomerDetails($customercode)
	{
	  $query = $this->db->query("SELECT * FROM ".DB_PREFIX."customers WHERE customercode ='".$customercode."'");
	  return $query->row;
	}
	public function getcustomerPrice($data = array()) 
	{
		$sql = "SELECT sku,customer_price FROM " . DB_PREFIX . "b2b_customers_price";

	     $sql.= " WHERE customers_code != ''";
	    if($data['sku']!=''){
			$sql .= " AND sku =  '" . $data['sku'] . "'";
		}
		if($data['transaction_date']!=''){
			$sql .= " AND ('" .$data['transaction_date']. "' between FromDate and ToDate)";
		}
		if($data['customer_code']!=''){
			$sql .= " AND customers_code =  '" . $data['customer_code'] . "'";
		}
		if($data['location_code']!=''){
			$sql .= " AND location_code =  '" . $data['location_code'] . "'";
		}

		//echo $sql; exit;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getSalesByinvNonew($invoiceNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "quotation WHERE purchase_id = '" . $invoiceNumber . "'");
		return $query->row;
	}

	public function DeleteSalesBysku($sku,$invoiceNumber)
	{
	   $sql = "DELETE FROM " . DB_PREFIX . "sales_detail WHERE sku = '" . $sku . "' and invoice_no = '" . $invoiceNumber . "'"; 
		$this->db->query($sql);

	}
	public function getSalesListnew($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "sales_header WHERE customer_code != ''";

		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY invoice_no";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$sql = "SELECT * FROM " . DB_PREFIX . "sales_header WHERE customer_code != ''";
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesnew($purchaseId)
	{
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "quotation WHERE purchase_id = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getShippingAddressById($id)
	{
		$query = $this->db->query("SELECT  * FROM ".DB_PREFIX."shipping WHERE id = '".$id."'");
		return $query->row;
	}	
	public function getSalesnewtotal($purchaseId)
	{
		$query = $this->db->query("SELECT  customer_code,sub_total,discount,gst,actual_total,net_total FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getSalesProductnew($purchaseId)
	{
		$query = $this->db->query("SELECT qp.*,p.name as description FROM " . DB_PREFIX . "quotation_products as qp LEFT JOIN ".DB_PREFIX."product as p on qp.product_id=p.product_id WHERE qp.purchase_id = '" .$purchaseId . "' ");
		return $query->rows;
	}
	public function getSalesProductbysku($sku,$transaction_no)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "b2b_sales_detail WHERE sku = '" .$sku . "' and invoice_no = '" .$transaction_no. "'");
		return $query->row;
	}
	public function clear_type(){

		$query = $this->db->query("SELECT void_type FROM ".DB_PREFIX."clearcart_detail WHERE void_type != '' group by void_type ");
		return $query->rows;
	}

	public function get_clearcartdetailsTotal($data){

		$sql = "SELECT count(sku) as total FROM " . DB_PREFIX . "clearcart_detail ";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDate($data['filter_date_from']); 
			$data['filter_date_to']    = changeDate($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function get_suppliersales($data){

			$sql = "select SD.sku,P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name, count(SD.sku) as saleQty from tbl_sales_header as SH
				LEFT join tbl_sales_detail as SD on SH.invoice_no=SD.invoice_no
				LEFT join tbl_product as P on SD.sku=P.sku
				LEFT join tbl_vendor as V on V.vendor_code = P.sku_vendor_code ";

			if($data['filter_date_from'] && $data['filter_date_to']){
				$sql .=" where SH.invoice_date between '".$data['filter_date_from']."' and '".$data['filter_date_to']."' ";
			}
			if($data['filter_location']){
				$sql .=" AND SH.location_code='".$data['filter_location']."' ";
			}
			if($data['filter_supplier'] !='All'){
				$sql .= "AND P.sku_vendor_code= '".$data['filter_supplier']."' ";
			}
			$sql .= " AND P.sku_vendor_code!='' group by SD.sku, P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name";
		
				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					$sql .= " ORDER BY " . $data['sort'];
				} else {
					$sql .= " ORDER BY SD.sku";
				}
				if (isset($data['order']) && ($data['order'] == 'ASC')) {
					$sql .= " ASC";
				} else {
					$sql .= " DESC";
				}
				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}
					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}
					
					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
				}
			$query = $this->db->query($sql);
			return $query->rows;
	}
	public function get_clearcartdetails($data){
		$sql = "SELECT * FROM " . DB_PREFIX . "clearcart_detail ";
		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDate($data['filter_date_from']); 
			$data['filter_date_to']    = changeDate($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function get_totalclearcartdetails($data)
	{
		$sql = "SELECT sum(net_total) as totalSale FROM " . DB_PREFIX . "clearcart_detail ";

		if($data['filter_date_from']!='' && $data['filter_date_to']){

			$data['filter_date_from']  = changeDate($data['filter_date_from']); 
			$data['filter_date_to']    = changeDate($data['filter_date_to']); 

			$sql .="WHERE CAST(invoice_date as DATE) BETWEEN '".$data['filter_date_from']."' AND '".$data['filter_date_to']."' ";
		}
		if($data['filter_location']){

			$sql .= "AND location_code ='".$data['filter_location']."' ";
		}
		if($data['filter_clear_type'] && $data['filter_clear_type'] !='All'){

			$sql .= "AND void_type = '".$data['filter_clear_type']."' ";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function get_totalsupplierssku($data){

			
			$sql = "select SD.sku,P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name, count(SD.sku) as saleQty from tbl_sales_header as SH
				LEFT join tbl_sales_detail as SD on SH.invoice_no=SD.invoice_no
				LEFT join tbl_product as P on SD.sku=P.sku
				LEFT join tbl_vendor as V on V.vendor_code = P.sku_vendor_code ";

			if($data['filter_date_from'] && $data['filter_date_to']){
				$sql .=" where SH.invoice_date between '".$data['filter_date_from']."' and '".$data['filter_date_to']."' ";
			}
			if($data['filter_location']){
				$sql .=" AND SH.location_code='".$data['filter_location']."' ";
			}
			if($data['filter_supplier']!='All'){
				$sql .= "AND P.sku_vendor_code= '".$data['filter_supplier']."' ";
			}
			$sql .= " AND P.sku_vendor_code!='' group by SD.sku, P.name,SD.sku_price,P.sku_vendor_code,V.vendor_name";
			$query = $this->db->query($sql);
			return count($query->rows);
	}
	/*sales new end */
	public function getVoidSalesDetailreport($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "void_sales_header  ";

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDate($data['filter_date_from']); 
				$data['filter_date_to']  = changeDate($data['filter_date_to']); 
				$sql .= " WHERE invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			if($data['filter_invoiceno']){
				$sql .= " and invoice_no LIKE '%" . $this->db->escape($data['filter_invoiceno']) . "%'";
			}
			if($data['filter_location']){
				$sql .= " and location_code = '" . $data['filter_location'] . "'";
			}
			

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY invoice_no";
			}
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);
			return $query->rows;
		}

	public function getVoidSalesDetail($invoice_no,$field_value='')
	{
	  	$sql = "SELECT * FROM " . DB_PREFIX . "void_sales_detail WHERE invoice_no = '".$invoice_no."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function create_invoice($id){
		$lastId = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key`='config_next_quotation_invoiceno' ")->row;

		$update = $this->db->query("UPDATE ".DB_PREFIX."quotation set invoice_no = '".$lastId['value']."' where purchase_id='".$id."' ");
		if($update){
			$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."setting set value = value+1 WHERE `key`='config_next_quotation_invoiceno' ");
		}
		return $lastId['value'];
	}
	public function delete_quote($id){
		$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."quotation set status ='Void' WHERE purchase_id='".$id."' ");
	}
	public function getProductName($id){
		$sql= $this->db->query("SELECT name FROM ".DB_PREFIX."product where product_id='".$id."' ")->row;
		return $sql['name'];
	}
	public function deletePurchase($purchase_id){
		$this->db->query("UPDATE ".DB_PREFIX."quotation set deleted='1' where purchase_id='".$purchase_id."'");
	}
	public function checkCompanyCurrency($data){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."' and currency_code='".$data['currency']."' ");
		return $query->row;	
	}
	public function getVendorCurrency($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers where customercode = '" .$vendor_id. "'");
		return $query->row['currency_code'];
	}
	public function getCustomerTerm($customer_code){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers where customercode = '" .$customer_code. "'");
		return $query->row['term_id'];
	}
	public function getShippingAddress($vendor_id) {
		$query = $this->db->query("SELECT id,customer_id,shipping_code,address1,address2,city,country,contact_no,zip,isshipping,isdefault,name FROM ".DB_PREFIX."shipping where customer_id = '" .$vendor_id. "' GROUP BY address1,address2,zip ORDER BY `isshipping` DESC");
		return $query->rows;
	}
}
?>