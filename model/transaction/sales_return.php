<?php

class ModelTransactionSalesReturn extends Model {

	public function getTotalPurchase($data) {
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "sales_header WHERE company_id = '". (int)$company_id ."' AND sales_return = '1'";
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}

	public function getSalesAutoIdnew() 
	{
		return $this->db->query("SELECT id FROM ".DB_PREFIX."sales_header ORDER BY id DESC LIMIT 1")->row['id'];
	}
	public function getPurchaseList($data) {

	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "sales_header WHERE company_id = '" . (int)$company_id . "' AND sales_return = '1' ORDER BY id DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getB2BCustomersbyCode($customercode)
	{
	  $customercode = str_pad($customercode, 4, "0", STR_PAD_LEFT);
	  $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers WHERE customercode ='".$customercode."'");
		return $query->row;
	}
	public function addSalesnew($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId	    = $this->session->data['user_id'];
		$userName	= $this->session->data['username'];

		$total = 0;
		$gst   = 0;
		$sub_total =$totalGst=0;
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}
		}
			if($data['currency_code']=='SGD'){
				$data['conversion_rate'] = '1';
			}
			$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      = $data['conversion_rate'] * $gst;
			$data['fc_discount'] = $data['conversion_rate'] * $discount;
			$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];

		    $data['header_remarks']   = changeDates($data['reference_date']);
		    $sqlheader= "INSERT INTO " . DB_PREFIX . "sales_header (location_code,invoice_no,invoice_date,customer_code,sub_total,discount,gst,actual_total,round_off,net_total,cashier,terminal_code,shift_no,detail_lines,header_remarks,createdby,modifiedby,discount_type,hold,company_id,sales_return,tax_class_id,tax_type,term_id,shipping_id, bill_discount_percentage,bill_discount_price,currency_code,conversion_rate,fc_subtotal,fc_discount,fc_tax,fc_nettotal,remarks) VALUES('" .$data['location_code']. "', '" . $data['transaction_no'] . "', curdate(), '" . $data['vendor'] . "', '" . $data['fc_subtotal']. "', '" . $data['fc_discount']."', '" . $data['fc_tax'] . "', '" . $sub_total . "', '0.00', '" . $data['fc_nettotal'] . "', '" . $cashier . "', '" . $terminal_code . "', '0', '0', '" . $data['header_remarks'] . "', '" . $userName . "', '" . $userName . "', 'Sales', '".$data['hold']."', '".$companyId."', '1', '".$data['tax_class_id']."', '".$data['tax_type']."', '".$data['term_id']."', '".$data['shipping_id']."', '".$data['bill_discount_percentage']."', '".$data['bill_discount_price']."', '".$data['currency_code']."', '".$data['conversion_rate']."', '".$sub_total."', '".$discount."', '".$gst."', '".$data['total']."','".$data['remarks']."')";

		    // echo $sqlheader; die;
		    $res = $this->db->queryNew($sqlheader);
		    if(!$res){
			  header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales_return/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);		
				exit;
		    }
		for($i=0; $i <count($data['products']) ; $i++) {
    	    $data['round_off']='0.00';
    	    if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		    }

			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}

			$sku 		   = $this->getSkuByProductId($data['products'][$i]['product_id']);
			$location_code = $data['location_code'];
			$qty 		   = $data['products'][$i]['quantity'];
			
			$child['product_id']   = $data['products'][$i]['product_id'];
			$child['qty']  		   = $qty;
			$child['location_code']= $data['location_code'];
			$this->cart->updatechildProductQty($child,'P');

			$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty=sku_qty+'".$qty."', modifiedon=curdate(),modifiedby='".$userName."' WHERE sku='".$sku."' AND location_Code='".$location_code."'");

			if($data['products'][$i]['hasChild']==1 && $data['hold']==0){
				// $this->updatechildProductQty($data['products'][$i]['sku'],$data['products'][$i]['quantity']);
			}
			$tax_class_id 	  = $data['products'][$i]['tax_class_id'];
    	    $total        	  = $data['products'][$i]['total'];
    	    $product_sku_cost = $data['products'][$i]['sku_cost'];
    	    $product_sku_avg  = $data['products'][$i]['sku_avg'];
    	    $data['remarks']  = $data['remarks']  == '' ? 0 : $data['remarks'];
    	    $product_sku_cost = $product_sku_cost == '' ? 0 : $product_sku_cost;
    	    $product_sku_avg  = $product_sku_avg  == '' ? 0 : $product_sku_avg;
    	                                               
		    $sqlsales= "INSERT INTO " . DB_PREFIX . "sales_detail (location_code,invoice_no,sku,product_id,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id) VALUES(
				  '" .$data['location_code']. "',
				  '" . $data['transaction_no'] . "',
				  '" . $sku. "',
				  '" . $data['products'][$i]['product_id'] . "',
				  '" . $this->db->escape($data['products'][$i]['name']) . "',
				  '" . $data['products'][$i]['quantity'] . "',
				  '" . $data['products'][$i]['price'] . "',
				  '" . $data['products'][$i]['total']. "',
				  '" . $data['products'][$i]['purchase_discount'] . "',
				  '" . $data['products'][$i]['tax_price']. "',
				  '" . $data['products'][$i]['net_price'] . "',
				  '" . $data['round_off'] . "',
				  '" .  $total . "',
				  '" . $data['remarks'] . "',
				  '" . $product_sku_cost. "',
				  '" . $product_sku_avg. "',
				  '" . $userName . "',
				  curdate(),
				  '" . $userName . "',
				  curdate(),
				  '" . $discount_percentage."',
				  '" . $discount_price."',
				  '" . $tax_class_id."' )";
//		   		 echo $sqlsales; die;
                  $res = $this->db->queryNew($sqlsales);

			   if(!$res){
			   		$delSql = "DELETE " . DB_PREFIX . "sales_detail where invoice_no='".$data['transaction_no']."'";
					$this->db->query($delSql);
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales_return/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);
					exit;
			   }
		}
		$purchaseId = $this->db->getLastId();
		return $purchaseId;
	}
	public function editSalesnew($purchaseId, $data)
	{
        $companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
			$gst='0';
           foreach ($data['totals'] as $total) {
				if($total['code']=='sub_total'){
					$sub_total = $total['value'];
				}else if($total['code']=='tax'){
					$gst = $total['value'];
				}else if($total['code']=='discount'){
					$discount = $total['value'];
				}else if($total['code']=='total'){
					$data['total'] = $total = $total['value'];
				}
		   }

		   $transaction_date       	 = changeDates($data['transaction_date']);
		   $data['header_remarks'] 	 = changeDates($data['reference_date']);
		   $sales_header 			 = $this->getSalesByinvNonew($data['transaction_no']);
		   $data['conversion_rate']  = $sales_header['conversion_rate'];
		   $data['currency_code'] 	 = $sales_header['currency_code'];
 		   if($data['currency_code'] == 'SGD'){
				$data['conversion_rate'] = '1';
			}
		   // printArray($data); die;
			$data['fc_subtotal'] 	  = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      	  = $data['conversion_rate'] * $gst;
			$data['fc_discount'] 	  = $data['conversion_rate'] * $discount;
			$data['fc_nettotal'] 	  = $data['conversion_rate'] * $data['total'];
		   
		  $sqlheader="UPDATE " . DB_PREFIX . "sales_header SET
				location_code     = '".$data['location_code'] . "'
				, invoice_date    = '".$transaction_date . "'
				, customer_code   = '".$data['vendor'] . "'
				, sub_total       = '".$data['fc_subtotal'] . "'
				, discount        = '".$data['fc_discount']. "'
				, gst             = '".$data['fc_tax'] . "'
				, actual_total    = '".$sub_total . "'
				, round_off       = '0.00'
				, net_total       = '".$data['fc_nettotal'] . "'
				, discount_type   = 'Sales'
				, hold 			  = '".$data['hold'] . "'
				, reference_no 	  = '".$data['reference_no'] . "'
				, header_remarks  = '".$data['header_remarks'] . "'
				, modifiedby 	  = '".$userName . "'
				, modifiedon 	  = '".date('Y-m-d H:i:s')."'
				, term_id 		  = '".$data['term_id']."'
				, shipping_id 	  = '".$data['shipping_id']."'
				, bill_discount_percentage 	= '".$data['bill_discount_percentage']."'
				, bill_discount_price 		= '".$data['bill_discount_price']."'
				, fc_subtotal     = '".$sub_total."'
				, fc_discount     = '".$discount."'
				, fc_tax          = '".$gst."'
				, fc_nettotal     = '".$data['total']."'
				, remarks     	  = '".$data['remarks']."'
				WHERE invoice_no  = '".$data['transaction_no']. "'";
				
			    $this->db->query($sqlheader); 

         /*sales details update*/ 
        // $this->db->query("DELETE FROM " . DB_PREFIX . "sales_detail WHERE invoice_no = '" . $data['transaction_no'] . "'"); 
			  
		for ($i=0; $i <count($data['products']) ; $i++) {
    	    $data['round_off']='0.00';
    	    if(empty($data['products'][$i]['purchase_discount'])) {
			   $data['products'][$i]['purchase_discount'] = '0.00';
		    }
			$discount_percentage = '';
			$discount_price      = '';
			if ($data['products'][$i]['purchase_discount_mode'] == 1) {
				$discount_percentage = $data['products'][$i]['purchase_discount_value'];
			} elseif ($data['products'][$i]['purchase_discount_mode'] == 2) {
				$discount_price      = $data['products'][$i]['purchase_discount_value'];
			}
			if($data['hold']=='0'){
				$location_code = $this->session->data['location_code'];
				$sku = $this->getSkuByProductId($data['products'][$i]['product_id']);
				$avgCostupdate = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$data['products'][$i]['quantity']. "',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $sku. "' AND location_Code='".$location_code."'";
				$this->db->query($avgCostupdate);
			}
			if($data['products'][$i]['hasChild']==1 && $data['hold']==0){
				$this->updatechildProductQty($data['products'][$i]['sku'],$data['products'][$i]['quantity']);
			}
			$tax_class_id = $data['products'][$i]['tax_class_id'];  
    	    $existProduct = $this->checkSalesReturnDetails($data['transaction_no'],$data['products'][$i]['product_id']);
			// printArray($existProduct); die;
			if($existProduct){
				$this->updateProductQty($data['products'][$i]['product_id'],$existProduct['qty'],$data['products'][$i]['quantity']);
				$sql = "UPDATE ".DB_PREFIX."sales_detail set 
								qty='".$data['products'][$i]['quantity']."',
								sku_price='".$data['products'][$i]['price']."',
								sub_total='".$data['products'][$i]['total']."',
								gst='".$data['products'][$i]['tax_price']."',
								actual_total='".$data['products'][$i]['total']."',
								net_total='".$data['products'][$i]['net_price']."',
								remarks='".$data['products'][$i]['remarks']."'";
				$res = $this->db->query($sql);
			
			}else{
	    	    // $total    =$data['products'][$i]['total']+$data['products'][$i]['tax_price'];
			    $sqlsales = "INSERT INTO " . DB_PREFIX . "sales_detail (location_code,invoice_no,sku,product_id,description,qty,sku_price,sub_total,discount,gst,actual_total,round_off,net_total,remarks,sku_cost,sku_avg,createdby,createdon,modifiedby,modifiedon,discount_percentage,discount_price,tax_class_id) VALUES('" .$data['location_code']. "', '" . $data['transaction_no'] . "','".$sku."','" . $data['products'][$i]['product_id'] . "', '" . $data['products'][$i]['name'] . "', '" . $data['products'][$i]['quantity'] . "', '" . $data['products'][$i]['price'] . "', '" . $data['products'][$i]['total']. "', '" . $data['products'][$i]['purchase_discount'] . "', '" . $data['products'][$i]['tax_price']. "', '" . $data['products'][$i]['net_price'] . "', '" . $data['round_off'] . "', '" .$data['products'][$i]['total']. "', '" . $data['remarks'] . "', '" . $data['products'][$i]['sku_cost'] . "', '" . $data['products'][$i]['sku_avg'] . "', '" . $userName . "', curdate(), '" . $userName . "', curdate(), '" . $discount_percentage. "', '" . $discount_price. "', '" . $tax_class_id. "' )";
	            $res = $this->db->queryNew($sqlsales);
			}
		   
			   if(!$res){
			   		$delSql = "DELETE " . DB_PREFIX . "sales_detail where invoice_no='".$data['transaction_no']."'";
					$this->db->query($delSql);
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/sales/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);
					exit;
			   }
		}
    
		return $purchaseId;

	}
	public function checkSalesReturnDetails($invoice_no,$product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where invoice_no='".$invoice_no."' AND product_id='".$product_id."' ")->row;
	}
	public function updateProductQty($product_id,$oldQty,$newQty){
		if($oldQty > $newQty){
			$qty = $oldQty - $newQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'".$qty."' WHERE product_id = '".$product_id."' ");
		}else{
			$qty = $newQty - $oldQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'".$qty."' WHERE product_id = '".$product_id."' ");
		}
	}
	public function addPurchase($data) {

		$companyId	= $this->session->data['company_id'];
		$userId	    = $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "purchase SET
			company_id = '" . (int)$companyId . "'
			, transaction_no = '" . $data['transaction_no'] . "'
			, transaction_date = '" . $data['transaction_date'] . "'
			, transaction_type = 'SALINV'
			, vendor_id = '" . $data['vendor'] . "'
			, reference_no = '" . $data['reference_no'] . "'
			, reference_date = '" . $data['reference_date'] . "'
			, remarks = '" . $data['remarks'] . "'
			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'
			, bill_discount_price = '" . $data['bill_discount_price'] . "'
			, total = '" . $data['total'] . "'
			, hold = '" . $data['hold'] . "'
			, purchase_return = '" . $data['purchase_return'] . "'
			, created_by = '" . (int)$userId . "'
			, created_date = curdate()");
		$purchaseId = $this->db->getLastId();

		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = '';
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product SET
					purchase_id           = '" . (int)$purchaseId . "'
					, product_id          = '" . (int)$product['product_id'] . "'
					, quantity            = '" . (int)$product['quantity'] . "'
					, price               = '" . (float)$product['price'] . "'
					, discount_percentage = '" . $discount_percentage . "'
					, discount_price      = '" . $discount_price . "'
					, tax_class_id        = '" . (int)$product['tax_class_id'] . "'
					, weight_class_id     = '" . (int)$product['weight_class_id'] . "'
					, net_price           = '" . (float)$product['net_price'] . "'
					, tax_price           = '" . (float)$product['tax_price'] . "'
					, total               = '" . (float)$product['total'] . "'");
		}

		foreach ($data['totals'] as $total) {

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total SET

				purchase_id = '" . (int)$purchaseId . "'

				, code = '" . $this->db->escape($total['code']) . "'

				, title = '" . $this->db->escape($total['title']) . "'

				, text = '" . $this->db->escape($total['text']) . "'

				, `value` = '" . (float)$total['value'] . "'

				, sort_order = '" . (int)$total['sort_order'] . "'");
		}
		return $purchaseId;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}
	public function updateInvoiceNumber($id){

		$lastId = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key`='config_next_sales_invoiceno' ")->row;

		$update = $this->db->query("UPDATE ".DB_PREFIX."sales_header set invoice_nom = '".$lastId['value']."' where invoice_no='".$id."' ");
		if($update){
			$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."setting set value = value+1 WHERE `key`='config_next_sales_invoiceno' ");
		}
		return $lastId['value'];
	}
	public function getSalesnew($purchaseId)
	{
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" .$purchaseId . "'");
		return $query->row;
	}
    public function getSalesProductnew($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_detail WHERE invoice_no = '" .$purchaseId . "'");
		return $query->rows;
	}
	public function getB2BCustomers()
	{
	  $query = $this->db->query("SELECT tax_allow,tax_type,customercode,name FROM " . DB_PREFIX . "customers WHERE customercode !='' order by name asc");
	  return $query->rows;
	}
	public function getPurchase($purchaseId) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}

	public function getPurchaseProduct($purchaseId) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");

		return $query->rows;

	}

	public function editPurchase($purchaseId, $data) {

		$this->db->query("UPDATE " . DB_PREFIX . "purchase SET
			total = '" . $data['total'] . "'
			, date_modified = curdate()
			WHERE purchase_id = '" . (int)$purchaseId . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");

		foreach ($data['products'] as $product) {

			$discount_percentage = '';
			$discount_price      = '';
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}

			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,quantity,price,discount_percentage,discount_price,tax_class_id,weight_class_id,net_price,tax_price,total) VALUES(" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['quantity'] . "','" . (float)$product['price'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . (int)$product['weight_class_id'] . "','" . (float)$product['net_price'] . "','" . (float)$product['tax_price'] . "','" . (float)$product['total'] . "')");		

		    }

		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_total WHERE purchase_id = '" . (int)$purchaseId . "'");
		foreach ($data['totals'] as $total) {
				if(empty($total['value'])) {
					 $total['value'] = '0.00';
			}
				$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total (purchase_id,code,title,text,value,sort_order) VALUES('" . (int)$purchaseId . "','" . $this->db->escape($total['code']) . "','" . $this->db->escape($total['title']) . "','" . $this->db->escape($total['text']) . "','" . $total['value'] . "','" . (int)$total['sort_order'] . "')");
		}
		return $purchaseId;
	}

	public function getPurchaseByTransNo($transNumber) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND transaction_type='SALINV' AND purchase_return = '0'");
		return $query->row;
	}
	public function getInventoryDetailsById($productId) {
		$companyId	= $this->session->data['company_id'];
		if(!empty($productId)) {
			$query = $this->db->query("SELECT p.sku, pd.name FROM " . DB_PREFIX . "product p
			LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
			LEFT JOIN " . DB_PREFIX . "product_to_company pc ON (p.product_id = pc.product_id)
			WHERE pc.company_id = '" . (int)$companyId . "' AND p.product_id = '" . (int)$productId . "'");
		}
		return $query->row;
	}
	public function deletePurchase($transaction_no){
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set deleted='1' where invoice_no='".$transaction_no."'");
	}
	public function updatechildProductQty($product_id,$qty){
		$child_product = $this->db->query("SELECT child_sku FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ")->rows;
		foreach ($child_product as $value) {
			$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$qty."' WHERE sku = '".$value['child_sku']."' ");
		}
	}
	public function getCurrency() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE company_id = '".$company_id."'");
		return $query->rows;
	}
	public function getSalesByinvNonew($invoiceNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_header WHERE invoice_no = '" . $invoiceNumber . "'");
		return $query->row;
	}
	public function getNetworkName($network_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where id='".$network_id."'")->row['name'];
	}
}
?>