	<?php
class ModelTransactionPurchaseReturn extends Model {

	public function getTotalPurchase($data) {

	    $company_id	= $this->session->data['company_id'];
      $sql = "SELECT  purchase_id FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$company_id . "' AND purchase_return = '1' AND transaction_type='PURINV' and deleted!='1' ";
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			//$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
			$sql .= " AND (transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY purchase_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->num_rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_code!= '' order by vendor_name asc");
		return $query->rows;
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' ";
		// printArray($sql);die;
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseList($data) {
	    $company_id	= $this->session->data['company_id'];
	    if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE purchase_return = '1' AND transaction_type='PURINV'";
		$sql = "SELECT * FROM " . DB_PREFIX . "purchase WHERE purchase_return = '1' AND transaction_type='PURINV' AND deleted ='0' ";

		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'"; 
		}
		if($data['filter_transactionno']){
			//$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
			$sql .= " AND (transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$sql .= " ORDER BY purchase_id DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function addPurchase($data) {
		
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$data['reference_date']   = changeDates($data['reference_date']);
		$data['transaction_date'] = changeDates($data['transaction_date']);
         
        if(empty($data['bill_discount_price'])) {
            $data['bill_discount_price'] = '0.00';
        }
		$total = 0;
		$gst   = 0;
		$sub_total =0;
		$discount_value = 0;
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}
		}
		if($data['currency_code']=='SGD'){
			$data['conversion_rate'] = '1';
		}

		$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      = $data['conversion_rate'] * $gst;
		$data['fc_discount'] = $data['conversion_rate'] * $discount;
		$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];

		$sql = "INSERT INTO " . DB_PREFIX . "purchase (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,total,purchase_return,location_code,sub_total,gst,created_by,modified_by,discount,tax_class_id,tax_type,term_id,currency_code,conversion_rate, fc_subtotal,fc_discount,fc_tax,fc_nettotal) 
			VALUES('" . (int)$companyId . "',
				   '" . $data['transaction_no'] . "',
				   '" . $data['transaction_date'] . "',
				       'PURINV',
				   '" . $data['vendor'] . "',
				   '" . $data['reference_no'] . "',
				   '" . $data['reference_date'] . "',
				   '" . $data['remarks'] . "',
				   '" . $data['bill_discount_percentage'] . "',
				   '" . $data['bill_discount_price'] . "',
				   '" . $data['fc_nettotal']. "',
				   '" . $data['purchase_return']."',
				   '" . $data['location_code']."',
				   '" . $data['fc_subtotal']. "',
				   '" . $data['fc_tax']. "',
				   '" .$userName . "',
					'".$userName."',
					'".$data['fc_discount']."',
					'".$data['tax_class_id']."',
					'".$data['tax_type']."',
					'".$data['term_id']."',
					'".$data['currency_code']."',
					'".$data['conversion_rate']."',
					'".$sub_total."',
					'".$discount."',
					'".$gst."',
					'".$data['total']."')";

			$res = $this->db->queryNew($sql);

		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase_return/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);exit;
		}
		$purchaseId = $this->db->getLastId();

		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = '';
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}
			if($product['hasChild']==1 && $data['hold']==0){
				$child['product_id']    = $product['product_id'];
				$child['qty'] 			= $product['quantity'];
				$child['location_code'] = $data['location_code'];
				$this->cart->updatechildProductQty($child,'S');
			}

			$pqty  		   = $product['quantity'];
			$pSku 		   = $this->getSkuByProductId($product['product_id']);			
			$location_code = $data['location_code'];
			$avgCostupdate = "UPDATE ".DB_PREFIX."product_stock SET sku_qty=sku_qty-'" .$pqty. "', modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '".$pSku."' AND location_Code='".$location_code."'";

			$avg_method ='WAM';
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
			    $product['net_price'] = $product['total'] + $product['tax_price'];
			}
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
			    $product['total'] = $product['total'] - $product['tax_price'];
			}
			
			$res = $this->db->queryNew("INSERT INTO ".DB_PREFIX."purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage, discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,transaction_no,sku,foc,conversion_rate) VALUES ('".(int)$purchaseId."','".(int)$product['product_id']."','".(int)$product['weight_class_id']."','".(int)$product['quantity']."','".$product['price']."','".$product['raw_cost']."','".$discount_percentage."','".$discount_price."','".(int)$product['tax_class_id']."','".$product['net_price']."','".$product['tax_price']."','".$product['total']."','".$avg_price."','".$avg_method."','".$data['transaction_no']."','".$pSku."','".$product['foc_quantity']."','".$data['conversion_rate']."')");

		 		if(!$res){
			   		$this->db->query("DELETE " . DB_PREFIX . "purchase where purchase_id='".$purchaseId."'");
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase_return/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);			
					exit;
			    }else{
			   		$this->db->query($avgCostupdate);
			    }
		}
		return $purchaseId;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}
	public function getPurchase($purchaseId) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' "); //AND hold = '1'
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId,$product_id='') {
		if($product_id){
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "' AND product_id = '" . (int)$product_id . "'");
		}else{
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		}
		return $query->rows;
	}
	public function editPurchase($purchaseId, $data) {

			$data['reference_date']   = changeDate($data['reference_date']);
			$data['transaction_date'] = changeDate($data['transaction_date']);

			$userId = $this->session->data['user_id'];
			$userName = $this->session->data['username'];
			$purchase_return = 0;
			$total = 0;
			$gst   = 0;
			$sub_total =0;
			foreach ($data['totals'] as $total) {
				if($total['code']=='sub_total'){
					$sub_total = $total['value'];
				}else if($total['code']=='tax'){
					$gst = $total['value'];
				}else if($total['code']=='discount'){
					$discount = $total['value'];
				}else if($total['code']=='total'){
					$data['total'] = $total = $total['value'];
				}
			}
			$purchase_header = $this->getPurchaseHeader($purchaseId);
			$data['conversion_rate'] = $purchase_header['conversion_rate'];
			
			$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      = $data['conversion_rate'] * $gst;
			$data['fc_discount'] = $data['conversion_rate'] * $discount;
			$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];

			$this->db->query("UPDATE " . DB_PREFIX . "purchase SET
			transaction_no = '" . $data['transaction_no'] . "'
			, transaction_date = '" . $data['transaction_date'] . "'
			, transaction_type = 'PURINV'
			, vendor_id = '" . $data['vendor'] . "'
			, reference_no = '" . $data['reference_no'] . "'
			, reference_date = '" . $data['reference_date'] . "'
			, remarks = '" . $data['remarks'] . "'
			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'
			, bill_discount_price = '" . $data['bill_discount_price'] . "'
			, total = '" . $data['fc_nettotal'] . "'
			, sub_total = '" . $data['fc_subtotal'] . "'
			, gst = '" . $data['fc_tax'] . "'
			, discount = '" . $data['fc_discount'] . "'
			, hold = '" . $data['hold'] . "'
			, location_code = '".$data['location_code']."'
			, tax_class_id = '".$data['tax_class_id']."'
			, tax_type = '".$data['tax_type']."'
			, fc_subtotal = '".$sub_total."'
			, fc_tax = '".$gst."'
			, fc_discount = '".$discount."'
			, fc_nettotal = '".$data['total']."'
			, term_id = '".$data['term_id']."'
			, modified_by = '".$userName."'
			, date_modified = '".date('Y-m-d H:i:s')."'
			WHERE purchase_id = '" . (int)$purchaseId . "'");

		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = '';

			if ($product['purchase_discount_mode'] == 1) {
			   $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price = $product['purchase_discount_value'];
			}

			$pqty  = (int)$product['quantity'];
			$price = (float)$product['price'];
			$unit_cost = $price;

			$pSku = $this->getSkuByProductId($product['product_id']);
			$location_code = $data['location_code'];
			if(empty($product['foc_quantity'])){
				$product['foc_quantity'] = 0;
			}else{
				$pqty = $pqty+$product['foc_quantity'];
			}
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
			    $product['net_price'] = $product['total'] + $product['tax_price'];
			}				
			if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
			    $product['total'] = $product['total'] - $product['tax_price'];
			}
		$existProduct = $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchaseId."' AND product_id='".$product['product_id']."' ")->row;
		if(!empty($existProduct)){

			$this->updateProductQtyForPurchase($product['product_id'],$existProduct['quantity'],$product['quantity']);
			
			$updateSql = "UPDATE ".DB_PREFIX."purchase_to_product set weight_class_id ='". (int)$product['weight_class_id']."', quantity 		='". $product['quantity']."', price 			='". $product['price']."', raw_cost 		='". $product['raw_cost']."', tax_class_id 	='". $product['tax_class_id']."', net_price 		='". $product['net_price']."', tax_price 		='". $product['tax_price']."', total 		  	='". $product['total']."', avg_cost 		='". $avg_price."', avg_method 		='". $avg_method."', foc 			='". $product['foc_quantity']."', transaction_no 	='". $data['transaction_no']."', conversion_rate ='". $data['conversion_rate']."'where purchase_id='".$purchaseId."' AND product_id='".$product['product_id']."'"; 
			$this->db->query($updateSql);
		
		}else{

			$this->db->queryNew("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage, discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,transaction_no,sku,conversion_rate) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . $product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "',".$product['foc_quantity'].",'".$data['transaction_no']."','".$pSku."','".$data['conversion_rate']."')"); 
		}
		}
		return $purchaseId;
	}

	public function updateProductQtyForPurchase($product_id,$oldQty,$newQty){
		if($oldQty > $newQty){
			$qty = $oldQty - $newQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'".$qty."' WHERE product_id = '".$product_id."' ");
		}else{
			$qty = $newQty - $oldQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'".$qty."' WHERE product_id = '".$product_id."' ");
		}
	}
	public function getPurchaseByTransNo($transNumber) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return ='0'");
		return $query->row;
	}
	public function getInventoryDetailsById($productId) {
		$companyId	= $this->session->data['company_id'];
		if(!empty($productId)) {
			$query = $this->db->query("SELECT p.sku, p.name,ps.sku_qty as quantity FROM " . DB_PREFIX . "product p
									   LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku=ps.sku) WHERE p.product_id = '" . (int)$productId . "'");
		}
		return $query->row;
	}


	public function getPurchaseDetailsByKey($key)
	{
		$companyId	= $this->session->data['company_id'];
		$sql = "SELECT transaction_no FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_type = 'PURINV' AND purchase_return = '0' AND hold = '0' AND transaction_no LIKE '%".$key."%'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getPurchaseDetails($purchaseId){
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(quantity) as totalQty FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '".$purchaseId."'");
		return $query->row;
	}
	public function getProductDetailsById($purchaseId){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT PP.purchase_product_id, P.product_id,PP.quantity,PP.foc,PP.price,PP.total,PP.avg_cost,P.sku,P.name,PP.tax_price FROM " . DB_PREFIX . "purchase_to_product as PP
			LEFT JOIN " . DB_PREFIX . "product as P ON P.product_id = PP.product_id
			WHERE PP.purchase_id = '".$purchaseId."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT p.*,tc.title as tax_title FROM ".DB_PREFIX."purchase as p left join ".DB_PREFIX."tax_class as tc ON p.tax_class_id=tc.tax_class_id WHERE p.company_id = '" . (int)$companyId . "' AND p.purchase_id = '" . (int)$purchaseId . "'");
		return $query->row;
	}
	
	public function getProductByName($data = array()) 
	{
		$company_id	= $this->session->data['company_id'];
		$location   = $this->session->data['location_code'];
		$sql 		= "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,ps.location_code FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_stock ps ON (p.sku = ps.sku) WHERE p.product_id !='0' AND ps.location_code='".$location."' ";

		if(!empty($data['filter_name'])) {
			$data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']);

			$sql .= " AND ( p.sku_shortdescription LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%') ";
		}
		$sql .=" GROUP BY p.product_id";
		// echo $sql;
		return $this->db->query($sql)->rows;
	}
	public function getProductByNamexx($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		if($data['filter_name']){
		   // $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";
		if($data['filter_location_code']){              //newly added
			$sql.=" AND ps.location_Code = '".$data['filter_location_code']."'";
		}

		if(!empty($data['filter_name'])) {
			// $sql .= " p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR ";
			$sql .= " AND (p.sku_shortdescription LIKE '%".$this->db->escape($data['filter_name'])."%' OR";
			$sql .= " p.name LIKE '%".$this->db->escape($data['filter_name'])."%' OR";
			$sql .= " LOWER(p.sku) LIKE '%".$this->db->escape(utf8_strtolower($data['filter_name']))."%')";
		}
		$sql.=" GROUP BY p.product_id";
		//echo $sql;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getVendorsTaxId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tax_method FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['tax_method'];
	}
	public function getTotalPurchaseList($data = array()) {
	    $company_id	= $this->session->data['company_id'];
	    
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "purchase WHERE  purchase_return = '1' AND transaction_type='PURINV'  AND deleted='0' ";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no = '" . $data['filter_transactionno'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getTotalPurchaseListNew($data = array()) {
	    $company_id	= $this->session->data['company_id'];
	    
		$sql = "SELECT count(*) as total, SUM(total) AS grandtotal FROM " . DB_PREFIX . "purchase where purchase_return = '1' AND transaction_type='PURINV' AND hold ='' ";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		// echo $sql; die;
		
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getVendors_byId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row;
	}
	public function deletePurchase($purchase_id){
		$this->db->query("UPDATE ".DB_PREFIX."purchase set deleted='1' where purchase_id='".$purchase_id."'");
	}
	public function updatechildProductQty($product_id,$qty){ // not used
		$child_product = $this->db->query("SELECT child_sku FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ")->rows;
		foreach ($child_product as $value) {
			$update = $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$qty."' WHERE sku = '".$value['child_sku']."' ");
		}
	}
	public function getPurchaseReturnList($data){
		
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT P.* FROM " . DB_PREFIX . "purchase as P 
			LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_id=P.vendor_id
			LEFT JOIN " . DB_PREFIX . "user as U on U.user_id=P.created_by";

		$sql.=" WHERE P.purchase_return ='1' AND deleted='0' ";
		
		if($data['filter_supplier']){
			$sql .= " AND P.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND P.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			//$sql .= " AND P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
			$sql .= " AND (P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR P.reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to']))); 
			if($data['filter_reference_date']){
				$sql .= " AND P.reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND P.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.purchase_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getCurrency() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE company_id = '".$company_id."'");
		return $query->rows;
	}
	public function getcompanyCurrency(){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."' ")->row;
		return $query['currency_code'];	
	}
	public function getPurchaseHeader($purchaseId)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase WHERE purchase_id = '".$purchaseId."' ")->row;
	}
}
?>