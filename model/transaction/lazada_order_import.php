<?php
class ModelTransactionLazadaOrderImport extends Model {

	public function getLazadaOrders(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."lazada_order_details order by OrderNumber asc ")->rows;
	}
	public function getCustomerDetails($custCode){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where customercode ='".$custCode."' ")->row;
	}
	public function getProduct($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_networks where product_sku ='".$sku."' AND product_id !=''")->row;
	}
	public function getLazadaNetworkId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where name ='Lazada' AND status ='Active'")->row;
	}
	public function updateLazadaProduct($data){
		$network_id = $this->getLazadaNetworkId()['id'];
		return $this->db->query("INSERT INTO ".DB_PREFIX."product_networks (network_id,product_id,product_sku,price,	quantity,status,network_response_id) VALUES('".$network_id."','".$this->db->escape($data['product_id'])."','".$this->db->escape($data['sku'])."','0','0','Active','0') ");
	}
	public function deleteAllData(){
		$this->db->query("TRUNCATE ".DB_PREFIX."lazada_order_details");
	}
	public function checkCustomerByPhone($phone){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where mobile ='".$phone."' ")->row;
	}
	public function getShippingDetails($cust_code){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id ='".$cust_code."' AND isdefault='1'")->row;
	}
	public function createNewCustomer($data){
		$company_id = $this->session->data['company_id'];
		$address2   = $data[27].' '.$data[28];
        $contact    = $data[19] ? $data[19] : $data[20];

		$sql = "INSERT INTO ".DB_PREFIX."customers (cust_code, company_id, status, name, location_code, mobile, address1, address2, city, zipcode, email, address3, currency_code, tax_allow, country,network_response_id, createdon) 
			VALUES ('".$data['cust_code']."', '".$company_id."', '1', '".$this->db->escape($data[10])."', 'HQ', '".$this->db->escape($contact)."', '".$this->db->escape($data[26])."', '".$this->db->escape($address2)."', '".$this->db->escape($data[33])."', '".$this->db->escape($data[34])."', '".$this->db->escape($data[11])."', '1', 'SGD', '0', '1','', '".date('Y-m-d H:i:s')."')";

		$this->db->query($sql);
		$customer_id = $this->db->getLastId();

		if($customer_id !=''){
			$address1 = $data[14];
			$address2 = $data[15];

			$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,address2,city,country,zip,contact_no,notes,isdefault,name) VALUES('".$customer_id."','SH".$customer_id."','".$this->db->escape($address1)."','".$this->db->escape($address2)."','".$this->db->escape($data[21])."','".$this->db->escape($data[23])."','".$this->db->escape($data[22])."','".$this->db->escape($contact)."','".$this->db->escape($data[12])."','1', '".$this->db->escape($data[13])."') ";
			
			$this->db->query($sql);
			$shipping_id = $this->db->getLastId();
			$res 	= array('shipping_id' => $shipping_id, 'customercode' => $customer_id);
			return $res;
		
		}else{
			return false;
		}
	}
	public function insertTempTable($data){

		$sql = "INSERT INTO `tbl_lazada_order_details` (`OrderItemId`, `OrderType`, `OrderFlag`, `LazadaId`, `SellerSKU`, `LazadaSKU`, `Createdat`, `Updatedat`, `OrderNumber`, `InvoiceRequired`, `CustomerName`, `CustomerEmail`, `NationalRegistrationNumber`, `ShippingName`, `ShippingAddress`, `ShippingAddress2`, `ShippingAddress3`, `ShippingAddress4`, `ShippingAddress5`, `ShippingPhoneNumber`, `ShippingPhoneNumber2`, `ShippingCity`, `ShippingPostcode`, `ShippingCountry`, `ShippingRegion`, `BillingName`, `BillingAddress`, `BillingAddress2`, `BillingAddress3`, `BillingAddress4`, `BillingAddress5`, `BillingPhoneNumber`, `BillingPhoneNumber2`, `BillingCity`, `BillingPostcode`, `BillingCountry`, `PaymentMethod`, `PaidPrice`, `UnitPrice`, `ShippingFee`, `WalletCredits`, `ItemName`, `Variation`, `CDShippingProvider`, `ShippingProvider`, `ShipmentTypeName`, `ShippingProviderType`, `CDTrackingCode`, `TrackingCode`, `TrackingURL`, `ShippingProviderFirst`, `TrackingCodeFirst`, `TrackingURLFirst`, `Promisedshipping`, `Premium`, `Status`, `Cancel_ReturnInitiator`, `Reason`, `ReasonDetail`, `Editor`, `BundleID`, `BundleDiscount`, `RefundAmount`, `customercode`, `shipping_id`, `product_id`,`orderExist`,`sales_channel`) VALUES ('".$this->db->escape($data[0])."','".$this->db->escape($data[1])."','".$this->db->escape($data[2])."','".$this->db->escape($data[2])."','".$this->db->escape($data[4])."','".$this->db->escape($data[5])."','".$this->db->escape($data[6])."','".$this->db->escape($data[7])."','".$this->db->escape($data[8])."','".$this->db->escape($data[9])."','".$this->db->escape($data[10])."','".$this->db->escape($data[11])."','".$this->db->escape($data[12])."','".$this->db->escape($data[13])."','".$this->db->escape($data[14])."','".$this->db->escape($data[15])."','".$this->db->escape($data[16])."','".$this->db->escape($data[17])."','".$this->db->escape($data[18])."','".$this->db->escape($data[19])."','".$this->db->escape($data[20])."','".$this->db->escape($data[21])."','".$this->db->escape($data[22])."','".$this->db->escape($data[23])."','".$this->db->escape($data[24])."','".$this->db->escape($data[25])."','".$this->db->escape($data[26])."','".$this->db->escape($data[27])."','".$this->db->escape($data[28])."','".$this->db->escape($data[29])."','".$this->db->escape($data[30])."','".$this->db->escape($data[31])."','".$this->db->escape($data[32])."','".$this->db->escape($data[33])."','".$this->db->escape($data[34])."','".$this->db->escape($data[35])."','".$this->db->escape($data[36])."','".$this->db->escape($data[37])."','".$this->db->escape($data[38])."','".$this->db->escape($data[39])."','".$this->db->escape($data[40])."','".$this->db->escape($data[41])."','".$this->db->escape($data[42])."','".$this->db->escape($data[43])."','".$this->db->escape($data[44])."','".$this->db->escape($data[45])."','".$this->db->escape($data[46])."','".$this->db->escape($data[47])."','".$this->db->escape($data[48])."','".$this->db->escape($data[49])."','".$this->db->escape($data[50])."','".$this->db->escape($data[51])."','".$this->db->escape($data[52])."','".$this->db->escape($data[53])."','".$this->db->escape($data[54])."','".$this->db->escape($data[55])."','".$this->db->escape($data[56])."','".$this->db->escape($data[57])."','".$this->db->escape($data[58])."','".$this->db->escape($data[59])."','".$this->db->escape($data[60])."','".$this->db->escape($data[61])."','".$this->db->escape($data[62])."','".$data['customer_id']."','".$data['shipping_id']."','".$data['product_id']."','".$data['exist']."','".$data['sales_channel']."')";
        
		$this->db->query($sql);
	}

	public function insertTempTableType2($data){

		$sql = "INSERT INTO `tbl_lazada_order_details` (
		`OrderItemId`,`OrderType`,`OrderFlag`,`LazadaId`,`SellerSKU`,`LazadaSKU`,`Createdat`,`Updatedat`,`OrderNumber`,`InvoiceRequired`,`CustomerName`,`CustomerEmail`,`NationalRegistrationNumber`,`ShippingName`,`ShippingAddress`,`ShippingAddress2`,`ShippingAddress3`,`ShippingAddress4`,`ShippingAddress5`,`ShippingPhoneNumber`,`ShippingPhoneNumber2`,`ShippingCity`,`ShippingPostcode`,`ShippingCountry`,`ShippingRegion`,`BillingName`,`BillingAddress`,`BillingAddress2`,`BillingAddress3`,`BillingAddress4`,`BillingAddress5`,`BillingPhoneNumber`,`BillingPhoneNumber2`,`BillingCity`,`BillingPostcode`,`BillingCountry`,`PaymentMethod`,`PaidPrice`,`UnitPrice`,`ShippingFee`,`WalletCredits`,`ItemName`,`Variation`,`CDShippingProvider`,`ShippingProvider`,`ShipmentTypeName`,`ShippingProviderType`,`CDTrackingCode`,`TrackingCode`,`TrackingURL`,	`ShippingProviderFirst`,`TrackingCodeFirst`,`TrackingURLFirst`,`Promisedshipping`,`Premium`,`Status`,`customercode`,`shipping_id`,`product_id`,`orderExist`,`sales_channel`) VALUES ('".$this->db->escape($data[0])."','".$this->db->escape($data[1])."','".$this->db->escape($data[2])."','".$this->db->escape($data[4])."','".$this->db->escape($data[5])."','".$this->db->escape($data[6])."','".$this->db->escape($data[8])."','".$this->db->escape($data[9])."','".$this->db->escape($data[12])."','".$this->db->escape($data[13])."',	'".$this->db->escape($data[16])."','".$this->db->escape($data[17])."','".$this->db->escape($data[18])."','".$this->db->escape($data[19])."','".$this->db->escape($data[20])."','".$this->db->escape($data[21])."','".$this->db->escape($data[22])."','".$this->db->escape($data[23])."','".$this->db->escape($data[24])."','".$this->db->escape($data[25])."','".$this->db->escape($data[26])."','".$this->db->escape($data[27])."','".$this->db->escape($data[28])."','".$this->db->escape($data[29])."','".$this->db->escape($data[30])."','".$this->db->escape($data[31])."','".$this->db->escape($data[32])."','".$this->db->escape($data[33])."','".$this->db->escape($data[34])."','".$this->db->escape($data[35])."','".$this->db->escape($data[36])."','".$this->db->escape($data[37])."','".$this->db->escape($data[38])."','".$this->db->escape($data[39])."','".$this->db->escape($data[40])."','".$this->db->escape($data[41])."','".$this->db->escape($data[45])."','".$this->db->escape($data[46])."','".$this->db->escape($data[47])."','".$this->db->escape($data[49])."','".$this->db->escape($data[50])."','".$this->db->escape($data[51])."','".$this->db->escape($data[52])."','".$this->db->escape($data[53])."','".$this->db->escape($data[54])."','".$this->db->escape($data[55])."','".$this->db->escape($data[56])."','".$this->db->escape($data[57])."','".$this->db->escape($data[58])."','".$this->db->escape($data[59])."','".$this->db->escape($data[60])."','".$this->db->escape($data[61])."','".$this->db->escape($data[62])."','".$this->db->escape($data[63])."','".$this->db->escape($data[64])."','".$this->db->escape($data[65])."','".$data['customer_id']."','".$data['shipping_id']."','".$data['product_id']."','".$data['exist']."','".$data['sales_channel']."')";  
		$this->db->query($sql);
	}
	public function getDetailsFromTempTable(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."lazada_order_details GROUP BY OrderNumber")->rows;
	}
	public function checkOrderExistOrNot($ordernumber, $sales_channel){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where network_order_id='".$ordernumber."' AND network_id='".$sales_channel."' AND delivery_status !='Canceled' ")->row;
	}
	public function getSalesLastId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header ORDER BY id DESC LIMIT 1")->row['id'];
	}
	public function getTempOrderByOrderNumber($OrderNumber){
		return $this->db->query("SELECT count(id) as qty,product_id,SellerSKU,UnitPrice,customercode, shipping_id,SellerSKU, ItemName, UnitPrice,Variation FROM ".DB_PREFIX."lazada_order_details where OrderNumber='".$OrderNumber."' group by SellerSKU ")->rows;
	}
	public function getProductIdBySKU($sku){
		return $this->db->query("SELECT * from tbl_product_networks where `product_sku`='".$sku."' ORDER BY network_id ASC LIMIT 1")->row['product_id'];
	}
	public function getProductById($product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where product_id='".$product_id."' ")->row;
	}
	public function getProductBySKU($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where sku='".$sku."' ")->row;
	}
	public function addSalesOrder($data){

		$orders   = $this->getTempOrderByOrderNumber($data['OrderNumber']);
		$sales_id = 0;
		$userName	= $this->session->data['username'];
		if(!empty($orders)){
			
			$header = $orders[0]; 
			$sql = "INSERT INTO ".DB_PREFIX."sales_header (location_code,invoice_no,invoice_date,customer_code,currency_code,conversion_rate,tax_class_id,tax_type,reference_no,shipping_id,network_id,network_order_id,createdby,modifiedby) VALUES('HQ','".$data['invoice_no']."','".date('Y-m-d')."','".$this->db->escape($header['customercode'])."','SGD','1','2','1','".$this->db->escape($data['OrderNumber'])."','".$this->db->escape($data['shipping_id'])."','".$data['sales_channel']."','".$this->db->escape($data['OrderNumber'])."','".$userName."','".$userName."' )";
			$this->db->query($sql);
			$sales_id = $this->db->getLastId();
			$sub_total = 0;
			foreach ($orders as $value) {
				$product = $this->getProductBySKU($value['SellerSKU']);
				if(empty($product) && !$product['product_id']){
					$product = $this->getProductById($this->getProductIdBySKU($value['SellerSKU']));
				}
				$childAry['product_id'] = $product['product_id'];
				$childAry['order_no']   = $data['invoice_no'];
				$childAry['type']   	= 'SO';
				$this->cart->insertChildItems($childAry);

				if($product['product_id'] !=''){
					$sql = "INSERT INTO ".DB_PREFIX."sales_detail (location_code,sku,invoice_no,createdon,product_id,description,qty,sku_price,sub_total,net_total,createdby,conversion_rate,tax_class_id,discount,gst,cust_description) VALUES('HQ','".$this->db->escape($product['sku'])."','".$this->db->escape($data['invoice_no'])."','".date('Y-m-d')."','".$this->db->escape($product['product_id'])."','".$this->db->escape($product['name'])."','".$value['qty']."','".$this->db->escape($value['UnitPrice'])."','".$this->db->escape($value['UnitPrice'] * $value['qty'])."','".$this->db->escape($value['UnitPrice'] * $value['qty'])."','".$this->session->data['username']."','1','1','0','0','".$this->db->escape($value['Variation'])."') ";
					$this->db->query($sql);
					$sub_total += $value['UnitPrice'] * $value['qty'];
				}
			}
        	$net_total  = $sub_total;
			$inclusive_convert = 7 / (100 + 7);
			$tax_amount = $sub_total * $inclusive_convert;		
        	if($tax_amount){
        		$sub_total = $sub_total - $tax_amount; 
        	}
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set sub_total='".$sub_total."', actual_total='".$net_total."', net_total='".$net_total."', fc_subtotal='".$sub_total."', fc_nettotal='".$net_total."', gst='".$tax_amount."' where invoice_no='".$this->db->escape($data['invoice_no'])."' ");
		}
		if($sales_id){
			$this->deleteTempByOrderNummber($data['OrderNumber']);
		}
	}
	public function deleteTempByOrderNummber($OrderNumber){
		$this->db->query("DELETE FROM ".DB_PREFIX."lazada_order_details where OrderNumber='".$OrderNumber."' ");
	}
    public function checkOrderIdAndSku($orderId, $product_id){
        return $this->db->query("SELECT H.id FROM ".DB_PREFIX."sales_header as H LEFT JOIN ".DB_PREFIX."sales_detail as D on H.invoice_no = D.invoice_no WHERE D.product_id='".$this->db->escape($product_id)."' AND H.network_order_id='".$this->db->escape($orderId)."' AND H.delivery_status !='Canceled'")->row;
    }
    public function delete($id){
        $this->db->query("DELETE FROM ".DB_PREFIX."lazada_order_details where id='".$id."' ");
    }
    public function clearImportedOrders(){
        $this->db->query("DELETE FROM ".DB_PREFIX."lazada_order_details where orderExist='1' ");
    }

    public function checkShippingAddress($custCode, $data)
    {
    	return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id='".$custCode."' AND contact_no='".$this->db->escape($data[19])."' and address1='".$this->db->escape($data[14])."' and zip='".$this->db->escape($data[22])."' ")->row;
    }

    public function createNewShippingAddress($custCode, $data)
    {
    	$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,address2,city,country,zip,contact_no,notes) VALUES ('".$custCode."','SH".$custCode."','".$this->db->escape($data[18]).' '.$this->db->escape($data[19])."','".$this->db->escape($data[20]).' '.$this->db->escape($data[21])."','".$this->db->escape($data[26])."','".$this->db->escape($data[28])."','".$this->db->escape($data[27])."','".$this->db->escape($data[24])."','created when import lazada order at ".date('d/m/Y H:is')."') ";
    	$this->db->query($sql);
		$data['id'] = $this->db->getLastId();
    	return $data;
    }

    public function createNewShippingAddressCSV($custCode, $data)
    {
    	$this->db->query("INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,address2,city,country,zip,contact_no,notes) VALUES ('".$custCode."','SH".$custCode."','".$this->db->escape($data[14])."','".$this->db->escape($data[15])."','".$this->db->escape($data[21])."','".$this->db->escape($data[23])."','".$this->db->escape($data[22])."','".$this->db->escape($data[19])."','created when import lazada order at ".date('d/m/Y H:is')."') ");
		$data['id'] = $this->db->getLastId();
    	return $data;
    }

    public function updateShippingAddressFromInsertedByLazda()
    {
        // 10-10-2021 |^| lazada imported shipping address have name inside address column issue 
        /*$shippingIds = $this->db->query("SELECT sh.*, cu.name as cusname FROM tbl_shipping as sh left join tbl_customers as cu ON cu.customercode=sh.customer_id WHERE customer_id in (SELECT customer_code FROM `tbl_sales_header` WHERE network_id = 5) AND sh.isdefault = 1 ")->rows;

        foreach ($shippingIds as $shipping) {
            $address1 = str_replace($shipping['cusname'], '', $shipping['address1']);
            $address2 = str_replace($shipping['city'], '', $shipping['address2']);
            $address2 = str_replace($shipping['zip'], '', $address2);
            $address2 = str_replace($shipping['contact_no'], '', $address2);
            $address2 = trim($address2);
            $add2     = explode(',', $address1);
            $address2 = $add2[1] ? $address2.' '.$add2[1] : $address2;
            $address2 = str_replace($shipping['country'], '', $address2);
            $address1 = str_replace($add2[1], '', $address1);

            $this->db->query("UPDATE tbl_shipping set address1 = '".$this->db->escape($address1)."', address2='".$this->db->escape($address2)."' where id='".$shipping['id']."' ");
        }*/
    }
}
?>