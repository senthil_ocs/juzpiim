<?php
class ModelTransactionQSMorderImport extends Model {

	public function getQSMOrders(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."qsm_order_details order by OrderNo asc ")->rows;
	}
	public function getCustomerDetails($custCode){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where customercode ='".$custCode."' ")->row;
	}
	public function getProduct($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_networks where product_sku ='".$sku."' AND product_id !=''")->row;
	}
	public function getQSMNetworkId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where name ='qoo10' AND status ='Active'")->row;
	}
	public function updateQSMProduct($data){
		$network_id = $this->getQSMNetworkId()['id'];
		return $this->db->query("INSERT INTO ".DB_PREFIX."product_networks (network_id,product_id,product_sku,price,	quantity,status,network_response_id) VALUES('".$this->db->escape($network_id)."','".$this->db->escape($data['product_id'])."','".$this->db->escape($data['sku'])."','0','0','Active','0') ");
	}
	public function deleteAllData(){
		$this->db->query("TRUNCATE ".DB_PREFIX."qsm_order_details");
	}
	public function checkCustomerByPhone($phone){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where mobile ='".$phone."' ")->row;
	}
	public function getShippingDetails($cust_code){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id ='".$cust_code."' AND isdefault='1'")->row;
	}
	public function createNewCustomer($data){
		$company_id = $this->session->data['company_id'];
		$address2   = $data[27].' '.$data[28];
		$customerMobile = str_replace('-', '', $data[37]) !='' ? str_replace('-', '', $data[37]) : str_replace('-', '', $data[36]);

		$sql = "INSERT INTO ".DB_PREFIX."customers (cust_code, company_id, status, name, location_code, mobile, address1, city, zipcode, email, currency_code, tax_allow, country,network_response_id, createdon) 
			VALUES ('".$this->db->escape($data['cust_code'])."', '".$this->db->escape($company_id)."', '1', '".$this->db->escape($data[33])."', 'HQ', '".$this->db->escape($customerMobile)."', '".$this->db->escape($data[22])."', '', '".$this->db->escape($data[23])."', '', 'SGD', '0', '1','', '".date('Y-m-d H:i:s')."')";

		$this->db->query($sql);
		$customer_id = $this->db->getLastId();
        $data[23] = str_replace("'", '', $data[23]);
		if($customer_id !=''){
			$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,country,zip,contact_no,notes,isdefault, name) VALUES('".$this->db->escape($customer_id)."','SH".$this->db->escape($customer_id)."','".$this->db->escape($data[22])."','".$this->db->escape($data[24])."','".$this->db->escape($data[23])."','".$this->db->escape($customerMobile)."','created when import QSM order at ".date('d/m/Y H:is')."','1', '".$this->db->escape($data[18])."') ";
			
			$this->db->query($sql);
			$shipping_id = $this->db->getLastId();
			$res 		 = array('shipping_id' => $shipping_id, 'customercode' => $customer_id);
			return $res;
		
		}else{
			return false;
		}
	}
	public function insertTempTable($data){

		$sql = "INSERT INTO `tbl_qsm_order_details`(`ShippingStatus`, `OrderNo`, `CartNo`, `DeliveryCompany`, `TrackingNo`, `ShippingDate`, `OrderDate`, `PaymentComplete`, `DesiredShippingDate`, `EstimatedShippingDate`, `DeliveredDate`, `ShippingMethod`, `ItemCode`, `Item`, `Qty`, `Options`, `OptionCode`, `Gift`, `Recipient`, `RecipientPhonetic`, `RecipientPhoneNo`, `RecipientMobileNo`, `Address`, `PostalCode`, `Nation`, `PaymentOfShippingRate`, `PaymentNation`, `Currency`, `Payment`, `SellPrice`, `Discount`, `TotalPrice`, `SettlePrice`, `Customer`, `CustomerPhonetic`, `ShippingMemo`, `CustomerPhoneNo`, `CustomerMobileNo`, `SellerCode`, `JANCode`, `StandardNo`, `GiftSender`, `customercode`, `shipping_id`, `orderExist`) VALUES ('".$this->db->escape($data[0])."','".$this->db->escape($data[1])."','".$this->db->escape($data[2])."','".$this->db->escape($data[3])."','".$this->db->escape($data[4])."','".$this->db->escape($data[5])."','".$this->db->escape($data[6])."','".$this->db->escape($data[7])."','".$this->db->escape($data[8])."','".$this->db->escape($data[9])."','".$this->db->escape($data[10])."','".$this->db->escape($data[11])."','".$this->db->escape($data[12])."','".$this->db->escape($data[13])."','".$this->db->escape($data[14])."','".$this->db->escape($data[15])."','".$this->db->escape($data[16])."','".$this->db->escape($data[17])."','".$this->db->escape($data[18])."','".$this->db->escape($data[19])."','".$this->db->escape($data[20])."','".$this->db->escape($data[21])."','".$this->db->escape($data[22])."','".$this->db->escape($data[23])."','".$this->db->escape($data[24])."','".$this->db->escape($data[25])."','".$this->db->escape($data[26])."','".$this->db->escape($data[27])."','".$this->db->escape($data[28])."','".$this->db->escape($data[29])."','".$this->db->escape($data[30])."','".$this->db->escape($data[31])."','".$this->db->escape($data[32])."','".$this->db->escape($data[33])."','".$this->db->escape($data[34])."','".$this->db->escape($data[35])."','".$this->db->escape($data[36])."','".$this->db->escape($data[37])."','".$this->db->escape($data[38])."','".$this->db->escape($data[39])."','".$this->db->escape($data[40])."','".$this->db->escape($data[41])."','".$this->db->escape($data['customer_id'])."','".$this->db->escape($data['shipping_id'])."','".$this->db->escape($data['exist'])."')";

		$this->db->query($sql);
	}
	public function getDetailsFromTempTable(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."qsm_order_details GROUP BY OrderNo")->rows;
	}
	public function checkOrderExistOrNot($ordernumber){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where network_order_id='".$ordernumber."' AND network_id='2' AND delivery_status!='Canceled' ")->row;
	}
	public function getSalesLastId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header ORDER BY id DESC LIMIT 1")->row['id'];
	}
	public function getTempOrderByOrderNumber($OrderNo){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."qsm_order_details where OrderNo='".$OrderNo."' ")->rows;
	}
	public function getProductIdBySKU($sku){
		return $this->db->query("SELECT * from ".DB_PREFIX."product_networks where `product_sku`='".$sku."' ORDER BY network_id ASC LIMIT 1")->row['product_id'];
	}
	public function getProductById($product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where product_id='".$product_id."' ")->row;
	}
	public function getProductBySKU($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where sku='".$sku."' ")->row;
	}
	public function addSalesOrder($data){

		$orders   = $this->getTempOrderByOrderNumber($data['OrderNo']);
		$sales_id = 0;
		$userName = $this->session->data['username'];
		if(!empty($orders)){

			$sql = "INSERT INTO ".DB_PREFIX."sales_header (location_code,invoice_no,invoice_date,customer_code,currency_code,conversion_rate,tax_class_id,tax_type,reference_no,shipping_id,network_id,network_order_id,remarks,bill_discount_price,discount,fc_discount,createdby,modifiedby) VALUES('HQ','".$data['invoice_no']."','".date('Y-m-d')."','".$this->db->escape($data['customercode'])."','SGD','1','2','1','".$this->db->escape($data['OrderNo'])."','".$this->db->escape($data['shipping_id'])."','2','".$this->db->escape($data['OrderNo'])."','".$this->db->escape($data['ShippingMemo'])."','".$this->db->escape($data['Discount'])."','".$this->db->escape($data['Discount'])."','".$this->db->escape($data['Discount'])."','".$userName."','".$userName."') ";

			$this->db->query($sql);
			$sales_id  = $this->db->getLastId();
			$sub_total = 0;
			foreach ($orders as $value) {
				$product_sku = str_replace("'",'',$value['OptionCode']) !='' ? str_replace("'",'',$value['OptionCode']) : str_replace("'",'',$value['SellerCode']);
				if($product_sku !=''){
					$product  	 = $this->getProductBySKU($product_sku);
					if(empty($product)){
						$product = $this->getProductById($this->getProductIdBySKU($product_sku));
					}
				}
				$childAry['product_id'] = $product['product_id'];
				$childAry['order_no']   = $data['invoice_no'];
				$childAry['type']   	= 'SO';
				$this->cart->insertChildItems($childAry);
				
				if($product['product_id'] !=''){
					$data['Options'] = $data['Options'] !='' ? ' ('.$data['Options'].' )' : '';
					$sql = "INSERT INTO ".DB_PREFIX."sales_detail (location_code,sku,invoice_no,createdon,product_id,description,qty,sku_price,sub_total,net_total,createdby,conversion_rate,tax_class_id,discount,gst,cust_description) VALUES('HQ','".$this->db->escape($product['sku'])."','".$this->db->escape($data['invoice_no'])."','".date('Y-m-d')."','".$this->db->escape($product['product_id'])."','".$this->db->escape($product['name'])."','".$value['Qty']."','".$this->db->escape($value['SellPrice'])."','".$this->db->escape($value['SellPrice'] * $value['Qty'])."','".$value['SellPrice'] * $value['Qty']."','".$this->session->data['username']."','1','1','0','0','".$this->db->escape($value['Options'])."') ";

					$this->db->query($sql);
					$sub_total += $value['SellPrice'] * $value['Qty'];
				}
			}
			if($sub_total){
				$gst_sub_total = $sub_total - $data['Discount'];
				$net_total     = $sub_total - $data['Discount'];
				$inclusive_convert = 7 / (100 + 7);
				$tax_amount    = $gst_sub_total * $inclusive_convert;		
	        	if($tax_amount){
	        		$sub_total = $sub_total - $tax_amount; 
	        	}
				$this->db->query("UPDATE ".DB_PREFIX."sales_header set sub_total='".$sub_total."', actual_total='".$sub_total."', net_total='".$net_total."', fc_subtotal='".$sub_total."', fc_nettotal='".$net_total."',gst='".$tax_amount."', fc_tax='".$tax_amount."' where invoice_no='".$data['invoice_no']."' ");
			}else{
				$this->db->query("DELETE FROM ".DB_PREFIX."sales_header where invoice_no='".$data['invoice_no']."' ");
			}
		}
		if($sales_id){
			$this->deleteTempByOrderNummber($data['OrderNo']);
		}
	}
	public function deleteTempByOrderNummber($orderno){
		$this->db->query("DELETE FROM ".DB_PREFIX."qsm_order_details where OrderNo='".$orderno."' ");
	}
    public function checkOrderIdAndSku($orderId, $product_id){
        return $this->db->query("SELECT sh.id FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."sales_detail as sd on sh.invoice_no = sd.invoice_no WHERE sd.product_id='".$this->db->escape($product_id)."' AND sh.network_order_id='".$this->db->escape($orderId)."' AND sh.delivery_status!='Canceled' ")->row;
    }
    public function delete($id){
        $this->db->query("DELETE FROM ".DB_PREFIX."qsm_order_details where id='".$id."' ");
    }
    public function checkShippingAddress($custCode, $data){
    	$customerMobile = str_replace('-', '', $data[37]) !='' ? str_replace('-', '', $data[37]) : str_replace('-', '', $data[36]);
    	return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id='".$custCode."' AND contact_no='".$this->db->escape($customerMobile)."' and address1='".$this->db->escape($data[22])."' and zip='".$this->db->escape($data[23])."' ")->row;
    }
    public function addShippingAddress($data){
    	$customerMobile = str_replace('-', '', $data[37]) !='' ? str_replace('-', '', $data[37]) : str_replace('-', '', $data[36]);
    	$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,country,zip,contact_no,notes,isdefault) VALUES('".$this->db->escape($data['customer_id'])."','SH".$data['customer_id']."','".$this->db->escape($data[22])."','1','".$this->db->escape($data[23])."','".$this->db->escape($customerMobile)."','created when import qsm order at ".date('d/m/Y H:is')."','0') ";
    	    	$this->db->query($sql);
    	return $data['id'] = $this->db->getLastId();
    }
    public function clearImportedOrders(){
    	$this->db->query("DELETE FROM ".DB_PREFIX."qsm_order_details where orderExist='1'");
    }
}
?>