<?php
class ModelTransactionStockTransferOut extends Model {
	public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      
      	
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "StockTransfer_Header";

		$sql .= " WHERE transfer_no != '0'";
		
		if($data['hold_status']==1){
			$sql .= " AND hold_status = 1";
		}else{
			$sql .= " AND hold_status = 0";
		}
		if($data['filter_from_location'] != ''){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		if($data['filter_accepted_status'] == 'Yes'){
			$sql .= " AND accepted_status = 1 ";
		}else if($data['filter_accepted_status'] == 'No'){
			$sql .= " AND accepted_status = 0 ";
		}else if($data['filter_accepted_status'] == 'rejected'){
			$sql .= " AND revoke_status = 1 ";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "purchase as A, ".DB_PREFIX."purchase_to_product as B WHERE A.purchase_id = B.purchase_id";
		
		if($data['filter_supplier']){
			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "StockTransfer_Header WHERE transfer_no!='0'";
		$query = $this->db->query($sql);
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_from_location']){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		if($data['filter_accepted_status'] == 'Yes'){
			$sql .= " AND accepted_status = 1 ";
		}else if($data['filter_accepted_status'] == 'No'){
			$sql .= " AND accepted_status = 0 ";
		}else if($data['filter_accepted_status'] == 'rejected'){
			$sql .= " AND revoke_status = 1 ";
		}

		if($data['hold_status']==1){
			$sql .= " AND hold_status = 1";
		}else{
			$sql .= " AND hold_status = 0";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}

	public function getNameByProductId($pid){
		 $sql 	= "SELECT name FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['name'];
	}

	public function updateRevokeHeader($revoke_status,$userName,$date,$transfer_no){
		$updateHeaderSql = "UPDATE " . DB_PREFIX . "StockTransfer_Header SET revoke_status='".$revoke_status."',revoke_user='".$userName."',
												revoke_date='".$date."' where transfer_no='".$transfer_no."'";
		$this->db->query($updateHeaderSql);
	}

	public function updateItemDetails($data,$location_code){
		
		foreach ($data as $product) {
			$sku = $product['sku'];
			$qty = $product['sku_qty'];
			$sqlQ = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$qty. "' WHERE sku = '" . $sku . "'";
			$this->db->query($sqlQ);	
		}
	}
	public function addPurchase($data,$avg_method='',$fromHold='')
	{
		
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$reference_noAry =  explode("|",$data['reference_no']);
		$reference_no = $reference_noAry[0];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 0;
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';

		if($fromHold){
			$data['transaction_no'] = $data['transaction_no'];
		}else{
			$data['transaction_no'] = $frmLocationId.$data['transaction_no'];
		}

		$insertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon,
																	reference_no,
																	reference_date
																	) VALUES('" . $data['transaction_no'] . "',
																						'" . $data['transaction_date'] . "',	
																						 '" . $frmLocationId . "',
																						 '" . $frmLocationStatus . "',
																						 '" . $toLocationId . "',
																						 '" . $toLocationStatus . "',
																						 '" . $transfer_remarks . "',
																						 '" . $transfer_value . "',
																						 '" . $hold_status . "',
																						 '" . $accepted_status . "',
																						 '" . $accepted_user . "',
																						 '" . $accepted_date . "',
																						 '" . $revoke_status . "',
																						 '" . $revoke_user . "',
																						 '" . $revoke_date . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $reference_no . "',
																						 '" . $data['reference_date'] . "')";
	
		

		$res = $this->db->queryNew($insertSql);
		if($res=='error'){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
				exit;
		}

		if($reference_no==''){
			$this->db->query("UPDATE " . DB_PREFIX . "purchase SET transferred = '1' WHERE location_code = '" . $toLocationId . "'");	
		}else{
			$this->db->query("UPDATE " . DB_PREFIX . "purchase SET transferred = '1' WHERE location_code = '" . $toLocationId . "' and reference_no='".$reference_no."'");	
		}

		$purchaseId = $this->db->getLastId();
		$i=0;
		foreach ($data['products'] as $product) {
				$i++;
				
		/******************************/
			// New purchase Qty and price
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
			   $pSku = $this->getSkuByProductId($product['product_id']);
			   $pName = $this->getNameByProductId($product['product_id']);

			   $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$pqty. "',modifiedby='".$userName."',modifiedon=curdate() WHERE sku = '" . $pSku . "'");			
		
	    /************************************/
	    	   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "',
			   					'" . $pSku . "',
			   					'" . $pName . "',
			   					'" . $sku_uom."',
			   					'" . $product['quantity'] . "',
			   					'" . $product['price'] . "',
			   					'" . $transfer_value . "',
			   					'" . $accepted_qty . "',
			   					'" . $accepted_value . "',
			   					'" . $seq_no . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";
							 
	   		   $res = $this->db->queryNew($detailsInsertSql);
	   		   if($res=='error'){
					header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
					exit;
				}
		}
		return $data['transaction_no'];
	}
 
 public function addHoldPurchase($data,$avg_method='')
	{

		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 1;
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';

		$data['transaction_no'] = $frmLocationId.$data['transaction_no'];

		$insertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon) VALUES('" . $data['transaction_no'] . "',
																						'" . $data['transaction_date'] . "',												
																						 '" . $frmLocationId . "',
																						 '" . $frmLocationStatus . "',
																						 '" . $toLocationId . "',
																						 '" . $toLocationStatus . "',
																						 '" . $transfer_remarks . "',
																						 '" . $transfer_value . "',
																						 '" . $hold_status . "',
																						 '" . $accepted_status . "',
																						 '" . $accepted_user . "',
																						 '" . $accepted_date . "',
																						 '" . $revoke_status . "',
																						 '" . $revoke_user . "',
																						 '" . $revoke_date . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate()
																						 )";
	
		

		$res = $this->db->queryNew($insertSql);
		if($res=='error'){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/stock_transfer_out/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
				exit;
		}
		
		$purchaseId = $this->db->getLastId();
		$i=0;
		foreach ($data['products'] as $product) {
				$i++;
			
		/******************************/
			// New purchase Qty and price
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
			   $pSku = $this->getSkuByProductId($product['product_id']);
			   $pName = $this->getNameByProductId($product['product_id']);

			 
	    /************************************/
	    		

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "',
			   					'" . $pSku . "',
			   					'" . $pName . "',
			   					'" . $sku_uom."',
			   					'" . $product['quantity'] . "',
			   					'" . $product['price'] . "',
			   					'" . $transfer_value . "',
			   					'" . $accepted_qty . "',
			   					'" . $accepted_value . "',
			   					'" . $seq_no . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";
							 
	  	    $res = $this->db->queryNew($detailsInsertSql);
			if($res=='error'){
				header('Location: '.HTTP_SERVER.'index.php?route=transaction/stock_transfer_out/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
					exit;
			}
		  		   
		}
	}
function getTransferDetails($data){
     $sql = "SELECT * FROM " . DB_PREFIX . "StockTransfer_Detail WHERE transfer_no != '0'";
     if($data['transfer_no']){
        $sql .= " AND transfer_no ='" . $data['transfer_no'] . "'";
     }
     $sql .= " ORDER BY seq_no asc";
     $res =$this->db->query($sql);
     return $res->rows;
  }
   function getHoldHeaderbyId($purchase_id){
        $sql = "SELECT transfer_no as transaction_no,transfer_date,transfer_from_location,transfer_to_location,transfer_value,hold_status,reference_no FROM " . DB_PREFIX . "StockTransfer_Header";
        $sql.= " WHERE transfer_no = '".$purchase_id."'";
       $res =$this->db->query($sql);
     return $res->row;
  }
  function getHoldItemdetailsByTransNo($purchase_id){
    $sql = "SELECT STD.transfer_no,STD.sku,STD.sku_description,STD.sku_qty as quantity,STD.sku_cost as price,STD.transfer_value,P.product_id FROM tbl_StockTransfer_Detail as STD LEFT JOIN tbl_product as P on P.sku = STD.sku WHERE transfer_no = '".$purchase_id."'";
     $res =$this->db->query($sql);
     return $res->rows;
}


 public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
  public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	
	public function editPurchase($data,$purchaseId='')
	{
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		
		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 0;
		if($data['hold']=='1'){
			$hold_status = 1;
		}
		
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';

		$data['transaction_no'] = $data['transaction_no'];

		$insertSql = "UPDATE " . DB_PREFIX . "StockTransfer_Header SET transfer_date = '".$data['transaction_date']."'
																	,transfer_from_location= '".$frmLocationId."'
																	,transfer_from_location_status= '".$frmLocationStatus."'
																	,transfer_to_location= '".$toLocationId."'
																	,transfer_to_location_status= '".$toLocationStatus."'
																	,transfer_remarks= '".$transfer_remarks."'
																	,transfer_value= '".$transfer_value."'
																	,hold_status= '".$hold_status."'
																	,accepted_status='".$accepted_status."'
																	,accepted_user='".$accepted_user."'
																	,accepted_date='".$accepted_date."'
																	,revoke_status='".$revoke_status."'
																	,revoke_user='".$revoke_user."'
																	,revoke_date='".$revoke_date."'
																	,createdby='".$userName."'
																	,createdon= curdate()
																	,modifyby='".$userName."'
																	,modifyon=curdate() where transfer_no='".$data['transaction_no']."'";

		 $this->db->query($insertSql); 
		
		$sqlSte = "DELETE FROM " . DB_PREFIX . "StockTransfer_Detail where transfer_no='".$data['transaction_no']."'";
		$this->db->query($sqlSte);

		$i = 0;
		foreach ($data['products'] as $product) {
				$i++;
			
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			    $pSku = $this->getSkuByProductId($product['product_id']);
			    $pName = $this->getNameByProductId($product['product_id']);

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   // delete existing Details

			    

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Detail(
			   transfer_no,sku,sku_description,sku_uom,sku_qty,sku_cost,transfer_value,accepted_qty,accepted_value,seq_no,createdby,createdon, modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "','" . $pSku . "','" . $pName . "','" . $sku_uom."','" . $product['quantity'] . "',
			   					'" . $product['price'] . "','" . $transfer_value . "','" . $accepted_qty . "','" . $accepted_value . "','" . $seq_no . "',
			   					'" . $userName . "', curdate(),'" . $userName. "',curdate())";
							 

			   $this->db->query($detailsInsertSql); //  call HQ insert		   	 

		}
		return $purchaseId;
	}


	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stocktranser WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT TOP 1 pt.price AS unit_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price
				FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";
		if($data['filter_location_code']){
		   $sql.= " AND ps.location_code='".$data['filter_location_code']."'";	
		}
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			//$sql .= ") and p2b.sku_status= '1'";
			$sql .= ")";
		}

		

		//$sql .= " GROUP BY p.product_id";
		//echo $sql;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{
	
		/*$query = $this->db->query("SELECT p.product_id,p.quantity,p.price,p.average_cost,p.sku,p.name FROM " . DB_PREFIX . "product AS p WHERE p.product_id= '".$productId."'"); */
		$sql = "SELECT p.product_id,p.sku,p.name,ps.sku_qty as quantity,ps.sku_avg as average_cost ,ps.sku_price as price FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku) where p.product_id= '".$productId."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_purchase_details_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-details.csv');
		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";
		}
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "StockTransfer_Header WHERE transfer_no = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getPurchaseDetails($purchaseId){
		//$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(quantity) as totalQty,product_id FROM " . DB_PREFIX . "purchase_to_product WHERE `purchase_id` = '".$purchaseId."'");
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(sku_qty) as totalQty FROM " . DB_PREFIX . "StockTransfer_Detail WHERE transfer_no = '".$purchaseId."'");
		return $query->row;
	}
	
	public function getProductDetailsById($purchaseId){
		$query = $this->db->query("SELECT DISTINCT PP.purchase_product_id, P.product_id,PP.sku_qty as quantity,PP.sku_cost as price,PP.transfer_value as total,P.sku,P.name FROM " . DB_PREFIX . "StockTransfer_Detail as PP LEFT JOIN " . DB_PREFIX . "product as P ON P.sku = PP.sku WHERE PP.transfer_no = '".$purchaseId."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function getProductBarCode($productId){
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode_history WHERE product_id = '".$productId."'");
		return $query->row;
	}
	public function getAddressForLocation($data) {
		$query = $this->db->query("SELECT address1,address2 FROM " . DB_PREFIX . "location WHERE location_id = '".$data['id']."' AND location_code='".$data['code']."'");
		return $query->row;

	}
	public function getTransferAutoId() {
		$query = $this->db->query("SELECT TOP 1 RIGHT(transfer_no,6) as transfer_no  FROM " . DB_PREFIX . "StockTransfer_Header ORDER BY transfer_no DESC");
		return $query->row['transfer_no'];
	}
	public function getLocationName($id){
		$query = $this->db->query("SELECT location_name FROM " . DB_PREFIX . "location WHERE location_id = '".$id."'");
		return $query->row['location_name'];		
	}
	public function getLocationsDetailsByCode($locationCode){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code = '".$locationCode."' ");
		return $query->row;
	}
	public function getRefNosbyLocation($location_code){
	$sql ="select reference_no,reference_date from tbl_purchase where location_code='".$location_code."' AND transferred='0' GROUP BY reference_no,reference_date";
	$query = $this->db->query($sql);
	return $query->rows;
	}	

	public function getProductsbyLocation($location_code,$reference_no=''){
	$sql ="select PP.product_id,sum(PP.quantity) as quantity,sum(PP.foc) as foc_qty,PP.price,PR.sku,PR.name from tbl_purchase_to_product as PP
			LEFT JOIN tbl_purchase as P on P.purchase_id = PP.purchase_id
			LEFT JOIN tbl_product as PR on PR.product_id = PP.product_id 
			where P.location_code='".$location_code."' AND P.transferred='0'";
	if($reference_no!=''){
		$sql.=" AND P.reference_no='".trim($reference_no)."'";	
	}		
	$sql.="group by PP.purchase_product_id , PP.product_id,PP.price,PR.sku,PR.name order by PP.purchase_product_id ASC";

	$query = $this->db->query($sql);
	return $query->rows;
	}	

}
?>