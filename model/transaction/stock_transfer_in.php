<?php
class ModelTransactionStockTransferIn extends Model {
	public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "StockTransfer_Header";
		$sql .= " WHERE transfer_no != '0'";
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_from_location']){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "purchase as A, ".DB_PREFIX."purchase_to_product as B WHERE A.purchase_id = B.purchase_id";
		
		if($data['filter_supplier']){
			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "StockTransfer_Header WHERE transfer_no!='0'";

		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND createdon between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_from_location']){
			$sql .= " AND transfer_from_location = '" . $data['filter_from_location'] . "'";
		}
		if($data['filter_to_location']){
			$sql .= " AND transfer_to_location = '" . $data['filter_to_location'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY createdon";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}

	public function checkProductBySKU($sku){
		 $sql 	= "SELECT count(*) as tot FROM tbl_product WHERE sku='".$sku."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['tot'];
	}

	public function getNameByProductId($pid){
		 $sql 	= "SELECT name FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['name'];
	}

	public function updateItemDetails($header,$data,$location_code){
		//printArray($data); exit;
		$head = $header[0];
		$userName	= $this->session->data['username'];
		$NewinsertSql = "INSERT INTO " . DB_PREFIX . "AcceptedStockTransfer_Header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon) VALUES('" . $head['transfer_no'] . "',
																						'" . date('Y-m-d',strtotime($head['transfer_date']->format('d/m/Y'))). "',										
																						 '" . $head['transfer_from_location'] . "',
																						 '" . $head['transfer_from_location_status'] . "',
																						 '" . $head['transfer_to_location'] . "',
																						 '" . $head['transfer_to_location_status'] . "',
																						 '" . $head['transfer_remarks'] . "',
																						 '" . $head['transfer_value'] . "',
																						 '" . $head['hold_status']. "',
																						 '" . $head['accepted_status'] . "',
																						 '" . $headp['accepted_user'] . "',
																						 '" . date('Y-m-d',strtotime($head['accepted_date']->format('d/m/Y'))) . "',
																						 '" . $head['revoke_status'] . "',
																						 '" . $head['revoke_user'] . "',
																						 '" . date('Y-m-d',strtotime($head['revoke_date']->format('d/m/Y'))) . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate()
																						 )";
	
		$this->db->query($NewinsertSql);
		foreach ($data as $product) {
			$sku = $product['sku'];
			$qty = $product['accepted_qty'];
			//echo "<br>".$sql = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$qty. "' WHERE sku = '" . $sku . "' AND location_code='".$location_code."'";
			$sqlQ = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$qty. "' WHERE sku = '" . $sku . "'";

			$NewdetailsInsertSql = "INSERT INTO " . DB_PREFIX . "AcceptedStockTransfer_Detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $product['transfer_no'] . "',
			   					'" . $sku . "',
			   					'" . $product['sku_description'] . "',
			   					'" . $product['sku_uom']."',
			   					'" . $product['sku_qty'] . "',
			   					'" . $product['sku_cost'] . "',
			   					'" . $product['transfer_value'] . "',
			   					'" . $product['accepted_qty'] . "',
			   					'" . $product['accepted_value'] . "',
			   					'" . $product['seq_no'] . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";			 
	   		$this->db->query($NewdetailsInsertSql);
			$this->db->query($sqlQ);	
		}
	}

	public function addPurchase($data,$avg_method='')
	{
		
		require(DIR_SERVER.'hqconnect/common.php'); 

		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[1];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[1];

		$companyId	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0';
		}
		if(empty($data['remarks'])) {
			$data['remarks'] = '0';
		}
		if(empty($data['bill_discount_percentage'])) {
			$data['bill_discount_percentage'] = '0';
		}

		$purchase_return = 0;

		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$frmLocationStatus = 0;
		$toLocationStatus = 0;
		$transfer_remarks = $data['remarks'];
		
		$transfer_value = $data['total'];
		$hold_status = 0;
		
		$accepted_status =0;
		$accepted_user = '';
		$accepted_date  = '';

		$revoke_status = 0;
		$revoke_user = '';
		$revoke_date  = '';

		$data['transaction_no'] = $frmLocationId.$data['transaction_no'];

		$insertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Header(transfer_no,
																	transfer_date,
																	transfer_from_location,
																	transfer_from_location_status,
																	transfer_to_location,
																	transfer_to_location_status,
																	transfer_remarks,
																	transfer_value,
																	hold_status,
																	accepted_status,
																	accepted_user,
																	accepted_date,
																	revoke_status,
																	revoke_user,
																	revoke_date,
																	createdby,
																	createdon,
																	modifyby,
																	modifyon) VALUES('" . $data['transaction_no'] . "',
																						'" . $data['transaction_date'] . "',												
																						 '" . $frmLocationId . "',
																						 '" . $frmLocationStatus . "',
																						 '" . $toLocationId . "',
																						 '" . $toLocationStatus . "',
																						 '" . $transfer_remarks . "',
																						 '" . $transfer_value . "',
																						 '" . $hold_status . "',
																						 '" . $accepted_status . "',
																						 '" . $accepted_user . "',
																						 '" . $accepted_date . "',
																						 '" . $revoke_status . "',
																						 '" . $revoke_user . "',
																						 '" . $revoke_date . "',
																						 '" . $userName . "',
																						 curdate(),
																						 '" . $userName . "',
																						 curdate()
																						 )";
	

		$this->db->query($insertSql);		
		$api->insertTransferOutForHQ($insertSql); //  call HQ insert

		$purchaseId = $this->db->getLastId();
		$i=0;
		foreach ($data['products'] as $product) {

			   $pSku = $this->getSkuByProductId($product['product_id']);
			   $pName = $this->getNameByProductId($product['product_id']);

				$i++;
				$discount_percentage = '0';
				$discount_price      = '0';
				
			// New purchase Qty and price
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
		
			   $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$pqty. "' WHERE sku = '" . $pSku . "'");			
		
	    /************************************/
	    		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}

			   $sku_uom = 0;
			   $transfer_value = $product['total'];
			   $accepted_qty = 0;
			   $accepted_value = 0;
			   $seq_no = $i;

			   $detailsInsertSql = "INSERT INTO " . DB_PREFIX . "StockTransfer_Detail (
			   transfer_no,
			   sku,
			   sku_description,
			   sku_uom,
			   sku_qty,
			   sku_cost,
			   transfer_value,
			   accepted_qty,
			   accepted_value,
			   seq_no,
			   createdby,
			   createdon,
			   modifyby,
			   modifyon) VALUES('" . $data['transaction_no'] . "',
			   					'" . $pSku . "',
			   					'" . $pName . "',
			   					'" . $sku_uom."',
			   					'" . $product['quantity'] . "',
			   					'" . $product['price'] . "',
			   					'" . $transfer_value . "',
			   					'" . $accepted_qty . "',
			   					'" . $accepted_value . "',
			   					'" . $seq_no . "',
			   					'" . $userName . "',
			   					  curdate(),
			   					'" . $userName. "',
								   curdate()
								 )";
				
	   		   $this->db->query($detailsInsertSql);
	   		   $api->insertTransferOutForHQ($detailsInsertSql); //  call HQ insert
		}
		return $purchaseId;
	}
 
 
 public function addHoldPurchase($data,$avg_method='')
	{
		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[0];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[0];

		$companyId	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		//printArray($data);exit;
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0';
		}
		if(empty($data['remarks'])) {
			$data['remarks'] = '0';
		}
		if(empty($data['bill_discount_percentage'])) {
			$data['bill_discount_percentage'] = '0';
		}

		$purchase_return = 0;
		
		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		//$this->db->query("INSERT INTO " . DB_PREFIX . "purchase (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,total,hold,purchase_return,created_by,created_date) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','PURINV','" . $data['vendor'] . "','" . $data['reference_no'] . "','" . $data['reference_date'] . "','" .$data['remarks'] . "','" . $data['bill_discount_percentage'] . "','" . $data['bill_discount_price'] . "','" . $data['total'] . "','" . $data['hold'] . "','".$purchase_return."','" . (int)$userId . "',curdate())");
		//echo "INSERT INTO " . DB_PREFIX . "stocktranser(company_id,transaction_no,transaction_date,transaction_type,vendor_id,from_outlet_id,to_outlet_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,total,hold,purchase_return,created_by,created_date) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','PURINV', '0', '" . $frmLocationId . "','" .  $toLocationId . "','" . $data['reference_no'] . "','" . $data['reference_date'] . "','" . $data['remarks'] . "'," . $data['bill_discount_percentage'] . "," . $data['bill_discount_price'] . ",'" . $data['total'] . "','" . $data['hold'] . "','".$purchase_return."','" . (int)$userId . "',curdate())"; exit;
		$this->db->query("INSERT INTO " . DB_PREFIX . "stocktranser(company_id,transaction_no,transaction_date,transaction_type,vendor_id,from_outlet_id,to_outlet_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,total,hold,purchase_return,created_by,created_date) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','PURINV', '0', '" . $frmLocationId . "','" .  $toLocationId . "','" . $data['reference_no'] . "','" . $data['reference_date'] . "','" . $data['remarks'] . "'," . $data['bill_discount_percentage'] . "," . $data['bill_discount_price'] . ",'" . $data['total'] . "','" . $data['hold'] . "','".$purchase_return."','" . (int)$userId . "',curdate())");

		$purchaseId = $this->db->getLastId();
		foreach ($data['products'] as $product) {

				$discount_percentage = '';
				$discount_price      = '';
				if ($product['purchase_discount_mode'] == 1) {
				    $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price      = $product['purchase_discount_value'];
				}
		/******************************/
			// New purchase Qty and price
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
				
			if($avg_method=='1'){
					$avg_method ='WAM';
					$avg_price  = $this->WAM($product['product_id'],$pqty,$price);					
		   	}else{
		   		//FIFO method
					  $avg_method ='FIFO';
					  $productDetails = $this->getproductdetails($product['product_id']);
					  $avg_price      = (( $productDetails['quantity'] * $productDetails['price'] ) + ( $pqty * $price)) / ( $productDetails['quantity'] + $pqty );
		   	}
	    /************************************/
	    		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
	   		   //$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");
	   		   $this->db->query("INSERT INTO " . DB_PREFIX . "stocktranser_to_product (transfer_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");
			}
			foreach ($data['totals'] as $total) {
				if(empty($total['value'])) {
						 $total['value'] = '0.00';
				}			
				$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total (purchase_id,code,title,text,value,sort_order) VALUES('" . (int)$purchaseId . "','" . $this->db->escape($total['code']) . "','" . $this->db->escape($total['title']) . "','" . $this->db->escape($total['text']) . "','" . $total['value'] . "','" . (int)$total['sort_order'] . "')");
			}
			$invoice_no	= $this->getInventoryAutoId();
			$invoice_no	= (int)$invoice_no['last_id'];
			$lastInvoiceNo	= $invoice_no + 1;
			$lastInvoiceNo  =  str_pad($lastInvoiceNo, 6, "0", STR_PAD_LEFT);
			$company_id	= $this->session->data['company_id'];
			$this->db->query("UPDATE " . DB_PREFIX . "entity_increment SET increment_last_id = '".$lastInvoiceNo."' WHERE entity_type_id = '2' AND company_id = '" . (int)$company_id . "'");
			return $purchaseId;
	}
 public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
  public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	public function editPurchase($purchaseId, $data)
	{

		//printArray($data);exit;
		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$fromLocation = explode("|",$data['from_location_id']);
		$frmLocationId = $fromLocation[0];
		$toLocation = explode("|",$data['to_location_id']);
		$toLocationId = $toLocation[0];

		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$purchase_return = 0;
		$this->db->query("UPDATE " . DB_PREFIX . "purchase SET
			transaction_no = '" . $data['transaction_no'] . "'
			, transaction_date = '" . $data['transaction_date'] . "'
			, transaction_type = 'PURINV'
			, vendor_id = '" . $data['vendor'] . "'
			, from_outlet_id = '" . $frmLocationId . "'
			, to_outlet_id = '" . $toLocationId . "'
			, reference_no = '" . $data['reference_no'] . "'
			, reference_date = '" . $data['reference_date'] . "'
			, remarks = '" . $data['remarks'] . "'
			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'
			, bill_discount_price = '" . $data['bill_discount_price'] . "'
			, total = '" . $data['total'] . "'
			, hold = '" . $data['hold'] . "'
			, purchase_return = '".$purchase_return."'
			, created_by = '" . (int)$userId . "'
			, date_modified =curdate()
			WHERE purchase_id = '" . (int)$purchaseId . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "stocktranser_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		
		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = '';
		
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}

			$pqty  = (int)$product['quantity'];
			$price = (float)$product['price'];

			$pSku = $this->getSkuByProductId($product['product_id']);
			if($avg_method=='1'){
					//Weighted Average method calculation
				    $avg_method ='WAM';
					$avg_price  = $this->WAM($product['product_id'],$pqty,$price);
					// update product table qty, average_cost
					$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_avg='".$avg_price."',modifiedby='".$userName."',modifiedon=curdate() WHERE sku = '" . $pSku . "'");
		   	}else{
		   		//FIFO method
					  $avg_method ='FIFO';
					  $productDetails = $this->getproductdetails($product['product_id']);
					  $avg_price      = (( $productDetails['quantity'] * $productDetails['price'] ) + ( $pqty * $price)) / ( $productDetails['quantity'] + $pqty );
	    		      // update product table qty, average_cost
	    		   	  $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_avg='".$avg_price."',modifiedby='".$userName."',modifiedon=curdate() WHERE sku = '" . $pSku . "'");
		   	}

		   		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
				$avg_price = '0.00';
		   	
		   	 //$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");
		   	 $this->db->query("INSERT INTO " . DB_PREFIX . "stocktranser_to_product (transfer_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");


		}
		

		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_total WHERE purchase_id = '" . (int)$purchaseId . "'");
		foreach ($data['totals'] as $total) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total (purchase_id,code,title,text,value,sort_order) VALUES('" . (int)$purchaseId . "','" . $this->db->escape($total['code']) . "','" . $this->db->escape($total['title']) . "','" . $this->db->escape($total['text']) . "','" . (float)$total['value'] . "','" . (int)$total['sort_order'] . "')");
		}
		return $purchaseId;
	}


	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stocktranser WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT TOP 1 pt.price AS unit_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price
				FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";

		// /$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			$sql .= ")";
		}
		//$sql .= " GROUP BY p.product_id";
		//echo $sql;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{
	
		/*$query = $this->db->query("SELECT p.product_id,p.quantity,p.price,p.average_cost,p.sku,p.name FROM " . DB_PREFIX . "product AS p WHERE p.product_id= '".$productId."'"); */
		$sql = "SELECT p.product_id,p.sku,p.name,ps.sku_qty as quantity,ps.sku_avg as average_cost ,ps.sku_price as price FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku) where p.product_id= '".$productId."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_purchase_details_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-details.csv');
		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";
		}
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "StockTransfer_Header WHERE transfer_no = '" .$purchaseId . "'");
		return $query->row;
	}
	public function getPurchaseDetails($purchaseId){
		//$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(quantity) as totalQty,product_id FROM " . DB_PREFIX . "purchase_to_product WHERE `purchase_id` = '".$purchaseId."'");
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(sku_qty) as totalQty FROM " . DB_PREFIX . "StockTransfer_Detail WHERE transfer_no = '".$purchaseId."'");
		return $query->row;
	}

	function getTransferDetails($data){
     $sql = "SELECT * FROM " . DB_PREFIX . "StockTransfer_Detail WHERE transfer_no != '0'";
     if($data['transfer_no']){
        $sql .= " AND transfer_no ='" . $data['transfer_no'] . "'";
     }
     $sql .= " ORDER BY seq_no asc";
     $res = $this->db->query($sql);
     return $res->rows;
  }

  public function accepted_qtyUpdate($qty,$total,$transfer_no,$sku){
  	$updateItemSql = "UPDATE " . DB_PREFIX . "StockTransfer_Detail SET accepted_qty='".$qty."',accepted_value='".$total."' where transfer_no='".$transfer_no."' AND sku='".$sku."'"; 
  	$this->db->query($updateItemSql);

  }
  public function updateRevokeHeader($accepted_status,$userName,$date,$transfer_no){
		$updateHeaderSql = "UPDATE " . DB_PREFIX . "StockTransfer_Header SET accepted_status='".$accepted_status."',accepted_user='".$userName."',
												accepted_date='".$date."' where transfer_no='".$transfer_no."'";

		$this->db->query($updateHeaderSql);
	}

	public function getProductsDetailsBySKU($sku){
     $select = "select TOP 1 A.*,B.* from tbl_product as A LEFT JOIN  tbl_product_stock as B on A.sku = B.sku where A.sku='".$sku."' ";
     $list = $this->db->query($sql);;
     return $res->row;
  }
 	
 	public function getBarcodeBySKU($sku){
	  $select = "select barcode from tbl_product_barcode where sku='".$sku."'";
	  $list = $this->db->query($sql);;
     return $res->row;
	}
  
	
	public function getProductDetailsById($purchaseId){
		$query = $this->db->query("SELECT DISTINCT PP.purchase_product_id, P.product_id,PP.sku_qty as quantity,PP.sku_cost as price,PP.transfer_value as total,P.sku,P.name FROM " . DB_PREFIX . "StockTransfer_Detail as PP LEFT JOIN " . DB_PREFIX . "product as P ON P.sku = PP.sku WHERE PP.transfer_no = '".$purchaseId."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function getProductBarCode($productId){
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode_history WHERE product_id = '".$productId."'");
		return $query->row;
	}
	public function getAddressForLocation($data) {
		$query = $this->db->query("SELECT address1,address2 FROM " . DB_PREFIX . "location WHERE location_id = '".$data['id']."' AND location_code='".$data['code']."'");
		return $query->row;

	}
	public function getTransferAutoId() {
		$query = $this->db->query("SELECT TOP 1 RIGHT(transfer_no,6) as transfer_no  FROM " . DB_PREFIX . "StockTransfer_Header ORDER BY transfer_no DESC");
		return $query->row['transfer_no'];
	}
	public function getLocationName($id){
		$query = $this->db->query("SELECT location_name FROM " . DB_PREFIX . "location WHERE location_id = '".$id."'");
		return $query->row['location_name'];		
	}
	public function getLocationsDetailsByCode($locationCode){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code = '".$locationCode."' ");
		return $query->row;
	}
}
?>