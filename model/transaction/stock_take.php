<?php 
class ModelTransactionStockTake extends Model {
	public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "purchase";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='PURINV'";
		
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "purchase as A, ".DB_PREFIX."purchase_to_product as B WHERE A.purchase_id = B.purchase_id";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	public function getStockTakeCount($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "stocktake_header where stocktake_id !=''";

		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND CAST(stocktake_date as DATE)  between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getStockTakeList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "stocktake_header where stocktake_id !=''";
		//printArray($sql);exit;
		/*if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";  
		}*/
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND CAST(stocktake_date as DATE) between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY stocktake_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
		//printArray($query);exit;
	}
	public function getupdateStockTakeCSV($data)
	{
		$sql = "select D.location_code,D.sku,D.sku_description,SUM(D.scanned_qty) as scanned_qty,D.sku_qty  from tbl_stocktake_detail as D JOIN tbl_stocktake_header as H on D.stocktake_id = H.stocktake_id AND H.is_updated!='1'";
		
		$sql.=" Group by D.location_code,D.sku,D.sku_description,D.sku_qty";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY D.sku_description";
		}
		$query = $this->db->query($sql); 
		return $query->rows;
	}
	public function getupdateStockTakeList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT D.location_code,D.sku,D.sku_description,SUM(D.scanned_qty) as scanned_qty,D.sku_qty  from ".DB_PREFIX."stocktake_detail as D JOIN ".DB_PREFIX."stocktake_header as H on D.stocktake_id = H.stocktake_id AND H.is_updated!='1'";
		
		$sql.=" Group by D.location_code,D.sku,D.sku_description,D.sku_qty";
		
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY D.sku_description";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}
	public function addPurchase($data,$avg_method='')
	{
		
		$companyId	= $this->session->data['company_id'];
		//$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$purchase_return = 0;


		$stockheader_sql 	= "INSERT INTO " . DB_PREFIX ."stocktake_header (location_code,stocktake_date,terminal_code,is_updated,createdby,createdon) VALUES('".$data['location']."',curdate(),'".$data['terminal']."','0','".$userName."',curdate())";

		$this->db->query($stockheader_sql);
		$stocktake_id = $this->db->getLastId();
		if($stocktake_id!=''){
          foreach ($data['products'] as $product) { 
          	$qty  = $product['quantity'];
          	$sku_description = $product['name'];
          	$sku = $product['sku'];
          	$productDetails = $this->getproductdetails($product['product_id']);	
            $old_qty = $productDetails['quantity'];
            $stockdetails_sql = "INSERT INTO " . DB_PREFIX ."stocktake_detail (location_code,stocktake_id,sku,sku_description,sku_qty,scanned_qty,createdby,createdon)
                                 VALUES ('".$data['location']."','".$stocktake_id."','".$sku."','".$sku_description."','".$old_qty."','".$qty."','".$userName."',curdate())";

            $this->db->query($stockdetails_sql);
          }
      }    
      	
	}
	public function addStockTake($data,$avg_method='')
	{
		
		$companyId	= $this->session->data['company_id'];
		//$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$purchase_return = 0;

          for($i=0;$i<count($postary);$i++){
            $sku = $postary[$i]->sku;
            $qty = $postary[$i]->sku_qty;
            $sku_description = $postary[$i]->sku_description;
            $scanned_qty = $postary[$i]->scanned_qty;
            $stock_sql = "INSERT INTO " . DB_PREFIX ."stocktake_summary (location_code,sku,sku_description,sku_qty,scanned_qty,createdby)
                                 VALUES ('".$data['location']."','".$sku."','".$sku_description."','".(int)$qty."','".$scanned_qty."','1')";
            $this->db->query($stock_sql);
          }
	}
 
 	public function addHoldPurchase($data,$avg_method='')
	{
		
		$companyId	= $this->session->data['company_id'];
		//$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		//printArray($data);exit;
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0.00';
		}
		
		$purchase_return = 0;
		
		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$this->db->query("INSERT INTO " . DB_PREFIX . "purchase (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,total,hold,purchase_return,created_by,created_date) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','PURINV','" . $data['vendor'] . "','" . $data['reference_no'] . "','" . $data['reference_date'] . "','" .
			 $data['remarks'] . "','" . $data['bill_discount_percentage'] . "','" . $data['bill_discount_price'] . "','" . $data['total'] . "',
			 '" . $data['hold'] . "','".$purchase_return."','" . $userName . "',curdate())");
		$purchaseId = $this->db->getLastId();
		foreach ($data['products'] as $product) {

				$discount_percentage = '';
				$discount_price      = '';
				if ($product['purchase_discount_mode'] == 1) {
				    $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price      = $product['purchase_discount_value'];
				}
		/******************************/
			// New purchase Qty and price
				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
				
			if($avg_method=='1'){
					$avg_method ='WAM';
					$avg_price  = $this->WAM($product['product_id'],$pqty,$price);					
		   	}else{
		   		//FIFO method
					  $avg_method ='FIFO';
					  $productDetails = $this->getproductdetails($product['product_id']);
					  $avg_price      = (( $productDetails['quantity'] * $productDetails['price'] ) + ( $pqty * $price)) / ( $productDetails['quantity'] + $pqty );
		   	}
	    /************************************/
	    		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
	   		   $this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");
			}
			foreach ($data['totals'] as $total) {
				if(empty($total['value'])) {
						 $total['value'] = '0.00';
				}			
				$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total (purchase_id,code,title,text,value,sort_order) VALUES('" . (int)$purchaseId . "','" . $this->db->escape($total['code']) . "','" . $this->db->escape($total['title']) . "','" . $this->db->escape($total['text']) . "','" . $total['value'] . "','" . (int)$total['sort_order'] . "')");
			}
			$invoice_no	= $this->getInventoryAutoId();
			$invoice_no	= (int)$invoice_no['last_id'];
			$lastInvoiceNo	= $invoice_no + 1;
			$lastInvoiceNo  =  str_pad($lastInvoiceNo, 6, "0", STR_PAD_LEFT);
			$company_id	= $this->session->data['company_id'];
			$this->db->query("UPDATE " . DB_PREFIX . "entity_increment SET increment_last_id = '".$lastInvoiceNo."' WHERE entity_type_id = '2' AND company_id = '" . (int)$company_id . "'");
			return $purchaseId;
	}
 public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
public function getStocktakeTotals($stocktake_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stocktake_header WHERE stocktake_id = '" . (int)$stocktake_id . "' ORDER BY sort_order");

		return $query->rows;
	}

  public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	public function editPurchase($purchaseId, $data)
	{

		//printArray($data);exit;
		$data['reference_date']   = changeDate($data['reference_date']);
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$purchase_return = 0;
		$this->db->query("UPDATE " . DB_PREFIX . "purchase SET
			transaction_no = '" . $data['transaction_no'] . "'
			, transaction_date = '" . $data['transaction_date'] . "'
			, transaction_type = 'PURINV'
			, vendor_id = '" . $data['vendor'] . "'
			, reference_no = '" . $data['reference_no'] . "'
			, reference_date = '" . $data['reference_date'] . "'
			, remarks = '" . $data['remarks'] . "'
			, bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'
			, bill_discount_price = '" . $data['bill_discount_price'] . "'
			, total = '" . $data['total'] . "'
			, hold = '" . $data['hold'] . "'
			, purchase_return = '".$purchase_return."'
			, created_by = '" . $userName . "'
			, date_modified =curdate()
			WHERE purchase_id = '" . (int)$purchaseId . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		
		foreach ($data['products'] as $product) {
			$discount_percentage = '';
			$discount_price      = '';
		
			if ($product['purchase_discount_mode'] == 1) {
			    $discount_percentage = $product['purchase_discount_value'];
			} elseif ($product['purchase_discount_mode'] == 2) {
				$discount_price      = $product['purchase_discount_value'];
			}

			$pqty  = (int)$product['quantity'];
			$price = (float)$product['price'];

			$pSku = $this->getSkuByProductId($product['product_id']);
			if($avg_method=='1'){
					//Weighted Average method calculation
				    $avg_method ='WAM';
					$avg_price  = $this->WAM($product['product_id'],$pqty,$price);
					// update product table qty, average_cost
					$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_avg='".$avg_price."' WHERE sku = '" . $pSku . "'");
		   	}else{
		   		//FIFO method
					  $avg_method ='FIFO';
					  $productDetails = $this->getproductdetails($product['product_id']);
					  $avg_price      = (( $productDetails['quantity'] * $productDetails['price'] ) + ( $pqty * $price)) / ( $productDetails['quantity'] + $pqty );
	    		      // update product table qty, average_cost
	    		   	  $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_avg='".$avg_price."' WHERE sku = '" . $pSku . "'");
		   	}

		   		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
				$avg_price = '0.00';
		   	
		   	 $this->db->query("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,
		   	 	discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . (int)$product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "')");

		}
		

		$this->db->query("DELETE FROM " . DB_PREFIX . "purchase_total WHERE purchase_id = '" . (int)$purchaseId . "'");
		foreach ($data['totals'] as $total) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "purchase_total (purchase_id,code,title,text,value,sort_order) VALUES('" . (int)$purchaseId . "','" . $this->db->escape($total['code']) . "','" . $this->db->escape($total['title']) . "','" . $this->db->escape($total['text']) . "','" . (float)$total['value'] . "','" . (int)$total['sort_order'] . "')");
		}
		return $purchaseId;
	}


	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT TOP 1 pt.price AS unit_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price
				FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";

		// /$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			$sql .= ")";
		}
		//$sql .= " GROUP BY p.product_id";
		//echo $sql;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{
	
		/*$query = $this->db->query("SELECT p.product_id,p.quantity,p.price,p.average_cost,p.sku,p.name FROM " . DB_PREFIX . "product AS p WHERE p.product_id= '".$productId."'"); */
		$sql = "SELECT p.product_id,p.sku,p.name,ps.sku_qty as quantity,ps.sku_avg as average_cost ,ps.sku_price as price FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku) where p.product_id= '".$productId."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_purchase_details_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-details.csv');
		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";
		}
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "'");
		return $query->row;
	}
	public function getStockTakeDetailsById($stocktake_id)
	{
		//$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX ."stocktake_header WHERE stocktake_id = '" . (int)$stocktake_id . "'");
		return $query->row;
	}
	public function getStockTakeDetails($stocktake_id){
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(scanned_qty) as totalQty, sku FROM " . DB_PREFIX . "stocktake_detail WHERE `stocktake_id` = '".$stocktake_id."'");
		return $query->row;
	}
	public function getStockstDetailsById($stocktake_id){
		//$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stocktake_detail
			WHERE stocktake_id = '".$stocktake_id."'");
		return $query->rows;
	}
	public function getProductDetailsById($purchaseId){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT P.product_id,PP.quantity,PP.price,PP.total,PP.avg_cost,P.sku,P.name FROM " . DB_PREFIX . "purchase_to_product as PP
			LEFT JOIN " . DB_PREFIX . "product as P ON P.product_id = PP.product_id
			WHERE PP.purchase_id = '".$purchaseId."'");
		return $query->rows;
	}
	public function getProductBarCode($productId){
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode_history WHERE product_id = '".$productId."'");
		return $query->row;
	}
	public function getVendorsTaxId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tax_method FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['tax_method'];
	}
	/*public function getLocations(){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location ORDER BY location_id DESC");
		return $query->rows;
	}*/
	public function getCompanyDetails($company_id) {
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");$query = $this->db->query("SELECT c.location_code,a.location_code,a.location_name FROM " . DB_PREFIX . "company AS c LEFT JOIN " .DB_PREFIX . "location AS a ON c.location_code=a.location_code  WHERE c.company_id = '" . (int)$company_id . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	public function getStockTakeAutoId() {
		$query = $this->db->query("SELECT MAX(stocktake_id)+1 as transaction_no from tbl_stocktake_header");
		return $query->row['transaction_no'];
	}
	public function updatestatus(){
		$sql 		   = "UPDATE " . DB_PREFIX . "stocktake_header SET is_updated = '1' WHERE is_updated ='0'";
		$this->db->query($sql);
	}

	public function updatestatusX($postData){
		
		$sku 		   = $postData['sku']; 
		$sql 		   = "UPDATE " . DB_PREFIX . "stocktake_header SET is_updated = '1' WHERE 
						stocktake_id IN(select DISTINCT stocktake_id from tbl_stocktake_detail where location_code ='" . $location_code . "' AND sku ='" . $sku . "')";
		$this->db->query($sql);
	}

	
	public function addStockTakeSummary($postData){
		$itemsary = $postData['postary'];
		for($i=0;$i<count($itemsary['sku']);$i++){
		    $sku 			 = $itemsary['sku'][$i];
            $sku_description = $itemsary['sku_description'][$i];
            $qty 			 = $itemsary['sku_qty'][$i];
            $scanned_qty 	 = $itemsary['scanned_qty'][$i];
            $discrepancy_qty = $itemsary['scanned_qty'][$i];
            $location_code   = $itemsary['location_code'][$i];
            $is_updated      = 1;
            $stock_sql = "INSERT INTO " . DB_PREFIX ."stocktake_summary (location_code,sku,sku_description,sku_qty,scanned_qty,discrepancy_qty,is_updated,createdby,createdon)
                          VALUES ('".$location_code."','".$sku."','".$sku_description."','".(int)$qty."','".$scanned_qty."','".$discrepancy_qty."','".$is_updated."','1',curdate())";
            $this->db->query($stock_sql);
            $stockupdate_sql = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty +'".$scanned_qty."' WHERE sku ='" . $sku . "' 
            					AND location_code='".$location_code."'";
            $this->db->query($stockupdate_sql);
          }		

	}
}
?>