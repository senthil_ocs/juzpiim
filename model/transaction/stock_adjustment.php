<?php
class ModelTransactionStockAdjustment extends Model {
	public function getTotalPurchase($data)
	{	
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS totalPurchase FROM " . DB_PREFIX . "stockadjustment_header";
		
		$sql .= " WHERE stkAdjustment_id != ''";
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND stkAdjustment_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		/*if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}*/
		$query = $this->db->query($sql);
		return $query->row['totalPurchase'];
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "stock_adjustment_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.*,B.sku,B.sku_description,B.qty,B.unit_cost,B.total_value,B.gst,B.net_value FROM " . DB_PREFIX . "stockadjustment_header as A, ".DB_PREFIX."stockadjustment_detail as B WHERE A.stkAdjustment_id = B.stockAdjustment_id";
		if($data['filter_supplier']){
			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	public function getPurchaseList($data)
	{
		
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "stockadjustment_header WHERE stkAdjustment_id != ''";
		//$sql = "SELECT A.stkAdjustment_id,A.stkAdjustment_date,A.terminal_code,A.location_code,A.add_or_deduct,B.sku,B.sku_description,B.qty,B.unit_cost,B.total_value,B.gst,B.net_value FROM " . DB_PREFIX . "stockadjustment_header as A, ".DB_PREFIX."stockadjustment_detail as B WHERE A.stkAdjustment_id = B.stockAdjustment_id";
		if (!empty($data['show_hold'])) {
			$sql .= " AND hold = '" . $data['show_hold'] . "'";
		} else {
		    $sql .= " AND hold = '0'";
		}
		/*if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}*/
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND stkAdjustment_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		/*if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND stkAdjustment_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}*/
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY stkAdjustment_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		 //printArray($sql);die;
		$query = $this->db->query($sql);
		//printArray($query); exit;
		return $query->rows;
	}

	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}

	public function addPurchase($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$data['transaction_date'] = changeDate($data['transaction_date']);

	
		$sql1 = "INSERT INTO " . DB_PREFIX . "stockadjustment_header (location_code,stkAdjustment_date,total_value,gst,net_total,add_or_deduct,terminal_code,remarks,hold,created_by,created_date,modified_by,modified_date) VALUES('" . $data['location'] . "','" . $data['transaction_date'] . "','" . $data['total'] . "','0','" . $data['total'] . "','".$data['add_or_deduct']."','" . $data['terminal'] . "','" . $data['remarks'] . "','" . $data['hold'] . "','" . $userName . "',curdate(),'" . $userName . "',curdate())";
		// echo $sql1; die;
		
		$this->db->query($sql1);
		$StockId = $this->db->getLastId();

		foreach ($data['products'] as $product) { // each product stored
			$pqty  = (float)$product['quantity'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
			
			if($data['add_or_deduct']=='0') {
			    $avg_method ='WAM';
				$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_cost='".$product['price']."', modifiedon = curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='" . $data['location'] . "'");
				$op = "+";
		   	}else{
			  $avg_method ='FIFO';
		   	  $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$pqty. "',sku_cost='".$product['price']."', modifiedon = curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='" . $data['location'] . "'");
		   	  $op = "-";
		   	}
	    		//$srtQuantity = $product['quantity'];
	    		$ProductDatas[] = array("sku"=>$pSku,"quantity"=>$pqty,"operand"=>$op);
	    		$sql2 = "INSERT INTO " . DB_PREFIX . "stockadjustment_detail (stockAdjustment_id,sku,sku_description,qty,unit_cost,net_value,gst,total_value,created_by,created_date,modified_by,modified_date,product_id) values(
						'" . $StockId . "'
						,'" . $product['sku'] . "'
						,'" . $this->db->escape($product['name']) . "'
						,'" . $pqty . "'
						,'" . (float)$product['price'] . "'
						,'" . (float)$product['net_price'] . "'
						,'" . $product['tax_price'] . "'
						,'" . (float)$product['total'] . "'
						,'" . (int)$userId . "',curdate(),'" . (int)$userId . "',curdate(),'".$product['product_id']."')";					
	   		    $this->db->query($sql2);
	   		    
		} 

		/*if($ProductDatas){
			if(in_array($this->session->data['location_code'],API_LOCATION)){
				$product_data =array("ProductDatas"=> $ProductDatas);
				$passedData  = array("json"=>json_encode($product_data));
				$res = curlpost('stock_update',$passedData);				
			}
		}*/
		return $StockId;
	}
	 
	public function addHoldPurchase($data,$avg_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$data['transaction_date'] = changeDate($data['transaction_date']);

		$sql1 = "INSERT INTO " . DB_PREFIX . "stockadjustment_header (location_code,stkAdjustment_date,total_value,gst,net_total,add_or_deduct,terminal_code,remarks,hold,created_by,created_date,modified_by,modified_date) VALUES('" . $data['location'] . "','" . $data['transaction_date'] . "','" . $data['total'] . "','0','" . $data['total'] . "','".$data['add_or_deduct']."','" . $data['terminal'] . "','" . $data['remarks'] . "','" . $data['hold'] . "','" . $userName . "',curdate(),'" . $userName . "',curdate())";
		$this->db->query($sql1);
		$StockId = $this->db->getLastId();
		
		foreach ($data['products'] as $product) { // each product stored
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
	    	$pqty  = (float)$product['quantity'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
			

	    		$sql2 = "INSERT INTO " . DB_PREFIX . "stockadjustment_detail (stockAdjustment_id,product_id,sku,sku_description,qty,unit_cost,net_value,gst,total_value,created_by,created_date,modified_by,modified_date) values(
						'" . $StockId . "'
						,'" . $product['product_id'] . "'
						,'" . $product['sku'] . "'
						,'" . trim($product['name']) . "'
						,'" . $pqty . "'
						,'" . (float)$product['price'] . "'
						,'" . (float)$product['net_price'] . "'
						,'0'
						,'" . (float)$product['total'] . "'
						,'" . (int)$userId . "',curdate(),'" . (int)$userId . "',curdate())";						
	   		    $this->db->query($sql2); 
	   		    $purchase_product_Id = $this->db->getLastId();
	   		    
		} //exit;
		// each product end
		/*foreach ($data['totals'] as $total) {
			$totSql = "INSERT INTO " . DB_PREFIX . "stock_adjustment_total(purchase_id,code,title,text,value,sort_order) values(
				 '" . (int)$purchaseId . "'
				,'" . $this->db->escape($total['code']) . "'
				,'" . $this->db->escape($total['title']) . "'
				,'" . $this->db->escape($total['text']) . "'
				,'" . (float)$total['value'] . "'
				,'" . (int)$total['sort_order'] . "')";
			$this->db->query($totSql);
		}*/		
		return $StockId;
		
	}

	 public function checkstock($pid,$strLocation){
		 	$stockQry = $this->db->query("SELECT sum(QtyOnHand) as totQty FROM " . DB_PREFIX . "inventoryStockMaster 
		 								where LocationCode = '".$strLocation."' AND ItemCode = '".$pid."'");
		 	$stock = $stockQry->row['totQty'];
		 	return $stock;

	 }
 	public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
  public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM `tbl_stock_adjustment_to_product` WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stockadjustment_header WHERE stkAdjustment_id = '" . (int)$purchaseId . "' AND hold = '1'");
		return $query->row;
	}
	public function getPurchaseProduct($purchaseId)
	{
		$query = $this->db->query("SELECT DISTINCT stockAdjustment_id,sku,sku_description,qty,unit_cost as price,total_value as total,gst,net_value,product_id FROM " . DB_PREFIX . "stockadjustment_detail WHERE stockAdjustment_id = '" . (int)$purchaseId . "'");
		return $query->rows;
	}
	public function editPurchase($purchaseId, $data)
	{
		// printArray($data); die;

		$data['transaction_date'] = date('Y-m-d', strtotime($data['transaction_date']));
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		/*echo "UPDATE " . DB_PREFIX . "stockadjustment_header SET
			location_code = '" . $data['location'] . "'
			, stkAdjustment_date = '" . $data['transaction_date'] . "'
			, total_value = '" . $data['total'] . "'
			, gst = '0'
			, net_total = '" . $data['total'] . "'
			, add_or_deduct = '" . $data['add_or_deduct'] . "'
			, terminal_code = '" . $data['terminal'] . "'
			, remarks = '" . $data['remarks'] . "'
			, hold = '" . $data['hold'] . "'
			, modified_by = '" . (int)$userId . "'
			, modified_date = curdate()
			WHERE stkAdjustment_id = '" . (int)$purchaseId . "'"; */

		$this->db->query("UPDATE " . DB_PREFIX . "stockadjustment_header SET
			location_code = '" . $data['location'] . "'
			, stkAdjustment_date = '" . $data['transaction_date'] . "'
			, total_value = '" . $data['total'] . "'
			, gst = '0'
			, net_total = '" . $data['total'] . "'
			, add_or_deduct = '" . $data['add_or_deduct'] . "'
			, terminal_code = '" . $data['terminal'] . "'
			, remarks = '" . $data['remarks'] . "'
			, hold = '" . $data['hold'] . "'
			, modified_by = '" . $userName . "'
			, modified_date = curdate()
			WHERE stkAdjustment_id = '" . (int)$purchaseId . "'");


		//$this->db->query("DELETE FROM " . DB_PREFIX . "stock_adjustment_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		
		foreach ($data['products'] as $product) { // each product stored
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
	    	$pqty  = (float)$product['quantity'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
			
			if($data['hold'] == 0){
			if($data['add_or_deduct']=='0') {
				//Weighted Average method calculation
			    //$avg_method ='WAM';
				// update product table qty, average_cost
				$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty+'" .$pqty. "',sku_cost='".$product['price']."', modifiedon = curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='" . $data['location'] . "'");
		   	}else{
		   		//FIFO method
			  //$avg_method ='FIFO';
		      // update product table qty, average_cost
		   	  $this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_qty = sku_qty-'" .$pqty. "',sku_cost='".$product['price']."', modifiedon = curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='" . $data['location'] . "'");
		   	}
	    	}	
	    		$sql2 = "INSERT INTO " . DB_PREFIX . "stockadjustment_detail (stockAdjustment_id,product_id,sku,sku_description,qty,unit_cost,total_value,gst,net_value,created_by,created_date,modified_by,modified_date) values(
						'" . (int)$purchaseId . "'
						,'" . $product['product_id'] . "'
						,'" . $product['sku'] . "'
						,'" . trim($product['name']) . "'
						,'" . $pqty . "'
						,'" . (float)$product['price'] . "'
						,'" . (float)$product['net_price'] . "'
						,'0'
						,'" . (float)$product['total'] . "'
						,'" . (int)$userId . "',curdate(),'" . (int)$userId . "',curdate())"; 
						//printArray($sql);
						/*echo "INSERT INTO " . DB_PREFIX . "stockadjustment_detail (stockAdjustment_id,product_id,sku,sku_description,qty,unit_cost,total_value,gst,net_value,created_by,created_date,modified_by,modified_date) values(
						'" . (int)$purchaseId . "'
						,'" . $product['product_id'] . "'
						,'" . (int)$product['sku'] . "'
						,'" . trim($product['name']) . "'
						,'" . $pqty . "'
						,'" . (float)$product['price'] . "'
						,'" . (float)$product['net_price'] . "'
						,'0'
						,'" . (float)$product['total'] . "'
						,'" . (int)$userId . "',curdate(),'" . (int)$userId . "',curdate())"; exit;*/
	   		    $this->db->query($sql2);
	   		}
			
		return $purchaseId;
	}
	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stock_adjustment WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "stock_adjustment_to_product AS pt LEFT JOIN " . DB_PREFIX . "stock_adjustment AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getcatName($catId){

		$query = $this->db->query("SELECT  category_name FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$catId. "'");
		return $query->row['category_name'];	
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT pt.price AS unit_cost FROM " . DB_PREFIX . "stock_adjustment_to_product AS pt LEFT JOIN " . DB_PREFIX . "stock_adjustment AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC LIMIT 1");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT p.product_id, p.sku, p.quantity, p.price,p.power, p.status, p.image,p.average_cost, pd.name,pcat.category_id FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_company pc ON (p.product_id = pc.product_id)";
		
		//if(!empty($data['filter_cat'])) {
			$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_category pcat ON (p.product_id = pcat.product_id)";
			$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		//}		
		
		$sql.= " WHERE pc.company_id = '".$company_id."' AND p.status = '1' AND p.date_available <= NOW() ";
		
		if(!empty($data['filter_power'])) {
			$filterpower = $data['filter_power'];
			$sql.= "AND p.power LIKE '%" .$filterpower."%'";
		}	

		if(!empty($data['filter_cat'])) {
			$filterCat = $data['filter_cat'];
			$sql.= "AND pcat.category_id ='".$filterCat."'";
		}		
		
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LCASE(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			//$sql .= ") and p2b.sku_status= '1'";
			$sql .= ")";
		}
		$sql .= " GROUP BY p.product_id";
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getcategoriesDetailsforautoFill($name)
	{
		$query = $this->db->query("SELECT C.category_id,C.category_name,C.department_id,C.status,D.department_name FROM " . DB_PREFIX . "category  C 
		 LEFT JOIN " . DB_PREFIX . "department D ON (C.department_id = D.department_id) WHERE C.category_name LIKE '%".$name."%' AND C.status= '1'");
		return $query->rows;
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{
		$query = $this->db->query("SELECT p.product_id,p.quantity,p.price,p.average_cost,p.sku,pd.name FROM " . DB_PREFIX . "product AS p,". DB_PREFIX ."product_description AS pd WHERE p.product_id= '".$productId."' AND pd.product_id= '".$productId."'");
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_purchase_details_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-details.csv');
		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";
		}
	}
	public function getCompanyLocation($companyId)
	{
		$query = $this->db->query("SELECT location_code FROM " . DB_PREFIX . "company WHERE company_id = '".$companyId."'");
		return $query->row;
	}
	public function getLotNumberByInventoryCode($sku)
	{
		$query = $this->db->query("SELECT id,LotNo,ExpiryDate,QtyOnHand FROM " . DB_PREFIX . "inventoryByLotNo WHERE InventoryCode = '".$sku."' AND QtyOnHand != 0");
		return $query->rows;
	}
	public function getGeneralSettings(){
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE `key` = 'config_saleslot'");
		return $query->row;
	}
	public function getPurchaseDetails($purchaseId){

		$query = $this->db->query("SELECT COUNT(*) as totalItem FROM " . DB_PREFIX . "stockadjustment_detail WHERE stockAdjustment_id = '".$purchaseId."'");
		return $query->row;
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stockadjustment_header WHERE stkAdjustment_id = '" . (int)$purchaseId . "'");
		return $query->row;
	}
	//,PBH.barcode
	//LEFT JOIN " . DB_PREFIX . "product_barcode_history as PBH ON P.product_id = PBH.product_id  
	public function getProductDetailsById($purchaseId){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "stockadjustment_detail
			WHERE stockAdjustment_id = '".$purchaseId."'");
		//LEFT JOIN " . DB_PREFIX . "product as P ON P.product_id = PP.product_id
		return $query->rows;
	}
	public function getProductBarCode($productId){
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode_history WHERE product_id = '".$productId."'");
		return $query->row;
	}
	public function getCustomerDetails($name){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers WHERE status = '0' and name like '%".$name."%'");
		return $query->rows;
	}
	public function getPurchaseAutoId() {
		$query = $this->db->query("SELECT TOP 1 transaction_no  FROM " . DB_PREFIX . "stock_adjustment where transaction_no!='0' ORDER BY purchase_id DESC");
		return $query->row['transaction_no'];
	}
	public function getReasonsByType($type){
		$query = $this->db->query("SELECT reasoncode FROM " . DB_PREFIX . "stock_adjustment_master WHERE Type = '".$type."' ");
		return $query->rows;
	}
	public function getlotNames($lotids) {
		$query = $this->db->query("SELECT LotNo FROM " . DB_PREFIX . "inventoryByLotNo where id IN(" .$lotids. ")");
		$res =  $query->rows;
		$str = '';
		for($i=0;$i<count($res);$i++){
			 $str.=$res[$i]['LotNo'].',';
		}
		return rtrim($str,",");
	}
	public function updateQtyCalculationforInLotNoTbl($qty,$pid,$purchase_id='',$purchase_product_id='',$lotIds=''){
		$res = $this->qtyUPdateInLot($qty,$pid,$purchase_id,$purchase_product_id,$lotIds);
		$detailsAry[] = $res;
		if(count($res)>=1){
			if($res['QtyOnHand']<$qty){
				  $Nqty = $qty - $res['QtyOnHand']; 
				  if($Nqty>=1){
				  	$res = $this->updateQtyCalculationforInLotNoTbl($Nqty,$pid,$purchase_id,$purchase_product_id,$lotIds);
				  	$detailsAry[] = $res[0];
				  }
			}
		}
		return $detailsAry;
	}
	public function updateQtyforinventoryStockMaster($qty,$pid,$locationId){
		 $this->db->query("UPDATE " . DB_PREFIX . "inventoryStockMaster SET QtyOnHand = `QtyOnHand`-'" .$qty. "' WHERE ItemCode = '" . (int)$pid . "' AND LocationId = '".$locationId."'");

	}
	public function qtyUPdateInLot($qty,$pid,$purchase_id='',$purchase_product_id='',$lotIds=''){
		if($lotIds){
		 $sql 	  = "select id,ProductId,LotNo,ExpiryDate,QtyOnHand from " . DB_PREFIX . "inventoryByLotNo where ProductId='".$pid."' AND id IN(".$lotIds.") AND QtyOnHand!='0' order by ExpiryDate asc LIMIT 0,1";
		}else{
		$sql 	  = "select id,ProductId,LotNo,ExpiryDate,QtyOnHand from " . DB_PREFIX . "inventoryByLotNo where ProductId='".$pid."' and QtyOnHand!='0' order by ExpiryDate asc LIMIT 0,1";
		}
		$query    = $this->db->query($sql);
		$res      = $query->rows;
		if($qty>=$res[0]['QtyOnHand']){
			$updateQty = 0;//$qty - $res[0]['QtyOnHand'];
			$res[0]['qty_taken'] = $res[0]['QtyOnHand'];
		}else{
			$updateQty =  $res[0]['QtyOnHand'] + $qty;
			$res[0]['qty_taken'] = $qty;
		}
		$res[0]['qty_taken_date'] =date("Y-m-d H:i:s");
		if(count($res)>=1){
			$id = $res[0]['id'];

			$updateSql = "UPDATE " . DB_PREFIX . "inventoryByLotNo SET QtyOnHand = '" .$updateQty. "' where ProductId='".$pid."' AND id='".$id."'";
			$this->db->query($updateSql);

			$type = 'SALINV';
		    $this->manageLotHistory($pid,$purchase_id,$purchase_product_id,$res[0]['LotNo'],$res[0]['ExpiryDate'],$res[0]['qty_taken'],$type);

			return $res[0];
		}else{
			return false;
		}
	}
	public function getVendorsTaxId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tax_method FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['tax_method'];
	}
	public function getStockAdjustAutoId() {
		$query = $this->db->query("SELECT MAX(stkAdjustment_id)+1 as transaction_no from tbl_stockadjustment_header");
		return $query->row['transaction_no'];
	}
}
?>