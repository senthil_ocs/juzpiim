<?php
class ModelTransactionPurchase extends Model {
	public function getTotalPurchase($data)
	{
	    $company_id	= $this->session->data['company_id'];	
		$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "purchase as P 
			LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_id=P.vendor_id
			LEFT JOIN " . DB_PREFIX . "user as U on U.user_id=P.created_by";

		$sql.=" WHERE P.transaction_type='PURINV' AND P.purchase_return ='0' AND deleted='0' ";

		if($data['filter_supplier']){
			$sql .= " AND P.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND P.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND (P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR P.reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			if($data['filter_reference_date']){
				$sql .= " AND P.reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND P.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			
		}
		// echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getTotalPOrequest($data)
	{
      	$sql = "SELECT COUNT(Nettotal) AS totalPurchase, sum(Nettotal) AS grandtotal FROM " . DB_PREFIX . "po_req_header  WHERE PO_Req_No != '' ";
		
		if($data['filter_supplier']){
			$sql .= " AND Vendor_Code = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND PO_Req_No LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		// if($data['filter_date_from']!=''){
		// 	$data['filter_date_from'] = changeDate($data['filter_date_from']); 
		// 	$sql .= " AND PO_Req_Date =  '" . $data['filter_date_from'] . "'";
		// }
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND PO_Req_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getPOrequestList($data)
	{

		$sql = "SELECT PO.*,V.vendor_name FROM " . DB_PREFIX . "po_req_header as PO LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_code = PO.Vendor_Code";
		$sql .= " WHERE PO.PO_Req_No != ''";
		
		if($data['filter_supplier']){
			$sql .= " AND PO.Vendor_Code = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND PO.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND PO.PO_Req_No LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to']))); 
			$sql .= " AND PO_Req_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY PO.PO_Req_Date";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		// if (isset($data['start']) || isset($data['limit'])) {
		// 	if ($data['start'] < 0) {
		// 		$data['start'] = 0;
		// 	}
		// 	if ($data['limit'] < 1) {
		// 		$data['limit'] = 20;
		// 	}
		// 	//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		// 	$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		// }
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalPurchaseNew($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(purchase_id) AS totalPurchase, sum(total) AS grandtotal FROM " . DB_PREFIX . "purchase";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='PURINV'";
		
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 

			if($data['filter_reference_date']){
				$sql .= " AND reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getPurchaseTotals($purchase_id,$field_value='')
	{
      	$sql = "SELECT SUM(net_price) as sub_total,SUM(tax_price) as total_tax_price,SUM(total) as total_price FROM " . DB_PREFIX . "purchase_to_product";
		$sql.= " WHERE purchase_id = '".$purchase_id."' GROUP BY purchase_id";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getTotalPurchaseHistoryNew($purchase_id,$field_value='')
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot,code FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' GROUP BY code order by code asc";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getTotalPurchaseHistory($purchase_id,$field_value)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT SUM(value) as tot FROM " . DB_PREFIX . "purchase_total";
		$sql.= " WHERE purchase_id = '".$purchase_id."' AND code='".$field_value."' order by sort_order";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}
	public function getPurchaseProductList($data)
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT A.transaction_no,A.purchase_id,B.product_id,B.quantity,B.price,B.net_price,B.tax_price FROM " . DB_PREFIX . "purchase as A, ".DB_PREFIX."purchase_to_product as B WHERE A.purchase_id = B.purchase_id";
		
		if($data['filter_supplier']){
			$sql .= " AND A.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND A.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			
			$sql .= " AND A.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendors() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_code!= '' order by vendor_name asc");
		return $query->rows;
	}
	public function getPurchaseList($data)
	{
	    $company_id	= $this->session->data['company_id'];	
		$sql = "SELECT P.*,V.vendor_name as vendor_name FROM " . DB_PREFIX . "purchase as P 
			LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_id=P.vendor_id
			LEFT JOIN " . DB_PREFIX . "user as U on U.user_id=P.created_by";

		$sql.=" WHERE P.transaction_type='PURINV' AND P.purchase_return ='0' AND deleted='0' ";

		if($data['filter_supplier']){
			$sql .= " AND P.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND P.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			//$sql .= " AND P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
			$sql .= " AND (P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR P.reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			if($data['filter_reference_date']){
				$sql .= " AND P.reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND P.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			
		}
		$sql .= " ORDER BY P.purchase_id DESC ";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSkuByProductId($pid){
		 $sql 	= "SELECT sku FROM tbl_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->row;
	 	 return $res['sku'];
	}
	public function addPurchase($data,$cost_method='')
	{
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$data['userId']  		   = $userId;
		$purchase_return 		   = 0;
		$data['transaction_date']  = changeDate($data['transaction_date']);
		$data['arrival_date_from'] = changeDate($data['arrival_date_from']);
		$data['arrival_date_to']   = changeDate($data['arrival_date_to']);

		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}
		}

		if($data['currency_code']=='SGD'){
			$data['conversion_rate'] = '1';
		}
		$data['fc_subtotal']	 = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      	 = $data['conversion_rate'] * $gst;
		$data['fc_discount'] 	 = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee'] = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal']     = $data['conversion_rate'] * $data['total'];

		$istagged = 0;
		if(!empty($data['invoice_no'])){
			$istagged = 1;
		}

		$sql = "INSERT INTO " . DB_PREFIX . "purchase (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,handling_fee,total,hold,purchase_return,location_code,sub_total,gst,created_by,modified_by,discount,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_nettotal,tax_class_id,tax_type,fc_discount,fc_handling_fee,term_id,arrival_date_to,attachment,istagged,customer_id,shipping_id) VALUES('" . (int)$companyId . "','" . $this->db->escape($data['transaction_no']) . "','" . $this->db->escape($data['transaction_date']) . "','PURINV','" . $this->db->escape($data['vendor']) . "','" . $this->db->escape($data['reference_no']) . "','" . $this->db->escape($data['arrival_date_from']) . "','" . $this->db->escape($data['remarks']) . "','" . $this->db->escape($data['bill_discount_percentage']) . "','" . $this->db->escape($data['bill_discount_price']) . "','".$this->db->escape($data['fc_handling_fee'])."','" . $this->db->escape($data['fc_nettotal']) . "','" . $this->db->escape($data['hold']) . "','".$purchase_return."','" . $this->db->escape($data['location_code']) . "','" . $this->db->escape($data['fc_subtotal']). "','" . $this->db->escape($data['fc_tax']). "','".$userName."','".$userName."','".$this->db->escape($data['fc_discount'])."','".$this->db->escape($data['currency_code'])."','".$this->db->escape($data['conversion_rate'])."','".$sub_total."','".$gst."','".$this->db->escape($data['total'])."','".$this->db->escape($data['tax_class_id'])."','".$this->db->escape($data['tax_type'])."','".$discount."','".$handling_fee."','".$this->db->escape($data['term_id'])."','".$this->db->escape($data['arrival_date_to'])."','".$this->db->escape($data['attachment'])."','".$istagged."','".$this->db->escape($data['customer_id'])."','".$this->db->escape($data['shipping_id'])."')";

		// echo $sql; die;
		$res = $this->db->queryNew($sql);
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);	
				exit;
		}
		$purchaseId = $this->db->getLastId();
		$data['purchaseId'] = $purchaseId;
		$orderDiscount = 0;
		foreach ($data['products'] as $product) {
				$orderDiscount+=$product['purchase_discount'];
				$discount_percentage = '';
				$discount_price      = '';
				if ($product['purchase_discount_mode'] == 1) {
				    $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price      = $product['purchase_discount_value'];
				}
			/******************************/
			// New purchase Qty and price
				$pqty  = (float)$product['quantity'];
				$price = (float)$product['price'];
			// need to calculate avg cost method here
			$pSku = $this->getSkuByProductId($product['product_id']);
			$unit_cost = $price;
			
			if($product['foc_quantity']>=1){
				$pqty = $pqty+$product['foc_quantity'];
			}
			$childAry['product_id'] = $product['product_id'];
			$childAry['order_no']   = $data['transaction_no'];
			$childAry['type']   	= 'PO';
			$this->cart->insertChildItems($childAry);

			$productDetails = $this->getproductdetails($product['product_id']);
			if($data['config_vendor_update']=='1'){
				if($productDetails['sku_vendor_code'] != $data['vendor']){
					// $this->db->queryNew("UPDATE ".DB_PREFIX."product SET sku_vendor_code ='".$data['vendor']."' where product_id='".$product['product_id']."' ");
					// dissabled at 19- July by ragu
				}
			}
			$location_code = $data['location_code'];

		    /************************************/
	    		if(empty($product['raw_cost'])) {
					$product['raw_cost'] = '0.00';
				}
				if(empty($product['price'])) {
					$product['price'] = '0.00';
				}
				if(empty($product['net_price'])) {
					$product['net_price'] = '0.00';
				}
				if(empty($product['tax_price'])) {
					$product['tax_price'] = '0.00';
				}
				if(empty($product['total'])) {
					$product['total'] = '0.00';
				}
				if(empty($discount_price)) {
					$discount_price = '0.00';
				}
				if(empty($avg_price)) {
					$avg_price = '0.00';
				}
				if(empty($product['foc_quantity'])){
					$product['foc_quantity'] = 0;
				}
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
				    $product['net_price'] = $product['total'] + $product['tax_price'];
				}				
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
				    $product['total'] = $product['total'] - $product['tax_price'];
				}
				$res = $this->db->queryNew("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,transaction_no,sku,conversion_rate,description) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . $this->db->escape($product['quantity']) . "','" . $this->db->escape($product['price']) . "','" . $this->db->escape($product['raw_cost']) . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$this->db->escape($product['tax_class_id']) . "','" . $this->db->escape($product['net_price']) . "','" . $this->db->escape($product['tax_price']) . "','" . $this->db->escape($product['total']) . "','" . $avg_price . "','" . $avg_method . "','".$this->db->escape($product['foc_quantity'])."','".$data['transaction_no']."', '" . $pSku . "','".$data['conversion_rate']."','".$this->db->escape($product['description'])."')");
				   
		   	if($res){
		   		$updateSql = "update " . DB_PREFIX . "purchase set discount='".$orderDiscount."' where purchase_id='".$purchaseId."'";
		   		$ProductDatas[] = array("sku"=>$pSku,"quantity"=>$pqty,"operand"=>"+");
		   	}else{
		   		$delSql = "DELETE " . DB_PREFIX . "purchase where purchase_id='".$purchaseId."'";
				$this->db->query($delSql);
				exit;
		   	}
		}
		if($istagged){
			$this->updateTaggedSalesOrders($data);
		}
		return $purchaseId;
	}
 	public function updateTaggedSalesOrders($data){

		$this->resetSalesOrderAndInvoice($data);
	 	if(!empty($data['invoice_no'])){
	 		foreach ($data['invoice_no'] as $value) {
	 			if($value){
		 			$order = $this->getSalesOrderByInvoiceNo($value);
		 			if(!empty($order)){
		 				$oldOrder = $this->db->query("SELECT * FROM ".DB_PREFIX."tagged_sales_orders where purchase_id='".$data['purchaseId']."' AND order_no='".$value."' ")->row;

		 				if(!empty($oldOrder)){
		 					$this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set vendor='".$data['vendor']."', customer_id='".$order['customercode']."', cust_name='".$order['name']."' where purchase_id='".$data['purchaseId']."' AND order_no='".$value."' ");
		 				}else{
				 			$this->db->query("INSERT ".DB_PREFIX."tagged_sales_orders (transaction_no,purchase_id,vendor,order_no,order_id,customer_id,cust_name,created_by) VALUES('".$data['transaction_no']."','".$data['purchaseId']."','".$data['vendor']."','".$value."','".$order['id']."','".$order['customercode']."','".$order['name']."','".$data['userId']."')");
		 				}
		 				$this->updateSalesOrderAndDetails($data,$value);
		 			}
	 			}
	 		}
 		}
 	}

 	public function resetSalesOrderAndInvoice($data) {

 		$sql = '';
 		if(!empty($data['invoice_no'])){
 			$data['invoice_no'] = array_filter($data['invoice_no']);
	 		$sql = " AND order_no NOT IN ('".implode($data['invoice_no'],"','")."') ";
 		}
 		$unwantedOrders = $this->db->query("SELECT order_no FROM ".DB_PREFIX."tagged_sales_orders where purchase_id='".$data['purchaseId']."' ".$sql." ")->rows;
 		$removable = array_column($unwantedOrders,'order_no');

 		if(!empty($removable)){
 			foreach($removable as $key => $value){
				$data['soDetails'] 	   = $this->getSalesOrderDetailsByInvoiceNo2($value);

		 		foreach($data['products'] as $cart){
		 			$detailsProducts   = array_column($data['soDetails'],'product_id');
		 			if(in_array($cart['product_id'], $detailsProducts)){	 				
		 				$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0',purchase_id='' where invoice_no ='".$value."' and product_id='".$cart['product_id']."' ");
		 				$SI_no = $this->getSalesInvoiceBySO($value)['invoice_no'];

		 				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0',purchase_id='' where invoice_no ='".$SI_no."' and product_id='".$cart['product_id']."' ");
		 			}else{
		 				$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0',purchase_id='' where invoice_no='".$value."' AND product_id = (SELECT parant_product_id FROM ".DB_PREFIX."order_child_items where order_no ='".$value."' and child_product_id ='".$cart['product_id']."') ");

		 				$SI_no = $this->getSalesInvoiceBySO($value)['invoice_no'];
		 				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0',purchase_id='' where  invoice_no ='".$SI_no."' and product_id= (SELECT parant_product_id FROM ".DB_PREFIX."order_child_items where order_no ='".$value."' and child_product_id ='".$cart['product_id']."') ");
		 			}
		 		}	
 				$this->checkSalesOrderToUpdate($value); // if that order all items not tagged means update as not tagged in header
 			}
	 		$unwantedOrders	= implode($removable, "','");
	 		$this->db->query("DELETE FROM ".DB_PREFIX."tagged_sales_orders where purchase_id='".$data['purchaseId']."' AND order_no IN ('".$unwantedOrders."') ");
 		}
 	}
 	public function updateSalesOrderAndDetails($data,$invoice_no) {
 		$data['soDetails'] = $this->getSalesOrderDetailsByInvoiceNo($invoice_no, $tagged = 1);

 		foreach($data['products'] as $cart){
 			$detailsProducts = array_column($data['soDetails'],'product_id');

 			if(in_array($cart['product_id'], $detailsProducts)){	 				
 				$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='1',purchase_id='".$data['purchaseId']."' where invoice_no ='".$invoice_no."' and product_id='".$cart['product_id']."' ");
 				$SI_no = $this->getSalesInvoiceBySO($invoice_no)['invoice_no'];
 				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='1',purchase_id='".$data['purchaseId']."' where invoice_no ='".$SI_no."' and product_id='".$cart['product_id']."' ");
 			}else{
 				$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='1',purchase_id='".$data['purchaseId']."' where invoice_no='".$invoice_no."' AND product_id = (SELECT parant_product_id FROM ".DB_PREFIX."order_child_items where order_no ='".$invoice_no."' and child_product_id ='".$cart['product_id']."') ");
 				$SI_no = $this->getSalesInvoiceBySO($invoice_no)['invoice_no'];
 				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='1',purchase_id='".$data['purchaseId']."' where  invoice_no ='".$SI_no."' and product_id= (SELECT parant_product_id FROM ".DB_PREFIX."order_child_items where order_no ='".$invoice_no."' and child_product_id ='".$cart['product_id']."') ");
 			}
 		}
		$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged='1' where invoice_no='".$invoice_no."'");
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged='1' where sales_trans_no='".$invoice_no."'");
 	}
 	public function checkSalesOrderToUpdate($invoice_no){
 		$taggedItems = $this->db->query("SELECT count(*) as m FROM ".DB_PREFIX."sales_detail where invoice_no='".$invoice_no."' AND tagged='1'")->row['m'];
 		if($taggedItems == '0'){
 			$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged='0' where invoice_no='".$invoice_no."'");
 			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged='0' where sales_trans_no='".$invoice_no."'");
 		}
 	}
 	public function getSalesInvoiceBySO($invoice_no){
 		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where sales_trans_no ='".$invoice_no."' ")->row;
 	}
 	public function getInventoryAutoId() {
 		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_last_id AS last_id  FROM " . DB_PREFIX . "entity_increment WHERE company_id = '" . (int)$company_id . "' AND entity_type_id = 2");
		return $query->row;
	}
  	public function WAM($pid,$qty,$price){
		 $cost  = '0';
		 $stp1  = '';
		 $stp2  = '';
		 $sql 	= "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
		 $query = $this->db->query($sql);
		 $res 	= $query->rows;
		 if(count($res)>=1){
		 	$stp1 = $qty * $price;
		 	$stp2 = $qty;
		 	for($i=0;$i<count($res);$i++){
		 		 $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
		 		 $stp2+= $res[$i]['quantity'];
		 	}
		 	   $cost = $stp1 / $stp2;
		 }else{
			   $cost = ($qty * $price) / $qty;
		 }
		 return $cost;
	}
	public function getPurchase($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND purchase_id = '" . (int)$purchaseId . "' "); //AND hold = '1' removed for convert all purchase (14-4-20)
		return $query->row;
	}

	public function getPurchaseProduct($purchaseId,$product_id='') {
		if($product_id){
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "' AND product_id = '" . (int)$product_id . "'");
		}else{
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '" . (int)$purchaseId . "'");
		}
		return $query->rows;
	}

	public function editPurchase($purchaseId, $data,$costMethod)
	{
			$data['reference_date']   = changeDate($data['reference_date']);
			$data['transaction_date'] = changeDate($data['transaction_date']);
			$data['arrival_date_from']= changeDate($data['arrival_date_from']);
			$data['arrival_date_to']  = changeDate($data['arrival_date_to']);

			$userId   = $this->session->data['user_id'];
			$userName = $this->session->data['username'];
			$data['userId']  = $userId;
			$purchase_return = 0;
			$total = 0;
			$gst   = 0;
			$sub_total =0;
			foreach ($data['totals'] as $total) {
				if($total['code']=='sub_total'){
					$sub_total = $total['value'];
				}else if($total['code']=='tax'){
					$gst = $total['value'];
				}else if($total['code']=='discount'){
					$discount = $total['value'];
				}else if($total['code']=='handling'){
					$handling_fee = $total['value'];
				}else if($total['code']=='total'){
					$data['total'] = $total = $total['value'];
				}
			}
			$purchase_header = $this->getPurchaseHeader($purchaseId);
			$data['conversion_rate'] = $purchase_header['conversion_rate'];
			
			$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      = $data['conversion_rate'] * $gst;
			$data['fc_discount'] = $data['conversion_rate'] * $discount;
			$data['fc_handling_fee'] = $data['conversion_rate'] * $handling_fee;
			$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];
			$attachmentStr = '';
			if($data['isAttachment']){
				$attachmentStr = " , attachment='".$data['attachment']."'";
			}
			$istagged = 0;
			if(!empty($data['invoice_no'])){
				$istagged = 1;
			}
			$this->db->query("UPDATE ".DB_PREFIX."purchase SET
				transaction_no 			= '".$data['transaction_no']."'
				, transaction_date 		= '".$data['transaction_date']."'
				, transaction_type 		= 'PURINV'
				, vendor_id 			= '".$data['vendor']."'
				, reference_no 			= '".$data['reference_no']."'
				, reference_date 		= '".$data['arrival_date_from']."'
				, arrival_date_to 		= '".$data['arrival_date_to']."'
				, remarks 				= '".$data['remarks']."'
				, bill_discount_percentage	= '".$data['bill_discount_percentage']."'
				, bill_discount_price 	= '".$data['bill_discount_price']."'
				, handling_fee 			= '".$data['fc_handling_fee']."'
				, total 				= '".$data['fc_nettotal']."'
				, sub_total 			= '".$data['fc_subtotal']."'
				, gst 					= '".$data['fc_tax']."'
				, discount 				= '".$data['fc_discount']."'
				, hold 					= '".$data['hold']."'
				, purchase_return 		= '".$purchase_return."'
				, location_code 		= '".$data['location_code']."'
				, tax_class_id 			= '".$data['tax_class_id']."'
				, tax_type 				= '".$data['tax_type']."'
				, fc_subtotal 			= '".$sub_total."'
				, fc_tax 				= '".$gst."'
				, fc_discount 			= '".$discount."'
				, fc_handling_fee 		= '".$handling_fee."'
				, fc_nettotal 			= '".$data['total']."'
				, term_id 				= '".$data['term_id']."'
				, customer_id 			= '".$data['customer_id']."'
				, shipping_id 			= '".$data['shipping_id']."'
				, istagged 				= '".$istagged."'
				, modified_by 			= '".$userName."'
				  ".$attachmentStr."
				, date_modified 		= '".date('Y-m-d H:i:s')."'
			WHERE purchase_id = '" . (int)$purchaseId . "'");

		foreach ($data['products'] as $product) {
				$discount_percentage = '';
				$discount_price      = '';

				if ($product['purchase_discount_mode'] == 1) {
				   $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price = $product['purchase_discount_value'];
				}

				$pqty  = (int)$product['quantity'];
				$price = (float)$product['price'];
				$unit_cost = $price;

				$pSku = $this->getSkuByProductId($product['product_id']);
				$location_code = $data['location_code'];
				if(empty($product['foc_quantity'])){
					$product['foc_quantity'] = 0;
				}else{
					$pqty = $pqty+$product['foc_quantity'];
				}
			$childAry['product_id'] = $product['product_id'];
			$childAry['order_no']   = $data['transaction_no'];
			$childAry['type']   	= 'PO';
			$this->cart->insertChildItems($childAry);

			if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
			    $product['total'] = $product['total'] - $product['tax_price'];
			}
			$existProduct = $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchaseId."' AND product_id='".$product['product_id']."' ")->row;
			if(!empty($existProduct)){
				$updateSql = "UPDATE ".DB_PREFIX."purchase_to_product set weight_class_id ='". (int)$product['weight_class_id']."', quantity ='". $this->db->escape($product['quantity'])."', price ='". $this->db->escape($product['price'])."', raw_cost ='".$this->db->escape($product['raw_cost'])."',tax_class_id='".$this->db->escape($product['tax_class_id'])."',net_price='".$this->db->escape($product['net_price'])."',tax_price='".$this->db->escape($product['tax_price'])."',total='".$this->db->escape($product['total'])."',avg_cost	='".$avg_price."',avg_method='".$avg_method."',foc='".$this->db->escape($product['foc_quantity'])."',transaction_no	='".$data['transaction_no']."',conversion_rate='".$data['conversion_rate']."', description='".$this->db->escape($product['description'])."' where purchase_id='".$purchaseId."' AND product_id='".$product['product_id']."'";
				$this->db->query($updateSql);
			
			}else{

				$this->db->queryNew("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage, discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,transaction_no,sku,conversion_rate,description) VALUES('" . (int)$purchaseId . "','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . $product['quantity'] . "','" . $this->db->escape($product['price']) . "','" . $this->db->escape($product['raw_cost']) . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$this->db->escape($product['tax_class_id']) . "','" . $this->db->escape($product['net_price']) . "','" . $this->db->escape($product['tax_price']) . "','" . $this->db->escape($product['total']) . "','" . $avg_price . "','" . $avg_method . "',".$this->db->escape($product['foc_quantity']).",'".$data['transaction_no']."','".$pSku."','".$data['conversion_rate']."','".$this->db->escape($product['description'])."')"); 
			}
			$insertedItems .= '"'.$product['product_id'].'",';
		}
		$data['purchaseId'] = $purchaseId;
		$this->updateTaggedSalesOrders($data);
		
		$insertedItems = substr($insertedItems, 0, -1);
		$this->db->query("DELETE FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchaseId."' AND product_id NOT IN (".$insertedItems.")");
		$this->db->query("DELETE FROM ".DB_PREFIX."order_child_items where order_no ='".$data['transaction_no']."' AND parant_product_id NOT IN (".$insertedItems.")");
		return $purchaseId;
	}
	public function getPurchaseByTransNo($transNumber)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "purchase WHERE company_id = '" . (int)$companyId . "' AND transaction_no = '" . $transNumber . "' AND purchase_return = '0'");
		return $query->row;
	}
	public function getPurchaseAvgCost($productId)
	{
		$query = $this->db->query("SELECT AVG(pt.price) AS average_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1'");
		return $query->row['average_cost'];
	}
	public function getPurchaseUnitCost($productId)
	{
		$query = $this->db->query("SELECT TOP 1 pt.price AS unit_cost FROM " . DB_PREFIX . "purchase_to_product AS pt LEFT JOIN " . DB_PREFIX . "purchase AS p ON pt.purchase_id = p.purchase_id WHERE pt.product_id = '" . (int)$productId . "' AND p.purchase_return != '1' ORDER BY pt.purchase_product_id DESC");
		return $query->row['unit_cost'];
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,p.package as hasChild FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)
				";
		if($data['filter_name']){
		   // $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";
		if($data['location_code']){ //newly added
			$sql.=" AND ps.location_Code = '".$data['location_code']."'";
		}

		if(!empty($data['filter_name'])) {
			// $sql .= " AND (p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR ";
			$sql .= " AND (p.sku_shortdescription LIKE '%".$data['filter_name']."%' OR";
			$sql .= " p.name LIKE '%".$this->db->escape($data['filter_name'])."%' OR";
			$sql .= " LOWER(p.sku) LIKE '%".$this->db->escape(utf8_strtolower($data['filter_name']))."%')";
		}
		// echo $sql; die;
		$query   = $this->db->query($sql);
		$results = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getProductForStockAdjust($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,p.package as hasChild FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)
				";
		if($data['filter_name']){
		   // $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1' AND p.package='0' ";
		if($data['location_code']){ //newly added
			$sql.=" AND ps.location_Code = '".$data['location_code']."'";
		}

		if(!empty($data['filter_name'])) {
			// $sql .= " AND (p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR ";
			$sql .= " AND (p.sku_shortdescription LIKE '%".$data['filter_name']."%' OR";
			$sql .= " p.name LIKE '%".$this->db->escape($data['filter_name'])."%' OR";
			$sql .= " LOWER(p.sku) LIKE '%".$this->db->escape(utf8_strtolower($data['filter_name']))."%')";
		}
		// echo $sql; die;
		$query   = $this->db->query($sql);
		$results = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getProductByNameNew($data = array()) {
		// $data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']); 
		
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		
		$sql.= " WHERE p.sku_status= '1'";
		if(!empty($data['filter_name'])) {

			$data['filter_name_convert']= html_entity_decode($data['filter_name']);
			$data['filter_name_entity'] = htmlentities($data['filter_name']);

			$sql .= " AND (p.sku_shortdescription LIKE '%".$this->db->escape($data['filter_name'])."%' OR ";
			$sql .= " p.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR ";
			$sql .= " p.name LIKE '%".$this->db->escape(trim($data['filter_name_convert']))."%' OR";
			$sql .= " p.name LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%' OR";
			$sql .= " p.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR ";
			$sql .= " p.sku LIKE '%".$this->db->escape(trim($data['filter_name_convert']))."%' OR ";
			$sql .= " p.sku LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%' )";
		}
		$sql.=" GROUP BY p.product_id";
		// echo $sql;
		return $this->db->query($sql)->rows;
	}
	public function updatebuyPrice($data) {
		$pId = $data['product_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "product SET purchanse_cost = '" . $data['raw_cost'] . "' WHERE product_id = '" . (int)$pId . "'");
	}
	public function getproductdetails($productId)
	{	 
		$sql = "SELECT p.product_id,p.sku,p.name,ps.sku_qty as quantity,ps.sku_avg as average_cost ,ps.sku_price as price, p.sku_vendor_code FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku) where p.product_id= '".$productId."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function export_purchase_summary_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-summary.csv');
		print "S No,Transaction Date,Transaction No,Supplier,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_date]\",\"$row[transaction_no]\",\"$row[vendor_name]\",\"$row[sub_total]\",\"$row[gst]\",\"$row[net_total]\"\r\n";
		}
	}
	public function export_purchase_details_to_csv($data){
		if(!$data) return false;
		ob_end_clean();
		header( 'Content-Type: text/csv' );
		header( 'Content-Disposition: attachment;filename=purchase-details.csv');
		print "S No,Transaction No,Inventory Code,Price,Qty,Total,GST,Net Total\r\n";
		for($i=0;$i<count($data);$i++)	{
			$row	= $data[$i];
			$sno 	= $i+1;
			print "\"$sno\",\"$row[transaction_no]\",\"$row[inventory_code]\",\"$row[price]\",\"$row[qty]\",\"$row[net_price]\",\"$row[tax_price]\",\"$row[total]\"\r\n";
		}
	}
	public function getPurchaseDetailsById($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT p.*,tc.title as tax_title FROM ".DB_PREFIX."purchase as p left join ".DB_PREFIX."tax_class as tc ON p.tax_class_id=tc.tax_class_id WHERE p.company_id = '" . (int)$companyId . "' AND p.purchase_id = '" . (int)$purchaseId . "'");
		return $query->row;
	}
	public function getPurchaseDetails($purchaseId){
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(quantity) as totalQty,product_id FROM " . DB_PREFIX . "purchase_to_product WHERE `purchase_id` = '".$purchaseId."'");
		return $query->row;
	}
	public function getProductDetailsById($purchaseId,$transaction_no=''){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT PP.discount_price,PP.purchase_product_id,PP.quantity as qty,PP.foc,PP.price as prices,PP.total,PP.net_price,PP.avg_cost,PP.tax_price,PP.conversion_rate,P.*,PP.description FROM ".DB_PREFIX."purchase_to_product as PP
			LEFT JOIN ".DB_PREFIX."product as P ON P.product_id = PP.product_id
			WHERE PP.purchase_id = '".$purchaseId."' and PP.transaction_no='".$transaction_no."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function getProductBarCode($productId){
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode_history WHERE product_id = '".$productId."'");
		return $query->row;
	}
	public function getVendorsTaxId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tax_method FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['tax_method'];
	}
	public function getVendor($vendor_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row;
	}
	public function getWastageheaderList($data)
	{
		$company_id	= $this->session->data['company_id'];	
		
		$sql = "SELECT DISTINCT *  FROM " .DB_PREFIX. "wastage_header where wastage_no != '' ";

		$filter_date_from = '';
		if($data['filter_date_from']!=''){
			$filter_date_from = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from'])));
		}
		$filter_date_to = '';
		if($data['filter_date_to']!=''){
			$filter_date_to = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));
		}

		if($filter_date_from!='' && $filter_date_to!=''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_from)  . "' AND '" . changeDate($filter_date_to) . "'";
		}

		if($filter_date_from!='' && $filter_date_to == ''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_from)  . "' AND '" . changeDate($filter_date_from) . "'";
		}

		if($filter_date_from =='' && $filter_date_to != ''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_to)  . "' AND '" . changeDate($filter_date_to) . "'";
		}

		if($data['filter_location']!='' ){
			$sql .= " AND location_code ='".$data['filter_location']."'";
		}

		$sql .= "order by wastage_date desc";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getWastageheaderListTotal($data)
	{
		$company_id	= $this->session->data['company_id'];	
		
		$sql = "SELECT COUNT(DISTINCT wastage_no) AS total,SUM(nettotal) as total_amount  FROM " .DB_PREFIX. "wastage_header where wastage_no != '' ";

		$filter_date_from = '';
		if($data['filter_date_from']!=''){
			$filter_date_from = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from'])));
		}
		$filter_date_to = '';
		if($data['filter_date_to']!=''){
			$filter_date_to = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));
		}

		if($filter_date_from!='' && $filter_date_to!=''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_from)  . "' AND '" . changeDate($filter_date_to) . "'";
		}

		if($filter_date_from!='' && $filter_date_to == ''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_from)  . "' AND '" . changeDate($filter_date_from) . "'";
		}

		if($filter_date_from =='' && $filter_date_to != ''){
			$sql .= " AND wastage_date between  '" . changeDate($filter_date_to)  . "' AND '" . changeDate($filter_date_to) . "'";
		}

		if($data['filter_location']!='' ){
			$sql .= " AND location_code ='".$data['filter_location']."'";
		}

		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getWastagedetailsList($data,$wastage_no)
	{
		$company_id	= $this->session->data['company_id'];
		
		$sql  = (" SELECT * FROM " . DB_PREFIX . "wastage_detail WHERE wastage_no = '".$wastage_no."'");

		// $sql = (" SELECT * FROM " . DB_PREFIX . "wastage_detail AS wd LEFT JOIN " . DB_PREFIX . "wastage_header AS wh ON wd.wastage_no = wh.wastage_no where wh.wastage_no = '".$wastage_no."' " );
		// if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
		// 	$sql .= " AND wh.wastage_date between  '" . changeDate($data['filter_date_from']) . "' AND '" . changeDate($data['filter_date_to']) . "'";
		// }

		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getWastagedetailsListTotal($data)
	{
		$company_id	= $this->session->data['company_id'];

		$sql = (" SELECT COUNT(wd.sku) AS total, sum(wd.subtotal) AS sub_total, sum(wd.gst) AS total_gst, sum(wd.nettotal) AS net_total, sum(wd.actualtotal) AS actual_total FROM " . DB_PREFIX . "wastage_detail AS wd LEFT JOIN " . DB_PREFIX . "wastage_header AS wh ON wd.wastage_no = wh.wastage_no where wh.wastage_no != '' " );

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND wh.wastage_date between  '" . changeDate($data['filter_date_from']) . "' AND '" . changeDate($data['filter_date_to']) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getLocation($location_code){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_code = '". $location_code ."'"; 
		$query = $this->db->query($sql);
		return $query->row;	
	}
	public function getpurchaseDetailsbyInvoice($invoiceno){
		$sql = "SELECT PP.*,P.sku as sku_name,P.name,P.sku_shortdescription as sku_description from " . DB_PREFIX . "purchase_to_product as PP  LEFT JOIN " . DB_PREFIX . "product as P on P.product_id=PP.product_id
				where PP.transaction_no='".$invoiceno."' order by PP.purchase_product_id asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function gePORequestDetailsById($transaction_no)
	{
		$sql = "SELECT 	Sku,Description,Qty,Skucost,Subtotal,Gst FROM " . DB_PREFIX . "Po_Req_Detail WHERE Po_Req_No = '" .$transaction_no . "'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getQtyDetailsByLocation($sku,$location_code)
	{
		$sql = "SELECT sku_qty FROM " . DB_PREFIX . "product_stock WHERE sku = '" .$sku . "' AND location_Code = '" .$location_code . "'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getReservedStockdetails($locationcode,$sku) {
		$sql ="select * from " . DB_PREFIX . "product_reserved_stock WHERE location_Code ='".$locationcode."' AND sku ='".$sku."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getpo_req_header($data){

    		
		$where="";
		if($data['filter_fromdate1'] !='' && $data['filter_todate1'] !=''){
			$where .=" where A.PO_Req_Date between '".$data['filter_fromdate1']."' AND '".$data['filter_todate1']."' ";
		}
		if($data['filter_department'] !=''){

		}
		if($data['filter_supplier'] !=''){
			$where .=" AND A.Vendor_Code = '".$data['filter_supplier']."' ";
		}
		$sql ="SELECT A.*,B.vendor_name as vendor_name from ".DB_PREFIX."po_req_header as A left join ".DB_PREFIX."vendor as B on a.Vendor_Code=b.vendor_code".$where;
       
       //print_r($sql);exit;
		$query = $this->db->query($sql);

		return $query->rows;
	}
	public function getpo_req_header_vendor(){
		$sql ="SELECT A.Vendor_Code as Vendor_Code,B.vendor_name as vendor_name from ".DB_PREFIX."po_req_header as A left join ".DB_PREFIX."vendor as B on a.Vendor_Code=b.vendor_code ";
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function get_po_req_detail($id){
		$sql 	= "SELECT Sku,Description,Qty from " . DB_PREFIX . "Po_Req_Detail where PO_Req_no='".$id."' ";
		$sql2	= "SELECT A.*,B.vendor_name as vendor_name from ".DB_PREFIX."po_req_header as A left join ".DB_PREFIX."vendor as B on a.Vendor_Code=b.vendor_code where A.PO_Req_no='".$id."' ";
		// echo $sql.'<br>';
		// echo $sql2; die;
		$data['details'] = $this->db->query($sql)->rows;
		$data['header']  = $this->db->query($sql2)->rows;
		return $data;
	}
	public function update_autopo_details($data){
		// echo "UPDATE ".DB_PREFIX."Po_Req_Detail set Qty='".$data['qty']."',Subtotal='".$data['subtotal']."',Nettotal='".$data['nettotal']."' where Sku='".$data['sku']."' "; die;
		$sql = $this->db->query("UPDATE ".DB_PREFIX."Po_Req_Detail set Qty='".$data['qty']."',Subtotal='".$data['subtotal']."',Nettotal='".$data['nettotal']."' where Sku='".$data['sku']."' ");
	}
	public function getproductbySKU($sku,$location){
		$sql = "SELECT p.product_id,p.sku,p.name,ps.sku_qty as quantity,ps.sku_cost as average_cost ,ps.sku_price as price, p.sku_vendor_code, p.sku_gstallowed as gst FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku) where p.sku= '".$sku."' and ps.location_Code='".$location."' ";
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function gettax_bycompany(){
		$sql = "SELECT * FROM " . DB_PREFIX . "tax_class WHERE company_id = '".$this->session->data['company_id']."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getCurrency() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE company_id = '".$company_id."'");
		return $query->rows;
	}
	public function checkCompanyCurrency($data){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."' and currency_code='".$data['currency']."' ");
		return $query->row;	
	}
	public function getcompanyCurrency(){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."' ")->row;
		return $query['currency_code'];	
	}
	public function updateInvoiceNumber($id){
		$lastId = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key`='config_next_purchase_invoiceno' ")->row;

		$update = $this->db->query("UPDATE ".DB_PREFIX."purchase_invoice_header set invoice = '".$lastId['value']."' where purchase_id='".$id."' ");
		if($update){
			$update_lastId = $this->db->query("UPDATE ".DB_PREFIX."setting set value = value+1 WHERE `key`='config_next_purchase_invoiceno' ");
		}
		return $lastId['value'];
	}
	public function updateStatus($id){
		// inner select query changed by |^| @ 14-04-2022 to improve speed
		$this->db->query("UPDATE ".DB_PREFIX."purchase SET deleted ='1', delete_notes='".$this->session->data['username']." - at : ".date('Y-m-d H:i:s')." from PO' WHERE purchase_id='".$id."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."tagged_sales_orders as tg ON sh.invoice_no = tg.order_no SET sh.istagged='0' where tg.purchase_id='".$id."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_detail as sd LEFT JOIN ".DB_PREFIX."tagged_sales_orders as tg ON sd.invoice_no = tg.order_no SET sd.tagged='0' where tg.purchase_id='".$id."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header as sh LEFT JOIN ".DB_PREFIX."tagged_sales_orders as tg ON sh.sales_trans_no = tg.order_no SET sh.istagged='0' where purchase_id='".$id."' ");

		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details as si LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON si.invoice_no=sh.invoice_no SET si.tagged='0' where sh.sales_trans_no in (SELECT order_no from ".DB_PREFIX."tagged_sales_orders where purchase_id='".$id."' )");
	}
	public function getProductName($id){
		$sql= $this->db->query("SELECT name FROM ".DB_PREFIX."product where product_id='".$id."' ")->row;
		return $sql['name'];
	}
	public function deletePurchase($purchase_id){
		$this->db->query("UPDATE ".DB_PREFIX."purchase set deleted='1', delete_notes='".$this->session->data['username']." - at : ".date('Y-m-d H:i:s')." from single delete fn' where purchase_id='".$purchase_id."'");
	}
	public function updatechildProductQty($product_id,$qty){ // not used
		$child_product = $this->db->query("SELECT child_sku,quantity FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ")->rows;
		$quantity = $value['quantity'] * $qty;

		foreach ($child_product as $value) {
			$update = $this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'" .$quantity."' WHERE sku = '".$value['child_sku']."' ");
		}
	}
	public function getPurchaseAutoId(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase_invoice_header ORDER BY purchase_id DESC LIMIT 1");
		return $query->row['purchase_id'];
	}
	public function addPurchaseInvoice($data,$cost_method=''){
		
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		if(empty($data['bill_discount_price'])) {
			$data['bill_discount_price'] = '0.00';
		}
		if($data['bill_discount_price']>0 || $data['bill_discount_percentage']>0){
			$bill_discount = 1;
		}
		$purchase_return = 0;
		$data['reference_date']    = changeDates($data['reference_date']);
  	    $data['transaction_date']  = changeDates($data['transaction_date']);
  	    $data['arrival_date_from'] = changeDates($data['arrival_date_from']);
  	    $data['arrival_date_to']   = changeDates($data['arrival_date_to']);

		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}
		}
		if($data['currency_code']=='SGD'){
			$data['conversion_rate'] = '1';
		}
		
		$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
		$data['fc_tax']      = $data['conversion_rate'] * $gst;
		$data['fc_discount'] = $data['conversion_rate'] * $discount;
		$data['fc_handling_fee'] = $data['conversion_rate'] * $handling_fee;
		$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];

		$sql = "INSERT INTO " . DB_PREFIX . "purchase_invoice_header (company_id,transaction_no,transaction_date,transaction_type,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,handling_fee,total,purchase_return,location_code,sub_total,gst,created_by,modified_by,discount,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,purchase_trans_no,tax_class_id,tax_type,term_id,arrival_date_to,attachment,customer_id,shipping_id) VALUES('" . (int)$companyId . "','" . $data['transaction_no'] . "','" . $data['transaction_date'] . "','PURINV','" . $data['vendor'] . "','" . $data['reference_no'] . "','" . $data['arrival_date_from'] . "','" . $data['remarks'] . "','" . $data['bill_discount_percentage'] . "','" . $data['bill_discount_price'] . "','".$data['fc_handling_fee']."','" . $data['fc_nettotal'] . "','".$purchase_return."','" . $data['location_code'] . "','" . $data['fc_subtotal']. "','" . $data['fc_tax']. "','".$userName."','".$userName."','".$data['fc_discount']."','".$data['currency_code']."','".$data['conversion_rate']."','".$subtotal."','".$gst."','".$discount."','".$handling_fee."','".$data['total']."','".$data['oldPurchase_id']."','".$data['tax_class_id']."','".$data['tax_type']."','".$data['term_id']."','".$data['arrival_date_to']."','".$data['attachment']."','".$data['customer_id']."','".$data['shipping_id']."')";

		$res = $this->db->queryNew($sql);
		$purchaseId = $this->db->getLastId();
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase_invoice/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);	
				exit;
		}

		$this->updateInvoiceNumber($purchaseId);
		$orderDiscount = 0;
		foreach ($data['products'] as $product) {
				$orderDiscount+=$pro = '';
				if ($product['purchase_discount_mode'] == 1) {
				    $discount_percentage = $product['purchase_discount_value'];
				} elseif ($product['purchase_discount_mode'] == 2) {
					$discount_price      = $product['purchase_discount_value'];
				}
				$pqty  	  = (float)$product['quantity'];
				$price 	  = (float)$product['price'];
				$pSku  	  = $this->getSkuByProductId($product['product_id']);
				$unit_cost= $price;
			
			if($product['foc_quantity'] >=1 ){
				$pqty = $pqty+$product['foc_quantity'];
			}
			$child['product_id']    = $product['product_id'];
			$child['qty']           = $pqty;
			$child['location_code'] = $data['location_code'];

			$this->cart->updatechildProductQty($child,'P');
			$productDetails = $this->getproductdetails($product['product_id']);
			if($data['config_vendor_update']=='1'){
				if($productDetails['sku_vendor_code'] != $data['vendor']){
					// $this->db->queryNew("UPDATE ".DB_PREFIX."product SET sku_vendor_code ='".$data['vendor']."' where product_id='".$product['product_id']."' ");
					// dissabled ad 19 - jully by ragu
				}
			}
			$location_code = $data['location_code'];
			$unit_cost 	   = $unit_cost * $data['conversion_rate'];
			if($cost_method=='1'){
				//Weighted Average method calculation
				$avg_method ='WAM';
				$price 		= $price * $data['conversion_rate'];
				$avg_price  = $this->WAM($product['product_id'],$pqty,$price);
				$avgCostupdate = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty=sku_qty+'".$pqty."', sku_avg='".$avg_price."',sku_cost='".$unit_cost."',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='".$location_code."'"; 
					
		   	}else{
		   		//FIFO method
				$avg_method 	='FIFO';
				$avgCostupdate 	= "UPDATE " . DB_PREFIX . "product_stock SET sku_qty=sku_qty+'".$pqty."',  sku_cost='".$unit_cost."',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='".$location_code."'"; 
	    	}
	    	
				if(empty($product['foc_quantity'])){
					$product['foc_quantity'] = 0;
				}
			    $product['total'] = $product['total'] + $product['tax_price']; 	
				if($data['oldPurchase_id'] ==''){
					$product['original_quantity'] = $product['quantity'];
				}
				
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
				    $product['net_price'] = $product['total'];
				}				
				if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
				    $product['total'] = $product['total'] - $product['tax_price'];
				}

				$sql = "INSERT INTO " . DB_PREFIX . "purchase_invoice_details (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,transaction_no,sku,conversion_rate,original_qty,description) VALUES('".$purchaseId."','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . $product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "','".$product['foc_quantity']."', '".$data['transaction_no']."','" . $pSku . "','".$data['conversion_rate']."','".(int)$product['original_quantity']."','".$this->db->escape($product['description'])."')"; 
				// echo $sql; die;
				$res = $this->db->queryNew($sql);
			if(isset($data['oldPurchase_id'])){
				$this->updatePurchaseToProduct($data['oldPurchase_id'],$product['product_id'],$product['quantity']);
			}
 		    if($res){
 		    	$this->db->query($avgCostupdate); 
		    }else{
		   		$delSql = "DELETE " . DB_PREFIX . "purchase_invoice_header where purchase_id='".$purchaseId."'";
				$this->db->query($delSql);
				exit;
		   }
		}
		$this->updatePurchaseIsinvoice($data['oldPurchase_id']);
		return $purchaseId;
	}
	/*When insert purchase invoice*/
	public function updatePurchaseToProduct($oldPurchase_id,$product_id,$received_qty){
		$this->db->query("UPDATE ".DB_PREFIX."purchase_to_product set received_qty=received_qty +'".$received_qty."', foc='0' where purchase_id='".$oldPurchase_id."' AND product_id='".$product_id."' ");
		$qtys = $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$oldPurchase_id."' AND product_id='".$product_id."' ")->row;

		$conversion_status = '1';
		if($qtys['quantity'] <= $qtys['received_qty']){
			$conversion_status = '2';
		};
		$this->db->query("UPDATE ".DB_PREFIX."purchase_to_product set conversion_status='".$conversion_status."' where purchase_id='".$oldPurchase_id."' AND product_id='".$product_id."' ");
		return $conversion_status;
	}	
	/*When insert purchase invoice*/
	public function updatePurchaseIsinvoice($purchase_id)
	{
		$allCount = $this->db->query("SELECT count(*) as allCount FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchase_id."' ")->row['allCount'];

		$completedQty = $this->db->query("SELECT count(*) as completedQty FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchase_id."' AND conversion_status='2' ")->row['completedQty'];

		if($allCount == $completedQty){
			$this->db->query("UPDATE ".DB_PREFIX."purchase set isinvoice='1' where purchase_id='".$purchase_id."' ");
		}
	}

	public function getTotalPurchaseInvoice($data)
	{
	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(purchase_id) AS totalPurchase, sum(total) AS grandtotal FROM " . DB_PREFIX . "purchase_invoice_header";
		$sql .= " WHERE company_id ='".(int)$company_id."' AND purchase_return = '0' AND deleted='0' ";
		
		if($data['filter_xero'] != 0){
			if($data['filter_xero'] == '1') {
				$sql .= " AND xero_purchase_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND xero_purchase_id  IS NULL ";
			}
		}
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND (transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getPurchaseInvoiceList($data)
	{
	    $company_id	= $this->session->data['company_id'];	
		$sql = "SELECT P.purchase_id,P.transaction_no,P.reference_no,P.reference_date,P.location_code,P.transaction_date,P.transaction_type,P.total,P.sub_total,P.gst,P.invoice,P.vendor_id,P.created_by,P.created_date,V.vendor_code,V.vendor_name,U.firstname,U.lastname,P.xero_purchase_id,P.fc_nettotal FROM " . DB_PREFIX . "purchase_invoice_header as P 
			LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_id=P.vendor_id
			LEFT JOIN " . DB_PREFIX . "user as U on U.user_id=P.created_by";

		$sql.=" WHERE deleted='0' ";

		if($data['filter_supplier']){
			$sql .= " AND P.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND P.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_xero'] != 0){
			if($data['filter_xero'] == '1') {
				$sql .= " AND P.xero_purchase_id  IS NOT NULL ";
			} else if ($data['filter_xero'] == '2') {
				$sql .= " AND P.xero_purchase_id  IS NULL ";
			}
		}
		if($data['filter_transactionno']){
			$sql .= " AND (P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR P.reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
						if($data['filter_reference_date']){
				$sql .= " AND P.reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND P.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			
		}
		$sql .= " ORDER BY P.purchase_id DESC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getPurchaseInvoiceDetailsById($purchaseId)
	{
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT p.*,tc.title as tax_title FROM ".DB_PREFIX."purchase_invoice_header as p left join ".DB_PREFIX."tax_class as tc ON p.tax_class_id=tc.tax_class_id WHERE p.company_id = '" . (int)$companyId . "' AND p.purchase_id = '" . (int)$purchaseId . "'");
		return $query->row;
	}
	public function getPurchaseInvoiceDetails($purchaseId){
		$query = $this->db->query("SELECT COUNT(*) as totalItem, SUM(quantity) as totalQty,product_id FROM " . DB_PREFIX . "purchase_invoice_details WHERE `purchase_id` = '".$purchaseId."'");
		return $query->row;
	}
	public function getInvoice_ProductDetailsById($purchaseId,$transaction_no=''){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT PP.discount_price,PP.purchase_product_id,PP.quantity as qty,PP.foc,PP.price as prices,PP.total,PP.net_price,PP.avg_cost,PP.tax_price,PP.conversion_rate,P.*,PP.description FROM " . DB_PREFIX . "purchase_invoice_details as PP
			LEFT JOIN " . DB_PREFIX . "product as P ON P.product_id = PP.product_id
			WHERE PP.purchase_id = '".$purchaseId."' and PP.transaction_no='".$transaction_no."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function getProductInvoiceDetailsById($purchaseId,$transaction_no=''){
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT PP.discount_price,PP.purchase_product_id,PP.quantity as qty,PP.foc,PP.price as prices,PP.total,PP.net_price as nettotal,PP.avg_cost,PP.tax_price,P.*, PP.description FROM ".DB_PREFIX."purchase_invoice_details as PP
			LEFT JOIN ".DB_PREFIX."product as P ON P.product_id = PP.product_id
			WHERE PP.purchase_id = '".$purchaseId."' and PP.transaction_no='".$transaction_no."' ORDER BY PP.purchase_product_id ASC");
		return $query->rows;
	}
	public function deletePurchaseInvoice($purchase_id){
		$sql= $this->db->query("UPDATE ".DB_PREFIX."purchase_invoice_header set deleted='1' where purchase_id='".$purchase_id."'");
	}
	public function getVendorCurrency($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['currency_code'];
	}
	public function editPurchaseInvoice($purchaseId,$data,$cost_method='')
	{
			$data['reference_date']   = changeDates($data['reference_date']);
			$data['transaction_date'] = changeDates($data['transaction_date']);

			$userId   = $this->session->data['user_id'];
			$userName = $this->session->data['username'];
			$purchase_return = 0;
			$total = 0;
			$gst   = 0;
			$sub_total =0;
			foreach ($data['totals'] as $total) {
				if($total['code']=='sub_total'){
					$sub_total = $total['value'];
				}else if($total['code']=='tax'){
					$gst = $total['value'];
				}else if($total['code']=='discount'){
					$discount = $total['value'];
				}else if($total['code']=='handling'){
					$handling_fee = $total['value'];
				}else if($total['code']=='total'){
					$data['total'] = $total['value'];
				}
			}
			$purchase_header = $this->getPurchaseInvoiceHeader($purchaseId);
			$data['conversion_rate'] = $purchase_header['conversion_rate'];
			
			$data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
			$data['fc_tax']      = $data['conversion_rate'] * $gst;
			$data['fc_discount'] = $data['conversion_rate'] * $discount;
			$data['fc_handling_fee'] = $data['conversion_rate'] * $handling_fee;
			$data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];
			

			$updSql = "UPDATE " . DB_PREFIX . "purchase_invoice_header SET 
			  bill_discount_percentage = '" . $data['bill_discount_percentage'] . "'
			, bill_discount_price = '" . $data['bill_discount_price'] . "'
			, handling_fee = '" . $data['fc_handling_fee'] . "'
			, tax_class_id = '".$data['tax_class_id']."'
			, tax_type = '".$data['tax_type']."'
			, total = '" . $data['fc_nettotal'] . "'
			, sub_total = '" . $data['fc_subtotal'] . "'
			, gst = '" . $data['fc_tax'] . "'
			, discount = '" . $data['fc_discount'] . "' 
			, fc_subtotal = '".$sub_total."'
			, fc_tax = '".$gst."'
			, fc_discount = '".$discount."'
			, fc_handling_fee = '".$handling_fee."'
			, fc_nettotal = '".$data['total']."' 
			WHERE purchase_id = '" .$purchaseId . "'";
			// echo $updSql; die;
			$this->db->query($updSql);

		// foreach ($data['products'] as $product) {
		// 		$orderDiscount+=$pro = '';
		// 		if ($product['purchase_discount_mode'] == 1) {
		// 		    $discount_percentage = $product['purchase_discount_value'];
		// 		} elseif ($product['purchase_discount_mode'] == 2) {
		// 			$discount_price      = $product['purchase_discount_value'];
		// 		}
		// 		/******************************/
		// 		$pqty  = (float)$product['quantity'];
		// 		$price = (float)$product['price'];
		// 		// need to calculate avg cost method here
		// 	$pSku = $this->getSkuByProductId($product['product_id']);
		// 	$unit_cost = $price;
			
		// 	if($product['foc_quantity']>=1){
		// 		$pqty = $pqty+$product['foc_quantity'];
		// 	}

		// 	$productDetails = $this->getproductdetails($product['product_id']);
		// 	if($data['config_vendor_update']=='1'){
		// 		if($productDetails['sku_vendor_code'] != $data['vendor']){
		// 			// $this->db->queryNew("UPDATE ".DB_PREFIX."product SET sku_vendor_code ='".$data['vendor']."' where product_id='".$product['product_id']."' "); 
		// 		}
		// 	}
		// 	$location_code = $data['location_code'];
		// 	$unit_cost = $unit_cost * $data['conversion_rate'];
	 //    	/************************************/
	 //    		if(empty($product['raw_cost'])) {
		// 			$product['raw_cost'] = '0.00';
		// 		}
		// 		if(empty($product['price'])) {
		// 			$product['price'] = '0.00';
		// 		}
		// 		if(empty($product['net_price'])) {
		// 			$product['net_price'] = '0.00';
		// 		}
		// 		if(empty($product['tax_price'])) {
		// 			$product['tax_price'] = '0.00';
		// 		}
		// 		if(empty($product['total'])) {
		// 			$product['total'] = '0.00';
		// 		}
		// 		if(empty($discount_price)) {
		// 			$discount_price = '0.00';
		// 		}
		// 		if(empty($avg_price)) {
		// 			$avg_price = '0.00';
		// 		}
		// 		if(empty($product['foc_quantity'])){
		// 			$product['foc_quantity'] = 0;
		// 		}

		// 	    $product['total'] = $product['total'] + $product['tax_price']; 	
		// 		if($data['oldPurchase_id'] ==''){
		// 			$product['original_quantity'] = $product['quantity'];
		// 		}
		// 		if($data['tax_class_id'] == '2' && $data['tax_type'] =='1'){
		// 		    $product['net_price'] = $product['total'] + $product['tax_price'];
		// 		}				
		// 		if($data['tax_class_id'] == '2' && $data['tax_type'] =='2'){
		// 		    $product['total'] = $product['total'] - $product['tax_price'];
		// 		}
		// 		$existProduct = $this->checkPurchaseInvoiceDetails($purchaseId,$product['product_id']);
		// 		if(!empty($existProduct)){
		// 			// $this->updateProductQty($product['product_id'],$existProduct['quantity'],$product['quantity']);
		// 			$sql = "UPDATE ".DB_PREFIX."purchase_invoice_details set quantity='".$product['quantity']."', price='".$product['price']."', raw_cost='".$product['raw_cost']."', discount_percentage='".$discount_percentage."', discount_price='".$discount_price."', tax_class_id='".$product['tax_class_id']."', net_price='".$product['net_price']."', avg_method='".$avg_method."' where purchase_id='".$purchaseId."' AND product_id='".$product['product_id']."' "; 
		// 		}else{

		// 			$sql = "INSERT INTO " . DB_PREFIX . "purchase_invoice_details (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,foc,transaction_no,sku,conversion_rate,description) VALUES('".$purchaseId."','" . (int)$product['product_id'] . "','" . (int)$product['weight_class_id'] . "','" . $product['quantity'] . "','" . $product['price'] . "','" . $product['raw_cost'] . "','" . $discount_percentage . "','" . $discount_price . "','" . (int)$product['tax_class_id'] . "','" . $product['net_price'] . "','" . $product['tax_price'] . "','" . $product['total'] . "','" . $avg_price . "','" . $avg_method . "','".$product['foc_quantity']."', '".$data['transaction_no']."','" . $pSku . "','".$data['conversion_rate']."','".$this->db->escape($product['description'])."')";

		// 			$unit_cost     = $product['price'] * $data['conversion_rate'];
		// 			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty=sku_qty+'".$pqty."',  sku_cost='".$unit_cost."',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='".$location_code."'"); 

		// 		}
		// 		$res = $this->db->query($sql);

		// 	$insertedItems .= '"'.$product['product_id'].'",';
		// }
		// $insertedItems = substr($insertedItems, 0, -1);
		// $this->db->query("DELETE FROM ".DB_PREFIX."purchase_invoice_details where purchase_id='".$purchaseId."' AND product_id NOT IN (".$insertedItems.")");
		return $purchaseId;
	}
	public function updateProductQty($product_id,$oldQty,$newQty){
		if($oldQty > $newQty){
			$qty = $oldQty - $newQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'".$qty."' WHERE product_id = '".$product_id."' ");
		}else{
			$qty = $newQty - $oldQty;
			$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'".$qty."' WHERE product_id = '".$product_id."' ");
		}
	}
	public function checkPurchaseInvoiceDetails($purchase_id,$product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_invoice_details WHERE purchase_id = '".$purchase_id."' AND product_id='".$product_id."' ")->row;
	}
	public function getPurchaseInvoiceHeader($purchaseId)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_invoice_header WHERE purchase_id = '".$purchaseId."' ")->row;
	}
	public function getPurchaseHeader($purchaseId)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase WHERE purchase_id = '".$purchaseId."' ")->row;
	}
	public function getPurchaseInvoiceDetails_Id($purchaseId)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_invoice_details WHERE purchase_id = '".$purchaseId."' ")->rows;
	}
	public function getpurchaseinvoiceDetailsbyInvoice($invoiceno){
		$sql = "SELECT PP.*,P.sku as sku_title,P.sku_description,P.name from " . DB_PREFIX . "purchase_invoice_details as PP  LEFT JOIN " . DB_PREFIX . "product as P on P.product_id= PP.product_id
				where PP.transaction_no='".$invoiceno."' order by PP.purchase_product_id asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getVendorTerm($vendor_id){
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['term_id'];
	}
	public function getTotalPursCompSummary($data){
		$sql ="SELECT count(a.sku) as total from tbl_purchase_to_product a
               inner join tbl_purchase b on a.purchase_id=b.purchase_id
               inner join tbl_product c on c.sku=a.sku where b.transaction_date!=''";

        if($data['filter_transactionno']){
            $sql .= " AND b.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
        }
        if($data['filter_department']){
            $sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
        }
        if($data['filter_category']){
            $sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        }
        if($data['filter_location']){
            $sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
        }
        if($data['filter_product_id']){
            $sql .= " AND a.product_id = '".$data['filter_product_id']."' ";
        }
        else if($data['filter_sku']){
        	$data['filter_sku'] = replaceQuotes($data['filter_sku']);
            $sql .= " AND a.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
        }
        if($data['filter_brand']){
			$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if($data['filter_vendor']){
			$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}

        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $data['filter_date_to']  = changeDates($data['filter_date_to']);
            
            $sql .= " AND b.transaction_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
            
        } else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
            $data['filter_date_from'] = changeDates($data['filter_date_from']);
            $sql .= " AND b.transaction_date = '" . $data['filter_date_from'] . "'";
            
        } else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
            $data['filter_date_to'] = changeDates($data['filter_date_to']);
            $sql .= " AND b.transaction_date = '" . $data['filter_date_to'] . "'";
        }
        $query = $this->db->query($sql);
        return $query->row;
	}
	public function getPurchaseCompSummaryList($data)
	{ 
		$sql ="SELECT b.location_code,c.sku_department_code, c.sku_category_code, c.sku_brand_code,c.sku_vendor_code, c.sku,sum(a.quantity) qty,sum(a.received_qty) received_qty,
			   d.department_name,br.brand_name,cat.category_name from tbl_purchase_to_product a
               inner join tbl_purchase b on a.purchase_id=b.purchase_id
               inner join tbl_product c on c.product_id=a.product_id 
			   left join tbl_department d on d.department_code=c.sku_department_code 
			   left join tbl_category cat on cat.category_code=c.sku_category_code
 			   left join tbl_brand br on br.brand_code=c.sku_brand_code  where b.transaction_date!=''"; 

				if($data['filter_transactionno']){
		            $sql .= " AND b.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		        }
		        if($data['filter_department']){
		            $sql .= " AND c.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		        }
		        if($data['filter_category']){
		            $sql .= " AND c.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		        }
		        if($data['filter_location']){
		            $sql .= " AND b.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		        } 
				if($data['filter_product_id']){
		            $sql .= " AND a.product_id = '".$data['filter_product_id']."' ";
		        }
		        else if($data['filter_sku']){
		        	$data['filter_sku'] = replaceQuotes($data['filter_sku']);
		            $sql .= " AND c.sku LIKE '%" . $this->db->escape($data['filter_sku']) . "%'";
		        }
		        if($data['filter_brand']){
					$sql .= " AND c.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
				}
				if($data['filter_vendor']){
					$sql .= " AND c.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
				}

		        if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
		            $data['filter_date_from'] = changeDates($data['filter_date_from']);
		            $data['filter_date_to']  = changeDates($data['filter_date_to']);
		            
		            $sql .= " AND b.transaction_date between   '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		            
		        } else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
		            $data['filter_date_from'] = changeDates($data['filter_date_from']);
		            $sql .= " AND b.transaction_date = '" . $data['filter_date_from'] . "'";
		            
		        } else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
		            $data['filter_date_to'] = changeDates($data['filter_date_to']);
		            $sql .= " AND b.transaction_date = '" . $data['filter_date_to'] . "'";
		        }
			$sql.= 'group by c.sku';
			/*$sql.= ' group by  c.sku_department_code, c.sku_category_code, c.sku_brand_code, c.sku_vendor_code, a.sku, b.location_code,d.department_name,br.brand_name,cat.category_name';
			$sql.= ' order by c.sku_department_code, c.sku_category_code, c.sku_brand_code, a.sku';*/

	
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			if($data['filterVal']!='1'){
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function checkPurchaseCovertversion($invoice_id){
		$data['total_records'] = $this->db->query("SELECT count(*) as total_records FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$invoice_id."' ")->row['total_records'];

		$data['completed_records'] = $this->db->query("SELECT count(*) as completed_records FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$invoice_id."' AND conversion_status ='2' ")->row['completed_records'];

		$data['partial_records'] = $this->db->query("SELECT count(*) as partial_records FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$invoice_id."' AND conversion_status ='1' ")->row['partial_records'];
		return $data;	
	}
	public function getVendorId($purchase_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase where purchase_id='".$purchase_id."' ")->row['vendor_id'];
	}
	public function getPurchaseProductDetails($purchase_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchase_id."'")->rows;
	}
    public function getPurchaseProductDetailsForConvert($purchase_id){
        return $this->db->query("SELECT * FROM ".DB_PREFIX."purchase_to_product where purchase_id='".$purchase_id."' AND conversion_status !='2' ")->rows;
    }
	public function AddCopyOrders($data){
		
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$location	= $this->session->data['location_code'];
		
		foreach ($data['totals'] as $total) {
			if($total['code']=='sub_total'){
				$sub_total = $total['value'];
			}else if($total['code']=='tax'){
				$gst = $total['value'];
			}else if($total['code']=='handling'){
				$handling_fee = $total['value'];
			}else if($total['code']=='total'){
				$data['total'] = $total['value'];
			}else if($total['code']=='discount'){
				$discount = $total['value'];
			}
		}
		$tax_class_id = $data['tax_class_id'] == '0' ? 1 : 2;
		$tax_type     = $tax_class_id == '2' ? $data['tax_class_id'] : 1;
		$arrivalFrom  = date('Y-m-d');
		$arrivalTo    = date('Y-m-d',strtotime('+1 day'));
		$transaction_date = date('Y-m-d');
		$sql = "INSERT INTO ".DB_PREFIX."purchase (company_id,transaction_no,transaction_date,transaction_type,vendor_id,total,purchase_return,location_code,sub_total,gst,created_by,modified_by,discount,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_nettotal,handling_fee,tax_class_id,tax_type,fc_discount,fc_handling_fee,term_id,reference_date,arrival_date_to,customer_id,shipping_id) VALUES('".$companyId."','".$data['trans_no']."','".$transaction_date."','PURINV','".$data['vendor']."','".$data['total']."','0','".$location."','".$sub_total."','".$gst."','".$userName."','".$userName."','".$discount."','SGD','1','".$sub_total."','".$gst."','".$data['total']."','".$handling_fee."','".$tax_class_id."','".$tax_type."','".$discount."','".$handling_fee."','".$data['term_id']."','".$arrivalFrom."','".$arrivalTo."','".$data['customer_id']."','".$data['shipping_id']."')";
		// echo $sql; die;

		$this->db->queryNew($sql);
		$purchase_id = $this->db->getLastId();
		foreach ($data['products'] as $product) {

			$pqty  = (int)$product['quantity'];
			$price = (float)$product['price'];

			$productDetails = $this->getproductdetails($product['product_id']);
			if($data['config_vendor_update']=='1'){
				if($productDetails['sku_vendor_code'] != $data['vendor']){
					$this->db->queryNew("UPDATE ".DB_PREFIX."product SET sku_vendor_code ='".$data['vendor']."' where product_id='".$product['product_id']."' ");
				}
			}

			$res = $this->db->queryNew("INSERT INTO " . DB_PREFIX . "purchase_to_product (purchase_id,product_id,quantity,price,raw_cost,tax_class_id,net_price,tax_price,total,transaction_no,sku,conversion_rate,description) VALUES('" . (int)$purchase_id . "','".$this->db->escape($product['product_id'])."','".$pqty."','".$this->db->escape($product['price']) ."','".$this->db->escape($product['raw_cost'])."','".$this->db->escape($product['tax_class_id'])."','".$this->db->escape($product['sub_total'])."','".$this->db->escape($product['purchase_tax'])."','".$this->db->escape($product['total'])."','".$data['trans_no']."','".$this->db->escape($product['sku']) . "','1','".$this->db->escape($product['description'])."')");
			   
		   if(!$res){
		   		$this->db->query("DELETE ".DB_PREFIX."purchase where purchase_id='".$purchaseId."'"); exit;
		   }
		}
		return $purchase_id;
	}

	public function addBulkPurchaseInvoice($data)
    {
		$companyId	= $this->session->data['company_id'];
		$userId		= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];

		$data['reference_date']   = date('Y-m-d');
		$data['transaction_date'] = date('Y-m-d');
		$data['arrival_date_to']  = date('Y-m-d',strtotime('+1 day'));

        foreach ($data['totals'] as $total) {
            if($total['code']=='sub_total'){
                $sub_total = $total['value'];
            }else if($total['code']=='tax'){
                $gst = $total['value'];
            }else if($total['code']=='handling'){
                $handling_fee = $total['value'];
            }else if($total['code']=='total'){
                $data['total'] = $total['value'];
            }else if($total['code']=='discount'){
                $discount = $total['value'];
            }
        }
        if($data['currency_code']=='SGD'){
            $data['conversion_rate'] = '1';
        }
        
        $data['fc_subtotal'] = $data['conversion_rate'] * $sub_total;
        $data['fc_tax']      = $data['conversion_rate'] * $gst;
        $data['fc_discount'] = $data['conversion_rate'] * $discount;
        $data['fc_handling_fee'] = $data['conversion_rate'] * $handling_fee;
        $data['fc_nettotal'] = $data['conversion_rate'] * $data['total'];

		$sql = "INSERT INTO " . DB_PREFIX . "purchase_invoice_header (company_id,transaction_no,transaction_date,transaction_type,purchase_trans_no,vendor_id,reference_no,reference_date,remarks,bill_discount_percentage,bill_discount_price,handling_fee,tax_class_id,tax_type,total,purchase_return,created_by,modified_by,location_code,sub_total,gst,discount,currency_code,conversion_rate,fc_subtotal,fc_tax,fc_discount,fc_handling_fee,fc_nettotal,term_id,arrival_date_to,customer_id,shipping_id) VALUES('".$companyId."','".$data['transaction_no']."','".$data['transaction_date']."','PURINV','".$data['oldPurchase_id']."','".$data['vendor_id']."','".$data['oldTransaction_no']."','".$data['reference_date']."','".$data['remarks']."','".$data['bill_discount_percentage']."','".$data['bill_discount_price']."','".$data['fc_handling_fee']."','".$data['tax_class_id']."','".$data['tax_type']."','".$data['fc_nettotal']."','0','".$userName."','".$userName."','".$data['location_code']."','".$data['fc_subtotal']."','".$data['fc_tax']."','".$data['fc_discount']."','".$data['currency_code']."','".$data['conversion_rate']."','".$sub_total."','".$gst."','".$discount."','".$handling_fee."','".$data['total']."','".$data['term_id']."','".$data['arrival_date_to']."','".$data['customer_id']."','".$data['shipping_id']."')";

		$res 		= $this->db->queryNew($sql);
		$purchaseId = $this->db->getLastId();
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=transaction/purchase_invoice/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);	
				exit;
		}
		if(isset($data['oldPurchase_id'])){
			$this->db->query("UPDATE ".DB_PREFIX."purchase_to_product set conversion_status='2', received_qty=quantity where purchase_id='".$data['oldPurchase_id']."' ");
			$this->db->query("UPDATE ".DB_PREFIX."purchase set isinvoice='1' where purchase_id='".$data['oldPurchase_id']."'");
		}
		foreach ($data['products'] as $product) {
			
            $pqty      = (int)$product['quantity'] - $product['received_qty'];
			$price     = (float)$product['price'];
			$pSku      = $this->getSkuByProductId($product['product_id']);
			$unit_cost = $price;

			if($product['foc']>=1){
				$pqty = $pqty + $product['foc'];
			}

            if($pqty > 0){

    			$productDetails = $this->getproductdetails($product['product_id']);
    			if($data['config_vendor_update']=='1'){
    				if($productDetails['sku_vendor_code'] != $data['vendor']){
    					$this->db->queryNew("UPDATE ".DB_PREFIX."product SET sku_vendor_code ='".$data['vendor']."' where product_id='".$product['product_id']."' ");
    				}
    			}
    			$child['product_id']    = $product['product_id'];
    			$child['qty']    		= $pqty;
    			$child['location_code'] = $data['location_code'];
    			$this->cart->updatechildProductQty($child,'P');

    			$cost_method   = $this->config->get('config_average_cost_method');
    			$location_code = $data['location_code'];
    			$unit_cost     = $unit_cost * $data['conversion_rate'];
    			$avg_price 	   = 0;
    			if($cost_method == '1'){
    				//Weighted Average method calculation
    				$avg_method ='WAM';
    				$price = $price * $data['conversion_rate'];
    				$avg_price  = $this->WAM($product['product_id'],$pqty,$price);
    				$avgCostupdate = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty=sku_qty+'".$pqty."', sku_avg='".$avg_price."',sku_cost='".$unit_cost."',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='".$location_code."'"; 
    					
    		   	}else{
    		   		//FIFO method
    				$avg_method ='FIFO';
    				$avgCostupdate = "UPDATE " . DB_PREFIX . "product_stock SET sku_qty=sku_qty+'".$pqty."', sku_cost='".$unit_cost."',modifiedon=curdate(),modifiedby='".$userName."' WHERE sku = '" . $pSku . "' AND location_Code='".$location_code."'"; 
    	    	}

    			$sqls = "INSERT INTO " . DB_PREFIX . "purchase_invoice_details (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,net_price,tax_price,total,avg_cost,avg_method,transaction_no,sku,conversion_rate,original_qty,description) VALUES('".$purchaseId."','".$product['product_id']."','0','".$pqty."','".$product['price']."','".$product['raw_cost']."','".$data['discount_percentage']."','".$data['discount_price']."','".$product['tax_class_id']."','".$product['net_price']."','".$product['tax_price']."','".$product['total']."','".$avg_price."','".$avg_method."','".$data['transaction_no']."','".$pSku."','".$data['conversion_rate']."','".$product['quantity']."','".$product['description']."')";				
    			
    			$res = $this->db->queryNew($sqls);
     		    if($res){
    		   		$this->db->query($avgCostupdate); 
    		    }else{
    				$this->db->query("DELETE " . DB_PREFIX . "purchase_invoice_header where purchase_id='".$purchaseId."'");   exit;
    		   }
            }
		}
		return array('invoice_no' => $purchaseId, 'order_no' => $data['oldTransaction_no']);
	}
	public function removeattachment($purchase_id)
	{
		$this->db->query("UPDATE ".DB_PREFIX."purchase SET attachment='' WHERE purchase_id = '".$purchase_id."' ");
	}
	public function removeInvoiceAttachment($purchase_id){
		$this->db->query("UPDATE ".DB_PREFIX."purchase SET attachment='' WHERE purchase_id = '".$purchase_id."' ");
	}
	public function getPurchaseServiceList($data){

	    $company_id	= $this->session->data['company_id'];	
		$sql = "SELECT P.*,V.vendor_name as vendor_name FROM " . DB_PREFIX . "service_purchase_header as P LEFT JOIN " . DB_PREFIX . "vendor as V on V.vendor_id=P.vendor_id LEFT JOIN " . DB_PREFIX . "user as U on U.user_id=P.created_by WHERE P.purchase_return ='0' ";

		if($data['filter_supplier']){
			$sql .= " AND P.vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND P.location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			//$sql .= " AND P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
			$sql .= " AND (P.transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'
			OR P.reference_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%')";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']   = changeDates($data['filter_date_to']); 
			if($data['filter_reference_date']){
				$sql .= " AND P.reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND P.transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
			
		}
		// echo $sql; die;
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY P.purchase_id";
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql;exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getPurchaseServiceTotal($data){

	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(purchase_id) AS totalPurchase, sum(total) AS grandtotal FROM " . DB_PREFIX . "service_purchase_header";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='PURINV'";
		
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 

			if($data['filter_reference_date']){
				$sql .= " AND reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getTotalPurchaseServiceNote($data){

	    $company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(purchase_id) AS totalPurchase, sum(total) AS grandtotal FROM " . DB_PREFIX . "service_purchase_header";
		$sql .= " WHERE company_id = '". (int)$company_id ."' AND purchase_return = '0' AND transaction_type='PURINV'";
		
		if($data['filter_supplier']){
			$sql .= " AND vendor_id = '" . $data['filter_supplier'] . "'";
		}
		if($data['filter_location_code']){
			$sql .= " AND location_code = '" . $data['filter_location_code'] . "'";
		}
		if($data['filter_transactionno']){
			$sql .= " AND transaction_no LIKE '%" . $this->db->escape($data['filter_transactionno']) . "%'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 

			if($data['filter_reference_date']){
				$sql .= " AND reference_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}else{
				$sql .= " AND transaction_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
			}
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getPurchaseServiceNoteDetails($invoiceno){
		$sql = "SELECT PP.*,P.sku as sku_name,P.name,P.sku_shortdescription as sku_description from " . DB_PREFIX . "service_purchase_detail as PP  LEFT JOIN " . DB_PREFIX . "product as P on P.product_id=PP.product_id
				where PP.transaction_no='".$invoiceno."' order by PP.purchase_product_id asc"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getSalesOrder($data){
		$cart_items = array_keys($data['cart_items']);
		foreach($cart_items as $cart){
			$parants = $this->getParantProductByChildProduct($cart);
			foreach($parants as $parant){
				$cart_items[] = (int)$parant['parant_product_id'];
			}
		}
		if(!empty($cart_items)){
			$cart_items = implode($cart_items, ',');
			$sql        = "SELECT sh.invoice_no,cu.name FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."sales_detail as sd ON sh.invoice_no=sd.invoice_no LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no LIKE '%".$data['invoice_no']."%' AND sh.delivery_status IN ('Pending') AND sh.sales_return='0' AND sd.product_id IN(".$cart_items.") group by sh.invoice_no";
			return $this->db->query($sql)->rows;
		}else{
			return array();
		}
	}
	public function getParantProductByChildProduct($cp){
		return $this->db->query("SELECT parant_product_id FROM ".DB_PREFIX."order_child_items where child_product_id ='".$cp."' AND type='SO' ")->rows;
	}
	public function getSalesOrderByInvoiceNo($invoice_no){
		return $this->db->query("SELECT sh.id,cu.name,cu.customercode FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."customers as cu ON sh.customer_code=cu.customercode where sh.invoice_no LIKE '".$invoice_no."' and sh.delivery_status='Pending' ")->row;
	}
	public function getSalesOrderDetailsByInvoiceNo($invoice_no, $tagged = 0){
		$where = "tagged='0' AND";
		if($tagged){
			$where = '';
		}
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where ".$where." invoice_no ='".$invoice_no."' ")->rows;
	}
	public function getSalesOrderDetailsByInvoiceNo2($invoice_no){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where tagged='1' AND invoice_no ='".$invoice_no."' ")->rows;
	}
	public function getTaggedSalesOrders($purchase_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."tagged_sales_orders where purchase_id ='".$purchase_id."' AND status='1' ")->rows;
	}
	public function getProductChildItems($product_id){
		return $this->db->query("SELECT cp.*,pd.name,pd.sku as childsku FROM ".DB_PREFIX."child_products as cp LEFT JOIN ".DB_PREFIX."product as pd ON cp.child_sku=pd.product_id where cp.parant_sku='".$product_id."' ")->rows;
	}
	public function getShippingAddress($shipping_id){
		if($shipping_id > 0){
			return $this->db->query("SELECT cu.name as customer_name,sh.*, cu.mobile as cust_mobile FROM ".DB_PREFIX."shipping as sh LEFT JOIN ".DB_PREFIX."customers as cu on cu.customercode=sh.customer_id where id='".$shipping_id."' ")->row;
		}
	}
	public function getTaggedOrders($data)
	{
		// echo "SELECT sd.*, sh.invoice_no, sh.isinvoice FROM ".DB_PREFIX."sales_detail as sd LEFT JOIN ".DB_PREFIX."sales_header as sh ON sd.invoice_no= sh.invoice_no where sh.istagged='1' and sd.tagged='1' and sd.product_id='".$data['product_id']."' AND sd.purchase_id='".$data['purchase_id']."'"; die;

		return $this->db->query("SELECT sd.*, sh.invoice_no, sh.isinvoice FROM ".DB_PREFIX."sales_detail as sd LEFT JOIN ".DB_PREFIX."sales_header as sh ON sd.invoice_no= sh.invoice_no where sh.istagged='1' and sd.tagged='1' and sd.product_id='".$data['product_id']."' AND sd.purchase_id='".$data['purchase_id']."'")->rows;
	}
	public function checkIsDeliveredOrNot($order_no, $product_id)
	{
		return $this->db->query("SELECT sd.do_qty FROM ".DB_PREFIX."sales_invoice_details as sd LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON sd.invoice_no= sh.invoice_no where sh.istagged='1' AND sd.product_id='".$product_id."' AND sh.sales_trans_no='".$order_no."'")->row['do_qty'];
	} 
	public function untagProduct($order_no, $product_id){
		// echo "UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0', purchase_id='', modifiedon='".date('Y-m-d')."', modifiedby='".$this->session->data['username']."' where invoice_no IN (SELECT invoice_no from ".DB_PREFIX."sales_invoice_header where sales_trans_no ='".$order_no."') and product_id='".$data['product_id']."' "; die;

		$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0', purchase_id='', modifiedon='".date('Y-m-d')."', modifiedby='".$this->session->data['username']."' where invoice_no='".$order_no."' and product_id='".$product_id."'");

		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0', purchase_id='', modifiedon='".date('Y-m-d')."', modifiedby='".$this->session->data['username']."' where invoice_no IN (SELECT invoice_no from ".DB_PREFIX."sales_invoice_header where sales_trans_no ='".$order_no."') and product_id='".$product_id."' ");
		
		$this->db->query("INSERT INTO ".DB_PREFIX."untag_history (`source`, `transaction_no`, `product_id`, `location`, `username`) VALUES ( 'PO', '".$order_no."', '".$product_id."', '', '".$this->session->data['username']."') ");
		
		$this->updateSOandSIHeader($order_no);
	}
	public function updateSOandSIHeader($order_no)
	{
		$taggedCount = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_detail where invoice_no='".$order_no."' AND tagged='1' ")->num_rows;
		if($taggedCount == '0'){
			$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged=0 where invoice_no='".$order_no."' ");
			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged=0 where sales_trans_no='".$order_no."' ");
			$this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set status=0 where order_no='".$order_no."' ");
		}
	}
}
?>