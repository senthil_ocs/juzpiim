<?php
class ModelTransactionShopeeOrderImport extends Model {

	public function getShopeeOrders()
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shopee_order_details order by id asc ")->rows;
	}
	public function getCustomerDetails($custCode){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where customercode ='".$custCode."' ")->row;
	}
	public function getProduct($sku)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_networks where product_sku ='".$sku."' AND product_id !=''")->row;
	}
	public function getQSMNetworkId()
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where name ='qoo10' AND status ='Active'")->row;
	}
	public function updateQSMProduct($data)
	{
		$network_id = $this->getQSMNetworkId()['id'];
		return $this->db->query("INSERT INTO ".DB_PREFIX."product_networks (network_id,product_id,product_sku,price,	quantity,status,network_response_id) VALUES('".$this->db->escape($network_id)."','".$this->db->escape($data['product_id'])."','".$this->db->escape($data['sku'])."','0','0','Active','0') ");
	}
	public function deleteAllData()
	{
		$this->db->query("TRUNCATE ".DB_PREFIX."shopee_order_details");
	}
	public function checkCustomerByPhone($phone)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where mobile ='".$phone."' ")->row;
	}
	public function getShippingDetails($cust_code)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id ='".$cust_code."' AND isdefault='1'")->row;
	}
	public function createNewCustomer($data)
	{
		$company_id     = $this->session->data['company_id']; 
		$customerMobile = str_replace('-', '', $data[44]) !='' ? str_replace('-', '', $data[44]) : 0;

		$sql = "INSERT INTO ".DB_PREFIX."customers (cust_code, company_id, status, name, location_code, mobile, address1, city, zipcode, email, currency_code, tax_allow, country,network_response_id, createdon) VALUES ('".$this->db->escape($data['cust_code'])."', '".$this->db->escape($company_id)."', '1', '".$this->db->escape($data[42])."', 'HQ', '".$this->db->escape($customerMobile)."', '".$this->db->escape($data[45])."', '".$this->db->escape($data[48])."', '".$this->db->escape($data[51])."', '', 'SGD', '0', '1','', '".date('Y-m-d H:i:s')."')";

		$this->db->query($sql);
		$customer_id = $this->db->getLastId();

		if($customer_id !=''){ 
			$sql 	 = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,country,zip,contact_no,notes,isdefault, name) VALUES('".$this->db->escape($customer_id)."','SH".$this->db->escape($customer_id)."','".$this->db->escape($data[45])."','1','".$this->db->escape($data[51])."','".$this->db->escape($customerMobile)."','created when import shopee order at ".date('d/m/Y H:is')."','1', '".$data[43]."') ";
			$this->db->query($sql);
			$shipping_id = $this->db->getLastId();
			$res 		 = array('shipping_id' => $shipping_id, 'customercode' => $customer_id);
			return $res; 
		}else{
			return false;
		}
	}

	public function insertTempTable($data)
	{
		$sql = "INSERT INTO ".DB_PREFIX."shopee_order_details (`OrderID`, `OrderStatus`, `Cancelreason`, `ReturnRefundStatus`, `TrackingNumber`, `ShippingOption`, `ShipmentMethod`, `EstimatedShipOutDate`, `ShipTime`, `OrderCreationDate`, `OrderPaidTime`, `ParentSKUReferenceNo`, `ProductName`, `SKUReferenceNo`, `VariationName`, `OriginalPrice`, `DealPrice`, `Quantity`, `ProductSubtotal`, `SellerRebate`, `SellerDiscount`, `ShopeeRebate`, `SKUTotalWeight`, `Noofproductinorder`, `OrderTotalWeight`, `VoucherCode`, `SellerVoucher`, `SellerAbsorbedCoinCashback`, `ShopeeVoucher`, `BundleDealIndicator`, `ShopeeBundleDiscount`, `SellerBundleDiscount`, `ShopeeCoinsOffset`, `CreditCardDiscountTotal`, `TotalAmount`, `BuyerPaidShippingFee`, `ShippingRebateEstimate`, `TransactionFee`, `CommissionFee`, `ServiceFee`, `GrandTotal`, `EstimatedShippingFee`, `UsernameBuyer`, `ReceiverName`, `PhoneNumber`, `DeliveryAddress`, `Town`, `District`, `City`, `Province`, `Country`, `ZipCode`, `Remarkfrombuyer`, `OrderCompleteTime`, `Note`, `customercode`, `shipping_id`, `product_id`, `orderExist`) VALUES ('".$this->db->escape($data[0])."', '".$this->db->escape($data[1])."', '".$this->db->escape($data[1])."', '".$this->db->escape($data[2])."', '".$this->db->escape($data[3])."', '".$this->db->escape($data[4])."', '".$this->db->escape($data[5])."', '".$this->db->escape($data[6])."', '".$this->db->escape($data[7])."', '".$this->db->escape($data[8])."', '".$this->db->escape($data[9])."', '".$this->db->escape($data[10])."', '".$this->db->escape($data[11])."', '".$this->db->escape($data[12])."', '".$this->db->escape($data[13])."', '".$this->db->escape($data[14])."', '".$this->db->escape($data[15])."', '".$this->db->escape($data[16])."', '".$this->db->escape($data[17])."', '".$this->db->escape($data[18])."', '".$this->db->escape($data[19])."', '".$this->db->escape($data[20])."', '".$this->db->escape($data[21])."', '".$this->db->escape($data[22])."', '".$this->db->escape($data[23])."', '".$this->db->escape($data[24])."', '".$this->db->escape($data[25])."', '".$this->db->escape($data[26])."', '".$this->db->escape($data[27])."', '".$this->db->escape($data[28])."', '".$this->db->escape($data[29])."', '".$this->db->escape($data[30])."', '".$this->db->escape($data[31])."', '".$this->db->escape($data[32])."', '".$this->db->escape($data[33])."', '".$this->db->escape($data[34])."', '".$this->db->escape($data[35])."', '".$this->db->escape($data[37])."', '".$this->db->escape($data[38])."', '".$this->db->escape($data[39])."', '".$this->db->escape($data[40])."', '".$this->db->escape($data[41])."', '".$this->db->escape($data[42])."', '".$this->db->escape($data[43])."', '".$this->db->escape($data[44])."','".$this->db->escape($data[45])."', '".$this->db->escape($data[46])."', '".$this->db->escape($data[47])."', '".$this->db->escape($data[48])."', '".$this->db->escape($data[49])."', '".$this->db->escape($data[50])."', '".$this->db->escape($data[51])."', '".$this->db->escape($data[52])."', '".$this->db->escape($data[53])."', '".$this->db->escape($data[54])."', '".$data['customer_id']."', '".$data['shipping_id']."', '".$data['product_id']."', '".$data['exist']."')";

		$this->db->query($sql); 
	}
	public function getDetailsFromTempTable()
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shopee_order_details GROUP BY OrderID")->rows;
	}
	public function checkOrderExistOrNot($ordernumber)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where network_order_id='".$ordernumber."' AND network_id='3'")->row;
	}
	public function getSalesLastId()
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header ORDER BY id DESC LIMIT 1")->row['id'];
	}
	public function getTempOrderByOrderNumber($OrderID)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shopee_order_details where OrderID='".$OrderID."' ")->rows;
	}
	public function getProductIdBySKU($sku)
	{
		return $this->db->query("SELECT * from ".DB_PREFIX."product_networks where `product_sku`='".$sku."' ORDER BY network_id ASC LIMIT 1")->row['product_id'];
	}
	public function getProductById($product_id)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where product_id='".$product_id."' ")->row;
	}
	public function getProductBySKU($sku)
	{
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where sku='".$sku."' ")->row;
	}
	public function addSalesOrder($data)
	{
		$orders   = $this->getTempOrderByOrderNumber($data['OrderID']);
		$sales_id = 0;
		$userName = $this->session->data['username'];
		if(!empty($orders)){
			$payment_status = 'Pending';
			$payment_remark = '';
			if(($data['OrderStatus'] == 'To ship' || $data['OrderStatus'] == 'Shipping') && $data['OrderPaidTime'] !='' && $data['OrderPaidTime'] !='-'){
				$payment_status = 'Paid';
				$payment_remark = 'Paid-';  // For pament details will insert in payment tables when convert SO to SI
			}
			$delivery_status = 'Pending';
			if($data['OrderStatus'] == 'Cancelled'){
				$delivery_status = 'Canceled';
				$payment_status  = 'Canceled';
			}

			$sql = "INSERT INTO ".DB_PREFIX."sales_header (location_code, invoice_no, invoice_date, customer_code, currency_code, conversion_rate, tax_class_id, tax_type, reference_no, shipping_id, network_id, network_order_id, remarks, bill_discount_price, discount, fc_discount, createdby, modifiedby, payment_status, delivery_status) VALUES('HQ','".$data['invoice_no']."','".date('Y-m-d')."','".$this->db->escape($data['customercode'])."','SGD','1','2','1','".$this->db->escape($data['OrderID'])."','".$this->db->escape($data['shipping_id'])."','3','".$this->db->escape($data['OrderID'])."','".$this->db->escape($payment_remark.$data['Remarkfrombuyer'])."','".$this->db->escape($data['SellerDiscount'])."','".$this->db->escape($data['SellerDiscount'])."','".$this->db->escape($data['SellerDiscount'])."','".$userName."','".$userName."', '".$payment_status."', '".$delivery_status."') ";
			$this->db->query($sql);
			$sales_id  = $this->db->getLastId();

			if($sales_id){
				$sub_total = 0;
				foreach ($orders as $value) {
					$product_sku = $value['SKUReferenceNo'];
					if($product_sku !=''){
						$product  	 = $this->getProductBySKU($product_sku);
						if(empty($product)){
							$product = $this->getProductById($this->getProductIdBySKU($product_sku));
						}
					}
					$childAry['product_id'] = $product['product_id'];
					$childAry['order_no']   = $data['invoice_no'];
					$childAry['type']   	= 'SO';
					$this->cart->insertChildItems($childAry);
					
					if($product['product_id'] !=''){
						$data['Options'] = $data['Options'] !='' ? ' ('.$data['Options'].' )' : '';
						$sql = "INSERT INTO ".DB_PREFIX."sales_detail (location_code,sku,invoice_no,createdon,product_id,description,qty,sku_price,sub_total,net_total,createdby,conversion_rate,tax_class_id,discount,gst,cust_description) VALUES('HQ','".$this->db->escape($product['sku'])."','".$this->db->escape($data['invoice_no'])."','".date('Y-m-d')."','".$this->db->escape($product['product_id'])."','".$this->db->escape($product['name'])."','".$value['Quantity']."','".$this->db->escape($value['DealPrice'])."','".$this->db->escape($value['DealPrice'] * $value['Quantity'])."','".$value['DealPrice'] * $value['Quantity']."','".$this->session->data['username']."','1','1','0','0','".$this->db->escape($value['Options'])."') ";
						$this->db->query($sql);
						$sub_total += $value['DealPrice'] * $value['Quantity'];
					}
				}
				$net_total = $data['GrandTotal'];
				$discount  = $sub_total - $data['GrandTotal'];

				if($sub_total){
					$gst_sub_total = $sub_total - $discount;
					$inclusive_convert = 7 / (100 + 7);
					$tax_amount    = $gst_sub_total * $inclusive_convert;		
		        	if($tax_amount){
		        		$sub_total = $sub_total - $tax_amount; 
		        	}
					$this->db->query("UPDATE ".DB_PREFIX."sales_header set sub_total='".$sub_total."', actual_total='".$sub_total."', net_total='".$net_total."', fc_subtotal='".$sub_total."', fc_nettotal='".$net_total."',gst='".$tax_amount."', fc_tax='".$tax_amount."', discount='".$discount."', fc_discount='".$discount."' where invoice_no='".$data['invoice_no']."' ");
				}else{
					$this->db->query("DELETE FROM ".DB_PREFIX."sales_header where invoice_no='".$data['invoice_no']."' ");
				}
			} 
			$this->deleteTempByOrderNummber($data['OrderID']);
		}
	}
	public function deleteTempByOrderNummber($OrderID)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."shopee_order_details where OrderID='".$OrderID."' ");
	}
    public function checkOrderIdAndSku($orderId, $product_id)
    {
        return $this->db->query("SELECT sh.id FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."sales_detail as sd on sh.invoice_no = sd.invoice_no WHERE sd.product_id='".$this->db->escape($product_id)."' AND sh.network_order_id='".$this->db->escape($orderId)."' AND sh.delivery_status!='Canceled' ")->row;
    }
    public function delete($id)
    {
        $this->db->query("DELETE FROM ".DB_PREFIX."shopee_order_details where id='".$id."' ");
    }
    public function checkShippingAddress($custCode, $data)
    { 
    	return $this->db->query("SELECT * FROM tbl_shipping where customer_id='".$custCode."' and address1='".$this->db->escape($data[45])."'")->row;
    }
    public function addShippingAddress($data)
    {
    	$customerMobile = str_replace('-', '', $data[44]) !='' ? str_replace('-', '', $data[44]) : 0;

    	$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id, shipping_code, address1, country, zip, contact_no, notes, isdefault, name) VALUES('".$this->db->escape($data['customer_id'])."','SH".$data['customer_id']."','".$this->db->escape($data[45])."','Singapore','".$this->db->escape($data[51])."','".$this->db->escape($customerMobile)."','created when import shopee order at ".date('d/m/Y H:is')."','0', '".$this->db->escape($data[43])."') ";
    	$this->db->query($sql);
    	return $data['id'] = $this->db->getLastId();
    }
    public function clearImportedOrders()
    {
    	$this->db->query("DELETE FROM ".DB_PREFIX."shopee_order_details where orderExist='1'");
    }
    public function editSalesOrder($data, $salesHeader)
    {
    	$this->db->query("UPDATE ".DB_PREFIX."sales_header set status='Canceled', delivery_status='Canceled', payment_status='Canceled', cancel_reason='Order Canceled in shopee' where invoice_no = '".$salesHeader['invoice_no']."' ");

		if($salesHeader['isinvoice'] == '1'){
			$invoiceHeader  =  $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where sales_trans_no='".$salesHeader['invoice_no']."'")->row;

			if(!empty($invoiceHeader)){
		    	if($invoiceHeader['payment_status'] == 'Paid' || $invoiceHeader['payment_status'] == 'Partial'){
		    		$this->db->query("UPDATE ".DB_PREFIX."order_payment set payment_status ='Canceled', stripe_response ='Order Canceled in shopee' where order_id='".$invoiceHeader['invoice_no']."' ");
		    	}
		    	if($invoiceHeader['delivery_status'] != 'Pending'){
		    		$this->revertDeliveryOrder($invoiceHeader);
		    	}
		    	$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set do_qty='0' where invoice_no='".$invoiceHeader['invoice_no']."'");
		    	$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set delivery_status ='Canceled', payment_status='Canceled', status='Canceled', paid_amount='0', cancel_reason='Order Canceled in shopee' where invoice_no='".$invoiceHeader['invoice_no']."'");
		    }
		}
    	$this->deleteTempByOrderNummber($data['OrderID']);
    }
    public function revertDeliveryOrder($invoiceHeader)
    {
		$do_details = $this->db->query("SELECT d.product_id, sum(d.qty) as tqty FROM ".DB_PREFIX."sales_do_header as h LEFT JOIN ".DB_PREFIX."sales_do_details as d on h.do_no=d.do_no WHERE h.sales_transaction_no ='".$invoiceHeader['invoice_no']."' and h.status NOT IN ('Delivered','Canceled') GROUP by d.product_id")->rows;

		if(!empty($do_details)){
			foreach($do_details as $detail){
				$product = $this->db->query("SELECT * FROM ".DB_PREFIX."product where product_id='".$product_id."' ")->row;
		    	$qty 	 = (int)$detail['tqty'];
				if($product['package'] =='0'){
			    	$this->db->query("UPDATE * FROM ".DB_PREFIX."product_stock set sku_qty = sku_qty +'".$qty."' where product_id='".$detail['product_id']."' AND location_code='".$invoiceHeader['location_code']."' ");
				}else{
					$childs =$this->db->query("SELECT * FROM ".DB_PREFIX."child_products where parant_sku='".$detail['product_id']."'")->rows;
					foreach ($childs as $value) {
						$qty = $qty * $value['quantity'];
			    		$this->db->query("UPDATE * FROM ".DB_PREFIX."product_stock set sku_qty = sku_qty +'".$qty."' where product_id='".$value['child_sku']."' AND location_code ='".$invoiceHeader['location_code']."' ");
					}
				}
			}
			$this->db->query("UPDATE * FROM ".DB_PREFIX."sales_do_header set status = 'Canceled' where sales_transaction_no ='".$invoiceHeader['invoice_no']."'");
		}
    }
}
?>