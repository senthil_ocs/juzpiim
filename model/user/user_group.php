<?php
class ModelUserUserGroup extends Model {
	public function addUserGroup($data) {
		$companyId	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "user_group (name,company_id,permission) VALUES ('" . $this->db->escape($data['name']) . "','".$companyId."','" . (isset($data['permission']) ? serialize($data['permission']) : '') . "')"); 
			 
	}

	public function editUserGroup($user_group_id, $data) {
		$companyId	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "user_group SET 
			  name = '" . $this->db->escape($data['name']) . "'
			, company_id = '".$companyId."'
			, permission = '" . (isset($data['permission']) ? serialize($data['permission']) : '') . "' 
		WHERE user_group_id = '" . (int)$user_group_id . "'");
	}

	public function deleteUserGroup($user_group_id) {
		$companyId	= $this->session->data['company_id'];
		$tableDetails = $this->getUserGroup($user_group_id);
		$strLogDetails = array(
			'type' 		=> 'User Groups/Rights',
			'id'   		=> $user_group_id,
			'code'   	=> $tableDetails['name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "' AND company_id = '" . (int)$companyId . "'");
	}

	public function addPermission($user_id, $type, $page) {
		$companyId	= $this->session->data['company_id'];
		$user_query = $this->db->query("SELECT DISTINCT user_group_id FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$companyId . "'");

		if ($user_query->num_rows) {
			//$user_group_query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "' AND company_id = '" . (int)$companyId . "'");
			$user_group_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "' AND company_id = '" . (int)$companyId . "'");

			if ($user_group_query->num_rows) {
				$data = unserialize($user_group_query->row['permission']);

				$data[$type][] = $page;

				$this->db->query("UPDATE " . DB_PREFIX . "user_group SET permission = '" . serialize($data) . "' WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "' AND company_id = '" . (int)$companyId . "'");
			}
		}
	}

	public function getUserGroup($user_group_id) {
		$companyId	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "' AND company_id = '" . (int)$companyId . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_group_id . "' AND company_id = '" . (int)$companyId . "'");

		$user_group = array(
			'name'       => $query->row['name'],
			'max_discount'       => $query->row['max_discount'],
			'permission' => unserialize($query->row['permission']),
			'common_permissions'=>unserialize($query->row['common_permissions']),
			'accesstype' => unserialize($query->row['accesstype'])
		);

		return $user_group;
	}

	public function getUserGroups($data = array()) {
		$companyId	= $this->session->data['company_id'];
		
		$sql = "SELECT * FROM " . DB_PREFIX . "user_group WHERE company_id = '" . (int)$companyId . "'";

		$sql .= " ORDER BY name";	

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUserGroups() {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user_group WHERE company_id = '" . (int)$companyId . "'");
		return $query->row['total'];
	}	
	public function getGroupAccessRights($accesstype)
	{
	  //$sql   = "SELECT mainlinks,sublinks FROM  " . DB_PREFIX . "user_rights where accesstype='".$accesstype."' group by sublinks";
	  $sql   = "SELECT * FROM  " . DB_PREFIX . "access where access_type='".$accesstype."'";

	  $query = $this->db->query($sql);
	  return $query->rows;
	}

	public function getRights($accesstype)
	{
	  //$sql   = "SELECT mainlinks,sublinks FROM  " . DB_PREFIX . "user_rights where accesstype='".$accesstype."' group by sublinks";
	  $sql   = "SELECT DISTINCT(sublinks),mainlinks FROM  " . DB_PREFIX . "user_rights where accesstype='".$accesstype."'";

	  $query = $this->db->query($sql);
	  return $query->rows;
	}

	public function getControllerbysublinks($link)
	{
      $sql   = "SELECT controller FROM  " . DB_PREFIX . "user_rights  WHERE sublinks in(".$link.") ";
	  $query = $this->db->query($sql);
	  return $query->rows; 
	}

	// 

	public function addUserGroupwithrights($data)
	{
		$companyId	= $this->session->data['company_id'];
		$data['permission'] = isset($data['permission']) ? serialize($data['permission']) : '';
		$data['pers'] 		= isset($data['pers']) ? serialize($data['pers']) : '';
		$data['accesstype'] = isset($data['accesstype']) ? serialize($data['accesstype']) : '';

		$sql = "INSERT INTO " . DB_PREFIX . "user_group (name,company_id,permission,common_permissions,accesstype,max_discount) 
		VALUES ('".$data['name']."', '".$companyId."', '".$data['permission']. "', '".$data['pers']."', '".$data['accesstype']."', '".$data['max_discount']."')";
		
		$this->db->query($sql);
	}

	public function editUserGroupwithrights($user_group_id, $data)
	{
		
		$companyId	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "user_group SET 
			  name = '" . $this->db->escape($data['name']) . "'
			, company_id = '".$companyId."'
			, permission = '" . (isset($data['permission']) ? serialize($data['permission']) : '') . "'
			, common_permissions='".(isset($data['pers']) ? serialize($data['pers']) : '')."'
			, accesstype='".(isset($data['accesstype']) ? serialize($data['accesstype']) : '')."'
		WHERE user_group_id = '" . (int)$user_group_id . "'");
		/*. "' ,max_discount = '" . $this->db->escape($data['max_discount']*/
	}

	public function getUserGroupName($name,$user_group_id)
	{
		$company_id = $this->session->data['company_id'];
		$where =' ';
		if($user_group_id!=""){
			$where .= " AND user_group_id != '".$user_group_id."' ";
		}

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_group WHERE name = '".$name."' AND company_id = '".$company_id."'".$where);


		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}

	public function updateusergroupReportsAccess($str,$group_id){
		$sql = "UPDATE " . DB_PREFIX . "user_group SET access_reports='" . $str. "' WHERE user_group_id = '" . (int)$group_id. "'";
		$this->db->query($sql);
	}
	public function updateusergroupReportsAccessPOS($strpos,$group_id){
		$sql = "UPDATE " . DB_PREFIX . "user_group SET access_reports_pos='" . $strpos. "' WHERE user_group_id = '" . (int)$group_id. "'";
		$this->db->query($sql);
	}
	public function getusergroupReportsAccess($group_id,$field=''){
		$sql = "SELECT $field from " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$group_id. "'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getAccessReportByType($accesstype,$report_group='')
	{
	  $sql   = "SELECT * FROM  " . DB_PREFIX . "access where access_type='".$accesstype."' AND group_name='".$report_group."' order by sort_id ASC";
	  $query = $this->db->query($sql);
	  return $query->rows;
	}
	public function userReportAccess(){
		$userId = $this->session->data['user_id'];
		$sql = "select ug.access_reports,ug.user_group_id,u.user_id from tbl_user_group as ug left join tbl_user as u on u.user_group_id = ug.user_group_id where u.user_id='".$userId."'";
		$query = $this->db->query($sql);
		 return $query->row;

	}
}
?>