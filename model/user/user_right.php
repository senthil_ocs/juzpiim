<?php
class ModelUserUserRight extends Model {

	public function getTotalUserRights()
	{
		$query = $this->db->query("SELECT COUNT(*) As total FROM " . DB_PREFIX . "user_right");

		return $query->row['total'];
	}

	public function getUserRights($data = array()) {
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		$companyId	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "user_right WHERE company_id = '" . (int)$companyId . "'";

		$sql .= " ORDER BY name";	

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getUserRight($user_right_id) {
		$companyId	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "user_right WHERE user_right_id = '" . (int)$user_right_id . "' AND company_id = '" . (int)$companyId . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user_right WHERE user_right_id = '" . (int)$user_right_id . "' AND company_id = '" . (int)$companyId . "'");

		$user_right = array(
			'name'       => $query->row['name'],
			'permission' => unserialize($query->row['permission'])
		);

		return $user_right;
	}
	
}
?>