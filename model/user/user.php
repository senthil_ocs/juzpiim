<?php
class ModelUserUser extends Model {
	public function addUser($data) {
		$company_id	= $this->session->data['company_id'];
		/*$sales_agent = isset($data['sales_agent']) ? 1 : 0 ;*/
		// printArray($data['location_code']);die;   sales_agent,   , '".$sales_agent."'
		$res = $this->db->queryNew("INSERT INTO " . DB_PREFIX . "user (username,salt,password,firstname,lastname,email,company_id,user_group_id,status,date_added,location_code,original_password) 
			VALUES('" . $this->db->escape($data['username']) . "','','" .$this->db->escape(md5($data['password'])) . "','" . $this->db->escape($data['firstname']) . "',
			'" . $this->db->escape($data['lastname']) . "','" . $this->db->escape($data['email']) . "','".$company_id."','" . (int)$data['user_group_id'] . "','" . (int)$data['status'] . "',curdate(),'".$data['location_code']."','".$data['password']."') ");
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=user/user/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['username']);
			exit;
		}

	}

	public function checkpasswordforuser($entered_password,$user_id=''){
		$sql = "select count(*) as total from " . DB_PREFIX . "user where password='".md5($entered_password)."'";
		if($user_id!=''){
			$sql = "select count(*) as total from " . DB_PREFIX . "user where password='".md5($entered_password)."' AND user_id!='".$user_id."'";
		}
		$res = $this->db->query($sql);
		return $res->row['total'];
	}
	public function getUserDetails() {
		/* WHERE sales_agent='1'*/
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user ");
		return $query->rows;
	}

	public function editUser($user_id, $data) {
		$company_id	= $this->session->data['company_id'];
		/*
			$sales_agent = isset($data['sales_agent']) ? 1 : 0 ;
			, sales_agent = '" . $sales_agent . "'
		*/

		$this->db->query("UPDATE " . DB_PREFIX . "user SET 
			username = '" . $this->db->escape($data['username']) . "'
			, firstname = '" . $this->db->escape($data['firstname']) . "'
			, lastname = '" . $this->db->escape($data['lastname']) . "'
			, email = '" . $this->db->escape($data['email']) . "'
			, user_group_id = '" . (int)$data['user_group_id'] . "'
			, company_id = '".$company_id."'
			
			, status = '" . (int)$data['status'] . "'
			, location_code = '" .$data['location_code'] . "'
			WHERE user_id = '" . (int)$user_id . "'");

		if ($data['password']) {
			$this->db->query("UPDATE " . DB_PREFIX . "user SET 
				salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
				, password = '" . $this->db->escape(md5($data['password'])) . "' , original_password='".$data['password']."'
				WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$company_id . "'");
		}
	}

	public function editPassword($user_id, $password) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "user SET 
			salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
			, password = '" . $this->db->escape(md5($data['password'])) . "'
			, code = '' 
			WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$company_id . "'");
	}

	public function editCode($email, $code) {
		
		$this->db->query("UPDATE " . DB_PREFIX . "user SET salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($code)))) . "',code = '' 
			WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function deleteUser($user_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getUser($user_id);
		$strLogDetails = array(
			'type' 		=> 'User',
			'id'   		=> $user_id,
			'code' 		=> $tableDetails['username'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$company_id . "'");
	}

	public function getUser($user_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$company_id . "'");

		return $query->row;
	}

	public function getUserByEmail($email,$user_id) {
		$where =' ';
		if($user_id!=""){
			$where.= " AND user_id!= '".$user_id."' ";
		}
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE email = '" . $this->db->escape($email) . "' " .$where);

		return $query->row;
	}

	public function getUserByUserName($name,$user_id) {
		$where =' ';
		if($user_id!=""){
			$where.= " AND user_id!= '".$user_id."' ";
		}
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($name) . "' " .$where);

		return $query->row;
	}

	public function getUserByCode($code) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE code = '" . $this->db->escape($code) . "' AND code != '' AND company_id = '" . (int)$company_id . "'");

		return $query->row;
	}

	public function getUsers($data = array()) {
		$company_id	= $this->session->data['company_id'];

		$sql = "SELECT * FROM " . DB_PREFIX . "user WHERE company_id = '" . (int)$company_id . "'";

		$sort_data = array();

		if ($data['filter_name']){
			$sql.= " AND username LIKE '".$data['filter_name']."' "; 
		}
		if ($data['filter_location_code']){
			$sql.= " AND location_code = '" . $data['filter_location_code'] . "' "; 
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY username";	
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql;die;

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalUsers($data) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user WHERE company_id = '" . (int)$company_id . "'";
		if ($data['filter_name']){
			$sql.= " AND username LIKE '".$data['filter_name']."' "; 
		}
		
		if ($data['filter_location_code']){
			$sql.= " AND location_code = '" . $data['filter_location_code'] . "' "; 
		}

		$query = $this->db->query($sql);		
		return $query->row['total'];
	}

	public function getTotalUsersByGroupId($user_group_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user WHERE user_group_id = '" . (int)$user_group_id . "' AND company_id = '" . (int)$company_id . "'");

		return $query->row['total'];
	}

	public function getTotalUsersByEmail($email) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "' AND company_id = '" . (int)$company_id . "'");

		return $query->row['total'];
	}	

	public function doCheckUserStatus($email)
	{
		$query = $this->db->query("SELECT status FROM " . DB_PREFIX . "user WHERE email = '" . $this->db->escape($email) . "'");
		$result	= $query->row;
		if($result) {
			return $result;
		}
	}
	/*10-Mar-2016*/
	public function getUsersByEmailAddress($email) 
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE LOWER(email) = '".$email."'");
		
		return $query->row;
	}
	public function getLocationbyCode($location_code)
	{

		$query = $this->db->query("SELECT location_name FROM " . DB_PREFIX . "location WHERE location_code = '" .$location_code . "'");
		return $query->row['location_name'];
	}
	/*10-Mar-2016*/
	/*** 26-Feb-2016 ***/
	/*public function getUserList($data) 
	{
	    $company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE company_id = '" . (int)$company_id . "'";

		if(!empty($data['filter_username'])){
			$sql .= " AND username LIKE '%" . $this->db->escape($data['filter_username']) . "%'";
		}
		if($data['filter_email']){
			$sql .= " AND email LIKE '%" . $this->db->escape($data['filter_email']) . "%'";
		}
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY user_id";	
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		echo "$sql"; 
		$query = $this->db->query($sql);
		return $query->rows;
	}*/
	/*** 26-Feb-2016 Ends ***/
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>