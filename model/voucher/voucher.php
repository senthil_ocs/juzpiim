<?php
class ModelVoucherVoucher extends Model {

	public function getLastvoucherCode(){
		//$voucher_code	= $this->session->data['voucher_code']; 

		$query = $this->db->query("SELECT MAX(voucher_code) as vouchercode FROM " . DB_PREFIX . "voucher");
		if(!empty($query->row)){
			$termcode=$query->row['vouchercode'];

		}else{
			$termcode=1;
		}
		
		return $termcode;
	}
	
	public function addvoucher($data){ 

			$locations = implode(',', $data['location']);
		  if(isset($data['from_code']) && isset($data['to_code'])){
		  	$range = range($data['from_code'], $data['to_code']); 
		  	foreach ($range as $value) {
		  	/*$valid_to1  = date('Y-m-d',strtotime(str_replace('/', '-', $data['valid_to1']))); 
		  	$valid_from1  = date('Y-m-d',strtotime(str_replace('/', '-', $data['valid_from1'])));*/
		  	$vCode = $data['prefix'].$value;
		  	//echo '<br>'."INSERT INTO " . DB_PREFIX . "voucher (voucher_code,voucher_amount,valid_from,valid_to,is_active,createdon,createdby) VALUES ('".$vCode."','".$data['voucher_amount1']."','".$data['valid_from1']."','".$data['valid_to1']."','".$data['is_active1']."',curdate(),1)";
		  	$sql = "INSERT INTO " . DB_PREFIX . "voucher (voucher_code,voucher_amount,min_amount,valid_from,valid_to,is_active,createdon,createdby) VALUES ('".$vCode."','".$data['voucher_amount1']."','".$data['min_amount1']."','".$data['valid_from1']."','".$data['valid_to1']."','".$data['is_active1']."',curdate(),1)";
		  	$query = $this->db->query($sql);
		  } //exit;
		  } if(isset($data['voucher_code'])){
		  	/*$valid_to  = date('Y-m-d',strtotime(str_replace('/', '-', $data['valid_to']))); 
		  	$valid_from  = date('Y-m-d',strtotime(str_replace('/', '-', $data['valid_from'])));*/
		  	$sql = "INSERT INTO " . DB_PREFIX . "voucher (voucher_code,voucher_amount,min_amount,valid_from,valid_to,is_active,location_code,createdon,createdby) VALUES ('".$data['voucher_code']."','".$data['voucher_amount']."','".$data['min_amount']."','".$data['valid_from']."','".$data['valid_to']."','".$data['is_active']."','".$locations."',curdate(),1)";
		  	$query = $this->db->query($sql);
		 		foreach ($data['location'] as $value) {
		 			if(isset($value)){
						$this->db->query("INSERT INTO " . DB_PREFIX . "voucher_location(voucher_code,location_code) VALUES ('".$data['voucher_code']."','".$value."')");

		 			}
		 		}
		  }	
		  
		$voucher_code = $this->db->getLastId();
		return $voucher_code;
	}
	public function getTotalvouchers($data){

      	$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "voucher";
			
		if (!empty($data['filter_voucher_code'])){
			$sql.= " where voucher_code LIKE '%" . $this->db->escape($data['filter_voucher_code']) . "%'"; 
		}

		$query = $this->db->query($sql); 
		return $query->row['total'];

	} 


	public function getvouchers($data){
		if ($data) {

			$sql = "SELECT * FROM " . DB_PREFIX . "voucher as V LEFT JOIN tbl_voucher_location as VL on VL.voucher_code = V.voucher_code " ;
			$sql .=" where VL.voucher_code = V.voucher_code";
 
			if (!empty($data['filter_voucher_code'])){
				$sql.= "  AND V.voucher_code LIKE '%" . $this->db->escape($data['filter_voucher_code']) . "%'"; 
			}

			if (!empty($data['filter_location_code'])){
				$sql.= "  AND VL.location_code LIKE '%" . $this->db->escape($data['filter_location_code']) . "%'"; 
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDate($data['filter_date_from']); 
				$data['filter_date_to']  = changeDate($data['filter_date_to']); 
				$sql .= " AND V.valid_from >=  '" . $data['filter_date_from'] . "' AND V.valid_from <='" . $data['filter_date_to'] . "'";
			}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY V.createdon";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
			$query = $this->db->query($sql);
			return $query->rows;
		} 	

	}
	 
	public function editvoucher($data,$voucher_code)
	{
		
		$sql="UPDATE " . DB_PREFIX . "voucher SET 
									voucher_code 		= '".$data['voucher_code']."',
									voucher_amount 		= '".$data['voucher_amount']."',
									valid_from		    = '".date('Y-m-d',strtotime($data['valid_from']))."',
									valid_to    		= '".date('Y-m-d',strtotime($data['valid_to']))."',
									is_active 			= '".$data['is_active']."',
									modifiedon 			= curdate(),
									modifiedby			= '1'
									WHERE voucher_code = '".$voucher_code."'";
		//echo $sql;exit;
		 $query = $this->db->query($sql);

		 $this->db->query("DELETE FROM " . DB_PREFIX . "voucher_location WHERE voucher_code = '" . $data['voucher_code'] . "'");
	 	for($l=0;$l<count($data['location']);$l++){
			if($data['location'][$l]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "voucher_location(voucher_code,location_code) VALUES ('".$data['voucher_code']."','".$data['location'][$l]."')");
			}
		}
	}
	public function deletevoucher($voucher_code)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "voucher WHERE voucher_code = '" . $voucher_code . "'");
	}

	public function getvoucher($voucher_code)
	{
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "voucher WHERE voucher_code = '" . $voucher_code . "'");
		return $query->row;
	}
}
?>