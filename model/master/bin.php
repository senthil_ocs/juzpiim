<?php
class ModelMasterBin extends Model {
	public function addBin($data) {
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "bin SET 
		  bin_name = '" . $this->db->escape($data['name']) . "'
		, description = '" . $this->db->escape($data['description']) . "'
		, company_id = '".$company_id."'
		, create_user = '".$userId."'
		, create_date = NOW()
		, modify_user = '".$userId."'
		, modify_date = NOW()
		");
		$this->cache->delete('bin');
	}
	
	public function editBin($bin_id, $data) {
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "bin SET 
		  bin_name = '" . $this->db->escape($data['name']) . "'
		, description = '" . $this->db->escape($data['description']) . "'
		, company_id = '".$company_id."'
		, create_user = '".$userId."'
		, create_date = NOW()
		, modify_user = '".$userId."'
		, modify_date = NOW()
		WHERE bin_id = '" . (int)$bin_id . "'");
		$this->cache->delete('bin');
	}
	
	public function deleteBin($bin_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "bin WHERE bin_id = '" . (int)$bin_id . "'");
		$this->cache->delete('bin');
	}
	
	public function getBin($bin_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "bin WHERE bin_id = '" . (int)$bin_id . "' AND company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
		
	public function getBins($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "bin 
			WHERE company_id = '" . (int)$company_id . "'";
			$sort_data = array(
				'bin_name',
				'description'
			);	
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY bin_name";	
			}
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}		
			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			$bin_data = $this->cache->get('bin');
			if (!$bin_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "bin WHERE company_id = '" . (int)$company_id . "' ORDER BY bin_name ASC");
				$bin_data = $query->rows;
				$this->cache->set('bin', $bin_data);
			}
			return $bin_data;			
		}	
	}
	
	public function getTotalBins() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "bin WHERE company_id = '" . (int)$company_id . "'");
		return $query->row['total'];
	}	
}
?>