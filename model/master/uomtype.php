<?php
class ModelMasterUomType extends Model {
	public function addUomType($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "uom_type_master (uom_code,uom_type_name,uom_type_value,createby,createon,modifyby,modifyon) VALUES ('" . $this->db->escape($data['uom_code']) . "','" . $this->db->escape($data['name']) . "','".$this->db->escape($data['type_value'])."','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate())");		 
	

	}
	
	public function editUomType($uom_type_id, $data) {
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "uom_type_master SET 
			uom_code = '" . $data['uom_code'] . "'
			, uom_type_name = '" . $this->db->escape($data['name']) . "'
			, uom_type_value = '" . $this->db->escape($data['type_value']) . "'
			, modifyby = '".strtoupper($userName)."'
			, modifyon = curdate()
		 WHERE uom_type_id = '" . (int)$uom_type_id . "'");
	
	}
	
	public function deleteuomtype($uom_type_id) {
		$tableDetails = $this->getuomtype($uom_type_id);
		$strLogDetails = array(
			'type' 		=> 'Uom Type',
			'id'   		=> $uom_type_id,
			'code' 		=> $tableDetails['uom_type_name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "uom_type_master WHERE uom_type_id = '" . (int)$uom_type_id . "'");
		$this->cache->delete('uom_type_master');
	}
	
	public function getuomtype($uom_type_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "uom_type_master WHERE uom_type_id = '" . (int)$uom_type_id . "'");
		return $query->row;
	}
		
	public function getuomtypes($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "uom_type_master";
			
			$sort_data = array(
				'uom_code',
				'uom_type_name',
				'uom_type_value'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY uom_type_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			if (!$uom_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uom_type_master WHERE uom_type_name!= '' ORDER BY uom_type_name ASC");
				$uom_data = $query->rows;
				$this->cache->set('uom_type_master', $uom_data);
			}
			return $uom_data;			
		}	
	}
	
	public function getTotaluomtypes() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "uom_type_master ");
		return $query->row['total'];
	}	
	public function checkDuplicationUomType($value1,$value2,$uom_type_id){
		$where = "";
		if(isset($uom_type_id) && $uom_type_id !=''){
			$where = "AND uom_type_id != '".$uom_type_id."'";
		}
		$strValue = $value1.'*'.$value2;
		$query = $this->db->query("SELECT COUNT(*) AS CNT FROM " . DB_PREFIX . "uom_type_master WHERE uom_type_value = '" . $strValue . "' $where");
		return $query->row['CNT'];
	}
	public function getuoms() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uom");
		return $query->rows;
	}

	public function getTypeListbyUOMCode($code){
		$sql  = "select * from " . DB_PREFIX . "uom_type_master where uom_code = '" . $code . "'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>