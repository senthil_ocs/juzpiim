<?php
class ModelMasterCompany extends Model {

	public function getCompanyDetails($company_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	
	public function editCompanyDetails($company_id,$data,$formFieldNames) {
		$escapeFields	= array('c_password','last_eod_date','name','location_code','company_id');
		foreach($data as $key=>$values) {
			if(in_array($key,$formFieldNames)) {
				if(!in_array($key,$escapeFields)) {
					$this->db->query("UPDATE " . DB_PREFIX . "company SET ".$this->db->escape($key). " = '" . $this->db->escape($values) . "'  WHERE company_id = '" . (int)$company_id . "'");	
				}
			}
		}
		return true;
	}
	public function getAutosearchCount(){
		$sql = "SELECT `value` FROM tbl_setting where `key`='config_autosearch_string' ";
		$query = $this->db->query($sql);
		return $query->row['value'];
	}

}