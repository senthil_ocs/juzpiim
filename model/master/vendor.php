<?php
class ModelMasterVendor extends Model {
	public function addVendor($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];		
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "vendor (vendor_code,vendor_name,address1,address2,country,postal_code,phone,fax,email,attn,remarks,gst,status,company_id,create_user,create_date,modify_user,modify_date,tax_method,currency_code,term_id,tax_no) VALUES ('" . $this->db->escape($data['code']) . "','" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['address1']) . "','" . $this->db->escape($data['address2']) . "','" . $this->db->escape($data['country']) . "','" . $this->db->escape($data['zipcode']) . "','" . $this->db->escape($data['phone']) . "','" . $this->db->escape($data['fax']) . "','" . $this->db->escape($data['email']) . "','" . $this->db->escape($data['attn']) . "','" . $this->db->escape($data['remarks']) . "','".$this->db->escape($data['gst'])."','" . $this->db->escape($data['status']) . "','".$company_id."','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate(),'".$data['tax_method']."','".$data['currency_code']."','".$data['term_id']."','".$data['tax_no']."')");
			 	
		$this->cache->delete('vendor');
	}
	
	public function editVendor($vendor_id, $data) {
		
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		
		$this->db->query("UPDATE " . DB_PREFIX . "vendor SET 
			  vendor_code 	= '" . $this->db->escape($data['code']) . "'
			, vendor_name 	= '" . $this->db->escape($data['name']) . "'
			, address1 		= '" . $this->db->escape($data['address1']) . "'
			, address2 		= '" . $this->db->escape($data['address2']) . "'
			, country 		= '" . $this->db->escape($data['country']) . "'
			, postal_code 	= '" . $this->db->escape($data['zipcode']) . "'
			, phone 		= '" . $this->db->escape($data['phone']) . "'
			, fax 			= '" . $this->db->escape($data['fax']) . "'
			, email 		= '" . $this->db->escape($data['email']) . "'
			, attn 			= '" . $this->db->escape($data['attn']) . "'
			, remarks 		= '" . $this->db->escape($data['remarks']) . "'
			, gst 			= '" . $this->db->escape($data['gst'])."'
			, status 		= '" . $this->db->escape($data['status']) . "'
			, company_id 	= '" . $company_id."'
			, modify_user 	= '" . strtoupper($userName)."'
			, modify_date 	= curdate()
			, currency_code = '" . $data['currency_code']."'
			, tax_method 	= '" . $this->db->escape($data['tax_method']) . "'
			, term_id 		= '" . $data['term_id']."'
			, tax_no 		= '" . $data['tax_no']."'
		WHERE vendor_id = '" . (int)$vendor_id . "'");
	
		$this->cache->delete('vendor');
	}
	
	public function getvendoremail($email,$vendor_id ='')
	{
		$query = "SELECT count(*) as total FROM " . DB_PREFIX . "vendor WHERE email ='" . $email . "'";
		if($vendor_id != ''){
			$query .= "AND vendor_id != '".$vendor_id."'";
		}
		$query = $this->db->query($query);
		return $query->row['total'];
	}
	public function deleteVendor($vendor_id) {
		$tableDetails = $this->getVendor($vendor_id);
		$strLogDetails = array(
			'type' 		=> 'Supplier',
			'id'   		=> $vendor_id,
			'code' 		=> $tableDetails['vendor_code'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor WHERE vendor_id = '" . (int)$vendor_id . "'");
		$this->cache->delete('vendor');
	}
	
	public function getVendor($vendor_id) {
		$company_id	= $this->session->data['company_id'];

		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "vendor WHERE vendor_id = '" . (int)$vendor_id . "' AND company_id = '" . (int)$company_id . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor WHERE vendor_id = '" . (int)$vendor_id . "' AND company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
		
	public function getVendors($data = array()) {
			$company_id	= $this->session->data['company_id'];

			//$sql = "SELECT * FROM " . DB_PREFIX . "vendor
			$sql = "SELECT v.*,x.xero_id from " . DB_PREFIX . "vendor as v LEFT JOIN " . DB_PREFIX . "xero_to_supplier as x on v.vendor_id = x.vendor_id and x.location ='".$this->session->data['location_code']."' WHERE v.vendor_id != ''";
			if(!empty($data['filter_supplier'])){
				$sql .= " AND v.vendor_name LIKE '%" . $data['filter_supplier']. "%'";
			}
			if(!empty($data['filter_phone'])){
				$sql .= " AND v.phone LIKE '%" . $this->db->escape($data['filter_phone']) . "%'";
			}
			$sort_data = array(
				'vendor_name',
				'vendor_code',
				'country',
				'postal_code',
				'phone',
				'email',
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY v.". $data['sort'];	
			} else {
				$sql .= " ORDER BY v.vendor_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
			// echo $sql; die;
			$query = $this->db->query($sql);
			return $query->rows;
	}
	
	public function getTotalVendors($data) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendor WHERE company_id = '" . (int)$company_id . "'";
      	if(!empty($data['filter_supplier'])){
				$sql .= " AND vendor_name LIKE '%" . $data['filter_supplier']. "%'";
			}
			if(!empty($data['filter_phone'])){
				$sql .= " AND phone LIKE '%" . $this->db->escape($data['filter_phone']) . "%'";
			}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}	
	
	public function getCountry() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT country_id,name FROM " . DB_PREFIX . "country WHERE company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}	
	
	public function getCountryName($country_id) {
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'");
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getSuppliers() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor where company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}

	public function getSuppliersDetailsbyCode($code) {
		$sql = "SELECT * FROM " . DB_PREFIX . "vendor where vendor_code = '" .$code . "'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getSuppliersDetailsbyid($id) {
		$sql = "SELECT * FROM " . DB_PREFIX . "vendor where vendor_id = '" .$id . "'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getvendor_byCode($code){
      $sql = $this->db->query("SELECT * from ".DB_PREFIX."vendor where vendor_code='".$code."' ");
      return $sql->rows;
    }
    public function getxerovendorByid($id){
       $sql = $this->db->query("SELECT * from ".DB_PREFIX."xero_to_supplier where vendor_id='".$id."' AND xero_id!='' ");
       return $sql->row;
    }
	public function getCurrency() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "currency WHERE company_id = '" . (int)$company_id . "' AND status='1' ORDER BY default_field DESC");
		return $query->rows;
	}
	public function getVendorCode(){ 
		return $this->db->query("SELECT MAX(REPLACE(vendor_code,'V',''))+1 as c FROM `tbl_vendor` ORDER BY `vendor_id` DESC LIMIT 1")->row['c'];
	}
}
?>