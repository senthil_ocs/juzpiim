<?php
class ModelMasterSalesMan extends Model {
	public function addSalesman($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "sales_man SET 
			 salesman_name = '" . $this->db->escape($data['name']) . "'
			, salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
			, password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "'
			, company_id = '".$company_id."'
			, create_user = '".strtoupper($userName)."'
			, create_date = curdate()
			, modify_user = '".strtoupper($userName)."'
			, modify_date = curdate()
		");
		$this->cache->delete('salesman');
	}
	
	public function editSalesman($salesman_id, $data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "sales_man SET 
			salesman_name = '" . $this->db->escape($data['name']) . "'
			, salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
			, password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "'
			, company_id = '".$company_id."'
			, modify_user = '".strtoupper($userName)."'
			, modify_date = curdate()
		WHERE salesman_id = '" . (int)$salesman_id . "'");
	
		$this->cache->delete('salesman');
	}
	
	public function deleteSalesman($salesman_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "sales_man WHERE salesman_id = '" . (int)$salesman_id . "'");
		$this->cache->delete('salesman');
	}
	
	public function getSalesman($salesman_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_man WHERE salesman_id = '" . (int)$salesman_id . "' AND company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
		
	public function getSalesmans($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "sales_man WHERE company_id = '" . (int)$company_id . "'";
			
			$sort_data = array(
				'salesman_name'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY salesman_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}		
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$salesman_data = $this->cache->get('salesman');
		
			if (!$salesman_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_man WHERE company_id = '" . (int)$company_id . "' ORDER BY salesman_name ASC");
	
				$salesman_data = $query->rows;
			
				$this->cache->set('salesman', $salesman_data);
			}

			return $salesman_data;			
		}	
	}
	
	public function getTotalSalesmans() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sales_man WHERE company_id = '" . (int)$company_id . "'");
		
		return $query->row['total'];
	}	
}
?>