<?php
class ModelMasterReports extends Model {
	
	public function getCompanyDetails($company_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE c.company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	
	public function getPaymodeList() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "payment WHERE company_id = '" . (int)$company_id . "'");
		
		return $query->rows;
	}
	
	public function getSalesmanList() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_man WHERE company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	
	public function getVendorList() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "vendor WHERE company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	
	public function getCountryName($country_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "'");
		return $query->row;
	}
	
	public function getCashierList() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier AS CA
		LEFT JOIN " . DB_PREFIX . "cashier_group AS CG ON (CA.cashier_group_id = CG.cashier_group_id) 
		WHERE CA.company_id = '" . (int)$company_id . "'");
		
		return $query->rows;
	}
	
	public function getTaxList() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tr.tax_rate_id, tr.name AS name, tr.rate, tr.type, tr.geo_zone_id, gz.name AS geo_zone, tr.date_added, tr.date_modified FROM " . DB_PREFIX . "tax_rate tr 
				LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr.geo_zone_id = gz.geo_zone_id) WHERE tr.company_id = '".$company_id."'");

		return $query->rows;
	}
	
}
?>