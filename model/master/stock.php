<?php
class ModelMasterStock extends Model {
	public function addStock($data) {
		$userId	= $this->session->data['user_id'];
		$userName = $this->session->data['username'];
		$companyId	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "StockType SET 
			  StockCode = '" . $this->db->escape($data['code']) . "'
			, StockName = '" . $this->db->escape($data['name']) . "'
			, Method = '" . $this->db->escape($data['method']) . "'
			, Position = '" . $this->db->escape($data['position']) . "'
			, CompanyID = '".$companyId."'
			, CreateUser = '".strtoupper($userName)."'
			, CreateDate = curdate()
			, ModifyUser = '".strtoupper($userName)."'
			, ModifyDate = curdate()
		");
		$this->cache->delete('stocktype');
	}
	
	public function editStock($stock_id, $data) {
		$userId	= $this->session->data['user_id'];
		$companyId	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "StockType SET 
			  StockCode = '" . $this->db->escape($data['code']) . "'
			, StockName = '" . $this->db->escape($data['name']) . "'
			, Method = '" . $this->db->escape($data['method']) . "'
			, Position = '" . $this->db->escape($data['position']) . "' 
			, CompanyID = '".$companyId."'
			, ModifyUser = '".strtoupper($userName)."'
			, ModifyDate = curdate()
		WHERE StockID = '" . (int)$stock_id . "'");
		$this->cache->delete('stocktype');
	}
	
	public function deleteStock($stock_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "StockType WHERE StockID = '" . (int)$stock_id . "'");
		$this->cache->delete('stocktype');
	}
	
	public function getStock($stock_id) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "StockType WHERE StockID = '" . (int)$stock_id . "'  AND CompanyID = '" . (int)$companyId . "'");
		return $query->row;
	}
		
	public function getStocks($data = array()) {
		$companyId	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "StockType
			WHERE CompanyID = '" . (int)$companyId . "'";
			$sort_data = array(
				'StockName',
				'StockCode',
				'Position'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY StockName";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}		
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$stocktype_data = $this->cache->get('stocktype');
			if (!$stocktype_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "StockType WHERE CompanyID = '" . (int)$companyId . "' ORDER BY StockName ASC");
				$stocktype_data = $query->rows;
				$this->cache->set('stocktype', $stocktype_data);
			}
			return $stocktype_data;			
		}	
	}
	
	public function getTotalStocks() {
		$companyId	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "StockType WHERE CompanyID = '" . (int)$companyId . "'");
		return $query->row['total'];
	}	
}
?>