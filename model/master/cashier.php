<?php
class ModelMasterCashier extends Model {
	public function addCashier($data) {
		$user_id	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "cashier SET 
			  cashier_group_id = '" . $this->db->escape($data['group']) . "'
			, cashier_name = '" . $this->db->escape($data['name']) . "'
			, email = '" . $this->db->escape($data['email']) . "'
			, salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
			, password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "'
			, status = '" . $this->db->escape($data['status']) . "'
			, company_id = '".$company_id."'
			, create_user = '".strtoupper($userName)."'
			, create_date = curdate()
			, modify_user = '".strtoupper($userName)."'
			, modify_date = curdate()
		");
		
		$this->db->query("UPDATE `" . DB_PREFIX . "company` SET total_cashier = (total_cashier + 1) WHERE company_id = '" . (int)$company_id . "'");
	
		$this->cache->delete('cashier');
	}
	
	public function editCashier($cashier_id, $data) {
		$user_id	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "cashier SET 
		 	cashier_group_id = '" . $this->db->escape($data['group']) . "'
			, email = '" . $this->db->escape($data['email']) . "'
			, cashier_name = '" . $this->db->escape($data['name']) . "'
			, salt = '" . $this->db->escape($salt = substr(md5(uniqid(rand(), true)), 0, 9)) . "'
			, password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "'
			, status = '" . $this->db->escape($data['status']) . "'
			, company_id = '".$company_id."'
			, create_user = '".strtoupper($username)."'
			, create_date = curdate()
			, modify_user = '".strtoupper($username)."'
			, modify_date = curdate()
		WHERE  cashier_id = '" . (int)$cashier_id . "'");
	
		$this->cache->delete('cashier');
	}
	
	public function deleteCashier($cashier_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "cashier WHERE cashier_id = '" . (int)$cashier_id . "'");
		$this->cache->delete('cashier');
	}
	
	public function getCashier($cashier_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier AS CA
		LEFT JOIN " . DB_PREFIX . "cashier_group AS CG ON (CA.cashier_group_id = CG.cashier_group_id) 
		WHERE CA.cashier_id = '" . (int)$cashier_id . "' AND CA.company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}
		
	public function getCashiers($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "cashier AS CA
			LEFT JOIN " . DB_PREFIX . "cashier_group AS CG ON (CA.cashier_group_id = CG.cashier_group_id) 
			WHERE CA.company_id = '" . (int)$company_id . "'";
			
			$sort_data = array(
				'CA.cashier_name',
				'CG.cashier_group_name',
				'CG.email',
				'CG.status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY CA.cashier_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}		
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$cashier_data = $this->cache->get('cashier');
			if (!$cashier_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier AS CA
				LEFT JOIN " . DB_PREFIX . "cashier_group AS CG ON (CA.cashier_group_id = CG.cashier_group_id) 
				WHERE CA.company_id = '" . (int)$company_id . "' ORDER BY CA.cashier_name ASC");
				$cashier_data = $query->rows;
				$this->cache->set('cashier', $cashier_data);
			}

			return $country_data;			
		}	
	}
	
	public function getTotalCashiers() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cashier WHERE company_id = '" . (int)$company_id . "'");
		return $query->row['total'];
	}
	
	public function isCheckAlreadyUser($email,$cashier_id='') {
		$company_id	= $this->session->data['company_id'];
		if($cashier_id) {
			$query = $this->db->query("select 
				NOT EXISTS (select email from ". DB_PREFIX."cashier where email = '".$this->db->escape($email)."' AND cashier_id != '".$this->db->escape($cashier_id)."') AND 
				NOT EXISTS (select email from ". DB_PREFIX ."user where email = '". $this->db->escape($email)."') AS total");	
		} else {
		$query = $this->db->query("select 
				NOT EXISTS (select email from ". DB_PREFIX."cashier where email = '".$this->db->escape($email)."') AND 
				NOT EXISTS (select email from ". DB_PREFIX ."user where email = '". $this->db->escape($email)."') AS total");
		}
		return $query->row['total'];
	}
	
	public function getCashierCollection() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}	
	
	public function getCashierGroupName($group_id) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "' AND cashier_group_id = '" . (int)$group_id . "'");
		return $query->row;
	}
}
?>