<?php
class ModelMasterCashierCashiergroup extends Model {

	public function getCashierGroupDetails($cashiergroup_id='') {
		if(!empty($dept_id)) {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "CashierGroup As CG WHERE CG.CashierGroupId = '" . (int)$cashiergroup_id . "'");
		} else {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "CashierGroup");
		}
		return $query->row;
	}
	
	public function editCashierGroupDetails($cashiergroup_id,$data) {
		foreach($data as $key=>$values) {
			//if(in_array($key,$formFieldNames)) {
				$this->db->query("UPDATE " . DB_PREFIX . "CashierGroup SET ".$this->db->escape($key). " = '" . $this->db->escape($values) . "'  WHERE CashierGroupId = '" . (int)$cashiergroup_id . "'");	
			//}
		}
	}
	
	public function addCashiergroupDetails($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "CashierGroup SET CashierGroupName = '" . $this->db->escape($data['CashierGroupName']) . "', Remarks = '" . $this->db->escape($data['Remarks']) . "', CreateUser = 1 , CreateDate = NOW(), ModifyUser = 1, ModifyDate = NOW()");
		$dept_id = $this->db->getLastId();
		return $dept_id;
	}
	
	public function getTotalCashierGroup($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "CashierGroup As CG";

		if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
			$sql .= " WHERE LCASE(CG.CashierGroupName) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getCashierGroup($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "CashierGroup As CG";

			if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
				$sql .= " WHERE LCASE(CG.CashierGroupName) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
			}

			$sort_data = array(
				'CG.CashierGroupName'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY CG.CashierGroupName";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			$cashiergroup_data = $this->cache->get('CashierGroup.' . $this->config->get('config_language_id'));

			if (!$cashiergroup_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "CashierGroup AS CG ORDER BY CG.CashierGroupName ASC");
				$cashiergroup_data = $query->rows;
				$this->cache->set('CashierGroup.' . $this->config->get('config_language_id'), $cashiergroup_data);
			}

			return $cashiergroup_data;
		}
	}
	
	public function deleteCashierGroup($cashiergroup_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "CashierGroup WHERE CashierGroupId = '" . (int)$cashiergroup_id . "'");
		$this->cache->delete('CashierGroup');
	}

}