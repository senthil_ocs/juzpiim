<?php
class ModelMasterPayment extends Model {
	public function addPayment($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];


		$this->db->query("INSERT INTO " . DB_PREFIX . "payment (payment_name,description,status,position,company_id,create_user,create_date,modify_user,modify_date) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['description']) . "','" . (int)$this->db->escape($data['status']) . "','" . (int)$this->db->escape($data['position']) . "','".$company_id."','".$userId."',curdate(),'".$userId."',curdate())");
	}
	
	public function paymentTypeCheck($payment_name){
		$sql = "select COUNT(invoice_no) as tot from vw_sales_paymode where payment_type='".$payment_name."'";
		$query = $this->db->query($sql);
		return $query->row['tot'];
	}

	public function getPaymentName($payment_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment WHERE payment_id = '" . (int)$payment_id . "'");
		return $query->row['payment_name'];
	}
	public function editPayment($payment_id, $data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "payment SET 
			payment_name = '" . $this->db->escape($data['name']) . "'
			, description = '" . $this->db->escape($data['description']) . "'
			, status = '" . (int)$this->db->escape($data['status']) . "'
			, position = '" . (int)$this->db->escape($data['position']) . "'
			, company_id = '".$company_id."'
			, modify_user = '".$userId."'
			, modify_date = curdate()
		WHERE payment_id = '" . (int)$payment_id . "'");
	
		$this->cache->delete('payment');
	}
	
	public function deletePayment($payment_id) {
		$tableDetails = $this->getPayment($payment_id);
		$strLogDetails = array(
			'type' 		=> 'Payment',
			'id'   		=> $payment_id,
			'code' 		=> $tableDetails['payment_name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "payment WHERE payment_id = '" . (int)$payment_id . "'");
		$this->cache->delete('payment');
	}


	
	public function getPayment($payment_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment WHERE payment_id = '" . (int)$payment_id . "'");
		return $query->row;
	}
		
	public function getPayments($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "payment WHERE company_id = '" . (int)$company_id . "'";
			
			$sort_data = array(
				'payment_name',
				'position',
				'status'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY position";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}		
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$payment_data = $this->cache->get('payment');
		
			if (!$payment_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment WHERE company_id = '" . (int)$company_id . "' ORDER BY payment_name ASC");
	
				$payment_data = $query->rows;
			
				$this->cache->set('payment', $payment_data);
			}

			return $payment_data;			
		}	
	}
	
	public function getTotalPayments() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "payment WHERE company_id = '" . (int)$company_id . "'");
		return $query->row['total'];
	}
	
	public function getAllPaymode($data=array()) {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$string     = '"'. implode('","', explode(',', $data['payment_method'])) .'"';
		//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "payment WHERE company_id = '".$company_id."' AND status = '1'";
		$sql = "SELECT * FROM " . DB_PREFIX . "payment WHERE company_id = '".$company_id."' AND status = '1'";
		if(isset($data['payment_method']) && !empty($data['payment_method'])) {
			$sql .= " AND (payment_id IN (".$string.") OR (LOWER(payment_name) IN (".$string.") ))";
		}
		$sql .= " ORDER BY position ASC";
		$query = $this->db->query($sql);
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>