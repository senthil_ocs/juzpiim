<?php
class ModelMasterCashiergroup extends Model {

	public function getCashierGroupCollection($cashiergroup_id='') {
		$company_id	= $this->session->data['company_id'];
		if(!empty($cashiergroup_id)) {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "' 
			AND cashier_group_id = '" . (int)$cashiergroup_id . "'");
		} else {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "'");
		}
		return $query->row;
	}
	
	public function getCollection() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	
	public function editCashierGroup($cashiergroup_id,$data) {
		$user_id	= $this->session->data['user_id'];
		$userName	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "cashier_group SET 
		 cashier_group_name = '" . $this->db->escape($data['name']) . "'
		, permission = '" . (isset($data['permission']) ? serialize($data['permission']) : '') . "' 
		, remarks = '" . $this->db->escape($data['remarks']) . "'
		, company_id = '".$company_id."'
		, create_user = '".strtoupper($userName)."'
		, create_date = curdate()
		, modify_user = '".strtoupper($userName)."'
		, modify_date = curdate()
		WHERE cashier_group_id = '" . (int)$cashiergroup_id . "'");
	}
	
	public function addCashierGroup($data) {
		$user_id	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "cashier_group SET 
			cashier_group_name = '" . $this->db->escape($data['name']) . "'
			, permission = '" . (isset($data['permission']) ? serialize($data['permission']) : '') . "' 
			, remarks = '" . $this->db->escape($data['remarks']) . "'
			, company_id = '".$company_id."'
			, create_user = '".strtoupper($userName)."'
			, create_date = curdate()
			, modify_user = '".strtoupper($userName)."'
			, modify_date = curdate()
			");
	}
	
	public function getTotalCashierGroups($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "cashier_group As CG WHERE CG.company_id = '" . (int)$company_id . "'";
		/*if (isset($data['filter_name']) && !is_null($data['filter_name'])) {
			$sql .= " WHERE LCASE(CG.CashierGroupName) LIKE '%" . $this->db->escape(strtolower($data['filter_name'])) . "%'";
		}*/
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	
	public function getCashierGroups($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "cashier_group As CG WHERE CG.company_id = '" . (int)$company_id . "'";
			$sort_data = array(
				'CG.cashier_group_name'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY CG.cashier_group_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			$cashiergroup_data = $this->cache->get('CashierGroup.' . $this->config->get('config_language_id'));
			if (!$cashiergroup_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier_group AS CG WHERE CG.company_id = '" . (int)$company_id . "' ORDER BY CG.cashier_group_name ASC");
				$cashiergroup_data = $query->rows;
				$this->cache->set('CashierGroup.' . $this->config->get('config_language_id'), $cashiergroup_data);
			}

			return $cashiergroup_data;
		}
	}
	
	public function deleteCashierGroup($cashiergroup_id) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("DELETE FROM " . DB_PREFIX . "cashier_group WHERE company_id = '" . (int)$company_id . "' AND cashier_group_id = '" . (int)$cashiergroup_id . "'");
		$this->cache->delete('CashierGroup');
	}
	
	public function getPermissionFunctionKey() {
		$_permission	= array('120'=>'Reprint','Disc','Void','Tender','Clear','Hold','Release','Sales','Ex Item','Exit','$ Chg','Auto','Search','Refund');
		return $_permission;
	}
	
	public function getShortCutKey() {
		$_permission	= array('120'=>'F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','F11','F12','Home','End');
		return $_permission;
	}

}