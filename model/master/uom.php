<?php
class ModelMasterUom extends Model {
	public function addUom($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
	    $this->db->query("INSERT INTO " . DB_PREFIX . "uom (code,uom_name,description,company_id,create_user,create_date,modify_user,modify_date) VALUES('" . $this->db->escape($data['code']) . "','" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['description']) . "','".$company_id."','".$userId."',curdate(),'".$userId."',curdate())");	
	}
	
	public function editUom($uom_id, $data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "uom SET 
			  code = '" . $this->db->escape($data['code']) . "'
			, uom_name = '" . $this->db->escape($data['name']) . "'
			, description = '" . $this->db->escape($data['description']) . "'
			, company_id = '".$company_id."'
			, modify_user = '".$userId."'
			, modify_date = curdate()
		 WHERE uom_id = '" . (int)$uom_id . "'");
	
		//$this->cache->delete('uom');
	}
	
	public function deleteUom($uom_id) {
		$tableDetails = $this->getTableIdBy($uom_id);
		$strLogDetails = array(
			'type' 		=> 'Uom',
			'id'   		=> $tableDetails['uom_id'],
			'code' 		=> $uom_id,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "uom WHERE code = '" . trim($uom_id). "'");
		//$this->cache->delete('uom');
	}
	
	
	public function getUom($uom_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "uom WHERE uom_id = '" . (int)$uom_id . "' AND company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
		
	public function getUoms($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {

			$sql = "SELECT * FROM " . DB_PREFIX . "uom
			WHERE company_id = '" . (int)$company_id . "'";
			
			$sort_data = array(
				'uom_name',
				'code',
				'description'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY uom_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}		
			
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			if (!$uom_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "uom WHERE company_id = '" . (int)$company_id . "' ORDER BY uom_name ASC");
				$uom_data = $query->rows;
				$this->cache->set('uom', $uom_data);
			}
			return $uom_data;			
		}	
	}
	
	public function getTotalUoms() {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "uom WHERE company_id = '" . (int)$company_id . "'");
		return $query->row['total'];
	}
	public function getUOMByCode($uom_code,$uom_id='') {
		$sql = "SELECT count(uom_id) as tot  FROM " . DB_PREFIX . "uom WHERE code = '" .$this->db->escape(trim($uom_code)). "'";
		if($uom_id){
			$sql.=" AND uom_id!= '" . (int)$uom_id . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows[0]['tot'];
	}
	public function getTotalProductsByUOM($code) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE sku_uom = '" . trim($code). "'");
		return $query->row['total'];
	}
	public function getTableIdBy($code){
		$sql = "SELECT uom_id FROM ".DB_PREFIX."uom WHERE code = '".trim($code)."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>