<?php
class ModelInventoryInventoryMovement extends Model {	
	
		public function getProductsByInventoryCode($inventory_code) {
		
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "product p ";
		//$sql .= " LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";			
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " WHERE p2co.company_id = '".$company_id."' ";		
		
		$sql .= " AND p.sku = '".$inventory_code."'";

		//$sql .= " GROUP BY p.product_id ORDER BY p.product_id ASC";

		$query = $this->db->query($sql);

		return $query->row;
	}

	public function getPurchaseByProductId($product_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase AS p LEFT JOIN " . DB_PREFIX . "purchase_to_product AS pp ON(p.purchase_id = pp.purchase_id) JOIN " . DB_PREFIX . "product_description AS pd ON(pp.product_id = pd.product_id) WHERE pp.product_id = '".$product_id."' AND p.company_id = '".$company_id."'");

		return $query->rows;
	}

	public function getPurchaseReturnByProductId($product_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase AS p LEFT JOIN " . DB_PREFIX . "purchase_to_product AS pp ON(p.purchase_id = pp.purchase_id) JOIN " . DB_PREFIX . "product_description AS pd ON(pp.product_id = pd.product_id) WHERE pp.product_id = '".$product_id."' AND p.company_id = '".$company_id."' AND p.purchase_return = '1'");

		return $query->rows;
	}

	public function getSalesByProductId($product_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order AS O LEFT JOIN " .DB_PREFIX . "order_product AS OP ON(O.order_id = OP.order_id) WHERE OP.product_id = '".$product_id."' AND O.company_id = '".$company_id."' AND OP.quantity != '-1'ORDER BY O.order_id DESC");

		return $query->rows;

	}

	public function getSalesReturnByProductId($product_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order AS O LEFT JOIN " .DB_PREFIX . "order_product AS OP ON(O.order_id = OP.order_id) WHERE OP.product_id = '".$product_id."' AND O.company_id = '".$company_id."' AND OP.quantity = '-1' ORDER BY O.order_id DESC");

		return $query->rows;

	}
}
?>