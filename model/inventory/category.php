<?php
class ModelInventoryCategory extends Model {
	public function addCategory($data) {
	
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];

		$this->db->query("INSERT INTO " . DB_PREFIX . "category (category_name,category_code,company_id,remarks,status,createdby,createdon,modifiedby,modifiedon,department_code) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['code']) . "','".$company_id."','" . $this->db->escape($data['remarks']) . "','" . (int)$this->db->escape($data['status']) . "','".$userName."',curdate(),'".$userName."', curdate(), '". $this->db->escape($data['department_code']) ."') ");	 
	
		// get category details array
		$lastcateId = $this->db->getLastId();
		if(in_array($this->session->data['location_code'],API_LOCATION) && $lastcateId) {
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "category  WHERE category_id = '" . $lastcateId . "'");
			$query1 = $this->db->query("SELECT * FROM ". DB_PREFIX . "department  WHERE department_code = '" .$this->db->escape($data['department_code']) . "' ");
			$status = 'inActive';
			if($query->row['status']=='1'){
				$status = 'Active';
			}
			$category_data = array(
				"category_name"=> $query1->row['department_name'],
				"sub_category_name" => $query->row['category_name'],
				"status"=>$status
			);
			$res = curlpost('create_sub_category',$category_data);
		}
	}
	
	public function editCategory($category_id, $data) {
	
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		$this->db->query("UPDATE " . DB_PREFIX . "category SET 
		 category_name = '" . $this->db->escape($data['name']) . "'
		, category_code = '" . $this->db->escape($data['code']) . "'
		, remarks = '" . $this->db->escape($data['remarks']) . "'
		, status = '" . (int)$this->db->escape($data['status']) . "'
		, company_id = '".$company_id."'
		, modifiedby = '".$userName."'
		, modifiedon = curdate()
		,department_code = '" . $this->db->escape($data['department_code']). "'
		WHERE category_id = '" . (int)$category_id . "'");
		$this->cache->delete('category');
	}
	
	public function deleteCategory($category_id) {
		$tableDetails = $this->getTableIdBy($category_id);
		$strLogDetails = array(
			'type' 		=> 'Category',
			'id'   		=> $tableDetails['category_id'],
			'code' 		=> $category_id,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "category WHERE category_code = '" . trim($category_id) . "'");
		$this->cache->delete('category');
	}
	
	public function getCategory($category_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "' AND company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}
	
	public function getCategoryByDept($department_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "category WHERE status = '1' AND department_id = '" . (int)$department_id . "' AND company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
		
	public function getCategorys($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql = "SELECT * FROM " . DB_PREFIX . "category ";
			
			$sort_data = array(
				'category_name',
				'remarks',
				'status'
			);	
			$sql .= " WHERE category_name != '' ";

			if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql.= " AND category_name LIKE '%" . $data['filter_name'] . "%' ";
	    	}
			if(isset($data['filter_department']) && $data['filter_department'] != "") {
				$sql.= " AND department_code = '" . $this->db->escape($data['filter_department']) . "' ";
	    	}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY category_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			$query = $this->db->query($sql);

			return $query->rows;
		} else {

			if (!$category_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_name!='' ORDER BY category_name ASC");
	
				$category_data = $query->rows;
			
				$this->cache->set('category', $category_data);
			}

			return $category_data;	
		}	
	}
	public function getUsedCategorys(){
		return $this->db->query("SELECT c.* FROM " . DB_PREFIX . "category as c LEFT JOIN ".DB_PREFIX."product as p ON c.category_code=p.sku_category_code WHERE c.category_name!='' GROUP BY p.sku_category_code ORDER BY c.category_name ASC")->rows;
	}
	
	public function getTotalCategorys($data) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category WHERE category_id != ''";
		
		if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql.= " AND category_name LIKE '%" . $data['filter_name'] . "%' ";
	    }
		if(isset($data['filter_department']) && $data['filter_department'] != "") {
				$sql.= " AND department_code = '" . $this->db->escape($data['filter_department']) . "' ";
	    }

      	$query = $this->db->query($sql);
		return $query->row['total'];
	}	
	
	public function getTotalCategoryByDepartmentId($dept_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "category WHERE department_id = '" . (int)$dept_id . "'");
		return $query->row['total'];
	}
	public function getCategoryByCode($cat_code,$cat_id='') {
		$sql = "SELECT count(category_id) as tot  FROM " . DB_PREFIX . "category WHERE category_code = '" . trim($cat_code). "'";
		if($cat_id){
			$sql.=" AND category_id!= '" . (int)$cat_id . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows[0]['tot'];
	}
	public function getTableIdBy($code){
		$sql = "SELECT category_id FROM ".DB_PREFIX."category WHERE category_code = '".trim($code)."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getCategoryNameByCode($cat_code) {
		$sql = "SELECT *  FROM " . DB_PREFIX . "category WHERE category_code = '" . trim($cat_code). "'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getAllCategorys() {
		$company_id = $this->session->data['company_id'];
		$sql = "SELECT *  FROM " . DB_PREFIX . "category where company_id='".$company_id."' and category_name!='' order by category_name ASC ";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getAllSubCategorys($data) {
		$sql = "SELECT a.*,b.category_name as category_name,c.department_name,c.department_code  FROM ".DB_PREFIX."subcategory as a LEFT JOIN ".DB_PREFIX."category as b ON a.category_id=b.category_code LEFT JOIN ".DB_PREFIX."department as c ON b.department_code = c.department_code WHERE a.id != ''";
		
		if(isset($data['filter_name']) && $data['filter_name'] != "") {
			$sql.= " AND a.name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
    	}
		if(isset($data['filter_category']) && $data['filter_category'] != "") {
			$sql.= " AND a.category_id='".$this->db->escape($data['filter_category'])."' ";
    	}
		if(isset($data['filter_department']) && $data['filter_department'] != "") {
			$sql.= " AND c.department_code='".$this->db->escape($data['filter_department'])."' ";
    	}
		/*if($data['filter_name']!='' && $data['filter_category']==''){
			$sql.= " WHERE a.name LIKE '%" . $data['filter_name'] . "%' ";
		}
		if($data['filter_category']!='' && $data['filter_name']==''){
			$sql.= " WHERE a.category_id='".$data['filter_category']."' ";
		}
		if($data['filter_category']!='' && $data['filter_name']!=''){
			$sql.= " WHERE a.name='".$data['filter_name']."' AND a.category_id='".$data['filter_category']."' ";
		}*/
					
		$sql .= " order by a.id DESC";
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}

		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getAllSubCategorysTotal($data) {
		$sql = "SELECT count(*) as total  FROM ".DB_PREFIX."subcategory as a LEFT JOIN ".DB_PREFIX."category as b ON a.category_id=b.category_code LEFT JOIN ".DB_PREFIX."department as c ON b.department_code = c.department_code WHERE a.id != ''";
		
		if(isset($data['filter_name']) && $data['filter_name'] != "") {
			$sql.= " AND a.name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
    	}
		if(isset($data['filter_category']) && $data['filter_category'] != "") {
			$sql.= " AND a.category_id='".$this->db->escape($data['filter_category'])."' ";
    	}
		if(isset($data['filter_department']) && $data['filter_department'] != "") {
			$sql.= " AND c.department_code='".$this->db->escape($data['filter_department'])."' ";
    	}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function addSubCategory($data){
		$sql = "INSERT INTO ".DB_PREFIX."subcategory (category_id,name,status,created_by,modified_by,created_on,subCate_code) VALUES('".$this->db->escape($data['category_id'])."','".$this->db->escape($data['name'])."','".$this->db->escape($data['status'])."','".$this->db->escape($data['created_by'])."','".$this->db->escape($data['modified_by'])."',curdate(),'".$this->db->escape($data['subCate_code'])."' )";
		// echo $sql; die;
		$query = $this->db->query($sql);
	}
	public function updateSubCategory($data){
		$sql = "UPDATE ".DB_PREFIX."subcategory set category_id='".$this->db->escape($data['category_id'])."', name='".$this->db->escape($data['name'])."', status='".$this->db->escape($data['status'])."', modified_by='".$this->db->escape($data['modified_by'])."',modified_on=curdate(),subCate_code='".$this->db->escape($data['subCate_code'])."' where id='".$data['id']."' ";
		$query = $this->db->query($sql);	
	}
	public function subCate_delete($id){
		$sql = "DELETE from ".DB_PREFIX."subcategory where id='".$id."' ";
		$query = $this->db->query($sql);
	}
	public function getSubCate($id){
		$sql = $this->db->query("SELECT * FROM  ".DB_PREFIX."subcategory where id='".$id."' ");
		return $sql->row;
	}
	public function ajaxGetSubcategory($catId){
		$sql = $this->db->query("SELECT * FROM  ".DB_PREFIX."subcategory where category_id='".$catId."' and status='1' ");
		return $sql->rows;	
	}
	public function ajaxGetCategory($department_code){
		$sql = $this->db->query("SELECT * FROM  ".DB_PREFIX."category where department_code='".$this->db->escape($department_code)."' and status='1' ");
		return $sql->rows;	
	}
}
?>