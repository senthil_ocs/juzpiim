<?php
class ModelInventoryNetworks extends Model {

	public function addProductShopify($productdata,$response_id = '') {

		if($response_id == '') {
			$this->load->model('master/vendor');
			$vendor = $this->model_master_vendor->getVendor($productdata['vendor']);
			$vendor = $vendor['vendor_name'];
			if($productdata['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$productdata['image'];
			} else {
				$image = '';
			}
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://".SHOPIFY_API.":".SHOPIFY_PASSWORD."@".SHOPIFY_HOSTNAME."/admin/api/".SHOPIFY_VERSION."/products.json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"product\": {\r\n    \"title\": \"".$productdata['name']."\",\r\n    \"body_html\": \"".$productdata['description']."\",\r\n    \"vendor\": \"".$vendor."\",\r\n    \"product_type\": \"".$productdata['product_department']."\",\r\n    \"variants\": [\r\n      {\r\n        \"price\": \"".$productdata['price_Shopify']."\",\r\n        \"sku\": \"".$productdata['sku']."\",\r\n        \"cost\": \"".$productdata['price_Shopify']."\",\r\n        \"requiresShipping\": true,\r\n        \"tracked\": true,\r\n        \"inventory_quantity\": ".round($productdata['minimum'])."\r\n      }\r\n    ],\r\n    \"images\": [\r\n      {\r\n        \"src\": \"".$image."\"\r\n      }\r\n    ]\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: application/json",
			    "Cookie: __cfduid=d14180a67ae4b7c2f1e708a2f79043c0f1592975738"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$response = json_decode($response);
			$product = $response->product;
			return $product->id;
		} else if($response_id != '') {
			$this->load->model('master/vendor');
			$vendor = $this->model_master_vendor->getVendor($productdata['vendor']);
			$vendor = $vendor['vendor_name'];
			if($productdata['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$productdata['image'];
			} else {
				$image = '';
			}
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://".SHOPIFY_API.":".SHOPIFY_PASSWORD."@".SHOPIFY_HOSTNAME."/admin/api/".SHOPIFY_VERSION."/products/".$response_id.".json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"product\": {\r\n    \"id\": ".$response_id.",\r\n   \"title\": \"".$productdata['name']."\",\r\n    \"body_html\": \"".$productdata['description']."\",\r\n    \"vendor\": \"".$vendor."\",\r\n    \"product_type\": \"".$productdata['product_department']."\",\r\n    \"variants\": [\r\n      {\r\n        \"price\": \"".$productdata['price_Shopify']."\",\r\n        \"sku\": \"".$productdata['sku']."\",\r\n        \"cost\": \"".$productdata['price_Shopify']."\",\r\n        \"requiresShipping\": true,\r\n        \"tracked\": true,\r\n        \"inventory_quantity\": ".round($productdata['minimum'])."\r\n      }\r\n    ],\r\n    \"images\": [\r\n      {\r\n        \"src\": \"".$image."\"\r\n      }\r\n    ]\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: application/json",
			    "Cookie: __cfduid=d14180a67ae4b7c2f1e708a2f79043c0f1592975738"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$response = json_decode($response);
			$product = $response->product;
			return $product->id;
		}
	}
	
	public function certificate_key() {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".QSM_API."&v=1.0&returnType=json&method=CertificationAPI.CreateCertificationKey&user_id=".QSM_USER_ID."&pwd=".QSM_PASSWORD."",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$res = json_decode($response);
		$res = (array)$res;
		return $res['ResultObject'];
	}

	public function addProductQSM($postArray,$response_id = '') {
		$certificate_key = $this->certificate_key();
		if($response_id == '') {
			if($postArray['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$postArray['image'];
			} else {
				$image = QSM_IMAGE;
			}
			$curl = curl_init();
			$request_url = "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".$certificate_key."&v=1.0&returnType=json&method=ItemsBasic.SetNewGoods&SecondSubCat=".QSM_SEC_SUB_CAT."&OuterSecondSubCat=&ManufactureNo=&BrandNo=&ItemTitle=".rawurlencode($postArray['name'])."&SellerCode=&IndustrialCode=&ProductionPlace=&AdultYN=&ContactTel=&StandardImage=".rawurlencode($image)."&ItemDescription=".rawurlencode($postArray['description'])."&AdditionalOption=&ItemType=&RetailPrice=".$postArray['price_qoo10']."&ItemPrice=".$postArray['price_qoo10']."&ItemQty=".round($postArray['minimum'])."&ExpireDate=&ShippingNo=&AvailableDateType=&AvailableDateValue=&Weight="; 
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $request_url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl); 
			
			curl_close($curl);
			$res = json_decode($response);
			$res = (array)$res;
			return $res['ResultObject']->GdNo;

		} else if($response_id != '') {
			$curl = curl_init();
			$request_url = "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".$certificate_key."&v=1.0&returnType=json&method=ItemsBasic.UpdateGoods&ItemCode=".$response_id."&SecondSubCat=".QSM_SEC_SUB_CAT."&ItemTitle=".rawurlencode($postArray['name'])."&ShortTitle=&BriefDescription=".rawurlencode($postArray['description'])."&SellerCode=&IndustrialCodeType=&IndustrialCode=&BrandNo=&ManufactureNo=&ManufactureDate=&ModelNm=&Material=&ProductionPlaceType=3&ProductionPlace=&RetailPrice=".$postArray['price_qoo10']."&Gift=&DisplayLeftPeriod=&AudultYN=N&ContactTel=&ContactEmail=&ShippingNo=0&OptionShippingNo1=&OptionShippingNo2=&Weight=&DesiredShippingDate=&AvailableDateType=&AvailableDateValue=";

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $request_url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			 $response = curl_exec($curl);
			curl_close($curl);
			return json_decode($response);
		}
	}
}