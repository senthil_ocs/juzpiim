<?php
class ModelInventoryNetworks extends Model {

	public function addProductShopify($productdata,$response_id = '') {

		if($response_id == '') {
			$this->load->model('master/vendor');
			$vendor = $this->model_master_vendor->getVendor($productdata['vendor']);
			$vendor = $vendor['vendor_name'];
			if($productdata['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$productdata['image'];
			} else {
				$image = '';
			}

			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://".SHOPIFY_API.":".SHOPIFY_PASSWORD."@".SHOPIFY_HOSTNAME."/admin/api/".SHOPIFY_VERSION."/products.json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_POSTFIELDS =>"{\r\n  \"product\": {\r\n   \"title\": \"".$productdata['name']."\",\r\n    \"body_html\": \"".$productdata['description']."\",\r\n    \"vendor\": \"".$vendor."\",\r\n    \"product_type\": \"".$productdata['product_department']."\",\r\n    \"variants\": [\r\n      {\r\n        \"price\": \"".$productdata['price']."\",\r\n        \"sku\": \"".$productdata['sku']."\",\r\n        \"cost\": \"".$productdata['sku_cost']."\",\r\n        \"requiresShipping\": true,\r\n        \"tracked\": true,\r\n        \"inventory_quantity\": ".round($productdata['qty'])."\r\n      }\r\n    ],\r\n    \"images\": [\r\n      {\r\n        \"src\": \"".$image."\"\r\n      }\r\n    ]\r\n  }\r\n}",
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: application/json",
			    "Cookie: __cfduid=d14180a67ae4b7c2f1e708a2f79043c0f1592975738"
			  ),
			));

			$response = curl_exec($curl);

			curl_close($curl);
			$response = json_decode($response);
			$product = $response->product;
			return $product->id;
		} else if($response_id != '') {
			$this->load->model('master/vendor');
			$vendor = $this->model_master_vendor->getVendor($productdata['vendor']);
			$vendor = $vendor['vendor_name'];
			if($productdata['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$productdata['image'];
			} else {
				$image = '';
			}

			$postdata = "{\r\n  \"product\": {\r\n    \"id\": ".$response_id.",\r\n    \"product_type\": \"".$productdata['product_department']."\",\r\n    \"variants\": [\r\n      {\r\n        \"price\": \"".$productdata['price']."\",\r\n        \"sku\": \"".$productdata['sku']."\",\r\n        \"cost\": \"".$productdata['sku_cost']."\",\r\n        \"requiresShipping\": true,\r\n        \"tracked\": true,\r\n        \"inventory_quantity\": ".round($productdata['qty'])."\r\n      }\r\n]\r\n  }\r\n}"; 


			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://".SHOPIFY_API.":".SHOPIFY_PASSWORD."@".SHOPIFY_HOSTNAME."/admin/api/".SHOPIFY_VERSION."/products/".$response_id.".json",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "PUT",
			  CURLOPT_POSTFIELDS =>$postdata,
			  CURLOPT_HTTPHEADER => array(
			    "Content-Type: application/json",
			    "Cookie: __cfduid=d14180a67ae4b7c2f1e708a2f79043c0f1592975738"
			  ),
			));

			$response = curl_exec($curl);
			curl_close($curl);
			$response = json_decode($response);
			$product = $response->product;
			return $product->id;
		}
	}
	
	public function certificate_key() {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".QSM_API."&v=1.0&returnType=json&method=CertificationAPI.CreateCertificationKey&user_id=".QSM_USER_ID."&pwd=".QSM_PASSWORD."",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$res = json_decode($response);
		$res = (array)$res;
		return $res['ResultObject'];
	}

	public function addProductQSM($postArray,$response_id = '') {
		$certificate_key = $this->certificate_key();
		if($response_id == '') {
			if($postArray['image'] != ''){
				$image = HTTP_SERVER.'uploads/'.$postArray['image'];
			} else {
				$image = QSM_IMAGE;
			}
			$curl = curl_init();
			$request_url = "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".$certificate_key."&v=1.0&returnType=json&method=ItemsBasic.SetNewGoods&SecondSubCat=".QSM_SEC_SUB_CAT."&OuterSecondSubCat=&ManufactureNo=&BrandNo=&ItemTitle=".rawurlencode($postArray['name'])."&SellerCode=&IndustrialCode=&ProductionPlace=&AdultYN=&ContactTel=&StandardImage=".rawurlencode($image)."&ItemDescription=".rawurlencode($postArray['description'])."&AdditionalOption=&ItemType=&RetailPrice=".$postArray['price']."&ItemPrice=".$postArray['price']."&ItemQty=".round($postArray['qty'])."&ExpireDate=&ShippingNo=&AvailableDateType=&AvailableDateValue=&Weight="; 
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $request_url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			$response = curl_exec($curl); 
			
			curl_close($curl);
			$res = json_decode($response);
			$res = (array)$res;
			return $res['ResultObject']->GdNo;

		} else if($response_id != '') {
			$curl = curl_init();
			$request_url = "https://api.qoo10.sg/GMKT.INC.Front.QAPIService/Giosis.qapi?key=".$certificate_key."&v=1.0&returnType=json&method=ItemsBasic.UpdateGoods&ItemCode=".$response_id."&SecondSubCat=".QSM_SEC_SUB_CAT."&ItemTitle=".rawurlencode($postArray['name'])."&ShortTitle=&BriefDescription=".rawurlencode($postArray['description'])."&SellerCode=&IndustrialCodeType=&IndustrialCode=&BrandNo=&ManufactureNo=&ManufactureDate=&ModelNm=&Material=&ProductionPlaceType=3&ProductionPlace=&RetailPrice=".$postArray['price']."&Gift=&DisplayLeftPeriod=&AudultYN=N&ContactTel=&ContactEmail=&ShippingNo=0&OptionShippingNo1=&OptionShippingNo2=&Weight=&DesiredShippingDate=&AvailableDateType=&AvailableDateValue=";

			curl_setopt_array($curl, array(
			  CURLOPT_URL => $request_url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			));

			 $response = curl_exec($curl);
			curl_close($curl);
			return json_decode($response);
		}
	}
	public function addProductShopee($postArray,$response_id = '')
	{
		$count    = strlen($postArray["description"]);
		if($count < 20){
			$postArray["description"] = $postArray["description"].'____________________';
		}
		
		if($response_id == '') {
			$name = $postArray["name"];
			$description = $postArray["description"];
			$price = $postArray["price"];
			$qty = $postArray["qty"];
			$request_json ='{
				  "category_id": '.SHOPEE_CATEGORY_ID.',
				  "name": "'.$name.'",
				  "description": "'.$description.'",
				  "price": '.$price.',
				  "stock": '.$qty.',
				  "images": [{
				    "url": "'.SHOPEE_IMAGE.'"
				  }],
				  "weight": 5,
				  "attributes": [{
				    "attributes_id": 10136,
				    "value": "Blooming Home"
				  }],
				  "logistics": [{
				    "logistic_id": 19911,
				    "enabled": true
				  }],
				  "partner_id": '.SHOPEE_PARTNER_ID.',
				  "shopid": '.SHOPEE_SHOP_ID.',
				  "timestamp": '.time().',
				  "status": "NORMAL"
			}';
			$url = SHOPEE_BASE_URL."item/add";
			$response_result = $this->updateShopee($url,$request_json);
			return $response_result;
		} else if ($response_id != '') {
			$name = $postArray["name"];
			$description = $postArray["description"];
			$price = (float)$postArray["price"];
			$qty = (int)$postArray["qty"];
			$response_id = $response_id + 0;

			/* To update Name and Description */
			$products =  array("item_id" => $response_id,
                  "category_id" => SHOPEE_CATEGORY_ID,
                  "name" => $name,
                  "description" => $description,
                  "partner_id" => SHOPEE_PARTNER_ID,
                  "shopid" => SHOPEE_SHOP_ID,
                  "timestamp" => time(),
            );
			$request_json = stripcslashes(json_encode($products));
			$url = SHOPEE_BASE_URL."item/update";
			$response_result = $this->updateShopee($url,$request_json);

			/* To update Price */
            $products_price = array(
			    "item_id" => $response_id,
			    "price" => $price,
			    "partner_id" => SHOPEE_PARTNER_ID,
			    "shopid" => SHOPEE_SHOP_ID,
			    "timestamp" => time()
			);
			$request_json_price = json_encode($products_price);
			$url_price = SHOPEE_BASE_URL."items/update_price";
			$response_result = $this->updateShopee($url_price,$request_json_price);

			/* To update Stock */
            $products_qty = array(
			    "item_id" => $response_id,
			    "stock" => $qty,
			    "partner_id" => SHOPEE_PARTNER_ID,
			    "shopid" => SHOPEE_SHOP_ID,
			    "timestamp" => time()
			);
			$request_json_qty = stripcslashes(json_encode($products_qty));
			$url_qty = SHOPEE_BASE_URL."items/update_stock";
			$response_result = $this->updateShopee($url_qty,$request_json_qty);
			return $response_result;
		}
	}
	function updateShopee ($url,$request_json) {
		$data = $url."|".$request_json;
		$token = rawurlencode(hash_hmac('sha256', $data, SHOPEE_PARTNER_KEY));
		$curl = curl_init();
		curl_setopt_array($curl, array(
		    CURLOPT_URL => $url,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_SSL_VERIFYPEER => false,
		    CURLOPT_SSL_VERIFYHOST => false,
		    CURLOPT_POST => true,
		    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		    CURLOPT_CUSTOMREQUEST => "POST",
		    CURLOPT_POSTFIELDS => $request_json,
		    CURLOPT_HTTPHEADER => array(
		      "Authorization:".$token,
		      "Content-Type: application/json"
		    ),
		));
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		return json_decode($response);
	}

	public function addProductLazada($postArray,$response_id = '')
	{
		require("lazada/api.php");

		$product_details = array(
				"primary_category"  => "10000552", 
				"name"              => $postArray['name'], 
				"short_description" => $postArray['description'], 
				"brand"             => "ABBA", 
				"seller_sku"        => $postArray["sku"], 
				"color_family"      => "Green", 
				"quantity"          => $postArray['qty'], 
				"price"             => $postArray['price'], 
				"package_length"    => "11",
				"package_height"    => "12",
				"package_weight"    => "13",
				"package_width"     => "14",
				"package_content"   => $postArray['name'],
		);
		if($response_id == '') {
			$result_products = addProduct($product_details,'lazada','add');
		} else {
			$result_products = addProduct($product_details,'lazada','update');
		}
		return $result_products->data;
	}
	public function addProductLazmall($postArray,$response_id = '')
	{
		require("lazada/api.php");
		$product_details = array(
				"primary_category"  => "10000552", 
				"name"              => $postArray['name'], 
				"short_description" => $postArray['description'], 
				"brand"             => "Megafurniture", 
				"seller_sku"        => $postArray["sku"], 
				"color_family"      => "Green", 
				"quantity"          => $postArray['qty'], 
				"price"             => $postArray['price'], 
				"package_length"    => "11",
				"package_height"    => "12",
				"package_weight"    => "13",
				"package_width"     => "14",
				"package_content"   => $postArray['name'],
		);
		if($response_id == '') {
			$result_products = addProduct($product_details,'lazmall','add');
		} else {
			$result_products = addProduct($product_details,'lazmall','update');
		}
		return $result_products->data;
	}
}