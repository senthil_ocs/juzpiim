<?php
class ModelInventoryReports extends Model {
	
	public function getProductDepartmentName($product_id) {
		$query = $this->db->query("SELECT d.department_name FROM " . DB_PREFIX . "department d
				LEFT JOIN " . DB_PREFIX . "product_to_department p2d ON (d.department_id = p2d.department_id)
				WHERE p2d.product_id = '" . (int)$product_id . "'");
		return $query->row['department_name'];
	}
	public function getProductCategoryName($product_id) {
		$query = $this->db->query("SELECT c.category_name FROM " . DB_PREFIX . "category c
				LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (c.category_id = p2c.category_id)
				WHERE p2c.product_id = '" . (int)$product_id . "'");
		return $query->row['category_name'];
	}
	public function getProductBrandName($product_id) {
		$product_brand_data = array();
		$query = $this->db->query("SELECT b.brand_name FROM " . DB_PREFIX . "brand b
				LEFT JOIN " . DB_PREFIX . "product_to_brand p2b ON (b.brand_id = p2b.brand_id)
				WHERE p2b.product_id = '" . (int)$product_id . "'");
		foreach ($query->rows as $result) {
			$product_brand_data[] = $result['brand_name'];
		}	
		return $product_brand_data;
	}
	public function getCompanyDetails($company_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE c.company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	public function getReportCategory() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT c.*, d.department_name FROM " . DB_PREFIX . "category as c 
		        LEFT JOIN " . DB_PREFIX . "department AS d ON (c.department_id = d.department_id) 
				WHERE c.company_id = '" . (int)$company_id . "' ");	
		return $query->rows;
	}
	public function getprofitsummaryDetails($data)
	{
	   $sql = "SELECT invoice_date as salesmonth,max(b.department_name) department_name,sum(Actual_sales ) Actual_sales ,  sum((SKu_avg*Qty)) CostOfSales, Sum(Actual_sales-(SKu_avg*Qty))  Profit from vw_profit_report a , tbl_department b";

	   $sql.= " Where a.sku_department_code = b.department_code";
		if($data['filter_department']){
			$sql .= " AND sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if($data['filter_category']){
			$sql .= " AND a.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to']))); 
			$sql .= " AND Invoice_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}

		$sql.=" Group by invoice_date,sku_department_code Order by invoice_date,sku_department_code";
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getprofitsummaryDetailsCount2($data)
	{
	   $sql = "SELECT invoice_date as salesmonth,max(b.department_name) department_name,sum(Actual_sales ) Actual_sales ,  sum((SKu_avg*Qty)) CostOfSales, Sum(Actual_sales-(SKu_avg*Qty))  Profit from vw_profit_report a , tbl_department b";

	   $sql.= " Where a.sku_department_code = b.department_code";
		if($data['filter_department']){
			$sql .= " AND sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if($data['filter_category']){
			$sql .= " AND a.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to']))); 
			$sql .= " AND Invoice_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$sql.=" Group by invoice_date,sku_department_code Order by invoice_date,sku_department_code";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getprofitsummaryDetailsCount($data)
	{
	   $sql = "SELECT invoice_date as salesmonth,max(b.department_name) department_name,sum(Actual_sales ) Actual_sales ,sum((SKu_avg*Qty)) CostOfSales, Sum(Actual_sales-(SKu_avg*Qty))  Profit from vw_profit_report a , tbl_department b";

	   $sql.= " Where a.sku_department_code = b.department_code";
		if($data['filter_location']){
			$sql .= " AND a.location_code = '" . $this->db->escape($data['filter_location']) . "'";
		}
		if($data['filter_department']){
			$sql .= " AND a.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_category']){
			$sql .= " AND a.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));
			$sql .= " AND a.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		// $sql.=" Group by datename(Month,invoice_date),sku_department_code Order by datename(Month,invoice_date),sku_department_code";
		// echo $sql; die;		
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getprofitDetails($data)
	{
	   $sql = "SELECT Invoice_Date,max(b.department_name) as department_name,sku,max(sku_description) sku_description, sum(Actual_sales ) Actual_sales , sum(SKu_avg*Qty) CostOfSales, Sum(Actual_sales-(SKu_avg*Qty))  Profit from vw_profit_report a , tbl_department b";
	   $sql.= " Where a.sku_department_code = b.department_code";
		if($data['filter_department']){
			$sql .= " AND a.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$sql.=" Group by invoice_date,a.sku_department_code,sku Order by invoice_date,a.sku_department_code,sku";
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getprofitDetailsCount($data)
	{
	    $sql = "SELECT Invoice_Date,max(b.department_name) as department_name,sku,max(sku_description) sku_description, sum(Actual_sales ) Actual_sales , 
	    sum(SKu_avg*Qty) CostOfSales, Sum(Actual_sales-(SKu_avg*Qty))  Profit from vw_profit_report a , tbl_department b";
	   $sql.= " Where a.sku_department_code = b.department_code";
		if($data['filter_department']){
			$sql .= " AND a.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from']))); 
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));
			$sql .= " AND invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		$sql.=" Group by invoice_date,a.sku_department_code,sku Order by invoice_date,a.sku_department_code,sku";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getLocationDetails($location){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."location where location_code='".$location."' ")->row;
	}

    public function getInventoryMovementForSales($data)
    {
        $sql = "SELECT dh.created_at as rdate, sum(dd.qty) as qty,dh.sales_transaction_no as invoice_no, '1' as method,sh.location_code as location, 'Sales' as type FROM ".DB_PREFIX."sales_do_header as dh LEFT JOIN ".DB_PREFIX."sales_do_details as dd ON dh.do_no=dd.do_no LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON sh.invoice_no = dh.sales_transaction_no where dh.status NOT IN ('Not_delivered','Canceled','Faild_Delivery') AND dd.product_id ='".$data['filter_product_id']."' ";

        if($data['filter_date_from'] !='' && $data['filter_date_to'] !=''){
            $sql .=" AND DATE(dh.created_at) between '".changeDates($data['filter_date_from'])."' AND '".changeDates($data['filter_date_to'])."' ";
        }
        if($data['filter_location']){
            $sql .=" AND sh.location_code ='".$data['filter_location']."' ";
        }
        $sql .=" Group by dh.created_at, sh.invoice_no, dd.product_id ";
        // echo $sql; die;
        return $this->db->query($sql)->rows;
    }
    public function getInventoryMovementForPurchase($data)
    {
        $sql = "SELECT ph.transaction_date as rdate,ph.transaction_no as invoice_no, sum(pd.quantity) as qty, '0' as method, ph.location_code as location, 'Purchase' as type FROM ".DB_PREFIX."purchase_invoice_details as pd LEFT JOIN ".DB_PREFIX."purchase_invoice_header as ph ON ph.purchase_id = pd.purchase_id where ph.purchase_id !='' AND ph.deleted='0' AND pd.product_id ='".$data['filter_product_id']."' ";

        if($data['filter_date_from'] !='' && $data['filter_date_to'] !=''){
            $sql .=" AND ph.transaction_date between '".changeDates($data['filter_date_from'])."' AND '".changeDates($data['filter_date_to'])."' ";
        }
        if($data['filter_location']){
            $sql .=" AND ph.location_code ='".$data['filter_location']."' ";
        }
        $sql .=" Group by ph.transaction_date, ph.transaction_no, pd.product_id ";
        // echo $sql; die; 
        return $this->db->query($sql)->rows;
    }
    public function getInventoryMovementForStock($data)
    {
        $sql = "SELECT sh.stkAdjustment_date as rdate,sh.stkAdjustment_id as invoice_no, sum(sd.qty) as qty,sh.add_or_deduct as method, sh.location_code as location, 'Stock' as type FROM ".DB_PREFIX."stockadjustment_detail as sd LEFT JOIN ".DB_PREFIX."stockadjustment_header as sh ON sh.stkAdjustment_id = sd.stockAdjustment_id where sh.stkAdjustment_id !='' AND sd.product_id ='".$data['filter_product_id']."' ";

        if($data['filter_date_from'] !='' && $data['filter_date_to'] !=''){
            $sql .=" AND sh.stkAdjustment_date between '".changeDates($data['filter_date_from'])."' AND '".changeDates($data['filter_date_to'])."' ";
        }
        if($data['filter_location']){
            $sql .=" AND sh.location_code ='".$data['filter_location']."' ";
        }
        $sql .=" Group by sh.stkAdjustment_date, sh.stkAdjustment_id, sd.product_id ";
        // echo $sql; 
        return $this->db->query($sql)->rows;
    }

    public function getInventoryMovementForSalesChild($data)
    {
        $sql = "SELECT dh.created_at as rdate, sum(dd.qty * cp.quantity) as qty,dh.sales_transaction_no as invoice_no, '1' as method,sh.location_code as location, 'Sales Child' as type FROM ".DB_PREFIX."child_products as cp left join ".DB_PREFIX."sales_do_details as dd on cp.parant_sku = dd.product_id left join ".DB_PREFIX."sales_do_header as dh ON dd.do_no = dh.do_no LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh on dh.sales_transaction_no=sh.invoice_no where dh.status NOT IN ('Not_delivered','Canceled','Faild_Delivery') AND cp.child_sku  ='".$data['filter_product_id']."' ";

        if($data['filter_date_from'] !='' && $data['filter_date_to'] !=''){
            $sql .=" AND DATE(dh.created_at) between '".changeDates($data['filter_date_from'])."' AND '".changeDates($data['filter_date_to'])."' ";
        }
        if($data['filter_location']){
            $sql .=" AND sh.location_code ='".$data['filter_location']."' ";
        }
        $sql .=" Group by dh.created_at, sh.invoice_no ";
        // echo $sql; die;
        return $this->db->query($sql)->rows;
    }

    public function getInventoryMovementForPurchaseChild($data)
    {
        $sql = "SELECT ph.transaction_date as rdate,ph.transaction_no as invoice_no, sum(pd.quantity * cp.quantity) as qty, '0' as method, ph.location_code as location, 'Purchase Child' as type FROM ".DB_PREFIX."child_products as cp LEFT JOIN ".DB_PREFIX."purchase_invoice_details as pd on cp.parant_sku=pd.product_id LEFT JOIN ".DB_PREFIX."purchase_invoice_header as ph ON ph.purchase_id = pd.purchase_id where ph.deleted='0' AND cp.child_sku ='".$data['filter_product_id']."' ";

        if($data['filter_date_from'] !='' && $data['filter_date_to'] !=''){
            $sql .=" AND ph.transaction_date between '".changeDates($data['filter_date_from'])."' AND '".changeDates($data['filter_date_to'])."' ";
        }
        if($data['filter_location']){
            $sql .=" AND ph.location_code ='".$data['filter_location']."' ";
        }
        $sql .=" Group by ph.transaction_date, ph.transaction_no ";
        // echo $sql; die; 
        return $this->db->query($sql)->rows;
    }
}
?>