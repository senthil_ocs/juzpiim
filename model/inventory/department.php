<?php
class ModelInventoryDepartment extends Model {
	public function addDepartment($data) {
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];

		$this->db->query("INSERT INTO " . DB_PREFIX . "department (department_name,department_code,status,remarks,company_id,createdby,createdon,modifiedby,modifiedon) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['code']) . "','".$data['status']."','" . $this->db->escape(trim($data['remarks'])) . "','".$company_id."','".$userName."',curdate(),'".$userName."',curdate())");

		$lastcateId = $this->db->getLastId();
		if(in_array($this->session->data['location_code'],API_LOCATION) && $lastcateId){
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "department  WHERE department_id = '" . $lastcateId . "'");
			$status = 'inActive';
			if($query->row['status']=='1'){
				$status = 'Active';
			}
			$category_data = array(
				"category_name"=>$query->row['department_name'],
				"status"=>$status
			);
			$res = curlpost('create_category',$category_data);
		}
	}
	
	public function editDepartment($department_id, $data) {
		
		$userId	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
		$this->db->query("UPDATE " . DB_PREFIX . "department SET 
		 department_name = '" . $this->db->escape($data['name']) . "'
		, department_code = '" . $this->db->escape($data['code']) . "'
		, remarks = '" . $this->db->escape(trim($data['remarks'])) . "'
		, status = '" . $data['status'] . "'
		, company_id = '".$company_id."'
		, modifiedby = '".strtoupper($userName)."'
		, modifiedon = curdate()
		WHERE department_id = '" . (int)$department_id . "'");
	
	}
	/*, createdby = '".strtoupper($userName)."'
		, createdon = curdate()*/
	public function deleteDepartment($department_id) {
		$tableDetails = $this->getTableIdBy($department_id);
		$strLogDetails = array(
			'type' 		=> 'Department',
			'id'   		=> $tableDetails['department_id'],
			'code' 		=> $department_id,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "department WHERE department_code = '" . trim($department_id). "'");
		
	}
	
	public function getDepartment($department_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "' ");
		return $query->row;
	}

	public function getDepartmentName($department_code) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE department_code = '" .$this->db->escape($department_code) . "' ");
		return $query->row;
	}
	
	public function getCollection() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "department WHERE company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}
		
	public function getDepartments($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "department";
			
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sort_data = array(
				'department_name',
				'remarks',
				'status',
				'createdon'

			);	
			
			if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql .= " WHERE department_name LIKE '%" . $data['filter_name'] . "%' ";
			}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY department_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			

			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
		
			if (!$department_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "department WHERE status='1' ORDER BY department_name ASC");
		
				$department_data = $query->rows;
			}

			return $department_data;	
		}	
	}
	
	public function getTotalDepartments($data) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "department";
		if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql.= " WHERE department_name LIKE '%" . $data['filter_name'] . "%' ";
	     }


      	$query = $this->db->query($sql);
		return $query->row['total'];
	}	

	public function getDepartmentByCode($department_code,$department_id='') {
		$sql = "SELECT count(department_id) as tot  FROM " . DB_PREFIX . "department WHERE department_code = '" .$this->db->escape(trim($department_code)). "'";
		if($department_id){
			$sql.=" AND department_id!= '" . (int)$department_id . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows[0]['tot'];
	}
	public function getTableIdBy($code){
		$sql = "SELECT department_id FROM ".DB_PREFIX."department WHERE department_code = '".trim($code)."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getAllDepartments(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."department ")->rows;
	}
	
}
?>