<?php
class ModelInventoryBrand extends Model {
	public function addBrand($data) {
		$this->load->model('inventory/category');
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$categoryDetails	= $this->model_inventory_category->getCategory($this->request->post['category']);
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "brand (brand_name,brand_code,company_id,remarks,status,createdby,createdon,modifiedby,modifiedon) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['code']) . "','".$company_id."','" . $this->db->escape($data['remarks']) . "','" . $this->db->escape($data['status']) . "','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate())"); 
		
		$brand_id = $this->db->getLastId();
		
		if (isset($data['category'])) {
			foreach ($data['category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_category (brand_id,category_id) VALUES ('" . (int)$brand_id . "','" . (int)$category_id . "')");
				
				$categoryDetails	= $this->model_inventory_category->getCategory($category_id);
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_department (brand_id,department_id) VALUES ('" . (int)$brand_id . "','" . $this->db->escape($categoryDetails['department_id'])  . "')"); 
			}
		}
		
	
		//$this->cache->delete('brand');
	}
	
	public function editBrand($brand_id, $data) {
		$this->load->model('inventory/category');
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "brand SET 
		 brand_name = '" . $this->db->escape($data['name']) . "'
		, brand_code = '" . $this->db->escape($data['code']) . "'
		, status = '" . $this->db->escape($data['status']) . "'
		, remarks = '" . $this->db->escape($data['remarks']) . "'
		, company_id = '".$company_id."'
		, modifiedby = '".strtoupper($userName)."'
		, modifiedon = curdate()
		WHERE brand_id = '" . (int)$brand_id . "'");
		/*
		, createdby = '".$userId."'
		, createdon = curdate()*/
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_category WHERE brand_id = '" . (int)$brand_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_department WHERE brand_id = '" . (int)$brand_id . "'");
		
		if (isset($data['category'])) {
			foreach ($data['category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_category (brand_id,category_id) VALUES ('" . (int)$brand_id . "','" . (int)$category_id . "')");
				
				$categoryDetails	= $this->model_inventory_category->getCategory($category_id);
				$this->db->query("INSERT INTO " . DB_PREFIX . "brand_to_department (brand_id,department_id) VALUES ('" . (int)$brand_id . "','" . $this->db->escape($categoryDetails['department_id'])  . "')"); 
			}
		}
	
		//$this->cache->delete('brand');
	}
	
	public function deleteBrand($brand_id) {
		$tableDetails = $this->getTableIdBy($brand_id);
		$strLogDetails = array(
			'type' 		=> 'Brand',
			'id'   		=> $tableDetails['brand_id'],
			'code'   	=> $brand_id,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "brand WHERE brand_code = '" . trim($brand_id) . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_category WHERE brand_id = '" . (int)$brand_id . "'");
		//$this->db->query("DELETE FROM " . DB_PREFIX . "brand_to_department WHERE brand_id = '" . (int)$brand_id . "'");
		//$this->cache->delete('brand');
	}
	
	public function getBrandByCategory($category_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "brand AS B
		LEFT JOIN " . DB_PREFIX . "brand_to_category AS b2c ON (b2c.brand_id = B.brand_id)
		WHERE b2c.category_id = '" . (int)$category_id . "' AND B.company_id = '" . (int)$company_id . "'");
		return $query->rows;
	}
	
	
	public function getBrand($brand_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT B.* FROM " . DB_PREFIX . "brand As B 
		WHERE B.brand_id = '" . (int)$brand_id . "' AND B.company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}
		
	public function getBrands($data = array()) {
		$company_id	= $this->session->data['company_id'];
		
		if ($data) {

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql = "SELECT DISTINCT B.* FROM " . DB_PREFIX . "brand As B
			WHERE B.company_id = '" . (int)$company_id . "'";
			
			if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql.= " AND B.brand_name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
	    	}

			$sort_data = array(
				'B.brand_name',
				'B.status'
			);	
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY B.brand_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}		
			$query = $this->db->query($sql);

			return $query->rows;
			
		} else {
			//$brand_data = $this->cache->get('brand');
			if (!$brand_data) {
				$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "brand AS B
				WHERE status='1' ORDER BY B.brand_name ASC");
				$brand_data = $query->rows;
				$this->cache->set('brand', $brand_data);
			}
			return $brand_data;	
		}	
	}
	
	public function getTotalBrands($data) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "brand WHERE company_id = '" . (int)$company_id . "'";
		if(isset($data['filter_name']) && $data['filter_name'] != "") {
				$sql.= " AND brand_name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ";
	    }

      	$query = $this->db->query($sql);
		return $query->row['total'];
	}	
	
	public function getTotalBrandByCategoryId($category_id) {
	
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "brand_to_category WHERE category_id = '" . (int)$category_id . "'");
		return $query->row['total'];
	}
	
	public function getBrandCategory($brand_id) {
		$brand_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "brand_to_category WHERE brand_id = '" . (int)$brand_id . "'");

		foreach ($query->rows as $result) {
			$brand_category_data[] = $result['category_id'];
		}

		return $brand_category_data;
	}
	
	public function getBrandDepartment($brand_id) {
		$brand_department_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "brand_to_department WHERE brand_id = '" . (int)$brand_id . "'");

		foreach ($query->rows as $result) {
			$brand_department_data[] = $result['department_id'];
		}

		return $brand_department_data;
	}
	
	public function getCategoryDetailsById($category_id) {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id = '" . (int)$category_id . "'");
		return $query->row;
	}
	
	public function getDepartmentDetailsById($department_id) {
		$company_id	= $this->session->data['company_id'];
      	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "department WHERE department_id = '" . (int)$department_id . "'");
		return $query->row;
	}
	public function getBrandByCode($brand_code,$brand_id='') {
		$sql = "SELECT count(brand_id) as tot  FROM " . DB_PREFIX . "brand WHERE brand_code = '" . $this->db->escape(trim($brand_code)). "'";
		if($brand_id){
			$sql.=" AND brand_id!= '" . (int)$brand_id . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows[0]['tot'];
	}
	public function getTableIdBy($code){
		$sql = "SELECT brand_id FROM ".DB_PREFIX."brand WHERE brand_code = '".trim($code)."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>