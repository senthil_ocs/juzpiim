<?php 
class ModelInventoryDiscount extends Model {

	public function addProductDiscount($data){
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "pair_discount_detail (pair_discount_code,sku,qty,price,createdby,createdon) VALUES ('".$data['discount_code']."','".$data['sku']."','".$data['discount_quantity']."','".$data['discount_price']."','".strtoupper($userName)."',curdate())");
	}

	public function editDiscountHeader($discount_code,$data){
		$userName	= $this->session->data['username'];
		$fromDate = changeDate($data['discount_fromdate']);
		$toDate   = changeDate($data['discount_todate']);
		$locations = implode(',', $data['location']);
        
		$sql="UPDATE " . DB_PREFIX . "pair_discount_header SET 
								  pair_discount_description = '".$data['discount_description']."',
								  pair_discount_from     = '".$fromDate."',
								  pair_discount_to       = '".$toDate."',
								  modifiedby 			 ='".strtoupper($userName)."',
								  modifiedon			 = curdate()
								  WHERE pair_discount_code = '".$discount_code."'";
		
		$query = $this->db->query($sql);
		$this->db->query("DELETE FROM " . DB_PREFIX . "discount_locations WHERE discount_code = '".$discount_code."'");
		for($l=0;$l<count($data['location']);$l++){
			if($data['location'][$l]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "discount_locations(discount_code,location_code) VALUES ('".$discount_code."','".$data['location'][$l]."')");
			}
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "pair_discount_detail WHERE pair_discount_code = '".$data['discount_code']."'");
		if(isset($discount_code)){
			foreach ($data['product_special'] as $product) {
				$query = $this->db->query("INSERT INTO " . DB_PREFIX . "pair_discount_detail (pair_discount_code,sku,qty,price,createdby,createdon,modifiedby,modifiedon) 
																VALUES ('".$data['discount_code']."','".$product['sku']."','".$data['discount_quantity']."',
																'".$data['discount_price']."','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate())");
			}
		}

	}

	public function addDiscount($data)
	{
		$fromDate = changeDate($data['discount_fromdate']);
		$toDate   = changeDate($data['discount_todate']);
		$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		$company_id = $this->session->data['company_id'];
		$locations = implode(',', $data['location']); 
		$mixeditem = isset($data['mixed']) ? $data['mixed'] : false;
		if($mixeditem){
			$mixeditem = 1;
		}
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "pair_discount_header (pair_discount_code,pair_discount_description,pair_discount_qty,pair_discount_price,pair_discount_from,pair_discount_to,createdby,createdon,modifiedby,modifiedon,mixeditem) VALUES ('".$data['discount_code']."','".$data['discount_description']."','".$data['discount_quantity']."','".$data['discount_price']."', '".$fromDate."','".$toDate."','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate(),'".$mixeditem."')");
		$discount_id = $this->db->getLastId();
		for($l=0;$l<count($data['location']);$l++){
			if($data['location'][$l]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "discount_locations(discount_code,location_code) VALUES ('".$data['discount_code']."','".$data['location'][$l]."')");
			}
		}
		
		if(isset($discount_id)){
			foreach ($data['product_special'] as $product) {
				$query = $this->db->query("INSERT INTO " . DB_PREFIX . "pair_discount_detail (pair_discount_code,sku,qty,price,createdby,createdon,modifiedby,modifiedon) 
																VALUES ('".$data['discount_code']."','".$product['sku']."','".$data['discount_quantity']."',
																'".$data['discount_price']."','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate())");
			}
		}
		
		return $discount_id;
	}
	public function editDiscount($discount_id,$data)
	{
		$company_id = $this->session->data['company_id'];
		$query = $this->db->query("UPDATE " . DB_PREFIX . "discount SET discount_type = '".$data['discount_type']."',
								  discount_name = '".$data['discount_name']."',
								  amount1       = '".$data['amount1']."',
								  amount2 		= '".$data['amount2']."' WHERE company_id = '".$company_id."' AND discount_id = '".$discount_id."'");
	}
	public function deleteDiscount($discount_id)
	{
		$company_id = $this->session->data['company_id'];
		$strLogDetails = array(
			'type' 		=> 'Discount',
			'id'   		=> $discount_id,
			'code'   	=> $discount_id,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "pair_discount_header WHERE pair_discount_code = '".$discount_id."'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "pair_discount_detail WHERE pair_discount_code = '".$discount_id."'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "discount_locations WHERE discount_code = '".$discount_id."'");
	}
	public function getTotalDiscounts($data)
	{
		$sort_data = array(
				'DH.pair_discount_code',
				'DH.pair_discount_description',				
			);	
		$sql = "SELECT DH.pair_discount_code,DH.pair_discount_price,DH.createdon,DH.pair_discount_description,DH.pair_discount_qty,DH.pair_discount_from,DH.pair_discount_to from tbl_pair_discount_header as DH LEFT JOIN tbl_discount_locations as DL on DL.discount_code = DH.pair_discount_code";
			
		if($data['filter_sku']!=''){
			
				$sql .= " LEFT JOIN tbl_pair_discount_detail as PDL on PDL.pair_discount_code = DH.pair_discount_code";
			}
		$sql .=" where DL.location_code='" . $data['filter_location'] . "'";   

			
			
			if($data['filter_sku']!=''){
			
				$sql .= " AND PDL.sku ='" . $data['filter_sku'] . "'";
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDate($data['filter_date_from']); 
				$data['filter_date_to']  = changeDate($data['filter_date_to']); 
				$sql .= " AND  DH.pair_discount_from >=  '" . $data['filter_date_from'] . "' AND DH.pair_discount_to <='" . $data['filter_date_to'] . "'";
			}
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY DH.pair_discount_description";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
		$query = $this->db->query($sql);
		return $query->num_rows;
	}
	public function checkCode($code){
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "pair_discount_header where pair_discount_code='".$code."'");
		return $query->row['total'];
	}
	public function getDiscounts($data=array())
	{
	
		$sort_data = array(
				'DH.pair_discount_code',
				'DH.pair_discount_description',				
			);	
		$sql = "SELECT DH.pair_discount_code,DH.pair_discount_price,DH.createdon,DH.pair_discount_description,DH.pair_discount_qty,DH.pair_discount_from,DH.pair_discount_to from tbl_pair_discount_header as DH LEFT JOIN tbl_discount_locations as DL on DL.discount_code = DH.pair_discount_code";
			
		if($data['filter_sku']!=''){
			
				$sql .= " LEFT JOIN tbl_pair_discount_detail as PDL on PDL.pair_discount_code = DH.pair_discount_code";
			}
		$sql .=" where DL.location_code='" . $data['filter_location'] . "'";   

			
			
			if($data['filter_sku']!=''){
			
				$sql .= " AND PDL.sku ='" . $data['filter_sku'] . "'";
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDate($data['filter_date_from']); 
				$data['filter_date_to']  = changeDate($data['filter_date_to']); 
				$sql .= " AND  DH.pair_discount_from >=  '" . $data['filter_date_from'] . "' AND DH.pair_discount_to <='" . $data['filter_date_to'] . "'";
			}
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY DH.pair_discount_description";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
			// echo $sql; die;
			$query = $this->db->query($sql);
			return $query->rows;
	}
	
	public function getDiscount($discount_id)
	{
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pair_discount_header WHERE pair_discount_code = '".$discount_id."'");
		return $query->row;
	}

	public function getDiscountProducts($discount_id)
	{
		$query = $this->db->query("SELECT d.pair_discount_code,d.sku,d.price,p.name FROM " . DB_PREFIX . "pair_discount_detail d
									LEFT JOIN 	" . DB_PREFIX . "product p ON (p.sku=d.sku) WHERE pair_discount_code = '".$discount_id."'");
		return $query->rows;
	}

	public function getDiscountTypes()
	{
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "discount_mode WHERE discount_status_id IS NOT NULL");
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "discount_mode WHERE discount_status_id IS NOT NULL");
		return $query->rows;
	}
	public function getTypeName($typeId)
	{
		$company_id	= $this->session->data['company_id'];	
		$query = $this->db->query("SELECT name FROM " . DB_PREFIX . "discount_mode WHERE discount_status_id = '".$typeId."' AND company_id = '".$company_id."'");
		return $query->row['name'];
	}
	public function getProductsforSearch($data = array(),$type="") {
		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT DISTINCT p.sku as psku,p.product_id,p.name FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)
				LEFT JOIN " . DB_PREFIX . "product_barcode pb ON (p.sku = pb.sku)";
				
		$sql .= " WHERE p.product_id !='0' ";
		
		if (!empty($data['filter_stock'])) {
			$sql .= " AND pd.sku_qty >=1";
		}
		
		if($type='touchmenu' && !is_numeric($data['filter_sku'])){
				if (!empty($data['filter_name'])) {
					$sql .= " AND ( p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'";
				}
				if (!empty($data['filter_sku'])) {
					$sql .= " OR  p.sku LIKE '%" . $this->db->escape(trim($data['filter_sku'])) . "%'";
				}
				if (!empty($data['filter_barcodes'])) {
					$sql .= " OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%')";
				}
		}else if(is_numeric($data['filter_sku'])){
				$sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "'";
				if (!empty($data['filter_barcodes'])) {
					$sql .= " OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%')";
				}
		}else{
			if (!empty($data['filter_name'])) {
				$sql .= " AND p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'";
			}
		}	
		
		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function deleteProducts($discountcode,$psku)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "pair_discount_detail WHERE pair_discount_code = '".$discountcode."' AND sku = '".$psku."'");
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getDiscountLocation($discount_code){
		$query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "discount_locations WHERE discount_code='".$discount_code."' ");
		return $query->rows;
	}

	
	public function deleteAllPairDiscount($promotionId)
	{
		$query1 = $this->db->query(" DELETE FROM " .DB_PREFIX."pair_discount_header WHERE pair_discount_code='".$promotionId."' ");
		$query2 = $this->db->query(" DELETE FROM " .DB_PREFIX."discount_locations WHERE discount_code='".$promotionId."' ");
		$query3 = $this->db->query(" DELETE FROM " .DB_PREFIX."pair_discount_detail WHERE pair_discount_code='".$promotionId."' ");
		if($query1){
			return true;
		}
	}
}
?>