<?php
class ModelInventorySearch extends Model {
	
	public function getProductDepartmentName($product_id) {
		$query = $this->db->query("SELECT d.department_name FROM " . DB_PREFIX . "department d
				LEFT JOIN " . DB_PREFIX . "product_to_department p2d ON (d.department_id = p2d.department_id)
				WHERE p2d.product_id = '" . (int)$product_id . "'");

		return $query->row['department_name'];
	}
	
	public function getProductCategoryName($product_id) {
		$query = $this->db->query("SELECT c.category_name FROM " . DB_PREFIX . "category c
				LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (c.category_id = p2c.category_id)
				WHERE p2c.product_id = '" . (int)$product_id . "'");

		return $query->row['category_name'];
	}
	
	public function getProductBrandName($product_id) {
		$product_brand_data = array();
		
		$query = $this->db->query("SELECT b.brand_name FROM " . DB_PREFIX . "brand b
				LEFT JOIN " . DB_PREFIX . "product_to_brand p2b ON (b.brand_id = p2b.brand_id)
				WHERE p2b.product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_brand_data[] = $result['brand_name'];
		}
		
		return $product_brand_data;
	}

	public function getCategoryByDepart($department_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE department_id = '".$department_id."'");

		return $query->rows;
	}

	public function getProductByBarcode($barcode)
	{
		$query = $this->db->query("SELECT sku FROM " . DB_PREFIX . "product_barcode WHERE barcode = '".$barcode."'");
		$sku = $query->row['sku'];

		$query1 = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE sku = '".$sku."'");
		return $query1->row['product_id'];

	}

	public function getProductsDetailsByBarcode($barcode,$location_code)
	{
		$sql = "SELECT p.sku,p.name,ps.sku_price FROM " . DB_PREFIX . "product_barcode as pb 
				LEFT JOIN " . DB_PREFIX . "product as p on p.sku = pb.sku
				LEFT JOIN " . DB_PREFIX . "product_stock as ps on ps.sku = p.sku
				WHERE pb.barcode = '".$barcode."' AND pb.sku_status='1' AND ps.location_code='".$location_code."'";
		$query = $this->db->query($sql);
		return $query->row;
		
	}

	public function updateqe($data)
	{
		$sku 	  	 = $data['sku'];
		$location 	 = $data['location'];
		$description = $data['description'];
		$price       = $data['price'];
		$old_price   = $data['old_price'];
		$created_by  = $data['created_by'];

		$sql1 = "update ". DB_PREFIX . "product set name='".$description."' where sku='".$sku."'";
		$this->db->query($sql1);

		$sql2 = "update ". DB_PREFIX . "product_stock set sku_price='".$price."' where sku='".$sku."' and location_Code='".$location."'";
		$this->db->query($sql2);				
		
		if($price != $old_price){
			$date =  date('Y-m-d H:i:s');
			$sql3 = "INSERT INTO ". DB_PREFIX ."pricechange_log (sku,location_code,last_price,new_price,created_by,created_date)
			VALUES('".$sku."','".$location."','".$old_price."','".$price."','".$created_by."','".$date."')";
		    $insert = $this->db->query($sql3);
		}
	}


	
}
?>