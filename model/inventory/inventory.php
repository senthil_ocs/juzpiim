<?php
class ModelInventoryInventory extends Model {

	public function addProduct($prductdata) {
	
	$this->load->model('inventory/networks');
	$data 	  = $prductdata->post;	
	$userName = $this->session->data['username'];
	 if(!isset($data['sku_uom_type'])){
		$data['sku_uom_type'] = '1*1';
	}
	if($data['open_price'] ==''){
		$data['open_price'] = '0';
	}
	 
	$nxtSKU_fromProductMax = $this->getProductMatchMax();
	$nxtSKU_fromSettings   = $this->getProductNextSKUfromSettings();
	$package = 0;
	if(count($data['child_pruducts']) > 0){
		$package = 1;
	}
	$data['isserviced'] = $data['isserviced'] =='' ? 0 : $data['isserviced'];
 	$sql = "INSERT INTO " . DB_PREFIX . "product (name,sku,sku_description,sku_shortdescription,sku_department_code,sku_category_code,sku_brand_code,sku_uom,sku_uom_type,sku_vendor_code,sku_image,sku_status,sku_gstallowed,createby,createon,modifyby,modifyon,quantity,price,open_price,subtract,parent_sku,sku_subcategory_code,package,serviced) VALUES('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['sku']) . "','" . $this->db->escape($data['description']) . "','" . $this->db->escape($data['short_description']) . "','" . $this->db->escape($data['product_department']) . "','" . $this->db->escape($data['product_category']) . "','" . $this->db->escape($data['product_brand']) . "','".$data['sku_uom']."','".$data['sku_uom_type']."','".$data['vendor']."','".$data['image']."','" . (int)$data['status'] . "','" . $this->db->escape($data['sku_gstallowed']) . "','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate(),'" . (int)$data['quantity'] . "','".$data['price']."','".$data['open_price']."','".$data['subtract']."','".$data['parent_sku']."','".$data['sub_category']."','".$package."','".$data['isserviced']."')";
		
		$res = $this->db->query($sql);

		$product_id = $this->db->getLastId();
		
		
		
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=inventory/inventory/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
			exit;
		}

		$company_id	= $this->session->data['company_id'];
		$userName	= $this->session->data['username'];
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_company (product_id,company_id,updated_by) VALUES ('" . (int)$product_id . "','" . (int)$company_id . "','".$this->session->data['username']."')");
			
		foreach ($_POST['location_code'] as $loc) {
			if($loc['sku_reorder']==''){
				$loc['sku_reorder'] = 0;
			}
			$loc['sku_min'] = $loc['sku_min']!='' ? $loc['sku_min'] : 0;
			if($data['sku']){
				$res = "INSERT INTO " . DB_PREFIX . "product_stock (location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder,createdby,createdon,modifiedby,modifiedon,product_id) VALUES ('".$loc['location_code']."','" . $this->db->escape($data['sku']) . "',0,0,0,".(float)$loc['sku_price'].",0,".$loc['sku_min'].",".$loc['sku_reorder'].",'".$this->session->data['username']."',curdate(),'".$this->session->data['username']."',curdate(),'".$product_id."')";
				
				// if($res=='error'){
				// 	    $this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE sku = '" . $data['sku'] . "'");
				// 		header('Location: '.HTTP_SERVER.'index.php?route=inventory/inventory/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['transaction_no']);						
				// 		exit;
				//  }
				// echo $res; die;
				$this->db->query($res);
			}
		}
		
		if(!empty($data['child_pruducts']) && $data['ispackage']=='1'){
			$this->updateChildProducts($product_id,$data['child_pruducts'],$data['child_products_qty']);
		}
		if($data['ispackage']==''){
			$this->deleteChildProducts($product_id);
		}
		
		if (isset($data['product_barcode']) && $product_id!='') {
			$p_sku      = $data['sku'];
			if($p_sku!=''){
				foreach ($data['product_barcode'] as $product_barcode) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_barcode (sku,barcode,sku_status,createby,createon,modifyby,modifyon) VALUES ('" . $p_sku . "','" . $this->db->escape($product_barcode['barcode'])  . "','1','".$this->session->data['username']."',
									curdate(),'".$this->session->data['username']."',curdate())");
				}
			}
		}

		if($data['sku']){
			$next_sku = $data['sku']+1;
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '".$next_sku."'  WHERE `key` = 'next_sku_no'");		
		}

		if(in_array($this->session->data['location_code'],API_LOCATION) && $product_id){
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "product  WHERE product_id = '" . $product_id . "'");
			$product_barcodes = $this->getProductBarcodesbySKU($query->row['sku']);
			if(count($product_barcodes)>=1){
				$barcode1 = $product_barcodes[0]['barcode'];
				$barcode2 = $product_barcodes[1]['barcode'];
				$barcode3 = $product_barcodes[2]['barcode'];
			}

			$product_data = array(
				"category_name" 	=> $this->getModelnameByCode($query->row['sku_department_code']),
				"sub_category_name" => $this->getCategoriesnameByCode($query->row['sku_category_code']),
				"product_name" 		=> $query->row['name'],
				"sku" 				=> $query->row['sku'],
				"product_id" 		=> $query->row['product_id'],
				"price" 			=> $query->row['price'],
				"quantity" 			=> $query->row['quantity'],
				"product_option" 	=> '',
				"barcode1" 			=> $barcode1,
				"barcode2" 			=> $barcode2,
				"barcode3" 			=> $barcode3
			);
			$res = curlpost('create_product',$product_data);			
		}

		if($prductdata->get['from'] == 'lazada_order_import'){
			$this->db->query("UPDATE ".DB_PREFIX."lazada_order_details SET SellerSKU='".$data['sku']."' where id='".$prductdata->get['lazada_id']."' ");
		}
		if($prductdata->get['from'] == 'qsm_order_import'){
			$this->db->query("UPDATE ".DB_PREFIX."qsm_order_details SET OptionCode='".$data['sku']."' where id='".$prductdata->get['qsm_id']."' ");
		}
	}
	public function getProductMatchMax(){
		$sql = "SELECT max(sku) + 1 as maxsku from ".DB_PREFIX."product";
		$query = $this->db->query($sql);
		return $query->row['maxsku'];
	}
	public function getProductNextSKUfromSettings(){
		$sql = "SELECT value from ".DB_PREFIX."setting where `key` = 'next_sku_no'";
		$query = $this->db->query($sql);
		return $query->row['value'];
	}
	public function getInventorySkuType($sku) {
		$company_id	= $this->session->data['company_id'];
		$likeIncrement	= $company_id."_"."200";
		$query = $this->db->query("SELECT sku FROM " . DB_PREFIX . "entity_increment WHERE increment_last_id LIKE '%".$likeIncrement."' AND company_id = '" . (int)$company_id . "' AND entity_type_id = '2'");
	}
	public function getProdResponse($sku,$network_id = ''){
		return $this->db->query("SELECT * FROM " . DB_PREFIX . "product_networks WHERE product_sku = '".$sku."' AND network_id = '".$network_id."'")->row;
	}
	public function editProduct($product_id, $productdata) {
		$userName = $this->session->data['username'];
		$this->load->model('inventory/networks');
		$data = $productdata->post;
		
		$date = date('Y-m-d H:i:s');
		if(!isset($data['sku_uom_type'])){
			$data['sku_uom_type'] = '1*1';
	 	}
		if($data['open_price']==''){
			$data['open_price'] = '0';
		}
		$package = 0;
		if(!empty($data['child_pruducts']) && $data['ispackage']=='1'){
			$package = $this->updateChildProducts($product_id,$data['child_pruducts'],$data['child_products_qty']);
		}
		if($data['ispackage']==''){
			$this->deleteChildProducts($product_id);
		}

		$this->db->query("UPDATE " . DB_PREFIX . "product SET
				  name 				= '" . $this->db->escape($data['name']) . "'
				, price 			= '" . $this->db->escape($data['price']) . "'
				, sku 				= '" . $data['sku'] . "'
				, sku_description 	  = '". $this->db->escape($data['description']) . "'
				, sku_shortdescription 	= '". $this->db->escape($data['short_description']). "'
				, sku_department_code 	= '". $this->db->escape($data['product_department']) . "'
				, sku_category_code 	= '". $this->db->escape($data['product_category']) . "'
				, sku_subcategory_code 	= '". $this->db->escape($data['sub_category']) . "'
				, sku_brand_code 		= '". $this->db->escape($data['product_brand']) . "'
				, sku_uom   			= '". $this->db->escape($data['sku_uom'])."'
				, sku_uom_type   		= '". $this->db->escape($data['sku_uom_type'])."'
				, sku_vendor_code 		= '". $this->db->escape($data['vendor'])."'
				, sku_status 			= '". (int)$data['status']."'
				, sku_gstallowed 		= '". $this->db->escape($data['sku_gstallowed'])."'
				, modifyby 				= '". strtoupper($userName)."'
				, quantity 				= '". (int)$data['quantity']."'
				, modifyon 				= curdate()
				, open_price 			= '". $data['open_price']."'
				, subtract 				= '". $data['subtract']."'
				, package 				= '". $package."'
				, serviced 				= '". $data['isserviced']."'
				, parent_sku 			= '". $data['parent_sku']."'	
			WHERE product_id 			= '". (int)$product_id."'");

			if($data['image'] != ''){
				$this->db->query("UPDATE " . DB_PREFIX . "product SET sku_image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
			}

			foreach ($_POST['location_code'] as $loc) {
				if(trim($loc['location_Code'])=='BB285'){
					$modifyPrice = $loc['sku_price'];
				}

			$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_price = ".(float)$loc['sku_price'].",sku_min = ".$this->db->escape($loc['sku_min']).",
			sku_reorder=".$this->db->escape($loc['sku_reorder']).", modifiedon = curdate(), modifiedby = '".$this->session->data['username']."',sku = '".$data['sku']."'
			WHERE product_id='".$product_id."' AND location_Code= '".$loc['location_Code']."' ");
			
			if($loc['sku_price'] != $_POST['old_price']){
				$date =  date('Y-m-d H:i:s');
				$insert = $this->db->query("INSERT INTO ". DB_PREFIX ."pricechange_log (sku,location_code,last_price,new_price,created_by,created_date)
				VALUES('".$_POST['sku']."','".$loc['location_Code']."','".$_POST['old_price']."','".$loc['sku_price']."','".$this->session->data['username']."','".$date."')");
			}
			}

			$p_sku      = $this->getProductSKu($product_id);
			$this->db->query("UPDATE " .DB_PREFIX."product_barcode SET sku_status='0',modifyby='".strtoupper($userName)."',modifyon=curdate() WHERE sku = '".$data['sku']."'"); 
			if (isset($data['product_barcode'])) {
				foreach ($data['product_barcode'] as $product_barcode) {	
					$barcode=$this->getbarcodeSku($data['sku'], $product_barcode['barcode']);
					if(count($barcode) !=0){
                       $this->db->query("UPDATE " . DB_PREFIX . "product_barcode SET sku ='" . $data['sku'] . "',barcode='" . $this->db->escape($product_barcode['barcode'])  . "',sku_status='1',modifyby='".strtoupper($userName)."',modifyon=curdate() WHERE sku = '".$data['sku']."' AND barcode='" . $product_barcode['barcode'] . "'");
					}else{
                      $this->db->query("INSERT INTO " . DB_PREFIX . "product_barcode (sku,barcode,sku_status,createby,createon,modifyby,modifyon) VALUES 
									('" . $data['sku'] . "','" . $this->db->escape($product_barcode['barcode'])  . "','1','".strtoupper($userName)."',
									curdate(),'".strtoupper($userName)."',curdate())");
					}				
				}
			}
	}
	public function getNetworks(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "networks WHERE status = 'Active'");
		return $query->rows;
	}
	public function getProductNetwork($product_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_networks WHERE product_id = '".$product_id."'");
		return $query->rows;
	}
	public function getbarcodeSku($sku,$barcode) {
		$company_id	= $this->session->data['company_id'];
		$sql ="SELECT * FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . $sku . "' AND barcode = '". $barcode ."'"; //exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function updateChildProducts($product_id,$childs,$quantity){
		if(is_array($childs)){
			foreach ($childs as $key => $value) {
				$qty[$value] = $quantity[$key];
			} 
		}
		$childs = array_unique($childs);
		$userId  = $this->session->data['user_id'];
		$res = '0';
		if(count($childs)>0){
			// parant_sku -> parant_product_id and child_sku -> child_product_id
			$this->db->query("DELETE FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ");
			foreach ($childs as $key => $value) {
			  	$this->db->query("INSERT INTO ".DB_PREFIX."child_products (parant_sku,child_sku,quantity,addedby,addedon) VALUES('".$product_id."','".$value."','".$qty[$value]."','".$userId."',curdate()) ");
			}
		$res = '1';
		}
		return $res;
	}
	public function deleteChildProducts($sku)
	{
		$this->db->query("DELETE FROM ".DB_PREFIX."child_products where parant_sku='".$sku."' ");
	}
	public function editProductSpecified($product_id, $productdata)
	{	
		$userName = $this->session->data['username'];
		$data = $productdata->post;
		$sku  = $this->getProductSKu($product_id);

		$this->db->query("UPDATE " . DB_PREFIX . "product_stock SET sku_price = '" . (float)$data['price'] . "',
			modifiedby = '".strtoupper($userName)."' WHERE sku = '" . $sku . "'");
		$this->db->query("UPDATE " . DB_PREFIX . "product SET sku_description = '" .$data['description'] . "' WHERE product_id = '" . $product_id . "'");
		
		
		
	}
	public function getProductSKuByBarCode($product_id){
		$sql = "SELECT sku FROM " . DB_PREFIX . "product_barcode where barcode='".$product_id."' AND sku_status='1'";	
		$query = $this->db->query($sql);
		return $query->row['sku'];
	}

	public function getProductSKu($product_id){
		$sql = "Select sku FROM " . DB_PREFIX . "product where product_id='".$product_id."'";	
		$query = $this->db->query($sql);
		return $query->row['sku'];
	}
	public function deleteProduct_BK($product_id) {
		$psku = $this->getProductSKu($product_id);
		$strLogDetails = array(
			'type' 		=> 'Inventory',
			'id'   		=> $product_id,
			'code'   	=> $psku,
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_stock WHERE sku = '" . (int)$psku . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_company WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . (int)$psku . "'");
		//$this->cache->delete('product');
	}
	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM ".DB_PREFIX."product_stock WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM ".DB_PREFIX."product WHERE product_id = '" . (int)$product_id . "'");
	}
	public function getProductsReport($data = array(),$type="") {
		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT p.product_id,p.sku,p.name,p.sku_brand_code,p.sku_status as status,p.sku as inventory_id,d.department_name,c.category_name,ps.sku_price,ps.sku_cost,b.brand_name,ps.sku_qty,ps.location_Code,p.sku_vendor_code,v.gst,v.tax_method,rs.reserved_qty,v.vendor_name as vendor_name,p.sku_subcategory_code FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "department as d ON d.department_code = p.sku_department_code
				LEFT JOIN " . DB_PREFIX . "category as c ON c.category_code = p.sku_category_code
				LEFT JOIN " . DB_PREFIX . "product_stock as ps ON ps.sku = p.sku
				LEFT JOIN " . DB_PREFIX . "brand as b ON b.brand_code = p.sku_brand_code
				LEFT JOIN " . DB_PREFIX . "vendor as v on p.sku_vendor_code = v.vendor_code
				LEFT JOIN " . DB_PREFIX . "product_reserved_stock as rs on ps.location_Code = rs.location_Code and ps.sku=rs.sku";

		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON p.sku = pb.sku";
		}
		$sql .= " WHERE p.product_id !='' AND p.sku_status ='1' ";
		
		if (!empty($data['filter_name']) && $data['filter_sku'] =='') {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}
		if (!empty($data['filter_sku'])) {
			$data['filter_sku'] = replaceQuotes($data['filter_sku']);
			 $sql .= " AND  p.sku ='".$this->db->escape(trim($data['filter_sku']))."'";
		}

		if (!empty($data['filter_location_code'])) {
			$sql .= " AND  ps.location_code ='" . $this->db->escape($data['filter_location_code']) . "'";
		}

		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND  pb.barcode ='" . $this->db->escape($data['filter_barcodes']) . "' AND pb.sku_status ='1'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND ps.sku_price >= '" . $this->db->escape($data['filter_price']) . "'";
		}

		if (isset($data['filter_priceto']) && !is_null($data['filter_priceto'])) {
			$sql .= " AND ps.sku_price <= '" . $this->db->escape($data['filter_priceto']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND DATE(createon) between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		
		$sort_data = array(
			'p.name',
			'ps.sku_price',
			'ps.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function removeImage($prod_Id) {
		$sql = $this->db->query("UPDATE " . DB_PREFIX . "product SET sku_image = '' WHERE product_id = '".$prod_Id."'");
		return $sql;
	}
	public function getTotalProductsReport($data = array(),$type="") {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total, sum(ps.sku_cost) AS total_price,  sum(ps.sku_price) AS total_selling_price FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "department as d ON d.department_code = p.sku_department_code
				LEFT JOIN " . DB_PREFIX . "category as c ON c.category_code = p.sku_category_code
				LEFT JOIN ". DB_PREFIX . "product_stock as ps ON ps.sku = p.sku";

		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON (p.sku = pb.sku)";
		}
		$sql .= " WHERE p.product_id !='' AND p.sku_status ='1' ";
		if (!empty($data['filter_name']) && $data['filter_sku'] =='') {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}

		if (!empty($data['filter_sku'])) {
			$data['filter_sku'] = replaceQuotes($data['filter_sku']);
			 $sql .= " AND  p.sku ='".$this->db->escape(trim($data['filter_sku']))."'";
		}

		if (!empty($data['filter_location_code'])) {
			$sql .= " AND  ps.location_code ='" . $this->db->escape($data['filter_location_code']) . "'";
		}

		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND  pb.barcode ='" . $this->db->escape($data['filter_barcodes']) . "' AND pb.sku_status ='1'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND ps.sku_price >= '" . $this->db->escape($data['filter_price']) . "'";
		}
		if (isset($data['filter_priceto']) && !is_null($data['filter_priceto'])) {
			$sql .= " AND ps.sku_price <= '" . $this->db->escape($data['filter_priceto']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND DATE(createon) between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getProduct($product_id,$location_code='') {
		$sql = "SELECT p.*,pd.sku_price as price,pd.sku_qty as quantity,pd.sku_avg as avg_cost,pd.sku_cost FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
		$sql .= " WHERE p.product_id = '" . (int)$product_id . "'";
		if($location_code!=''){
			$sql .= " AND pd.location_code = '" . (int)$location_code . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getProducts($data = array(),$type="") {
		$company_id	= $this->session->data['company_id'];
		$sqlOLD = "SELECT p.*,p.sku_status,p.sku as inventory_id,ps.location_code,ps.sku_price,ps.sku_cost,ps.sku_qty,d.department_name,c.category_name,b.brand_name, rs.reserved_qty   FROM " . DB_PREFIX . "product p 
				LEFT JOIN ". DB_PREFIX . "product_stock as ps ON ps.sku = p.sku
				LEFT JOIN " . DB_PREFIX . "department as d ON d.department_code = p.sku_department_code
				LEFT JOIN " . DB_PREFIX . "category as c ON c.category_code = p.sku_category_code
				LEFT JOIN " . DB_PREFIX . "brand as b ON b.brand_code = p.sku_brand_code
				LEFT JOIN tbl_product_reserved_stock as rs on ps.location_Code = rs.location_Code and ps.sku=rs.sku";

		$sql = "SELECT p.product_id,p.xero_id,p.name,p.sku_department_code,p.sku_category_code,
			p.sku_brand_code,p.sku_gstallowed,p.sku_status,p.sku as inventory_id,ps.location_code,ps.sku_price,ps.sku_cost,ps.sku_qty,d.department_name,c.category_name,b.brand_name, rs.reserved_qty   FROM " . DB_PREFIX . "product p 
			LEFT JOIN ". DB_PREFIX . "product_stock as ps ON ps.sku = p.sku
			LEFT JOIN " . DB_PREFIX . "department as d ON d.department_code = p.sku_department_code
			LEFT JOIN " . DB_PREFIX . "category as c ON c.category_code = p.sku_category_code
			LEFT JOIN " . DB_PREFIX . "brand as b ON b.brand_code = p.sku_brand_code
			LEFT JOIN tbl_product_reserved_stock as rs on ps.location_Code = rs.location_Code and ps.sku=rs.sku";

		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON p.sku = pb.sku";
		}
        
		$sql .= " WHERE p.product_id !='0'";
		if (!empty($data['location'])) {
			$sql .= " AND  ps.location_code ='" . $this->db->escape($data['location']) . "'";
		}
		if ($data['filter_name'] !='' && $data['filter_product_id'] =='') { 
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_shortdescription LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}

		if(!empty($data['filter_product_id'])){
			$sql .= " AND p.product_id = '".$this->db->escape($data['filter_product_id'])."'";
		}
		else if (!empty($data['filter_sku'])) {
			$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "'";
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND  pb.barcode ='" . $this->db->escape($data['filter_barcodes']) . "' AND pb.sku_status ='1'";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if (!empty($data['filter_sub_category'])) {
			$sql .= " AND p.sku_subcategory_code = '" . $this->db->escape($data['filter_sub_category']) . "'";
		}

		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND pd.sku_price >= '" . $this->db->escape($data['filter_price']) . "'";
		}
		if (isset($data['filter_status']) &&($data['filter_status'] =='1' || $data['filter_status']=='0') ) {
			$filter_status = $data['filter_status'] == '0' ? '0' : '1';
			$sql .= " AND p.sku_status= '" . $filter_status . "'";
		}
		else if($data['filter_status'] == 'is' || $data['filter_status']=='os'){
			if($data['filter_status'] == 'is'){
				$sql .= " AND ps.sku_qty >= 1";
			}else{
				$sql .= " AND ps.sku_qty <= 0";
			}
		}
		if (isset($data['filter_priceto']) && !is_null($data['filter_priceto'])) {
			$sql .= " AND pd.sku_price <= '" . $this->db->escape($data['filter_priceto']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND DATE(createon) between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}

		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		 //echo $sql; exit;
		return $this->db->query($sql)->rows;
	}

	public function getProductPriceAndName($ids){
		$sql = "SELECT p.*,pd.sku_price as price,pd.sku_qty as quantity FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_stock pd ON (p.sku = pd.sku) where p.product_id IN('".$ids."')";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getProductsforreport($data = array(),$type="",$idstring='') {

		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT *,p.sku_status as status,p.sku as inventory_id FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
		$sql .= " WHERE p.product_id !='0'  AND pd.location_Code = '".$data['location']."'";
		
		if (!empty($data['filter_name'])) {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}
		
		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code='".$this->db->escape($data['filter_department'])."'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code='".$this->db->escape($data['filter_category'])."'";
		}		

		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		$query = $this->db->query($sql);
		if($idstring=='1'){
			$res = $query->rows;
			$idsStr = '';
			for($i=0;$i<count($res);$i++){
				$idsStr.=$res[$i]['product_id'].',';
			}
			return $idsStr;
		}else{
			return $query->rows;
		}
	}

	public function getProductsforreportResult($idstring='') {

		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT p.name as name,p.sku_status as status,p.sku as inventory_id,pd.sku_price FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
		if($idstring){
			$sql.= " WHERE p.product_id IN(".$idstring.") ";
		}
		$query = $this->db->query($sql);
		return $query->rows;
		}

	public function getProductsTouchMenu($data = array(),$type="") {

		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT DISTINCT p.sku as psku,p.product_id,p.name,pd.sku_price FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
		if (!empty($data['filter_barcodes'])) {
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON p.sku = pb.sku";
		}

		$sql .= " WHERE p.product_id !='0' ";
		
		if (!empty($data['filter_stock'])) {
			$sql .= " AND pd.sku_qty >=1";
		}
		
		if($type='touchmenu' && !is_numeric($data['filter_sku'])){
				if (!empty($data['filter_name'])) {
					$sql .= " AND ( p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.sku_description LIKE '%" .trim($data['filter_name']) . "%'";
				}
				if (!empty($data['filter_sku'])) {
					$sql .= " OR  p.sku LIKE '%" . $this->db->escape(trim($data['filter_sku'])) . "%'";
				}
				if (!empty($data['filter_barcodes'])) {
					$sql .= " OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%') and pb.sku_status ='1'";
				}

		}else if(is_numeric($data['filter_sku'])){
				// $sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "'";
				if (!empty($data['filter_barcodes'])) {
				$sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "' OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%') and pb.sku_status ='1' ";
				// $sql .= " OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%') and pb.sku_status ='1'";
				}else{
					$sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "')";
				}
		}else{
			if (!empty($data['filter_name'])) {
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'  OR p.sku_description LIKE '%" . trim($data['filter_name']). "%')";
			}
		}
		
		
		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND pd.sku_price >= '" . $this->db->escape($data['filter_price']) . "'";
		}

		if (isset($data['filter_priceto']) && !is_null($data['filter_priceto'])) {
			$sql .= " AND pd.sku_price <= '" . $this->db->escape($data['filter_priceto']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND DATE(createon) between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}

		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getProductsTouchMenuNew($data = array(),$type="") 
	{	
		// $data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']);
		// $data['filter_sku'] = str_replace("&quot;",'"',$data['filter_sku']);
		$data['filter_name_convert'] = html_entity_decode($data['filter_name']);
		$data['filter_name_entity']  = htmlentities($data['filter_name']);
		$data['filter_sku_convert']  = html_entity_decode($data['filter_sku']);
		$data['filter_sku_entity']   = htmlentities($data['filter_sku']);

		$company_id	= $this->session->data['company_id'];		
		$sql = "SELECT DISTINCT p.sku as psku,p.product_id,p.name,pd.sku_price FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";

		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON p.sku = pb.sku";
		}
		$sql .= " WHERE p.product_id !='0' ";
		
		if (!empty($data['filter_stock'])) {
			$sql .= " AND pd.sku_qty >=1";
		}
		
		if($type='touchmenu' && !is_numeric($data['filter_sku'])){
				if (!empty($data['filter_name'])) {
					$sql .= " AND ( p.sku_shortdescription LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.name LIKE '%" .$this->db->escape(trim($data['filter_name']))."%' OR p.name LIKE '%" .$this->db->escape(trim($data['filter_name_convert']))."%' OR p.name LIKE '%" .$this->db->escape(trim($data['filter_name_entity']))."%'";
				}
				if (!empty($data['filter_sku'])) {
					$sql .= " OR p.sku LIKE '%" . $this->db->escape(trim($data['filter_sku'])) . "%' OR p.sku LIKE '%" . $this->db->escape(trim($data['filter_sku_convert'])) . "%' OR p.sku LIKE '%" . $this->db->escape(trim($data['filter_sku_entity'])) . "%')";
				}

		}else if(is_numeric($data['filter_sku'])){
				if (!empty($data['filter_barcodes'])) {
				$sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "' OR p.sku  ='" . $this->db->escape(trim($data['filter_sku_convert'])). "' OR p.sku  ='" . $this->db->escape(trim($data['filter_sku_entity'])). "' OR  pb.barcode LIKE '%" . $this->db->escape(trim($data['filter_barcodes'])) . "%') and pb.sku_status ='1' ";
				}else{
					$sql .= " AND ( p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "')";
				}
		}else{
			if (!empty($data['filter_name'])) {
				$sql .= " AND (p.sku_shortdescription LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%'  OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])). "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_entity'])). "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_convert'])). "%')";
			}
		}
		
		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		$sql.= " GROUP by p.sku";
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalProducts($data = array()) {
	
		$company_id	= $this->session->data['company_id'];
		$sql    = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p";
		$sql   .= " LEFT JOIN ".DB_PREFIX."product_stock ps ON (p.sku = ps.sku)";
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_barcode pb ON p.sku = pb.sku";
		}
		$sql .= " WHERE p.product_id !='0' AND ps.location_code='".$data['location']."'";
		
		if (!empty($data['filter_stock'])) {
			$sql .= " AND pd.sku_qty >=1";
		}
		if ($data['filter_name'] !='' && $data['filter_product_id'] =='' ) {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND (p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}
		if(!empty($data['filter_product_id'])){
			$sql .= " AND p.product_id = '".$this->db->escape($data['filter_product_id'])."'";
		}
		else if (!empty($data['filter_sku'])) {
			$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_sku'])). "'";
		}

		if(!empty($data['filter_product_id'])){
			$sql .= " AND p.product_id  ='" . $this->db->escape(trim($data['filter_product_id'])). "'";
		}
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND  pb.barcode ='" . $this->db->escape($data['filter_barcodes']) . "' and pb.sku_status ='1'";
		}
		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}
		if (!empty($data['filter_sub_category'])) {
			$sql .= " AND p.sku_subcategory_code = '" . $this->db->escape($data['filter_sub_category']) . "'";
		}
		if (isset($data['filter_status']) &&($data['filter_status'] =='1' || $data['filter_status']=='0') ) {
			$filter_status = $data['filter_status'] == '0' ? '0' : '1';
			$sql .= " AND p.sku_status= '" . $filter_status . "'";
		}
		else if($data['filter_status'] == 'is' || $data['filter_status']=='os'){
			if($data['filter_status'] == 'is'){
				$sql .= " AND ps.sku_qty >= 1";
			}else{
				$sql .= " AND ps.sku_qty <= 0";
			}
		}
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
		return $query->rows;
	}
	public function getProductDescriptions($product_id) {
		$product_description_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		foreach ($query->rows as $result) {
			$product_description_data[] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'short_description' => $result['short_description']
			);
		}
		return $product_description_data;
	}
	public function getInventoryAutoId() {
		$company_id	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT TOP 1 sku AS sku  FROM " . DB_PREFIX . "product ORDER BY sku DESC");
		$query = $this->db->query("SELECT max(SKU) AS sku from ".DB_PREFIX."product");
		return $query->row;
	}
	public function getPurchaseAutoId() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase ORDER BY purchase_id DESC LIMIT 1");
		return $query->row['purchase_id'];
	}
	public function getServicePurchaseAutoId() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "service_purchase_header ORDER BY purchase_id DESC LIMIT 1");
		return $query->row['purchase_id'];
	}
	public function getProductCategories($product_id) {
		$product_category_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}
		return $product_category_data;
	}
	public function getProductDepartments($product_id) {
		$product_department_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_department WHERE product_id = '" . (int)$product_id . "'");
		foreach ($query->rows as $result) {
			$product_department_data[] = $result['department_id'];
		}
		return $product_department_data;
	}
	public function getProductBrands($product_id) {
		$product_brand_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_brand WHERE product_id = '" . (int)$product_id . "'");
		foreach ($query->rows as $result) {
			$product_brand_data[] = $result['brand_id'];
		}
		return $product_brand_data;
	}
	public function getProductBarcodesbySKU($sku) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . $sku . "' AND sku_status='1'");
		return $query->rows;
	}
	public function getProductBarcodes($product_id) {
		$psku = $this->getProductSKu($product_id);
		$product_department_data = array();
		//echo "SELECT * FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . $psku . "' and sku_status='1'"; exit;
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_barcode WHERE sku = '" . $psku . "' and sku_status='1'");
		return $query->rows;
	}
	public function getInventoryDetailsById($product_id) {
		$company_id	= $this->session->data['company_id'];
		if(!empty($product_id)) {
			$query = $this->db->query("SELECT p.sku,p.name FROM " . DB_PREFIX . "product p
			WHERE p.product_id = '" . (int)$product_id . "'");
		}
		return $query->row;
	}
	public function getInventoryDetails($inventory_id,$product_id) {
		$company_id	= $this->session->data['company_id'];
		if(!empty($product_id)) {
			$query = $this->db->query("SELECT p.sku FROM " . DB_PREFIX . "product p
			WHERE p.sku = '" . $inventory_id . "' AND p.product_id != '" . (int)$product_id . "'");
		} else {
			$query = $this->db->query("SELECT p.sku FROM " . DB_PREFIX . "product p
							WHERE  p.sku = '" .$inventory_id . "'");
		}
		return $query->row;
	}
	public function getProductDetailsByBarcode($barcode) {
		
		$psku = $this->getProductSKu($product_id);
		$sql = "select p.product_id,p.sku,p.name from " . DB_PREFIX . "product as p LEFT JOIN " . DB_PREFIX . "product_barcode as pb on p.sku=pb.sku where pb.barcode='" . $barcode . "' and pb.sku_status='1'";
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getBarcodeDetails($barcode,$product_id) {
		$company_id	= $this->session->data['company_id'];
		$psku = $this->getProductSKu($product_id);

		if(!empty($product_id)) {
			$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode WHERE barcode = '" . $barcode . "' AND sku_status='1' AND sku != '" . $psku . "'");
		} else {
			$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_barcode WHERE barcode = '" . $barcode . "' AND sku_status='1'");
		}
		return $query->row;
	}
	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");
		return $query->row['total'];
	}
	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");
		return $query->row['total'];
	}
	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");
		return $query->row['total'];
	}
	public function getTotalProductsByDepartmentId($dept_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE sku_department_code = '" . trim($dept_id). "'");
		return $query->row['total'];
	}
	public function getTotalProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE sku_category_code = '" . trim($category_id). "'");
		return $query->row['total'];
	}
	public function getTotalProductsByBrandId($brand_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE sku_brand_code = '" . trim($brand_id) . "'");
		return $query->row['total'];
	}
	public function imageUploads($files) {
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$name = $files['image']['name'];
		$size = $files['image']['size'];
		if(strlen($name)){
			$ext = $this->getExtension($name);
			if(in_array($ext,$valid_formats)) {
				if($size<(1024*1024)) {
					$txt	='';
					$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $files['image']['tmp_name'];
					 //DIR_IMAGE."products/".$actual_image_name;
					move_uploaded_file($tmp, DIR_IMAGE.$actual_image_name);
						return $actual_image_name;
				}
			}
		}
	}
	public function getExtension($str) {
	 $i = strrpos($str,".");
	 if (!$i) { return ""; }
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
	}
	public function getProductChanges($product_id,$data=array()) {
		$sql = "SELECT * FROM product_changes WHERE Product_id = '" . (int)$product_id . "'";
		if($data['filter_fromdate']!='' && $data['filter_todate']!=''){
			$sql .= " AND DATE(dt_datetime) between  '" . $data['filter_fromdate'] . "' AND '" . $data['filter_todate'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getTotalProductChanges($product_id,$data=array()) {
		$sql = "SELECT COUNT(*) AS total FROM product_changes WHERE Product_id = '" . (int)$product_id . "'";
		if($data['filter_fromdate']!='' && $data['filter_todate']!=''){
			$sql .= " AND DATE(dt_datetime) between  '" . $data['filter_fromdate'] . "' AND '" . $data['filter_todate'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	public function getProductDescription($product_id,$data=array()) {
		$sql = "SELECT * FROM product_desc_changes WHERE Product_id = '" . (int)$product_id . "'";
		if($data['filter_fromdate']!='' && $data['filter_todate']!=''){
			$sql .= " AND DATE(dt_datetime) between  '" . $data['filter_fromdate'] . "' AND '" . $data['filter_todate'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getProductBarcode($product_id,$data=array()) {
		$sql= "SELECT * FROM product_barcode_changes WHERE Product_id = '" . (int)$product_id . "'";
		if($data['filter_fromdate']!='' && $data['filter_todate']!=''){
			$sql .= " AND DATE(dt_datetime) between  '" . $data['filter_fromdate'] . "' AND '" . $data['filter_todate'] . "'";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getTypeListbyUOMCode($code){
		$sql  = "select * from " . DB_PREFIX . "uom_type_master where uom_code = '" . $code . "'";
		$query = $this->db->query($sql);
		$res = $query->rows;
		if(count($res)>=1){
			return $res;
		}else{
			$res[0] = array("uom_code"=>$product_info['sku_uom'],"uom_type_value"=>"1*1");
			return $res;
		}
	}
	public function updateConfigSetting($value){
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '" . $value . "' WHERE store_id = '1' AND `key` = 'config_report_qty_negative'");
	}

	public function updateConfigSettings($value){
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '" . $value . "' WHERE store_id = '1' AND [key] = 'config_report_qty_negative_only'");
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '0' WHERE store_id = '1' AND [key] = 'config_report_qty_negative'");
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '0' WHERE store_id = '1' AND [key] = 'config_report_qty_zero_only'");

	}

	public function updateConfigSettings_zeroonly($value){
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '" . $value . "' WHERE store_id = '1' AND [key] = 'config_report_qty_zero_only'");
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '0' WHERE store_id = '1' AND [key] = 'config_report_qty_negative'");
		$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '0' WHERE store_id = '1' AND [key] = 'config_report_qty_negative_only'");
	}


	public function checkTranscationProductCount($product_id){
		$query = $this->db->query("SELECT COUNT(*) as CNT FROM " . DB_PREFIX . "purchase_to_product as PP LEFT JOIN " . DB_PREFIX . "purchase as P ON P.purchase_id = PP.purchase_id WHERE PP.product_id = '" . (int)$product_id . "'");
		return $query->row;
	}
	public function getSkuDetailsByProductId($product_id){
		$query = $this->db->query("SELECT sku FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		return $query->row;	
	}
	public function checkTranscationSalesProductCount($sku){
		$query = $this->db->query("SELECT COUNT(*) as CNT FROM " . DB_PREFIX . "sales_detail WHERE sku = '" . $sku . "'");
		return $query->row;	
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getStockReportListExport($data = array(),$type=""){
		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT *,p.sku_status as status,p.sku,pd.sku_avg as inventory_id FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_stock pd ON (p.sku = pd.sku)";
				
		$sql .= " WHERE p.product_id !='0' ";
		
		if (!empty($data['filter_stock'])) {
			$sql .= " AND pd.sku_qty >=1";
		}
		
		if (!empty($data['filter_name'])) {
			if(is_numeric($data['filter_name'])){
				$sql .= " AND p.sku  ='" . $this->db->escape(trim($data['filter_name'])). "'";
			}else{
				$sql .= " AND (p.sku_description LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%')";
			}	
		}
		
		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND p.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		}

		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND p.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}

		if (isset($data['filter_price']) && !is_null($data['filter_price'])) {
			$sql .= " AND pd.sku_price >= '" . $this->db->escape($data['filter_price']) . "'";
		}

		if (isset($data['filter_priceto']) && !is_null($data['filter_priceto'])) {
			$sql .= " AND pd.sku_price <= '" . $this->db->escape($data['filter_priceto']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND DATE(createon) between  '" . $data['filter_from_date'] . "' AND '" . $data['filter_to_date'] . "'";
		}

		$sort_data = array(
			'p.name',
			'pd.sku_price',
			'pd.sku_qty',
			'p.sku_status',
			'p.sort_order'
		);
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY p.product_id";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		
	$query = $this->db->query($sql);
	return $query->rows;
	}
	public function runSp_ProductStockSummaryByMonth($data = array()) {
		//exec Sp_ProductStockSummaryByMonth 'EP412','1-Aug-2018','30-Aug-2018'

		$sql = "exec Sp_ProductStockSummaryByMonth ";
		
		if (!empty($data['filter_location_code'])) {
			$sql .= "'" . $this->db->escape($data['filter_location_code']) . "',";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']); 
			$data['filter_date_to']  = changeDates($data['filter_date_to']); 
			$sql .= "'" . $data['filter_date_from']. "','" .$data['filter_date_to']."'";
		}
		// $query = $this->db->query($sql); 
	}

	public function getProductstockTotal($data = array()) {

		$sql = "SELECT count(*) as total FROM  tbl_product_stock_summary";
		$sql .= " WHERE Location_Code !=' ' ";
		if (!empty($data['filter_location_code'])) {
			$sql .= " AND Location_Code = '" . $this->db->escape($data['filter_location_code']) . "'";
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_to'] . "'";
		}
		if (!empty($data['filter_dept_code'])) {
			$sql .= " AND Dept_Code = '" . $this->db->escape($data['filter_dept_code']) . "'";
		}
		if (!empty($data['filter_sku'])) {
			$sql .= " AND Sku = '" . $this->db->escape($data['filter_sku']) . "'";
		}
		// printArray($sql);die;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	
	public function getProductstockTotalNew($data = array()) {

		$sql = "SELECT count(*) as total, sum(Opening_Stock) as total_Opening_Stock, sum(Purchase) as total_Purchase, sum(Purchase_Return) as total_Purchase_Return, sum(Sales) as total_Sales,  sum(Sales_Return) as total_Sales_Return, sum(Closing_Stock) as total_Closing_Stock, sum(Avg_Cost) as total_Avg_Cost, sum(Stock_Value) as total_Stock_Value FROM  tbl_product_stock_summary";

		// sum(transfer_qty) as total_transfer_qty

		$sql .= " WHERE Location_Code !=' ' ";
		if (!empty($data['filter_location_code'])) {
			$sql .= " AND Location_Code = '" . $this->db->escape($data['filter_location_code']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_to'] . "'";
		}
		if (!empty($data['filter_dept_code'])) {
			$sql .= " AND Dept_Code = '" . $this->db->escape($data['filter_dept_code']) . "'";
		}
		if (!empty($data['filter_sku'])) {
			$sql .= " AND Sku = '" . $this->db->escape($data['filter_sku']) . "'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}

	public function getProductstock($data = array()) {

		$sql = "SELECT * FROM  tbl_product_stock_summary WHERE Location_Code !='' ";
		
		if (!empty($data['filter_location_code'])) {
			$sql .= " AND Location_Code = '" . $this->db->escape($data['filter_location_code']) . "'";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		} else if($data['filter_date_from']!='' && $data['filter_date_to']==''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_from'] . "'";
		} else if($data['filter_date_from']=='' && $data['filter_date_to']!=''){
			$data['filter_date_to'] = changeDate($data['filter_date_to']); 
			$sql .= " AND Stock_Date =  '" . $data['filter_date_to'] . "'";
		}
		if (!empty($data['filter_dept_code'])) {
			$sql .= " AND Dept_Code = '" . $this->db->escape($data['filter_dept_code']) . "'";
		}
		if (!empty($data['filter_sku'])) {
			$sql .= " AND Sku = '" . $this->db->escape($data['filter_sku']) . "'";
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		}else {
			$sql .= " ORDER BY sku";
		}
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; exit;
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getLocation(){
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id != ''"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getDepartment(){
		$sql = "SELECT DISTINCT Dept_Code FROM " . DB_PREFIX . "product_stock_summary WHERE Dept_Code != ''"; 
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function addMissingProduct($data) {
	$userName	= $this->session->data['username'];
	 if(!isset($data['sku_uom_type'])){
			$data['sku_uom_type'] = '1*1';
	 }

	 if($data['open_price']==''){
			$data['open_price'] = '0';
	 }

	 $sql = "INSERT INTO " . DB_PREFIX . "product (name,price,sku,sku_description,sku_shortdescription,sku_department_code,sku_category_code,sku_brand_code,sku_uom,sku_uom_type,sku_vendor_code,sku_image,sku_status,sku_gstallowed,createby,modifyby,quantity,createon,modifyon,open_price,subtract) VALUES('" . $this->db->escape($data['name']) . "','".$data['price']."','" . $this->db->escape($data['sku']) . "','" . $this->db->escape($data['description']) . "','" . $this->db->escape($data['short_description']) . "','" . $this->db->escape($data['product_department']) . "','" . $this->db->escape($data['product_category']) . "','" . $this->db->escape($data['product_brand']) . "','".$data['sku_uom']."','".$data['sku_uom_type']."','".$data['vendor']."','".$data['image']."','" . (int)$data['status'] . "','" . $this->db->escape($data['sku_gstallowed']) . "','".strtoupper($userName)."','".strtoupper($userName)."','" . (int)$data['quantity'] . "',curdate(),curdate(),'".$data['open_price']."','".$data['subtract']."')";
		
	 	$this->db->query($sql);  
		$product_id = $this->db->getLastId();
		
		$company_id	= $this->session->data['company_id'];
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_company (product_id,company_id,updated_by) VALUES ('" . (int)$product_id . "','" . (int)$company_id . "','".strtoupper($userName)."')");
		
		$location_Code = LOCATION_CODE;
		
		if($product_id){
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_stock (location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder,createdby,createdon,modifiedby,modifiedon) 
			VALUES ('".$location_Code."','".$data['sku']."','".(float)$data['quantity']."','0.000','".(float)$data['unit_cost']."','".(float)$data['price']."',
					'".(float)$data['average_cost']."','0.000','0.000','".strtoupper($userName)."',curdate(),'".strtoupper($userName)."',curdate())");
		}
	
		if (isset($data['product_barcode']) && $product_id!='') {
			$p_sku      = $this->getProductSKu($product_id);
			if($p_sku!=''){
				foreach ($data['product_barcode'] as $product_barcode) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_barcode (sku,barcode,sku_status,createby,createon,modifyby,modifyon) VALUES 
									('" . $p_sku . "','" . $this->db->escape($product_barcode['barcode'])  . "','1','".strtoupper($userName)."',
									curdate(),'".strtoupper($userName)."',curdate())");
				}
			}
		}
		
		$inventoryType	= $this->config->get('config_inventory_increment');
		if (empty($inventoryType)) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "entity_increment WHERE increment_last_id = '" .$this->db->escape($data['sku']) . "'");
			$result	= $query->row;
			if(!empty($result)) {
				$increment_last_id	= $this->db->escape($data['sku']);
				$increment_last_id	=	(int)$increment_last_id + 1;
				$this->db->query("UPDATE " . DB_PREFIX . "entity_increment SET
				entity_type_id = '2' , increment_last_id  = '" . $increment_last_id . "'
				WHERE entityId = '" . (int)$result['entityId'] . "'");
			}
		}
		//$this->cache->delete('product');
	}
	public function getPurchaseLastId() {
		//echo "SELECT value  FROM " . DB_PREFIX . "setting WHERE [key] = 'next_sku_no'";exit;
		$query = $this->db->query("SELECT value  FROM " . DB_PREFIX . "setting WHERE [key] = 'next_sku_no'");
		return $query->row['value'];
	}

	public function getInventoryLastId() {
		//$query = $this->db->query("Select max(cast(SKU as numeric)) AS sku from tbl_product");
		$query = $this->db->query("SELECT max(SKU)+1 AS sku from ".DB_PREFIX."product");
		return $query->row['sku'];
	}

	public function getLocations(){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location ORDER BY location_Code ASC");
		return $query->rows;
	}
	public function getCompanyDetails($company_id) {
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");
		$query = $this->db->query("SELECT c.location_code, d.location_name FROM " . DB_PREFIX . "company As c LEFT JOIN " .DB_PREFIX . "location As d ON c.location_code = d.location_code WHERE company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	public function getLocationss($loc){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code= '".$loc."' ORDER BY location_Code ASC");
		return $query->rows;
	}
	public function getLocationnhq(){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code='HQ' ORDER BY location_Code ASC");
		return $query->rows;
	}
	public function getLocationhq(){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location WHERE location_code!='SK308' ORDER BY location_Code ASC");
		return $query->rows;
	}
	public function getStockdetails($sku,$loc) {
		if($loc == 'HQ'){
			/*$sql= $this->db->query("SELECT * FROM " .DB_PREFIX . "location");
			printArray($sql->rows); exit;*/
			$query = $this->db->query("SELECT location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder FROM " . DB_PREFIX . "product_stock WHERE sku = '" . $sku . "' AND location_Code IN (SELECT location_code FROM " .DB_PREFIX . "location)  GROUP BY location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder");
		} else{
		$query = $this->db->query("SELECT location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder FROM " . DB_PREFIX . "product_stock WHERE sku = '" . $sku . "' AND location_Code IN ('".$loc."')  GROUP BY location_Code,sku,sku_qty,sku_salesQty,sku_cost,sku_price,sku_avg,sku_min,sku_reorder");
		}  //SELECT location_code FROM " .DB_PREFIX . "location (or)  'HQ','BB283','BB285','EP412','JE217','SK308'
		return $query->rows;
	}
	public function getProductsNew($data = array(),$type='') {
	
		$sql = "SELECT sh.*,p.sku_shortdescription as sku_description,d.department_name,sh.reserved_qty,p.name,p.product_id from vw_product_stock_history as sh 
			LEFT JOIN tbl_product as p on p.sku = sh.sku
			LEFT JOIN tbl_department as d on d.department_code = p.sku_department_code";
		$sql .= " WHERE sh.sku !='' AND p.sku_status='1' ";

		if (!empty($data['filter_stock_date'])) {
	 	 	$filter_date = changeDate($data['filter_stock_date']);
			$sql .= " AND sh.stock_date = '" .$filter_date. "'";
		} 

		if (!empty($data['filter_location_code'])) {
			$sql .= " AND sh.location_Code = '" . $this->db->escape($data['filter_location_code']) . "'";
		}
	 	 if (!empty($data['filter_name'])) {
				$data['filter_name_Convert'] = html_entity_decode($data['filter_name']);
				$data['filter_name_entity'] = htmlentities($data['filter_name']);

				$sql .= " AND (p.sku  ='" . $this->db->escape(trim($data['filter_name']))."' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_Convert'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_entity'])) . "%' )";
		}
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}	
		if(empty($data['filter_stock'])){
			//$sql .= " AND sh.sku_qty > 0";
		}else if($data['filter_stock']=='1'){
			$sql .= " AND sh.sku_qty > 0";
		}else if($data['filter_stock']=='2'){
			$sql .= " AND sh.sku_qty < 0";
		}else if($data['filter_stock']=='3'){
			$sql .= " AND sh.sku_qty = 0";
		}

		if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
		}

		$sql .= " ORDER BY p.sku_description";
		$sql .= " ASC";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalProductsNew($data = array()) {
	
		$sql = "SELECT count(sh.sku) as total,sum(sh.sku_avg) as total_avg,sum(sh.sku_qty * sh.sku_avg) as total_price,sum(sh.sku_qty) as total_qty from vw_product_stock_history as sh 
			LEFT JOIN tbl_product as p on p.sku = sh.sku
			LEFT JOIN tbl_department as d on d.department_code = p.sku_department_code";
		
		$sql .= " WHERE sh.sku !='' AND p.sku_status='1' ";

		if (!empty($data['filter_stock_date'])) {
	 	 	$filter_date = changeDate($data['filter_stock_date']);
			$sql .= " AND sh.stock_date = '" . $this->db->escape($filter_date) . "'";
		 }	 

		 if (!empty($data['filter_location_code'])) {
			  $sql .= " AND sh.location_Code = '" . $this->db->escape($data['filter_location_code']) . "'";
	 	 }
	 	 if (!empty($data['filter_name'])) {
				$data['filter_name_Convert'] = html_entity_decode($data['filter_name']);
				$data['filter_name_entity'] = htmlentities($data['filter_name']);

				$sql .= " AND (p.sku  ='" . $this->db->escape(trim($data['filter_name']))."' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_Convert'])) . "%' OR p.name LIKE '%" . $this->db->escape(trim($data['filter_name_entity'])) . "%' )";
		}
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}

	 	 if(empty($data['filter_stock'])){
	 	 	//$sql .= " AND sh.sku_qty>0";
	 	 }else if($data['filter_stock']=='1'){
	 	 	$sql .= " AND sh.sku_qty > 0";
	 	 }else if($data['filter_stock']=='2'){
	 	 	$sql .= " AND sh.sku_qty < 0";
	 	 }else if($data['filter_stock']=='3'){
	 	 	$sql .= "AND sh.sku_qty = 0";
	 	 }
	 	
	 	if (!empty($data['filter_department'])) {
			$sql .= " AND p.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
	 	}
		// echo $sql;exit();
		$query = $this->db->query($sql);
		return $query->row;
	}

	function getModelnameByCode($code){
		$sql = "select department_name from tbl_department where department_code='".$code."'";
		$query = $this->db->query($sql);
		return $query->row['department_name'];		
	}
	function getCategoriesnameByCode($code){
		$sql = "select category_name from tbl_category where category_code='".$code."'";
		$query = $this->db->query($sql);
		return $query->row['category_name'];		
	}	
	public function runSp_inventoryMovement($data = array()) {
		//exec sp_product_movement 'HQ','1777','01-Jan-2019','21-Jun-2019'

		$sql = "exec sp_product_movement ";
		
		if (!empty($data['filter_location'])) {
			$sql .= "'" . $this->db->escape($data['filter_location']) . "',";
		}
		if (!empty($data['filter_sku'])) {
			$sql .= "'" . $this->db->escape($data['filter_sku']) . "',";
		}

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDate($data['filter_date_from']); 
			$fromDate =  date('d-M-Y',strtotime($data['filter_date_from']));
			$data['filter_date_to']  = changeDate($data['filter_date_to']); 
			$toDate =  date('d-M-Y',strtotime($data['filter_date_to']));
			$sql .= "'" . $fromDate. "','" .$toDate."'";
		}

		// echo $sql;exit;
		  //exec Sp_ProductStockSummaryByMonth 'HQ','01-Jan-2019','07-Jan-2019'
		$query = $this->db->query($sql); 		
	}

	public function getInventoryMovementData($data = array()) {

		$sql = "SELECT * from  vw_inventory_movement ";
		if (!empty($data['filter_location'])) {
			$sql .= " where location_code='".$data['filter_location']."' ";
		}
		$data['filter_name_convert'] = html_entity_decode($data['filter_sku']);
		$data['filter_name_entity'] = htmlentities($data['filter_sku']);
		
		if (!empty($data['filter_sku'])) {
			$sql .= " and (sku='".$this->db->escape(trim($data['filter_sku']))."' OR sku='".$this->db->escape(trim($data['filter_name_convert']))."' OR sku='".$this->db->escape(trim($data['filter_name_entity']))."') ";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from'])));
			$data['filter_to_date']   = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to'])));

			$sql .= " AND created_at between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_to_date'] . "'";
		}
		$sql .= " ORDER BY created_at ASC ";
		//echo $sql; die;
		$query = $this->db->query($sql);
		return $query->rows; 		
	}

	public function getInventoryMovementDataOpeningBalance($data = array()) {

		$sql = "SELECT SUM(quantity) as total,operation from  vw_inventory_movement ";
		if (!empty($data['filter_location'])) {
			$sql .= " where location_code='".$data['filter_location']."' ";
		}
		$data['filter_name_Convert'] = html_entity_decode($data['filter_sku']);
		$data['filter_name_entity'] = htmlentities($data['filter_sku']);
		
		if (!empty($data['filter_sku'])) {
			$sql .= " and (sku='".$this->db->escape(trim($data['filter_sku']))."' OR sku='".$this->db->escape(trim($data['filter_name_Convert']))."' OR sku='".$this->db->escape(trim($data['filter_name_entity']))."') ";
		}
		if($data['filter_date_from']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from'])));
			$sql .= " AND created_at < '".$data['filter_date_from']."'";
		}
		$sql .= " GROUP By operation ORDER BY operation ASC ";
		$query = $this->db->query($sql);
		return $query->rows; 		
	}



	public function getpromationSKU($promcode) {
		$sql ="select sku from " . DB_PREFIX . "promotions_sku WHERE PromotionCode ='".$promcode."'";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getReservedStockdetails($locationcode,$sku) {
		$sql ="select * from " . DB_PREFIX . "product_reserved_stock WHERE location_Code ='".$locationcode."' AND sku ='".$sku."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	/*public function getReservedStockhistory($locationcode,$sku) {
		$sql ="select * from vw_product_stock_history WHERE location_Code ='".$locationcode."' AND sku ='".$sku."'";
		$query = $this->db->query($sql);
		return $query->row;
	}*/
	public function getProductByNamenew($data = array()) 
	{
		$company_id	= $this->session->data['company_id'];
		$location   = $this->session->data['location_code'];
		$sql 		= "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,ps.location_code FROM ".DB_PREFIX."product p LEFT JOIN ".DB_PREFIX."product_stock ps ON (p.sku = ps.sku) WHERE p.product_id !='0' ";

		if(!empty($data['filter_name'])) {
			$data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']);

			$sql .= " AND ( p.sku_shortdescription LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%') ";
		}
		$sql .=" GROUP BY p.product_id";
		// echo $sql; die;
		return $this->db->query($sql)->rows;
	}
	public function getProductByName($data = array()) {
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)";
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";
		if($data['location_code']){              //newly added
			$sql.=" AND ps.location_Code = '".$data['location_code']."'";
		}

		// /$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		/*if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			$sql .= ") and p2b.sku_status= '1'";
		}*/
		if(!empty($data['filter_name'])) {
			$data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']);

			$sql .= " AND ( p.sku_shortdescription LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' OR p.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%') ";
		}
		$sql .= " GROUP BY p.product_id";
		// echo $sql; die;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	public function getChildProducts($product_id){
		$sql = "SELECT p.product_id as sku, p.name, c.quantity,p.sku as product_sku FROM ".DB_PREFIX."child_products as c left join ".DB_PREFIX."product as p ON c.child_sku=p.product_id where c.parant_sku='".$product_id."' ";
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getProductByNames($data = array()) {
		$data['filter_name'] = str_replace("&quot;",'"',$data['filter_name']);

		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT p.product_id,p.sku,p.name,p.sku_uom,p.sku_department_code,ps.sku_qty,ps.sku_avg as average_cost ,ps.sku_price as price,ps.sku_cost as unit_cost,prs.reserved_qty FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_stock ps ON (p.sku = ps.sku)
				LEFT JOIN tbl_product_reserved_stock prs ON (prs.sku = ps.sku AND prs.location_Code=ps.location_Code)";
		if($data['filter_name']){
		   $sql.= " LEFT JOIN " . DB_PREFIX . "product_barcode p2b ON (p.sku = p2b.sku)";	
		}
		$sql.= " WHERE p.sku_status= '1'";
		if($data['location_code']){              //newly added
			$sql.=" AND ps.location_Code = '".$data['location_code']."'";
		}
		if($data['selected_productsStr']){
			$data['selected_productsStr'] = str_replace(',', "','", $data['selected_productsStr']);
			$sql.=" AND p.sku NOT IN ('".$data['selected_productsStr']."') ";
		}
		// /$sql.= "LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";
		if(!empty($data['filter_name'])) {
			$sql .= " AND ( p2b.barcode LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR ";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "p.name LIKE '%" . $this->db->escape($word) . "%'";
			}
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			//$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
			$sql .= " OR LOWER(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			$sql .= ")";
		}
		//$sql .= " GROUP BY p.product_id";
		// echo $sql; die;
		$query        = $this->db->query($sql);
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}

	public function removeMultipleImage($image_id) {
		$sql = $this->db->query("DELETE FROM " . DB_PREFIX . "product_images WHERE id = '".$image_id."' ");
		return $sql;
	}


	public function networkUpdate($data){

		$this->load->model('inventory/networks');
		if($data['network'] == 'Shopify'){
			$value = 1;
		} else if($data['network'] == 'qoo10'){
			$value = 2;
		} else if($data['network'] == 'Shopee'){
			$value = 3;
		} else if($data['network'] == 'Lazada'){
			$value = 4;
		}else if($data['network'] == 'Lazmall'){
			$value = 5;
		}

		$prod_network = $this->getProdResponse($data['sku'],$value);

		if($value=='1'){
			$response_id = $this->model_inventory_networks->addProductShopify($data,$prod_network['network_response_id']);
			if(empty($prod_network)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_networks(network_id, product_id, product_sku, price,sku_cost, quantity, network_response_id, status) VALUES ('".$value."','".$data['prod_id']."','".$data['sku']."','".$data['price']."','".$data['sku_cost']."','".$data['qty']."','".$response_id."','Active')");
			} else{
				$this->db->query("UPDATE " . DB_PREFIX . "product_networks SET price = '".$data['price']."',quantity = '".$data['qty']."', sku_cost='".$data['sku_cost']."' WHERE product_sku = '".$data['sku']."' AND network_id='".$value."' ");
			}
		}
		if($value=='2'){
			$response_id = $this->model_inventory_networks->addProductQSM($data,$prod_network['network_response_id']);
			if(empty($prod_network)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_networks(network_id, product_id, product_sku, price,sku_cost, quantity, network_response_id, status) VALUES ('".$value."','".$data['prod_id']."','".$data['sku']."','".$data['price']."','".$data['sku_cost']."','".$data['qty']."','".$response_id."','Active')");
			} else{
				$this->db->query("UPDATE " . DB_PREFIX . "product_networks SET price = '".$data['price']."',quantity = '".$data['qty']."', sku_cost='".$data['sku_cost']."' WHERE product_sku = '".$data['sku']."' AND network_id='".$value."' ");
			}
		}

		if($value=='3'){
			$response_id = $this->model_inventory_networks->addProductShopee($data,$data['network_response']);

			if($data['network_response'] == '') {
				if(isset($response_id->item_id)){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_networks(network_id, product_id, product_sku, price,sku_cost, quantity, network_response_id, status) VALUES ('".$value."','".$data['prod_id']."','".$data['sku']."','".$data['price']."','".$data['sku_cost']."','".$data['qty']."','".$response_id->item_id."','Active')");
				}
				else if($response_id->error !=''){
					$error =  $response_id->msg;
				}
			} else if($data['network_response'] != ''){
				$this->db->query("UPDATE " . DB_PREFIX . "product_networks SET price = '".$data['price']."',quantity = '".$data['qty']."', sku_cost='".$data['sku_cost']."' WHERE network_response_id = '" . $data['network_response'] . "'");	
			}
		} 
		if($value=='4'){
			$response_id = $this->model_inventory_networks->addProductLazada($data,$data['network_response']);
			if($data['network_response'] == '') {
				if(isset($response_id->item_id)){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_networks(network_id, product_id, product_sku, price,sku_cost, quantity, network_response_id, status) VALUES ('".$value."','".$data['prod_id']."','".$data['sku']."','".$data['price']."','".$data['sku_cost']."','".$data['qty']."','".$response_id->item_id."','Active')");
				}
			} else if($data['network_response'] != ''){
				$this->db->query("UPDATE " . DB_PREFIX . "product_networks SET price = '".$data['price']."',quantity = '".$data['qty']."', sku_cost='".$data['sku_cost']."' WHERE network_response_id = '" . $data['network_response'] . "'");	
			}
		}
		if($value=='5'){
			$response_id = $this->model_inventory_networks->addProductLazmall($data,$data['network_response']);
			if($data['network_response'] == '') {
				if(isset($response_id->item_id)){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_networks(network_id, product_id, product_sku, price,sku_cost, quantity, network_response_id, status) VALUES ('".$value."','".$data['prod_id']."','".$data['sku']."','".$data['price']."','".$data['sku_cost']."','".$data['qty']."','".$response_id->item_id."','Active')");
				}
			} else if($data['network_response'] != ''){
				$this->db->query("UPDATE " . DB_PREFIX . "product_networks SET price = '".$data['price']."',quantity = '".$data['qty']."', sku_cost='".$data['sku_cost']."' WHERE network_response_id = '" . $data['network_response'] . "'");	
			}
		}

		if($error !=''){
			return array('status' => false, 'message' => $error);
		}else{
			return array('status' => true, 'message' => $response_id);
		}
	}

	public function getMultipleImages($product_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_images WHERE product_id = '".$product_id."'");
		return $query->rows;
	}

	public function updateMultipleImage($product)
    {
		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_images SET title = '".$product['title']."', sort_order = '".$product['priority']."'  WHERE id = '".$product['id']."'");
		return $query;
	}
	public function addMultipleImage($product)
    {
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "product_images (product_id, sku, title, image, sort_order) VALUES ('".$product['product_id']."','".$product['sku']."','".$product['title']."','".$product['fileName']."','".$product['priority']."')");
		return $query;
	}

	public function getMinimumQuantityReportCount($data)
    {  
		$sql = "SELECT pt.product_id FROM ".DB_PREFIX."product_stock as ps 
            LEFT OUTER JOIN ".DB_PREFIX."product as pt ON ps.product_id=pt.product_id and pt.sku_status ='1'  and ps.sku_qty <= ps.sku_min
            LEFT OUTER JOIN ".DB_PREFIX."department as dp ON pt.sku_department_code=dp.department_code 
            LEFT OUTER JOIN tbl_sales_detail as SDD on pt.sku = SDD.sku  
            LEFT OUTER JOIN tbl_sales_header as SDH on SDH.invoice_no = SDD.invoice_no and SDH.delivery_status NOT IN('Canceled','Delivered')
            where pt.product_id !='' ";
        
        if($data['filter_location']){
            $sql .=" AND ps.location_Code='".$data['filter_location']."' ";
        }
        if($data['filter_department']){
            $sql .=" AND pt.sku_department_code='".$data['filter_department']."' ";
        }
        if($data['filter_product_id']){
            $sql .=" AND ps.product_id='".$data['filter_product_id']."' ";
        }
        if (!empty($data['filter_category'])) {
            $sql .= " AND pt.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
        } 
        if (!empty($data['filter_vendor'])) {
            $sql .= " AND pt.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
        }
        if (!empty($data['filter_brand'])) {
            $sql .= " AND pt.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
        }
        $sql .=" group by pt.product_id ORDER BY pt.product_id ASC ";

		return $this->db->query($sql)->num_rows;
	}

	public function getMinimumQuantityReports($data){

		$sql = "SELECT pt.product_id,ps.location_Code,pt.name,pt.sku_shortdescription,pt.sku_department_code, ps.sku_qty,ps.sku_min,pt.sku,dp.department_name,SUM(SDD.qty) as RESERVED_QTY 
            FROM ".DB_PREFIX."product_stock as ps 
            LEFT OUTER JOIN ".DB_PREFIX."product as pt ON ps.product_id=pt.product_id and pt.sku_status ='1'  and ps.sku_qty <= ps.sku_min
            LEFT OUTER JOIN ".DB_PREFIX."department as dp ON pt.sku_department_code=dp.department_code 
			LEFT OUTER JOIN tbl_sales_detail as SDD on pt.sku = SDD.sku  
			LEFT OUTER JOIN tbl_sales_header as SDH on SDH.invoice_no = SDD.invoice_no where pt.product_id !='' and SDH.delivery_status = 'Pending' ";
		
		if($data['filter_location']){
			$sql .=" AND ps.location_Code='".$data['filter_location']."' ";
		}
		if($data['filter_department']){
			$sql .=" AND pt.sku_department_code='".$data['filter_department']."' ";
		}
		if($data['filter_product_id']){
			$sql .=" AND ps.product_id='".$data['filter_product_id']."' ";
		}
		if (!empty($data['filter_category'])) {
			$sql .= " AND pt.sku_category_code = '" . $this->db->escape($data['filter_category']) . "'";
		} 
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND pt.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		if (!empty($data['filter_brand'])) {
			$sql .= " AND pt.sku_brand_code = '" . $this->db->escape($data['filter_brand']) . "'";
		}
		$sql .=" group by pt.product_id ORDER BY pt.product_id ASC ";
		
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		 // echo $sql; 
		return $this->db->query($sql)->rows;
	}

	public function getstock_valuation_with_reserved_stock_ReportCount($data){

		$sql= "SELECT S.sku,MAX(S.sku_qty) as sku_qty,MAX(S.sku_avg) as sku_price,SUM(SDD.qty) as qty 
				from tbl_product_stock as S
				INNER JOIN tbl_product as P on P.sku = S.sku
				LEFT OUTER JOIN tbl_sales_detail as SDD on S.sku =  SDD.sku
				LEFT OUTER JOIN tbl_sales_header as SDH on SDH.invoice_no = SDD.invoice_no WHERE SDH.delivery_status IN('Pending')";		
	 	if (!empty($data['filter_department'])) {
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
	 	}
	 	if (!empty($data['filter_location'])) {
			$sql .= " AND SDH.location_code = '" . $this->db->escape($data['filter_location']) . "'";
	 	}
	 	if($data['filter_product_id']){
			$sql .= " AND P.product_id = '".$this->db->escape($data['filter_product_id'])."' ";
	 	}
	 	if($data['filter_vendor']){
			$sql .= " AND P.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
	 	}
	 	$sql .=" GROUP BY S.sku";
		$query = $this->db->query($sql);	
		return count($query->rows);
	}

	public function getStockCount($data){

		$sql= "SELECT count(P.sku) as total from tbl_product as P
				LEFT JOIN tbl_product_stock as S on P.sku = S.sku
				where P.sku!='' AND P.package!='1' AND P.sku_status='1' ";
	 	if (!empty($data['filter_department'])) {
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
	 	}
	 	if (!empty($data['filter_location'])) {
			$sql .= " AND S.location_code = '" . $this->db->escape($data['filter_location']) . "'";
	 	}
	 	if($data['filter_product_id']){
			$sql .= " AND P.product_id = '".$this->db->escape($data['filter_product_id'])."' ";
	 	}
	 	if($data['filter_vendor']){
			$sql .= " AND P.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
	 	}
	 	if(!empty($data['filter_name'])) {
			$data['filter_name_Convert'] = html_entity_decode($data['filter_name']);
			$data['filter_name_entity'] = htmlentities($data['filter_name']);

			$sql .= " AND (P.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' 
							OR P.name LIKE '%".$this->db->escape(trim($data['filter_name_Convert']))."%'
							OR P.name LIKE '%".trim($data['filter_name_entity'])."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name_Convert']))."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%'
							) ";
		}
		//echo $sql;exit;
	 	$res = $this->db->query($sql);	
		return $res->row['total'];
	}
	public function getStockProduts($data){

		$sql= "SELECT S.sku_qty,P.* from tbl_product as P
				LEFT JOIN tbl_product_stock as S on P.sku = S.sku
				where P.sku!='' AND P.package !='1' AND P.sku_status='1' ";		
	 	if (!empty($data['filter_department'])) {
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
	 	}
	 	if (!empty($data['filter_location'])) {
			$sql .= " AND S.location_code = '" . $this->db->escape($data['filter_location']) . "'";
	 	}
	 	if($data['filter_product_id']){
			$sql .= " AND P.product_id = '".$this->db->escape($data['filter_product_id'])."' ";
	 	}
	 	if($data['filter_vendor']){
			$sql .= " AND P.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
	 	}
	 	if(!empty($data['filter_name'])) {
			$data['filter_name_Convert'] = html_entity_decode($data['filter_name']);
			$data['filter_name_entity'] = htmlentities($data['filter_name']);

			$sql .= " AND (P.name LIKE '%".$this->db->escape(trim($data['filter_name']))."%' 
							OR P.name LIKE '%".$this->db->escape(trim($data['filter_name_Convert']))."%'
							OR P.name LIKE '%".trim($data['filter_name_entity'])."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name']))."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name_Convert']))."%'
							OR P.sku LIKE '%".$this->db->escape(trim($data['filter_name_entity']))."%'
							) ";
		}
		$sql .=  " order by S.sku_qty DESC";
	 	if($export == ''){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		// echo $sql; 
	 	$query = $this->db->query($sql);
		return $query->rows;
	 	
	}

	public function getgetstock_valuation_with_reserved_stock_Reports($data,$export=''){
		$sql= "SELECT S.sku,P.name as name,P.product_id as product_id,MAX(S.sku_qty) as sku_qty,MAX(S.sku_avg) as sku_price,SUM(SDD.qty) as RESERVED_QTY from tbl_product_stock as S INNER JOIN tbl_product as P on P.sku = S.sku 
		LEFT OUTER JOIN tbl_sales_detail as SDD on S.sku =  SDD.sku 
		LEFT OUTER JOIN tbl_sales_header as SDH on SDH.invoice_no = SDD.invoice_no WHERE SDH.delivery_status IN('Pending') ";

	 	if (!empty($data['filter_department'])) {
			$sql .= " AND P.sku_department_code = '" . $this->db->escape($data['filter_department']) . "'";
	 	}
	 	if (!empty($data['filter_location'])) {
			$sql .= " AND SDH.location_code = '" . $this->db->escape($data['filter_location']) . "'";
	 	}
	 	if($data['filter_product_id']){
			$sql .= " AND P.product_id = '".$this->db->escape($data['filter_product_id'])."' ";
	 	}
	 	if($data['filter_vendor']){
			$sql .= " AND P.sku_vendor_code = '" . $this->db->escape($data['filter_vendor']) . "'";
	 	} 	
	 	$sql .=" GROUP BY S.sku, P.name, P.product_id";
		
		if($export == ''){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}
		return $this->db->query($sql)->rows;
	}

	public function getgetstock_valuation_with_reserved_stock_Reports_total($data,$export=''){
		$sql = "SELECT (SD.qty-PS.sku_qty) AS Aval FROM `tbl_sales_invoice_details` as SD LEFT JOIN tbl_product AS P on P.sku = SD.sku LEFT JOIN tbl_product_stock AS PS on PS.sku = SD.sku AND SD.location_code = PS.location_Code LEFT JOIN tbl_sales_invoice_header AS SH on SH.invoice_no=SD.invoice_no where do_qty=0";

		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_from'])));
			$data['filter_date_to']  = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date_to']))); 
			$sql .= " AND SH.invoice_date between  '" . $data['filter_date_from'] . "' AND '" . $data['filter_date_to'] . "'";
		}
		if($data['filter_location']){
			$sql .=" AND PS.location_code='".$data['filter_location']."' ";
		}
		if($data['filter_department']){
			$sql .=" AND P.sku_department_code='".$data['filter_department']."' ";
		}
		if($export == ''){
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
		}		
		return $this->db->query($sql)->rows;
	}
	public function getReservedQtyByProduct($product_id){
		$sql = "SELECT sum(qty) as resQty FROM ".DB_PREFIX."sales_do_details as sdd LEFT JOIN ".DB_PREFIX."sales_do_header as sdh ON sdd.do_no=sdh.do_no where sdd.product_id='".$product_id."' AND sdh.status IN ('Pending_Delivery','Scheduling','On_Delivery') ";
		return $this->db->query($sql)->row['resQty'];
	}
	public function getlazadaInfo($lazId){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."lazada_order_details where id='".$lazId."' ")->row;
	}
	public function getQsmInfo($qsmId){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."qsm_order_details where id='".$qsmId."' ")->row;
	}
	public function getSubCategoryByCode($code){
		if($code!=''){
			return $this->db->query("SELECT * FROM ".DB_PREFIX."subcategory where subCate_code='".$code."' ")->row['name'];
		}else{
			return '';
		}
	}
	public function getResevedStockforNormalItems($product_id,$location_code){
		$sql = "SELECT SUM(D.qty) as qty FROM ".DB_PREFIX."sales_detail as D LEFT JOIN tbl_sales_header as H 
		        on H.invoice_no = D.invoice_no WHERE D.product_id ='".$product_id."' and H.location_code ='".$location_code."' and H.delivery_status IN('Pending') AND D.cancel='0' ";

		$results = $this->db->query($sql)->row;
        return $results['qty'];
	}
	public function getResevedStockforpackageItems($product_id,$location_code){
		  $sql = "SELECT SUM(D.qty * C.quantity) as totalQty FROM ".DB_PREFIX."sales_detail as D 
					LEFT JOIN tbl_sales_header as H on H.invoice_no = D.invoice_no 
					LEFT JOIN tbl_child_products as C on C.parant_sku = D.product_id
					WHERE C.child_sku ='".$product_id."' AND H.location_code='".$location_code."' AND H.delivery_status IN('Pending') AND D.cancel='0' ";
		// echo $sql; die;
		return $this->db->query($sql)->row['totalQty'];
	}

    public function getPartiallyReservedStock($product_id, $location_code)
    {
        $resvQty = $this->db->query("SELECT sum(sid.qty) as qty from tbl_sales_invoice_header as sih left join tbl_sales_invoice_details as sid on sid.invoice_no = sih.invoice_no where sih.location_code = '".$location_code."' AND sid.product_id = '".$product_id."' and sih.delivery_status in ('Scheduling','Partial_Scheduling','Pending_Delivery','On_Delivery')")->row['qty'];

        $doQty = $this->db->query("SELECT sum(sdd.qty) as qty from tbl_sales_do_details as sdd left join tbl_sales_do_header as sdh on sdd.do_no = sdh.do_no where sdd.product_id = '".$product_id."' AND sdh.status not in ('Canceled','Faild_Delivery','Not_delivered','Delivered')")->row['qty'];
        return $resvQty - $doQty;
    }
	public function getstockHeader($data){
		$sql = 'SELECT * from '.DB_PREFIX.'stockadjustment_header ';
		if(isset($data['stock_date_from']) && isset($data['stock_date_to'])){
			
			$data['stock_date_from'] = date('Y-m-d',strtotime(str_replace('/','-',$data['stock_date_from'])));
			$data['stock_date_to']   = date('Y-m-d',strtotime(str_replace('/','-',$data['stock_date_to'])));

			$sql .= "  WHERE stkAdjustment_date BETWEEN '".$data['stock_date_from']."' AND '".$data['stock_date_to']."' ";
		}
		if($data['filter_location_code']!=''){
			$sql .= " AND location_code= '".$data['filter_location_code']."' ";
		}
		if($data['filter_transactionNo']!=''){
			$sql .= " AND stkAdjustment_id= '".$data['filter_transactionNo']."' ";
		}
		$query = $this->db->query($sql);
		return $query->rows;
	}
	public function getstockHeaderDetails($id)
	{
	   $sql   = "SELECT * from ".DB_PREFIX."stockadjustment_detail where stockAdjustment_id='".$id."' ";
	   $query = $this->db->query($sql);
	   return $query->rows;	
	}
	public function getPurchaseAndSalesProductsCount($product_id)
	{
		$purchase = $this->db->query("SELECT COUNT(*) as CNT FROM ".DB_PREFIX."purchase_to_product where product_id='".$product_id."' ")->row['CNT'];
		$sales    = $this->db->query("SELECT COUNT(*) as CNT FROM ".DB_PREFIX."sales_detail WHERE product_id='".$product_id."'")->row['CNT'];
		return array('purchase'=> $purchase, 'sales'=> $sales);
	}
}
?>