<?php
class ModelInventoryProductImport extends Model {

	public function getTempProducts(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_import order by id asc ")->rows;
	}
	public function getCustomerDetails($custCode){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where customercode ='".$custCode."' ")->row;
	}
	public function getProduct($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_networks where product_sku ='".$sku."' AND product_id !=''")->row;
	}
	public function getQSMNetworkId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where name ='qoo10' AND status ='Active'")->row;
	}
	public function updateQSMProduct($data){
		$network_id = $this->getQSMNetworkId()['id'];
		return $this->db->query("INSERT INTO ".DB_PREFIX."product_networks (network_id,product_id,product_sku,price,	quantity,status,network_response_id) VALUES('".$this->db->escape($network_id)."','".$this->db->escape($data['product_id'])."','".$this->db->escape($data['sku'])."','0','0','Active','0') ");
	}
	public function deleteAllData(){
		$this->db->query("TRUNCATE ".DB_PREFIX."product_import");
	}
	public function checkCustomerByPhone($phone){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."customers where mobile ='".$phone."' ")->row;
	}
	public function getShippingDetails($cust_code){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."shipping where customer_id ='".$cust_code."' AND isdefault='1'")->row;
	}
	public function createNewCustomer($data){
		$company_id = $this->session->data['company_id'];
		$address2   = $data[27].' '.$data[28];
		$customerMobile = str_replace('-', '', $data[37]) !='' ? str_replace('-', '', $data[37]) : str_replace('-', '', $data[36]);

		$sql = "INSERT INTO ".DB_PREFIX."customers (cust_code, company_id, status, name, location_code, mobile, address1, city, zipcode, email, currency_code, tax_allow, country,network_response_id, createdon) 
			VALUES ('".$this->db->escape($data['cust_code'])."', '".$this->db->escape($company_id)."', '1', '".$this->db->escape($data[33])."', 'HQ', '".$this->db->escape($customerMobile)."', '".$this->db->escape($data[22])."', '', '".$this->db->escape($data[23])."', '', 'SGD', '0', '1','', '".date('Y-m-d H:i:s')."')";

		$this->db->query($sql);
		$customer_id = $this->db->getLastId();

		if($customer_id !=''){
			$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,country,zip,contact_no,notes,isdefault) VALUES('".$this->db->escape($customer_id)."','SH".$this->db->escape($customer_id)."','".$this->db->escape($data[22])."','1','".$this->db->escape($data[23])."','".$this->db->escape($customerMobile)."','','1') ";
			
			$this->db->query($sql);
			$shipping_id = $this->db->getLastId();
			$res 		 = array('shipping_id' => $shipping_id, 'customercode' => $customer_id);
			return $res;
		
		}else{
			return false;
		}
	}
	public function insertTempTable($data){
		$sql = "INSERT INTO ".DB_PREFIX."product_import (`sku`, `name`, `description`, `department`, `category`, `subcategory`, `brand`, `vendor`, `price`, `cost`) VALUES ('".$this->db->escape($data[0])."', '".$this->db->escape($data[1])."', '".$this->db->escape($data[2])."', '".$this->db->escape($data[3])."', '".$this->db->escape($data[4])."', '".$this->db->escape($data[5])."', '".$this->db->escape($data[6])."', '".$this->db->escape($data[7])."', '".$this->db->escape($data[8])."', '".$this->db->escape($data[9])."')";
		$this->db->query($sql);
	}
	public function getDetailsFromTempTable(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product_import ")->rows;
	}
	public function checkOrderExistOrNot($ordernumber){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header where network_order_id='".$ordernumber."' AND network_id='2' AND delivery_status!='Canceled' ")->row;
	}
	public function getSalesLastId(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."sales_header ORDER BY id DESC LIMIT 1")->row['id'];
	}
	public function getProductById($product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where product_id='".$product_id."' ")->row;
	}
	public function getProductBySKU($sku){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."product where sku='".$sku."' ")->row;
	}
	public function addProduct($data){

		$product   = $this->getProductBySKU($data['sku']);
		$userName  = $this->session->data['username'];

		$department= $this->getDepartmentCode($data['department']);
		$category  = $this->getCategoryCode($department, $data['category']);
		$vendor    = $this->getVendorCode($data['vendor']);

		if(!empty($product)){
			$sql = "UPDATE ".DB_PREFIX."product set 
				sku 				 = '".$this->db->escape($data['sku'])."',
				name 				 = '".$this->db->escape($data['name'])."',
				sku_description 	 = '".$this->db->escape($data['description'])."',
				sku_shortdescription = '".$this->db->escape($data['description'])."',
				sku_department_code  = '".$this->db->escape($department)."',
				sku_category_code    = '".$this->db->escape($category)."',
				sku_brand_code       = '".$this->db->escape($data['brand'])."',
				sku_vendor_code      = '".$this->db->escape($vendor)."',
				sku_status			 = '1',
				price 				 = '".$this->db->escape($data['price'])."',
				open_price			 = '".$this->db->escape($data['price'])."',
				modifyby			 = '".$userName."',
				open_price			 = '".date('Y-m-d')."',
				sku_subcategory_code = '".$this->db->escape($data['subcategory'])."' where sku='".$data['sku']."' ";
				$this->db->query("DELETE FROM ".DB_PREFIX."product_import where sku = '".$data['sku']."' ");
		}else{

			$sql = "INSERT INTO ".DB_PREFIX."product (sku,name,sku_description,sku_shortdescription,sku_department_code,sku_category_code,sku_brand_code,sku_vendor_code,sku_status,createby,createon,price,open_price,sku_subcategory_code) VALUES ('".$this->db->escape($data['sku'])."','".$this->db->escape($data['name'])."','".$this->db->escape($data['description'])."','".$this->db->escape($data['description'])."','".$this->db->escape($department)."','".$this->db->escape($category)."','".$this->db->escape($data['brand'])."','".$this->db->escape($vendor)."','1','".$userName."','".date('Y-m-d')."','".$this->db->escape($data['price'])."','".$this->db->escape($data['price'])."','".$this->db->escape($data['subcategory'])."') ";
			$this->db->query($sql);
			$productId  = $this->db->getLastId();
			if($productId){
				$locations = $this->getAllLocations();
				foreach($locations as $loc){
					$sql = "INSERT INTO ".DB_PREFIX."product_stock (location_Code,sku,sku_cost,sku_price,product_id,createdby,createdon) VALUES('".$loc['location_code']."','".$this->db->escape($data['sku'])."','".$this->db->escape($data['cost'])."','".$this->db->escape($data['price'])."','".$productId."','".$userName."','".date('Y-m-d')."')";
					$this->db->query($sql);
				}
				$this->db->query("DELETE FROM ".DB_PREFIX."product_import where sku = '".$data['sku']."' ");
			}
		}
	}
    public function checkOrderIdAndSku($orderId, $product_id){
        return $this->db->query("SELECT sh.id FROM ".DB_PREFIX."sales_header as sh LEFT JOIN ".DB_PREFIX."sales_detail as sd on sh.invoice_no = sd.invoice_no WHERE sd.product_id='".$this->db->escape($product_id)."' AND sh.network_order_id='".$this->db->escape($orderId)."' AND sh.delivery_status!='Canceled' ")->row;
    }
    public function delete($id){
        $this->db->query("DELETE FROM ".DB_PREFIX."product_import where id='".$id."' ");
    }
    public function getAllLocations(){
    	return $this->db->query("SELECT * FROM ".DB_PREFIX."location ")->rows;
    }
    public function getDepartmentCode($depName){
    	$department = $this->db->query("SELECT * FROM ".DB_PREFIX."department where department_name='".$this->db->escape($depName)."' ")->row;

    	if(!empty($department)){
    		return $department['department_code'];
    	}else{
    		$maxId = $this->db->query("SELECT * FROM ".DB_PREFIX."department order by department_id desc ")->row['department_id'];
    		$depmCode = 'D'.str_pad($maxId + 1, 4, '0', STR_PAD_LEFT);
    		$this->db->query("INSERT INTO ".DB_PREFIX."department (department_code, department_name, company_id, status, createdby, createdon) VALUES ('".$depmCode."','".$this->db->escape($depName)."','1','1','".$this->session->data['username']."','".date('Y-m-d')."') ");
    		return $depmCode;
    	}
    }
    public function getCategoryCode($depCode, $cateName){
    	$cate = $this->db->query("SELECT * FROM ".DB_PREFIX."category where department_code ='".$this->db->escape($depCode)."' AND category_name ='".$this->db->escape($cateName)."' ")->row;
    	if(!empty($cate)){
    		return $cate['category_code'];
    	}else{
    		$maxId = $this->db->query("SELECT * FROM ".DB_PREFIX."category order by category_id desc ")->row['category_id'];
    		$catCode = 'C'.str_pad($maxId + 1, 4, '0', STR_PAD_LEFT);
    		$this->db->query("INSERT INTO ".DB_PREFIX."category (category_code, category_name, company_id, status, department_code, createdby, createdon) VALUES ('".$catCode."','".$this->db->escape($cateName)."','1','1','".$depCode."','".$this->session->data['username']."','".date('Y-m-d')."') ");
    		return $catCode;
    	}
    }
    public function getVendorCode($venName){
    	$vend = $this->db->query("SELECT * FROM ".DB_PREFIX."vendor where vendor_name ='".$this->db->escape($venName)."' ")->row;
    	if(!empty($vend)){
    		return $vend['vendor_code'];
    	}else{
    		$maxId = $this->db->query("SELECT * FROM ".DB_PREFIX."vendor order by vendor_id desc ")->row['vendor_id'];
    		$vendCode = 'V'.str_pad($maxId + 1, 4, '0', STR_PAD_LEFT);
    		$this->db->query("INSERT INTO ".DB_PREFIX."vendor (company_id,vendor_code,vendor_name,status,create_user,create_date) VALUES ('1','".$vendCode."','".$this->db->escape($venName)."','1','".$this->session->data['username']."','".date('Y-m-d')."' )");
    		return $vendCode;
    	}
    }
}
?>