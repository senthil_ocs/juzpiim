<?php 
class ModelLocalisationTaxClass extends Model {
	public function addTaxClass($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "tax_class (title,company_id,description,date_added) VALUES ('" . $this->db->escape($data['title']) . "','".$company_id."','" . $this->db->escape($data['description']) . "',curdate())"); 
			

		$tax_class_id = $this->db->getLastId();

		if (isset($data['tax_rule'])) {
			foreach ($data['tax_rule'] as $tax_rule) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "tax_rule (tax_class_id,tax_rate_id,based,priority) VALUES ('" . (int)$tax_class_id . "','" . (int)$tax_rule['tax_rate_id'] . "','" . $this->db->escape($tax_rule['based']) . "','" . (int)$tax_rule['priority'] . "')");
			}
		}

		$this->cache->delete('tax_class');
	}

	public function editTaxClass($tax_class_id, $data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "tax_class SET 
			title = '" . $this->db->escape($data['title']) . "'
			, description = '" . $this->db->escape($data['description']) . "'
			, date_modified = curdate() WHERE tax_class_id = '" . (int)$tax_class_id . "' AND company_id = '".$company_id."'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rule WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		if (isset($data['tax_rule'])) {
			foreach ($data['tax_rule'] as $tax_rule) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "tax_rule (tax_class_id,tax_rate_id,based,priority) VALUES ('" . (int)$tax_class_id . "','" . (int)$tax_rule['tax_rate_id'] . "','" . $this->db->escape($tax_rule['based']) . "','" . (int)$tax_rule['priority'] . "')");
			}
		}

		$this->cache->delete('tax_class');
	}

	public function deleteTaxClass($tax_class_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getTaxClass($tax_class_id);
		$strLogDetails = array(
			'type' 		=> 'Tax Class',
			'id'   		=> $tax_class_id,
			'code' 		=> $tableDetails['title'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);

		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$tax_class_id . "' AND company_id = '".$company_id."'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "tax_rule WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		$this->cache->delete('tax_class');
	}

	public function getTaxClass($tax_class_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE tax_class_id = '" . (int)$tax_class_id . "' AND company_id = '".$company_id."'");

		return $query->row;
	}

	public function getTaxClasses($data = array()) {
		$company_id	= $this->session->data['company_id'];
		
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "tax_class WHERE company_id = '".$company_id."'";
			$sql .= " ORDER BY title";	
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}
			$query = $this->db->query($sql);
			return $query->rows;		
		} else {
			$tax_class_data = $this->cache->get('tax_class');
			if (!$tax_class_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_class WHERE company_id = '".$company_id."' order by tax_class_id desc ");

				$tax_class_data = $query->rows;

				$this->cache->set('tax_class', $tax_class_data);
			}
			$tax_class_id = array_column($tax_class_data, 'tax_class_id');
			array_multisort($tax_class_id, SORT_DESC, $tax_class_data);
			return $tax_class_data;			
		}
	}

	public function getTotalTaxClasses() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "tax_class WHERE company_id = '".$company_id."'");

		return $query->row['total'];
	}	

	public function getTaxRules($tax_class_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tax_rule WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->rows;
	}

	public function getTotalTaxRulesByTaxRateId($tax_rate_id) {
		$query = $this->db->query("SELECT COUNT(DISTINCT tax_class_id) AS total FROM " . DB_PREFIX . "tax_rule WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");

		return $query->row['total'];
	}		
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>