<?php 
class ModelLocalisationStockStatus extends Model {
	public function addStockStatus($data) {
		$company_id	= $this->session->data['company_id'];
		foreach ($data['stock_status'] as $language_id => $value) {
			if (isset($stock_status_id)) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "stock_status (stock_status_id,company_id,language_id,name) VALUES ('" . (int)$stock_status_id . "','".$company_id."','" . (int)$language_id . "','" . $this->db->escape($value['name']) . "')");					
			} else {
				$this->db->query("INSERT INTO " . DB_PREFIX . "stock_status (language_id,company_id,name) VALUES('" . (int)$language_id . "','".$company_id."','" . $this->db->escape($value['name']) . "')"); 					
				$stock_status_id = $this->db->getLastId();
			}
		}

		$this->cache->delete('stock_status');
	}

	public function editStockStatus($stock_status_id, $data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("DELETE FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND company_id = '".$company_id."'");

		foreach ($data['stock_status'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "stock_status (language_id,company_id,name) VALUES ('" . (int)$language_id . "','".$company_id."','" . $this->db->escape($value['name']) . "')"); 
				
		}

		$this->cache->delete('stock_status');
	}

	public function deleteStockStatus($stock_status_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getStockStatus($stock_status_id);
		$strLogDetails = array(
			'type' 		=> 'Stock Status',
			'id'   		=> $stock_status_id,
			'code' 		=> $tableDetails['name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND company_id = '".$company_id."'");

		$this->cache->delete('stock_status');
	}

	public function getStockStatus($stock_status_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND company_id = '".$company_id."' AND language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getStockStatuses($data = array()) {
	
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			
			$sql = "SELECT * FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND company_id = '".$company_id."'";

			$sql .= " ORDER BY name";	

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	

				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}			

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$stock_status_data = $this->cache->get('stock_status.' . (int)$this->config->get('config_language_id'));

			if (!$stock_status_data) {
				$query = $this->db->query("SELECT stock_status_id, name FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND company_id = '".$company_id."' ORDER BY name");

				$stock_status_data = $query->rows;

				$this->cache->set('stock_status.' . (int)$this->config->get('config_language_id'), $stock_status_data);
			}	

			return $stock_status_data;			
		}
	}

	public function getStockStatusDescriptions($stock_status_id,$stock_name='') {
		$company_id	= $this->session->data['company_id'];
		$stock_status_data = array();
		if($stock_name) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE stock_status_id != '" . (int)$stock_status_id . "' AND company_id = '".$company_id."' AND name ='".$this->db->escape($stock_name)."'");
		} else {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "stock_status WHERE stock_status_id = '" . (int)$stock_status_id . "' AND company_id = '".$company_id."'");
		}
		
		foreach ($query->rows as $result) {
			$stock_status_data[$result['language_id']] = array('name' => $result['name']);
		}
		return $stock_status_data;
	}

	public function getTotalStockStatuses() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "stock_status WHERE language_id = '" . (int)$this->config->get('config_language_id') . "' AND company_id = '".$company_id."'");

		return $query->row['total'];
	}	
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>