<?php
class ModelAjaxCart extends Model {

	public function getTotalsCollection($apply_tax_type='') {
		
		//echo $apply_tax_type;
		$total_data = array();
		$total = 0;

		if($apply_tax_type==''){
			$apply_tax_type       	= $this->config->get('config_apply_tax_purchase');
		}

		$taxes = $this->cart->getTaxes($apply_tax_type);
		$this->load->model('ajax/addcart');
		$sort_order = array();
		$results = $this->model_ajax_addcart->getExtensions('total');


		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('total/' . $result['code']);
				$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes,$apply_tax_type);
			}
			// echo 'model_total_'.$result['code'].'<br>';
			$sort_order = array();
			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}
			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		// printArray($total_data); die;
		$this->session->data['purchase_totals']	= $total_data;
		return $total_data;
	}

}
?>