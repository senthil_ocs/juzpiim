<?php
class ModelAjaxInformation extends Model {
	public function isCheck($code) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p
		WHERE (i.Barcode = '" . $this->db->escape($code) . "' 
		OR i.InventoryCode = '" . $this->db->escape($code) . "') 
		AND i.CompanyID = '" . $companyId . "' 
		AND i.InActive = '1'
		");
		if ($query->num_rows) {
			return true;
		}
		
		return false;
		//return $query->row;
	}
	
	public function getInformation($code) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p
		WHERE (i.Barcode = '" . $this->db->escape($code) . "' 
		OR i.InventoryCode = '" . $this->db->escape($code) . "')
		AND i.CompanyID = '" . $companyId . "' 
		AND i.InActive = '1'
		");
		
		return $query->rows;
	}
	
	public function getIventoryDetails($inventory_id) {
		$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product p
		WHERE i.InventoryID = '" . $this->db->escape($inventory_id). "' 
		AND i.CompanyID = '" . $companyId . "' 
		AND i.InActive = '1'
		");
		
		return $query;
	}
}
?>