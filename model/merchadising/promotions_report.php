<?php
class ModelMerchadisingPromotionsReport extends Model {
	
	public function getTotalSpecialProducts($data) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_special p2s ON (p.product_id = p2s.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' ";

		if (!empty($data['date_start']) && !empty($data['date_end'])) {
			$sql .= " AND ('".$data['date_start']."' BETWEEN p2s.date_start and p2s.date_end) AND ('".$data['date_end']."' BETWEEN p2s.date_start and p2s.date_end)";
		} else {
		    $sql .= " AND CURDATE() between p2s.date_start and p2s.date_end";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getSpecialProducts($data = array()) {
		
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT p.*, pd.* FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_special p2s ON (p.product_id = p2s.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' AND status = '1' ";

		if (!empty($data['date_start']) && !empty($data['date_end'])) {
			$sql .= " AND ('".$data['date_start']."' BETWEEN p2s.date_start and p2s.date_end) AND ('".$data['date_end']."' BETWEEN p2s.date_start and p2s.date_end)";
		} else {
		    $sql .= " AND CURDATE() between p2s.date_start and p2s.date_end";
		}

		$sql .= " GROUP BY p.product_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getCompanyDetails($company_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE c.company_id = '" . (int)$company_id . "'");
		return $query->row;
	}
	
}
?>