<?php
class ModelMerchadisingPriceChangeUtility extends Model {

	public function editProduct($product_id, $data) {
		
		if (!empty($data['price'])) {
		    $update_field = " price = '" . (float)$data['price'] . "' ";
		} elseif (!empty($data['quantity'])) {
		    $update_field = " quantity = '" . (int)$data['quantity'] . "' ";
		} else {
		    $update_field = " status = '" . (int)$data['status'] . "' ";
		} 

		$this->db->query("UPDATE " . DB_PREFIX . "product SET ".$update_field." 
			WHERE product_id IN (" . $product_id . ")");

		$this->cache->delete('product');
	}
	
	public function getTotalProducts($data = array()) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (!empty($data['filter_category_id'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";			
		}
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' "; 
		
		if (!empty($data['filter_inventory_id'])) {
			$sql .= " AND p.sku LIKE '%" . $this->db->escape($data['filter_inventory_id']) . "%'";
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND p2b.barcode LIKE '%" . $this->db->escape($data['filter_barcodes']) . "%'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_price'])) {
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getProducts($data = array()) {
		
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT * FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		if (!empty($data['filter_category_id'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";			
		}
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' "; 
		
		if (!empty($data['filter_inventory_id'])) {
			$sql .= " AND p.sku LIKE '%" . $this->db->escape($data['filter_inventory_id']) . "%'";
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND p2b.barcode LIKE '%" . $this->db->escape($data['filter_barcodes']) . "%'";
		}
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_price'])) {
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_price']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}

		$sql .= " GROUP BY p.product_id";
		
		$sort_data = array(
			'pd.name',
			'p.price',
			'p.quantity',
			'p.status',
			'p.sort_order'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY pd.name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}
}
?>