<?php
    class ModelApiApiCommon extends Model {
       
        function getProducts(){
             $select = "select A.*,B.sku_qty,B.sku_salesQty,B.sku_cost,B.sku_price,B.sku_avg from tbl_product as A LEFT JOIN  tbl_product_stock as B on(A.sku=B.sku)";
             $list = $this->db->query($select);
             foreach($list as $li){
              $nsku = trim($li['sku']);
              $newres[] = array("product_id"=>trim($li['product_id']),"name"=>trim($li['name']),"sku"=>$nsku,
                                "sku_description"=>trim($li['sku_description']),"sku_shortdescription"=>trim($li['sku_shortdescription']),"sku_department_code"=>$li['sku_department_code'],
                                "sku_category_code"=>trim($li['sku_category_code']),"sku_brand_code"=>trim($li['sku_brand_code']),"sku_uom"=>$li['sku_uom'],
                                "sku_uom_type"=>trim($li['sku_uom_type']),"sku_image"=>trim($li['sku_image']),"sku_status"=>$li['sku_status'],
                                "sku_gstallowed"=>trim($li['sku_gstallowed']),"sku_qty"=>trim($li['sku_qty']),"sku_salesQty"=>$li['sku_salesQty'],
                                "sku_cost"=>trim($li['sku_cost']),"sku_price"=>trim($li['sku_price']),"sku_avg"=>$li['sku_avg']
                                );
            }
             return $newres;
        }

        public function showJsonResults($arr) {
            echo $this->array2json($arr);
        }
        public function array2json($arr) {
            $parts = array();
            $is_list = false;
            //Find out if the given array is a numerical array
            $keys = array_keys($arr);
            $max_length = count($arr)-1;
            if($arr) {
                if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {
                    //See if the first key is 0 and last key is length - 1
                    $is_list = true;
                    for($i=0; $i<count($keys); $i++) {
                        //See if each key correspondes to its position
                        if($i != $keys[$i]) { //A key fails at position check.
                            $is_list = false; //It is an associative array.
                            break;
                       }
                   }
                }
            }

            foreach($arr as $key=>$value) {
                if(is_array($value)) {
                    //Custom handling for arrays
                    if($is_list) $parts[] = $this->array2json($value); /* :RECURSION: */
                    else $parts[] = '"' . $key . '":' . $this->array2json($value); /* :RECURSION: */
                } else {
                    $str = '';
                    if(!$is_list) $str = '"' . $key . '":';
                               //Custom handling for multiple data types
                    if(is_numeric($value)) $str .= $value; //Numbers
                    elseif($value === false) $str .= 'false'; //The booleans
                    elseif($value === true) $str .= 'true';
                    else $str .= '"' . $value . '"'; //All other things
                    // :TODO: Is there any more datatype we should be in the lookout for? (Object?)
                    $parts[] = $str;
               }
            }

            $json = implode(',',$parts);

            if($is_list) return  '[' . $json . ']';//Return associative JSON
            return  '{' . $json . '}';//Return associative JSON
        }

        public function showXmlResults($_response, $sub_element='') {
            $result = $this->assocArrayToXML($_response, $sub_element);
            ob_clean();
            if($result) {
                echo $this->formatXmlString($result);
            }
        }

        public function assocArrayToXML($ar, $sub_element='childdata', $root_element_name='data') {
            if(!empty($ar)){
                $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><{$root_element_name}></{$root_element_name}>");
                $f = create_function('$f,$c,$a','
                        foreach($a as $k=>$v) {
                            if(is_array($v)) {
                                if (is_numeric($k)) {
                                    $ch=$c->addChild('.$sub_element.');
                                } else {
                                    $ch=$c->addChild($k);
                                }
                                $f($f,$ch,$v);
                            } else {
                                $c->addChild($k,$v);
                            }
                        }');
                $f($f,$xml,$ar);
                return html_entity_decode($xml->asXML());
            }
        }

        

        public function formatXmlString($xml) {
            // add marker linefeeds to aid the pretty-tokeniser (adds a linefeed between all tag-end boundaries)
            $xml = preg_replace('/(>)(<)(\/*)/', "$1\n$2$3", $xml);

            // now indent the tags
            $token      = strtok($xml, "\n");
            $result     = ''; // holds formatted version as it is built
            $pad        = 0; // initial indent
            $matches    = array(); // returns from preg_matches()

              // scan each line and adjust indent based on opening/closing tags
            while ($token !== false) :
                // test for the various tag states
                // 1. open and closing tags on same line - no change
                if(preg_match('/.+<\/\w[^>]*>$/', $token, $matches)) :
                    $indent=0;
                // 2. closing tag - outdent now
                elseif(preg_match('/^<\/\w/', $token, $matches)) :
                    $pad--;
                // 3. opening tag - don't pad this one, only subsequent tags
                elseif (preg_match('/^<\w[^>]*[^\/]>.*$/', $token, $matches)) :
                    $indent=1;
                // 4. no indentation needed
                else :
                    $indent = 0;
                endif;
                // pad the line with the required number of leading spaces
                $line    = str_pad($token, strlen($token)+$pad, ' ', STR_PAD_LEFT);
                $result .= $line . "\n"; // add to the cumulative result, with linefeed
                $token   = strtok("\n"); // get the next token
                $pad    += $indent; // update the pad size for subsequent lines
              endwhile;
              return $result;
        }
    }        
?>