<?php
class ModelPosPos extends Model {

	public function getProductByName($data = array())
	{
		$company_id	= $this->session->data['company_id'];
		if ($data['limit'] < 1) {
			$data['limit'] = 20;
		}
		$sql = "SELECT  * FROM " . DB_PREFIX . "product p ";
				//LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		/*if (!empty($data['filter_category_id']) || !empty($data['filter_category'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}*/
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";			
		}

		/*if (!empty($data['filter_department'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_department p2d ON (p.product_id = p2d.product_id)";			
		}
		
		if (!empty($data['filter_brand'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_brand p2bd ON (p.product_id = p2bd.product_id)";			
		}*/
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' "; 
		
		if (!empty($data['filter_inventory_id'])) {
			$sql .= " AND p.sku LIKE '%" . $this->db->escape($data['filter_inventory_id']) . "%'";
		}
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND p2b.barcode LIKE '%" . $this->db->escape($data['filter_barcodes']) . "%'";
		}

		
		
		if (!empty($data['filter_name'])) {
			$sql .= " AND p.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if(!empty($data['searchkey'])) {
			$sql .= " AND (";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['searchkey'])));
			/*foreach ($words as $word) {
				$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
			}			
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['searchkey']) . "%'";*/			
			$sql .= " OR LCASE(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['searchkey'])) . "%'";	
			$sql .= ")";			
		}

		if (!empty($data['filter_price']) && !empty($data['filter_priceto']) ) {
			$sql .= " AND p.price BETWEEN '" . $this->db->escape($data['filter_price']) . "'  AND '".$this->db->escape($data['filter_priceto'])."'";
		} else if(!empty($data["filter_price"])){
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_price']) . "%'";
		} else if(!empty($data["filter_priceto"])){
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_priceto']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}

		/*if (isset($data['filter_description']) && !is_null($data['filter_description'])) {
			$sql .= " AND pd.short_description LIKE '" . $this->db->escape($data['filter_description']) . "%'";
		}*/

		/*if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND p.status = '" . (int)$data['filter_status'] . "'";
		}*/
		
		/*if (!empty($data['filter_department'])) {
			$sql .= " AND p2d.department_id = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if (!empty($data['filter_category'])) {
			$sql .= " AND p2c.category_id = '" . $this->db->escape($data['filter_category']) . "'";
		}*/
		
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.vendor_id = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		
		/*if (!empty($data['filter_brand'])) {
			$sql .= " AND p2bd.brand_id = '" . $this->db->escape($data['filter_brand']) . "'";
		}*/

		//$sql .= " GROUP BY p.product_id";
		
		$sort_data = array(
			'p.name',
			'p.price',
			'p.quantity',
			//'p.status',
			'p.sort_order'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY p.name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}	
		$query = $this->db->query($sql);

		return $query->rows;
		
	}

	public function getTotalProductByName($data = array())
	{
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT TOP 50 COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p ";
				//LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";

		/*if (!empty($data['filter_category_id']) || !empty($data['filter_category'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id)";			
		}*/
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)";			
		}
		
		/*if (!empty($data['filter_department'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_department p2d ON (p.product_id = p2d.product_id)";			
		}
		
		if (!empty($data['filter_brand'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_brand p2bd ON (p.product_id = p2bd.product_id)";			
		}*/
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "product_to_company p2co ON (p.product_id = p2co.product_id)"; 
		
		$sql .= " WHERE p2co.company_id='".$company_id."' "; 
		
		if (!empty($data['filter_barcodes'])) {
			$sql .= " AND p2b.barcode LIKE '%" . $this->db->escape($data['filter_barcodes']) . "%'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND p.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_price']) && !empty($data['filter_priceto']) ) {
			$sql .= " AND p.price BETWEEN '" . $this->db->escape($data['filter_price']) . "'  AND '".$this->db->escape($data['filter_priceto'])."'";
		} else if(!empty($data["filter_price"])){
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_price']) . "%'";
		} else if(!empty($data["filter_priceto"])){
			$sql .= " AND p.price LIKE '%" . $this->db->escape($data['filter_priceto']) . "%'";
		}

		if (isset($data['filter_quantity']) && !is_null($data['filter_quantity'])) {
			$sql .= " AND p.quantity = '" . $this->db->escape($data['filter_quantity']) . "'";
		}

		/*if (isset($data['filter_description']) && !is_null($data['filter_description'])) {
			$sql .= " AND pd.short_description LIKE '" . $this->db->escape($data['filter_description']) . "%'";
		}*/		
		
		/*if (!empty($data['filter_department'])) {
			$sql .= " AND p2d.department_id = '" . $this->db->escape($data['filter_department']) . "'";
		}
		
		if (!empty($data['filter_category'])) {
			$sql .= " AND p2c.category_id = '" . $this->db->escape($data['filter_category']) . "'";
		}*/
		
		if (!empty($data['filter_vendor'])) {
			$sql .= " AND p.vendor_id = '" . $this->db->escape($data['filter_vendor']) . "'";
		}
		
		/*if (!empty($data['filter_brand'])) {
			$sql .= " AND p2bd.brand_id = '" . $this->db->escape($data['filter_brand']) . "'";
		}*/

		//$sql .= " LIMIT 0,50"; 

		$query = $this->db->query($sql);

		return $query->row['total'];
		
	}

	public function getProductBarcodes($product_id)
	{
		$product_department_data = array();
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_to_barcode WHERE product_id = '" . (int)$product_id . "'");
		$result = array();
		foreach ($query->rows as $key => $value){
		    $result[$key] = $value['barcode'];
		}
		return $result;
	}

	public function getProductSpecials($product_id)
	{
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND CAST(curdate() AS DATE) >= date_start AND CAST(curdate() AS DATE) <= date_end ORDER BY priority, price");
		return $query->row;
	}	
}
?>