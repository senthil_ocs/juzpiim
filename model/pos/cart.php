<?php
class ModelPosCart extends Model {
	public function getLastIncrementId() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT increment_prefix As prefix, increment_last_id as last_id FROM " . DB_PREFIX . "entity_increment 
			WHERE entity_type_id = '1' AND company_id = '" . (int)$company_id . "'");
		$results	= $query->row;
		/*if(empty($results)) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "entity_increment SET 
				  entity_type_id = '1'
				, increment_prefix = 'HQ'
				, increment_last_id = '100000001'
				, company_id = '".$company_id."'
			");
			$lastId	= $this->db->getLastId();
			if(!empty($lastId)) {
				$query = $this->db->query("SELECT DISTINCT increment_prefix As prefix, increment_last_id as last_id FROM " . DB_PREFIX . "entity_increment WHERE entityId = '".$lastId."'");
				$results	= $query->row;
			}
		}*/
		return $results;
	}
	
	public function isCheckOrder($order_id,$order_status='') {
		$current_date = date('Y-m-d H:i:s');
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "order_status WHERE company_id = '".$company_id."' AND name='Voided'";
		$query = $this->db->query($sql);
		$orderStatus	= $query->row;
		if(!empty($orderStatus)) {
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "order WHERE company_id = '".$company_id."' 
				AND (order_id='" .$order_id . "' OR invoice_no = '" .$order_id . "')
				AND order_status_id NOT IN (".$orderStatus['order_status_id'].")
				AND DATEDIFF(date_added, curdate()) = 0;
			");
			$results	= $query->row;
		}
		if(!empty($results)) {
			return true;
		}
		return false;
	}
	
	public function isCheckInvoiceId($invoice_no) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "order WHERE company_id = '".$company_id."' AND invoice_no = '" .$invoice_no . "'");
		$results	= $query->row;
		if(!empty($results)) {
			return false;
		}
		return true;
	}
	
	public function getInvoiceDetails($invoice_no) {
		$company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "order WHERE company_id = '".$company_id."' AND invoice_no = '" .$invoice_no . "'");
		$results	= $query->row;
		if(!empty($results)) {
			return $results;
		}
	}
	
	public function cancelOrder($order_id) 
	{
		$company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		$sql        = "SELECT DISTINCT * FROM " . DB_PREFIX . "order_status WHERE company_id = '".$company_id."' AND name='Voided'";
		$query      = $this->db->query($sql);
		$results	= $query->row;
		$ordersts   = $results['order_status_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "order WHERE company_id = '".$company_id."' AND (order_id = '" . (int)$order_id . "' OR invoice_no = '" . (int)$order_id . "') AND order_status_id!='".$ordersts."'");
		$results	= $query->row;
		if(!empty($results)) {
			$order_id	= $results['order_id'];
			$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '".$ordersts."', date_modified = curdate() WHERE order_id = '" . (int)$order_id . "'");
			$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");
			foreach ($order_product_query->rows as $order_product) {
				$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$order_product['product_id'] . "'");
				$results	= $query->row;
				if(!empty($results)) {
					$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity + " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
				}
			}
			return true;
		}
		return false;
	}
	
	public function addOrder($data) {
		
		$company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data["attachedcustomer"];
		$invoice_no	= $this->getLastIncrementId();
		$invoice_no	= (int)$invoice_no['last_id'];
		$lastInvoiceNo	= $invoice_no + 1;
		$customer_id = $this->session->data['user_id'];
		//$data['payment_details'] = $data['card_amount'].','.$data['cash_amount'].','.$data['visa_amount'];
		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "order` SET 
					invoice_prefix = '" . $this->db->escape($data['invoice_prefix']) . "'
					, invoice_no = '" . (int)$invoice_no . "'
					, company_id = '" . (int)$company_id . "'
					, customer_id = '" . (int)$customer_id . "'
					, user_id = '".(int)$userId."'
					, total = '" . (float)$data['total'] . "'
					, tendar = '" . (float)$data['tendar'] . "'
					, changes = '" . (float)$data['changes'] . "'
					, payment_code = '" . $this->db->escape($data['payment_code']) . "'
					, payment_method = '" . $this->db->escape($data['payment_method']) . "'
					, silp_number = '" . $this->db->escape($data['silp_number']) . "'
					, language_id = '" . (int)$data['language_id'] . "'
					, currency_id = '" . (int)$data['currency_id'] . "'
					, currency_code = '" . $this->db->escape($data['currency_code']) . "'
					, currency_value = '" . (float)$data['currency_value'] . "'
					, ip = '" . $this->db->escape($data['ip']) . "'
					, forwarded_ip = '" . $this->db->escape($data['forwarded_ip']) . "'
					, payment_details = '" . $this->db->escape($data['payment_details']) . "'
					, date_added = curdate(), date_modified = curdate()");

		$order_id = $this->db->getLastId();
		
		$_paymentDetails		= $this->getAllPaymode($data);
		if (count($_paymentDetails) > 1) {
		    foreach ($_paymentDetails as $paymentDetail) {
				if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
				    $paidAmount = $data['cash_amount'];
				} else {
				    $paidAmount = $data['card_amount'];
				}
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_method SET 
					    order_id = '" . (int)$order_id . "'
					    , payment_id = '" . (int)$paymentDetail['payment_id'] . "'
						, payment_method_name = '" . $paymentDetail['payment_name'] . "'
					    , paid_amount = '" . (float)$paidAmount . "'");
			}					
		} else {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_method SET 
					order_id = '" . (int)$order_id . "'
					, payment_id = '" . (int)$data['payment_method'] . "'
					, payment_method_name = '" . $_paymentDetails[0]['payment_name'] . "'
					, paid_amount = '" . (float)$data['tendar'] . "'");
		}

		foreach ($data['products'] as $product) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_product SET 
					order_id = '" . (int)$order_id . "'
					, product_id = '" . (int)$product['product_id'] . "'
					, name = '" . $this->db->escape($product['name']) . "'
					, quantity = '" . (int)$product['quantity'] . "'
					, price = '" . (float)$product['price'] . "'
					, total = '" . (float)$product['total'] . "'
					, tax = '" . (float)$product['tax'] . "'
					, reward = '" . (int)$product['reward'] . "'");

			//$order_id = $this->db->getLastId();
		}

		foreach ($data['totals'] as $total) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "order_total SET 
				order_id = '" . (int)$order_id . "'
				, code = '" . $this->db->escape($total['code']) . "'
				, title = '" . $this->db->escape($total['title']) . "'
				, text = '" . $this->db->escape($total['text']) . "'
				, `value` = '" . (float)$total['value'] . "'
				, sort_order = '" . (int)$total['sort_order'] . "'");
		}	
		
		$this->db->query("UPDATE " . DB_PREFIX . "entity_increment SET increment_last_id = '".$lastInvoiceNo."' WHERE entity_type_id = '1' AND company_id = '" . (int)$company_id . "'");
		
		if($this->config->get('config_order_status_id')) {
			$order_status_id = $this->config->get('config_order_status_id');
		} else {
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "order_status WHERE company_id = '".$company_id."' AND name='Complete'";
			$query = $this->db->query($sql);
			$results	= $query->row;
			if(!empty($results)) {
				$order_status_id	= $results['order_status_id'];	
			}
		}
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '".$order_status_id."', date_modified = curdate() WHERE order_id = '" . (int)$order_id . "'");
		$order_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_product WHERE order_id = '" . (int)$order_id . "'");

		foreach ($order_product_query->rows as $order_product) {
			$this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = (quantity - " . (int)$order_product['quantity'] . ") WHERE product_id = '" . (int)$order_product['product_id'] . "' AND subtract = '1'");
		}
		
		$this->db->query("UPDATE `" . DB_PREFIX . "company` SET total_transaction = (total_transaction + 1) WHERE company_id = '" . (int)$company_id . "'");
		
		return $order_id;
		//return $lastId;
	}
	
	public function getTotalInvoiceByUser() {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$sql = "SELECT COUNT(*) AS total,
				 (SELECT COUNT(*) FROM  " . DB_PREFIX . "order_product AS op WHERE e.order_id = op.order_id) AS total_items , ot.text AS net_amount,
				 p.payment_name AS paymode FROM " . DB_PREFIX . "order AS e 
				 LEFT JOIN " . DB_PREFIX . "order_total AS ot ON (e.order_id = ot.order_id AND ot.code='total') 
				 LEFT JOIN " . DB_PREFIX . "payment AS p ON (e.payment_method = p.payment_id)
				 WHERE e.company_id = '".$company_id."' AND e.customer_id = '" .$user_id . "' AND DATEDIFF(e.date_added, curdate()) = 0";
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
	
	public function getAllInvoiceByUser($data) {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$sql = "SELECT e.*, e.invoice_no AS invoice_no,e.date_added AS order_time,e.tendar AS tendar,u.username AS cashier,e.changes AS changes,
				 (SELECT COUNT(*) FROM  " . DB_PREFIX . "order_product AS op WHERE e.order_id = op.order_id) AS total_items , ot.text AS net_amount,
				 p.payment_name AS paymode FROM " . DB_PREFIX . "order AS e 
				 LEFT JOIN " . DB_PREFIX . "order_total AS ot ON (e.order_id = ot.order_id AND ot.code='total')
				 LEFT JOIN " . DB_PREFIX . "payment AS p ON (e.payment_method = p.payment_id)
				 LEFT JOIN " . DB_PREFIX . "user AS u ON (u.user_id = e.customer_id)
				 WHERE e.company_id = '".$company_id."' AND e.customer_id = '" .$user_id . "' AND DATEDIFF(e.date_added, curdate()) = 0";		 
				 
		if (isset($data['order_id'])) {
			$sql .= " AND (e.order_id = '".(int)$data['order_id']."' OR e.invoice_no = '".(int)$data['order_id']."')";
		} 	 
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 5;
			}
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;
		}
		return false;
	}
	
	public function getAllPaymode($data=array()) {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$string     = '"'. implode('","', explode(',', $data['payment_method'])) .'"';
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "payment WHERE company_id = '".$company_id."' AND status = '1'";
		if(isset($data['payment_method']) && !empty($data['payment_method'])) {
			$sql .= " AND (payment_id IN (".$string.") OR (LOWER(payment_name) IN (".$string.") ))";
		}
		$sql .= " ORDER BY position ASC";
		$query = $this->db->query($sql);
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	
	public function getTotalByOrder($data=array()) {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$sql = "SELECT title,text,value FROM " . DB_PREFIX . "order_total";
		if(isset($data['order_id'])) {
			$sql .= " WHERE order_id=".$data['order_id'];
		}
		$sql .= " ORDER BY sort_order ASC";
		$query = $this->db->query($sql);
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	
	public function updatePaymentMethod($data) {
		//print_r($data); exit;
		$user_id	= $this->session->data['user_id'];
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "order SET 
		 	payment_method = '" . $this->db->escape($data['payment_method']) . "'
			, payment_code = '" . $this->db->escape($data['payment_code']) . "'
			, silp_number = '" . $this->db->escape($data['silp_number']) . "'
			, tendar = '" . $this->db->escape($data['tendar']) . "'
			, changes = '" . $this->db->escape($data['changes']) . "'
			, company_id = '".$company_id."'
			, date_modified = curdate()
		WHERE  order_id = '" . (int)$this->db->escape($data['order_id']) . "'");
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "order_payment_method WHERE order_id = '" . (int)$this->db->escape($data['order_id']) . "'");
		$_paymentDetails		= $this->getAllPaymode($data);
		if (count($_paymentDetails) > 1) {
		    foreach ($_paymentDetails as $paymentDetail) {
				if(strtolower(trim($paymentDetail['payment_name']))=='cash') {
				    $paidAmount = $data['cash_amount'];
				} else {
				    $paidAmount = $data['card_amount'];
				}
				$this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_method SET 
					    order_id = '" . (int)$this->db->escape($data['order_id']) . "'
					    , payment_id = '" . (int)$paymentDetail['payment_id'] . "'
						, payment_method_name = '" . $paymentDetail['payment_name'] . "'
					    , paid_amount = '" . (float)$paidAmount . "'");
			}					
		} else {
		    $this->db->query("INSERT INTO " . DB_PREFIX . "order_payment_method SET 
					order_id = '" . (int)$this->db->escape($data['order_id']) . "'
					, payment_id = '" . (int)$data['payment_method'] . "'
					, payment_method_name = '" . $_paymentDetails[0]['payment_name'] . "'
					, paid_amount = '" . (float)$data['tendar'] . "'");
		}
		
		return true;
	}
	
	public function addHoldProduct($data) {
	
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "cache_cart WHERE bill_no = '" . (int)$data['bill_no'] . "' AND company_id = '".$company_id."' AND user_id = '" .$user_id . "' AND DATEDIFF(date_added, curdate()) = 0");
		$results	= $query->row;
		if(!empty($results)) {
			return false;
		}
		foreach ($data['products'] as $product) { 
			$this->db->query("INSERT INTO " . DB_PREFIX . "cache_cart SET 
					  bill_no = '" . (int)$product['bill_no'] . "'
					, product_id = '" . (int)$product['product_id'] . "'
					, quantity = '" . (int)$product['quantity'] . "'
					, discount = '" . (int)$product['discount'] . "'
					, user_id = '".$user_id."'
					, company_id = '".$company_id."'
					, date_added = curdate()
					, date_modified = curdate()");
		}
		return true;
	}
	
	public function getHoldProduct($data) {
	
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cache_cart WHERE bill_no = '" . (int)$data['bill_no'] . "' AND company_id = '".$company_id."' AND user_id = '" .$user_id . "' AND DATEDIFF(date_added, curdate()) = 0");
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;	
		}
		return false;
	}
	
	public function getProductsByOrder($order_id) {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		$sql = "SELECT op.*,p.sku FROM " . DB_PREFIX . "order_product AS op 
		LEFT JOIN " . DB_PREFIX . "product AS p ON (op.product_id = p.product_id) WHERE op.order_id='".$order_id."'";
		$query = $this->db->query($sql);
		return $query->rows;
			
	}
	
	public function getAllDiscountMode($data=array()) {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "discount_mode";
		if(isset($data['discount_status_id'])) {
			$sql .= " AND discount_status_id=".$data['discount_status_id'];
		}
		$query = $this->db->query($sql);
		$results	= $query->rows;
		if(!empty($results)) {
			return $results;
		}
	}
	
	public function getTotalsCollection() {
	
		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();
		$this->load->model('pos/addcart');
		
		$sort_order = array(); 
		
		$results = $this->model_pos_addcart->getExtensions('total');
		
		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}
		
		array_multisort($sort_order, SORT_ASC, $results);
		
		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				//if($result['code']!='discount'){
					$this->load->model('total/' . $result['code']);
					$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
				//}
			}

			$sort_order = array(); 

			foreach ($total_data as $key => $value) {
				$sort_order[$key] = $value['sort_order'];
			}

			array_multisort($sort_order, SORT_ASC, $total_data);
		}
		$this->session->data['totals']	= $total_data;
		return $total_data;
	}
	
	public function getDefaultPaymentMode() {
	
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "payment WHERE company_id = '".$company_id."' AND payment_name='cash'";
		$query = $this->db->query($sql);
		$results	= $query->row;
		if(!empty($results)) {
			return $results;
		}	
	}
	
	public function getOrderPayment($orderId, $paymentId) {
		$sql = "SELECT * FROM " . DB_PREFIX . "order_payment_method WHERE order_id = '".$orderId."' AND payment_id = '".$paymentId."'";
		$query = $this->db->query($sql);
		$results	= $query->row;
		if(!empty($results)) {
			return $results;
		}	
	}
	
	public function getProductByName($data = array())
	{
		$company_id	= $this->session->data['company_id'];
		$sql = "SELECT p.product_id, p.sku, p.quantity, p.price, p.status, p.image, pd.name FROM " . DB_PREFIX . "product p 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_company pc ON (p.product_id = pc.product_id)
				WHERE pc.company_id = '".$company_id."' AND p.status = '1' AND p.date_available <= curdate() ";		
		if(!empty($data['filter_name'])) {
			$sql .= " AND (";
			$implode = array();
			$words = explode(' ', trim(preg_replace('/\s\s+/', ' ', $data['filter_name'])));
			foreach ($words as $word) {
				$implode[] = "pd.name LIKE '%" . $this->db->escape($word) . "%'";
			}			
			if ($implode) {
				$sql .= " " . implode(" AND ", $implode) . "";
			}
			$sql .= " OR pd.description LIKE '%" . $this->db->escape($data['filter_name']) . "%'";			
			$sql .= " OR LCASE(p.sku) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";			
			$sql .= ")";			
		}
		$sql .= " GROUP BY p.product_id";				
		$query        = $this->db->query($sql);	
		$results	  = $query->rows;
		if(!empty($results)) {
			return $results;
		}
		
	}
	
	public function getProductBarcodes($product_id) {
		$product_department_data = array();
		$query = $this->db->query("SELECT barcode FROM " . DB_PREFIX . "product_to_barcode WHERE product_id = '" . (int)$product_id . "'");
		$result = array();
		foreach ($query->rows as $key => $value){
		    $result[$key] = $value['barcode'];
		}
		return $result;
	}
	
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' AND CURDATE() >= date_start AND CURDATE() <= date_end ORDER BY priority, price");
		return $query->row;
	}
	
	public function dogetComputerTotal($pay_mode='') {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		
		if (!empty($pay_mode)) {
			$sum = ", (SUM(p.paid_amount) - SUM(e.changes)) AS cashTotal";
		} else {
		    $sum = ", SUM(p.paid_amount) AS cardTotal";
		}		 
		
		$sql = "SELECT p.*".$sum." FROM " . DB_PREFIX . "order AS e
				LEFT JOIN " . DB_PREFIX . "order_payment_method AS p ON (e.order_id = p.order_id)
				WHERE e.company_id = '".$company_id."' AND e.customer_id = '" .$user_id . "' AND DATEDIFF(e.date_added, curdate()) = 0";
		if (!empty($pay_mode)) {
			$sql .= " AND p.payment_method_name = '".$pay_mode."'";
		} else {
		    $sql .= " AND p.payment_method_name != 'CASH'";
		} 
		$query = $this->db->query($sql);
		$results	= $query->row;
		if(!empty($results)) {
			return $results;
		}
		return false;
	}
	
	public function dogetExpensesTotal() {
		$company_id	= $this->session->data['company_id'];
		$user_id	= $this->session->data['user_id'];
		
		$sql = "SELECT SUM(expense_amount) AS expenseTotal FROM " . DB_PREFIX . "expenses 
				WHERE company_id = '".$company_id."' AND customer_id = '" .$user_id . "' AND DATEDIFF(date_added, curdate()) = 0";
					
		$query = $this->db->query($sql);
		$results	= $query->row;
		if(!empty($results)) {
			return $results;
		}
		return false;
	}
	
	public function addTerminalSales($data) {		
		$company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];
		
		$query = $this->db->query("SELECT terminal_sales_id FROM " . DB_PREFIX . "terminal_sales WHERE company_id = '".$company_id."' AND customer_id = '" .$userId . "' AND DATEDIFF(date_added, curdate()) = 0");
		$results	= $query->row;
		
		if (!empty($results)) {
			$sql = "UPDATE ";
		} else {
		    $sql = "INSERT INTO ";
		}
		
		$sql .= "`" . DB_PREFIX . "terminal_sales` SET 
					customer_id = '" . (int)$userId . "'
					, company_id = '" . (int)$company_id . "'
					, computer_cash = '" . (float)$data['computer_cash'] . "'
					, computer_card = '" . (float)$data['computer_card'] . "'
					, computer_expenses = '" . (float)$data['computer_expenses'] . "'
					, manual_cash = '" . (float)$data['manual_cash'] . "'
					, manual_card = '" . (float)$data['manual_card'] . "'
					, manual_expenses = '" . (float)$data['manual_expenses'] . "'
					, computer_grant_total = '" . (float)$data['computer_grant_total'] . "'
					, manual_grant_total = '" . (float)$data['manual_grant_total'] . "'";
		
		if (!empty($results)) {
			$sql .= " , date_modified = curdate() WHERE terminal_sales_id = '".(int)$results['terminal_sales_id']."' ";
		} else {
		    $sql .= " , date_added = curdate(), date_modified = curdate() ";
		}
		$this->db->query($sql);
		if (!empty($results)) {
			$terminal_id = $results['terminal_sales_id'];
		} else {
		    $terminal_id = $this->db->getLastId();
		}
		
		if (!empty($this->session->data['settlement_cash'])) {
		    $filteredArray = array();
			$totalValue = 0;
			$filteredArray = array_filter($this->session->data['settlement_cash']);
			$delete_ids = implode(',', array_keys($filteredArray));
			foreach($filteredArray as $key=>$cash) {
				$totalValue = (float)$key * (float)$cash;				
				$query_sale = $this->db->query("SELECT terminal_sales_payment_id FROM " . DB_PREFIX . "terminal_sales_payment WHERE terminal_sales_id = '".(int)$terminal_id."' AND cash = '".(float)$key."'");
				$results_sales	= $query_sale->row;				
				if (!empty($results_sales)) {
					$sql_query = "UPDATE ";
				} else {
					$sql_query = "INSERT INTO ";
				}				
				$sql_query .= "`" . DB_PREFIX . "terminal_sales_payment` SET 
					terminal_sales_id = '" . (int)$terminal_id . "'
					, cash = '" . (float)$key . "'
					, number_cash_type = '" . (float)$cash . "'
					, sales_total = '" . (float)$totalValue . "'";
				
				if (!empty($results_sales)) {
					$sql_query .= " WHERE terminal_sales_payment_id = '".(int)$results_sales['terminal_sales_payment_id']."' ";
				}				
				$this->db->query($sql_query);
			}
			$qry = "DELETE FROM " . DB_PREFIX . "terminal_sales_payment WHERE terminal_sales_id = '".(int)$terminal_id."' AND (cash NOT IN (".$delete_ids.")) ";
			$this->db->query($qry);
		}
		return $terminal_id;
	}
	
	public function dogetTermialSales() {
	    $company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];		
		$query = $this->db->query("SELECT tp.cash, tp.number_cash_type FROM " . DB_PREFIX . "terminal_sales AS t
							 	   LEFT JOIN " . DB_PREFIX . "terminal_sales_payment AS tp ON (t.terminal_sales_id = tp.terminal_sales_id)
								   WHERE t.company_id = '".$company_id."' AND t.customer_id = '" .$userId . "' AND DATEDIFF(t.date_added, curdate()) = 0");
		$results	= $query->rows;		
		return $results;		
	}
	
	public function doInsertOwnExpenses($data) {	
		$company_id	= $this->session->data['company_id'];
		$userId	= $this->session->data['user_id'];		
		$this->db->query("INSERT INTO `" . DB_PREFIX . "expenses` SET 
					customer_id = '" . (int)$userId . "'
					, company_id = '" . (int)$company_id . "'
					, expense_amount = '" . (float)$data['expense_amount'] . "'
					, description = '" . $this->db->escape($data['description']) . "'
					, date_added = curdate()");
		$expense_id = $this->db->getLastId();		
		return $expense_id;
	}

	public function getLastOrderProduct()
	{		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order ORDER BY order_id DESC LIMIT 1");

		return $query->rows;
	}

	public function clear() 
	{
		unset($this->session->data['cart']);
		unset($this->session->data['totals']);
		unset($this->session->data['tender']);
		unset($this->session->data['last_product']);
		unset($this->session->data['order_id']);
		unset($this->session->data['discount']);
		unset($this->session->data['discount_mode']);
		unset($this->session->data['is_manually']);
		unset($this->session->data['is_exchange']);
		unset($this->session->data['attachedcustomer']);
		unset($this->session->data['update_price']);
	}

	public function getQuoteID() 
	{
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT  MAX(id) as bill FROM " . DB_PREFIX . "cache_cart");
		$results	= $query->row;
		if(!empty($results)){
			$bill_no = $results["bill"];
		} else{
			$bill_no = 1;
		}
        $quote = $company_id.date('mdHis').$bill_no;
		return $quote;
	}

}
?>