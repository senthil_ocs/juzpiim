<?php
class ModelPosAddcart extends Model {
	public function getProductInformation($code) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT p.*, pd.name AS name, ss.name AS stock FROM " . DB_PREFIX . "product p 
		LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_to_company p2c ON (p.product_id = p2c.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_special p2s ON (p.product_id = p2s.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id) 
		LEFT JOIN " . DB_PREFIX . "stock_status ss ON (p.stock_status_id = ss.stock_status_id) 
		WHERE (p.sku = '" .$code . "' OR p2b.barcode=  '" . $code . "')
			AND p2c.company_id = '" . (int)$company_id . "' 
			AND p.status = '1'");
		if ($query->num_rows) {
			return $query->row;
		}
		return false;
	}
	
	public function getProductDetails($product_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT p.*, pd.name AS name, ss.name AS stock FROM " . DB_PREFIX . "product p 
		LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_to_company p2c ON (p.product_id = p2c.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_special p2s ON (p.product_id = p2s.product_id) 
		LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id) 
		LEFT JOIN " . DB_PREFIX . "stock_status ss ON (p.stock_status_id = ss.stock_status_id) 
		WHERE (p.product_id = '".(int)$product_id."') AND p2c.company_id = '" . (int)$company_id . "' AND p.status = '1'");
		if ($query->num_rows) {
			return $query->row;
		}
		return false;
	}
	
	function getExtensions($type) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE type = '" . $this->db->escape($type) . "'");

		return $query->rows;
	}
	
	function getExtensionsByPosition($type, $position) {
		$module_data = array();
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE type = '" . $this->db->escape($type) . "'");
		
		foreach ($query->rows as $result) {
			if ($this->config->get($result['key'] . '_status') && ($this->config->get($result['key'] . '_position') == $position)) {
				$module_data[] = array(
					'code'       => $result['key'],
					'sort_order' => $this->config->get($result['key'] . '_sort_order')
				);
			}
		}
		
		$sort_order = array(); 
	  
		foreach ($module_data as $key => $value) {
      		$sort_order[$key] = $value['sort_order'];
    	}

    	array_multisort($sort_order, SORT_ASC, $module_data);
    	
    	return $module_data;
	}
	
	/*function updatePrice($product_id, $product_price) {
	    
		$this->db->query("UPDATE " . DB_PREFIX . "product SET price = ".(float)$product_price." WHERE product_id = '" . (int)$product_id . "' ");
		return true;
		
	}*/
}
?>