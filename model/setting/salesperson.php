<?php
class ModelSettingSalesperson extends Model {

	public function getTotalSalesman($data){

      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "sales_person ";
		$query = $this->db->query($sql); 
		return $query->row['total'];
	} 

	public function routeList(){
		$sql = "SELECT name,id FROM " . DB_PREFIX . "route";
		$query = $this->db->query($sql)->rows;
		return $query;
	}

	public function setPriority($post_order_ids){
		$post_order = isset($_POST["post_order_ids"]) ? $_POST["post_order_ids"] : [];
		$this->db->query("UPDATE " . DB_PREFIX . "sales_do_header SET sort_id =99+id where status='Delivered'");

		if(count($post_order)>0){
			for($sort_id = 0; $sort_id < count($post_order); $sort_id++){
				$query = $this->db->query("UPDATE " . DB_PREFIX . "sales_do_header SET sort_id = '".($sort_id+1)."' WHERE do_no = '".$post_order[$sort_id]."'");
			}
			echo true; 
		}else{
			echo false; 
		}
	}

	public function setScheduleStatus($salerOrderId,$checkStatus){
		
		$currentDate = date("Y-m-d H:i:s");
		$date   = $checkStatus=='Assigned' ? date('Y-m-d H:i:s') : '';
		$userId = $checkStatus=='Assigned' ? $this->session->data['user_id'] : '';
		$this->db->query("UPDATE " . DB_PREFIX . "sales_do_header SET status = '".$checkStatus."', assignedon='".$date."',assignedby='".$userId."' WHERE do_no = '".$salerOrderId."'");
		// $this->db->query("UPDATE " . DB_PREFIX . "sales_invoice_header SET delivery_status='".$checkStatus."' where id='".$salerOrderId."' ");
	}

	public function getSalesman($data)
	{
		if ($data) {
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "sales_person WHERE id != '' ";
			if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}
			if ($data['filter_phone']){
			$sql.= " AND phone LIKE '" . $this->db->escape($data['filter_phone']) . "%'"; 
			}
			$query = $this->db->query($sql)->rows;
			foreach ($query as $key => $value) {
				$route_id = $value['route_id'];
				$sql2 = $this->db->query("SELECT name FROM " . DB_PREFIX . "route where id = '".$route_id."'")->row;
				$query[$key]['route'] = $sql2['name'];
			}
			return $query;
		} 	
	}

	public function getTotalSchedule($data){

		$filter_date = date('Y-m-d',strtotime(str_replace('/','-',$data['filter_date'])));
      	$sql = "SELECT COUNT(dv.sales_order_id) AS total FROM ".DB_PREFIX."delivery as dv LEFT JOIN ".DB_PREFIX."sales_invoice_header as sh ON dv.sales_order_id=sh.invoice_no  WHERE sh.invoice_date='".$filter_date."' GROUP BY dv.sales_order_id ORDER BY dv.sort_id ASC";
		$query = $this->db->query($sql);
		// echo $query->row['total']; die;
		return $query->row['total'];
	}
	 
	public function getSchedule($data)
	{
		$filter_date = changedates($data['filter_date']);
		$sql = "SELECT doh.*,c.name as name,sih.invoice_date as invoice_date FROM ".DB_PREFIX."sales_do_header as doh LEFT JOIN ".DB_PREFIX."customers as c ON doh.customer_id = c.customercode LEFT JOIN ".DB_PREFIX."sales_invoice_header as sih ON sih.invoice_no=doh.sales_transaction_no where doh.deliveryman_id='".$data['salesman_id']."' ";
		if($filter_date !=''){
			$sql .= "AND sih.invoice_date ='".$filter_date."' ";
		}
		$sql .="order by doh.sort_id asc,doh.status desc";
		// echo $sql; die;
		$query = $this->db->query($sql)->rows;
		return $query; 	
	}
	public function getScheduleCustName($data){

		$query = $this->db->query("SELECT name FROM ".DB_PREFIX."sales_person WHERE id='".$data['salesman_id']."'");
		return $query->row['name']; 	
	}
	public function getInvoiceDate($invoice_no){
		$query = $this->db->query("SELECT invoice_date FROM ".DB_PREFIX."sales_invoice_header WHERE invoice_no='".$invoice_no."'");
		return $query->row['invoice_date']; 	
	}
	public function getsalesDetailsCount($do_no){
		$sql = $this->db->query("SELECT count(*) as total_items FROM " . DB_PREFIX . "sales_do_details where do_no = '".$do_no."' group by do_no")->row;
		return $sql['total_items'];
	}
	public function addSalesman($data)
	{
		/* password, route_id, ,'".$data['password']."' ,'".$data['route_id']."' */
		$currentDate = date("Y-m-d H:i:s");
		$sql = "INSERT INTO " . DB_PREFIX . "sales_person (id, name, email,  phone, address1, address2, city, state, zip_code, status, addedon, addedby, modifiedon, modifiedby) VALUES ('','".$this->db->escape($data['name'])."','".$this->db->escape($data['email'])."','".$this->db->escape($data['phone'])."','".$this->db->escape($data['address1'])."','".$this->db->escape($data['address2'])."','".$this->db->escape($data['city'])."','".$this->db->escape($data['state'])."','".$this->db->escape($data['zipcode'])."','".$this->db->escape($data['status'])."','".$currentDate."',1,'".$currentDate."',1)";
		$query = $this->db->query($sql);
		$salesman_id = $this->db->getLastId();
		return $salesman_id;
	}
	public function editSalesman($data,$salesman_id)
	{
		/*password        = '".$data['password']."',route_id   		= '".$data['route_id']."',*/
		$currentDate = date("Y-m-d H:i:s");
		$query = $this->db->query("UPDATE " . DB_PREFIX . "sales_person SET 
									status 			= '".$this->db->escape($data['status'])."',
									name		    = '".$this->db->escape($data['name'])."',
									email           = '".$this->db->escape($data['email'])."',
									phone    		= '".$this->db->escape($data['phone'])."',
									address1 		= '".$this->db->escape($data['address1'])."',
									address2 		= '".$this->db->escape($data['address2'])."',
									city 			= '".$this->db->escape($data['city'])."',
									state 			= '".$this->db->escape($data['state'])."',
									zip_code 		= '".$this->db->escape($data['zipcode'])."',
									modifiedby      = '1',
									modifiedon		= '".$currentDate."' WHERE id = '".$salesman_id."'");
	}
	public function getSalesmanData($salesman_id)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "sales_person WHERE id = '" . (int)$salesman_id . "'");
		return $query->row;
	}
	public function getsalesmanphone($phone,$salesman_id ='')
	{
		$query = "SELECT count(*) as total FROM " . DB_PREFIX . "sales_person WHERE phone ='" . $phone . "'";
		if($salesman_id != ''){
			$query .= "AND id != '".$salesman_id."'";
		}
		$query = $this->db->query($query);
		return $query->row['total'];
	}
	public function getsalesmanemail($email,$salesman_id ='')
	{
		$query = "SELECT count(*) as total FROM " . DB_PREFIX . "sales_person WHERE email ='" . $email . "'";
		if($salesman_id != ''){
			$query .= "AND id != '".$salesman_id."'";
		}
		$query = $this->db->query($query);
		return $query->row['total'];
	}
}
?>