<?php
class ModelSettingCustomers extends Model {

	public function getTotalCustomers($data = array())
	{
		$company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customers WHERE customercode!= ''";
		
		$sort_data = array(
			'name',
			'email',				
		);
       
        if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR primary_email LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
        }

		// if ($data['filter_name']){
		// 	$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ||  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
		// }

		if ($data['filter_name']){
			$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
		}

		if ($data['filter_email']){
			$sql.= " AND email LIKE '" . $this->db->escape($data['filter_email']) . "%'"; 
		}
		if ($data['filter_Type']!="") {	
			$sql .= "AND customer_type = '" . $this->db->escape($data['filter_Type']) . "'";
		}
		if ($data['xero_button'] == "1") {	
			$sql .= "AND xero_id IS NOT NULL ";
		}else if ($data['xero_button'] == "0"){
			$sql .= "AND xero_id IS NULL ";
		}
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$data['filter_date_from'] = changeDates($data['filter_date_from']);
			$data['filter_date_to']   = changeDates($data['filter_date_to']);
			$sql .= " AND createdon between  '".$data['filter_date_from']."' AND '".$data['filter_date_to']."'";
		}
		/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY customercode";	
		}
		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}*/
			
		
		//echo "$sql";	 exit;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getCustomers($data = array())
	{		
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			//$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE company_id = '" . (int)$company_id . "'";


			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customers where customercode!='' ";
			$sort_data = array(
				'name',
				'email',				
			);

			 if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR primary_email LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
             }
			/*if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ||  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}*/
			if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}

			if ($data['filter_email']){
				$sql.= " AND email LIKE '%" . $this->db->escape($data['filter_email']) . "%'"; 
			}
			if ($data['filter_Type']!="") {	
				$sql .= "AND customer_type = '" . $this->db->escape($data['filter_Type']) . "'";
			}
			if ($data['xero_button'] == "1") {	
				$sql .= "AND xero_id IS NOT NULL ";
			}else if ($data['xero_button'] == "0"){
				$sql .= "AND xero_id IS NULL ";
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$data['filter_date_from'] = changeDates($data['filter_date_from']);
				$data['filter_date_to']   = changeDates($data['filter_date_to']);
				$sql .= " AND createdon between  '".$data['filter_date_from']."' AND '".$data['filter_date_to']."'";
			}
			$sql .= " ORDER BY customercode DESC";
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			// echo $sql; die;
			$query = $this->db->query($sql);

			return $query->rows;
			
		} else {
			
				$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customers ORDER BY customercode DESC");
				$customer_data = $query->rows;
				return $customer_data;	
		}	
	}

	public function getCustomer($customer_id)
	{
		$company_id	= $this->session->data['company_id'];
		$query 		= $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE customercode = '" .$customer_id . "'");
		// echo "SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE customercode = '" . $customer_id . "' AND company_id = '" . (int)$company_id . "'"; die;
		return $query->row;
	}
	public function getCustomerMobile($mobile)
	{
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE  mobile = '" . $mobile . "' ");
		return $query->row;
	}
	public function getCustomerZipcode($zipcode)
	{
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customers WHERE  zipcode = '" . $zipcode . "' ");	
		return $query->row;
	}
	public function getCustomerUniqueId()
	{
		//echo "SELECT customercode FROM " . DB_PREFIX . "customers ORDER BY customercode DESC LIMIT 1";
		$query = $this->db->query("SELECT TOP 1 customercode FROM " . DB_PREFIX . "customers ORDER BY customercode DESC");
		//$query = $this->db->query("SELECT customercode FROM " . DB_PREFIX . "customers ORDER BY customercode DESC OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY");
		//printArray($query); exit;
		return $query->row;
	}

	public function addCustomer($data)
	{
	   $company_id	= $this->session->data['company_id'];	  
	   if($data['do_not']){
	   	  $do_nots = serialize($data['do_not']);
	   }
	   $data['dummyvalue']='1';
		if($data['birth_date'] !=''){
			$data['birth_date'] = changeDate($data['birth_date']);
		}
	    if($data['birth_date']=='--'){
		   $data['birth_date'] = date('d-m-Y');
	    }
	    $date = date('Y-m-d H:i:s');
	    $lastCustId 		   = $this->getCustLastId();
    	$data['customer_code'] = 'CU'.str_pad($lastCustId + 1, 4, '0', STR_PAD_LEFT);

		$sql = "INSERT INTO " . DB_PREFIX . "customers (cust_code,
				company_id,
				customer_type,
				status,
				name,
				customer_title,
				birth_date,
				home,
				work,
				location_code,
				mobile,
				address1,
				address2,
				city,
				zipcode,
				email,
				address3,
				membercardno,
				total_points,
				total_redemption,
				last_earning_points,
				state,
				notes,
				company,
				registration_no,
				currency_code,
				tax_allow,
				website,
				fax,
				term_id,
				country,
				createdon) VALUES (
						'".$data['customer_code']."',
						'".$company_id."',
						'".$this->db->escape($data['customer_type'])."',
						'".$this->db->escape($data['status'])."',
						'".$this->db->escape($data['name'])."',
						'".$this->db->escape($data['customer_title'])."',
						'".$this->db->escape($data['birth_date'])."',
						'".$this->db->escape($data['home'])."',
						'".$this->db->escape($data['work'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['contacts'])."',
						'".$this->db->escape($data['address1'])."',
						'".$this->db->escape($data['address2'])."',
						'".$this->db->escape($data['city'])."',
						'".$this->db->escape($data['zipcode'])."',
						'".$this->db->escape($data['email'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['dummyvalue'])."',
						'".$this->db->escape($data['state'])."',
						'".$this->db->escape($data['notes'])."',
						'".$this->db->escape($data['company'])."',
						'".$this->db->escape($data['registration_no'])."',
						'".$this->db->escape($data['currency_code'])."',
						'".$this->db->escape($data['tax_allow'])."',
						'".$this->db->escape($data['website'])."',
						'".$this->db->escape($data['fax'])."',
						'".$this->db->escape($data['term_id'])."',
						'".$this->db->escape($data['country'])."',
						'".$date."')";
		// echo $sql; die; 
		$res = $this->db->query($sql);
		if(!$res){
			header('Location: '.HTTP_SERVER.'index.php?route=setting/customers/insert&token=' . $this->session->data['token'].'&errortrans_no='.$data['
				customer_code']);						
				exit;
		}
		$customer_id = $this->db->getLastId();
		$shippingSql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,address2,zip,contact_no,notes,isdefault) VALUES('".$customer_id."','".$this->db->escape($data['customer_code'])."','".$this->db->escape($data['address1'])."','".$this->db->escape($data['address2'])."','".$this->db->escape($data['zipcode'])."','".$this->db->escape($data['contacts'])."','".$this->db->escape($data['notes'])."','1') ";
		$res = $this->db->query($shippingSql);
		// $this->db->query("UPDATE tbl_setting set value=value+1 where `key`='next_member_no'");
		
		if(in_array($this->session->data['location_code'],API_LOCATION) &&  $customer_id){
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX . "customers  WHERE customercode = '" . $customer_id . "'");
			$customer_data = array(
				"firstname"		=> $query->row['name'],
				"lastname"		=> $query->row['name'],
				"email"			=> $query->row['primary_email'],
				"telephone"		=> $query->row['mobile'],
				"password"		=> '',
				"company"		=> $query->row['company'],
				"address_1"		=>  $query->row['address1'],
				"unitno"		=> $query->row['address2'],
				"postcode"		=>$query->row['zipcode'],
			);
			$res = curlpost('create_customer',$customer_data);	
		}
		return $customer_id;
	}

	public function editCustomer($data,$customer_id)
	{
		$company_id	= $this->session->data['company_id'];
		 if($data['do_not']){
	   	  	$do_nots = serialize($data['do_not']);
	   	}
	   	$birth_date = '';
	   	if(isset($data['birth_date'])){
	   		$birth_date = date('Y-m-d',strtotime($data['birth_date']));
	   	}
	   	// echo $data['birth_date']; die;
		$sql = "UPDATE " . DB_PREFIX . "customers SET cust_code = '".$this->db->escape($data['customer_code'])."',
									customer_type   = '".$this->db->escape($data['customer_type'])."',
									name		    = '".$this->db->escape($data['name'])."',
									customer_title  = '".$this->db->escape($data['customer_title'])."',
									birth_date		= '".$birth_date."',
									home 			= '".$this->db->escape($data['home'])."',
									work 			= '".$this->db->escape($data['work'])."',
									mobile 			= '".$this->db->escape($data['contacts'])."',
									address1 		= '".$this->db->escape($data['address1'])."',
									city 			= '".$this->db->escape($data['city'])."',
									company 		= '".$this->db->escape($data['company'])."',
									registration_no = '".$this->db->escape($data['registration_no'])."',
									currency_code 	= '".$this->db->escape($data['currency_code'])."',
									tax_allow 		= '".$this->db->escape($data['tax_allow'])."',
									tax_type 		= '".$this->db->escape($data['tax_type'])."',
									fax 			= '".$this->db->escape($data['fax'])."',
									state 			= '".$this->db->escape($data['state'])."',
									website 		= '".$this->db->escape($data['website'])."',
									email 			= '".$this->db->escape($data['email'])."',
									notes 			= '".$this->db->escape($data['notes'])."',
									term_id 		= '".$this->db->escape($data['term_id'])."',
									country 		= '".$this->db->escape($data['country'])."',
									zipcode 		= '".$this->db->escape($data['zipcode'])."'
									WHERE customercode = '".$customer_id."' ";
			// echo $sql; die;
			$query = $this->db->query($sql);
			$shippingSql = "UPDATE ".DB_PREFIX."shipping SET address1='".$this->db->escape($data['address1'])."',zip='".$this->db->escape($data['zipcode'])."',notes='".$this->db->escape($data['notes'])."' where customer_id='".$customer_id."' AND isdefault='1' ";
			$res = $this->db->query($shippingSql);
		}
		public function deleteCustomer($customer_id)
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "customers WHERE customercode = '" .$customer_id . "'");
		}

		public function getCustomerByPhone($mobile,$customer_id)
		{
			if($customer_id){
				$sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "customers WHERE mobile = '".$mobile."' AND customercode!='".$customer_id."'";
			}else {
				$sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "customers WHERE mobile = '".$mobile."'";
			}
			$query = $this->db->query($sql);
			return $query->row['cnt'];
		}
		public function getCustomerByCode($code)
		{
		    $sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "customers WHERE customercode = '".$code."'";
			$query = $this->db->query($sql);
			return $query->row['cnt'];
		}
		public function getCustomerByEmail($email)
		{
			$company_id	= $this->session->data['company_id'];
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customers WHERE email = '".$email."' AND company_id = '".$company_id."'");
			return $query->row;
		}
		public function getcustomeremail($email,$customer_id ='')
		{
			$query = "SELECT count(*) as total FROM " . DB_PREFIX . "customers WHERE email ='" . $email . "'";
			if($customer_id != ''){
				$query .= "AND customercode != '".$customer_id."'";
			}
			$query = $this->db->query($query);
			return $query->row['total'];
		}
	    /*customer type*/
		public function addCustomerType($data)
		{
			$company_id	= $this->session->data['company_id'];
			$query = $this->db->query("INSERT INTO " . DB_PREFIX . "customer_type (company_id,name,discount,taxcategory,added_date) VALUES ('".$company_id."','".$data['name']."','".$data['discount']."','".$data['taxcategory']."',curdate())");
			$customertype_id = $this->db->getLastId();

			return $customertype_id;
		}
		public function getTotalCustomerType($data=array())
		{
			$company_id	= $this->session->data['company_id'];
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
	      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer_type WHERE company_id = '" . (int)$company_id . "'";

	      	if (!empty($data['filter_name'])){
					$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
				}
				if (!empty($data['filter_discount'])) {	
					$sql .= " AND discount = '" . $data['filter_discount'] . "'";
				}
				if (!empty($data['filter_taxcategory'])) {	
					$sql .= " AND taxcategory = '" . $data['filter_taxcategory'] . "'";
				}
				
				/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					
					$sql .= " ORDER BY " . $data['sort'];	
				} else {
					$sql .= " ORDER BY name";	
				}
				
				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
				
				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}					

					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}	
				
					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				}*/
				//echo $sql; exit;

				$query = $this->db->query($sql);

			return $query->row['total'];
		}
		public function getCustomerType($data = array())
		{		
			$company_id	= $this->session->data['company_id'];
			if ($data) {
				
				$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "customer_type WHERE company_id = '" . (int)$company_id . "'";
				
				$sort_data = array(
					'name',
					'discount',
					'taxcategory',				
				);
				if (!empty($data['filter_name'])) {
					$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
				}
				if (!empty($data['filter_discount'])) {	
					$sql .= " AND discount = '" . $data['filter_discount'] . "'";
				}
				if (!empty($data['filter_taxcategory'])) {	
					$sql .= " AND taxcategory = '" . $data['filter_taxcategory'] . "'";
				}
				
				if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
					
					$sql .= " ORDER BY " . $data['sort'];	
				} else {
					$sql .= " ORDER BY name";	
				}
				
				if (isset($data['order']) && ($data['order'] == 'DESC')) {
					$sql .= " DESC";
				} else {
					$sql .= " ASC";
				}
				
				if (isset($data['start']) || isset($data['limit'])) {
					if ($data['start'] < 0) {
						$data['start'] = 0;
					}					

					if ($data['limit'] < 1) {
						$data['limit'] = 20;
					}	
				
					//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
					$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
				}
				
				$query = $this->db->query($sql);

				return $query->rows;
				
			} else {
				$customer_data = $this->cache->get('customer_type');
				if (!$customer_data) {
					$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_type WHERE company_id = '" . (int)$company_id . "'");
					$customer_data = $query->rows;
					$this->cache->set('customer_type', $customer_data);
				}
				return $customer_data;	
			}	
		}
		public function deleteCustomerType($customertype_id)
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "customer_type WHERE customertype_id = '" . (int)$customertype_id . "'");
		}

		public function getTaxRates()
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT DISTINCT * FROM " .DB_PREFIX . "tax_rate WHERE company_id = '".$company_id."'");

			return $query->rows;
		}
		public function getCustTypes($customertype_id)
		{
			$company_id	= $this->session->data['company_id'];

			//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "customer_type WHERE customertype_id = '" . (int)$customertype_id . "' AND company_id = '" . (int)$company_id . "'"; die;

			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_type WHERE customertype_id = '" . (int)$customertype_id . "' AND company_id = '" . (int)$company_id . "'");
			
			return $query->row;
		}
		public function editCustomerType($data,$customertype_id)
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("UPDATE " . DB_PREFIX . "customer_type SET name   = '".$data['name']."',
									discount		= '".$data['discount']."',
									taxcategory		= '".$data['taxcategory']."'
									 WHERE customertype_id = '".$customertype_id."' AND company_id = '".$company_id."'");

		}

		public function getTypes()
		{

			$company_id	= $this->session->data['company_id'];
			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer_type WHERE company_id = '".$company_id."'");

			return $query->rows;
		}

		public function getTypeDetailsList($customertype_id)
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT T.name, D.discount_name,C.discount,C.taxcategory FROM " . DB_PREFIX ."tax_rate AS T," . DB_PREFIX . "discount AS D, " . DB_PREFIX . "customer_type AS C WHERE T.tax_rate_id = C.taxcategory AND D.discount_id = C.discount AND C.customertype_id = '".$customertype_id."' AND C.company_id = '".$company_id."'" );

			return $query->row;
		}


		public function getDiscounts()
		{
			$company_id	= $this->session->data['company_id'];

			$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "discount WHERE company_id = '".$company_id."'");

			return $query->rows;
		}
		public function getCustomerOrderDetails($invoicenumber){
				
			$query = $this->db->query("SELECT DISTINCT A.user_id,A.invoice_no,A.date_added,B.customer_id,B.primary_email as Email,B.firstname,B.lastname,B.customer_title from tbl_order as A LEFT JOIN tbl_customers as B ON A.user_id = B.customer_id WHERE A.invoice_no = '".$invoicenumber."'");
			return $query->row;
		}

		public function getCustomerGroup($name,$customertype_id)
		{
		$where ='';
		if($customertype_id!=""){
			$where.= " AND customertype_id!= '".$customertype_id."' ";
		}
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_type WHERE name LIKE '".$name."%'".$where);
		return $query->row;
		}
		public function getCustomerByCust_code($code){
		    $sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "customers WHERE cust_code = '".$code."'";
			$query = $this->db->query($sql);
			return $query->row['cnt'];
		}
		public function getCustomerByName($name){
		    $sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "customers WHERE name = '".$name."'";
			$query = $this->db->query($sql);
			return $query->row['cnt'];	
		}
		public function getCurrency()
		{
			$company_id	= $this->session->data['company_id'];
      		return $this->db->query("SELECT * FROM " . DB_PREFIX . "currency WHERE company_id = '" . (int)$company_id . "' AND status='1' ORDER BY default_field DESC")->rows;
		}
		public function addShippingAddress($data){
		
			foreach ($data['code'] as $key => $value) {
				if($data['shipping_id'][$key] == ""){
					$sql = "INSERT INTO ".DB_PREFIX."shipping (customer_id,shipping_code,address1,address2,city,country,zip,contact_no,notes,name) VALUES('".$data['customer_id']."','".$value."','".$data['address1'][$key]."','".$data['address2'][$key]."','".$data['city'][$key]."','".$data['country'][$key]."','".$data['zip'][$key]."','".$data['contact'][$key]."','".$data['notes'][$key]."','".$data['name'][$key]."') ";
				} else {
					$sql = "UPDATE " . DB_PREFIX . "shipping SET customer_id= '".$data['customer_id']."', shipping_code = '".$value."', address1 = '".$data['address1'][$key]."', address2 = '".$data['address2'][$key]."',city = '".$data['city'][$key]."',country ='".$data['country'][$key]."',zip = '".$data['zip'][$key]."',contact_no ='".$data['contact'][$key]."',notes = '".$data['notes'][$key]."',name = '".$data['name'][$key]."' WHERE id = '".$data['shipping_id'][$key]."'";
				}
			$this->db->query($sql);
			}
		}
		public function getShippingAddress($customer_id){
			$sql = "SELECT * FROM ".DB_PREFIX."shipping where customer_id='".$customer_id."' ";
			return $this->db->query($sql)->rows;
		}
	public function getCustLastId(){
		return $this->db->query("SELECT customercode FROM ".DB_PREFIX."customers ORDER BY customercode DESC LIMIT 1")->row['customercode'];
	}
}
?>