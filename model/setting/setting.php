<?php 
class ModelSettingSetting extends Model {
	public function getSetting($group, $store_id = 0) {
		$store_id	= $this->session->data['company_id'];
		$data = array(); 
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND group = '" . $this->db->escape($group) . "'");
		
		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$data[$result['key']] = $result['value'];
			} else {
				$data[$result['key']] = unserialize($result['value']);
			}
		}
		
		return $data;
	}
	
	/*public function editSetting($group, $data, $store_id = 0) {
		$store_id	= $this->session->data['company_id'];
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND [group] = '" . $this->db->escape($group) . "'");

		foreach ($data as $key => $value) {
			if (!is_array($value)) {

				$this->db->query("INSERT INTO " . DB_PREFIX . "setting (store_id,[group],[key],value) VALUES('" . (int)$store_id . "','" . $this->db->escape($group) . "','" . $this->db->escape($key) . "','" . $this->db->escape($value) . "')"); 
			} else {

				$this->db->query("INSERT INTO " . DB_PREFIX . "setting (store_id,[group],[key],value,serialized) VALUES ('" . (int)$store_id . "','" . $this->db->escape($group) . "','" . $this->db->escape($key) . "','" . $this->db->escape(serialize($value)) . "',1)");				
			}
		}
	}
	
	public function deleteSetting($group, $store_id = 0) {
		$store_id	= $this->session->data['company_id'];
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND [group] = '" . $this->db->escape($group) . "'");
	}
	*/

	public function editSetting($group, $data, $store_id = 0) {
		// printArray($data); die;
		foreach ($data as $key => $value) {
			$sql ="UPDATE " . DB_PREFIX . "setting SET value = '" . $this->db->escape($value) . "' WHERE `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "'";
			$this->db->query($sql);
		}
		foreach ($data['network_status'] as $key => $value) {
			$xero_post      = $data['xero_post'][$key] =='1' ? 1 : 0;
			$payment_method = $data['payment_method'][$key];
			$sql ="UPDATE ".DB_PREFIX."networks SET status = '".$value."', xero_post='".$xero_post."',allow_default='".$data['allow_default'][$key]."', payment_method='".$payment_method."' WHERE id = '".$key."'";
			$this->db->query($sql);
		}
	}
	public function editSettingValue($group = '', $key = '', $value = '', $store_id = 0) {
		$store_id	= $this->session->data['company_id'];
		if (!is_array($value)) {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '" . $this->db->escape($value) . "' WHERE `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "setting SET value = '" . $this->db->escape(serialize($value)) . "' WHERE `group` = '" . $this->db->escape($group) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "', serialized = '1'");
		}
	}

	public function getNetworks(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "networks");
		return $query->rows;
	}
	public function getPaymentMethods(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."payment_type_master where status='1'")->rows;
	}		
}
?>