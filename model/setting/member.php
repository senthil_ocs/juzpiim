<?php
class ModelSettingMember extends Model {

	public function getLastMemberCode(){
		$company_id	= $this->session->data['company_id']; 
		//$sql = "SELECT MAX(member_code) as membercode FROM " . DB_PREFIX . "member WHERE company_id = '" . (int)$company_id . "'";
		$sql = "SELECT value from tbl_setting where `key`='next_member_no'";
		$query = $this->db->query($sql);
		if(!empty($query->row)){
			$memcode=$query->row['value'];

		}else{
			$memcode='0001';
		}
		
		return $memcode;
	}
	public function addMember($data){
		$company_id	= $this->session->data['company_id'];
		//$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		 $sql = "INSERT INTO " . DB_PREFIX . "member (company_id,member_code,member_type,member_title,name,company,birth_date,status,home,work,mobile,address1,address2,city,state,zipcode,notes,createdby,createdon,modifiedby,modifiedon) VALUES ('".$company_id."','".$data['member_code']."','".$data['member_type']."','".$data['member_title']."','".$data['name']."','".$data['company']."','".$data['birth_date']."','".$data['status']."','".$data['home']."','".$data['work']."','".$data['mobile']."','".$data['address1']."','".$data['address2']."','".$data['city']."','".$data['state']."','".$data['zipcode']."','".$data['notes']."','".$userName."',curdate(),'".$userName."',curdate())";
		 $query = $this->db->query($sql);

		$member_id = $this->db->getLastId();

		$next_member_no = $data['member_code']+1;
		$setting_sql = "update tbl_setting set value='".$next_member_no."' where `key`='next_member_no'";
		$this->db->query($setting_sql);
		return $member_id;

	}
	public function getTotalMembers($data){

		$company_id	= $this->session->data['company_id'];
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "member WHERE company_id = '" . (int)$company_id . "'";
		
		$sort_data = array(
			'name',
			'member_code',				
		);
       
        if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR member_code LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
        }

		// if ($data['filter_name']){
		// 	$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ||  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
		// }

		if ($data['filter_name']){
			$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
		}

		if ($data['filter_company']){
			$sql.= " AND company LIKE '" . $this->db->escape($data['filter_company']) . "%'"; 
		}

		if ($data['filter_phone']){
			$sql.= " AND mobile LIKE '" . $this->db->escape($data['filter_phone']) . "%'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$fromdate=date("Y-m-d",strtotime(str_replace('/', '-', $data['filter_date_from'])));
			$todate=date("Y-m-d",strtotime(str_replace('/', '-', $data['filter_date_to'])));

			$sql .= " AND createdon between  '" .$this->db->escape($fromdate) . "' AND DATEADD(day, 1,'" .$this->db->escape($todate). "')";
		}	
		
		$query = $this->db->query($sql); 
		
		return $query->row['total'];

	} 

	public function getMembers($data){
		//echo $userId; exit;
		$company_id	= $this->session->data['company_id'];
		if ($data) {

			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "member WHERE company_id = '" . (int)$company_id . "'";
			$sort_data = array(
				'name',
				'member_code',				
			);

			 if(!empty($data['searchkey'])){
        	$sql.= " AND ( name LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['searchkey']) . "%' OR member_code LIKE '" . $this->db->escape($data['searchkey']) . "%' )"; 
             }
			/*if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' ||  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}*/
			if ($data['filter_name']){
				$sql.= " AND name LIKE '%" . $this->db->escape($data['filter_name']) . "%' OR  mobile LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}

			if ($data['filter_company']){
				$sql.= " AND company LIKE '" . $this->db->escape($data['filter_company']) . "%'"; 
			}
			
			if ($data['filter_phone']){
			$sql.= " AND mobile LIKE '" . $this->db->escape($data['filter_phone']) . "%'"; 
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$fromdate=date("Y-m-d",strtotime(str_replace('/', '-', $data['filter_date_from'])));
				$todate=date("Y-m-d",strtotime(str_replace('/', '-', $data['filter_date_to'])));

				$sql .= " AND createdon between  '" .$this->db->escape($fromdate) . "' AND DATEADD(day, 1,'" .$this->db->escape($todate). "')";
			}	
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY createdon";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	


			$query = $this->db->query($sql);
			return $query->rows;
			
		} 	

	}
	 
	public function editMember($data,$member_id)
	{
		$company_id	= $this->session->data['company_id'];
		//$userId	= $this->session->data['user_id'];
		$userName	= $this->session->data['username'];
		//$modifiedon = date_format($data['createdon'],"d/m/Y")
		$query = $this->db->query("UPDATE " . DB_PREFIX . "member SET 
									status 			= '".$data['status']."',
									name		    = '".$data['name']."',
									member_title    = '".$data['member_title']."',
									company 		= '".$data['company']."',
									birth_date		= '".$data['birth_date']."',
									home 			= '".$data['home']."',
									work 			= '".$data['work']."',
									mobile 			= '".$data['mobile']."',
									address1 		= '".$data['address1']."',
									address2 		= '".$data['address2']."',
									city 			= '".$data['city']."',
									state 			= '".$data['state']."',
									zipcode 		= '".$data['zipcode']."',
									notes 			= '".$data['notes']."',
									modifiedby      = '".$userName."',
									modifiedon		= curdate() WHERE member_id = '".$member_id."' AND company_id = '".$company_id."'");

		}
		public function deleteMember($member_id)
		{
			$company_id	= $this->session->data['company_id'];
			$this->db->query("DELETE FROM " . DB_PREFIX . "member WHERE member_id = '" . (int)$member_id . "' AND company_id = '".$company_id."'");
		}

	public function getmember($member_id)
	{
		$company_id	= $this->session->data['company_id'];

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "member WHERE member_id = '" . (int)$member_id . "' AND company_id = '" . (int)$company_id . "'");
		
		return $query->row;
	}

}
?>