<?php
class ModelSettingEmailTemplate extends Model {
	
	public function addEmailTemplate($data){
		// printArray($data); die;
		$sql = "INSERT INTO ".DB_PREFIX."email_template (`name`, `subject`, `description`, `status`, `created_by`) VALUES ('".$this->db->escape($data['name'])."','".$this->db->escape($data['subject'])."','".$this->db->escape($data['description'])."','".$this->db->escape($data['status'])."','".$this->session->data['user_id']."')";
		$this->db->query($sql);
	}
	
	public function getTotalEmailTemplate($data){
		$sql = "SELECT count(*) as total FROM ".DB_PREFIX."email_template where name!='' ";
		if($data['filter_name'] !=''){
			$sql .= " AND name LIKE '%".$data['filter_name']."%' ";
		}
		if($data['filter_status'] !=''){
			$sql .= " AND status= '".$data['filter_status']."' ";
		}
		return $this->db->query($sql)->row['total'];
	}

	public function getAllEmailTemplate($data){
		$sql = "SELECT * FROM ".DB_PREFIX."email_template where name!='' ";
		if($data['filter_name'] !=''){
			$sql .= " AND name LIKE '%".$data['filter_name']."%' ";
		}
		if($data['filter_status'] !=''){
			$sql .= " AND status= '".$data['filter_status']."' ";
		}
		return $this->db->query($sql)->rows;
	}
	
	public function getEmailTemplate($id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."email_template where id='".$id."'")->row;
	}
	
	public function updateEmailTemplate($id,$data){
		$this->db->query("UPDATE ".DB_PREFIX."email_template set name='".$data['name']."',subject='".$data['subject']."', description='".$data['description']."', status='".$data['status']."' where id='".$id."' ");
	}
}
?>