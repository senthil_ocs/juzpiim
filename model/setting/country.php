<?php
class ModelSettingCountry extends Model {
	public function addCountry($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "country (name,company_id,iso_code_2,iso_code_3,status) VALUES ('" . $this->db->escape($data['name']) . "','".$company_id."','" . $this->db->escape($data['iso_code_2']) . "','" . $this->db->escape($data['iso_code_3']) . "','" . (int)$data['status'] . "')");
		$this->cache->delete('country');
	}

	public function editCountry($country_id, $data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "country SET 
			name = '" . $this->db->escape($data['name']) . "'
			, company_id = '".$company_id."'
			, iso_code_2 = '" . $this->db->escape($data['iso_code_2']) . "'
			, iso_code_3 = '" . $this->db->escape($data['iso_code_3']) . "'
			, status = '" . (int)$data['status'] . "' 
			WHERE country_id = '" . (int)$country_id . "'");

		$this->cache->delete('country');
	}

	public function deleteCountry($country_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getCountry($country_id);
		$strLogDetails = array(
			'type' 		=> 'Country',
			'id'   		=> $country_id,
			'code' 		=> $tableDetails['name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."'");
		$this->cache->delete('country');
	}

	public function getCountry($country_id) {
		$company_id	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."'");

		return $query->row;
	}

	public function getCountries($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "country WHERE company_id = '".$company_id."'";

			$sort_data = array(
				'name',
				'iso_code_2',
				'iso_code_3'
			);	

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY name";	
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	

				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}		

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$country_data = $this->cache->get('country');

			if (!$country_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country WHERE company_id = '".$company_id."' ORDER BY name ASC");

				$country_data = $query->rows;

				$this->cache->set('country', $country_data);
			}

			return $country_data;			
		}	
	}

	public function getTotalCountries() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "country WHERE company_id = '".$company_id."'");

		return $query->row['total'];
	}	
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>