<?php
class ModelSettingTouchMenu extends Model {

	public function getTotalTouchMenu($data = array())
	{
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code!= ''";
		if ($data['filter_menu_code']){
			$sql.= " AND touch_menu_description LIKE '%" . $this->db->escape($data['filter_menu_code']) .  "%'"; 
		}
		if ($data['filter_sort_code']){
			$sql.= " AND sort_code = '" . $this->db->escape($data['filter_sort_code']) . "'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
		}	
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getTouchMenu($data = array())
	{		
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code!= ''";
			
			$sort_data = array(
			'touch_menu_code',
			'sort_code',				
			);

			
			if ($data['filter_menu_code']){
				$sql.= " AND touch_menu_description LIKE '%" . $this->db->escape($data['filter_menu_code']) .  "%'";  
			}
			if ($data['filter_sort_code']){
				$sql.= " AND sort_code = '" . $this->db->escape($data['filter_sort_code']) . "'"; 
			}
			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
			}	
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY sort_code";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			//echo $sql;
			$query = $this->db->query($sql);

			return $query->rows;
	}

	public function getLastTouchMenuById()
	{
		$query = $this->db->query("SELECT MAX(sort_code)+1 as lastSortCode  FROM " . DB_PREFIX . "product_touch_menu");
		return $query->row['lastSortCode'];
	}

	public function getTouchMenuById($id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code = '" . trim($id). "'");
		return $query->row;
	}

	public function addTouchMenu($data)
	{
		$insertSql = "INSERT INTO " . DB_PREFIX . "product_touch_menu (touch_menu_code,touch_menu_description,status,sort_code,createdby,createdon,modifiedby,modifiedon) VALUES ('".$this->db->escape($data['touch_menu_code'])."','".$this->db->escape($data['touch_menu_description'])."','".$data['touch_menu_status']."','".$data['sort_code']."','".$this->session->data['username']."',curdate(),'".$this->session->data['username']."',curdate())";
		$query = $this->db->query($insertSql);
		$touchmenu_id = $this->db->getLastId();
		return $touchmenu_id;
	}

	public function editTouchMenu($data,$id)
	{
		
		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_touch_menu SET
									touch_menu_description   = '".$this->db->escape($data['touch_menu_description'])."',
									status		= '".$data['touch_menu_status']."',
									sort_code		= '".$data['sort_code']."',
									modifiedby		= '".$this->session->data['username']."',
									modifiedon 			= curdate()
									WHERE touch_menu_code = '".$data['touch_menu_code']."'");
		}


		public function deleteTouchMenu($id)
		{
			$strLogDetails = array(
				'type' 		=> 'Touch Menu',
				'id'   		=> $id,
				'code' 		=> $id,
				'userid'   	=> $this->session->data['user_id'],
				'username' 	=> $this->session->data['username'],
				'date'		=> date('Y-m-d H:i:s'),
			);
			$this->insertlog($strLogDetails);
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code = '" . trim($id) . "'");
		}


		public function getTouchMenuByMenuCode($menu_code,$id)
		{
			$where = "";
			if($id!=''){
				$where = " AND id != '".$id."'";
			}
			$query = $this->db->query("SELECT count(*) as cnt FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code = '".$menu_code."' ".$where."");

			return $query->row;
		}
		public function updateSortOrder($code,$sort_code){
		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_touch_menu SET sort_code = '".$sort_code."' WHERE touch_menu_code = '".$code."'");
		}
		public function insertlog($data){
			$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
		}
}
?>