<?php
class ModelSettingCompany extends Model {
	
	public function getCompanyDetails($company_id) {
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "company As c WHERE company_id = '" . (int)$company_id . "'");
		return $query->row;
	}

	public function getCurrentLocation(){
		$query = $this->db->query("SELECT location_code FROM " . DB_PREFIX . "company");
		return $query->row['location_code'];
	}
	
	public function editCompanyDetails($company_id,$data,$formFieldNames) {
		//print_r($data);
		$escapeFields	= array('c_password','last_eod_date','name','location_code','company_id');
		$userId	= $this->session->data['user_id'];
		$userName = $this->session->data['username'];
		//print_r($escapeFields); exit;
		foreach($data as $key=>$values) {
			//printArray($values);
			if(in_array($key,$formFieldNames)) {
				//if(!in_array($key,$escapeFields)) {					
					$this->db->query("UPDATE " . DB_PREFIX . "company SET ".$this->db->escape($key). " = '" . $this->db->escape($values) . "'  WHERE company_id = '" . (int)$company_id . "'");	
				//}
			}
		}
	$this->db->query("UPDATE " . DB_PREFIX . "company SET modify_date =  curdate(), modify_user='".$userName."' WHERE company_id = '" . (int)$company_id . "'");
		$filename = $this->imageUploads($data->files);
		if(!empty($filename)) {
			$this->db->query("UPDATE " . DB_PREFIX . "company SET logo = '" . $filename . "' WHERE company_id = '" . (int)$company_id . "'");
		}
		return true;
	}
	
	public function imageUploads($files) {
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");
		$name = $files['logo']['name'];
		$size = $files['logo']['size'];
		if(strlen($name)){
			$ext = $this->getExtension($name);
			if(in_array($ext,$valid_formats)) {
				if($size<(1024*1024)) {
					$txt	='';
					$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
					$tmp = $files['logo']['tmp_name'];
					move_uploaded_file($tmp, DIR_IMAGE.$actual_image_name);
						return $actual_image_name;
				}
			} 
		}
	}
	
	public function getExtension($str) {
	
	 $i = strrpos($str,".");
	 if (!$i) { return ""; } 
	 $l = strlen($str) - $i;
	 $ext = substr($str,$i+1,$l);
	 return $ext;
	 
	}


	public function getCountry()
	{
		//echo "SELECT DISTINCT * FROM " . DB_PREFIX . "country"; exit;
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country WHERE status = '1'");
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "country");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "country");


		return $query->rows;
	}	

	public function getZone()
	{
		//$query = $this->db->query("SELECT DISTINCT * FROM " .DB_PREFIX . "zone WHERE status = '1'");
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "zone WHERE status = '1'");

		return $query->rows;
	}

	public function getZonesByCountry($country_id)
	{
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "zone WHERE country_id = '".$country_id."' AND status = '1'");
	
		return $query->rows;
	}
	public function getLocations(){
		$query = $this->db->query("SELECT * FROM " .DB_PREFIX . "location ORDER BY location_id DESC");
		return $query->rows;
	}
	public function getUserGroupIdByUserId($userId){
		$query = $this->db->query("SELECT user_group_id FROM " .DB_PREFIX . "user where user_id = '".$userId."'");
		return $query->row;
	}
	public function getStucture()
	{	
		$myServer = "118.200.119.248:1206";
		$myUser = "sa";
		$myPass = "Juz@321";
		$myDB = "juzpos";

		$dbhandle = mssql_connect($myServer, $myUser, $myPass) or die("Couldn't connect to SQL Server on $myServer"); 
		$selected = mssql_select_db($myDB, $dbhandle) or die("Couldn't open database $myDB"); 

		//$query = "SELECT * FROM INFORMATION_SCHEMA.TABLES";
		$query = "SELECT * FROM 'tbl_country'";
		//Execute the SQL query and return records
		$result = mssql_query($query) or die('A error occured: ' . mysql_error());
		
		while ( $record = mssql_fetch_array($result) )
		{
			print '<pre>'; print_r($record); print '</pre>'; exit;
			echo $record["TABLE_NAME"] . "<br />";
		}
		//Free result set memory
		mssql_free_result($result);
		//Close the connection
		//mssql_close($dbhandle);
		
		//$result = mssql_num_rows($query);
		//print_r($result); exit;
		//$query = $this->db->query("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = " . DB_PREFIX . "country");

		//return $query->rows;
	}

	public function getCurrency() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE company_id = '".$company_id."'");
		return $query->rows;
	}
}