<?php
class ModelSettingZone extends Model {
	public function addZone($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "zone (status,company_id,name,code,country_id) VALUES('" . (int)$data['status'] . "','".$company_id."','" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['code']) . "','" . (int)$data['country_id'] . "')");
		$this->cache->delete('zone');
	}

	public function editZone($zone_id, $data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "zone SET 
			status = '" . (int)$data['status'] . "'
			, company_id = '".$company_id."'
			, name = '" . $this->db->escape($data['name']) . "'
			, code = '" . $this->db->escape($data['code']) . "'
			, country_id = '" . (int)$data['country_id'] . "' 
			WHERE zone_id = '" . (int)$zone_id . "'");

		$this->cache->delete('zone');
	}

	public function deleteZone($zone_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getZone($zone_id);
		$strLogDetails = array(
			'type' 		=> 'Zones',
			'id'   		=> $zone_id,
			'code' 		=> $tableDetails['code'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND company_id = '".$company_id."'");

		$this->cache->delete('zone');	
	}

	public function getZone($zone_id) {
		$company_id	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND company_id = '".$company_id."'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE zone_id = '" . (int)$zone_id . "' AND company_id = '".$company_id."'");

		return $query->row;
	}

	public function getZones($data = array()) {
		$company_id	= $this->session->data['company_id'];
		
		$sql = "SELECT z.name, z.zone_id, c.name AS country FROM " . DB_PREFIX . "zone z 
				LEFT JOIN " . DB_PREFIX . "country c ON (z.country_id = c.country_id) 
				WHERE z.company_id = '".$company_id."'";

		$sort_data = array(
			'c.name',
			'z.name',
			'z.code'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY c.name";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}		

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getZonesByCountryId($country_id) {
		$company_id	= $this->session->data['company_id'];
		$zone_data = $this->cache->get('zone.' . (int)$country_id);

		if (!$zone_data) {
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."' AND status = '1' ORDER BY name");

			$zone_data = $query->rows;

			$this->cache->set('zone.' . (int)$country_id, $zone_data);
		}

		return $zone_data;
	}

	public function getTotalZones() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone WHERE company_id = '".$company_id."'");

		return $query->row['total'];
	}

	public function getTotalZonesByCountryId($country_id) {	
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."'");

		return $query->row['total'];
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>