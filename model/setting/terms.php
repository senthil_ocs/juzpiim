<?php
class ModelSettingTerms extends Model {
	public function addTerms($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "terms (terms_name,terms_code,noof_days,status,date_added) VALUES ('" . $this->db->escape($data['terms_name']) . "','" . $this->db->escape($data['terms_code']) . "','". (int)$data['noof_days'] ."','" . (int)$this->db->escape($data['status']) . "',curdate())");
			
	}

	public function editTerms($terms_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "terms SET
			terms_name = '" . $this->db->escape($data['terms_name']) . "'
			, terms_code = '" . $this->db->escape($data['terms_code']) . "'
			, noof_days = '". (int)$data['noof_days'] ."'
			, status = '" . (int)$this->db->escape($data['status']) . "'
			WHERE terms_id = '" . (int)$terms_id . "'");

		
	}

	public function deleteTerms($terms_id) {
		/*$tableDetails = $this->getTerm($terms_id);
		$strLogDetails = array(
			'type' 		=> 'Terms',
			'id'   		=> $terms_id,
			'code' 		=> $tableDetails['terms_code'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		printArray($strLogDetails);
		$this->insertlog($strLogDetails);*/
		$this->db->query("DELETE FROM " . DB_PREFIX . "terms WHERE terms_id = '" . (int)$terms_id . "'");
	}

	public function getTerm($terms_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "terms WHERE terms_id = '" . (int)$terms_id . "'");

		return $query->row;
	}


	public function getTerms($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "terms WHERE terms_id IS NOT NULL ";

		$sort_data = array(
			'terms_name',
			'terms_code',
			'noof_days',
			'status',
			'date_added'
		);
		if ($data['filter_name']){
			$sql.= " AND terms_name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
		}
		if ($data['filter_code']){
			$sql.= " AND terms_code LIKE '" . $this->db->escape($data['filter_code']) . "%'"; 
		}
		if ($data['filter_status']!="") {	
			$sql .= "AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}
		if ($data['filter_days']){
			$sql.= " AND noof_days LIKE '" . $this->db->escape($data['filter_days']) . "%'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND date_added between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
		}	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY terms_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo "$sql";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalTerms() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "terms");

		return $query->row['total'];
	}

	public function getTotalUsersByGroupId($user_group_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "user WHERE user_group_id = '" . (int)$user_group_id . "' AND company_id = '" . (int)$company_id . "'");

		return $query->row['total'];
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>