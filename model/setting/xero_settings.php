<?php
class ModelSettingXeroSettings extends Model {
	public function addXeroSettings($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "xero_settings (name, description, status, available_to, xero_acc_code, isNetwork) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['decription']) . "','" . (int)$this->db->escape($data['status']) . "','" . $this->db->escape($data['available_to']) . "','". $this->db->escape($data['xero_acc_code']) ."','". $this->db->escape($data['isNetwork']) ."')");
			
	}

	public function editXeroSettings($xero_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "xero_settings SET
			name = '" . $this->db->escape($data['name']) . "'
			, description = '" . $this->db->escape($data['description']) . "'
			, status = '" . (int)$this->db->escape($data['status']) . "'
			, available_to = '" . $this->db->escape($data['available_to']) . "'
			, xero_acc_code = '". $this->db->escape($data['xero_acc_code']) ."'
			, isNetwork = '". $this->db->escape($data['isNetwork']) ."'
			WHERE id = '" . (int)$xero_id . "'");

		
	}

	public function deleteXeroSettings($xero_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "xero_settings WHERE id = '" . (int)$xero_id . "'");
	}

	public function getXeroSetting($xero_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "xero_settings WHERE id = '" . (int)$xero_id . "'");

		return $query->row;
	}


	public function getXeroSettings($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "xero_settings WHERE id IS NOT NULL ";

		$sort_data = array(
			'name',
			'available_to',
			'xero_acc_code',
			'status',
			'isNetwork'
		);
		if ($data['filter_name']){
			$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
		}
		if ($data['filter_available_to']){
			$sql.= " AND available_to = '" . $this->db->escape($data['filter_available_to']) . "'"; 
		}
		if ($data['filter_status']!="") {	
			$sql .= "AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}
		if ($data['filter_xero_acc_code']){
			$sql.= " AND xero_acc_code LIKE '" . $this->db->escape($data['filter_xero_acc_code']) . "%'"; 
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY terms_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo "$sql";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalXeroSettings($data) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "xero_settings where id!='' ";

		$sort_data = array(
			'name',
			'available_to',
			'xero_acc_code',
			'status',
			'isNetwork'
		);
		if ($data['filter_name']){
			$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
		}
		if ($data['filter_available_to']){
			$sql.= " AND available_to = '" . $this->db->escape($data['filter_available_to']) . "'"; 
		}
		if ($data['filter_status']!="") {	
			$sql .= "AND status = '" . $this->db->escape($data['filter_status']) . "'";
		}
		if ($data['filter_xero_acc_code']){
			$sql.= " AND xero_acc_code LIKE '" . $this->db->escape($data['filter_xero_acc_code']) . "%'"; 
		}
		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY terms_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo "$sql";
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>