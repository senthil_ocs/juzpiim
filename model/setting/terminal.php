<?php
class ModelSettingTerminal extends Model {

	public function getLastTerminalCode(){
		//$terminal_code	= $this->session->data['terminal_code']; 

		$query = $this->db->query("SELECT MAX(terminal_code) as terminalcode FROM " . DB_PREFIX . "terminal_configuration");
		if(!empty($query->row)){
			$termcode=$query->row['terminalcode'];

		}else{
			$termcode=1;
		}
		
		return $termcode;
	}
	
	public function addTerminal($data){
			
		  $sql = "INSERT INTO " . DB_PREFIX . "terminal_configuration (terminal_code,terminal_name,system_name,cash_machine,cash_machine_port,coin_machine,coin_machine_port,Cust_display,Cust_Display_Port,Cust_Display_Type,Cust_Display_Msg1,Cust_Display_Msg2,speaker_enabled,weighingscale_enabled,weighingscale_port,createdon,createdby) VALUES ('".$data['terminal_code']."','".$data['terminal_name']."','".$data['system_name']."','".$data['cash_machine']."','".$data['cash_machine_port']."','".$data['coin_machine']."','".$data['coin_machine_port']."','".$data['Cust_display']."','".$data['Cust_Display_Port']."','".$data['Cust_Display_Type']."','".$data['Cust_Display_Msg1']."','".$data['Cust_Display_Msg2']."','".$data['speaker_enabled']."','".$data['weighingscale_enabled']."','".$data['weighingscale_port']."',curdate(),1)";
		 $query = $this->db->query($sql);

		$terminal_code = $this->db->getLastId();
		return $terminal_code;
	}
	public function getTotalTerminals($data){

      	$sql = "SELECT count(*) as total FROM " . DB_PREFIX . "terminal_configuration";
			
		if (!empty($data['filter_terminal_name'])){
			$sql.= " where terminal_name LIKE '%" . $this->db->escape($data['filter_terminal_name']) . "%' OR system_name LIKE '%" . $this->db->escape($data['filter_terminal_name']) . "%' "; 
		}

		$query = $this->db->query($sql); 
		return $query->row['total'];

	} 


	public function getTerminals($data){
		if ($data) {

			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "terminal_configuration";
			
			if (!empty($data['filter_terminal_name'])){
				$sql.= " where terminal_name LIKE '%" . $this->db->escape($data['filter_terminal_name']) . "%' OR system_name LIKE '%" . $this->db->escape($data['filter_terminal_name']) . "%' "; 
			}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY createdon";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);
			return $query->rows;
		} 	

	}
	 
	public function editTerminal($data,$terminal_code)
	{
		
		$sql="UPDATE " . DB_PREFIX . "terminal_configuration SET 
									terminal_code 		= '".$data['terminal_code']."',
									terminal_name 		= '".$data['terminal_name']."',
									system_name		    = '".$data['system_name']."',
									cash_machine    	= '".$data['cash_machine']."',
									cash_machine_port 	= '".$data['cash_machine_port']."',
									coin_machine		= '".$data['coin_machine']."',
									coin_machine_port 	= '".$data['coin_machine_port']."',
									Cust_display    	= '".$data['Cust_display']."',
									Cust_Display_Port 	= '".$data['Cust_Display_Port']."',
									Cust_Display_Type	= '".$data['Cust_Display_Type']."',
									Cust_Display_Msg1 	= '".$data['Cust_Display_Msg1']."',
									Cust_Display_Msg2 	= '".$data['Cust_Display_Msg2']."',
									speaker_enabled		='".$data['speaker_enabled']."',
									weighingscale_enabled ='".$data['weighingscale_enabled']."',
									weighingscale_port	='".$data['weighingscale_port']."'
									WHERE terminal_code = '".$terminal_code."'";
		//echo $sql;exit;
		 $query = $this->db->query($sql);
		}
		public function deleteTerminal($terminal_code)
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "terminal_configuration WHERE terminal_code = '" . $terminal_code . "'");
		}

	public function getterminal($terminal_code)
	{
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "terminal_configuration WHERE terminal_code = '" . $terminal_code . "'");
		return $query->row;
	}
}
?>