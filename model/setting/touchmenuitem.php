<?php
class ModelSettingTouchMenuItem extends Model {

	public function getTotalTouchMenuItem($data = array())
	{
      	$sql = "SELECT COUNT(tmi.id) AS total FROM " . DB_PREFIX . "product_touch_menu_item as tmi  
      			LEFT JOIN " . DB_PREFIX . "product_touch_menu as tm ON tm.touch_menu_code = tmi.touch_menu_code	WHERE tmi.touch_menu_code != ''";
		
		$sort_data = array(
			'tmi.touch_menu_code',
			'tmi.sort_code',				
		);
       
		if ($data['filter_menu_code']){
			$sql.= " AND tm.touch_menu_code = '" . $this->db->escape($data['filter_menu_code']) . "'"; 
		}
		if ($data['filter_description']){
			$sql.= " AND tmi.sku_description LIKE '%" . $this->db->escape($data['filter_description']) .  "%'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND tmi.createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
		}
			
		
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getTouchMenuItem($data = array())
	{		
		
			$sql = "SELECT tmi.* FROM " . DB_PREFIX . "product_touch_menu_item as tmi  
      			LEFT JOIN " . DB_PREFIX . "product_touch_menu as tm ON tm.touch_menu_code = tmi.touch_menu_code	WHERE tmi.touch_menu_code != ''";
		
			$sort_data = array(
			'tmi.touch_menu_code',
			'tmi.sort_code',				
			);

			if ($data['filter_menu_code']){
				$sql.= " AND tm.touch_menu_code = '" . $this->db->escape($data['filter_menu_code']) . "'"; 
			}

			if ($data['filter_description']){
				$sql.= " AND tmi.sku_description LIKE '%" . $this->db->escape($data['filter_description']) .  "%'"; 
			}

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND tmi.createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
			}

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY tmi.touch_menu_code , tmi.sort_code";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}	
			//echo $sql;
			$query = $this->db->query($sql);
			return $query->rows;				
	}


	public function getTouchMenuItemById($menu_code,$sku)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product_touch_menu_item WHERE touch_menu_code = '".$menu_code."' and sku_code = '".base64_decode($sku)."'");
		return $query->row;
	}

	public function addTouchMenuItem($data)
	{
		$query = $this->db->query("INSERT INTO " . DB_PREFIX . "product_touch_menu_item (touch_menu_code,sku_code,sku_description,status,sort_code,createdby,createdon,modifiedby,modifiedon) VALUES ('".$this->db->escape($data['touch_menu_code'])."','".$this->db->escape($data['sku_code'])."','".$this->db->escape($data['touch_menu_description'])."','".$data['touch_menu_status']."','".$data['sort_code']."','".$this->session->data['username']."',curdate(),'".$this->session->data['username']."',curdate())");
		$touchmenu_id = $this->db->getLastId();

		if(file_exists($_FILES["product_image"]["tmp_name"])){
			
          	 $base64file = base64_encode(file_get_contents($_FILES["product_image"]["tmp_name"]));
          	 $query = $this->db->query("UPDATE " . DB_PREFIX . "product  SET imgData = '".$base64file."' WHERE sku = '".$this->db->escape($data['sku_code'])."'");
          }

		return $touchmenu_id;
	}

	public function editTouchMenuItem($data,$menucode,$sku)
	{
		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_touch_menu_item SET 
									sku_description   = '".$this->db->escape($data['touch_menu_description'])."',
									status		= '".$data['touch_menu_status']."',
									sort_code		= '".$data['sort_code']."',
									modifiedby		= '".$this->session->data['username']."',
									modifiedon 			= curdate()
									WHERE touch_menu_code = '".$menucode."' AND sku_code = '".base64_decode($sku)."'");
		
          if(file_exists($_FILES["product_image"]["tmp_name"])){
          	$base64file = base64_encode(file_get_contents($_FILES["product_image"]["tmp_name"]));
          	//echo "UPDATE " . DB_PREFIX . "product  SET imgData = '".$base64file."' WHERE sku = '".base64_decode($sku)."'"; exit;
          	 $query = $this->db->query("UPDATE " . DB_PREFIX . "product  SET imgData = '".$base64file."' WHERE sku = '".base64_decode($sku)."'");
          }
         
		 
	}

	public function deleteTouchMenuItem($id)
	{
		$tableDetails = $this->getTouchMenuItemByItemId($id);
		$strLogDetails = array(
			'type' 		=> 'Touch Menu Item',
			'id'   		=> $id,
			'code' 		=> $tableDetails['sku_code'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_touch_menu_item WHERE id = '".$id."'");
	}


	public function getTouchMenuItemByMenuCode($menu_code,$sku,$type='')
	{
		if($type!='update'){
			$sql = "SELECT count(*) as cnt FROM " . DB_PREFIX . "product_touch_menu_item WHERE touch_menu_code = '".$menu_code."' and sku_code = '".$sku."'";
		}
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getTouchMenuCode(){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_touch_menu WHERE touch_menu_code != '' ");
		return $query->rows;
	}
	public function getProducts(){
		$query = $this->db->query("SELECT P.sku,PD.name,PD.short_description FROM " . DB_PREFIX . "product as P LEFT JOIN " . DB_PREFIX . "product_description as PD ON P.product_id = PD.product_id WHERE P.status = '1' LIMIT 0, 10");
		return $query->rows;
	}
	public function getProductDetailsBySku($sku){
		$sql = "SELECT PD.short_description as description FROM " . DB_PREFIX . "product as P LEFT JOIN " . DB_PREFIX . "product_description as PD ON P.product_id = PD.product_id WHERE P.status = '1' AND P.sku ='".$sku."'";
		$query = $this->db->query($sql);
		return $query->row;
	}
	public function getLastSortCode($menuCode,$sku){
		$query = $this->db->query("SELECT MAX(sort_code)+1 as SortCode FROM " . DB_PREFIX . "product_touch_menu_item WHERE touch_menu_code = '".$menuCode."' and sku_code != '".$sku."'");
		return $query->row['SortCode'];
	}

	public function updateSortOrder($id,$sort_code){
		$query = $this->db->query("UPDATE " . DB_PREFIX . "product_touch_menu_item SET sort_code = '".$sort_code."' WHERE id = '".$id."'");
	}
	public function getTouchMenuItemByItemId($id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_touch_menu_item WHERE id = '".$id."' ");
		return $query->row;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	public function getTMname($code){
		$sql = "select touch_menu_description from tbl_product_touch_menu where touch_menu_code='".$code."'";
		$query = $this->db->query($sql);
		return $query->row['touch_menu_description'];
	}
}
?>