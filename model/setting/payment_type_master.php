<?php
class ModelSettingPaymentTypeMaster extends Model {
	public function addPaymentTypeMaster($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "payment_type_master (name, code, xero_account_code, status) VALUES ('" . $this->db->escape($data['name']) . "','" . $this->db->escape($data['code']) . "','". $this->db->escape($data['xero_account_code']) ."','" . (int)$this->db->escape($data['status']) . "')");
			
	}

	public function editPaymentTypeMaster($payment_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "payment_type_master SET
			name = '" . $this->db->escape($data['name']) . "'
			, code = '" . $this->db->escape($data['code']) . "'
			, xero_account_code = '". $this->db->escape($data['xero_account_code']) ."'
			, status = '" . (int)$this->db->escape($data['status']) . "'
			WHERE id = '" . (int)$payment_id . "'");

		
	}

	public function deletePaymentTypeMaster($payment_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "payment_type_master WHERE id = '" . (int)$payment_id . "'");
	}

	public function getPaymentTypeMaster($payment_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "payment_type_master WHERE id = '" . (int)$payment_id . "'");

		return $query->row;
	}


	public function getPaymentTypeMasters($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "payment_type_master WHERE id IS NOT NULL ";

		$sort_data = array(
			'name',
			'code',
			'xero_account_code',
			'status',
			'added_date'
		);
		if ($data['filter_name']){
			$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
		}
		if ($data['filter_code']){
			$sql.= " AND code LIKE '" . $this->db->escape($data['filter_code']) . "%'"; 
		}
		if ($data['filter_status']!="") {	
			$sql .= "AND status ='".$data['filter_status']."' ";
		}
		if ($data['filter_xeroAccountCode']){
			$sql.= " AND xero_account_code LIKE '" . $this->db->escape($data['filter_xeroAccountCode']) . "%'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND added_date between  '" .changeDates($data['filter_date_from']) . "' AND '" .changeDates($data['filter_date_to']). "'";
		}	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY terms_name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
		}
		//echo "$sql";
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalPaymentTypeMaster($data) {

		$sort_data = array('name', 'code', 'xero_account_code', 'status', 'added_date');
		
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "payment_type_master WHERE id !='' ";
		if ($data['filter_name']){
			$sql.= " AND name LIKE '" . $this->db->escape($data['filter_name']) . "%'"; 
		}
		if ($data['filter_code']){
			$sql.= " AND code LIKE '" . $this->db->escape($data['filter_code']) . "%'"; 
		}
		if ($data['filter_status'] !='') {	
			$sql .= "AND status ='".$data['filter_status']."' ";
		}
		if ($data['filter_xeroAccountCode']){
			$sql.= " AND xero_account_code LIKE '" . $this->db->escape($data['filter_xeroAccountCode']) . "%'"; 
		}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND added_date between  '" .changeDates($data['filter_date_from']) . "' AND '" .changeDates($data['filter_date_to']). "'";
		}	
		// echo $sql; die;
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}
?>