<?php
class ModelSettingRoute extends Model {

	public function getTotalRoute($data)
	{
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "route";
		
		if (!empty($data['filter_name'])){
				$sql.= " where name LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}
		if (!empty($data['filter_code'])){
				$sql.= " where code LIKE '%" . $this->db->escape($data['filter_code']) . "%'"; 
			}	
		$query = $this->db->query($sql);
		return $query->row['total'];
	}

	public function getroutecode($code){
  		$sql = "SELECT count(*) as count FROM " . DB_PREFIX . "route where code= '".$code."' ";
		$data = $this->db->query($sql)->row;
		return $data['count'];
	}
	public function getRouteZipcode($zipcodes,$code =''){
		$zipcode = join("','",$zipcodes);
		if($code != '') {
			$routesql = "SELECT id FROM " . DB_PREFIX . "route where code= '".$code."' ";
			$data = $this->db->query($routesql)->row;
			$sql = "SELECT count(*) as count FROM " . DB_PREFIX . "route_ziprange where zip_code in ('".$zipcode."') AND route_id != '".$data['id']."'";
		} else{
  			$sql = "SELECT count(*) as count FROM " . DB_PREFIX . "route_ziprange where zip_code in ('".$zipcode."') ";
		}
		$data = $this->db->query($sql)->row;
		return $data['count'];
	}
	public function getRouteZip($zipcode,$getcode =''){
		if($getcode != '') {
			$routesql = "SELECT id FROM " . DB_PREFIX . "route where code= '".$getcode."' ";
			$data = $this->db->query($routesql)->row;
			$sql = "SELECT count(*) as count FROM " . DB_PREFIX . "route_ziprange where zip_code ='".$zipcode."' AND route_id != '".$data['id']."'";
		} else{
  			$sql = "SELECT count(*) as count FROM " . DB_PREFIX . "route_ziprange where zip_code = '".$zipcode."' ";
		}
		$data = $this->db->query($sql)->row;
		return $data['count'];
	}
	public function getroutedata($code){
  		$sql = "SELECT * FROM " . DB_PREFIX . "route where code= '".$code."'";
		$data['route'] = $this->db->query($sql)->row;
  		$sql2 = "SELECT zip_code FROM " . DB_PREFIX . "route_ziprange where route_id= '".$data['route']['id']."'";
		$data['zipcodes'] = $this->db->query($sql2)->rows;
		return $data;
	}
	
	public function getRoute($data){
		if ($data) {

			/*$sql = "SELECT rte.name, rte.code, rte.description, rte.status, rte.addedon, rzr.zip_code FROM " . DB_PREFIX . "route rte LEFT JOIN " . DB_PREFIX . "route_ziprange rzr ON (rte.id = rzr.route_id) ";*/

      		/*$sql = "SELECT * FROM " . DB_PREFIX . "route";*/

      		$sql ="SELECT R.id,R.name,R.code,R.description,R.addedon,R.status,GROUP_CONCAT(Z.zip_code ORDER BY Z.zip_code ASC SEPARATOR ', ') as zip_code FROM `tbl_route` as R LEFT JOIN tbl_route_ziprange as Z on R.id = Z.route_id GROUP BY R.id";
			
			if (!empty($data['filter_name'])){
				$sql.= " where R.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'"; 
			}

			if (!empty($data['filter_code'])){
				$sql.= " where R.code LIKE '%" . $this->db->escape($data['filter_code']) . "%'"; 
			}
			if (isset($data['sort'])) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY R.addedon";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql)->rows;
			/*foreach ($query as $value) {
				$zipquery = $this->db->query("SELECT R.id,R.name,R.code,R.description,GROUP_CONCAT(Z.zip_code ORDER BY Z.zip_code ASC SEPARATOR ', ') as zipcodes FROM `tbl_route` as R LEFT JOIN tbl_route_ziprange as Z on R.id = Z.route_id GROUP BY R.id")->row;
			}*/
			return $query;
			}
	}

	public function addRoute($data)
	{
		$currentDate = date("Y-m-d H:i:s");
		$insertSql = "INSERT INTO " . DB_PREFIX ."route(id,name,code,description,status,addedby,addedon,modifiedby,modifiedon) VALUES ('', '".$this->db->escape($data['name'])."','".$this->db->escape($data['code'])."','".$this->db->escape($data['description'])."','".$data['status']."',1,'".$currentDate."',1,'".$currentDate."')";
		$this->db->query($insertSql);
		$route_id = $this->db->getLastId();
		$zipcode = array_unique($data['zipcode']);
		foreach ($zipcode as $value) {
			$insertZipSql = "INSERT INTO " . DB_PREFIX ."route_ziprange(id,route_id,zip_code,status,addedby,addedon,modifiedby,modifiedon) VALUES('', '".$this->db->escape($route_id)."', '".$this->db->escape($value)."','".$this->db->escape($data['status'])."',1,'".$currentDate."',1,'".$currentDate."') ";

			$this->db->query($insertZipSql);
		}
		return $route_id;
	}

	public function editRoute($data,$code)
	{
		$currentDate = date("Y-m-d H:i:s");
		$sql="UPDATE " . DB_PREFIX . "route SET
									name		= '".$this->db->escape($data['name'])."',
									code		= '".$this->db->escape($data['code'])."',
									description	= '".$this->db->escape($data['description'])."',
									status	    = '".$data['status']."',
									modifiedby	= '1',
									modifiedon 	= '".$currentDate."'
									WHERE code = '".$code."'";
		$query = $this->db->query($sql);
		$sql1 = $this->db->query("SELECT id FROM " . DB_PREFIX . "route where code= '".$code."'")->row;
		$route_id = $sql1['id'];
		$sql1 = $this->db->query("DELETE FROM " . DB_PREFIX . "route_ziprange WHERE route_id = '".$route_id."'");
		$zipcode = array_unique($data['zipcode']);
		foreach ($zipcode as $value) {
			$insertZipSql = "INSERT INTO " . DB_PREFIX ."route_ziprange(id,route_id,zip_code,status,addedby,addedon,modifiedby,modifiedon) VALUES('', '".$route_id."', '".$value."','".$data['status']."',1,'".$currentDate."',1,'".$currentDate."') ";

			$this->db->query($insertZipSql);
		}
	}
		
}
?>