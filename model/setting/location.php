<?php
class ModelSettingLocation extends Model {


	public function addLocation($data)
	{
		$userId	= $this->session->data['user_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "location (location_code,location_name,address1,address2,country,postcode,phone,created_by,created_date,type,email,business_reg_no) VALUES('".$this->db->escape($data['code'])."','".$this->db->escape($data['name'])."','".$this->db->escape($data['address1'])."','".$this->db->escape($data['address2'])."','".$this->db->escape($data['country'])."','".$this->db->escape($data['postcode'])."','".$this->db->escape($data['phone'])."','".$userId."',curdate(),'".$this->db->escape($data['type'])."','".$data['email']."','".$data['business_reg_no']."')");

		$location_id = $this->db->getLastId();
		return $location_id;
	}

	public function editLocation($data,$location_id)
	{
		$userId	= $this->session->data['user_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "location SET 
				location_code = '".$this->db->escape($data['code'])."',
				location_name  = '".$this->db->escape($data['name'])."',
				address1       = '".$this->db->escape($data['address1'])."',
				address2       = '".$this->db->escape($data['address2'])."',
				country        = '".$this->db->escape($data['country'])."',
				postcode       = '".$this->db->escape($data['postcode'])."',
				phone          = '".$this->db->escape($data['phone'])."',
				email          = '".$this->db->escape($data['email'])."',
				business_reg_no= '".$this->db->escape($data['business_reg_no'])."',
				modified_date  =  curdate(), 
				modified_by    =  '".$userId."',
				type           = '".$this->db->escape($data['type'])."' 
				WHERE location_id = '".$location_id."'");

	}

	public function getTotalLocation()
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "location");

		return $query->row['total'];		
	}

	public function getLocation($location_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		
		return $query->row;
	}

	public function getLocationsCode($location_code='')
	{
		if($location_code=='HQ' || $location_code==''){
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location ORDER BY location_id DESC");
		}else{
			$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "location where location_code='".$location_code."'");
		}
		
		return $query->rows;
	}

	public function getLocations($data = array()) 
	{		
			
			$sql = "SELECT * FROM " . DB_PREFIX . "location";
			
			$sort_data = array(
				'location_name',
				'country'
			);	
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				
				$sql .= " ORDER BY location_name";	
			} else {
				$sql .= " ORDER BY country";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}		
			$query = $this->db->query($sql);

			return $query->rows;
			
		}


	public function deleteLocation($location_id)
	{
		$tableDetails = $this->getLocation($location_id);
		$strLogDetails = array(
			'type' 		=> 'Location',
			'id'   		=> $location_id,
			'code' 		=> $tableDetails['location_code'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
	
}
?>