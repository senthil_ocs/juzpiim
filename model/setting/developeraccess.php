<?php 
class ModelSettingDeveloperaccess extends Model {

	public function purchaseInvoiceRevert($postData) {

		$store_id	= $this->session->data['company_id'];
		$invoice_no = $postData['purchase_invoice_no']; 
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase_invoice_header	 WHERE transaction_no = '" .$invoice_no . "'");
		if($query->row) {

			$purchase_invoice_header = $query->row;
			$purchase_id = $purchase_invoice_header['purchase_id'];
			$transaction_no = $purchase_invoice_header['transaction_no'];
			$location_code = $purchase_invoice_header['location_code'];
			$purchase_trans_id = $purchase_invoice_header['purchase_trans_no'];
			
			if($purchase_invoice_header) {
				$purchase_invoice_details = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase_invoice_details WHERE purchase_id = '" .$purchase_id. "'")->rows;

				if($purchase_invoice_details) {
					$allow = 0;
					foreach ($purchase_invoice_details as $piDetails) {
						$balance = $this->db->query("SELECT (ps.sku_qty - pd.quantity) as balance FROM ".DB_PREFIX."product_stock as ps LEFT JOIN ".DB_PREFIX."purchase_invoice_details as pd on pd.product_id = ps.product_id where ps.product_id = '".$piDetails['product_id']."' and ps.location_Code='".$purchase_invoice_header['location_code']."' and pd.transaction_no = '".$purchase_invoice_header['transaction_no']."' ")->row['balance'];
						if ($balance >= 0) {
							$allow = 1;
						} else {
							$allow = 0;
						}					
					}
					if($allow == 1) {
						foreach ($purchase_invoice_details as $key => $product_details) {
						
							$product_id = $product_details['product_id']; 
							$quantity = $product_details['quantity']; 

							$product_stock_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_stock WHERE location_Code = '".$location_code."' and product_id = '".$product_id."'");
							$product_stock_data  = $product_stock_query->row;	
							if($product_stock_data != '') {
								$updated_stock_qty = $product_stock_data['sku_qty'] - $quantity;
								if($updated_stock_qty >= 0) {
									$tbl_product_stock_update_query = $this->db->query("update  " . DB_PREFIX . "product_stock set sku_qty = '".$updated_stock_qty."'  where location_Code = '".$location_code."' and product_id = '".$product_id."' " );

									$purchase_to_product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase_to_product WHERE purchase_id = '".$purchase_trans_id."' and product_id = '".$product_id."'");

									$purchase_to_product_data = $purchase_to_product_query->row; 
									if($purchase_to_product_data) {
										$purchase_to_product_qty = $purchase_to_product_data['received_qty'] - $quantity;
										if($purchase_to_product_qty >= 0) {

											//$purchase_to_product_qty = $purchase_to_product_data['received_qty'] - $quantity;
											if($purchase_to_product_qty == 0) {
												$status = 0; // default
											} else {
												$status = 1; // partial
											}
											
											$purchase_to_product_query = $this->db->query("update  " . DB_PREFIX . "purchase_to_product set  received_qty = ".(int)$purchase_to_product_qty.", conversion_status = '".$status."'  WHERE purchase_id = '".$purchase_trans_id."' and product_id = '".$product_id."'");
										}
									}
								}
							}
						}	

						$purchase_count = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase_invoice_header WHERE purchase_trans_no = '" .$purchase_trans_id . "'")->num_rows;

						if($purchase_count == 1 ) { // invoice not present set aas 0
							$purchase_update_query = $this->db->query("update " . DB_PREFIX . "purchase set isinvoice = '0' WHERE purchase_id = '" .$purchase_trans_id. "'");
						}
							/* Delete Purchase invoice details*/ 
							$purchase_invoice_details_delete_query = $this->db->query("Delete FROM " . DB_PREFIX . "purchase_invoice_details WHERE purchase_id = '" .$purchase_id. "'");

							$query = $this->db->query("DELETE FROM " . DB_PREFIX . "purchase_invoice_header WHERE purchase_id = '" .$purchase_id . "'");

							$resultant = array('status' => 200, 'message' => 'Purchase invoice revert successfully!');
						
					} else {
						$resultant = array('status' => 201, 'message' => 'Not enought qty to revert this invoice!');
					}	
		   } else  {
			$resultant = array('status' => 201, 'message' => 'Purchase invoice is not found!');
		   }
		} 
		}
		return $resultant;
	}

	public function purchaseOrderRevert($postData) {

		$store_id	= $this->session->data['company_id'];
		$purchase_no = $postData['purchase_order_no']; 
		$purchase_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "purchase WHERE transaction_no = '" .$purchase_no. "' and deleted = '1' ");
		$purchase_data = $purchase_query->row;
		if($purchase_data) {
			$purchase_update_query = $this->db->query("update " . DB_PREFIX . "purchase set deleted = '0' WHERE transaction_no = '" .$purchase_no. "' ");
			return array('status' => 200, 'message' => 'Purchase order revert successfully!');
		}else{

			return array('status' => 201, 'message' => 'Purchase order not found!');
		}
	}

}
?>