<?php
class ModelSettingPromotions extends Model {

	public function getLastTerminalCode(){
		//$terminal_code	= $this->session->data['terminal_code']; 

		$query = $this->db->query("SELECT MAX(terminal_code) as terminalcode FROM " . DB_PREFIX . "terminal_configuration");
		if(!empty($query->row)){
			$termcode=$query->row['terminalcode'];

		}else{
			$termcode=1;
		}
		
		return $termcode;
	}
	public function addPromotions($data){
		$date= date('Y-m-d');
		$Days = implode(',', $_POST['days']); 
		$locations = implode(',', $data['location']);
	    $res = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "promotions WHERE PromotionCode = '" .$data['promotion_code'] . "'" );
       	if(empty($data['promotion_price'])){
       		$data['promotion_price'] = 0;
       	}
       $sql = "INSERT INTO " . DB_PREFIX . "promotions(PromotionCode,Description,FromDate,ToDate,FromTime,ToTime,PromotionPerc,PromotionPrice,CreatedBy,Createdon,ModyfiedBy,ModyfiedOn) VALUES ('".$data['promotion_code']."','".$data['description']."','".$data['from_date']."','".$data['to_date']."','".$data['from_time']."','".$data['to_time']."','".$data['promotion_prescription']."','".$data['promotion_price']."','".$this->session->data['username']."', curdate(),'".$this->session->data['username']."', curdate())";

		$this->db->query("INSERT INTO " . DB_PREFIX . "promotions_day(PromotionCode,day) VALUES ('".$data['promotion_code']."','".$Days."')");

		for($l=0;$l<count($data['location']);$l++){
			if($data['location'][$l]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "promotion_locations(promotion_code,location_code) VALUES ('".$data['promotion_code']."','".$data['location'][$l]."')");
			}
		}

		for($i=0;$i<count($data['sku']);$i++){
			if($data['sku'][$i]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "promotions_sku(PromotionCode,sku,name,sku_price,promotion_price) VALUES ('".$data['promotion_code']."','".$data['sku'][$i]."','".$data['name'][$i]."','".$data['sku_price'][$i]."','".$data['pro_price'][$i]."')");
			}
		}
		$query =$this->db->query($sql); 
		$res = $this->db->getLastId();
		
        return $res;
	}

	/*public function addTerminal($data){
			
		  $sql = "INSERT INTO " . DB_PREFIX . "terminal_configuration (terminal_code,terminal_name,system_name,cash_machine,cash_machine_port,coin_machine,coin_machine_port,Cust_display,Cust_Display_Port,Cust_Display_Type,Cust_Display_Msg1,Cust_Display_Msg2,speaker_enabled,weighingscale_enabled,weighingscale_port,createdon,createdby) VALUES ('".$data['terminal_code']."','".$data['terminal_name']."','".$data['system_name']."','".$data['cash_machine']."','".$data['cash_machine_port']."','".$data['coin_machine']."','".$data['coin_machine_port']."','".$data['Cust_display']."','".$data['Cust_Display_Port']."','".$data['Cust_Display_Type']."','".$data['Cust_Display_Msg1']."','".$data['Cust_Display_Msg2']."','".$data['speaker_enabled']."','".$data['weighingscale_enabled']."','".$data['weighingscale_port']."',curdate(),1)";
		 $query = $this->db->query($sql);

		$terminal_code = $this->db->getLastId();
		return $terminal_code;
	}*/
	public function getTotalPromotions($data){

      	$sql="select P.PromotionCode,P.Description,P.FromDate,P.ToDate,P.FromTime,P.ToTime,P.PromotionPerc,P.PromotionPrice,PL.location_code from tbl_promotions as P LEFT JOIN tbl_promotion_locations as PL on PL.promotion_code = P.PromotionCode ";
			if($data['filter_sku']!=''){
				$sql .=" LEFT JOIN tbl_promotions_sku as PSKU on PSKU.PromotionCode = P.PromotionCode";
			}

			$sql .=" where PL.location_code ='" .$data['filter_location']. "'";   

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND  P.FromDate >='" .changeDate($data['filter_date_from']). "' AND  P.ToDate <='" .changeDate($data['filter_date_to']). "'";
			}

			if($data['filter_sku']!=''){
				$sql .= " AND PSKU.name LIKE '%".$data['filter_sku']."%'";
			}
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY P.createdon";	
			}
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			$query = $this->db->query($sql);
		return $query->num_rows;

	} 


	public function getPromotions($data){
		if ($data) {

			$sql="select P.PromotionCode,P.Description,P.FromDate,P.ToDate,P.FromTime,P.ToTime,P.PromotionPerc,P.PromotionPrice,PL.location_code from tbl_promotions as P LEFT JOIN tbl_promotion_locations as PL on PL.promotion_code = P.PromotionCode ";
			if($data['filter_sku']!=''){
				$sql .=" LEFT JOIN tbl_promotions_sku as PSKU on PSKU.PromotionCode = P.PromotionCode";
			}

			$sql .=" where PL.location_code ='" .$data['filter_location']. "'";   

			if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
				$sql .= " AND  P.FromDate >='" .changeDate($data['filter_date_from']). "' AND  P.ToDate <='" .changeDate($data['filter_date_to']). "'";
			}

			if($data['filter_sku']!=''){
				$sql .= " AND PSKU.name LIKE '%".$data['filter_sku']."%'";
			}
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY P.createdon";	
			}
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);
			return $query->rows;
		} 	

	}
	 
	public function editTerminal($data,$PromotionCode)
	{
		$Days = implode(',', $data['days']); 
		$locations = implode(',', $data['location']); 
		
	     $sql="UPDATE " . DB_PREFIX . "promotions SET 
									PromotionCode 		= '".$data['promotion_code']."',
									Description 		= '".$data['description']."',
									FromDate		    = '".date("Y-m-d",strtotime(str_replace('/', '-',$data['from_date'])))."',
									ToDate    			= '".date("Y-m-d",strtotime(str_replace('/', '-',$data['to_date'])))."',
									FromTime 			= '".$data['from_time']."',
									ToTime				= '".$data['to_time']."',
									PromotionPerc 		= '".(int)$data['promotion_prescription']."',
									PromotionPrice    	= '".(int)$data['promotion_price']."'
									WHERE PromotionCode = '".$PromotionCode."'";   

		 $query = $this->db->query($sql); 
		 $updatesql="UPDATE " . DB_PREFIX . "promotions_day SET 									
		 									day = '".$Days."'	 WHERE PromotionCode ='".$data['promotion_code']."'
		 									"; 
		 $query = $this->db->query($updatesql);
		 	$this->db->query("DELETE FROM " . DB_PREFIX . "promotions_sku WHERE PromotionCode = '".$data['promotion_code']."'");
		 for($j=0;$j<count($data['exist_sku_']);$j++){
			if($data['exist_sku_'][$j]){
			$this->db->query("INSERT INTO " . DB_PREFIX . "promotions_sku(PromotionCode,sku,name,sku_price,promotion_price) VALUES ('".$data['promotion_code']."','".$data['exist_sku_'][$j]."','".$data['exist_name'][$j]."','".$data['exist_sku_price'][$j]."','".$data['exist_pro_price'][$j]."')");
			}
	}

	$this->db->query("DELETE FROM " . DB_PREFIX . "promotion_locations WHERE promotion_code = '".$data['promotion_code']."'");
	for($l=0;$l<count($data['location']);$l++){
		if($data['location'][$l]){
		$this->db->query("INSERT INTO " . DB_PREFIX . "promotion_locations(promotion_code,location_code) VALUES ('".$data['promotion_code']."','".$data['location'][$l]."')");
		}
	}
	 for($i=0;$i<count($data['sku']);$i++){
		if($data['sku'][$i]){
		$this->db->query("INSERT INTO " . DB_PREFIX . "promotions_sku(PromotionCode,sku,name,sku_price,promotion_price) VALUES ('".$data['promotion_code']."','".$data['sku'][$i]."','".$data['name'][$i]."','".$data['sku_price'][$i]."','".$data['pro_price'][$i]."')");
		}
	}
	
	}
	public function deleteTerminal($terminal_code)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "promotions WHERE PromotionCode = '" . $terminal_code . "'");
	}

	public function getterminal($terminal_code)
	{
		
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "promotions WHERE PromotionCode = '" . $terminal_code . "'");
		return $query->row;
	}
	public function getPromotionDetailsById($promotionId)
	{
		//$companyId	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "promotions WHERE PromotionCode = '" .$promotionId . "'");
		return $query->row;
	}
	public function getDaysDetails($promotionId){

	    $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "promotions_day WHERE PromotionCode = '".$promotionId."'");
		return $query->row;
	}
	public function getSkuDetailsById($promotionId){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "promotions_sku WHERE PromotionCode = '".$promotionId."'");
		return $query->rows;
	}
	public function getSkuCount($promotionId){
		$query = $this->db->query("SELECT COUNT(*) as totalItems FROM " . DB_PREFIX . "promotions_sku WHERE promotion_id = '".$promotionId."'");
		return $query->row;
	}

	public function deleteAllpromotion($promotionId)
	{
		$query1 = $this->db->query(" DELETE FROM " .DB_PREFIX."promotions WHERE PromotionCode='".$promotionId."' ");
		$query2 = $this->db->query(" DELETE FROM " .DB_PREFIX."promotions_day WHERE PromotionCode='".$promotionId."' ");
		$query3 = $this->db->query(" DELETE FROM " .DB_PREFIX."promotions_sku WHERE PromotionCode='".$promotionId."' ");
		$query3 = $this->db->query(" DELETE FROM " .DB_PREFIX."promotion_locations WHERE promotion_code='".$promotionId."' ");
		if($query1){
			return true;
		}
	}
	
	public function viewPromotions($promotionId){
		 $query = $this->db->query("SELECT  P.*, PD.* FROM  " . DB_PREFIX . "promotions as P, " . DB_PREFIX . "promotions_day as PD WHERE  P.PromotionCode = PD.PromotionCode and P.PromotionCode = '" .$promotionId . "'" );
		$data =  $query->row;
		return $data;
	}
	public function viewPromotionSku($promotionId){
		 $query = $this->db->query("SELECT  * FROM " . DB_PREFIX . "promotions_sku WHERE PromotionCode = '" .$promotionId . "'" );
		$data =  $query->rows;
		return $data;
	}
	public function getpromationLocations($promation_code){
		$query = $this->db->query("SELECT  location_code FROM " . DB_PREFIX . "promotion_locations WHERE promotion_code = '" .$promation_code . "'" );
		$data =  $query->rows;
		return $data;
	}

}
?>