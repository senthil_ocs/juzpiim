<?php
class ModelSettingGeoZone extends Model {
	public function addGeoZone($data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("INSERT INTO " . DB_PREFIX . "geo_zone (name,company_id,description,date_added) VALUES ('" . $this->db->escape($data['name']) . "','".$company_id."','" . $this->db->escape($data['description']) . "',curdate())"); 
			
		$geo_zone_id = $this->db->getLastId();

		if (isset($data['zone_to_geo_zone'])) {
			foreach ($data['zone_to_geo_zone'] as $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "zone_to_geo_zone (country_id,company_id,zone_id,geo_zone_id,date_added) VALUES ('"  . (int)$value['country_id'] . "','".$company_id."','"  . (int)$value['zone_id'] . "','"  .(int)$geo_zone_id . "',curdate())"); 
			}
		}

		$this->cache->delete('geo_zone');
	}

	public function editGeoZone($geo_zone_id, $data) {
		$company_id	= $this->session->data['company_id'];
		$this->db->query("UPDATE " . DB_PREFIX . "geo_zone SET 
			name = '" . $this->db->escape($data['name']) . "'
			, company_id = '".$company_id."'
			, description = '" . $this->db->escape($data['description']) . "'
			, date_modified = curdate() 
			WHERE geo_zone_id = '" . (int)$geo_zone_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");

		if (isset($data['zone_to_geo_zone'])) {
			foreach ($data['zone_to_geo_zone'] as $value) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "zone_to_geo_zone (country_id,company_id,zone_id,geo_zone_id,date_added) VALUES('"  . (int)$value['country_id'] . "','".$company_id."','"  . (int)$value['zone_id'] . "','"  .(int)$geo_zone_id . "',curdate())");
			}
		}

		$this->cache->delete('geo_zone');
	}

	public function deleteGeoZone($geo_zone_id) {
		$company_id	= $this->session->data['company_id'];
		$tableDetails = $this->getGeoZone($geo_zone_id);
		$strLogDetails = array(
			'type' 		=> 'Geo Zones',
			'id'   		=> $geo_zone_id,
			'code' 		=> $tableDetails['name'],
			'userid'   	=> $this->session->data['user_id'],
			'username' 	=> $this->session->data['username'],
			'date'		=> date('Y-m-d H:i:s'),
		);
		$this->insertlog($strLogDetails);
		$this->db->query("DELETE FROM " . DB_PREFIX . "geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");

		$this->cache->delete('geo_zone');
	}

	public function getGeoZone($geo_zone_id) {
		$company_id	= $this->session->data['company_id'];
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");

		return $query->row;
	}

	public function getGeoZones($data = array()) {
		$company_id	= $this->session->data['company_id'];
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "geo_zone WHERE company_id = '".$company_id."'";

			$sort_data = array(
				'name',
				'description'
			);	

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY name";	
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	

				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$geo_zone_data = $this->cache->get('geo_zone');

			if (!$geo_zone_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone WHERE company_id = '".$company_id."' ORDER BY name ASC");

				$geo_zone_data = $query->rows;

				$this->cache->set('geo_zone', $geo_zone_data);
			}

			return $geo_zone_data;				
		}
	}

	public function getTotalGeoZones() {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "geo_zone WHERE company_id = '".$company_id."'");

		return $query->row['total'];
	}	

	public function getZoneToGeoZones($geo_zone_id) {
		$company_id	= $this->session->data['company_id'];	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");

		return $query->rows;	
	}		

	public function getTotalZoneToGeoZoneByGeoZoneId($geo_zone_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$geo_zone_id . "' AND company_id = '".$company_id."'");

		return $query->row['total'];
	}

	public function getTotalZoneToGeoZoneByCountryId($country_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE country_id = '" . (int)$country_id . "' AND company_id = '".$company_id."'");

		return $query->row['total'];
	}	

	public function getTotalZoneToGeoZoneByZoneId($zone_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "zone_to_geo_zone WHERE zone_id = '" . (int)$zone_id . "' AND company_id = '".$company_id."'");

		return $query->row['total'];
	}

	public function getZones()
	{
		$company_id = $this->session->data['company_id'];

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone WHERE company_id = '".$company_id."'");

		return $query->rows;
	}
	public function insertlog($data){
		$this->db->query("INSERT INTO " . DB_PREFIX . "delete_log (deletetype,deleteId,deletedbyId,deletedbyname,createdon,code) VALUES ('".$data['type']."','".$data['id']."','".$data['userid']."','".$data['username']."','".$data['date']."','".$data['code']."')");		 
	}
}
?>