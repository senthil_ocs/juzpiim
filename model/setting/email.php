<?php
class ModelSettingEmail extends Model {

	public function getTotalEmail($data)
	{
      	$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "email_settings";
		
		if (!empty($data['filter_report_id'])){
				$sql.= " where report_id LIKE '%" . $this->db->escape($data['filter_report_id']) . "%'"; 
			}
		if (!empty($data['filter_email_id'])){
				$sql.= " where email_id LIKE '%" . $this->db->escape($data['filter_email_id']) . "%'"; 
			}
		
		if($data['filter_date_from']!='' && $data['filter_date_to']!=''){
			$sql .= " AND createdon between  '" .$this->db->escape($data['filter_date_from']) . "' AND '" .$this->db->escape($data['filter_date_to']). "'";
		}	
		
		$query = $this->db->query($sql);
		
		
		return $query->row['total'];
	}

	public function getEmail($data){
		if ($data) {

			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "email_settings";
			
			if (!empty($data['filter_report_id'])){
				$sql.= " where report_id LIKE '%" . $this->db->escape($data['filter_report_id']) . "%'"; 
			}

			if (!empty($data['filter_email_id'])){
				$sql.= " where email_id LIKE '%" . $this->db->escape($data['filter_email_id']) . "%'"; 
			}
			if (isset($data['sort'])) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY createdon";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
			
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}					

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				//$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];;
			}

			$query = $this->db->query($sql);
			//printArray($query);exit;
			return $query->rows;
			}
	}


	public function getEmailById($report_id)
	{
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "email_settings WHERE report_id = '" .$report_id. "'");
		return $query->row;
	}

	public function addEmail($data)
	{ 
		 $insertSql = "INSERT INTO " . DB_PREFIX ."email_settings(report_name,email_id,active,createdby,createdon,modifyby,modifyon) VALUES ('".$this->db->escape($data['report_name'])."','".$data['email_id']."','".$data['active']."',1,curdate(),1,curdate())";
		//echo $insertSql; exit;
		$this->db->query($insertSql);
		$report_id = $this->db->getLastId();
		return $report_id;
	}

	public function editEmail($data,$report_id)
	{
		
		$sql="UPDATE " . DB_PREFIX . "email_settings SET
									report_name		= '".$this->db->escape($data['report_name'])."',
									email_id		= '".$data['email_id']."',
									active			= '".$data['active']."',
									modifyby		= '1',
									modifyon 			= curdate()
									WHERE report_id = '".$report_id."'";
		$query = $this->db->query($sql);
	}


		public function deleteEmail($report_id)
		{
			$strLogDetails = array(
				'type' 		=> 'Email',
				'id'   		=> $report_id,
				'email' 		=> $email_id,
				'userid'   	=> $this->session->data['user_id'],
				'username' 	=> $this->session->data['username'],
				'date'		=> date('Y-m-d H:i:s'),
			);
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "email_settings WHERE report_id = '" . $report_id . "'");
		}


		public function getEmailByReportID($report_id)
		{
			$where = "";
			if($report_id!=''){
				$where = " AND report_id != '".$report_id."'";
			}
			$query = $this->db->query("SELECT count(*) as cnt FROM " . DB_PREFIX . "email_settings WHERE report_id = '".$report_id."' ".$where."");

			return $query->row;
		}
		public function getEmailDetails(){
			return $this->db->query("SELECT * FROM ".DB_PREFIX."email_settings WHERE active='1' AND report_name='Cancel' ")->rows;
		}
		
}
?>