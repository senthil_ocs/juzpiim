<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include("includes/common.php");

// include Payment
require 'payment.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<title>Stripe Payment Gateway</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<style type="text/css">
		.header {
			background-color: #160e87;
			color:#fff;
			height:70px;
		}
		.footer {
			background-color: #160e87;
			color: #fff;
			position: fixed;
			width: 100%;
			height: 50px;
			left: 0;
			right: 0;
			bottom: 0;
			text-align: center;
		}
		.section {
			margin: 10px;
		}
		.required {
			color: red;
			padding-left: 5px;

		} 
		.mobile_view {
			padding-bottom: 40px;

		}
	</style>
</head>
<body>
<header>
	<div class="header">
	</div>
</header>
<div class="container mobile_view">
	<section class="section">
		<form action="./stripePayment.php" class="form-horizontal" method="POST" id="payment-form">
			<input type="hidden" required name="order_id" value="<?php echo $_POST['order_id']; ?>">
              	<input type="hidden" name="customer_id" value="<?php echo $_POST['customer_id']; ?>">
              	<input type="hidden" name="delivery_man_id" value="<?php echo $_POST['delivery_man_id']; ?>">
              	<input type="hidden" name="payment_date" value="<?php echo $_POST['payment_date']; ?>">
              	<input type="hidden" name="payment_type" value="<?php echo $_POST['payment_type']; ?>">
              	<input type="hidden" name="do_no" value="<?php echo $_POST['do_no']; ?>">
              	<input type="hidden" name="payment_method" value="<?php echo $_POST['payment_method']; ?>">
			<fieldset>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="accountNumber">Payment Amount<span class="required">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="amount" value="<?php echo isset($_REQUEST['amount'])? $_REQUEST['amount'] : $_REQUEST['grand_total']; ?>" maxlength="10" placeholder="Enter the amount" readonly required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="reference_no">Reference No<span class="required">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control" name="reference_no" maxlength="40" placeholder="Enter the reference no" value="<?php echo isset($_REQUEST['order_id'])? $_REQUEST['order_id'] : ''; ?>" required readonly>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="accountNumber">Card Number<span class="required">*</span></label>
					<div class="col-sm-6">
						<input type="text" class="form-control number-only" size="20" data-stripe="number" maxlength="16" name="cardno" value="" placeholder="Enter the card no" required>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label" for="expirationMonth">Expiration Date<span class="required">*</span></label>
					<div class="col-sm-9">
						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<select class="form-control col-sm-3 col-xs-6" name="exp_month" data-stripe="exp_month" required>
									<option value="">Month</option>
									<option value="01">Jan (01)</option>
									<option value="02">Feb (02)</option>
									<option value="03">Mar (03)</option>
									<option value="04">Apr (04)</option>
									<option value="05">May (05)</option>
									<option value="06">June (06)</option>
									<option value="07">July (07)</option>
									<option value="08">Aug (08)</option>
									<option value="09">Sep (09)</option>
									<option value="10">Oct (10)</option>
									<option value="11">Nov (11)</option>
									<option value="12">Dec (12)</option>
								</select>
							</div>
							<div class="col-xs-6 col-sm-3">
								<select class="form-control" name="exp_year" data-stripe="exp_year" required>
									<option value="">Year</option>
								<?php 
									$current_year = date('y'); 
									$baseyear = substr(date('Y'), 0, 2);
									for ($i = $current_year; $i <= $current_year+25; $i++) {
										echo "<option value='".$i."'>".$baseyear.$i."</option>";
									}
								?>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="cvNumber">Card CVV<span class="required">*</span></label>
					<div class="col-sm-3">
						<input type="text" class="form-control number-only" data-stripe="cvc" name="ccv" maxlength="3" placeholder="123"value="">
					</div> 
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-9">
						<button type="submit" name="pay" id="pay" class="btn btn-primary">Pay Now</button>
					</div>
					<?php if(isset($response)){echo $response;} ?> <div class='col-sm-offset-3 col-sm-9  text-danger payment-errors'></div>
				</div>
			</fieldset>
		</form>
	</section>
</div>
<footer>
<!-- 	<div class="footer"></div> -->
</footer>
<script src="jquery.min.js"></script>
<script type="text/javascript" src="js.stripe.js"></script>
<!-- TO DO : Place below JS code in js file and include that JS file -->
<script type="text/javascript">

	$(function(){
      
  $('.number-only').keyup(function(e) {
        if(this.value!='-')
          while(isNaN(this.value))
            this.value = this.value.split('').reverse().join('').replace(/[\D]/i,'')
                                   .split('').reverse().join('');
    })
    .on("cut copy paste",function(e){
    	e.preventDefault();
    });

});

	/*
	Stripe.setPublishableKey('<?php echo $params['public_test_key']; ?>');

	$(function() {
		var $form = $('#payment-form');
		$form.submit(function(event) {
			// Disable the submit button to prevent repeated clicks:
			$form.find('.submit').prop('disabled', true);

			// Request a token from Stripe:
			Stripe.card.createToken($form, stripeResponseHandler);
			
			// Prevent the form from being submitted:
			return false;
		});
	});

	function stripeResponseHandler(status, response) {
		// Grab the form:
		var $form = $('#payment-form');

		if (response.error) { // Problem!

			// Show the errors on the form:
			$form.find('.payment-errors').text(response.error.message);
			$form.find('.submit').prop('disabled', false); // Re-enable submission

		} else { // Token was created!

			// Get the token ID:
			var token = response.id;

			// Insert the token ID into the form so it gets submitted to the server:
			$form.append($('<input type="hidden" name="stripeToken">').val(token));

			// Submit the form:
			$form.get(0).submit();
		}
	}; */
</script>
</body>
</html>