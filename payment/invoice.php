<?php
include("includes/common.php");
 //error_reporting(E_ALL);

// echo  base64_decode('MjAxMjAwNTg='); die;  /*For Testing only */
$payment_action_url     = HTTP_SERVER.'payment/stripePayment.php';
$invoice_no             = base64_decode($_GET['id']); 
$data['purchaseInfo']   = $objServiceApi->getSalesInvoice($invoice_no);
$data['doHeadder']      = $objServiceApi->getSalesDoHeader(base64_decode($_GET['do_no']));
$data['doDetails']      = $objServiceApi->getDoDetails(base64_decode($_GET['do_no']));
$data['vendorDetail']   = $objServiceApi->getB2BCustomersbyCode($data['purchaseInfo']['customer_code']);
$data['shippingAddress']= $objServiceApi->getShippingAddress($data['purchaseInfo']['shipping_id']);
$data['paymentTypes']   = $objServiceApi->getPaymentTypes();
$data['productDetails'] = $objServiceApi->getSalesInvoiceDetails($data['purchaseInfo']['invoice_no']);

if($_GET['do_no']){
    $data['productDetails'] = $data['doDetails'];
}
$purchase_id      = $data['purchaseInfo']['invoice_no'];
$customer_id      = $data['purchaseInfo']['customer_code'];
$do_no            = $_GET['do_no'];
$delivery_man_id  = $data['doHeadder']['deliveryman_id'];
$paid_amount      = $data['purchaseInfo']['paid_amount'];
$grand_total      = $data['purchaseInfo']['net_total'] - $data['purchaseInfo']['paid_amount'];
$net_total        = $data['purchaseInfo']['net_total'];

$tax_type = $data['purchaseInfo']['tax_type']=='0' ? '(7% Exclusive)':'(7% Inclusive)';
$tax_type = $data['purchaseInfo']['tax_class_id']=='2' ? $tax_type :'(0%)';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Invoice Details for <?php echo $invoice_no; ?></title>
    <link rel='stylesheet' type='text/css' href='css/style.css' />
    <link rel='stylesheet' type='text/css' href='css/responsive.css' />
    <link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
    <script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
    <script type='text/javascript' src='js/example.js'></script>
</head>
<style>
/*body {
  font-family: Verdana, sans-serif;
  margin: 0;
}

* {
  box-sizing: border-box;
}

.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}*/

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 40%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.mySlides img {
    width: 100%;
}
@media only screen and (max-width: 768px) {
    .mySlides img {
        width: 100% !important;
    }
    .modal-content {
        width:90%;
    }
}

</style>
<body>
    <div id="page-wrap">
        <?php if(empty($data['purchaseInfo'])){ ?>
            <div id="customer">
                <h2>Invoice details not available</h2>
            </div>
        <?php } else if($data['purchaseInfo']['payment_status'] !='Paid' && $data['purchaseInfo']['payment_status'] !='Hold' ){ ?>
        <textarea id="header">INVOICE</textarea>
        <div id="identity">
            <div id="customer-title">Invoice Date: <?php echo date('d/m/Y', strtotime($data['purchaseInfo']['invoice_date'])); ?></div>
        </div>
        <div style="clear:both"></div>
        <div id="customer">
            <div id="address">
                <p>Vendor Details:</p>
                <p><?php echo $data['vendorDetail']['cust_code']; ?> - <?php echo $data['vendorDetail']['name']; ?>,</p>
                <p><?php echo $data['shippingAddress']['address1']; ?></p>
                <p><?php echo $data['shippingAddress']['address2']; ?></p>
                <p><?php echo $data['shippingAddress']['city'].' - '.$data['shippingAddress']['country']; ?></p>
                <p>Pincode - <?php echo $data['shippingAddress']['zip']; ?></p>
                <p><?php echo $data['vendorDetail']['email']; ?></p>
            </div>
            <table id="meta">
                <tr>
                    <td class="meta-head">Invoice #</td>
                    <td><?php echo $data['purchaseInfo']['invoice_no']; ?></td>
                </tr>
                <tr>
                    <td class="meta-head">Do No #</td>
                    <td><?php echo $data['doHeadder']['do_no']; ?></td>
                </tr>
                <tr>
                    <td class="meta-head">Ref Date</td>
                    <td><?php echo date('d/m/Y', strtotime($data['purchaseInfo']['header_remarks'])); ?></td>
                </tr>
                <tr>
                    <td class="meta-head">Ref No</td>
                    <td><?php echo $data['purchaseInfo']['reference_no']; ?></td>
                </tr>
            </table>
        </div>
        <table id="items-desktop">
            <thead>
                <tr class="item-row">
                    <th class="sno">S.No</th>
                    <th class="prdcode">SKU</th>
                    <th class="prdname">Product Name</th>
                    <th class="prdqty">Quantity</th>
                    <th class="prdunit">Price(unit)</th>
                    <th class="prdtotal">Total Price</th>
                </tr>
            </thead>
            <tbody>
                <?php
                 if($data['productDetails']){
                    foreach ($data['productDetails'] as $key => $product) { 
                        echo '<tr class="item-row">
                                <td class="sno">'.($key+1).'</td>
                                <td class="prdcode">'.$product['sku'].'</td>
                                <td class="prdname">'.$product['description'].'</td>
                                <td class="prdqty">'.$product['qty'].'</td>
                                <td class="prdunit">'.$product['sku_price'].'</td>
                                <td class="prdtotal">'.$product['sub_total'].'</td>
                            </tr>';
                    }
                } ?>
            </tbody>
             <form action="<?php echo $payment_action_url; ?>" method="post" class="form payment_form_desktop">
                <input type="hidden" required name="order_id" value="<?php echo $purchase_id; ?>">
                <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
                <input type="hidden" name="delivery_man_id" value="<?php echo $delivery_man_id; ?>">
                <input type="hidden" name="payment_date" value="<?php echo date('Y-m-d'); ?>">
                <input type="hidden" name="grand_total" value="<?php echo $grand_total; ?>">
                <input type="hidden" name="do_no" value="<?php echo $_GET['do_no']; ?>">
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Sub Total </td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['sub_total']; ?></div></td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Discount </td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['discount']; ?></div></td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Handling Fee </td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['handling_fee']; ?></div></td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">GST <?php echo $tax_type; ?></td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['gst']; ?></div></td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Total </td>
                    <td class="total-value total-line"><div id="total">$<?php echo $grand_total; ?></div></td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Payment Method </td>
                    <td class="total-value">
                        <div class="form-check">
                            <select class="form-check-input" name="payment_method" id="web_payment_method" style="width: 99%;height: 28px;border-radius: 0px;" onchange="paymentMethodQRCode(this.value)">
                                <?php foreach ($data['paymentTypes'] as $paymentTypes) { ?>
                                    <option value="<?php echo $paymentTypes['code']; ?>" <?php if($paymentTypes['code'] == 'Stripe'){ echo 'Selected'; } ?>><?php echo $paymentTypes['name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Payment Type </td>
                    <td class="total-value">
                        <div style="display: flex; float: right;">
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="web_payment_type" id="exampleRadios1" value="Full" checked>
                              <label class="form-check-label" for="exampleRadios1">
                                Full&nbsp;&nbsp;&nbsp;&nbsp;
                              </label>
                            </div>
                            <div class="form-check">
                              <input class="form-check-input" type="radio" name="web_payment_type" id="exampleRadios2" value="Partial">
                              <label class="form-check-label" for="exampleRadios2">
                                Partial
                              </label>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="1" class="total-line">Amount</td>
                    <td class="total-value"><input name="amount" type="textarea" class="form-control number-only" id="web_amount" maxlength="14" value="<?php echo $grand_total; ?>" readonly></td>
                </tr>
                <tr class="QrCode">
                    <td colspan="2" class="blank"></td>
                    <td colspan="2" class="blank"></td>
                    <td colspan="2" class="blank" align="center" onclick="openModal();currentSlide(1)"> <img src="<?php echo BARCODE_IMAGE; ?>" style="width: 200px;"> </td>
                </tr>
                <tr>
                    <td colspan="2" class="blank"></td>
                    <td colspan="2" class="blank"></td>
                    <td colspan="1" class="blank"></td>
                    <td class="total-value blank">
                        <button type="submit" id="webProceedBtn"  name="payment" value="payment" class="button3">Proceed Payment</button>
                        <button type="submit" name="payment" id="webCompleteBtn" value="cod" class="button3">Complete Payment</button>
                </tr>
            </form>
        </table>
        <form action="<?php echo $payment_action_url; ?>" method="post" class="form payment_form_mobile">
            <input type="hidden" required name="order_id" value="<?php echo $purchase_id; ?>">
            <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
            <input type="hidden" name="delivery_man_id" value="<?php echo $delivery_man_id; ?>">
            <input type="hidden" name="payment_date" value="<?php echo date('Y-m-d'); ?>">
            <input type="hidden" name="grand_total" value="<?php echo $grand_total; ?>">
            <input type="hidden" name="do_no" id="do_no" value="<?php echo $_GET['do_no']; ?>">
        <table id="items-mobile">

            <?php if($data['productDetails']){  $do_total = 0;
                    foreach ($data['productDetails'] as $key => $product) { ?>
                        <?php $product['sub_total'] = $product['qty'] * $product['sku_price'];
                        $do_total += $product['sub_total'];
                        $product['sku'] = $product['sku'] !='' ? $product['sku'] : '-'; ?>
                        <tr class="mbltop">
                                <td class="mblabel"><?php echo $product['description']; ?></td>
                                <td class="mbltile">SKU</td>
                                <td class="mblvle"><?php echo $product['sku']; ?></td>
                                <td class="mbltile">Quantity</td>
                                <td class="mblvle"><?php echo $product['qty']; ?></td>
                                <td class="mbltile">Price(unit)</td>
                                <td class="mblvle"><?php echo $product['sku_price']; ?></td>
                                <td class="mbltile">Total Price</td>
                                <td class="mblvle"><?php echo $product['sub_total']; ?></td>
                            </tr>
                    <?php
                    }
                } ?>
                <tr class="mblbtm">
                    <td class="total-line">Sub Total</td>
                    <td class="total-value total-line"><div id="subtotal">$<?php echo $data['purchaseInfo']['sub_total']; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">Discount Price</td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['discount']; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">Handling Fee</td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['handling_fee']; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">GST <?php echo $tax_type; ?></td>
                    <td class="total-value total-line"><div id="total">$<?php echo $data['purchaseInfo']['gst']; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">Total</td>
                    <td class="total-value total-line"><div id="total">$<?php echo $net_total; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">Do Total</td>
                    <td class="total-value total-line"><div id="total">$<?php echo number_format($grand_total,2); ?></div></td>
                </tr>
                <?php if($paid_amount > 0){ ?>
                <tr class="mblbtm">
                    <td class="total-line">Partially Paid</td>
                    <td class="total-value total-line"><div id="total">$<?php echo $paid_amount; ?></div></td>
                </tr>
                <tr class="mblbtm">
                    <td class="total-line">Balance Payment</td>
                    <td class="total-value total-line"><div id="total">$<?php echo number_format($grand_total,2); ?></div></td>
                </tr>
                <?php } ?>
                <tr class="mblbtm">
                    <td class="total-line" style="width: 40%;"><h3>Payment Method</h3></td>
                    <td style="width: 50%;">
                        <div>
                            <div class="form-check">
                                <select class="form-check-input" name="payment_method" id="payment_method" style="width: 99%;height: 28px;border-radius: 0px;font-size: 20px;" onchange="paymentMethodQRCode(this.value)">
                                    <?php foreach ($data['paymentTypes'] as $paymentTypes) { ?>
                                        <option value="<?php echo $paymentTypes['code']; ?>" <?php if($paymentTypes['code'] == 'Stripe'){ echo 'Selected'; } ?>><?php echo $paymentTypes['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="mblbtm">
                    <td class="total-line" style="width: 40%;"><h3>Payment Type</h3></td>
                    <td class="total-value" style="width: 50%;">
                        <div style="display: flex; align-items: center;">
                            <div class="form-check" style="width: 45%;text-align: left;">
                                <label class="form-check-label" style="float: left;margin-right: 10px;">
                                    <input class="form-check-input" type="radio" name="payment_type" value="Full" checked style="transform: scale(1.6);margin: 5px 0px;">
                                </label>
                                <h2 style="margin: 3px;">Full</h2>
                            </div>
                            <div class="form-check" style="width: 55%;text-align: left;">
                                <label class="form-check-label" style="float: left;margin-right: 10px;">
                                    <input class="form-check-input" id="partialPayment_M" type="radio" name="payment_type" value="Partial" style="transform: scale(1.6);margin: 5px 0px;">
                                </label>
                                <h2 style="margin: 3px;">Partial</h2>
                            </div>
                        </div>
                    </td>
                </tr>
                
                <tr class="mblbtm">
                    <td class="total-line" style="width: 40%;"><h3>Amount</h3> </td>
                    <td class="total-value" style="width: 50%;">
                        <div class="col-xs-4">    
                            <input name="amount" id="mbl_amount" type="number" class="form-control number-only" value="<?php echo $grand_total; ?>" maxlength="14" readonly step=".01" style="height: 25px;font-size: 20px;">
                        </div>
                    </td>
                </tr>
                <tr class="mblbtm QrCode">
                    <td class="total-line" style="width: 40%;"></td>
                    <td colspan="2" class="total-line" style="width: 50%;" onclick="openModal();currentSlide(1)"> <img src="<?php echo BARCODE_IMAGE; ?>" style="width: 100%;"> </td>
                </tr>
                <tr class="mblbtm">
                    <input type="hidden" value="Stripe" name="payment" id="payment_type">
                    <td class="btnDiv text-center" style="float: none;">
                        <button class="button3" id="submitBtn" style="font-size: 20px;">Proceed Payment</button>
                    </td>
                </tr>
            </table>
        </form>
    <?php } else{  ?>
        <div id="customer" style="text-align: center;color: green;">
                <h2>Payment done already</h2>
            </div>
    <?php } ?>

    <div id="myModal" class="modal">
        <span class="close" onclick="closeModal()">&times;</span>
        <div class="modal-content">
            <div class="mySlides">
              <img src="<?php echo BARCODE_IMAGE; ?>">
            </div>
        </div>
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
    $(".QrCode").hide();
    radio_btn     = $('#web_payment_method').val();
    if(radio_btn == 'Stripe'){
        $('#webProceedBtn').show();
        $('#webCompleteBtn').hide();
    } else {
        $('#webProceedBtn').hide();
        $('#webCompleteBtn').show();
    }
});

$(function(){      
    $('.payment_complete').hide();
    $('.number-only').keyup(function(e) {
        if(this.value!='-')
          while(isNaN(this.value))
            this.value = this.value.split('').reverse().join('').replace(/[\D]/i,'').split('').reverse().join('');
    })
    .on("cut copy paste",function(e){
        e.preventDefault();
    });
});

$('input[type=radio][name=web_payment_type]').change(function() {
    
    if($('input[name="web_payment_type"]:checked').val()=='Full'){
        $('input[name=amount]').attr('readonly', 'readonly');
        $('input[name=amount]').val($('input[name=grand_total]').val());
    }else{
        $('input[name=amount]').removeAttr('readonly');
        var mbl_amount = $('#mbl_amount').val();
        $('#web_amount').focus().val('').val(mbl_amount);
    }
});

$('input[type=radio][name=payment_type]').change(function() {
    if($('input[name=payment_type]:checked','form').val()=='Full'){
        $('input[name=amount]').val($('input[name=grand_total]').val());
        $('input[name=amount]').attr('readonly', 'readonly');;
    }else{
        $('input[name=amount]').removeAttr('readonly');
        var mbl_amount = $('#mbl_amount').val();
        $('#mbl_amount').focus().val('').val(mbl_amount);
    }
});

$('#payment_method').change(function() {
    radio_btn = $('#payment_method').val();
    if(radio_btn =='Stripe'){
        $('#submitBtn').html('Proceed Payment');
        $('#payment_type').val('Stripe');
    } else {
        $('#submitBtn').html('Complete Payment');
        $('#payment_type').val('cod');
    }
});

$('#web_payment_method').change(function() {
    radio_btn     = $('#web_payment_method').val();
    if(radio_btn == 'Stripe'){
        $('#webProceedBtn').show();
        $('#webCompleteBtn').hide();
    } else {
        $('#webProceedBtn').hide();
        $('#webCompleteBtn').show();
    }
});

function paymentMethodQRCode(payment_method){
    $(".QrCode").hide();
    if(payment_method == 'PNQR'){
        $(".QrCode").show();
    }
}

$('#submitBtn').click(function(e){
   var grand_total = $('input[name=grand_total]','.form').val();
   var amount      = $('input[name=amount]','.form').val();
    if(parseFloat(grand_total) < parseFloat(amount)){
        swal("Mega Furniture", "Please enter less than grand total");
        $('input[name=amount]').val($('input[name=grand_total]').val());
        return false;
    }
    if(amount < 1){
        swal("Mega Furniture", "Please enter minimum 1");
        $('input[name=amount]').val($('input[name=grand_total]').val());
        return false;
    }
    $(".btnDiv").html('<div class="loader"></div>');
    $(".form").submit();
});

</script>
<script>
function openModal() {
  document.getElementById("myModal").style.display = "block";
}

function closeModal() {
  document.getElementById("myModal").style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
 // var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  /*for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }*/
  slides[slideIndex-1].style.display = "block";
 // dots[slideIndex-1].className += " active";
  //captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
</body>
</html>