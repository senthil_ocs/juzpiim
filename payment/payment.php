<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
/**
 * Stripe - Payment Gateway integration
 * ==============================================================================
 * @version v1.0: stripe-payment-gateway.php
 * @copyright Copyright (c) 2017, http://www.freewebmentor.com
 * @author Prem Tiwari
 * You are free to use, distribute, and modify this software
 * ==============================================================================
 *
 */

// include Stripe
require 'stripe/Stripe.php';

$config = STRIPE_PAYMENT_CONFIG;
$params = array(
		"testmode"   => $config['testmode'],
		"private_live_key" => $config['private_live_key'],
		"public_live_key"  => $config['public_live_key'],
		"private_test_key" => $config['private_test_key'], //$config['public_test_key'],
		"public_test_key"  => $config['public_test_key'] //$config['private_test_key']
	);
if ($params['testmode'] == "on") {
	Stripe::setApiKey($params['private_test_key']);
	$pubkey = $params['public_test_key'];
} else {
	 Stripe::setApiKey($params['private_live_key']);
	$pubkey = $params['public_live_key'];
}

if(isset($_POST['web_payment_type'])){
	$_POST['payment_type'] = $_POST['web_payment_type'];
}
if(isset($_POST['payment']) && $_POST['payment'] == 'cod'){
	$response_status = $objServiceApi->insertCashOnDelivery($_POST);
	header("Location: ".HTTPS_SERVER.'payment/paymentResult.php?id='.base64_encode($_POST['order_id']).'&status=Success&paid_status='.$response_status); 
}

if(isset($_POST['pay']))
{
	$last_id 		= $objServiceApi->insertPaymentTransaction($_POST);
	$amount  		= str_replace(".","",$_POST['amount']);
	$payment_status = '';
	$payment_statuses = 0;
	try { 
		$token = Stripe_Token::create(
						array(
							"card" => array(
								"name" => isset($_POST['name'])?$_POST['name']:'tested',
								"number" => $_POST['cardno'],
								"exp_month" => $_POST['exp_month'],
								"exp_year" => $_POST['exp_year'],
								"cvc" => $_POST['ccv']
								)
						)
					);
		if(isset($token['id']) && $token['id'] !='')
		{
			$amount = str_replace(".","",$_POST['amount']);  // Chargeble amount ['stripeToken']
			$invoiceid = $_POST['reference_no'];                      // Invoice ID
			$description = "Invoice #" . $invoiceid . " - " . $invoiceid;
			try {
				$charge = Stripe_Charge::create(array(
								"amount" => $amount,
								"currency" => $config['currencyCode'],
								"source" =>  $token['id'],
								"description" => $description)
				);
				if ($charge->card->address_zip_check == "fail") {
					throw new Exception("zip_check_invalid");
				} else if ($charge->card->address_line1_check == "fail") {
					throw new Exception("address_check_invalid");
				} else if ($charge->card->cvc_check == "fail") {
					throw new Exception("cvc_check_invalid");
				}
				// Payment has succeeded, no exceptions were thrown or otherwise caught

				$payment_status   = 'success';
				$payment_statuses = 1;
				$response = $charge;
				$error    = $response;
			} catch (Exception $e) {
				
				if ($e->getMessage() == "zip_check_invalid") {
					$error = $e->getMessage(); 
					$payment_status = 'declined';
					$payment_statuses = 0;
				} else if ($e->getMessage() == "address_check_invalid") {
					$error = $e->getMessage(); 
					$payment_status = 'declined';
					$payment_statuses = 0;
				} else if ($e->getMessage() == "cvc_check_invalid") {
					$error = $e->getMessage(); 
					$payment_status = 'declined';
					$payment_statuses = 0;
				} else {
					$error = $e->getMessage(); 
					$payment_status = 'declined';
					$payment_statuses = 0;
				}
			}
		}

	}catch(Stripe_CardError $e) {

		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;

	} catch (Stripe_InvalidRequestError $e) {
		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;
	} catch (Stripe_AuthenticationError $e) {
		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;
	} catch (Stripe_ApiConnectionError $e) {
		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;
	} catch (Stripe_Error $e) {
		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;
	} catch (Exception $e) {
		$error = $e->getMessage(); 
		$payment_status = 'failed';
		$payment_statuses = 0;
	}
	$objArray 		 = array('payment_status' => $payment_statuses,'response' => $error);
	$response_status = $objServiceApi->updatePaymentTransaction($objArray, $last_id);
	$order_id 		 = base64_encode($_POST['order_id']);
	if($payment_statuses == 1) {
		header("Location: ".HTTPS_SERVER.'payment/paymentResult.php?id='.$order_id.'&status=Success'); 
	} else{
		header("Location: ".HTTPS_SERVER.'payment/paymentResult.php?id='.$order_id.'&status=Error&order_id='.$_POST['order_id']); 
	}
}
?>