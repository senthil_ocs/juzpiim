<?php 

include("includes/common.php");

	$amount     = '';
	$invoice_no = '';
	$admin_no   = '';
	$name       = '';
	
	if(isset($_GET['id']) && $_GET['id'] !=''){
		
		$purchaseInfo= $objServiceApi->getSalesInvoice(base64_decode($_GET['id']));
		$customer    = $objServiceApi->getB2BCustomersbyCode($purchaseInfo['customer_code']);
		$company     = $objServiceApi->getCompannyDetails();
		$payment     = $objServiceApi->getTotalPaidAmount(base64_decode($_GET['id']));
		$amount 	 = number_format($payment,2);
		$invoice_no  = base64_decode($_GET['id']);
		$admin_no 	 = $company['email'];
		$name 		 = $customer['name'];
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex">
	<title>Stripe Payment Gateway</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<style type="text/css">

		.fa-check-circle{
			font-size:100px;
			color:#0fad00; 
		}
		.fa-times-circle{
			font-size:100px;
			color:#D8000C; 
		}
		.success{
			color:#0fad00;
		}
		.danger{
			color:#D8000C;
		}
		.header {
			background-color: #160e87;
			color:#fff;
			height:70px;
		}
		.footer {
			background-color: #160e87;
			color: #fff;
			position: fixed;
			width: 100%;
			height: 50px;
			left: 0;
			right: 0;
			bottom: 0;
			text-align: center;
		}
		.section {
			margin: 10px;
		}
		.mobile_view {
			padding-bottom: 40px;

		}
	</style>
</head>
<body>
<header>
	<div class="header">
	</div>
</header>
<div class="container">
<?php if(isset($_GET['status']) && $_GET['status'] == 'Success'){  ?>
	<div class="row text-center success">
        <div class="col-sm-6 col-sm-offset-3">
        <br><br> <h2>Success</h2>
        <!-- <img src="./css/success.png" width="100px" height="150px"> -->
        <i class="fa fa-check-circle text-success" aria-hidden="true"></i>

        <h3>Dear, <?php echo $name; ?></h3>
        <p style="font-size:20px;">Thank you for your payment amount: $<?php echo $amount;?> for order <?php echo $invoice_no;?> completed successully</p>
        <p style="font-size:20px;">If you have any problem contacts <?php echo $admin_no;?></p>
    <br><br>
        </div>
        
	</div>
<?php } else {  ?>
	<div class="row text-center danger">
        <div class="col-sm-6 col-sm-offset-3">
        <br><br> <h2 style="color:#">Failure</h2>
        <!-- <img src="./css/success.png" width="100px" height="150px"> -->
        <i class="fa fa-times-circle text-danger" aria-hidden="true"></i>

        <h3>Dear, <?php echo $name; ?></h3>
        <p style="font-size:20px;">Your payment transaction has failure. Please try again</p>
        <p style="font-size:20px;">If you have any problem contacts <?php echo $admin_no;?></p>
    <br><br>
        </div>
        
	</div>
<?php } ?>
</div>
<footer>
	<!-- <div class="footer"></div> -->
</footer>
</body>
</html>