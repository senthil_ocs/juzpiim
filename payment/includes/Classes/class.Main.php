<?php
	class Main {
		function escape($value)
		{
			return str_replace(array("\\", "\0", "\n", "\r", "\x1a", "'", '"'), array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"'), $value);
		}
        /*START LOGIN*/       
		function admin_login($objArray)
		{
			global $objCommon, $global_config;
			$cleanusername =	strip_tags(stripslashes($objArray['username']));
			$password	   =	$objArray['password'];
			$md5password   =	$password;
			$error ='';
			if ($cleanusername == '') { 
				$status	=	'invalid';
				$error	=	1;
			} else if ($password == "") {
				$status	=	'invalid';
				$error	=	1;
			}		
			if (!$error) {
				$sql 	= "SELECT * FROM ".ADMIN." WHERE `Username` ='".$cleanusername."' AND  `Password` ='".$password."' AND Status='Active'";
				$result = $objCommon->getSelectQuery($sql);
				if ($result) {
					$sql1 	= "SELECT * FROM tbl_user_group WHERE `id` ='".$result[0]['Type']."'AND Status='1'";
					$result1 = $objCommon->getSelectQuery($sql1);

					if(!empty($objArray["customCheck1"])) {  						
					    setcookie ("member_login",$cleanusername,time()+ (10 * 365 * 24 * 60 * 60));  
					    setcookie ("member_password",$password,time()+ (10 * 365 * 24 * 60 * 60));
					    
					    $_SESSION['SESS_ADMINID']			=	$result[0]['Id'];
					    $_SESSION['SESS_ADMINTYPENAME']		=	strtolower($result1[0]['name']);
						$_SESSION['SESS_Type']				=	$result[0]['Type'];
						$_SESSION['SESS_ADMINNAME']	    	=	$result[0]['FirstName'];
						$_SESSION['SESS_ADMINUSERNAME']	    =	$result[0]['Username'];
						$_SESSION['SESS_ADMINLASTNAME']		=	$result[0]['LastName'];
						$_SESSION['SESS_ADMINEMAIL']    	=	$result[0]['Email'];
						$_SESSION['flash_message'] 			=	"";
						$_SESSION['error_message']			=	"";
						$status ='success';
						redirect($global_config["AdminSiteGlobalPath"].'location.php');
			   		} else { 
			    		//if(isset($_COOKIE["member_login"]) && isset($_COOKIE["member_password"])) {  
		     			setcookie ("member_login","");  
		     			setcookie ("member_password","");
		     			$_SESSION['SESS_ADMINID']			=	$result[0]['Id'];
		     			$_SESSION['SESS_ADMINTYPENAME']		=	strtolower($result1[0]['name']);
						$_SESSION['SESS_Type']				=	$result[0]['Type'];
						$_SESSION['SESS_ADMINNAME']	    	=	$result[0]['FirstName'];
						$_SESSION['SESS_ADMINUSERNAME']	    =	$result[0]['Username'];
						$_SESSION['SESS_ADMINLASTNAME']		=	$result[0]['LastName'];
						$_SESSION['SESS_ADMINEMAIL']    	=	$result[0]['Email'];
						$_SESSION['flash_message'] 			=	"";
						$_SESSION['error_message']			=	"";
						$status ='success';
						redirect($global_config["AdminSiteGlobalPath"].'location.php');
			    		//} 
			   		}	
				} else {
					$status ='Invalid login details';
				}
			}
			return $status;
		}		
		/*START LOGOUT*/
		function logout()
		{		
			global $global_config;
			unset($_SESSION['SESS_ADMINID']);
			unset($_SESSION['SESS_ADMINNAME']);
			unset($_SESSION['SESS_ADMINUSERNAME']);
			unset($_SESSION['SESS_ADMINLASTNAME']);
			unset($_SESSION['SESS_ADMINEMAIL']);
			unset($_SESSION['SESS_LOCATIONCODE']);
			unset($_SESSION['language']);			
			redirect($global_config["SiteGlobalPath"].'login.php');
		}
			
		function listing_individualcustomer()
		{
			global $objCommon,$global_config;
			$wherecaluse    	= 	" WHERE CustNRICNO!='' AND IsCompany='0' AND Location='".$_SESSION['SESS_LOCATIONCODE']."'";						
			$order 				= 	" Order BY CustName ASC";							 
			$customer_lists		=	$objCommon->doGetTableListing(CUSTOMER,$wherecaluse,'*',$limit,$order);	
			return $customer_lists;
		}

		function listing_corporatecustomer()
		{
			global $objCommon,$global_config;
			$wherecaluse    	= 	" WHERE CustNRICNO!='' AND IsCompany='1' AND Location='".$_SESSION['SESS_LOCATIONCODE']."'";						
			$order 				= 	" Order BY CustName ASC";							 
			$customer_lists		=	$objCommon->doGetTableListing(CUSTOMER,$wherecaluse,'*',$limit,$order);	
			return $customer_lists;
		}
		
		function customer_count()
		{
			global $objCommon,$global_config;
			$where   	= 	" WHERE CustomerId!='' AND Location='".$_SESSION['SESS_LOCATIONCODE']."'";
			$count_sql			=	"SELECT COUNT(CustomerId) as TotalCnt from ".CUSTOMER." $where";
			$customer_count 	= 	$objCommon->getSelectQuery($count_sql);
			return $customer_count[0]['TotalCnt'];
		}

		function profile_details($admin_id)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE Id = '".$admin_id."' ";
			$adminlists = $objCommon->doGetTableSingleRecord(ADMIN,$whereclause);	
			return $adminlists;
		}
		function update_profile($objArray, $admin_id)
		{
			global $objCommon,$global_config;
			if($objArray['frm_Email']!=""){ 
				$where 	= " WHERE Email = '".$objArray['frm_Email']."' AND Id!= '".$admin_id."'";
				$status = $objCommon->doCheckDuplication(ADMIN,$where);
			}
 			if($status==0) { 				
 				$objArray['frm_Modifieddate'] = doGetServerDateTime();
				$updateSalesmanWhere = " WHERE Id=".$admin_id."";
				$objCommon->UpdateInfoToDB($objArray,"frm_",ADMIN,$updateSalesmanWhere);
				$message = displayMessage("Admin details updated successfully!");
			} else {
				$message = errorMessage("Admin email already exist!");
			}	
			return $message;
		}

		function sync_details()
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE id!='' ORDER BY id DESC limit 1";
			$sync = $objCommon->doGetTableSingleRecord(SYNC,$whereclause);	
			return $sync;
		}
		function insert_sync($objArray)
		{
			global $objCommon,$global_config;				
 			//$objArray['frm_Modifieddate'] = doGetServerDateTime();
			$objCommon->AddInfoToDB($objArray, 'frm_', SYNC);
			$message = displayMessage("Sync date updated successfully!");
			return $message;
		}

		function settings_details()
		{
			global $objCommon,$global_config;
			$setlists = $objCommon->doGetTableSingleRecord(SETTINGS);
			return $setlists;
		}
		function update_settings($objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE settings_id = '1'";
			$objCommon->UpdateInfoToDB($objArray,"set_",SETTINGS,$WhereClause);
			$message = displayMessage("Settings details updated successfully!");
			return $message;
		}

		function getMembersNew($CustNRICNO,$customerCode='',$type='')
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE CustNRICNO = '".$CustNRICNO."'";
			
			if($customerCode){
				$whereclause.= " AND CustCode = '".$customerCode."'";
			}
			
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}

		function getCompanyMembers($customerCode,$mobile='')
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE IsCompany = '1'";
			
			if($customerCode){
				$whereclause.= " AND CustCode = '".$customerCode."'";
			}
			if($type=='company'){
				$whereclause.= " AND CustMobileNO = '".$mobile."'";
			}
			
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}


		function getMembers($CustNRICNO,$customerCode='',$type='')
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE CustNRICNO = '".$CustNRICNO."'";
			
			if($customerCode){
				$whereclause.= " AND CustCode = '".$customerCode."'";
			}
			if($type=='company'){
				$whereclause.= " AND IsCompany = '1'";
			}else{
				$whereclause.= " AND IsCompany = '0'";
			}
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}

		function update_members($otp, $CustNRICNO)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE CustNRICNO = '".$CustNRICNO."'";
			$objArray['cus_otp'] = $otp;
			$objArray['cus_otp_status'] = '0';
			$objCommon->UpdateInfoToDB($objArray,"cus_",CUSTOMER,$WhereClause);
			$message = displayMessage("Customer details updated successfully!");
			return $message;
		}

		function getOtpMembers($otp, $CustNRICNO)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE otp = '".$otp."' AND CustNRICNO = '".$CustNRICNO."'";
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}

		function getBeneficiery($CustCode)
		{
			global $objCommon,$global_config;
			$postvalue = explode("|",$CustCode); 
        	$whereclause = "WHERE CustId = '".$postvalue[0]."'"; // AND CustNRICNO = '".$postvalue[1]."'
			$Beneficiery = $objCommon->doGetTableListing(BENEFICIERY,$whereclause);
			return $Beneficiery;
		}
		function checkBeneficiery($CustNRICNO)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE CustNRICNO = '".$CustNRICNO."'";
			$Beneficiery = $objCommon->doGetTableListing(BENEFICIERY,$whereclause);
			return $Beneficiery;
		}
		function getBeneficieryNew($CustCode)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE CustCode = '".$CustCode."' ";
			$Beneficiery = $objCommon->doGetTableListing(BENEFICIERY,$whereclause);
			return $Beneficiery;
		}

		function getBeneficieryCount($CustNRICNO)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE CustNRICNO = '".$CustNRICNO."' ";
			$Beneficiery = $objCommon->doTotalRecordCnt(BENEFICIERY,$whereclause,"CustNRICNO");
			return $Beneficiery;
		}
		 
		function update_otpstatus($otpstatus, $CustNRICNO)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE CustNRICNO = '".$CustNRICNO."'";
			$objArray['cus_otp_status'] = $otpstatus; 
			$objCommon->UpdateInfoToDB($objArray,"cus_",CUSTOMER,$WhereClause);
			$message = displayMessage("Customer details updated successfully!");
			return $message;
		}

		function purpose_details()
		{
			global $objCommon,$global_config;
			$wherecaluse    	= 	" WHERE id!=''";						
			$order 				= 	"ORDER BY id DESC";							 
			$purpose_lists		=	$objCommon->doGetTableListing(PURPOSE,$wherecaluse,'*',$limit,$order);	
			return $purpose_lists;
		}

		function insert_purpose($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_name']!=""){ 
				$where 	= " WHERE name = '".$objArray['frm_name']."'";
				$status = $objCommon->doCheckDuplication(PURPOSE,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', PURPOSE);
		 	$message = displayMessage("Purpose details Created successfully!");
		 	} else {
				$message = errorMessage("Purpose Name Already Exist!");
			}	
			return $message;
		}

		function update_purpose($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",PURPOSE,$WhereClause);
			$message = displayMessage("Purpose details updated successfully!");
			return $message;
		}

		function delete_purpose($id)
		{
			global $objCommon,$global_config;
			if($id!=""){ 
				$where 	= " WHERE purpose_id = '".$id."'";
				$status = $objCommon->doCheckDuplication(APPLICATION,$where);
			}
			if($status==0) {
			$WhereClause = " id = '".$id."'";
		 	$objCommon->doDelete(PURPOSE, $WhereClause);
		 	$message = displayMessage("Purpose details Deleted successfully!");
		 	} else {
				$message = errorMessage("Purpose Name Already Exist in Applications!");
			}	
			return $message;
		}

		function currency_master_lists()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE status ='1' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";
			$order 				= 	"ORDER BY id DESC";							 
			$currency		=	$objCommon->doGetTableListing(CURRENCYMASTER,$whereclause,'*',$limit,$order);	
			return $currency;
		}

		function currency_master_details()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE id!='' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";						
			$order 				= 	"ORDER BY id DESC";							 
			$currency_master_lists		=	$objCommon->doGetTableListing(CURRENCYMASTER,$whereclause,'*',$limit,$order);
			return $currency_master_lists;
		}

		function insert_currency_master($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_currency_code']!=""){ 
				$where 	= " WHERE currency_code = '".$objArray['frm_currency_code']."' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";
				$status = $objCommon->doCheckDuplication(CURRENCYMASTER,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', CURRENCYMASTER);
		 	$message = displayMessage("Currency master details Created successfully!");
			} else {
				$message = errorMessage("Currency Master details Already Exist!");
			}	
			return $message;
		}

		function update_currency_master($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",CURRENCYMASTER,$WhereClause);
			$message = displayMessage("Currency master details updated successfully!");
			return $message;
		}

		function delete_currency_master($id,$objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_currency_code']!=""){ 
				$where 	= " WHERE currency_code = '".$objArray['frm_currency_code']."'";
				$status = $objCommon->doCheckDuplication(APPLICATION,$where);
			}
			if($status==0) {
			$WhereClause = " id = '".$id."'";
		 	$objCommon->doDelete(CURRENCYMASTER, $WhereClause);
		 	$message = displayMessage("Currency master details Deleted successfully!");
		 	} else {
				$message = errorMessage("Currency Master details Already Existed in Application!");
			}
			return $message;
		}

		function charges_master_details()
		{
			global $objCommon,$global_config;
			$wherecaluse    	= 	" WHERE id!=''";						
			$order 				= 	"ORDER BY id DESC";							 
			$charges_master_lists		=	$objCommon->doGetTableListing(CHARGESMASTER,$whereclause,'*',$limit,$order);
			return $charges_master_lists;
		}

		function insert_charges_master($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_currency_code']!=""){ 
				echo $where 	= " WHERE currency_code = '".$objArray['frm_currency_code']."' AND location_code='".$objArray['frm_location_code']."'";
				$status = $objCommon->doCheckDuplication(CHARGESMASTER,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', CHARGESMASTER);
		 	$message = displayMessage("Charges Master details Created successfully!");
			} else {
				$message = errorMessage("Charges Master details Already Exist!");
			}	
			return $message;
		}

		function update_charges_master($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."' AND location_code='".$_SESSION['SESS_LOCATIONCODE']."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",CHARGESMASTER,$WhereClause);
			$message = displayMessage("Charges Master details updated successfully!");
			return $message;
		}

		function delete_charges_master($id,$objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_currency_code']!=""){ 
				$where 	= " WHERE currency_code = '".$objArray['frm_currency_code']."'";
				$status = $objCommon->doCheckDuplication(APPLICATION,$where);
			}
			if($status==0) {
			$WhereClause = " id = '".$id."'";
		 	$objCommon->doDelete(CHARGESMASTER, $WhereClause);
		 	$message = displayMessage("Charges Master details Deleted successfully!");
		 	} else {
				$message = errorMessage("Charges Master details Already Existed in Application!");
			}
			return $message;
		}

		function rate_master_details()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";		//id!='' AND		
			$order 				= 	" GROUP BY currency_code ORDER BY id ASC";
			$rate_master_lists		=	$objCommon->doGetTableListing(RATEMASTER,$whereclause,'*',$limit,$order);	
			return $rate_master_lists;
		}

		function insert_rate_master($objArray)
		{
			global $objCommon,$global_config;
		 	$objCommon->AddInfoToDB($objArray, 'frm_', RATEMASTER);
		 	$message = displayMessage("Rate Master details Created successfully!");
			return $message;
		}

		function update_rate_master($date,$objArray)
		{
			global $objCommon,$global_config;
			$id = $objArray['frm_currency_id'];
			$WhereClause = " WHERE date = '".$date."' AND currency_id = '".$id."' ";
			$objCommon->UpdateInfoToDB($objArray,"frm_",RATEMASTER,$WhereClause);
			$message = displayMessage("Rate Master details updated successfully!");
			return $message;
			
		}   

		function usertype_details()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE id!=''";						
			$order 				= 	"ORDER BY id DESC";							 
			$usertype_lists		=	$objCommon->doGetTableListing(USERTYPE,$whereclause,'*',$limit,$order);	
			return $usertype_lists;
		}

		function insert_usertype($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_name']!=""){ 
				$where 	= " WHERE name = '".$objArray['frm_name']."'";
				$status = $objCommon->doCheckDuplication(USERTYPE,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', USERTYPE);
		 	$message = displayMessage("User Group details Created successfully!");
			} else {
				$message = errorMessage("User Group details Already Exist!");
			}
		 	
			return $message;
		}

		function update_usertype($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",USERTYPE,$WhereClause);
			$message = displayMessage("User Group details updated successfully!");
			return $message;
		}

		function delete_usertype($id)
		{
			global $objCommon,$global_config;
			if($id!=""){ 
				$where 	= " WHERE Type = '".$id."'";
				$status = $objCommon->doCheckDuplication(ADMIN,$where);
			}
			if($status==0) {
			$WhereClause = " id = '".$id."'";
		 	$objCommon->doDelete(USERTYPE, $WhereClause);
		 	$message = displayMessage("User Group details Deleted successfully!");
		 	} else {
				$message = errorMessage("User Group Already Exist in Users!");
			}
			return $message;
		}

		function users_details()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE Id!=''";						
			$order 				= 	"ORDER BY Id DESC";							 
			$user_lists		=	$objCommon->doGetTableListing(ADMIN,$whereclause,'*',$limit,$order);	
			return $user_lists;
		}

		function insert_users($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_UserName']!=""){ 
				$where 	= " WHERE UserName = '".$objArray['frm_UserName']."'";
				$status = $objCommon->doCheckDuplication(ADMIN,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', ADMIN);
		 	$message = displayMessage("User details Created successfully!");
			} else {
				$message = errorMessage("User details Already Exist!");
			}	
			return $message;
		}

		function update_users($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE Id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",ADMIN,$WhereClause);
			$message = displayMessage("User details updated successfully!");
			return $message;
		}

		function delete_users($id)
		{
			global $objCommon,$global_config;
			$WhereClause = " Id = '".$id."'";
		 	$objCommon->doDelete(ADMIN, $WhereClause);
		 	$message = displayMessage("User details Deleted successfully!");
			return $message;
		}

		function getPurpose()
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE id!=''";
			$purpose = $objCommon->doGetTableListing(PURPOSE,$whereclause);
			return $purpose;
		}

		function getRate($date)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE date = '".$date."' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";
			$rate = $objCommon->doGetTableListing(RATEMASTER,$whereclause);
			return $rate;
		}
		function getRateAPI($date,$location_code)
		{
			global $objCommon,$global_config;

			//$whereclause = " WHERE date = '".$date."' AND location_code ='".$location_code."'";
			//$rate = $objCommon->doGetTableListing(RATEMASTER,$whereclause);
			$sql   = "select rm.*,cm.allowed_hrs from tbl_rate_fix as rm LEFT JOIN tbl_currency_master as cm on cm.id=rm.currency_id 
					  WHERE rm.date = '".$date."' AND rm.location_code ='".$location_code."'";
			$rate 	  =	$objCommon->getSelectQuery($sql);

			return $rate;
		}

		function getCurrency($location_code='')
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE id!='' AND status='1' AND location_code='".$location_code."'";
			$order 		 = "ORDER BY sort_order ASC";
			$currency = $objCommon->doGetTableListing(CURRENCYMASTER,$whereclause,'*',$limit,$order);
			return $currency;
		}

		function insert_application($objArray)
		{
			global $objCommon,$global_config;
		 	$objCommon->AddInfoToDB($objArray, 'frm_', APPLICATION);
		 	$message = displayMessage("Application details Created successfully!");
			return $message;
		}

		function update_application($applicant_customer_code,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE applicant_customer_code = '".$applicant_customer_code."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",APPLICATION,$WhereClause);
			$message = displayMessage("Application details updated successfully!");
			return $message;
		}
			
		function getApplication($applicant_customer_code)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE applicant_customer_code = '".$applicant_customer_code."' ORDER BY id DESC limit 1";
			$app = $objCommon->doGetTableSingleRecord(APPLICATION,$whereclause);
			return $app;
		}

		function getApps($customer_code,$status_type='')
		{
			global $objCommon,$global_config;
			if($status_type!=''){
				$whereclause = " WHERE applicant_customer_code = '".$customer_code."' AND Status='".$status_type."'";
			}else{
				$whereclause = " WHERE applicant_customer_code = '".$customer_code."'";
			}
			
			$apps = $objCommon->doGetTableListing(APPLICATION,$whereclause);
			return $apps;
		}

		function update_appupload($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",APPLICATION,$WhereClause);
			$message = displayMessage("application PDF uploaded successfully!");
			return $message;
		}

		function getApplications($id)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE id = '".$id."' OR Transaction_no = '".$id."'";
			$app = $objCommon->doGetTableSingleRecord(APPLICATION,$whereclause);
			return $app;
		}

		function update_image($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"app_",APPLICATION,$WhereClause);
			$message = displayMessage("Application details updated successfully!");
			return $message;
		}

		function appupdate($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$update=$objCommon->UpdateInfoToDB($objArray,"frm_",APPLICATION,$WhereClause);
			$message = displayMessage("Application details updated successfully!");
			return $message;
		}
		
		function getApp()
		{
			global $objCommon,$global_config;
			$option = addslashes($_GET['Status']);
			$whereclause  = "WHERE A.id!='' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";
			if(isset($option) && $option!=''){
				$whereclause .= "AND A.Status = '".$option."'";
			}
			$applications = "SELECT DISTINCT D.application_id,A.*,B.name,C.BeneName FROM ".APPLICATION." AS A LEFT JOIN ".PURPOSE." AS B ON A.purpose_id = B.id LEFT JOIN ".BENEFICIERY." AS C ON A.beneficiary_id = C.id LEFT JOIN ".UPLOADS." AS D ON A.Transaction_no = D.application_id $whereclause" ;	
			//LEFT JOIN ".CUSTOMER." ON ".APPLICATION.".applicant_customer_code = ".CUSTOMER.".CustCode ,D.CustName
			$results 	  =	$objCommon->getSelectQuery($applications);
			return $results;
		}

		function getCustomerDetails($applicant_customer_code)
		{
			global $objCommon,$global_config;
			$whereclause  = "WHERE CustNRICNO='".trim($applicant_customer_code)."' OR CustCode='".trim($applicant_customer_code)."' ORDER BY CustomerId ASC limit 1";
			$customers = "SELECT * FROM ".CUSTOMER." $whereclause" ;	
			$results 	  =	$objCommon->getSelectQuery($customers);
			return $results;
		}

		function AppProcount()
		{
			global $objCommon,$global_config;
			$where_app     =  "WHERE id!='' AND Status = 'Processing' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";							
			$count_sql	   =  "SELECT COUNT(id) as TotalCnt from ".APPLICATION." $where_app";
			$process_count =  $objCommon->getSelectQuery($count_sql);
			return $process_count[0]['TotalCnt'];
		}

		function AppComcount()
		{
			global $objCommon,$global_config;
			$where_app     =  "WHERE id!='' AND Status = 'Completed' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";							
			$count_sql	   =  "SELECT COUNT(id) as TotalCnt from ".APPLICATION." $where_app";
			$compl_count   =  $objCommon->getSelectQuery($count_sql);
			return $compl_count[0]['TotalCnt'];
		}

		function AppPencount()
		{
			global $objCommon,$global_config;
			$where_app     =  "WHERE id!='' AND Status = 'Pending' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";							
			$count_sql	   =	"SELECT COUNT(id) as TotalCnt from ".APPLICATION." $where_app";
			$pend_count    = $objCommon->getSelectQuery($count_sql);
			return $pend_count[0]['TotalCnt'];
		}

		function AppCancount()
		{
			global $objCommon,$global_config;
			$where_app     =  "WHERE id!='' AND Status = 'Cancelled' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";							
			$count_sql	   =	"SELECT COUNT(id) as TotalCnt from ".APPLICATION." $where_app";
			$cancel_count  = $objCommon->getSelectQuery($count_sql);
			return $cancel_count[0]['TotalCnt'];
		}

		/*function getUser()
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE Id!=''";
			$order 		 = " ORDER BY Id ASC";
			$user = $objCommon->doGetTableListing(ADMIN,$whereclause,'*',$limit,$order);
			return $user;
		}*/

		function getAdmin()
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE Type = '6'";
			$admin = $objCommon->doGetTableSingleRecord(ADMIN,$whereclause);
			return $admin;
		}
		function getTransactions($filterAry)
		{
			global $objCommon,$global_config;
			$option = addslashes($_GET['Status']);
			$whereclause  = " WHERE id!=''";
			if($filterAry['CustNRICNO']!=''){
				$whereclause .= "AND applicant_customer_code = '".$filterAry['CustNRICNO']."'";
			}
			if($filterAry['status']!=''){
				$whereclause .= "AND Status = '".$filterAry['status']."'";
			}
			$applications = "SELECT * FROM ".APPLICATION."$whereclause" ;
			$results 	  =	$objCommon->getSelectQuery($applications);
			return $results;
		}
		function insert_img($objArray)
		{
			global $objCommon,$global_config;				
 			//$objArray['frm_Modifieddate'] = doGetServerDateTime();
			$objCommon->AddInfoToDB($objArray, 'frm_', UPLOADS);
			$message = displayMessage("File Added successfully!");
			return $message;
		}
		function getUploads($id)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE application_id = '".$id."'";
			$app = $objCommon->doGetTableListing(UPLOADS,$whereclause);
			return $app;
		}

		/*Display-rates.php & pay.php*/
		function getSettingsDetails()
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE Id = '1'";
			$set = $objCommon->doGetTableSingleRecord(NEWSETTINGS,$whereclause);
			return $set;
		}
		function getcurrencyrate()
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE IsDeleted='No' And status='Yes'";
			$cur = $objCommon->doGetTableListing(CURRENCY,$whereclause);
			return $cur;
		} 
		function getcurrencylist()
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE specialcurrency='Yes' And IsDeleted='No' And status='Yes'";
			$curs = $objCommon->doGetTableListing(CURRENCY,$whereclause);
			return $curs;
		}

		/*Admin display rates*/
		function currency_lists()
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE IsDeleted='No' AND language='english'";						
			$order 				= 	"ORDER BY MenuPosition,SortOrder ASC";							 
			$currency_master_lists		=	$objCommon->doGetTableListing(CURRENCY,$whereclause,'*',$limit,$order);
			return $currency_master_lists;
		}
		function curupdate($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",CURRENCY,$WhereClause);
			$message = displayMessage("currency details updated successfully!");
			return $message;
		}
		function insertCurrency($objArray)
		{
			global $objCommon,$global_config;
			if($objArray['frm_currencycode']!=""){ 
				$where 	= " WHERE currencycode = '".$objArray['frm_currencycode']."'";
				$status = $objCommon->doCheckDuplication(CURRENCY,$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArray, 'frm_', CURRENCY);
		 	$message = displayMessage("Currency details Created successfully!");
		 	} else {
				$message = errorMessage("Currency Name Already Exist!");
			}	
			return $message;
		}
		function GetContentTotalRecordsValues(){
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE language='english'";						
			$order 				= 	"";							 
			$currencylists		=	$objCommon->doGetTableListing(CURRENCY,$whereclause,'*',$limit,$order);
			return $currencylists;
		
		}
		function Getxmlrecord($tbl_name){
			global $objCommon,$global_config;
			$sql="SELECT * from ".$tbl_name." where $where  language='english' and IsDeleted='No' and status='Yes' ORDER BY SortOrder ASC";		
			$result=$objCommon->getSelectQuery($sql);
			return $result;
		}
		function getMembersByMobileNo($mobileNo)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE CustMobileNO = '".$mobileNo."'";
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer['CustomerId'];
		}
		function checkUserDevice($userid)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE user_id = '".$userid."'";
			$device = $objCommon->doGetTableSingleRecord('tbl_devices',$whereclause);
			return $device;
		}
		function fetchcustomer($ccode,$nricno)
		{
			global $objCommon,$global_config;
			$whereclause.= " WHERE CustCode = '".$ccode."' AND CustNRICNO = '".$nricno."'";
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}
		function fetchbeneficiary($CustCode,$CustNRICNO)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE CustCode= '".$CustCode."' AND CustNRICNO = '".$CustNRICNO."'";
			$bene = $objCommon->doGetTableListing(BENEFICIERY,$whereclause);
			return $bene;
		}
		function getCustDetailsByNRICNO($objArry)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE  CustCode= '".$objArry['cus_CustCode']."' AND CustNRICNO = '".$objArry['cus_CustNRICNO']."'";
			$cust = $objCommon->doGetTableSingleRecord('tbl_customer_1',$whereclause);
			return $cust;
		}
		function getBeneDetailsByNRICNO($objArr)
		{
			global $objCommon,$global_config;
			$benename = htmlspecialchars($objArr['frm_BeneName'],ENT_QUOTES);
			$whereclause = " WHERE CustNRICNO = '".$objArr['frm_CustNRICNO']."' AND BeneName = '".$benename."'";
			$bene = $objCommon->doGetTableSingleRecord('tbl_benificiery_details1',$whereclause);
			return $bene;
		}
		function insert_cust($objArry)
		{
			global $objCommon,$global_config;
			if($objArry['cus_CustCode']!="" && $objArry['cus_CustNRICNO']!=""){ 
				$where 	= " WHERE  CustCode= '".$objArry['cus_CustCode']."' AND CustNRICNO = '".$objArry['cus_CustNRICNO']."'";
				$status = $objCommon->doCheckDuplication('tbl_customer_1',$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArry, 'cus_', 'tbl_customer_1');
		 	$message = displayMessage("Customer details Inserted successfully!");
			} else {
				$message = errorMessage("Customer details Already Exist!");
			}	
			return $message;
		}
		function insert_bene($objArr)
		{
			global $objCommon,$global_config;
			if($objArr['frm_CustNRICNO']!="" && $objArr['frm_BeneName']!=""){ 
				$benename = htmlspecialchars($objArr['frm_BeneName'],ENT_QUOTES);
				$where 	= " WHERE CustNRICNO = '".$objArr['frm_CustNRICNO']."' AND BeneName = '".$benename."'";
				$status = $objCommon->doCheckDuplication('tbl_benificiery_details1',$where);
			}
			if($status==0) {
		 	$objCommon->AddInfoToDB($objArr, 'frm_', 'tbl_benificiery_details1');
		 	$message = displayMessage("Beneficiary details Inserted successfully!");
			} else {
				$message = errorMessage("Beneficiary details Already Exist!");
			}	
			return $message;
		}
		function fetchnricno($code)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE customer_code = '".$code."'";
			$nricno = $objCommon->doGetTableListing('tbl_customercontact',$whereclause);
			return $nricno;
		}
		function getpurposeName($purpose_id)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE id = '".$purpose_id."'";
			$purpose = $objCommon->doGetTableSingleRecord(PURPOSE,$whereclause);
			return $purpose;
		}
		function getcurcode($currency_code)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE currency_code = '".$currency_code."'";
			$currency = $objCommon->doGetTableSingleRecord(CURRENCYMASTER,$whereclause);
			return $currency;
		}
		/*function getbank($bank_id)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE id = '".$bank_id."'";
			$bank = $objCommon->doGetTableSingleRecord(BANK,$whereclause);
			return $bank;
		}*/
		function getBeneDetails($beneficiary_id)
		{
			global $objCommon,$global_config;
			$whereclause  = "WHERE id ='".$beneficiary_id."'";
			$results 	  =	$objCommon->doGetTableSingleRecord(BENEFICIERY, $whereclause);
			return $results;
		}
		function getCust($applicant_customer_code)
		{
			global $objCommon,$global_config;
			$whereclause  = "WHERE CustNRICNO='".trim($applicant_customer_code)."' OR CustCode='".trim($applicant_customer_code)."'";
			$results = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $results;
		}
		function getmax(){
			global $objCommon,$global_config;
			$applications = "SELECT MAX(id) as max_id FROM ".APPLICATION;
			$results 	  =	$objCommon->getSelectQuery($applications);
			return $results[0]['max_id'];
		}
		function checkBeneficierywithAcc($custId,$benId)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE CustId = '".$custId."' AND beneficiary_id = '".$benId."'";
			$bene = $objCommon->doGetTableSingleRecord(BENEFICIERY,$whereclause);	
			return $bene;
		}
		function getLocations()
		{
			global $objCommon,$global_config;
			$wherecaluse    	= 	" WHERE id!=''";							 
			$locations			=	$objCommon->doGetTableListing(LOCATIONS,$wherecaluse);	
			return $locations;
		}
		function getTransId()
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE settings_id = '1'";
			$transID = $objCommon->doGetTableSingleRecord(SETTINGS,$whereclause);
			return $transID;
		}
		function getcompMember($custcode,$nric)
		{
			global $objCommon,$global_config;
			$whereclause = "WHERE CustCode = '".$custcode."' AND CustNRICNO = '".$nric."'";
			$comp = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);	
			return $comp;
		}
		 function writeLog($str){
	      $fileName = date('Y-m-d').'.txt';
	      $file = $_SERVER['DOCUMENT_ROOT'].'/api_post_log/'.$fileName;
	      $myfile = fopen($file, "a") or die("Unable to open file!");
	      $txt = "\n";
	      $txt.= "Posted Date Time"."-->".date('Y-m-d H:i:s')."\n";
	      $txt.= "======================================================================================\n";
	      $txt.= $str;
	      fwrite($myfile, "\n". $txt);
	      fclose($myfile);
    	} 

    	function getmaxCurrencySortOrder(){
			global $objCommon,$global_config;
			$sortqry = "SELECT MAX(SortOrder) as sortorderid FROM ".CURRENCY;
			$results 	  =	$objCommon->getSelectQuery($sortqry);
			return $results[0]['sortorderid'];
		}
		function getHourDiff($rateAddedDateTime=''){
			global $objCommon,$global_config;
			$time1 = $rateAddedDateTime;
    		$time2 = date("Y-m-d H:i:s");
   			$hourdiff = (strtotime($time2) - strtotime($time1))/3600;
   			return $hourdiff;
		}

		function checkalreadyExist($icno,$type=''){
			global $objCommon,$global_config;
			if($type=''){
				$sortqry = "SELECT count(nric_no) as total FROM ".REGISTER." where nric_no='".$icno."'";
			}else{
				$sortqry = "SELECT count(code) as total FROM ".REGISTER." where code='".$icno."'";
			}
			$results 	  =	$objCommon->getSelectQuery($sortqry);
			return $results[0]['total'];
		}

		function insert_register($objArray)
		{
			global $objCommon,$global_config;
		 	$insertId = $objCommon->AddInfoToDB($objArray, 'frm_', REGISTER);
		 	return $insertId;
		}
		function update_request($transaction_no)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE Transaction_no = '".$transaction_no."'";
			$objArray['app_customer_request'] = '1';
			$objCommon->UpdateInfoToDB($objArray,"app_",APPLICATION,$WhereClause);
			return "1";
		}
		function request_details($type='')
		{
			global $objCommon,$global_config;
			if($type=='corporate'){
				$wherecaluse    	= 	" WHERE id!='' AND type='corporate'";
			}else{
				$wherecaluse    	= 	" WHERE id!='' AND type='cash'";
			}

			if($_SESSION['SESS_LOCATIONCODE']){
				$wherelocation = " And location_code=".$_SESSION['SESS_LOCATIONCODE'];
			} else {
				$wherelocation = '';
			}
			
			if($_REQUEST['Status']=='Pending'){
				$wherecaluse.=" AND status='Pending'";
			}else if($_REQUEST['Status']=='Approved'){
				$wherecaluse.=" AND status='Approved'";
			}

			$order 				= 	"ORDER BY id DESC";							 
			$purpose_lists		=	$objCommon->doGetTableListing(REGISTER,$wherecaluse,'*',$limit,$order,$wherelocation);	
			return $purpose_lists;
		}
		function request_details_count($type= '')
		{
			global $objCommon,$global_config;
			if($type=='corporate'){
				$wherecaluse    	= 	" WHERE id!='' AND type='corporate'";
			}else{
				$wherecaluse    	= 	" WHERE id!='' AND type='cash'";
			}
			$wherecaluse.=" AND status='Pending'";
			$wherelocation = " And location_code=".$_SESSION['SESS_LOCATIONCODE'];
			// $wherecaluse.=" AND status='Approved'";
			
			$order 				= 	"ORDER BY id DESC";							 
			$purpose_lists		=	$objCommon->doGetTableListing(REGISTER,$wherecaluse,'*',$limit,$order,$wherelocation);
			return $purpose_lists;
		}
		function update_requestCus($id,$objArray)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE id = '".$id."'";
			$objCommon->UpdateInfoToDB($objArray,"frm_",REGISTER,$WhereClause);
			$message = displayMessage("Register Request details updated successfully!");
			return $message;
		}
		function delete_requestCus($id)
		{
			global $objCommon,$global_config;
			$WhereClause = " id = '".$id."'";
		 	$objCommon->doDelete(REGISTER, $WhereClause);
		 	$message = displayMessage("Deleted successfully!");
		 	return $message;
		}
		function feesdetails(){
			global $objCommon,$global_config;
			$location_code = str_replace('00','',$_SESSION['SESS_LOCATIONCODE']);
			$sql = "SELECT CHM.*,cm.currency_name FROM `tbl_currency_master` as cm LEFT JOIN tbl_charges_master as CHM on CHM.currency_code = cm.currency_code 
			WHERE cm.location_code='".$location_code."' AND CHM.location_code='".$_SESSION['SESS_LOCATIONCODE']."'";
			$results 	  =	$objCommon->getSelectQuery($sql);
			return $results;

		}
		function currency_master_lists_byCode($code)
		{
			global $objCommon,$global_config;
			$whereclause    	= 	" WHERE currency_code='".$code."' AND location_code ='".$_SESSION['SESS_LOCATIONCODE']."'";
			$order 				= 	"ORDER BY id DESC limit 1";							 
			$currency		=	$objCommon->doGetTableListing(RATEMASTER,$whereclause,'*',$limit,$order);	
			return $currency[0]['price'];
		}
		function update_terms($id)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE CustomerId = '".$id."'";
			$objArray['frm_terms'] = 1;
			$objCommon->UpdateInfoToDB($objArray,"frm_",CUSTOMER,$WhereClause);
		}
		function getAppSync($fromdate,$todate,$application_ids='')
		{
			global $objCommon,$global_config;
			$whereclause  = "WHERE A.id!=''";
			
			if(isset($fromdate) && $fromdate!='' && isset($fromdate) && $fromdate!=''){
				$whereclause .= "AND date(A.added_date) BETWEEN '".$fromdate."' AND '".$todate."'";
			}
			if($application_ids){
				$application_ids = rtrim($application_ids,",");
				$whereclause .= "AND A.applicant_id NOT IN($application_ids)";				
			}
			
			$applications = "SELECT A.*,B.name,C.BeneName FROM ".APPLICATION." AS A LEFT JOIN ".PURPOSE." AS B ON A.purpose_id = B.id LEFT JOIN ".BENEFICIERY." AS C ON A.beneficiary_id = C.id $whereclause" ;
			
			$results 	  =	$objCommon->getSelectQuery($applications);
			return $results;
		}
		function getApplicationinfo($id)
		{
			global $objCommon,$global_config;
			$whereclause  = "WHERE id ='".$id."'";
			$results 	  =	$objCommon->doGetTableSingleRecord(APPLICATION, $whereclause);
			return $results;
		}
    }
?>