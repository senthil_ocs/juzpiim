<?php
	class ServiceApi {

		public function getSalesInvoice($purchaseId)
		{
			global $objCommon,$global_config;
			 $query = "SELECT  * FROM " . DB_PREFIX . "sales_invoice_header WHERE invoice_no = '" .$purchaseId . "'";
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}

		public function getSalesDoHeader($do_no)
		{
			global $objCommon,$global_config;
			 $query = "SELECT  * FROM " . DB_PREFIX . "sales_do_header WHERE do_no='".$do_no."'";
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}
		public function getSalesDodetails($do_no)
		{
			global $objCommon,$global_config;
			 $query = "SELECT  * FROM " . DB_PREFIX . "sales_do_details WHERE do_no='".$do_no."'";
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}
		public function getB2BCustomersbyCode($customercode)
		{
			global $objCommon,$global_config;
			$query = "SELECT * FROM " . DB_PREFIX . "customers WHERE customercode ='".$customercode."'";
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}

		public function getUser($user_id) {
			global $objCommon,$global_config;
			$company_id	= $this->session->data['company_id'];
			$query = "SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "' AND company_id = '" . (int)$company_id . "'";
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}

		public function getSalesInvoiceDetails($purchaseId)
		{	
			global $objCommon,$global_config;
			$query = "SELECT * FROM " . DB_PREFIX . "sales_invoice_details WHERE invoice_no = '" .$purchaseId . "'";
			$result = $objCommon->getSelectQuery($query);
			return $result;
		}

		public function insertPaymentTransaction($data)
		{	
			if($data['do_no'] !=''){
				$data['do_no'] = base64_decode($data['do_no']);
			}else{
				$data['do_no'] = '';
			}
			if(!isset($data['stripe_response'])){
				$data['stripe_response'] = '';
			}
			global $objCommon,$global_config;
			 $query = "INSERT INTO " . DB_PREFIX . "order_payment (order_id, do_no, customer_id, delivery_man_id, payment_type,payment_method, amount, payment_date, added_on,payment_status,stripe_response)
			VALUES ('".$data['order_id']."','".$data['do_no']."', '".$data['customer_id']."', '".$data['delivery_man_id']."', '".$data['payment_type']."','".$data['payment_method']."', '".$data['amount']."', '".$data['payment_date']."', '".date('Y-m-d h:i:s')."','Pending','".$data['stripe_response']."')"; 
			$result = $objCommon->doInsertSql($query);
			return $result;
		}
		
		public function updatePaymentTransaction($data, $last_id)
		{	
			global $objCommon,$global_config;
			$payment_status = 'Pending';
			if($data['payment_status']){
				$payment_status = 'Hold';
			}
			$query = "UPDATE ".DB_PREFIX."order_payment set payment_status = '".$payment_status."', stripe_response  = '".$data['response']."' where id =".$last_id.""; 
			$result = $objCommon->doUpdateSql($query);
			$response_status = $this->update_salesHeader($last_id,$payment_status);
			return $response_status;
		}
		public function update_salesHeader($payment_id,$payment_status){
			global $objCommon,$global_config;

			if($payment_status == 'Hold' || $payment_status == 'Paid'){
				$result = $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."order_payment where id='".$payment_id."' ");
				
				$salesHeader = $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no='".$result[0]['order_id']."' ")[0];

				$paid_amount    = $salesHeader['paid_amount'] + $result[0]['amount'];
				$salesHeader['paid_amount'] = $paid_amount; 
				$this->sendReceiptToCustomer($result[0], $salesHeader);

				$payment_status = $objCommon->getSelectQuery("SELECT CASE WHEN net_total = '".$paid_amount."' THEN 'Paid'ELSE 'Pending'END AS payment_status FROM tbl_sales_invoice_header where invoice_no='".$salesHeader['invoice_no']."' ")[0]['payment_status'];

				$objCommon->doUpdateSql("UPDATE ".DB_PREFIX."sales_invoice_header set paid_amount = paid_amount + '".$result[0]['amount']."', payment_status = '".$payment_status."' where invoice_no='".$result[0]['order_id']."' ");

				//Update sales Order
				if($salesHeader['sales_trans_no'] !=''){
					$objCommon->doUpdateSql("UPDATE ".DB_PREFIX."sales_header set payment_status='".$payment_status."' where invoice_no='".$salesHeader['sales_trans_no']."' ");
				}
			}
			return $payment_status;
		}
		public function getTotalPaidAmount($invoice_no){
			global $objCommon,$global_config;
			$sql = "SELECT sum(amount) as total_amount FROM ".DB_PREFIX."order_payment where order_id='".$invoice_no."' AND payment_status != 'Pending' ";
			$result = $objCommon->getSelectQuery($sql);
			return $result[0]['total_amount'];
		}
		public function getCompannyDetails()
		{	
			global $objCommon,$global_config;
			$query = "SELECT * FROM ".DB_PREFIX."company"; 
			$result = $objCommon->getSelectQuery($query);
			return $result[0];
		}

		public function insertCashOnDelivery($data){

			global $objCommon,$global_config;
			if($data['do_no'] !=''){
				$data['do_no'] = base64_decode($data['do_no']);
				$status = 'Hold';
			}else{
				$data['do_no'] = '';
				$status = 'Paid';
			}
			 $query = "INSERT INTO " . DB_PREFIX . "order_payment (order_id, do_no, customer_id, delivery_man_id, payment_type,payment_method, amount, payment_date, added_on,payment_status)
			VALUES ('".$data['order_id']."','".$data['do_no']."', '".$data['customer_id']."', '".$data['delivery_man_id']."', '".$data['payment_type']."','".$data['payment_method']."', '".$data['amount']."', '".$data['payment_date']."', '".date('Y-m-d h:i:s')."','".$status."')";
			$result = $objCommon->doInsertSql($query);
			return $this->update_salesHeader($result,$status);
		}
		public function getShippingAddress($shipping_id){
			global $objCommon,$global_config;
			$result = $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."shipping where id='".$shipping_id."' ");
			return $result[0];	
		}
		public function getPaymentTypes(){
			global $objCommon,$global_config;
			$result = $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."payment_type_master where status='1' order by id ASC ");
			return $result;	
		}
		public function getDoDetails($do_no){
			global $objCommon,$global_config;
			$result = $objCommon->getSelectQuery("SELECT sdd.*,sid.sku_price,sid.description FROM ".DB_PREFIX."sales_do_details as sdd LEFT JOIN ".DB_PREFIX."sales_invoice_details as sid ON sdd.invoice_no=sid.invoice_no AND sdd.sku=sid.sku where sdd.do_no='".$do_no."' ");
			return $result;
		}
		public function getcompanyDetails(){
			global $objCommon,$global_config;
			return $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."company")[0];
		}
		public function getEmailTemplate($id){
			global $objCommon,$global_config;
			return $objCommon->getSelectQuery("SELECT * FROM ".DB_PREFIX."email_template where status='1' AND id='".$id."' ")[0];
		}
		public function sendReceiptToCustomer($payment, $salesHeader){
			global $objCommon,$global_config;

			$customers 	 = $this->getB2BCustomersbyCode($salesHeader['customer_code']);
			$company 	 = $this->getcompanyDetails();
			$emailTemp 	 = $this->getEmailTemplate('2');
			$dynamicTemp = $subject = $trading_name = $contact_name = '';

			if(!empty($emailTemp)){
				$trading_name = $company['name'];
				$link         = $company['web_url'];
				$email        = $company['email'];
				$phone        = $company['phone'];
				$contact_name = $customers['name'];
				$currency_code= $salesHeader['currency_code'] !='' ? $salesHeader['currency_code'] : ''; 
				$amount       = $salesHeader['net_total']; 
				$invoice_date = $salesHeader['header_remarks'] !='' ? date('d M Y',strtotime($salesHeader['header_remarks'])) : '';
				$break        = '<br>';
				$paid_amount  = $salesHeader['paid_amount'];
				$invoice_no   = $salesHeader['invoice_no'];

				$searchArr    = array('[invoice_number]','[trading_name]','[contact_name]','[currency_code]','[amount]','[invoice_date]','[break]','[link]','[email]','[phone]');
				$replaceArr  = array($invoice_no,$trading_name,$contact_name,$currency_code,$amount,$invoice_date,$break,$link,$email,$phone);
				$subject     = str_replace($searchArr, $replaceArr, $emailTemp['subject']);
				$dynamicTemp = str_replace($searchArr, $replaceArr, $emailTemp['description']);
			}

			$str = "<h5 style='font-size:25px;margin: 0px 25%;'>".$subject."</h5><br>
						<table style='width:100%;font-size:16px;'>
						<tr>
							<td align='center'><b>AMOUNT PAID</b></td>
							<td align='center'><b>DATE PAID</b></td>
							<td align='center'><b>PAYMENT METHOD</b></td>
						</tr>
						<tr>
							<td align='center'>".$payment['amount']."</td>
							<td align='center'>".date('d/m/Y',strtotime($payment['payment_date']))."</td>
							<td align='center'>".$payment['payment_method']."</td>
						</tr>
					</table>";
			$str .= "<br><br><h4 style='margin: 0 10%;font-size: 16px;'>SUMMARY<h4><br>";
			$str .= "<table style='width:100%;font-size:16px;background-color: #f5f9fc;'>
						<tr style=''>
							<td style='padding: 10px 10%;'>Payment for invoice(s)<br>SI32433425</td>
							<td align='left'>".$currency_code." ".$amount."</td>
						</tr>
						<tr style='border-top: 1px solid #e8ecef;'>
							<td style='border-top: 1px solid #e8ecef;padding: 10px 10%;font-size:17px;'><b>Amount Charged<b></td>
							<td align='left' style='border-top: 1px solid #e8ecef;font-size:17px;'><b>".$currency_code." ".$paid_amount."<b></td>
						</tr>
					</table><br><hr><br><p style='margin: 0 10%;'>".$dynamicTemp."</p>";

			if(SEND_EMAIL_TO_CUSTOMER=='1' && $customers['email']!='' && $dynamicTemp!=''){
	        	$to 		= $customers['email'];
				$subject 	= $subject;
				$adminEmail = ADMIN_EMAIL;
				$headers 	= "From: " . $adminEmail . "\r\n";
				$headers   .= "MIME-Version: 1.0\r\n";
				$headers   .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to, $subject, $str, $headers);
			}
		}
}
?>