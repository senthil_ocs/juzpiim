<?php

require_once(MAIN_COMMON_PATH."class.ExtentedDB.php");
class CommonClass extends extendsClassDB{
	var $Request;
	var $SQLArray;
	function __construct() {
		$this->Request 	= array();
		$this->SQLArray	= array();
		$this->extendsClassDB1();
	}
	function getSelectQuery($strSQL,$uSelect=0,$queryType=false){
		$this->dbSetQuery($strSQL,"select",$uSelect);
		return $this->MakeStripSlashes($this->dbSelectQuery());
	}
	function ExecuteQry($strSQL,$strSQLType = "update") {
		global $objSmarty;
 		$this->dbSetQuery($strSQL,$strSQLType);
		$this->dbExecuteQuery();
	}
	function doInsert($strTableName,$objFieldsArray) {
		
		global $objSmarty;
		if(is_array($objFieldsArray))
		{
			$strInsertFields = "";
			$strInsertValues = "";
			for($i=0;$i<count($objFieldsArray);$i++)
			{
				$strInsertFields.= $objFieldsArray[$i]["Field"];
				$strInsertValues.= "'".str_replace("?","",mb_convert_encoding($objFieldsArray[$i]["Value"], "ASCII"))."'";
				if($i<count($objFieldsArray)-1)
				{
					if($objFieldsArray[$i]["Field"]!=""){
					$strInsertFields.=", ";
					$strInsertValues.=", ";
					}
				}
			}
			 $strInsertQry = "INSERT INTO $strTableName($strInsertFields) VALUES($strInsertValues)"; 
 			$this->ExecuteQry($strInsertQry);
			$InsertId = mysqli_insert_id($this->dbLink);
			return $InsertId;
		}
		else
		{
			$objSmarty->assign("strErrorMsg","Error while adding new Data, Fields array is empty");
			return false;
		}
	}
	function doInsertSql($query) {
		global $objSmarty;
		
		if($query){
			$this->ExecuteQry($query);
			$InsertId = mysqli_insert_id($this->dbLink);
			return $InsertId;
		}else {
			$objSmarty->assign("strErrorMsg","Error while adding new Data, Fields array is empty");
			return false;
		}
	}
	function doUpdateSql($query) {
		global $objSmarty;
		if($query){
			$this->ExecuteQry($query);
			return true;
		}else {
			return false;
		}
	}
	function doUpdate($strTableName,$objFieldsArray,$WhereClause) {
 		if(is_array($objFieldsArray))
		{
			$strUpdateFields = "";
			for($i=0;$i<count($objFieldsArray);$i++)
			{
				$strUpdateFields.= $objFieldsArray[$i]["Field"]."="."'".str_replace("?","",mb_convert_encoding(addslashes($objFieldsArray[$i]["Value"]), "ASCII"))."'";
				if($i<count($objFieldsArray)-1)
				{
					if($objFieldsArray[$i]["Field"]!=""){
					$strUpdateFields.=", ";
					}
				}
			}
			$strUpdateQry = "UPDATE $strTableName SET $strUpdateFields $WhereClause";
  			$this->ExecuteQry($strUpdateQry);
			return true;
		}
		else
		{
			return false;
		}
	}
	function doDelete($strTableName,$WhereClause=''){
		if(empty($WhereClause)){
			$WhereClause = '';
		}
		$strDeleteQuery = " DELETE FROM ".$strTableName." WHERE ".$WhereClause;
		return $this->ExecuteQry($strDeleteQuery,"delete");
	}
	function AddInfoToDB($objArray,$Prefix,$TableName){
		$counter = 0;
		foreach($objArray as $key=>$value){
			$pos = strpos($key, $Prefix);
			if (!is_integer($pos)) {
			}else{
				$key = str_replace($Prefix,"",$key);
				$insertArray[$counter]["Field"] = $key;
				$insertArray[$counter]["Value"] = str_replace("'", " ",$value);
				$counter++;
			}
		}
		$insert_id = $this->doInsert($TableName,$insertArray);
		return $insert_id;
	}
	function UpdateInfoToDB($objArray,$Prefix,$TableName,$Where){
		$counter = 0;
		foreach($objArray as $key=>$value){
			$pos = strpos($key, $Prefix);
			if (!is_integer($pos)) {
			}else{
				$key = str_replace($Prefix,"",$key);
				$UpdateArray[$counter]["Field"] = $key;
				$UpdateArray[$counter]["Value"] = str_replace("'", " ",$value);
				$counter++;
			}
		}
		$res =$this->doUpdate($TableName,$UpdateArray,$Where);
		return $res;
	}
	
	function getBrowserType() {
		$ua = $_SERVER[HTTP_USER_AGENT]; 
		if (strpos($ua,'MSIE')>0) {
		  $B_Name="MSIE";
		  $B_Name1=1;
		} else if (strpos($ua,'Netscape')>0){
		  $B_Name="Netscape";
		  $B_Name1=2;
		} else if (strpos($ua,'Safari')>0){
		  $B_Name="Safari";
		  $B_Name1=2;
		} else {
		  $B_Name="Firefox";
		  $B_Name1=2;
		}
		return $B_Name;
	}
	function getRequestValues() {
		global $_GET,$_POST;
		return $_GET;
	}
	/**
		 * Apply stripslashes function for array of values 
		 * @param 	ToStripslash (array)			
		 * @return  Stripped array
	*/
	function MakeStripSlashes($array,$replaceValue='',$replaceValueTo='') {
		if($array) {
			foreach($array as $key=>$value) {
				if(is_array($value))  {
					$value=$this->MakeStripSlashes($value);
					if($replaceValue==''&&$replaceValueTo=='')
						$array_temp[$key]=str_replace("#AMP#","",$value);
					else
						$array_temp[$key]=str_replace($replaceValue,$replaceValueTo,$value);                      
				}
				else
					$array_temp[$key]=stripslashes(stripslashes($value));
			}    
		}   
		return $array_temp;   
	}
	function getMatches($strMatch,$strContent) {
		if(preg_match_all($strMatch,$strContent,$objMatches)){
			return $objMatches;
		}
		return "";
	}
	function doPrint($strContent) {
		print $strContent."<br>";flush();
	}
	function getFileContentByFile($objFileName) {
		return file_get_contents($objFileName);
	}
	function makeStrip($value) {
		$Data 	= ltrim($value);	
		$Data 	= rtrim($Data);	
		$Data 	= strtolower($Data);	
		$Data 	= htmlentities($Data);	
		$Data 	= stripslashes($Data);	
		$Data 	= strip_tags($Data);	
		$array1 = array(" ","'","[","]","->","<","amp;","&","--",".","gt;","@","?",",");
		$array2 = array("-","","","","","","","","-","","","","","");
		$Data 	= str_replace($array1,$array2,$Data);
		return $Data;
	}		
	function doRearrangeURlLinks($objLink){
		global $global_config;
		$strCurrrentLink = '';
		$strCurrrentPos	 = 0;
		$urlregex = "^(https?|ftp)\:\/\/([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?[a-z0-9+\$_-]+(\.[a-z0-9+\$_-]+)*(\:[0-9]{2,5})?(\/([a-z0-9+\$_-]\.?)+)*\/?(\?[a-z+&\$_.-][a-z0-9;:@/&%=+\$_.-]*)?(#[a-z_.-][a-z0-9+\$_.-]*)?\$";
		if (eregi($urlregex, $objLink)) {
			return $objLink;
		} else {
			return  $global_config["SiteGlobalPath"].$objLink;
		}
		return $strCurrrentLink;
	}
	function createCropThumb($sourceImageFile,$thumbImageFile,$newWidth,$newHeight) {
	   
		$strFileName	= $sourceImageFile;
		$cropHeight		= $newHeight;
		$cropWidth		= $newWidth;
		$fileType		= explode('.', $strFileName); 
		$fileType		= $fileType[count($fileType) -1];
		$fileType		= strtolower($fileType);
	
		$originalImageSize 	= getimagesize($strFileName);
		$originalWidth 		= $originalImageSize[0];
		$originalHeight 	= $originalImageSize[1];
	
		if($fileType=='jpg') {
			$originalImageGd = imagecreatefromjpeg($strFileName);
		}
		if($fileType=='gif') { 
			$originalImageGd = imagecreatefromgif($strFileName);
		}	
		if($fileType=='png') {
			$originalImageGd = imagecreatefrompng($strFileName);
		}
		if($fileType=='bmp') {
			$originalImageGd = imagecreatefromjpeg($strFileName);
		}
		$croppedImageGd = imagecreatetruecolor($cropWidth, $cropHeight);
		
		$wm = $originalWidth /$cropWidth;
		$hm = $original_height /$cropHeight;
		$h_height = $cropHeight/2;
		$w_height = $cropWidth/2;
		
		$transparent = imagecolorallocate($croppedImageGd, 255, 255, 255);
		imagefill($croppedImageGd, 0, 0, $transparent);
		imagecolortransparent($croppedImageGd, $transparent);
		if($original_width > $original_height ) {
			$adjusted_width = $originalWidth / $hm;
			$half_width 	= $adjusted_width / 2;
			$int_width 		= $half_width - $w_height;
			imagecopyresampled($croppedImageGd ,$originalImageGd ,-$int_width,0,0,0, $adjusted_width, $cropHeight, $originalWidth , $originalHeight );
		} 
		elseif(($original_width < $original_height ) || ($original_width == $original_height ))
		{
			$adjusted_height = $originalHeight / $wm;
			$half_height = $adjusted_height / 2;
			$int_height = $half_height - $h_height;
			imagecopyresampled($croppedImageGd , $originalImageGd ,0,-$int_height,0,0, $cropWidth, $adjusted_height, $originalWidth , $originalHeight );
		} 
		else {
			imagecopyresampled($croppedImageGd , $originalImageGd ,0,0,0,0, $cropWidth, $cropHeight, $originalWidth , $originalHeight );
		}
		
		if($fileType=='jpg') {
			imagejpeg($croppedImageGd,$thumbImageFile); 
		}
		if($fileType=='gif') { 
			imagegif($croppedImageGd,$thumbImageFile); 
		}	
		if($fileType=='png') {
			imagepng($croppedImageGd,$thumbImageFile);
		}
		if($fileType=='bmp') {
			imagepng($croppedImageGd,$thumbImageFile);
		}
		
		imagedestroy($croppedImageGd); 
		imagedestroy($originalImageGd); 
	}	
	function resizetheUploadImage($photo){
	 //RESIZE UPLOADED IMAGE TO SAVE SPACE ON SERVER
		$set_height   = "900"; // maximum height allowed
		$set_width    = "600"; // maximum width allowed
		
		$_GET['src'] = $photo;
		if($ext=='png'){
			$image = imagecreatefrompng($_GET['src']);
		}else if($ext=='gif'){
			$image = imagecreatefromgif($_GET['src']);
		}else{
			$image = imagecreatefromjpeg($_GET['src']);
		}
		   //$image = imagecreatefromjpeg($_GET['src']);
	   $size = getimagesize($_GET['src']);

	   $new_w = $size[0];
	   $new_h = $size[1];
	   $resized = imagecreatetruecolor($new_w, $new_h);

	   if ($size[0] > $set_width){ // resizes if max width is violated
		 $new_w = "$set_width";
		 $new_h = round(($set_width/$size[0])*$size[1]);
		 $resized = imagecreatetruecolor($new_w, $new_h); 
	   }

	   if ($size[1] > $set_height){ // resizes if max height is violated
		 $new_h = "$set_height";
		 $new_w = round(($set_height/$size[1])*$size[0]);
		 $resized = imagecreatetruecolor($new_w, $new_h);
	   }

	   if (($size[0] > $set_width) || ($size[1] > $set_height)){ // resizes if max height & max width is violated
		 $new_w = "$set_width";
		 $new_h = round(($set_width/$size[0])*$size[1]);
			 if($new_h > $set_height){
				$new_h = "$set_height";
				$new_w = round(($set_height/$new_h)*$new_w);
			 }
		 $resized = imagecreatetruecolor($new_w, $new_h); 
	   }

	   $ow = $size[0];
	   $oh = $size[1];

	   $new_image_resized = $_GET['src'];
	   imagecopyresampled($resized, $image, 0, 0, 0, 0, $new_w, $new_h, $ow, $oh);
	  // imagejpeg($resized, $new_image_resized, 80);
		if($ext=='png'){
		  imagepng($resized, $new_image_resized, 8);
		}else if($ext=='gif'){
		  imagegif($resized, $new_image_resized, 80);
		}else{
		  imagejpeg($resized, $new_image_resized, 80);
		}
	   imagedestroy($resized);
	   imagedestroy($image);
	}
	function createthumb($input_file_name, $output_filename, $new_w, $new_h='') {
		if (preg_match("/(jpg|jpeg)$/i",$input_file_name)){
			$src_img = imagecreatefromjpeg($input_file_name);
		} else if (preg_match("/png$/i",$input_file_name)){
			$src_img = imagecreatefrompng($input_file_name);
		} else if(preg_match("/bmp$/i",$input_file_name)){
			 $src_img = imagecreatefromjpeg($input_file_name);
		} else if(preg_match("/gif$/i",$input_file_name)){
			$src_img = imagecreatefromgif($input_file_name);
		} else {
			throw(new Exception("ERROR: Cant work with file $input_file_name becuase its an unsupported file type for this function."));
		}
	
		if( $src_img == false ) {
			throw(new Exception("ERROR: Unabel to open image file $input_file_name"));
		}
	
		$old_x = imageSX($src_img);
		$old_y = imageSY($src_img);
	
		if( $new_h == 0 ) {
			$thumb_w = $new_w;
					$thumb_h = $old_y * ($new_w / $old_x);
	
		} else if( $new_w == 0 ) {
			$thumb_h = $new_h;
			$thumb_w = $old_x * ($new_h / $old_y);
		} else {
			if ($old_x > $old_y) {
				$thumb_w = $new_w;
				$thumb_h = $old_y * ($new_h/$old_x);
			} else if ($old_x < $old_y) {
				$thumb_w = $old_x * ($new_w/$old_y);
				$thumb_h = $new_h;
			} else if ($old_x == $old_y) {
				$thumb_w = $new_w;
				$thumb_h = $new_h;
			}
		}
	
		$dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
	
		if (preg_match("/png$/i",$input_file_name)){
			imagepng($dst_img,$output_filename); 
		} else {
			imagejpeg($dst_img,$output_filename); 
		}
	    return $output_filename;
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	}	
	function _gdinfo() {
		$myreturn=array();
		if (function_exists('gd_info')) {
			$myreturn=gd_info();
		} else {
			$myreturn=array('GD Version'=>'');
			ob_start();
			phpinfo(8);
			$info=ob_get_contents();
			ob_end_clean();
			foreach (explode("\n",$info) as $line) {
				if (strpos($line,'GD Version')!==false) {
					$myreturn['GD Version']=trim(str_replace('GD Version', '', strip_tags($line)));
				}
			}
		}
		return $myreturn;
	}
	function checkFile($filename){
	  if (file_exists($filename)) {
		return 1;
	  }
	}
	function removeLastChar($rtnStr){
	 $res = substr($rtnStr, 0, strlen($rtnStr)-1);
	 return $res;
	}
	function removeFirstChar($rtnStr){
	 $res = substr($rtnStr, 1);
	 return $res;
	}	
	function delTree($dir) {
		$files = glob( $dir . '*', GLOB_MARK );
		foreach( $files as $file ){
			if( substr( $file, -1 ) == '/' )
				delTree( $file );
			else
				unlink( $file );
		}
		if (is_dir($dir)) rmdir( $dir );
	} 	
	function slugName($string){
		$string = preg_replace("`\[.*\]`U","",$string);
		$string = preg_replace('`&(amp;)?#?[a-z0-9]+;`i','-',$string);
		$string = htmlentities($string, ENT_COMPAT, 'utf-8');
		$string = preg_replace( "`&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);`i","\\1", $string );
		$string = preg_replace( array("`[^a-z0-9]`i","`[-]+`") , "-", $string);
		return strtolower(trim($string, '-'));
   }	
   function array_unique_deep($array) {
        $values=array();
        //ideally there would be some is_array() testing for $array here...
        foreach ($array as $part) {
            if (is_array($part)) $values=array_merge($values,array_unique_deep($part));
            	else $values[]=$part;
        }
        return array_unique($values);
	}
	function msort($array, $id="id", $sort_ascending=true) {
        $temp_array = array();
        while(count($array)>0) {
            $lowest_id = 0;
            $index=0;
            foreach ($array as $item) {
                if (isset($item[$id])) {
					if ($array[$lowest_id][$id]) {
						if (strtolower($item[$id]) < strtolower($array[$lowest_id][$id])) {
							$lowest_id = $index;
						}
                    }
				}
				$index++;
			}
            $temp_array[] = $array[$lowest_id];
            $array = array_merge(array_slice($array, 0,$lowest_id), array_slice($array, $lowest_id+1));
        }
		if ($sort_ascending) {
			return $temp_array;
		} else {
			return array_reverse($temp_array);
		}
	}
	function filter_by_value ($array, $index, $value){ 
        if(is_array($array) && count($array)>0)  { 
            foreach(array_keys($array) as $key){ 
                $temp[$key] = $array[$key][$index]; 
                if ($temp[$key] == $value || $temp[$key]!=$_SESSION['user_id']){ 
                    $newarray[$key] = $array[$key]; 
                } 
            } 
		} 
      	return $newarray; 
    } 
	function instr($haystack, $needle) { 
		$pos = strpos($haystack, $needle, 0); 
	  	if ($pos != 0) return true; 
		return false; 
	} 
 	function MakeStrips($str) {
		$str = trim($str);
		$str = stripslashes($str);
		$str = strip_tags($str);
		$str = mysql_real_escape_string($str);
		return $str;
	}
	function getRandomNumbers($length=10){
		srand(date("s")); 
		$possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		$string = ""; 
		while(strlen($string)<$length){ 
			$string .=substr($possible_charactors, rand()%((strlen($possible_charactors))),1); 
		} 
		return(uniqid($string)); 
	}
	// function inputHistory($content, $fileName) {
 //        global $global_config;
 //        $myFile = $fileName.".txt";
 //        $filePath = $global_config["SiteLocalFileUpload"]."input/";
 //       // $filePath = $global_config["SiteBase"]."input/";
 //        $fh = fopen($filePath.$myFile, 'a+'); //  or die("can't open file")
 //        $strContent = "";

 //        if(count($content) > 0) {
 //            //$strContent .= "\n";
 //            $cnt = 0;
 //            foreach($content as $key=>$value) {
 //                $cnt++;
 //                $strContent .= $key." : ".$value;
 //                if($cnt != count($content)) {
 //                    $strContent .= " || ";
 //                }
 //            }
 //        } else {
 //            $strContent = "No inputs received";
 //        }
 //        $stringData = "\n".date('Y-m-d H:i:s')." || ".$strContent."\n";
 //        fwrite($fh, stripslashes($stringData));
 //        fclose($fh);
 //    }
	function replaceArray($subject) {
		$search  = array("'", '"');
		$replace = array('','');
		$subject = $subject;
		return str_replace($search, $replace, $subject);
	}
	function currenttimestamp($str_user_timezone,
		$str_server_timezone = CONST_SERVER_TIMEZONE,
		$str_server_dateformat = CONST_SERVER_DATEFORMAT) {
		
		// set timezone to user timezone
		date_default_timezone_set($str_user_timezone);
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone($str_server_timezone));
		$str_server_now = $date->format($str_server_dateformat);
		
		// return timezone to server default
		date_default_timezone_set($str_server_timezone);
		
		return $str_server_now;
	}
 	/** Common Functions :: START **/
	function doGetTableListing($strTable,$strWhereClause=NULL,$strSelect='*',$orderBy=NULL,$groupBy=NULL){
		global $objCommon,$global_config;
		$sql 		=	"SELECT $strSelect FROM ".$strTable." $strWhereClause $groupBy $orderBy";		
 		$strResults =	$this->getSelectQuery($sql);
		return $strResults;
	}		
	function doGetTableSingleRecord($strTable,$strWhereClause=NULL,$strSelect='*'){
		global $objCommon,$global_config;
		$sql 		=	"SELECT $strSelect FROM ".$strTable." $strWhereClause ";		
		$strResults =	$this->getSelectQuery($sql);
		return $strResults[0];
	}		
 	function doCheckDuplication($strTablename,$strWhereClause=NULL) {
		global $objCommon,$global_config;
		$sql = "SELECT count(*) as Cnt FROM ".$strTablename." ".$strWhereClause.""; 
		$result =  $this->getSelectQuery($sql);	
		return $result[0]['Cnt'];
	} 	
	function doDeleteById($strTablename,$strFieldName,$strFieldValue) {
		global $objCommon,$global_config;
		$sql	= "DELETE FROM ".$strTablename." WHERE ".$strFieldName." = '".$strFieldValue."'";
		$this->ExecuteQry($sql);
	}
	function doDeleteBydate($strTablename,$strFieldName,$strFieldValue) {
		global $objCommon,$global_config;
		$strCurrentDate = doGetServerDateTime();
 		$sql	= "UPDATE ".$strTablename." SET  is_deleted ='Yes', date_deleted= '".$strCurrentDate."' WHERE ".$strFieldName." = '".$strFieldValue."'";
		$objCommon->ExecuteQry($sql);
	}
 	function json2array($json,$type=''){
		$json_array = array();
		if(get_magic_quotes_gpc()){
			$json = stripslashes($this->prepareInput($json));
		}
		$json = substr($json, 1, -1);
		$json = str_replace(array(":", "{", "[", "}", "]"), array("=>", "array(", "array(", ")", ")"), $json);
		
		@eval("\$json_array = array({$json});");
		return $json_array;
	}
	function post_json2array($json)
	{
		if (get_magic_quotes_gpc()) {
			$json = stripslashes($this->prepareInput($json));
		}
		if (strlen($json) > 0) {			
			$json_array = json_decode($json,true);
			return $json_array;
		}		
	}
 	function prepareInput($inputContent) {
		global $global_config;
		$input	=	preg_replace("/[\r\n]/", "||", $inputContent);
		if (get_magic_quotes_gpc()) {
			$validInput = stripslashes($input);
		} else {
			$validInput = $input;
		}
		// If using MySQL
		//$validInput = mysql_real_escape_string($validInput);
		$validInputs	= str_replace('||',"\n",$validInput);	
		return $validInputs;
	}
	function getGoogleAddress($lat,$lng){
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		$json = @file_get_contents($url);
		$data=json_decode($json);
		$status = $data->status;
		if($status=="OK"){
			return $data->results[0]->formatted_address;
		} else {
			return false;
		}
	}
	function singaporeConvertDate($strFormat=NULL, $strDateTime=NULL) {
		if ($strDateTime=='') {
			$strDate	=	date("Y-m-d H:i:s");
		} else {
			$strDate	=	$strDateTime;
		}
		$strZone	=	date_default_timezone_get();
		$date 		= new DateTime($strDate, new DateTimeZone($strZone));
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		if ($strFormat=='')
			return $date->format('Y-m-d');
		else
			return $date->format($strFormat);
	}
	function doGetSalesmanCheque($strWhereClause=NULL,$orderBy=NULL){
		global $objCommon,$global_config;
 		$sql  =	"SELECT A.customer_name,A.order_payment_id,B.* FROM  ".ORDERCOLLECTION."  as A, ".CHEQUEDETAIL."  as B WHERE A.order_payment_id = B.collectionid $strWhereClause $orderBy";
		$strResults =	$objCommon->getSelectQuery($sql);
		return $strResults;
	}
	function doTotalRecordCnt($strTablename,$strWhereClause=NULL,$strSelect='*') {
		global $objCommon,$global_config;
		$sql = "SELECT count($strSelect) as Cnt FROM ".$strTablename." ".$strWhereClause.""; 
		$result =  $this->getSelectQuery($sql);
		return $result[0]['Cnt'];
	} 
  
	/** Common Functions :: END **/
	function inputHistory($content, $fileName) {

        global $global_config;

        $myFile = $fileName.".txt";

        $filePath = $global_config["SiteLocalFileUpload"]."input/";

        $fh = fopen($filePath.$myFile, 'a+'); //  or die("can't open file")

        $strContent = "";



        if(count($content) > 0) {

            //$strContent .= "\n";

            $cnt = 0;

            foreach($content as $key=>$value) {

                $cnt++;

                $strContent .= $key." : ".$value;

                if($cnt != count($content)) {

                    $strContent .= " || ";

                }

            }

        } else {

            $strContent = "No inputs received";

        }

        $stringData = "\n".date('Y-m-d H:i:s')." || ".$strContent."\n";

        fwrite($fh, stripslashes($stringData));

        fclose($fh);

    }

    public function SentEmail($emailAry='',$toEmail='',$filename=''){

    $Message = $emailAry['message'];
    $subject = $emailAry['subject'];

	$toEmail = $toEmail;
	$mail = new PHPMailer();
	$mail->IsSMTP();
	$mail->Host = SMTP_HOST_NAME;
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = SMTP_CERT;
	$mail->SMTPDebug = 1;
	$mail->Username = SMTP_USER_NAME; //$global_config["info_email"];
	$mail->Port = SMTP_PORT;
	$mail->Password = SMTP_PASS; //"ZOgTdORc3O";
	$mail->WordWrap = 50;
	$mail->IsHTML(true);
	$mail->Subject = $subject;
	$mail->From = SMTP_FROM; 
	$mail->FromName = SMTP_FROMNAME;  
	$mail->AddAddress($toEmail);
	//$mail->AddCC("ragu@oclocksolutions.com");
	if(!empty($filename)){
	$mail->AddAttachment($filename);
	}
	$mail->Body = $Message ;
	$mail->Send();
	/*if($mail->Send()) {
		 echo "mail sent ";
	} else {
		 echo "mail not sent ";
	}*/
    }


  }
?>