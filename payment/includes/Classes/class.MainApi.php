<?php
	class MainApi {		
		function escape($value)
		{
			return str_replace(array("\\", "\0", "\n", "\r", "\x1a", "'", '"'), array("\\\\", "\\0", "\\n", "\\r", "\Z", "\'", '\"'), $value);
		}		
		
		function gettodayPrice($currency_code)
		{
			global $objCommon,$global_config;
			$date = date('Y-m-d');
			$whereclause = " WHERE date = '".$date."' AND currency_code='".$currency_code."'";
			$rate = $objCommon->doGetTableSingleRecord(RATEMASTER,$whereclause);
			return $rate['price'];
		}
		
		function update_members_session($CustNRICNO,$postAry='')
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE CustNRICNO = '".$CustNRICNO."'";
			$objArray['cus_session_id']		  = $objCommon->getRandomNumbers();
			$objArray['cus_device_token'] 	  = $postAry['device_token'];
			$objArray['cus_device_type'] 	  = $postAry['device_type'];
			$objArray['cus_version'] 	      = $postAry['version'];
			$objCommon->UpdateInfoToDB($objArray,"cus_",CUSTOMER,$WhereClause);
			return $objArray['cus_session_id'];
		}
		function checkuserSession($CustNRICNO,$session_id)
		{
			global $objCommon,$global_config;
			$whereclause = " WHERE session_id = '".$session_id."' AND CustNRICNO = '".$CustNRICNO."'";
			$customer = $objCommon->doGetTableSingleRecord(CUSTOMER,$whereclause);
			return $customer;
		}
		function update_members_session_logout($session_id)
		{
			global $objCommon,$global_config;
			$WhereClause = " WHERE session_id = '".$session_id."'";
			$objArray['cus_session_id']		  = '';
			$objCommon->UpdateInfoToDB($objArray,"cus_",CUSTOMER,$WhereClause);
			return $objArray['cus_session_id'];
		}
		
		function CloudprintFunction($purpose,$bene,$customer,$currency,$bank,$d,$t){
			global $objCommon,$objMain,$global_config;
			/*$purpose = $objMain->getpurposeName($purpose_id);
			$bene = $objMain->getBeneDetails($beneficiary_id);
		 	$cust = $objMain->getCust($applicant_customer_code); 
		 	$currency = $objMain->getcurcode($currency_code);
		 	$bank = $objMain->getbank($bank_id);*/
		 	$cust = $customer[0];
		 	//printArray($cust); exit;
			$filename = 'APT Remittance';
		      $strSetting['value'] ='1/7vH5r6SZgfuE-hAXYQ2TxhjOxugFrNRAX55acm3rNUs'; //icafedev@gmail.com / icafe@dev
		    
		      //$strSetting['value'] ='1/LiKa6KPrx_w7lBy_zSLttxP69RDf1SAZv0K1Roa-0RQ'; //icafedev2@gmail.com / icafe@dev
		       
		    $strContent = 
		    	'<html>
		    	<body>
						<div style="padding: 40px;">
					 	  	<table width="100%">
					 	  		<tr width="100%">
						 	  	<td width="50%" style="float:left;"><h5>ARCADE PLAZA TRADERS PTE LTD</h5></td><td width="50%" style="float:right;padding-left:80px;"> <h5>REMITTANCE FORM(Form APT/R001/08) </td>
						 	  	</tr>
						 	  	<tr></tr><br>
						 	  	<tr><td width="50%" style="float:left;">11 COLLYER QUAY</td>
						 	  	<td width="50%" style="float:right;padding-left:80px;">Date:     '.$d.'   '.$t.'</td></tr>
						 	  	<tr><td>#02-03 THE ARCADE</td><td width="50%" style="float:right;padding-left:80px;">* Member ID:   </td></tr>
						 	  	<tr><td width="50%" style="floatleft;">SINGAPORE  049317</td><td width="50%" style="float:right;padding-left:80px;">(Proceed to Part B & C if member)</td></tr>
						 	  	<tr><td width="50%" style="float:left;">Phone: +65 62277660 FAX:+65 6220 2519</td>
						 	  	<td width="50%" style="float:right;padding-left:80px;"><input type="checkbox">Application on behalf of company</td></tr>
						 	  	<tr><td width="50%" style="float:left;">remittance@aptonline.com.sg</td><td width="50%" style="float:right;padding-left:80px;"></td></tr>
						 	  	<tr><td width="50%" style="float:left;">www.aptonline.com.sg</td><td width="50%" style="float:right;padding-left:80px;"><input type="checkbox" checked>Application as individual</td></tr>
					 	  	</table>
 	  					<b style="float:left;">A.PERSONAL/AUTHORISED BUSINESS REPRESENTATIVE DETAILS</b>
				 	  	<table width="100%" border="1" style="border-collapse:collapse;">
				 	  	<tr width="100%"><td width="30%" style="float:left;font-weight:bold;font-size:20px;"><b>Name as in IC/Passport/FIN:</b></td><td width="30%" style="float:left;"><p>'.$cust['CustName'].'</p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Date of Birth:</b></td><td width="25%" style="float:right;"><p>'.$cust['CustDOB'].'</p></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="30%" style="float:left;font-weight:bold;font-size:20px;"><b>Address:</b></td><td width="30%" style="float:left;">'.$cust['CustAddress1'].','.$cust['CustAddress2'].'<br>'.$cust['CustAddress3'].'</p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Postal code:</b></td><td width="25%" style="float:right;"><p>'.$cust['CustPostalCode'].'</p></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>IC/Passport/FIN: </b></td><td width="10%" style="float:left;"><p>'.$cust['CustNRICNO'].'</p></td><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Nationality: </b></td><td width="10%" style="float:left;"><p>xvdf'.$cust['Nationality'].'</p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Contact:</b></td><td width="25%" style="float:right;"><p>'.$cust['CustMobileNO'].'</p></td></tr>
				 	  	<tr>
				 	  	</table>
				 	  	<b width="70%" style="float:left;">B.RENITTANS DETAILS</b><b width="30%" style="float:right;">C.TRANSACTION DETAILS</b>
				 	  	<table width="100%" border="1" style="border-collapse:collapse;">
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Name of Beneficiary:</b></td><td width="45%" style="float:left;">'.$bene['BeneName'].'</td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Currency & Amount :</b></td><td width="15%" style="float:right;"> '.$currency_code.'<br> '.$amount.'</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Address:</b></td><td width="45%" style="float:left;">'.$bene['BeneAddress1'].'<br>'.$bene['BeneAddress2'].','.$bene['BeneCountry'].'</td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Exchange Rate :</b></td><td style="float:right;">'.$currency_rate.'</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Purpose of :</b></td><td width="45%" style="float:left;">'.$purpose['name'].'</td><td width="30%" style="float:left; font-weight:bold;font-size:20px;"><b>Charges :</b></td><td width="15%" style="float:right;">'.$additional_amount.'</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Beneficiary Bank Name:</b></td><td width="45%" style="float:left;">'.$bank['bank_name'].'</td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Total SGD Amount :</b></td><td width="15%" style="float:right;">'.$total_amount.'</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Address / Branch:</b></td><td width="45%" style="float:left;">'.$bank['address1'].','.$bank['bank_country'].'</td><td width="30%" style="float:left; font-weight:bold;font-size:20px;"><b> For Office Use Only </b></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Beneficiary Account:</b></td><td width="45%" style="float:left;">'.$bank['sgd_account'].'</td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Payment Mode :</b></td><td width="15%" style="float:right;">CASH</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Swift / ABA Chips:</b></td><td width="45%" style="float:left;">'.$bank['swift_code'].'</td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Bank :</b></td><td width="15%" style="float:right;">'.$cust['CustDOB'].'</td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Intermediary Bank Details(If any):</b></td><td width="45%" style="float:left;"></td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Cheque No :</b></td><td width="15%" style="float:right;"></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Message (If any)::</b></td><td width="45%" style="float:left;"><p></p></td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Payment Stamp :</b></td><td width="15%" style="float:right;"></td></tr>
				 	  	<tr>
				 	  	</table>
				 	  	<b width="100%" style="float:left;">D.BUSINESS DETAILS(COMPLETE THIS SECTION ONLY IF REMITTANCE IS ON BEHALF OF A COMPANY)</b>
				 	  	<table width="100%" border="1" style="border-collapse:collapse;">
				 	  	<tr width="100%"><td width="40%" style="float:left;font-weight:bold;font-size:20px;"><b>Name of Business:</b></td><td width="60%" style="float:right;"></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Address of Business: </b></td><td width="45%" style="float:left;"><p></p></td><td width="15%" style="float:left; font-weight:bold;font-size:20px;"><b>Postal Code:</b></td><td width="15%" style="float:right;"></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Tel No:</b></td><td width="25%" style="float:left;"><b>Fax No</b></td><td width="25%" style="float:left; font-weight:bold;font-size:20px;"><b>Business Type :</b></td><td width="25%" style="float:right;"></td></tr>
				 	  	<tr>
				 	  	</table>
				 	  	<b width="100%" style="float:left;">E.ORIGINATOR(COMPLETE THIS SECTION ONLY IF REMITTANCE IS ON BEHALF OF AN  ANOTHER INDIVIDUAL)</b>
				 	  	<table width="100%" border="1" style="border-collapse:collapse;">
				 	  	<tr width="100%"><td width="30%" style="float:left;font-weight:bold;font-size:20px;"><b>Name as in IC/Passport/FIN:</b></td><td width="30%" style="float:left;"><p></p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Date of Birth:</b></td><td width="25%" style="float:right;"><p></p></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="30%" style="float:left;font-weight:bold;font-size:20px;"><b>Address:</b></td><td width="30%" style="float:left;"><p></p><p></p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Postal code:</b></td><td width="25%" style="float:right;"><p></p></td></tr>
				 	  	<tr>
				 	  	<tr width="100%"><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>IC/Passport/FIN: </b></td><td width="10%" style="float:left;"><p></p></td><td width="25%" style="float:left;font-weight:bold;font-size:20px;"><b>Nationality: </b></td><td width="10%" style="float:left;"><p></p></td><td width="25%" style="float:right; font-weight:bold;font-size:20px;"><b>Contact:</b></td><td width="25%" style="float:right;"><input type="checkbox">Sole Proprietor<input type="checkbox">Limited Partnership<input type="checkbox">LLP<input type="checkbox">Private Limited<input type="checkbox">Others___</td></tr>
				 	  	<tr>
				 	  	</table>
				 	  	<h5 style="float:right; padding-right:100px; ">DARIS</h5>
				 	  	<div style="float:left;">
				 	  	<p>____________________________________________</p>
				 	  	<h5>Applicant Signature / Company Stamp</h5><br>
				 	  	</div>
				 	  	<div style="float:right;">
				 	  	<p>____________________________________________</p>
				 	  	<h5>For ARCADE PLAZA TRANDERS PTE LTD</h5><br>
				 	  	</div>
 	  	
				 </body>
				</html>';
			if($filename!=''){
			require_once(FILEPATH.'MPDF/mpdf.php');
		    $mpdf=new mPDF('c','A4','','','','','','','','');
		    $mpdf->SetDisplayMode('fullpage');
		    $mpdf->list_indent_first_level = 0;
		    $stylesheet = file_get_contents(FILEPATH.'MPDF/mpdf.css');
		    $mpdf->WriteHTML($stylesheet,1);
		    $mpdf->WriteHTML($strContent,2);
		    $fileName = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $filename)));
		    $strPdfFilename = 'APTPrint'.'_'.$fileName.'_'.strtotime(date('Y-m-d H:i:s')).'.pdf';
		    $mpdf->Output(FILEPATH.'pdf/'.$strPdfFilename,'F');
		    $strFilePath = FILEPATH.'pdf/'.$strPdfFilename;
		    $url = GLOBALPATH.'gcloudprinter/print.php';
		    
		    $fields = array(
		        'token' => $strSetting['value'],
		        'pdf'   => $strFilePath,
		    );
		    foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		    rtrim($fields_string, '&');
		    //open connection
		    $ch = curl_init(); 
		    //set the url, number of POST vars, POST data
		    curl_setopt($ch,CURLOPT_URL, $url);
		    curl_setopt($ch,CURLOPT_POST, count($fields));
		    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_FAILONERROR, true); 

		    //execute post
		    $result = curl_exec($ch); 
		    list($headers, $content) = explode("\r\n\r\n",$result,2);
		    //close connection
		    curl_close($ch);
		    return $strFilePath;
			}
		}
		
    }
?>