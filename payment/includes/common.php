<?php  	
	ob_start();
	session_start();
	ini_set( "display_errors", 1);	
	set_time_limit(-1);
	ini_set('memory_limit', '1024M');

	if (!defined( "MAIN_INCLUDE_PATH" )) {
		define( "MAIN_INCLUDE_PATH", dirname(__FILE__)."/");
	}	
	include('config.php');	
	define("ABSOLUTE_PATH",					str_replace("core/","",MAIN_INCLUDE_PATH));	
	define("MAIN_CLASS_PATH",				MAIN_INCLUDE_PATH."Classes/");
	define("MAIN_COMMON_PATH",				MAIN_CLASS_PATH."Common/");
	
	

	require_once(MAIN_COMMON_PATH."class.Common.php");
	$objCommon = new CommonClass();
	
	require_once(MAIN_CLASS_PATH."class.ServiceApi.php");
	$objServiceApi = new ServiceApi();

	require_once("functions.php");
	define("ANDROID_PUSH_API_KEY", "");

?>