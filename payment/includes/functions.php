<?php
	function printArray($str){
		print "<pre>";
			print_r($str);
		print "</pre>";
	}
	function redirect($url){
		header("Location:".$url);
		exit;
	}
	
	function displayMessage($strMessage) {
		$_SESSION['flash_message'] = $strMessage;		
	}

	function errorMessage($strMessage) {
		$_SESSION['flash_message_error'] = $strMessage;		
	}

	function needLogin(){
		global $global_config;
		if($_SESSION['SESS_ADMINID']==''){
			redirect($global_config["SiteGlobalPath"].'login.php');
		}
	}		
	function getRandomNumbers($length=10){
		srand(date("s")); 
		$possible_charactors = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
		$string = ""; 
		while(strlen($string)<$length){ 
			$string .=substr($possible_charactors, rand()%((strlen($possible_charactors))),1); 
		} 
		return(uniqid($string)); 
	}	
	function showJsonResults($arr)
	{
		echo array2json($arr);
	}			
	function array2json($arr) {
		$parts = array();
		$is_list = false;   
		//Find out if the given array is a numerical array
		$keys = array_keys($arr);
		$max_length = count($arr)-1;
		if($arr) {
			if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {
				//See if the first key is 0 and last key is length - 1
				$is_list = true;
				for($i=0; $i<count($keys); $i++) {
					//See if each key correspondes to its position                  
					if($i != $keys[$i]) { //A key fails at position check.
						$is_list = false; //It is an associative array.
						break;
				   }
			   }
			}
		}		
		foreach($arr as $key=>$value) {
			if(is_array($value)) {
				//Custom handling for arrays
				if($is_list) $parts[] =	array2json($value); /* :RECURSION: */
				else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */
			} else {
				$str = '';
				if(!$is_list) $str = '"' . $key . '":';
						   //Custom handling for multiple data types
				if(is_numeric($value)) $str .= '"'.$value.'"'; //Numbers
				elseif($value === false) $str .= 'false'; //The booleans
				elseif($value === true) $str .= 'true';
				else $str .= '"' . $value . '"'; ///All other things
				// :TODO: Is there any more datatype we should be in the lookout for? (Object?)
				$parts[] = $str;
		   }
		}
		$json = implode(',',$parts);		
		if($is_list) return  '[' . $json . ']';//Return associative JSON
		return  '{' . $json . '}';//Return associative JSON
	}	
	
	/** Common Functions **/
    function get_extension($filename){
        $str=explode('/',$filename);
        $len=count($str);
        $str2=explode('.',$str[($len-1)]);
        $len2=count($str2);
        $ext=$str2[($len2-1)];
        return $ext;
    }	
    function json2array($json,$type=''){
		$json_array = array();
		if(get_magic_quotes_gpc()){
			$json = stripslashes($this->prepareInput($json));
		}
		$json = substr($json, 1, -1);
		$json = str_replace(array(":", "{", "[", "}", "]"), array("=>", "array(", "array(", ")", ")"), $json);
		@eval("\$json_array = array({$json});");
		return $json_array;
	}
	
	function prepareInput($inputContent) {
		global $global_config;
		$input	=	preg_replace("/[\r\n]/", "||", $inputContent);
		if (get_magic_quotes_gpc()) {
			$validInput = stripslashes($input);
		} else {
			$validInput = $input;
		}
		// If using MySQL
		//$validInput = mysql_real_escape_string($validInput);
		$validInputs	= str_replace('||',"\n",$validInput);	
		return $validInputs;
	}
	
	/** DATE TIME Functions : START **/
	function doUpdateDateDisplay($strDate) {
		return date("d-m-Y",strtotime($strDate));
	}
	function doStoreDateDB($strDate) {
		return date("Y-m-d",strtotime($strDate));
	}	
	function doGetServerDate(){
		$strDate	=	date("Y-m-d");
		return $strDate;
	}
	// function doGetServerDateTime(){
	// 	$strDate	=	date("Y-m-d H:i:s");
	// 	return $strDate;
	// }
	function doGetServerDateTime()
    {
        $strDate	=	date("Y-m-d H:i:s");
        $strZone	=	date_default_timezone_get();
        $date 		=	new DateTime($strDate, new DateTimeZone($strZone));
        $date->setTimezone(new DateTimeZone('Asia/Singapore'));		
        return $date->format('Y-m-d H:i:s');
    }
	function doDisplayDate($strDate) {
		return date("d-M-Y",strtotime($strDate));
	}
	function doDisplayDateTime($strDate) {
		return date("d-M-Y H:i:s",strtotime($strDate));
	}				
	function doVerifyDateTime($strDate) {
		return $strDate;
	}	
	function doDisplayDateFormat($strDate) {
		return date('M j, Y H:i:s A',strtotime($strDate));
	}
	function doConvertLocalToSingapore() {
		$strDate	=	date("Y-m-d H:i:s");
		$strZone	=	date_default_timezone_get();
		$date 		=	new DateTime($strDate, new DateTimeZone($strZone));
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));		
		return $date->format('Y-m-d H:i:s');
	}
	function base64_to_jpeg($base64_string, $output_file)
	{		
		$ifp = fopen( $output_file, 'wb' ); 	
		// split the string on commas
		// $data[ 0 ] == "data:image/png;base64"
		// $data[ 1 ] == <actual base64 string>
		$data = explode( ',', $base64_string );		
		// we could add validation here with ensuring count( $data ) > 1
		fwrite( $ifp, base64_decode( $data[ 1 ] ) );	
		// clean up the file resource
		fclose( $ifp ); 	
		return $output_file; 
	}
	function checkLogin(){
		global $global_config;
		if($_SESSION['SESS_USERCODE']==''){
			redirect($global_config["SiteFrontGlobalPath"].'index.php');
		}
	}

	function rotateImageForApp($filename,$degrees){ // ragu
	  // Content type
	  header('Content-type: image/jpeg');
	  // Load
	  $source = imagecreatefromjpeg($filename);

	  // Rotate
	  $rotate = imagerotate($source, $degrees, 0);
	  // Output
	  imagejpeg($rotate,$filename);
	}

	/** DATE TIME Functions : END **/
?>