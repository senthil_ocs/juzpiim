<?php
final class DBMSSQL {
	private $link;

	public function __construct($hostname, $username, $password, $database) {
		
		$connectionInfo = array( "Database"=>$database, "UID"=>$username, "PWD"=>$password);
     
		if (!$this->link = sqlsrv_connect( $hostname, $connectionInfo)) {
			exit('Error: Could not make a database connection using ' . $username . '@' . $hostname);
		}

		@sqlsrv_query("SET NAMES 'utf8'", $this->link);
		@sqlsrv_query("SET CHARACTER SET utf8", $this->link);
	}

	public function query($sql) {

		//echo "<br>".$sql;
		$resource = sqlsrv_query($this->link,$sql);

		if ($resource) {
			if (is_resource($resource)) {
				$i = 0;

				$data = array();
				while ($result = sqlsrv_fetch_array($resource,SQLSRV_FETCH_ASSOC)) {
					$data[$i] = $result;

					$i++;
				}

				//sqlsrv_free_result($resource);

				$query = new stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = $i;

				unset($data);

				return $query;	
			} else {
				return true;
			}
		} else {
			//trigger_error('Error: ' . ''. '<br />' . $sql);
			//exit();
		}
	}
	public function queryNew($sql) {

		//echo "<br>".$sql;
		$resource = sqlsrv_query($this->link,$sql);

		if ($resource) {
			if (is_resource($resource)) {
				$i = 0;

				$data = array();
				while ($result = sqlsrv_fetch_array($resource,SQLSRV_FETCH_ASSOC)) {
					$data[$i] = $result;

					$i++;
				}

				//sqlsrv_free_result($resource);

				$query = new stdClass();
				$query->row = isset($data[0]) ? $data[0] : array();
				$query->rows = $data;
				$query->num_rows = $i;

				unset($data);

				return $query;	
			} else {
				return true;
			}
		} else {
			 if( ($errors = sqlsrv_errors() ) != null) {
			 	$errorStr='';
		        foreach( $errors as $error ) {
		            $errorStr.= "SQLSTATE: ".$error[ 'SQLSTATE']."<br />";
		            $errorStr.= "code: ".$error[ 'code']."<br />";
		            $errorStr.= "message: ".$error[ 'message']."<br />";
		            $_SESSION['purchase_error'] = $errorStr;
		        }
   			 }
   			 return "error";
			 //trigger_error('Error: ' . ''. '<br />' . $sql);			
		}
	}

	function SelectQryMSSQL($Qry) {
		$result  = sqlsrv_query($this->link,$Qry) or die ("QUERY Error:".$Qry."<br>".sqlsrv_error());		
	    $row = array(); 
		$record = array();
		while ($row =sqlsrv_fetch_array($result,SQLSRV_FETCH_ASSOC)){ 
			$record[] = $row; 
		}		
		
		return $record;
	}

	public function escape($value) {
		/*$unpacked = unpack('H*hex', $value);
		return '0x' . $unpacked['hex'];*/
		if(get_magic_quotes_gpc())
	   {
	    $value= stripslashes($value);
	   }
	   return str_replace("'", "''", $value);		
	}


	public function countAffected() {
		return mssql_rows_affected($this->link);
	}

	public function getLastId() {
		$last_id = false;

		$resource =  sqlsrv_query($this->link,"SELECT @@identity AS id");

		if ($row = sqlsrv_fetch_array($resource)) {
			$last_id = trim($row[0]);
		}

		//sqlsrv_free_result($resource);

		return $last_id;
	}

	public function __destruct() {
		sqlsrv_close($this->link);
	}
}
?>