<?php
class User {
	private $user_id;
	private $username;
	private $is_allow_home;
	private $permission = array();
	private $cashier_permission = array();

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($username, $password,$login_method='') {
		if($login_method) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
		
			if ($user_query->num_rows) {
				$this->session->data['user_id'] = $user_query->row['user_id'];
				
				if($user_query->row['location_code']){
					$this->session->data['location_code'] = $user_query->row['location_code'];
				}

				$this->session->data['username'] = $user_query->row['username'];
				$this->session->data['company_id']	= $user_query->row['company_id'];
				$this->session->data['email'] = $user_query->row['email'];
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->session->data['is_allow_home']	= 1;
				$this->session->data['cashier_id']	= $user_query->row['user_id'];
				$this->company_id  = $user_query->row['company_id'];
	
				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");
				$permissions = unserialize($user_group_query->row['permission']);
				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
				return TRUE;
			}
		} else {

			//$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE email = '" . $this->db->escape($email) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
			
			
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND   password = '" . $this->db->escape(md5($password)) . "' AND status = '1'");
			//printArray($user_query->rows); exit;
			
			//$cashier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier WHERE email = '" . $this->db->escape($email) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");
			$cashier_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier WHERE username = '" . $this->db->escape($username) . "' AND password = '" . $this->db->escape(md5($password)) . "' AND status = '1'");

			if ($cashier_query->num_rows) {
				$this->session->data['company_id']	= $cashier_query->row['company_id'];
				$this->session->data['is_allow_home']	= 1;
				$this->session->data['cashier_id']	= $cashier_query->row['cashier_id'];
				$this->session->data['username']	= $cashier_query->row['cashier_name'];
				$this->session->data['email'] = $cashier_query->row['email'];
				$this->company_id  = $cashier_query->row['company_id'];
				
				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "cashier_group WHERE cashier_group_id = '" . (int)$cashier_query->row['cashier_group_id'] . "'");
				$cashier_permission = unserialize($cashier_query->row['permission']);
				if (is_array($cashier_permission)) {
					foreach ($cashier_permission as $key => $value) {
						$this->cashier_permission[$key] = $value;
					}
				}
				return TRUE;
			} elseif ($user_query->num_rows) {
				$this->session->data['user_id'] = $user_query->row['user_id'];
				$this->session->data['username'] = $user_query->row['username'];
				$this->session->data['company_id']	= $user_query->row['company_id'];
				$this->session->data['email'] = $user_query->row['email'];
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->session->data['is_allow_home']	= 1;
				$this->session->data['cashier_id']	= $user_query->row['user_id'];
				$this->company_id  = $user_query->row['company_id'];
				
				if($user_query->row['location_code']){
					$this->session->data['location_code'] = $user_query->row['location_code'];
				}

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");
				$permissions = unserialize($user_group_query->row['permission']);
				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
				return TRUE;
			}
		}
		return FALSE;
	}
	
	public function dogetCashierDetails($user_id, $type) {
	    if ($type == 'user') {
		    $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . $user_id . "' AND status = '1'");
		} else {
		    $result = $this->db->query("SELECT * FROM " . DB_PREFIX . "cashier WHERE user_id = '" . $user_id . "' AND status = '1'");
		} 
		return $result->row;
	}

	public function logout() {
	
		unset($this->session->data['user_id']);
		unset($this->session->data['username']);
		unset($this->session->data['company_id']);
		unset($this->session->data['is_allow_home']);
		unset($this->session->data['cashier_id']);
		unset($this->session->data['is_cashier_login']);
		unset($this->session->data['location_code']);

		$this->user_id = '';
		$this->company_id = '';
		$this->username = '';
		
		session_destroy();
	}
	

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}
	
	public function hasCashierPermission() {
		//echo $this->cashier_permission; exit;
		if (isset($this->cashier_permission[$key])) {
			return in_array($value, $this->cashier_permission[$key]);
		} else {
			return false;
		}
	}
	
	public function isAllowHome() {
		return $this->session->data['is_allow_home'];
	}

	public function isLogged() {
		return $this->user_id;
	}
	
	public function isCashierLogged() {
		return $this->session->data['is_cashier_login'];
	}
	
	public function getCompanyId() {
		return $this->company_id;
	}

	public function getId() {
		return $this->user_id;
	}
	
	public function getCashierId() {
		return $this->session->data['cashier_id'];
	}

	public function getUserName() {
		return $this->username;
	}

	public function getPermission()
	{
		return $this->permission;
	}
}
?>