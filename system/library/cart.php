<?php
class Cart {
	private $config;
	private $db;
	private $data = array();
	private $recent_data = array();
	private $data_recurring = array();

	public function __construct($registry) {
		$this->config 	= $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session 	= $registry->get('session');
		$this->db 		= $registry->get('db');
		$this->tax 		= $registry->get('tax');
		$this->weight 	= $registry->get('weight');

		if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
			$this->session->data['cart'] = array();
		}
	}
	public function getLocation($location_code=''){
		if($location_code=='HQ' || $location_code=='' ){
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id != '' order by sort_order asc"; 
		}else{
			$sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_code = '".$location_code."' order by sort_order asc"; 
		}
		$query = $this->db->query($sql);
		return $query->rows;	
	}
	public function getLocationByCode($location_code){
		return $this->db->query("SELECT *,phone as contact_no FROM ".DB_PREFIX."location WHERE location_code = '".$location_code."'")->row;	
	}
	public function getProducts($type = 'cart',$apply_tax_type='') {

		if (!$this->data) {
			if(!empty($this->session->data[$type])){
			foreach ($this->session->data[$type] as $key => $quantity) {
				//printArray($key);
				//echo 'dsfsgsdsdg111111';
				$product = explode(':', $key);
				$product_id = $product[0];
				$stock = true;

				// Options
				if (!empty($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				}

				// Profile

				if (!empty($product[2])) {
					$profile_id = $product[2];
				} else {
					$profile_id = 0;
				}

					/*echo "SELECT p.*,p2s.sku_qty as quantity,p2s.sku_price as price FROM " . DB_PREFIX . "product p
						LEFT JOIN " . DB_PREFIX . "product_stock p2s ON (p.sku = p2s.sku)
					WHERE p.product_id = '" . (int)$product_id . "'
					AND p.sku_status = '1'"; die;*/

					$product_query = $this->db->query("SELECT p.*,p2s.sku_qty as quantity,p2s.sku_cost,p2s.sku_avg,p2s.sku_price as price,p.package as hasChild FROM " . DB_PREFIX . "product p
						LEFT JOIN " . DB_PREFIX . "product_stock p2s ON (p.sku = p2s.sku)
					WHERE p.product_id = '" . (int)$product_id . "' 
					AND p.sku_status = '1'");
                   
				if ($product_query->num_rows) {

					$price = $product_query->row['price'];

					// Product Discounts
					$discount_quantity = 0;

					foreach ($this->session->data[$type] as $key_2 => $quantity_2) {
						$product_2 = explode(':', $key_2);

						if ($product_2[0] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}

					/*$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

					if ($product_discount_query->num_rows) {
						$price = $product_discount_query->row['price'];
					}*/

					// Product Specials
					$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE
						product_id = '" . (int)$product_id . "' AND ((date_start = '0000-00-00' OR date_start < curdate()) AND (date_end = '0000-00-00' OR date_end > curdate())) ORDER BY priority ASC, price ASC LIMIT 1");

					$discount_price	= '0.00';
					$original_price	= $product_query->row['price'];

					if ($product_special_query->num_rows) {
						$price = $product_special_query->row['price'];
						if($original_price > $special_price) {
							$discount_price	= $original_price - $price;
						}
					} elseif(!empty($this->session->data['update_price'][$product_id])) {
						$special_price = $this->session->data['update_price'][$product_id];
						$original_price	= $product_query->row['price'];
						if($original_price > $special_price) {
							$discount_price	= $original_price - $special_price;
						}
					}
					if($type=='cart_purchase'){
						$discount_price	= '0.00';
					}
					/*$user_price	= '12.35';
					if($user_price) {
						if($price > $user_price) {
							$discount_price	= $price - $user_price;
						}
						$price = $user_price;
					}*/


					// Reward Points
					/*$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "'");

					if ($product_reward_query->num_rows) {
						$reward = $product_reward_query->row['points'];
					} else {
						$reward = 0;
					}*/

					// Stock
					if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
						$stock = false;
					}

					$isCheckExchangeItems	= $this->session->data['is_exchange'];
					if(is_array($isCheckExchangeItems)){
						if(in_array($key,$isCheckExchangeItems)) {
							$is_exchange_product	= true;
							$quantity	= -abs($quantity);
						} else {
							$is_exchange_product	= false;
						}
					}else{
						$is_exchange_product	= false;
					}

					$purchaseDiscount = '';
					$purchaseDiscountMode = '';
					$purchaseDiscountValue = '';
					$taxAmount = '';
					$productRawPrice = '';
					
					if ($type == 'cart_purchase' && (!empty($this->session->data['purchase']))) {
					    $productPrice = $this->session->data['purchase']['price'][$product_id];
						$productRawPrice = $this->session->data['purchase']['raw_cost'][$product_id];
						$netAmount = $productPrice;
						$purchaseDiscount = $this->session->data['purchase']['purchase_discount'][$product_id];
						$purchaseDiscountMode = $this->session->data['purchase']['purchase_discount_mode'][$product_id];
						$purchaseDiscountValue = $this->session->data['purchase']['purchase_discount_value'][$product_id];
						$subTotal = $productPrice * $quantity;
						$taxClassId = $this->session->data['purchase']['tax_class_id'][$product_id];
						

						/*if($this->session->data['purchase']['vendor'][$product_id]!=''){
							$apply_tax_type	=  $this->getVendorsTaxId($this->session->data['purchase']['vendor'][$product_id]);
						}*/	
												
						if($apply_tax_type==''){
							$apply_tax_type	= $this->config->get('config_apply_tax_purchase');
						}

						if($apply_tax_type=='0') {
						    $taxAmount = 0;
						} else {
						    $taxAmount = $this->tax->getTax($subTotal-$purchaseDiscount, $taxClassId,$apply_tax_type);

							if($apply_tax_type=='2') {
							    if ($taxAmount > 0) {
								   // $subTotal -= $taxAmount;
								}
							}
						}

						$totalAmount = ($productPrice * $quantity) - $purchaseDiscount;
						$weightClassId = $this->session->data['purchase']['weight_class_id'][$product_id];
					} else {
					    $productPrice = ($original_price + $option_price);
						$subTotal     = $productPrice * $quantity;
						$netAmount    = ($price + $option_price);
						$totalAmount  = $productPrice * $quantity;
						$taxClassId = $product_query->row['tax_class_id'];
						$weightClassId = $product_query->row['weight_class_id'];
					}

					if($discount_price && $type != 'cart_purchase'){
						$totalAmount     = ($productPrice * $quantity) - ($discount_price * $quantity);
						$net_amount      = ($productPrice * $quantity) - ($discount_price * $quantity);
					}
					// printArray($product_query); die;

					$this->data[$key] = array(  
						'key'                       => $key,
						'is_exchange_product'       => $is_exchange_product,
						'product_id'                => $product_query->row['product_id'],
						'sku'                       => $product_query->row['sku'],
						'name'                      => $product_query->row['name'],
						'sku_price'                 => $product_query->row['price'],
						'sku_cost'                  => $product_query->row['sku_cost'],
						'sku_avg'                   => $product_query->row['sku_avg'],
						'image'                     => $product_query->row['image'],
						'quantity'                  => $quantity,
						'minimum'                   => $product_query->row['minimum'],
						'subtract'                  => $product_query->row['subtract'],
						'stock'                     => $stock,
						'price'                     => $productPrice,
						'pricewithdiscount'         => $productPrice - $discount_price,
						'raw_cost'                  => $productRawPrice,
						'sku_qty'					=> $product_query->row['quantity'],
						//'discount_amount'           => $discount_price * $quantity,
						'discount_amount'           => $purchaseDiscount,
						'sub_total'                 => $subTotal,
						'total'                     => $totalAmount,
						//'net_amount'                => $productPrice * $quantity,
						'net_amount'                => $net_amount,
						'tax_class_id'              => $taxClassId,
						'weight'                    => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id'           => $weightClassId,
						'length'                    => $product_query->row['length'],
						'hasChild'                  => $product_query->row['hasChild'],
						'length_class_id'           => $product_query->row['length_class_id'],
						'purchase_discount'         => $purchaseDiscount,
						'purchase_discount_mode'    => $purchaseDiscountMode,
						'purchase_discount_value'   => $purchaseDiscountValue,
						'purchase_tax'              => $taxAmount,
						'reasons'               	=> $this->session->data['purchase']['reasons'][$product_id], /* code changed 26-09-2017 */
						'remark'               		=> $this->session->data['purchase']['remark'][$product_id], /* code changed 26-09-2017 */
						'neg_quantity'              => $this->session->data['purchase']['neg_quantity'][$product_id], /* code changed 28-09-2017 */
						'foc_quantity'              => $this->session->data['purchase']['foc_quantity'][$product_id],
						'original_quantity'         => $this->session->data['purchase']['original_quantity'][$product_id],
						'description'         		=> $this->session->data['purchase']['description'][$product_id],
					);

					//printArray($this->data);
				} else {
					$this->remove($key);
				}
			}
		}
		}
		return $this->data;
	}

	public function getProduct($product_id) {
		if (!$this->recent_data) {
			$stock = true;
			$product_query = $this->db->query("SELECT p.*,pd.name As name FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)
				WHERE p.product_id = '" . (int)$product_id . "'
				AND p.status = '1'");

			if ($product_query->num_rows) {

				$price = $product_query->row['price'];
				// Product Discounts
				$discount_quantity = 0;
				$quantity = 0;

				foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
					$product_2 = explode(':', $key_2);

					if ($product_2[0] == $product_id) {
						$discount_quantity += $quantity_2;
						$quantity	= $quantity_2;
					}

				}

				/*$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}*/

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE
					product_id = '" . (int)$product_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				$discount_price	= '0.00';
				if ($product_special_query->num_rows) {
					$special_price = $product_special_query->row['price'];
					$original_price	= $product_query->row['price'];
					if($original_price > $special_price) {
						$discount_price	= $original_price - $special_price;
					}
				} elseif(!empty($this->session->data['update_price'][$product_id])) {
					$special_price = $this->session->data['update_price'][$product_id];
					$original_price	= $product_query->row['price'];
					if($original_price > $special_price) {
						$discount_price	= $original_price - $special_price;
					}
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
					$stock = false;
				}
				$this->recent_data = array(
					'key'                       => $key,
					'product_id'                => $product_query->row['product_id'],
					'sku'                       => $product_query->row['sku'],
					'name'                      => $product_query->row['name'],
					'image'                     => $product_query->row['image'],
					'quantity'                  => $quantity,
					'remain_qty'                => ($product_query->row['quantity']) - $quantity,
					'minimum'                   => $product_query->row['minimum'],
					'subtract'                  => $product_query->row['subtract'],
					'stock'                     => $stock,
					'price'                     => $product_query->row['price'],
					'special_price'             => $special_price,
					'discount_amount'           => $discount_price * $quantity,
					'total'                     => ($price) * $quantity,
					'tax_class_id'              => $product_query->row['tax_class_id'],
					'weight'                    => ($product_query->row['weight'] + $option_weight) * $quantity,
					'weight_class_id'           => $product_query->row['weight_class_id'],
					'length'                    => $product_query->row['length'],
					'length_class_id'           => $product_query->row['length_class_id'],
				);
			}
		}
		return $this->recent_data;
	}

	public function getRecurringProducts(){
		$recurring_products = array();

		foreach ($this->getProducts() as $key => $value) {
			if ($value['recurring']) {
				$recurring_products[$key] = $value;
			}
		}

		return $recurring_products;
	}

	public function add($product_id, $qty = 1, $option, $profile_id = '', $is_exchange_item = false, $type = 'cart', $purchase = '') {

		$key = (int)$product_id;
		if ($qty > 0 || $purchase['foc_quantity'] > 0) {
			if (!isset($this->session->data[$type][$key])) {
				$this->session->data[$type][$key] = $qty;
			} else {
				$this->session->data[$type][$key] += $qty;
			}
			$hasChild = isset($purchase['hasChild']) ? $purchase['hasChild'] : 0;  
			
			if($is_exchange_item) {
				if(!in_array($key,$this->session->data['is_exchange'])) {
					$this->session->data['is_exchange'][] = $key;
				}
			}
			if (($type == 'cart_purchase') && (!empty($purchase))) {
				$this->session->data['purchase']['price'][$key] = $purchase['price'];
				$this->session->data['purchase']['raw_cost'][$key] = $purchase['raw_cost'];
				$this->session->data['purchase']['purchase_discount'][$key] = $purchase['purchase_discount'];
				$this->session->data['purchase']['purchase_discount_mode'][$key] = $purchase['purchase_discount_mode'];
				$this->session->data['purchase']['purchase_discount_value'][$key] = $purchase['purchase_discount_value'];
				$this->session->data['purchase']['tax_class_id'][$key] = $purchase['tax_class_id'];
				$this->session->data['purchase']['weight_class_id'][$key] = $purchase['weight_class_id'];
				$this->session->data['purchase']['vendor'][$key] = $purchase['vendor'];
				$this->session->data['purchase']['hasChild'][$key] = $hasChild;
				$this->session->data['purchase']['reasons'][$key] = $purchase['reasons']; /* code changed 26-09-2017 */
				$this->session->data['purchase']['remark'][$key] = $purchase['remark']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['neg_quantity'][$key] = $purchase['neg_quantity']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['foc_quantity'][$key] = $purchase['foc_quantity'];
				$this->session->data['purchase']['original_quantity'][$key] = $purchase['original_quantity'];
				$this->session->data['purchase']['description'][$key] = $purchase['description'];
			}
		}
		$this->data = array();
	}

	public function update($key, $qty, $type = 'cart', $purchase = '') {
		if ($qty > 0 || $purchase['foc_quantity'] > 0) {

			$hasChild = isset($purchase['hasChild']) ? $purchase['hasChild'] : 0;
			$this->session->data[$type][$key] = $qty;

			if (($type == 'cart_purchase') && (!empty($purchase))) {
				$this->session->data['purchase']['price'][$key] = $purchase['price'];
				$this->session->data['purchase']['raw_cost'][$key] = $purchase['raw_cost'];
				$this->session->data['purchase']['purchase_discount'][$key] = $purchase['purchase_discount'];
				$this->session->data['purchase']['purchase_discount_mode'][$key] = $purchase['purchase_discount_mode'];
				$this->session->data['purchase']['purchase_discount_value'][$key] = $purchase['purchase_discount_value'];
				$this->session->data['purchase']['tax_class_id'][$key] = $purchase['tax_class_id'];
				$this->session->data['purchase']['hasChild'][$key] = $hasChild;
				$this->session->data['purchase']['weight_class_id'][$key] = $purchase['weight_class_id'];

				$this->session->data['purchase']['reasons'][$key] = $purchase['reasons']; /* code changed 26-09-2017 */
				$this->session->data['purchase']['remark'][$key] = $purchase['remark']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['neg_quantity'][$key] = $purchase['neg_quantity']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['foc_quantity'][$key] = $purchase['foc_quantity']; /* code changed 7-08-2018 */
				$this->session->data['purchase']['description'][$key] = $purchase['description'];

			}
		} else {
			$this->remove($key);
		}

		$this->data = array();
	}
	public function ajaxUpdateqtyAndPrice($type,$key, $qty, $data){
			// $type = 'cart_purchase';
			$this->session->data[$type][$key] = $qty;
			$this->session->data['purchase']['price'][$key] = $data['price'];
			$this->session->data['purchase']['raw_cost'][$key] = $data['taxAmount'];
			$this->session->data['purchase']['purchase_discount'][$key] = $data['discount'];

			$this->data = array();
	}
	public function updatePrice($key, $price) {
		if ($price && ($price > 0)) {
			$this->session->data['update_price'][$key] = $price;
		}
	}

	public function addDiscount($method='fixed',$discount='4') {
		$method	='fixed';
		$discount	='4';
		if ((int)$qty && ((int)$qty > 0)) {
			//$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
			$price	= $this->getTotal();
			if (!empty($price)) {
				if($method=='fixed' && $price >= $discount) {
					$discount_amount	= (int)$price - (int)$discount;
				} else {
					if($discount <= 100 && $discount >=1) {
						$discount_amount	= (int)$price - ((int)$price * ($discount / 100));
					}
				}
				$this->session->data['discount']	= $discount_amount;
			}
		}
	}

	public function remove($key, $type = 'cart') {
		if (isset($this->session->data[$type][$key])) {
			unset($this->session->data[$type][$key]);
		}

		$this->data = array();
	}

	public function clear($type = 'cart') {
		$this->session->data[$type] = array();
		$this->data = array();
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function newdisount($productPrice){
		$damount =0;
		$pcount = count($this->getProducts());
		if($this->session->data['discount']){
			if($this->session->data['discount_mode'] == "1"){
				 $percentage = $this->session->data['discount']/100;
				 $damount = $productPrice-($productPrice*(1-$percentage));
			}else{
				$damount = $this->session->data['discount'] / $pcount;
			}
		}
		return $damount;
	}
	public function getTaxes($apply_tax_type='') {
		$tax_data = array();
		if($apply_tax_type==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax');
		}
		$dis_amount =0;

		foreach ($this->getProducts() as $product) {

			//printArray($product);
			if ($product['tax_class_id']) {
				if($apply_tax_type=='2'){
				   $dis_amount = $this->newdisount($product['pricewithdiscount']);
				   $tax_rates = $this->tax->getRates($product['pricewithdiscount']-$dis_amount, $product['tax_class_id'],$apply_tax_type);
				}else{
				  $dis_amount = $this->newdisount($product['pricewithdiscount']);
				  $tax_rates = $this->tax->getRates($product['pricewithdiscount']-$dis_amount, $product['tax_class_id'],$apply_tax_type);
			    }
			  	foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = $product['purchase_tax'];
					} else {
						$tax_data[$tax_rate['tax_rate_id']] +=  $product['purchase_tax'];
					}
				}
			}
		}

		//printArray($tax_data);
		//if($this->session->data['discount']){

		//}
		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts($type = 'cart') {
		return count($this->session->data[$type]);
	}

	public function hasRecurringProducts(){
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}

	public function getTaxeByProduct($product,$taxType='') {
		$tax_data = 0;

		if ($product['tax_class_id']) {
			$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id'],$taxType);

			foreach ($tax_rates as $tax_rate) {
				if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
					$tax_data = ($tax_rate['amount'] * $product['quantity']);
				} else {
					$tax_data += ($tax_rate['amount'] * $product['quantity']);
				}
			}
		}

		return $tax_data;
	}

	public function getDiscountByProduct($product) {
	    $discount_percentage = $product['discount_percentage'];
		$discount_price	= $product['discount_price'];
		$subTotal	= $product['subTotal'];
		$discount_amount = 0;
		if (!empty($discount_percentage) && ($discount_percentage > 0)) {
			if($discount_percentage <= 100) {
				$discount_amount = $discount_percentage;
				$discount_mode = 1;
				$percentage = $discount_amount/100;
				$discount_amount = $subTotal-($subTotal*(1-$percentage));
			}
		} elseif (!empty($discount_price) && ($discount_price > 0)) {
			if ($discount_price <= $subTotal) {
				$discount_amount = $discount_price;
				$discount_mode = 2;
			}
		}
		return $discount_amount;
	}
	public function getVendorsTaxId($vendor_id) {
		$company_id	= $this->session->data['company_id'];
		$query = $this->db->query("SELECT tax_method FROM " . DB_PREFIX . "vendor where vendor_id = '" .$vendor_id. "'");
		return $query->row['tax_method'];
	}
	public function getReportHeader(){
		$sql 	= "SELECT DISTINCT * FROM " . DB_PREFIX . "company";
		$query = $this->db->query($sql);
		$company_details = $query->row;
		if(!empty($company_details)) {
			$company_name	 = strtoupper($company_details['name']);
			$company_address1= strtoupper($company_details['address1']);
			$company_address2= strtoupper($company_details['address2']);
			$company_city	 = strtoupper($company_details['city']);
			$company_country = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_phone   = $company_details['phone'];
			$company_fax     = $company_details['fax'];
			$print_date      = date('d/m/Y H:i:s');
			$str = '<table class="table orderlist statusstock">
                  <tr>
                    <td align="center" colspan="2"><h2>'.$company_name.'</h2></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2">'.$company_address1.','.$company_address2.'
                    </td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2">'.$company_city.'</td>
                  </tr>
                 
                  <tr class="print_contact">
                     <td align="center" style="border-right: none !important;"><span class="print_phone"><strong>Phone:</strong> '.$company_phone.'</span> 
                     <span style="float:right; margin:0 0 0 -30px;"><strong>Fax:</strong> '.$company_fax.'</span></td>
                    <td align="center"><span class="print_date"><strong>Print Date:</strong> '.$print_date .'</span></td>
                  </tr>
                </table>';
            return $str;
		}
	}
	public function getAllTerms(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."terms where status='1' ")->rows;
	}
	public function getCountry(){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."country where status='1' ")->rows;
	}
	public function updateSalesInvoiceDeliveryStatus($invoice_no){
		$allProducts = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_invoice_details WHERE invoice_no = '" .$invoice_no . "' and qty!=do_qty")->rows;
		$salesInvoiceHeader = $this->db->query("SELECT * FROM " . DB_PREFIX . "sales_invoice_header WHERE invoice_no = '" .$invoice_no . "'")->row;

		if(count($allProducts) == 0){
			$allDoRows = $this->db->query("SELECT count(*) as total FROM ".DB_PREFIX."sales_do_header where sales_transaction_no='".$invoice_no."' ")->row['total'];
			$deliveredDoRows = $this->db->query("SELECT count(*) as total FROM ".DB_PREFIX."sales_do_header where sales_transaction_no='".$invoice_no."' AND status='Delivered' ")->row['total'];

			if($allDoRows == $deliveredDoRows){
				$delivery_status = 'Delivered';
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header SET delivery_status='".$delivery_status."' where invoice_no='".$invoice_no."' ");
				if($salesInvoiceHeader['sales_trans_no'] !=''){
					$this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status='".$delivery_status."' where invoice_no='".$salesInvoiceHeader['sales_trans_no']."' ");
				}
			}else if($deliveredDoRows > 0){
				$delivery_status = 'Partial';
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header SET delivery_status='".$delivery_status."' where invoice_no='".$invoice_no."' ");
				if($salesInvoiceHeader['sales_trans_no'] !=''){
					$this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status='".$delivery_status."' where invoice_no='".$salesInvoiceHeader['sales_trans_no']."' ");
				}
			}
		}else{

			$deliveredDoRows = 0;
			$deliveredDoRows = $this->db->query("SELECT count(*) as total FROM ".DB_PREFIX."sales_do_header where invoice_no='".$invoice_no."' AND status='Delivered' ")->row['total'];
			if($deliveredDoRows > 0){
				$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header SET delivery_status='Partial' where invoice_no='".$invoice_no."' ");
				if($salesInvoiceHeader['sales_trans_no'] !=''){
					$this->db->query("UPDATE ".DB_PREFIX."sales_header SET delivery_status='Partial' where invoice_no='".$salesInvoiceHeader['sales_trans_no']."' ");
				}
			}
		}
	}

	public function updatetaggedSalesOrder($data){
		// $this->db->query("UPDATE ".DB_PREFIX."tagged_sales_orders set status='0' where order_no='".$order_no."' ");
		$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_details set tagged='0' where invoice_no ='".$data['invoice_no']."' AND product_id='".$data['product_id']."' ");
		$salesOrderNo = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no='".$data['invoice_no']."' ")->row['sales_trans_no'];
		$this->db->query("UPDATE ".DB_PREFIX."sales_detail set tagged='0' where invoice_no='".$salesOrderNo."' AND product_id='".$data['product_id']."' ");
	}
	public function updateOrderandInvoice($invoice_no){
		$taggedItems = $this->db->query("SELECT count(*) as m FROM ".DB_PREFIX."sales_invoice_details where invoice_no='".$invoice_no."' AND tagged='1'")->row['m'];
 		if($taggedItems == '0'){
 			$this->db->query("UPDATE ".DB_PREFIX."sales_invoice_header set istagged='0' where invoice_no ='".$invoice_no."'");
			$salesOrderNo = $this->db->query("SELECT * FROM ".DB_PREFIX."sales_invoice_header where invoice_no='".$invoice_no."' ")->row['sales_trans_no'];
 			$this->db->query("UPDATE ".DB_PREFIX."sales_header set istagged='0' where invoice_no='".$salesOrderNo."'");
 		}
	}
	public function checkTaggedProduct($data,$invoice_date){
		$data['invoice_date'] = $invoice_date;
		$sql = "SELECT sum(sd.qty) as resqty FROM ".DB_PREFIX."sales_detail as sd LEFT JOIN 
				".DB_PREFIX."sales_header as sh ON sd.invoice_no=sh.invoice_no LEFT JOIN 
				".DB_PREFIX."tagged_sales_orders as tg ON tg.order_no=sh.invoice_no and sd.purchase_id = tg.purchase_id LEFT JOIN 
				".DB_PREFIX."purchase as pu ON pu.purchase_id = tg.purchase_id 
				WHERE sh.location_code='".$data['location_code']."' AND sd.product_id='".$data['product_id']."' 
				AND sh.invoice_date <= '".$data['invoice_date']."' AND sd.tagged='1' AND sh.delivery_status='Pending' 
				AND tg.status='1' AND pu.isinvoice='1' GROUP BY sd.product_id ";  

		$reserved_qty = (int)$this->db->query($sql)->row['resqty'];

		if($reserved_qty > 0){
			$needed_qty   	   = (int)$data['needed_qty'];  // SO Qty
			$available_qty 	   = (int)$data['avl_qty'];     // in stock
			if( ($available_qty- $reserved_qty) >= $needed_qty){
				return true;
			}
			return false;
		}
		return true;
	}
	public function updatechildProductQty($data,$frm=''){
		$child_product = $this->db->query("SELECT child_sku,quantity FROM ".DB_PREFIX."child_products where parant_sku='".$data['product_id']."' ")->rows;
		$location 	   = $data['location_code'];
		// same code added in doc/class/service.api for cran job
		if(!empty($child_product)){
			foreach ($child_product as $value) {
				$quantity = $value['quantity'] * $data['qty'];
				if($frm  == 'P'){ // P-Purchase S-sales
					$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty+'" .$quantity."' WHERE product_id = '".$value['child_sku']."' AND location_Code='".$location."' ");
				}else{
					$this->db->query("UPDATE ".DB_PREFIX."product_stock SET sku_qty = sku_qty-'" .$quantity."' WHERE product_id = '".$value['child_sku']."' AND location_Code='".$location."' ");
				}
			}
		}
	}
	public function checkNetworksXero($networkId){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."networks where id='".$networkId."' ")->row['xero_post'];
	}
	public function getChannelsList(){
		$sql =  "SELECT * FROM ".DB_PREFIX."networks where status='Active' order by sort_order asc";
		$result = $this->db->query($sql);
		return $result->rows;
	}
	public function checkChildItemsStock($product_id,$qty,$location){
		$childItems = $this->db->query("SELECT cp.*,ps.sku_qty as avail_qty,ps.sku as childsku FROM ".DB_PREFIX."child_products as cp LEFT JOIN ".DB_PREFIX."product_stock as ps ON cp.child_sku=ps.product_id where parant_sku='".$product_id."' AND ps.location_Code='".$location."' ")->rows;

		$renStr = '';
		if(!empty($childItems)){
			foreach ($childItems as $childs) {
				$doQty = $qty * $childs['quantity'];

				if($childs['avail_qty'] >= $doQty){
					// no need 
				}else{
					$renStr .= $childs['childsku'].' -No Stock<br>';
				}
			}
			if($renStr==''){
				$res = array('status'=> true);
			}else{
				$res = array('status'=> false, 'err' => $renStr);
			}
		}else{
			$res = array('status'=> true);
		}
		return $res;
	}
	public function insertChildItems($data){
		$childItems = $this->getProductChildItems($data['product_id']);
		if(!empty($childItems)){
			foreach($childItems as $child){
				$this->db->query("INSERT INTO ".DB_PREFIX."order_child_items (order_no,type,parant_product_id,child_product_id,qty) VALUES ('".$data['order_no']."','".$data['type']."','".$data['product_id']."','".$child['child_sku']."','".$child['quantity']."' )");
			}
		}
	}
	public function getProductChildItems($product_id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."child_products where parant_sku='".$product_id."' ")->rows;
	}
	public function getUserDetails($id){
		return $this->db->query("SELECT * FROM ".DB_PREFIX."user where user_id='".$id."' ")->row;
	}	
}
?>