<?php
class Cart {
	private $config;
	private $db;
	private $data = array();
	private $recent_data = array();
	private $data_recurring = array();

	public function __construct($registry) {
		$this->config = $registry->get('config');
		$this->customer = $registry->get('customer');
		$this->session = $registry->get('session');
		$this->db = $registry->get('db');
		$this->tax = $registry->get('tax');
		$this->weight = $registry->get('weight');

		if (!isset($this->session->data['cart']) || !is_array($this->session->data['cart'])) {
			$this->session->data['cart'] = array();
		}
	}

	public function getProducts($type = 'cart',$apply_tax_type='') {
		if (!$this->data) {
			foreach ($this->session->data[$type] as $key => $quantity) {
				//printArray($key);
				//echo 'dsfsgsdsdg111111';
				$product = explode(':', $key);
				$product_id = $product[0];
				$stock = true;

				// Options
				if (!empty($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				}

				// Profile

				if (!empty($product[2])) {
					$profile_id = $product[2];
				} else {
					$profile_id = 0;
				}

					/*echo "SELECT p.*,p2s.sku_qty as quantity,p2s.sku_price as price FROM " . DB_PREFIX . "product p
						LEFT JOIN " . DB_PREFIX . "product_stock p2s ON (p.sku = p2s.sku)
					WHERE p.product_id = '" . (int)$product_id . "'
					AND p.sku_status = '1'";*/

					$product_query = $this->db->query("SELECT p.*,p2s.sku_qty as quantity,p2s.sku_price as price FROM " . DB_PREFIX . "product p
						LEFT JOIN " . DB_PREFIX . "product_stock p2s ON (p.sku = p2s.sku)
					WHERE p.product_id = '" . (int)$product_id . "'
					AND p.sku_status = '1'");

				if ($product_query->num_rows) {

					$price = $product_query->row['price'];

					// Product Discounts
					$discount_quantity = 0;

					foreach ($this->session->data[$type] as $key_2 => $quantity_2) {
						$product_2 = explode(':', $key_2);

						if ($product_2[0] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}

					/*$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

					if ($product_discount_query->num_rows) {
						$price = $product_discount_query->row['price'];
					}*/

					// Product Specials
					$product_special_query = $this->db->query("SELECT TOP 1 price FROM " . DB_PREFIX . "product_special WHERE
						product_id = '" . (int)$product_id . "' AND ((date_start = '0000-00-00' OR date_start < GETDATE()) AND (date_end = '0000-00-00' OR date_end > GETDATE())) ORDER BY priority ASC, price ASC");

					$discount_price	= '0.00';
					$original_price	= $product_query->row['price'];

					if ($product_special_query->num_rows) {
						$price = $product_special_query->row['price'];
						if($original_price > $special_price) {
							$discount_price	= $original_price - $price;
						}
					} elseif(!empty($this->session->data['update_price'][$product_id])) {
						$special_price = $this->session->data['update_price'][$product_id];
						$original_price	= $product_query->row['price'];
						if($original_price > $special_price) {
							$discount_price	= $original_price - $special_price;
						}
					}
					if($type=='cart_purchase'){
						$discount_price	= '0.00';
					}
					/*$user_price	= '12.35';
					if($user_price) {
						if($price > $user_price) {
							$discount_price	= $price - $user_price;
						}
						$price = $user_price;
					}*/


					// Reward Points
					/*$product_reward_query = $this->db->query("SELECT points FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "'");

					if ($product_reward_query->num_rows) {
						$reward = $product_reward_query->row['points'];
					} else {
						$reward = 0;
					}*/

					// Stock
					if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
						$stock = false;
					}

					$isCheckExchangeItems	= $this->session->data['is_exchange'];
					if(in_array($key,$isCheckExchangeItems)) {
						$is_exchange_product	= true;
						$quantity	= -abs($quantity);
					} else {
						$is_exchange_product	= false;
					}

					$purchaseDiscount = '';
					$purchaseDiscountMode = '';
					$purchaseDiscountValue = '';
					$taxAmount = '';
					$productRawPrice = '';
					
					if ($type == 'cart_purchase' && (!empty($this->session->data['purchase']))) {
					    $productPrice = $this->session->data['purchase']['price'][$product_id];
						$productRawPrice = $this->session->data['purchase']['raw_cost'][$product_id];
						$netAmount = $productPrice;
						$purchaseDiscount = $this->session->data['purchase']['purchase_discount'][$product_id];
						$purchaseDiscountMode = $this->session->data['purchase']['purchase_discount_mode'][$product_id];
						$purchaseDiscountValue = $this->session->data['purchase']['purchase_discount_value'][$product_id];
						$subTotal = $productPrice * $quantity;
						$taxClassId = $this->session->data['purchase']['tax_class_id'][$product_id];
						if($apply_tax_type==''){
							$apply_tax_type	= $this->config->get('config_apply_tax');
						}

						if($apply_tax_type=='0') {
						    $taxAmount = 0;
						} else {
						    $taxAmount = $this->tax->getTax($subTotal, $taxClassId,$apply_tax_type);
							if($apply_tax_type=='2') {
							    if ($taxAmount > 0) {
								    $subTotal -= $taxAmount;
								}
							}
						}

						$totalAmount = ($productPrice * $quantity) - $purchaseDiscount;
						$weightClassId = $this->session->data['purchase']['weight_class_id'][$product_id];
					} else {
					    $productPrice = ($original_price + $option_price);
						$subTotal     = $productPrice * $quantity;
						$netAmount    = ($price + $option_price);
						$totalAmount  = $productPrice * $quantity;
						$taxClassId = $product_query->row['tax_class_id'];
						$weightClassId = $product_query->row['weight_class_id'];
					}

					if($discount_price && $type != 'cart_purchase'){
						$totalAmount     = ($productPrice * $quantity) - ($discount_price * $quantity);
						$net_amount      = ($productPrice * $quantity) - ($discount_price * $quantity);
					}


					$this->data[$key] = array(
						'key'                       => $key,
						'is_exchange_product'       => $is_exchange_product,
						'product_id'                => $product_query->row['product_id'],
						'sku'                       => $product_query->row['sku'],
						'name'                      => $product_query->row['name'],
						'image'                     => $product_query->row['image'],
						'quantity'                  => $quantity,
						'minimum'                   => $product_query->row['minimum'],
						'subtract'                  => $product_query->row['subtract'],
						'stock'                     => $stock,
						'price'                     => $productPrice,
						'pricewithdiscount'         => $productPrice - $discount_price,
						'raw_cost'                  => $productRawPrice,
						'discount_amount'           => $discount_price * $quantity,
						'sub_total'                 => $subTotal,
						'total'                     => $totalAmount,
						//'net_amount'                => $productPrice * $quantity,
						'net_amount'                => $net_amount,
						'tax_class_id'              => $taxClassId,
						'weight'                    => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id'           => $weightClassId,
						'length'                    => $product_query->row['length'],
						'length_class_id'           => $product_query->row['length_class_id'],
						'purchase_discount'         => $purchaseDiscount,
						'purchase_discount_mode'    => $purchaseDiscountMode,
						'purchase_discount_value'   => $purchaseDiscountValue,
						'purchase_tax'              => $taxAmount,
						'reasons'               	=> $this->session->data['purchase']['reasons'][$product_id], /* code changed 26-09-2017 */
						'remark'               		=> $this->session->data['purchase']['remark'][$product_id], /* code changed 26-09-2017 */
						'neg_quantity'              => $this->session->data['purchase']['neg_quantity'][$product_id], /* code changed 28-09-2017 */
					);

				} else {
					$this->remove($key);
				}
			}
		}
		return $this->data;
	}

	public function getProduct($product_id) {
		if (!$this->recent_data) {
			$stock = true;
			$product_query = $this->db->query("SELECT p.*,pd.name As name FROM " . DB_PREFIX . "product p
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "product_to_barcode p2b ON (p.product_id = p2b.product_id)
				WHERE p.product_id = '" . (int)$product_id . "'
				AND p.status = '1'");

			if ($product_query->num_rows) {

				$price = $product_query->row['price'];
				// Product Discounts
				$discount_quantity = 0;
				$quantity = 0;

				foreach ($this->session->data['cart'] as $key_2 => $quantity_2) {
					$product_2 = explode(':', $key_2);

					if ($product_2[0] == $product_id) {
						$discount_quantity += $quantity_2;
						$quantity	= $quantity_2;
					}

				}

				/*$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' AND customer_group_id = '" . (int)$customer_group_id . "' AND quantity <= '" . (int)$discount_quantity . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY quantity DESC, priority ASC, price ASC LIMIT 1");

				if ($product_discount_query->num_rows) {
					$price = $product_discount_query->row['price'];
				}*/

				// Product Specials
				$product_special_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product_special WHERE
					product_id = '" . (int)$product_id . "' AND ((date_start = '0000-00-00' OR date_start < NOW()) AND (date_end = '0000-00-00' OR date_end > NOW())) ORDER BY priority ASC, price ASC LIMIT 1");

				$discount_price	= '0.00';
				if ($product_special_query->num_rows) {
					$special_price = $product_special_query->row['price'];
					$original_price	= $product_query->row['price'];
					if($original_price > $special_price) {
						$discount_price	= $original_price - $special_price;
					}
				} elseif(!empty($this->session->data['update_price'][$product_id])) {
					$special_price = $this->session->data['update_price'][$product_id];
					$original_price	= $product_query->row['price'];
					if($original_price > $special_price) {
						$discount_price	= $original_price - $special_price;
					}
				}

				// Stock
				if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
					$stock = false;
				}
				$this->recent_data = array(
					'key'                       => $key,
					'product_id'                => $product_query->row['product_id'],
					'sku'                       => $product_query->row['sku'],
					'name'                      => $product_query->row['name'],
					'image'                     => $product_query->row['image'],
					'quantity'                  => $quantity,
					'remain_qty'                => ($product_query->row['quantity']) - $quantity,
					'minimum'                   => $product_query->row['minimum'],
					'subtract'                  => $product_query->row['subtract'],
					'stock'                     => $stock,
					'price'                     => $product_query->row['price'],
					'special_price'             => $special_price,
					'discount_amount'           => $discount_price * $quantity,
					'total'                     => ($price) * $quantity,
					'tax_class_id'              => $product_query->row['tax_class_id'],
					'weight'                    => ($product_query->row['weight'] + $option_weight) * $quantity,
					'weight_class_id'           => $product_query->row['weight_class_id'],
					'length'                    => $product_query->row['length'],
					'length_class_id'           => $product_query->row['length_class_id'],
				);
			}
		}
		return $this->recent_data;
	}

	public function getRecurringProducts(){
		$recurring_products = array();

		foreach ($this->getProducts() as $key => $value) {
			if ($value['recurring']) {
				$recurring_products[$key] = $value;
			}
		}

		return $recurring_products;
	}

	public function add($product_id, $qty = 1, $option, $profile_id = '', $is_exchange_item = false, $type = 'cart', $purchase = '') {

		$key = (int)$product_id;
		if ((int)$qty && ((int)$qty > 0)) {
			if (!isset($this->session->data[$type][$key])) {
				$this->session->data[$type][$key] = (int)$qty;
			} else {
				$this->session->data[$type][$key] += (int)$qty;
			}

			if($is_exchange_item) {
				if(!in_array($key,$this->session->data['is_exchange'])) {
					$this->session->data['is_exchange'][] = $key;
				}
			}
			if (($type == 'cart_purchase') && (!empty($purchase))) {
				$this->session->data['purchase']['price'][$key] = $purchase['price'];
				$this->session->data['purchase']['raw_cost'][$key] = $purchase['raw_cost'];
				$this->session->data['purchase']['purchase_discount'][$key] = $purchase['purchase_discount'];
				$this->session->data['purchase']['purchase_discount_mode'][$key] = $purchase['purchase_discount_mode'];
				$this->session->data['purchase']['purchase_discount_value'][$key] = $purchase['purchase_discount_value'];
				$this->session->data['purchase']['tax_class_id'][$key] = $purchase['tax_class_id'];
				$this->session->data['purchase']['weight_class_id'][$key] = $purchase['weight_class_id'];

				$this->session->data['purchase']['reasons'][$key] = $purchase['reasons']; /* code changed 26-09-2017 */
				$this->session->data['purchase']['remark'][$key] = $purchase['remark']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['neg_quantity'][$key] = $purchase['neg_quantity']; /* code changed 28-09-2017 */
			}
		}
		$this->data = array();
	}

	public function update($key, $qty, $type = 'cart', $purchase = '') {
		if ((int)$qty && ((int)$qty > 0)) {

			$this->session->data[$type][$key] = (int)$qty;

			if (($type == 'cart_purchase') && (!empty($purchase))) {
				$this->session->data['purchase']['price'][$key] = $purchase['price'];
				$this->session->data['purchase']['raw_cost'][$key] = $purchase['raw_cost'];
				$this->session->data['purchase']['purchase_discount'][$key] = $purchase['purchase_discount'];
				$this->session->data['purchase']['purchase_discount_mode'][$key] = $purchase['purchase_discount_mode'];
				$this->session->data['purchase']['purchase_discount_value'][$key] = $purchase['purchase_discount_value'];
				$this->session->data['purchase']['tax_class_id'][$key] = $purchase['tax_class_id'];
				$this->session->data['purchase']['weight_class_id'][$key] = $purchase['weight_class_id'];

				$this->session->data['purchase']['reasons'][$key] = $purchase['reasons']; /* code changed 26-09-2017 */
				$this->session->data['purchase']['remark'][$key] = $purchase['remark']; /* code changed 28-09-2017 */
				$this->session->data['purchase']['neg_quantity'][$key] = $purchase['neg_quantity']; /* code changed 28-09-2017 */
			}
		} else {
			$this->remove($key);
		}

		$this->data = array();
	}

	public function updatePrice($key, $price) {
		if ($price && ($price > 0)) {
			$this->session->data['update_price'][$key] = $price;
		}
	}

	public function addDiscount($method='fixed',$discount='4') {
		$method	='fixed';
		$discount	='4';
		if ((int)$qty && ((int)$qty > 0)) {
			//$product_discount_query = $this->db->query("SELECT price FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
			$price	= $this->getTotal();
			if (!empty($price)) {
				if($method=='fixed' && $price >= $discount) {
					$discount_amount	= (int)$price - (int)$discount;
				} else {
					if($discount <= 100 && $discount >=1) {
						$discount_amount	= (int)$price - ((int)$price * ($discount / 100));
					}
				}
				$this->session->data['discount']	= $discount_amount;
			}
		}
	}

	public function remove($key, $type = 'cart') {
		if (isset($this->session->data[$type][$key])) {
			unset($this->session->data[$type][$key]);
		}

		$this->data = array();
	}

	public function clear($type = 'cart') {
		$this->session->data[$type] = array();
		$this->data = array();
	}

	public function getWeight() {
		$weight = 0;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$weight += $this->weight->convert($product['weight'], $product['weight_class_id'], $this->config->get('config_weight_class_id'));
			}
		}

		return $weight;
	}

	public function getSubTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $product['total'];
		}

		return $total;
	}

	public function newdisount($productPrice){
		$damount =0;
		$pcount = count($this->getProducts());
		if($this->session->data['discount']){
			if($this->session->data['discount_mode'] == "1"){
				 $percentage = $this->session->data['discount']/100;
				 $damount = $productPrice-($productPrice*(1-$percentage));
			}else{
				$damount = $this->session->data['discount'] / $pcount;
			}
		}
		return $damount;
	}
	public function getTaxes($apply_tax_type='') {
		$tax_data = array();
		if($apply_tax_type==''){
			$apply_tax_type	= (string)$this->config->get('config_apply_tax');
		}
		$dis_amount =0;

		foreach ($this->getProducts() as $product) {
			if ($product['tax_class_id']) {
				if($apply_tax_type=='2'){
				   $dis_amount = $this->newdisount($product['pricewithdiscount']);
				   $tax_rates = $this->tax->getRates($product['pricewithdiscount']-$dis_amount, $product['tax_class_id']);
				}else{
				  $dis_amount = $this->newdisount($product['pricewithdiscount']);
				  $tax_rates = $this->tax->getRates($product['pricewithdiscount']-$dis_amount, $product['tax_class_id'],$apply_tax_type);
			    }
			   // printArray($tax_rates);
				foreach ($tax_rates as $tax_rate) {
					if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
						$tax_data[$tax_rate['tax_rate_id']] = ($tax_rate['amount'] * $product['quantity']);
					} else {
						$tax_data[$tax_rate['tax_rate_id']] += ($tax_rate['amount'] * $product['quantity']);
					}
				}
			}
		}

		//printArray($tax_data);
		//if($this->session->data['discount']){

		//}
		return $tax_data;
	}

	public function getTotal() {
		$total = 0;

		foreach ($this->getProducts() as $product) {
			$total += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
		}

		return $total;
	}

	public function countProducts() {
		$product_total = 0;

		$products = $this->getProducts();

		foreach ($products as $product) {
			$product_total += $product['quantity'];
		}

		return $product_total;
	}

	public function hasProducts($type = 'cart') {
		return count($this->session->data[$type]);
	}

	public function hasRecurringProducts(){
		return count($this->getRecurringProducts());
	}

	public function hasStock() {
		$stock = true;

		foreach ($this->getProducts() as $product) {
			if (!$product['stock']) {
				$stock = false;
			}
		}

		return $stock;
	}

	public function hasShipping() {
		$shipping = false;

		foreach ($this->getProducts() as $product) {
			if ($product['shipping']) {
				$shipping = true;

				break;
			}
		}

		return $shipping;
	}

	public function hasDownload() {
		$download = false;

		foreach ($this->getProducts() as $product) {
			if ($product['download']) {
				$download = true;

				break;
			}
		}

		return $download;
	}

	public function getTaxeByProduct($product,$taxType='') {
		$tax_data = 0;

		if ($product['tax_class_id']) {
			$tax_rates = $this->tax->getRates($product['price'], $product['tax_class_id'],$taxType);

			foreach ($tax_rates as $tax_rate) {
				if (!isset($tax_data[$tax_rate['tax_rate_id']])) {
					$tax_data = ($tax_rate['amount'] * $product['quantity']);
				} else {
					$tax_data += ($tax_rate['amount'] * $product['quantity']);
				}
			}
		}

		return $tax_data;
	}

	public function getDiscountByProduct($product) {
	    $discount_percentage = $product['discount_percentage'];
		$discount_price	= $product['discount_price'];
		$subTotal	= $product['subTotal'];
		$discount_amount = 0;
		if (!empty($discount_percentage) && ($discount_percentage > 0)) {
			if($discount_percentage <= 100) {
				$discount_amount = $discount_percentage;
				$discount_mode = 1;
				$percentage = $discount_amount/100;
				$discount_amount = $subTotal-($subTotal*(1-$percentage));
			}
		} elseif (!empty($discount_price) && ($discount_price > 0)) {
			if ($discount_price <= $subTotal) {
				$discount_amount = $discount_price;
				$discount_mode = 2;
			}
		}
		return $discount_amount;
	}

}
?>