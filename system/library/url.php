<?php
class Url {
	private $url;
	private $ssl;
	private $rewrite = array();
	
	public function __construct($url, $ssl = '') {
		$this->url = $url;
		$this->ssl = $ssl;
	}
		
	public function addRewrite($rewrite) {
		$this->rewrite[] = $rewrite;
	}
		
	public function link($route, $args = '', $connection = 'NONSSL') {
		if ($connection ==  'NONSSL') {
			$url = $this->url;
		} else {
			$url = $this->ssl;	
		}
		
		$url .= 'index.php?route=' . $route;
			
		if ($args) {
			$url .= str_replace('&', '&amp;', '&' . ltrim($args, '&')); 
		}
		
		foreach ($this->rewrite as $rewrite) {
			$url = $rewrite->rewrite($url);
		}
				
		return $url;
	}
	
	public function getCompanyAddress($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y');
		}
		return $company_address;
	}
	
	public function getCompanyAddressHeaderString($company_details) {
		$company_address = array();
		$combine_array	= array();
		if(!empty($company_details)) {
			$company_address['name']	 = strtoupper($company_details['name']);
			$company_address['address1'] = strtoupper($company_details['address1']);
			$company_address['address2'] = strtoupper($company_details['address2']);
			$company_address['city']	 = strtoupper($company_details['city']." ".$company_details['state']);
			$company_address['country']	 = strtoupper($company_details['country']." - ".$company_details['postal_code']);
			$company_address['phone']    = $company_details['phone'];
			$company_address['fax']      = $company_details['fax'];
			$company_address['print_date'] = date('d/m/Y H:i:s');

			$str = '<table style="width:100%;">
                  <tbody><tr>
                    <td align="center" colspan="2"><p style="font-size:14px;font-weight:bold;">'.$company_address['name'].'</p></td>
                  </tr>
                  <tr>
                    <td align="center" colspan="2"><p style="font-size:12px;font-weight:bold;">
                      '.$company_address['address1'].' '.$company_address['address2'].' '.$company_address['city'].', '.$company_address['country'].',
                      Phone : '.$company_address['phone'].'
                      </p></td>
                  </tr>                  
                </tbody></table><hr>';
            return $str;
		}
		
	}
	public function getCompanyAddressHeaderString2($data) {
		$combine_array	= array();
		if(!empty($data)) {

			$data['country']	= strtoupper($data['location']['country']." - ".$data['location']['postcode']);
			$data['address']    = $data['location']['address1'].' '.$data['location']['address1'];
			$data['print_date'] = date('d/m/Y H:i:s');
			$str = '<head><meta charset="UTF-8"></head>
			<style type="text/css"> .bottom_border{border-bottom: 2px solid black; }</style>
			<table style="width:100%;">
                  <tbody>
                  	<tr>
                       <td align="center"><img src="'.HTTP_SERVER.'image/'.$data['logo'].'" style="width: 100px;">
                    	</td>
	                </tr>
                  	<tr>
                       <td align="center">
                    	  <p style="font-size:24px;font-weight:bold;">'.strtoupper($data['name']).'</p>
                    	</td>
	                </tr>
	                <tr>
	                    <td align="center"><p>UEN No: '.$data['location']['business_reg_no'].'  GST Registration No: '.$data['location']['business_reg_no'].'</p>
	                    </td>
	                </tr>
	                <tr>
	                    <td align="center"><p>'.$data['address'].', '.$data['country'].'</p>
	                    </td>
	                </tr>
	                <tr>
	                    <td align="center"><p>Phone : '.$data['location']['phone'].'  Email : '.$data['location']['email'].'</p></td>
	                </tr>
	                <tr>
	                    <td align="right" style="font-size:16px;"><b>'.strtoupper($data['invoice_title']).'</b></td>
	                </tr>
                	</tbody>
                </table>';
            return $str;
		}
		
	}
	public function cleanSearchField($str){
		$srchAry = array("&amp;","amp;","&quot;");
		$replaceAry = array("&","",'"');
		$str = str_replace($srchAry, $replaceAry, $str);
		return $str;
	}
	public function purchasePdfHeaderString($data){

		$data['business_reg_no'] = $data['business_reg_no']!='' ? $data['business_reg_no']: '00000'; 
		$data['gst_reg'] = $data['gst_reg']!='' ? $data['gst_reg']: '00000';
		$data['fax'] = $data['fax']!='' ? $data['fax']: '00000';
		$logo = $data['logo'];

		$str = '<style type="text/css"> .customtable { font-family: Roboto,sans-serif; margin-bottom: 3px; margin-top: 15px; width: 100%; border: 1px solid; border-collapse: collapse; } .customtable td { border-left: 1px solid #000; } .customtable td:first-child { border-left: none; } .customtable th { border: 1px solid black; } .custmtable { font-family: Roboto,sans-serif; width: 100%; border: 1px solid black; } .addrstbl { font-family: Roboto,sans-serif; width: 100%; height: 180px; min-height: 180px; max-height: 180px; }</style>
				<font face="sans-serif">
				<body style="font-family: Roboto,sans-serif;"><table style="width:100%;">
              <tbody>
              <tr>
                <td align="center" colspan="2"><img src="image/'.$logo.'" style="width: 81px;"></td>
              </tr>
              <tr>
                <td align="center" colspan="2"><p style="font-size:24px;font-weight:bold;">'.strtoupper($data['name']).'</p></td>
              </tr>
              <tr>
                <td align="center" colspan="2"><p style="font-size:13px;font-weight:bold;">
                  Reg No: '.$data['business_reg_no'].' GST Reg No: '.$data['business_reg_no'].'</p></td>
              </tr>
              <tr>
                <td align="center" colspan="2">
                	<table>
                		<tr>
                			<td align="left"></td>
                			<td align="right">
			                	<p style="font-size:17px;font-weight:bold;">
			                  '.$data['address1'].' '.$data['address2'].' '.$data['city'].', '.$data['country'].'
                			</td>
                		</tr>
                	</table>
                  </td>
              </tr>
              <tr>
                <td align="center" colspan="2"><p style="font-size:13px;font-weight:bold;">
                  Tel: '.$data['phone'].' Fax: '.$data['fax'].'
                  </p></td>
              </tr>
              <br><br>
              <tr>
                <td align="center" colspan="2"><p style="font-size:17px;font-weight:bold;">'.$data['purchase_title'].'</p></td>
              </tr>
            </tbody></table>';
        // echo $str; die;
        return $str;	
	}
}
?>