<?php
class Rules {

	private $rule_id;
	private $rule_name;
	private $is_allow_home;
	private $cashier_permission = array();
	private $transaction_permission = array();

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');
		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");
			if ($user_query->num_rows) {
				$user_id	= $user_query->row['user_id'];
				$this->user_id	= $user_query->row['user_id'];
				$this->company_id	= $user_query->row['company_id'];
				$plan_details	= $user_query->row['plan_details'];
				//$plan_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pricingplan WHERE planName LIKE '%" . $plan_details . "%' AND status = 'Active'");
				$plan_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "pricingplan WHERE id = '" . $plan_details . "' AND status = 'Active'");
				if ($plan_query->num_rows) {
					$this->rule_id	= $plan_query->row['accessRule'];
				}
			}
		}
	}

	public function getAccess() {
		$rule_id	= $this->getRuleId();
		$rule_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "plan_restriction WHERE plan_restriction_id = '" . (int)$rule_id . "' AND status = '1'");
		if ($rule_query->num_rows) {
			return $rule_query->row;
		}
	}

	public function getAccessRuleMessage($messagefield='cashier') {
		$rule_access = $this->getAccess();
		$plan_restriction_rules	= unserialize(urldecode($rule_access['plan_restriction_rules']));
		$message	= array();
		if(!empty($plan_restriction_rules['plan_rule'])) {
			foreach($plan_restriction_rules['plan_rule'] as $plan_rule) {
				if($plan_rule['rule_field']=='cashier' && $messagefield=='cashier') {
					$cashierCount	= $this->getCasierCountByCompany();
					switch($plan_rule['operator']) {
						case "==":
							if($cashierCount == $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "!=":
							if($cashierCount != $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case ">=":
							if($cashierCount >= $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "<=":
							if($cashierCount <= $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case ">":
							if($cashierCount > $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "<":
							if($cashierCount < $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
					}
				}

				if($plan_rule['rule_field']=='transaction' && $messagefield=='transaction') {
					$transactionCount	= $this->getTransactionCountByCompany();
					switch($plan_rule['operator']) {
						case "==":
							if($transactionCount == $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "!=":
							if($transactionCount != $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case ">=":
							if($transactionCount >= $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "<=":
							if($transactionCount <= $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case ">":
							if($transactionCount > $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
						case "<":
							if($transactionCount < $plan_rule['value']) {
								$message[]	=  	$plan_rule['message'];
							}
						break;
					}
				}
			}
			return $message;
		}
	}

	public function getAccessConditionByRule($access_field='cashier') {

		$rule_access = $this->getAccess();
		$plan_restriction_rules	= unserialize(urldecode($rule_access['plan_restriction_rules']));
		if($access_field=='cashier') {
			$allow_cashier	= $plan_restriction_rules['allow_cashier'];
			$cashierCount	= $this->getCasierCountByCompany();
			if(empty($allow_cashier)) {
				return true;
			}
			if($cashierCount >= $allow_cashier) {
				return false;
			}

		}
		if($access_field=='transaction') {
			$allow_transaction	= $plan_restriction_rules['allow_transaction'];
			$transactionCount	= $this->getTransactionCountByCompany();
			if(empty($allow_transaction)) {
				return true;
			}
			if($transactionCount >= $allow_transaction) {
				return false;
			}
		}

		if($access_field=='product') {
			$allow_product	= $plan_restriction_rules['allow_product'];
			$productCount	= $this->getProductCountByCompany();
			if(empty($allow_product)) {
				return true;
			}
			if($productCount >= $allow_product) {
				return true;
			}
		}

		return true;
	}

	public function getCasierCountByCompany() {
		$company_id		=  $this->company_id;
		$cashier_query 	= $this->db->query("SELECT total_cashier FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."'");
		if ($cashier_query->num_rows) {
			return $cashier_query->row['total_cashier'];
		}

	}

	public function getTransactionCountByCompany() {
		$company_id		=  $this->company_id;
		$order_query 	= $this->db->query("SELECT total_transaction FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."'");
		if ($order_query->num_rows) {
			return $order_query->row['total_transaction'];
		}
	}

	public function getProductCountByCompany() {
		$company_id		=  $this->company_id;
		$order_query 	= $this->db->query("SELECT total_product FROM " . DB_PREFIX . "company WHERE company_id = '".$company_id."'");
		if ($order_query->num_rows) {
			return $order_query->row['total_product'];
		}
	}

	public function getRuleId() {
		return $this->rule_id;
	}

	public function getUserId() {
		return $this->user_id;
	}
}