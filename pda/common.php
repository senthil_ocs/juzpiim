<?php
	ob_start();
	session_start();
	set_time_limit(-1);
	error_reporting(0);
	ini_set('memory_limit', '1024M');
	ini_set('display_errors', '0');
	ini_set('output_buffering', '4096');
	ini_set('auto_detect_line_endings', true);
	if (!defined( "MAIN_INCLUDE_PATH" )) {
		define( "MAIN_INCLUDE_PATH", dirname(__FILE__)."/");
	}
	require_once("../config.php");
	define("SITEGLOBALPATH",HTTP_SERVER.'/pda/');
	define("SITELOCALPATH", DIR_SERVER.'/pda/');
	
	define('LOCATION_CODE', 'BB283');
	$transCode = date('ym');
	define('PURCHASE_CODE', 'PO'.LOCATION_CODE.$transCode);

	include_once(SITELOCALPATH."includes/class.apiCommon.php");
	include_once(SITELOCALPATH."includes/class.SqlFunctions.php");
	include_once(SITELOCALPATH."includes/class.api.php");
	$api = new api();

   ?>