<?php
//error_reporting(E_ALL);
class api extends MysqlFns
{
  function api()
  {
     $this->MysqlFns();
  }
  function printArray($str){
    print "<pre>";
      print_r($str);
    print "</pre>";
  }
  function Remove_whitespace($str)
  {
      if(isset($str)) {
          $RequestData = urldecode(trim($str));
      } else {
          $RequestData ="";
      }
      return $RequestData;
  }
  function dbInput($string)
  {
      $string = $this->Remove_whitespace($string);
      if (function_exists('mysql_real_escape_string')){
          return mysql_real_escape_string($string);
      }else if(function_exists('mysql_escape_string')) {
          return mysql_escape_string($string);
      }
      return addslashes($string);
  }

  function getProducts($location_code=''){
     /*$select = "select A.*,B.sku_qty,B.sku_salesQty,B.sku_cost,B.sku_price,B.sku_avg from tbl_product as A LEFT JOIN  tbl_product_stock as B on(A.sku=B.sku)
     order by A.sku OFFSET 0 ROWS FETCH NEXT 1000 ROWS ONLY";*/
     /*$select = "select A.*,B.sku_qty,B.sku_salesQty,B.sku_cost,B.sku_price,B.sku_avg from tbl_product as A LEFT JOIN  tbl_product_stock as B on(A.sku=B.sku)
     order by A.sku OFFSET 0 ROWS FETCH NEXT 1 ROWS ONLY";*/
     $where ='';
     if($location_code!=''){
       $where = "WHERE B.location_Code='".$location_code."'";
     }
     $select = "select A.*,B.sku_qty,B.sku_salesQty,B.sku_cost,B.sku_price,B.sku_avg, barcode
              from tbl_product as A LEFT JOIN  tbl_product_stock as B 
              on(A.sku=B.sku)
              left join v_product_barcode C on A.sku=C.sku ".$where."
              order by A.sku";

     $list = $this->SelectQryMSSQL($select);
     $barcodeList ='';
     foreach($list as $li){
        $nsku = trim($li['sku']);
        if($li['sku_cost']!='' && $li['sku_cost']!='0.00'){
           $li['sku_avg'] = $li['sku_cost'];
        }
        //$barcodeList = $this->getBarcodesbySKU($nsku);
        $newres[] = array("product_id"=>trim($li['product_id']),
                				  "name"=>trim(utf8_encode($li['name'])),
                				  "sku"=>trim($nsku),
                				  "sku_description"=>trim(utf8_encode($li['sku_description'])),
                				  "sku_shortdescription"=>trim(utf8_encode($li['sku_shortdescription'])),
        	      				  "sku_department_code"=>trim(utf8_encode($li['sku_department_code'])),
                          "sku_category_code"=>trim(utf8_encode($li['sku_category_code'])),
                          "sku_brand_code"=>trim(utf8_encode($li['sku_brand_code'])),
                          "sku_uom"=>utf8_encode($li['sku_uom']),
                          "sku_uom_type"=>trim(utf8_encode($li['sku_uom_type'])),
                          "sku_image"=>trim(utf8_encode($li['sku_image'])),
                          "sku_status"=>utf8_encode($li['sku_status']),
                          "sku_gstallowed"=>trim(utf8_encode($li['sku_gstallowed'])),
                          "sku_qty"=>trim($li['sku_qty']),
                          "sku_salesQty"=>$li['sku_salesQty'],
                          "sku_cost"=>trim($li['sku_cost']),
                          "sku_price"=>trim($li['sku_price']),
                          "sku_avg"=>$li['sku_avg'],
                          "barcodes"=>$li['barcode']
                        );
      }     
     
      return $newres;

  }

  function getBarcodes($location_code=''){
  	//$sql = "select * from tbl_product_barcode";
    $where ='';
     if($location_code!=''){
       $where = "WHERE B.location_Code='".$location_code."'";
     }
     $sql = "select A.* from tbl_product_barcode as A LEFT JOIN tbl_product_stock as B  on (A.sku=B.sku) ".$where."";

  	$res = $this->SelectQryMSSQL($sql);
    foreach($res as $re){
      $newres[] = array("sku"=>trim($re['sku']),"barcode"=>trim($re['barcode']),"sku_status"=>$re['sku_status']);
    }
    return $newres;
  }

  function getBarcodesbySKU($sku){
    $sql = "select barcode from tbl_product_barcode where sku='".$sku."'";
    $res = $this->SelectQryMSSQL($sql);
    return $res;
  }

  function getVendors(){
  	$sql = "select * from tbl_vendor";
  	$res = $this->SelectQryMSSQL($sql);
    foreach($res as $li){
      $newres[] = array("vendor_id"=>trim($li['vendor_id']),"company_id"=>trim($li['company_id']),
                        "vendor_name"=>trim($li['vendor_name']),"vendor_code"=>trim($li['vendor_code']),"address1"=>$li['address1'],
                        "address2"=>trim($li['address2']),"country"=>trim($li['country']),"postal_code"=>$li['postal_code'],
                        "phone"=>trim($li['phone']),"fax"=>trim($li['fax']),"email"=>$li['email'],
                        "gst"=>trim($li['gst']),"status"=>$li['status'],"tax_method"=>$li['tax_method']);
    }

    return $newres;
  }

  function dogetLocationCode(){
      $select = "SELECT location_code FROM tbl_company";
      $res = $this->SelectQryMSSQL($select);
      return $res[0]['location_code'];

  }
  function doUserLogin($objAry){
    $username    = $objAry['username'];
    $password    = $objAry['password'];
    $userRes = $this->loginCheck($username,$password);
    if($userRes['user_id']){
        $resultarray =$this->loginCheck($username,$password);
    }else{
       $resultarray = array("status"=>"failed","message"=>"Invalid username or password");
    }
    
    return $resultarray;
   }

   function loginCheck($username,$password){
      $select = "SELECT user_id,user_group_id,username,firstname,lastname,email,status FROM tbl_user WHERE username = '" . trim($username). "' AND password = '" . trim(md5($password)) . "' AND status = '1'";
      $res = $this->SelectQryMSSQL($select);
      return $res[0];
   }

   function insertTest(){
    $sql = "update tbl_user set company_id='2' where user_id='6'";
    $this->query($sql);
   }


    function ordersyncAPI($postData,$location_code='') {

      /*  $data = '{"Order":{"created_by":"1","discount":"","OrderDetails":[{"discount_price":"","price":"3.74","product_id":"8709","quantity":"1","sku":"950338","total_price":"2.74","unit_price":"3.74"}],"reference_date":"03/10/2018","reference_no":"1","shipping":"","sub_total":"2.74","tax":"0.00","total":"2.74","vendor_id":"72"}}';
      $objAry = json_decode($data);*/
        $transCode = date('ym');
        $purchaseCode =  'PO'.$location_code.$transCode;

        $objAry = json_decode($postData['data']);

        $avg_price  = $this->WAM($pid,$pqty,$price); // avg price calculation
        
       $transaction = "SELECT TOP 1 * FROM tbl_purchase WHERE transaction_no IS NOT NULL ORDER BY purchase_id DESC";
        $transInfo = $this->SelectQryMSSQL($transaction);
        if(!empty($transInfo)) {
          $trans          = substr($transInfo[0]['transaction_no'],-8);
          $new_trans      = $trans+1;
          $transaction_no = substr($purchaseCode,0,-2).$new_trans;
        } else {
          $transaction_no = $purchaseCode.'000001';
        }

        $transaction_date = date('Y-m-d');
        //$reference_date   = date('Y-m-d',strtotime($objAry->Order->reference_date));
        $reference_date   =  $this->changeDateFormat($objAry->Order->reference_date);
        $transaction_type = "PURINV";

        if(isset($objAry->Order) && $objAry->Order != "") {
            $objAry->Order->discount = 0;
            $insert = "INSERT INTO tbl_purchase (company_id, transaction_no, transaction_date, transaction_type, vendor_id, reference_no, reference_date,remarks,bill_discount_percentage, bill_discount_price, total,hold,purchase_return, created_by,created_date,date_modified,location_code,sub_total,gst) 
            values ('1','".$transaction_no."','".$transaction_date."','".$transaction_type."','".$objAry->Order->vendor_id."','".$objAry->Order->reference_no."','".
                          $reference_date."','app','0','".$objAry->Order->discount."','".$objAry->Order->total."','0','0','".$objAry->Order->created_by."',GETDATE(),GETDATE(),'".$location_code."','".$objAry->Order->total."','0')";
            $this->query($insert);
            $purchase_id = $this->getLastId();
            if(isset($purchase_id) && $purchase_id != "") {
               if(!empty($objAry->Order->OrderDetails)) {
                    foreach($objAry->Order->OrderDetails as $OrderDetails) {
                       
                        $tax_price = 0;
                        $OrderDetails->unit_price = $OrderDetails->price;
                        $OrderDetails->discount_price = 0;
                        $OrderDetails->total_price = $OrderDetails->quantity * $OrderDetails->price;
                        $net_price = $OrderDetails->total_price;  
                        $pid   = $OrderDetails->product_id;
                        $pqty  = $OrderDetails->quantity;
                        $price = $OrderDetails->price;
                        $foc   = $OrderDetails->foc;
                        $unit_price = $OrderDetails->unit_price;

                        $avg_price  = $this->WAM($pid,$pqty,$price); // avg price calculation
                        if($foc==''){
                          $foc = 0;
                        }

                       $orderInsert = "INSERT INTO tbl_purchase_to_product (purchase_id,product_id,weight_class_id,quantity,price,raw_cost,discount_percentage,discount_price,tax_class_id,foc,net_price,tax_price,total,avg_cost,avg_method,transaction_no,sku) 
                          values ('".$purchase_id."','".$OrderDetails->product_id."','1','".$OrderDetails->quantity."','".$OrderDetails->price."','".$OrderDetails->unit_price."','0','".$OrderDetails->discount_price."','1',".$foc.",'".$net_price."','".$tax_price."','".$OrderDetails->total_price."','".$avg_price."','WAM','".$transaction_no."','".$OrderDetails->sku."')";
                        $this->query($orderInsert);

                        if($OrderDetails->sku!=''){
                            if($foc){
                              $tqty = $OrderDetails->quantity + $foc;
                            }else{
                              $tqty = $OrderDetails->quantity;
                            }
                            $updateSql = "update tbl_product_stock set sku_qty = sku_qty+".$tqty.",sku_avg='".$avg_price."',sku_cost='".$unit_price."' where sku='".$OrderDetails->sku."' AND location_Code='".$location_code."'";
                            $this->query($updateSql);
                        }
                    }
                }
                $OrderInfo = (array)$objAry->Order;
                $finlArr   = array();
                foreach($OrderInfo as $key => $values) {
                    if($key == 'sub_total' || $key == 'discount' || $key == 'tax' || $key == 'shipping' || $key == 'total') {                       
                        $finlArr[$key] = $values;
                    }
                }              
                if($finlArr != "") {
                    $listArr = array('Sub-Total' => 'sub_total','Discount' => 'discount','GST' => 'tax','Order Totals' => 'total','shipping Price' => 'shipping');
                    foreach($finlArr as $k => $val) {
                        $title = array_search($k,$listArr);
                        $purchase_total = "INSERT INTO tbl_purchase_total (purchase_id,code,title,text,value,sort_order) VALUES ('".$purchase_id."','".$k."','".$title."','".$val."','".$val."','0')";
                        $this->query($purchase_total);
                    }                    
                } 
            }
        }
        if(isset($purchase_id) && $purchase_id == "") {
          $msg = "No data found";
           return $msg;
        } else {
          return $postData['data'];
        }
    }
    public function WAM($pid,$qty,$price){
       $cost  = '0';
       $stp1  = '';
       $stp2  = '';
       $sql   = "SELECT * FROM tbl_purchase_to_product WHERE product_id='".$pid."'";
       $res = $this->SelectQryMSSQL($sql);
       if(count($res)>=1){
        $stp1 = $qty * $price;
        $stp2 = $qty;
        for($i=0;$i<count($res);$i++){
           $stp1+= ($res[$i]['quantity'] * $res[$i]['price']);
           $stp2+= $res[$i]['quantity'];
        }
           $cost = $stp1 / $stp2;
       }else{
           $cost = ($qty * $price) / $qty;
       }
       return $cost;
    }

    public function updateProduct($objAry,$location_code=''){
        $sku_code  = $objAry['sku'];
        $sku_description = $this->clearField($objAry['sku_description']);
        $sku_department_code = $this->clearField($objAry['sku_department_code']);
        $sku_category_code = $this->clearField($objAry['sku_category_code']);
        $sku_brand_code = $this->clearField($objAry['sku_brand_code']);

         $sku_price = $objAry['sku_price'];

         $sql = "UPDATE tbl_product SET sku_description = '".$sku_description."',
                                       sku_department_code='".$sku_department_code."',
                                       sku_category_code='".$sku_category_code."',
                                       sku_brand_code='".$sku_brand_code."' where sku='".$sku_code."'";
         $this->query($sql);

        if($objAry['barcode']){
          $barcode = $objAry['barcode'];
          // duplicate check
           $duplicationsql = "SELECT  * FROM tbl_product_barcode WHERE barcode = '".$barcode."' AND sku_status='1'";
           $res = $this->SelectQryMSSQL($duplicationsql);
           if(count($res)>=1){

           }else{
           // insert
            $createdby = 'app';
            $barcodeSql = "INSERT INTO tbl_product_barcode (sku,barcode,sku_status,createby,createon,modifyby,modifyon) 
                      VALUES ('".$sku_code."','".$barcode."',1,'".$createdby."',GETDATE(),'".$createdby."',GETDATE())";
            $this->query($barcodeSql);
          }

        }
       
        if($sku_price){
           if($location_code!=''){
             $Usql = "UPDATE tbl_product_stock SET sku_price = '".$sku_price."' where sku='".$sku_code."' AND location_Code='".$location_code."'";
           }else{
            $Usql = "UPDATE tbl_product_stock SET sku_price = '".$sku_price."' where sku='".$sku_code."'";
           }
           $this->query($Usql);
        }
    }

    public function updatestock($jsonStr,$location_code,$total_qty='',$description=''){
     
      $resAry = json_decode($jsonStr);
      $location_code  = $location_code;
      $terminal_code  = $resAry->terminal;
      $createdby      = $resAry->created_by;
      $ItemsAry       = $resAry->stockDetails;
      $st_header = "INSERT INTO tbl_stocktake_header (location_code,stocktake_date,terminal_code,is_updated,createdby,createdon,total_qty,description) 
                    VALUES ('".$location_code."',GETDATE(),'".$terminal_code."',0,'".$createdby."',GETDATE(),'".$total_qty."','".$description."')";
      $this->query($st_header);
      $stocktake_id = $this->getLastId();
      if($stocktake_id!=''){
          for($i=0;$i<count($ItemsAry);$i++){
            $sku = $ItemsAry[$i]->sku;
            $qty = $ItemsAry[$i]->qty;
            $sku_description = $this->escape($ItemsAry[$i]->description); 
            $old_qty = $ItemsAry[$i]->old_qty;
            $stockdetails_sql = "INSERT INTO tbl_stocktake_detail (location_code,stocktake_id,sku,sku_description,sku_qty,scanned_qty,createdby,createdon)
                                 VALUES ('".$location_code."','".$stocktake_id."','".$sku."','".$sku_description."','".$old_qty."','".$qty."','".$createdby."',GETDATE())";
            $this->query($stockdetails_sql);
          }
      }     
      
    }

    public function escape($value) {
       $value= stripslashes(trim($value));
       return str_replace("'", "''", $value);   
  }

    public function clearField($field){
      $search_ary =array("'");
      $replace_ary =array("");
      return str_replace($search_ary, $replace_ary, $field);
    }

    public function validateLocation($location_code){
        $sql = "SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_code = '".$location_code."'";
        $res = $this->SelectQryMSSQL($sql);
        return $res[0]['location_code'];
    }
    function writeLog($str){

      $fileName = date('Y-m-d').'.txt';
      $file = SITELOCALPATH.'log/'.$fileName;
      $myfile = fopen($file, "a") or die("Unable to open file!");
      $txt = "\n";
      $txt.= "Posted Date Time"."-->".date('Y-m-d H:i:s.').gettimeofday()['usec']."\n";
      $txt.= "======================================================================================\n";
      $txt.= $str;
      fwrite($myfile, "\n". $txt);
      fclose($myfile);
    }  
}
?>
