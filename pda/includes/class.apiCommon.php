<?php
  class apiCommon
  {
	function replace($cnt)
	{
		$cnt = str_replace('"', "'", $cnt);
		$cnt = str_replace("\r","",$cnt);
		$cnt = str_replace(">	<","><",$cnt);
		$cnt = str_replace(">  <","><",$cnt);
		$cnt = str_replace(">	<","><",$cnt);
		$cnt = str_replace(">    <","><",$cnt);
		$cnt = str_replace(">	<","><",$cnt);
		$cnt = str_replace(">	<","><",$cnt);
		$cnt = trim($cnt);
		return $cnt;
	}
	function htmlallentities($str)
	{
	  $res = '';
	  $strlen = strlen($str);
	  for($i=0; $i<$strlen; $i++){
		$byte = ord($str[$i]);
		if($byte < 128) // 1-byte char
		  $res .= $str[$i];
		elseif($byte < 192); // invalid utf8
		elseif($byte < 224) // 2-byte char
		  $res .= '&#'.((63&$byte)*64 + (63&ord($str[++$i]))).';';
		elseif($byte < 240) // 3-byte char
		  $res .= '&#'.((15&$byte)*4096 + (63&ord($str[++$i]))*64 + (63&ord($str[++$i]))).';';
		elseif($byte < 248) // 4-byte char
		  $res .= '&#'.((15&$byte)*262144 + (63&ord($str[++$i]))*4096 + (63&ord($str[++$i]))*64 + (63&ord($str[++$i]))).';';
	  }
	  return $res;
	}

	function doStripContent($sContent)
	{
		$sContent					= str_replace(array("\t","\r","  ",'/n'),"",$sContent);
		$sContent					= stripslashes($sContent);
		$sContent					= preg_replace('#(<[/]?img.*>)#U', '', $sContent);
		$sContent					= str_replace(']]>', ']]&gt;', $sContent);
		$sContent					= str_replace('&#8217;', "'", $sContent);
		$sContent					= str_replace('"', "&rdquo;", $sContent);
		$sContent					= strip_tags($sContent);
		//$sContent					= addslashes($sContent);
		return $sContent;
	}
	function strip_tags_deep($array)
	{
	    array_walk($array,function(&$item){ if(!is_array($item)) { $item=strip_tags($item); } else { $item=$this->strip_tags_deep($item); } });
	    return $array;
	}
	function stringval($array)
	{
	    array_walk($array,function(&$item){ if(!is_array($item)) { $item=strval($item); } else { $item=$this->stringval($item); } });
	    return $array;
	}
	function arrayToJson($array,$key)
	{
     
      $array = $this->stringval($array);
      $result = array($key => $array);
      $result = json_encode($result);
      echo $result;
	}
	function changeDateFormat($date){
		$dateary = explode("/", $date);
		$newDate = $dateary[2].'-'.$dateary[1].'-'.$dateary[0];
		return $newDate;
}
}

?>