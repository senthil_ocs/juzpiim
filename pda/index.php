<?php
  include "common.php";
  error_reporting(0);
  $action ='';
  $location_code ='';
  $action = strtolower($_REQUEST['action']);
  $location_code = $_REQUEST['location_code'];
  $type ='';
  $strcontent = '';
  foreach ($_REQUEST as $key => $value) {
    $value = urldecode(stripslashes($value));
    $req[$key] = $value;
    $strcontent.=$key." - ".$value."<BR>";  
  }
  //$api->writeLog($strcontent);

  if($location_code!=''){
  	
  	$valide_location = $api->validateLocation($location_code);
  	if(!$valide_location){
  		$resultarray = array("status"=>"error","message"=>"Location code not found");
  		$api->arrayToJson($resultarray,$action);
  		exit;
  	}
  }else{
  		$resultarray = array("status"=>"error","message"=>"Location code should not empty");
  		$api->arrayToJson($resultarray,$action);
  		exit;
  }

 

  if($action=='appconfig'){
    $resultarray = array("status"=>"success","gstAmount"=>'7',"gstType"=>"P");
  }else if($action=='syncproduct'){
  	 $productsList = $api->getProducts($valide_location);
  	 $resultarray = array("status"=>"success","products"=>$productsList);
  }else if($action=='syncbarcodes'){
     $barcodesList = $api->getBarcodes($valide_location);
     $resultarray = array("status"=>"success","barcodes"=>$barcodesList);
  }else if($action=='syncvendors'){
     $vendorsList = $api->getVendors();
     $resultarray = array("status"=>"success","vendors"=>$vendorsList);
  }else if($action=='login'){
  	$userDetails  = $api->doUserLogin($_REQUEST);
    $locationCode = $api->dogetLocationCode();
    if($userDetails['status']=='failed') {
      $resultarray = array("status"=>"error","message"=>'Invalid username or password');
    } else {
      $resultarray = array("status"=>"success","userDetails"=>$userDetails,"locationCode"=>$locationCode);
    }
  }else if($action=='test'){
    $api->insertTest();
  }else if($action=='updateproduct'){
  	if($_REQUEST['sku']!='' && $_REQUEST['sku_price']!=''){
  		 $api->updateProduct($_REQUEST,$valide_location);
  		 $resultarray = array("status"=>"success","message"=>'updated success');	
  	}else{
  		$resultarray = array("status"=>"error","message"=>'Product SKU should not Empty');
  	}

  }else if($action=='ordersync') {
  	
    if(isset($_REQUEST['data'])){
      $orderInfo   = $api->ordersyncAPI($_REQUEST,$valide_location);
      $resultarray = array("status"=>"success","order"=>$orderInfo);
    }else{
      $resultarray = array("status"=>"error","message"=>"Order Details not posted");
    }
  }else if($action=='stockupdate'){
      
      /*$resary[]=array("sku"=>"000001","qty"=>"12","description"=>"ONION BAG","old_qty"=>"4");
      $resary[]=array("sku"=>"000002","qty"=>"22","description"=>"HAJMOLA IMLI SWEET","old_qty"=>"4");
      $mainary = array("location_code"=>"SK308","terminal"=>"Raguphone","created_by"=>"1","stockDetails"=>$resary);
      $_REQUEST['stock_details'] = json_encode($mainary);*/
      
      if(isset($_REQUEST['stock_details'])){
         $api->updatestock($_REQUEST['stock_details'],$valide_location,$_REQUEST['total_qty'],$_REQUEST['description']);
         $resultarray = array("status"=>"success","message"=>"stock updated successfully");
      }else{
         $resultarray = array("status"=>"error","message"=>"details not posted");
      }

 }else {
        $resultarray = array("status"=>"error","message"=>"Action not Found");
  }

  if($type==''){
    	$type = $action;
  }
  //print_r($resultarray);
  $api->arrayToJson($resultarray,$type);
?>